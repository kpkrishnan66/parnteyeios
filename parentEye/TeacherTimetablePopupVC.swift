//
//  TeacherTimetablePopupVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 29/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

import UIKit

class TeacherTimetablePopupVC: UIViewController,UITableViewDelegate,UITableViewDataSource
{
   
    @IBOutlet var alterTeacherofOtherClass: UIButton!
    
    @IBOutlet var alterTeacherofSameClass: UIButton!
    @IBOutlet var alterTeacherTableview: UITableView!
    @IBOutlet var alterOtherClass: UIButton!
    @IBOutlet var alterSameclass: UIButton!
    
    
    var user:userModel?
    var timeTableId = -1
    var classId     = -1
    var classEmployeeListArray = [EmployeeTimetableAlternateSameClass]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let backColur = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    let substitutedcolor = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)

   override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setUPUI()
        initializeTableview()
        getTeachersOfSameClass()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    private func setUPUI(){
        
        alterOtherClass.titleLabel!.numberOfLines = 2;
        alterOtherClass.titleLabel?.adjustsFontSizeToFitWidth = true
        alterSameclass.titleLabel!.numberOfLines = 2;
        alterSameclass.titleLabel?.adjustsFontSizeToFitWidth = true
        
        alterTeacherofOtherClass.backgroundColor = UIColor.white
        alterTeacherofOtherClass.setTitleColor(UIColor.black, for: .normal)
        alterTeacherofOtherClass.layer.borderWidth = 1
        alterTeacherofOtherClass.layer.borderColor = backColur.cgColor
        
        alterTeacherofSameClass.backgroundColor = colur
        alterTeacherofSameClass.setTitleColor(UIColor.white, for: .normal)
        alterTeacherofSameClass.layer.borderWidth = 0
        alterTeacherofSameClass.layer.borderColor = UIColor.white.cgColor
        
    }
    
    private func initializeTableview()
    {
        alterTeacherTableview.register(UINib(nibName: "TeacherTimetablePopupCell", bundle: nil), forCellReuseIdentifier: "TeacherTimetablePopupCell")
        alterTeacherTableview.estimatedRowHeight = 76.0
        alterTeacherTableview.rowHeight          = UITableViewAutomaticDimension
        alterTeacherTableview.allowsSelection = false
        
        
    }
    
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if classEmployeeListArray.count > 0{
            return classEmployeeListArray.count
        }
       return 0    
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeacherTimetablePopupCell") as! TeacherTimetablePopupCell
        let userPic = classEmployeeListArray[indexPath.row]
        
        cell.employeeName.text = classEmployeeListArray[indexPath.row].employeeName
        cell.employeeDesignation.text = classEmployeeListArray[indexPath.row].designation
        weak var profileImage = cell.employeeProfilePic
        
        if userPic.profilePic == ""{
            cell.employeeProfilePic.image = UIImage(named: "avatar")
            cell.employeeProfilePic.layer.masksToBounds = true
            cell.employeeProfilePic.layer.cornerRadius = 0
            cell.employeeProfilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: userPic.profilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    cell.employeeProfilePic.layer.masksToBounds = false
                    cell.employeeProfilePic.layer.cornerRadius = cell.employeeProfilePic.frame.height/2
                    cell.employeeProfilePic.clipsToBounds = true
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        cell.employeeSubstitute.layer.cornerRadius = 18
        cell.employeeSubstitute.layer.borderWidth = 1
        cell.employeeSubstitute.layer.borderColor = UIColor.black.cgColor
        cell.employeeSubstitute.tag = indexPath.row
        cell.employeeSubstitute.addTarget(self, action: #selector(self.substituteButtonTapped), for: UIControlEvents.touchUpInside)
       if  classEmployeeListArray[indexPath.row].substituted == true
            //if classEmployeeListArray[indexPath.row].em
       {
        cell.employeeSubstitute.backgroundColor = substitutedcolor
        cell.employeeSubstitute.setTitleColor(UIColor.white, for: .normal)
        cell.employeeSubstitute.setTitle("substituted", for: .normal)
        
        }
       else{
        cell.employeeSubstitute.backgroundColor = UIColor.white
        cell.employeeSubstitute.setTitleColor(UIColor.black, for: .normal)
        cell.employeeSubstitute.setTitle("substitute", for: .normal)
        
        }

        return cell
    }
    
    
    @IBAction func dismissMe(_ sender: Any) {
    
    self.dismiss(animated: false, completion: nil)
    }
    
    func getTeachersOfSameClass(){
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getTeachersListForSameClassTimetableSubstitution(classId: classId,timeTableId: timeTableId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
           self.classEmployeeListArray = parseEmployeeListForSubstitution.parseThisDictionaryForEmployeePopup(dictionary: result)
            self.alterTeacherTableview.reloadData()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
    }
    
    func getTeachersOfOtherClass(){
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getTeachersListForOtherClassTimetableSubstitution(timeTableId: timeTableId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.classEmployeeListArray.removeAll()
            self.classEmployeeListArray = parseEmployeeListForSubstitution.parseThisDictionaryForEmployeePopup(dictionary: result)
            self.alterTeacherTableview.reloadData()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
    }
    
    
    @objc func substituteButtonTapped(sender:UIButton){
        var substituteMessage = ""
        weak var weakSelf = self
        if classEmployeeListArray[sender.tag].substituted == false{
            substituteMessage = "Confirm teacher substitution"
        }
        else{
            substituteMessage = "Are you sure to cancel the teacher substitution?"
        }
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: substituteMessage, titleOfAlert: "ParentEye", oktitle: ConstantsInUI.confirm, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            hudControllerClass.showNormalHudToViewController(viewController: self)
            
            WebServiceApi.postSubstitution(userRoleId: (self.user?.currentUserRoleId)!,
                                           employeeId: self.classEmployeeListArray[sender.tag].employeeId ,
                                           timeTableId: self.timeTableId,
                                           substituted: self.classEmployeeListArray[sender.tag].substituted,
                                        onSuccess: { (result) in
                                            
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            if (result["error"] as? [String:AnyObject]) == nil{
                                                
                                                if self.classEmployeeListArray[sender.tag].substituted == false{
                                                
                                                 AlertController.showToastAlertWithMessage(message: ConstantsInUI.substitutionsuccess, stateOfMessage: .success)
                                                    for k in 0 ..< self.classEmployeeListArray.count{
                                                        
                                                        if k == sender.tag{
                                                            self.classEmployeeListArray[k].substituted = true
                                                            
                                                        }
                                                        else{
                                                            self.classEmployeeListArray[k].substituted = false
                                                            
                                                        }
                                                    }
                                                    self.alterTeacherTableview.reloadData()
                                                }
                                                else{
                                                  AlertController.showToastAlertWithMessage(message: ConstantsInUI.substitutionCancelsuccess, stateOfMessage: .success)
                                                    for k in 0 ..< self.classEmployeeListArray.count{
                                                        
                                                        if k == sender.tag{
                                                            self.classEmployeeListArray[k].substituted = false
                                                            
                                                        }
                                                        else{
                                                            self.classEmployeeListArray[k].substituted = false
                                                            
                                                        }
                                                    }
                                                    
                                                    self.alterTeacherTableview.reloadData()
                                                }
                                                
                                            }else{
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.substitutionFailure, stateOfMessage: .failure)
                                            }
                                            
                                            
                                            
                                            
                },
                                        onFailure: { (error) in
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                            
                },
                                        duringProgress: { (progress) in
                                            
            })
            
            
            
        }
        self.present(alert, animated: false, completion: nil)
        
    
    
    }
    
    
   
        
    @IBAction func alterTeacherofSameClassAction(_ sender: UIButton) {
     alterTeacherofOtherClass.backgroundColor = UIColor.white
        alterTeacherofOtherClass.setTitleColor(UIColor.black, for: .normal)
        alterTeacherofOtherClass.layer.borderWidth = 1
        alterTeacherofOtherClass.layer.borderColor = backColur.cgColor
        
        alterTeacherofSameClass.backgroundColor = colur
        alterTeacherofSameClass.setTitleColor(UIColor.white, for: .normal)
        alterTeacherofSameClass.layer.borderWidth = 0
        alterTeacherofSameClass.layer.borderColor = UIColor.white.cgColor
        
        getTeachersOfSameClass()
    }
    
    
    
    @IBAction func alterTeacherofOtherClassAction(_ sender: UIButton) {
        alterTeacherofSameClass.backgroundColor = UIColor.white
        alterTeacherofSameClass.setTitleColor(UIColor.black, for: .normal)
        alterTeacherofSameClass.layer.borderWidth = 1
        alterTeacherofSameClass.layer.borderColor = backColur.cgColor
        
        alterTeacherofOtherClass.backgroundColor = colur
        alterTeacherofOtherClass.setTitleColor(UIColor.white, for: .normal)
        alterTeacherofOtherClass.layer.borderWidth = 0
        alterTeacherofOtherClass.layer.borderColor = UIColor.white.cgColor
        getTeachersOfOtherClass()
        
        
    }
}
