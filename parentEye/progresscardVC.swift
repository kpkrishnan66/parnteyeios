//
//  progresscardVC.swift
//  parentEye
//
//  Created by scientia on 06/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import SpreadsheetView

class progresscardVC:UIViewController,UITextFieldDelegate,CommonHeaderProtocol,SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    @IBOutlet var progresscardSpreadsheetView: SpreadsheetView!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var evaluvationCategoryTextField: UITextField!
    @IBOutlet var evaluvationGroupTextField: UITextField!
    @IBOutlet var evaluvationGroupTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var evaluvationCategoryTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var lblNoResult: UILabel!
    
    var user:userModel?
    var profilePic = ""
    var evaluvationGroupList = [evaluvationGroup]()
    var displayProgressCardArray = Array<Array<Array<Array<String>>>>()
    var tempDisplayProgressCardArray = Array<Array<Array<Array<String>>>>()
    var displayPCArrayRowPosition = 0
    var displayPCArrayColPosition = 0
    var displayPCArrayGroupPosition = 0
    var displayPCArrayCatrgoryPosition = 0
    var rowCount = 0
    var mainSubCount = 0
    var studentId  = ""
    let examMarkList = ExamMarklist()
    var MarkList = [String] ()
    var mainSubCountArray = [Int]()
    var mark = [String]()
    var PCinner = false
    var currentEvaluvationGroup:Int = 0
    var currentSection = 0
    var datagridViewLoad = false
    let evaluvationGroupPicker = UIPickerView()
    let evaluvationCategoryPicker = UIPickerView()
    let studentAbsentcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let studentNotAssignedColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    var showOnlyProgressCard:Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        evaluvationGroupTextField.delegate = self
        evaluvationCategoryTextField.delegate = self
        setUpHeader()
        setupUI()
        setinitialIndexPathArrayPosition()
        getProgressReport()
    }
    
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }

    private func setUpHeader(){
        
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        header.profileChangeButton.isHidden = true
        header.profileImageRightSeparator.isHidden = true
        header.downArrowButton.isHidden = true
        header.multiSwitchButton.isHidden = true
        header.rightLabel.isHidden = true
        header.screenShowingLabel.text = ConstantsInUI.progressCard
        
    }
    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showSwitchSibling(fromButton:UIButton) {
        
    }
    
    private func setupUI() {
        lblNoResult.isHidden = true
        evaluvationGroupList.removeAll()
    }
    
    
    func getProgressReport(){
        
        if user?.currentTypeOfUser == .guardian{
            studentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getProgressReport(studentId: studentId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.evaluvationGroupList.removeAll()
            //self.progressCardHeaderArray.removeAll()
            self.populateGroupandCategoryEvaluvation(result: result)
            self.displayPCArrayRowPosition = 0
            if let studentProgressCardResult = result["studentProgressCard"] as? [String:[String:AnyObject]] {
                for(_,evaluvation_Group_List) in studentProgressCardResult {
                    
                    if let evaluvationGroupPosition = evaluvation_Group_List["position"] as? Int {
                        self.displayPCArrayGroupPosition = evaluvationGroupPosition
                    }
                    
                    for(evaluvation_Category,evaluvation_Category_list) in evaluvation_Group_List {
                        if evaluvation_Category != "position" {
                        if let evaluvationCategoryPosition = evaluvation_Category_list["position"] as? Int {
                            self.displayPCArrayCatrgoryPosition = evaluvationCategoryPosition
                        }
                        
                        if let headerArray = evaluvation_Category_list["header"] as? NSArray {
                                for _ in headerArray {
                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = "Bold"
                                }
                            
                        }
                            
                        var examSubjectsList = [examSubject]()
                        if let evaluvation_Subject_List = evaluvation_Category_list as? [String:AnyObject] {
                            for(evaluvationSubject,_) in evaluvation_Subject_List {
                                let examSubjectObject = examSubject()
                                examSubjectObject.setExamSubjctName(SubjectName: evaluvationSubject)
                               if let subList = evaluvation_Subject_List[evaluvationSubject] as? [String:AnyObject] {
                                print("exammm")
                                
                                    if let position = subList["position"] as? Int{
                                                self.displayPCArrayColPosition = 0
                                               self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = evaluvationSubject
                                        if self.showOnlyProgressCard {
                                            self.tempDisplayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = "Header"
                                            
                                        }
                                        
                                        
                                                self.displayPCArrayRowPosition = position
                                                
                                            }
                                            if let val = subList["value"] as? [String:AnyObject] {
                                                self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = evaluvationSubject
                                                
                                                examSubjectObject.setBranched(branchedornot: true)
                                                
                                                
                                                self.rowCount = self.rowCount + 1
                                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                                
                                                self.mainSubCount = self.mainSubCount + 1
                                                var examSubjects = [examSubject]()
                                                examSubjects = self.getInnerSubjectForProgressCard(subList: val)
                                                examSubjectObject.setExamSubjectList(subjectList: examSubjects)
                                            }
                                
                                            if let subList = subList["value"] as? NSArray {
                                                
                                                examSubjectObject.setBranched(branchedornot: false)
                                                self.displayPCArrayColPosition = 0
                                                self.rowCount = self.rowCount + 1
                                                self.mainSubCount = self.mainSubCount + 1
                                                self.mark.removeAll()
                                                self.displayPCArrayColPosition = 0
                                                
                                                //self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = evaluvationSubject
                                                
                                                for pos in subList{
                                                    self.mark.append(pos as! String)
                                                    examSubjectObject.examSubjectMarkList.append(pos as! String)
                                                    
                                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = pos as! String
                                                    
                                                    
                                                }
                                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                                
                                                
                                            }
                                        
                                                                        
                                }
                                examSubjectsList.append(examSubjectObject)
                                self.mainSubCountArray.append(self.mainSubCount-1)
                                self.mainSubCount = 0
                            }
                            self.examMarkList.setExamSubjectList(subjectList: examSubjectsList)
                            //self.examMarkList.setexamNames(self.progressCardArray)
                        }
                            if self.evaluvationGroupList.count > 0{
                                for pos in self.evaluvationGroupList {
                                    if pos.groupPosition == self.displayPCArrayGroupPosition {
                                        for sec in pos.evaluvationCategoryList {
                                            if sec.categoryPosition == self.displayPCArrayCatrgoryPosition {
                                                sec.categoryRowCount = self.rowCount
                                            }
                                        }
                                    }
                                }
                            }
                            print("row count=\(self.rowCount)")
                            self.rowCount = 0
                        }

                     }
                        
                        
              }
            }
            if self.evaluvationGroupList.count >= 1 {
                self.evaluvationGroupTextFieldHeight.constant = 30
                self.evaluvationCategoryTextFieldHeight.constant = 30
                self.lblNoResult.isHidden = true
                self.loadPickerView()
            }
            else{
                self.evaluvationGroupTextFieldHeight.constant = 0
                self.evaluvationCategoryTextFieldHeight.constant = 0
                self.lblNoResult.isHidden = false
            }

           self.progresscardSpreadsheetView.reloadData()
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            debugPrint(error.localizedDescription)
            self.evaluvationGroupTextFieldHeight.constant = 0
            self.evaluvationCategoryTextFieldHeight.constant = 0
        }) { (progress) in
            
        }
    }
    
    func populateGroupandCategoryEvaluvation(result:Dictionary<String, AnyObject>) {
        
        if let studentProgressCardResult = result["studentProgressCard"] as? [String:[String:AnyObject]] {
            
            for(evaluvation_Group,evaluvation_Group_List) in studentProgressCardResult {
                print(evaluvation_Group)
                if let groupPosition = evaluvation_Group_List["position"] as? Int {
                    if groupPosition == 0 {
                        self.currentEvaluvationGroup = groupPosition
                    }
                    let evaluvation_groupObject = evaluvationGroup()
                    evaluvation_groupObject.evaluvationGroupName = evaluvation_Group
                    evaluvation_groupObject.groupPosition = groupPosition
                    var evaluvationCategoryList = [evaluvationCategory]()
                       for(evaluvation_Category,evaluvation_category_list) in evaluvation_Group_List {
                        if evaluvation_Category != "position" {
                         
                         let evaluvationCategoryObject = evaluvationCategory()
                            if let categoryPosition = evaluvation_category_list["position"] as? Int {
                              evaluvationCategoryObject.categoryName = evaluvation_Category
                              evaluvationCategoryObject.categoryPosition = categoryPosition
                            }
                           var categoryHeaderList = [String]()
                           if let headerArray = evaluvation_category_list["header"] as? NSArray {
                              for header in headerArray {
                                 categoryHeaderList.append(header as! String)
                               }
                               evaluvationCategoryObject.categoryHeaderArray = categoryHeaderList
                           }
                            if let categorySubjectList = evaluvation_category_list as? [String:AnyObject] {
                                var evaluvationSubjectList = [evaluvationSubject]()
                                for(subject,subList) in categorySubjectList{
                                
                                    if subject != "position" && subject != "header"{
                                       let evaluvationSubjectObject = evaluvationSubject()
                                        if let subjectPosition = subList["position"] as? Int {
                                            evaluvationSubjectObject.subjectName = subject
                                            evaluvationSubjectObject.subjectPosition = subjectPosition
                                        }
                                         evaluvationSubjectList.append(evaluvationSubjectObject)
                                    }
                                    evaluvationCategoryObject.categorySubjectArray = evaluvationSubjectList
                                }
                            }
                            
                        
                         evaluvationCategoryList.append(evaluvationCategoryObject)
                         
                      }
                     evaluvation_groupObject.evaluvationCategoryList = evaluvationCategoryList
                        
                    }
                    evaluvationGroupList.append(evaluvation_groupObject)
                }
               
                
            }
            
            
            
            
        }
        else{
            
        }
    }
    
    func setinitialIndexPathArrayPosition(){
        
        displayProgressCardArray.removeAll()
        for _ in 0..<20 {
             //displayProgressCardArray.append(Array(count:20, repeatedValue:String()))
            displayProgressCardArray.append(Array(repeating:Array(repeating:Array(repeating:String(), count:20), count:100),count:20))
        }
        if user?.currentTypeOfUser == .guardian{
           if user?.Students![(user?.currentOptionSelected)!].showOnlyProgressCard != 0 {
              showOnlyProgressCard = true
            }
            else{
              showOnlyProgressCard = false
            }
        }
        if showOnlyProgressCard {
            tempDisplayProgressCardArray.removeAll()
            for _ in 0..<20 {
                tempDisplayProgressCardArray.append(Array(repeating:Array(repeating:Array(repeating:String(), count:20), count:100),count:20))
            }
        }
        
        
        
        for group in 0..<20{
            for category in 0..<20{
                for row in 0..<100 {
                    for col in 0..<20 {
                       displayProgressCardArray[group][category][row][col] = ""
                          if showOnlyProgressCard {
                            tempDisplayProgressCardArray[group][category][row][col] = ""
                          }
                    }
                }
               
            }
        }
    }
    
    func getInnerSubjectForProgressCard(subList:[String:AnyObject]) -> [examSubject] {
        PCinner = true
        var examSubjects = [examSubject]()
        var examInnerSubjects = [examSubject]()
        
        for(key,_) in subList{
            let examSubjectObject = examSubject()
            examSubjectObject.setExamSubjctName(SubjectName: key)
            if let subSubList = subList[key] as? [String:AnyObject] {
                if let position = subSubList["position"] as? Int{
                    self.displayPCArrayColPosition = 0
                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = key
                    self.displayPCArrayRowPosition = position
                    
                }
                if let val = subSubList["value"] as? [String:AnyObject] {
                    
                    examSubjectObject.setBranched(branchedornot: true)
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    examInnerSubjects = getInnerSubjectForProgressCard(subList: val)
                    examSubjectObject.setExamSubjectList(subjectList: examInnerSubjects)
                }
                
                if let subSubList = subSubList["value"] as? NSArray {
                    examSubjectObject.setBranched(branchedornot: false)
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    self.mark.removeAll()
                    displayPCArrayColPosition = 0
                    
                    displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][displayPCArrayRowPosition][displayPCArrayColPosition]   = "      "+key
                    
                    for pos in subSubList{
                        self.mark.append(pos as! String)
                        examSubjectObject.examSubjectMarkList.append(pos as! String)
                        displayPCArrayColPosition = displayPCArrayColPosition + 1
                        displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][displayPCArrayRowPosition][displayPCArrayColPosition]   = pos as! String
                        
                    }
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    
                }
            }
            examSubjects.append(examSubjectObject)
        }
        return examSubjects
    }

    
    func loadPickerView() {
        
        evaluvationGroupPicker.tag = 0
        evaluvationGroupPicker.delegate = self
        evaluvationGroupPicker.dataSource  = self
        evaluvationGroupTextField.inputView = evaluvationGroupPicker
        evaluvationGroupPicker.backgroundColor = UIColor.clear
        if evaluvationGroupList.count == 1 {
         evaluvationGroupTextField.isUserInteractionEnabled = false
        }
        else{
         evaluvationGroupTextField.isUserInteractionEnabled = true
        }
        self.evaluvationGroupPicker.reloadAllComponents()
        
        evaluvationCategoryPicker.tag = 1
        evaluvationCategoryPicker.delegate = self
        evaluvationCategoryPicker.dataSource = self
        evaluvationCategoryTextField.inputView = evaluvationCategoryPicker
        self.evaluvationCategoryPicker.reloadAllComponents()
        currentSection = 0
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == 0 {
                    evaluvationGroupTextField.text = pos.evaluvationGroupName
                    if pos.evaluvationCategoryList.count == 1{
                       evaluvationCategoryTextField.isUserInteractionEnabled = false
                    }
                    else{
                        evaluvationCategoryTextField.isUserInteractionEnabled = true
                    }
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                           evaluvationCategoryTextField.text = sec.categoryName
                        }
                    }

                    
                    
                }
            }
        }
        progresscardSpreadsheetView.dataSource = self
        progresscardSpreadsheetView.delegate = self
        progresscardSpreadsheetView.register(SpreadSheetCell.self, forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        progresscardSpreadsheetView.register(UINib(nibName: String(describing: SpreadSheetCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        progresscardSpreadsheetView.backgroundColor = .white
        self.currentSection = 0
         progresscardSpreadsheetView.reloadData()
        
    }
}

extension progresscardVC: UIPickerViewDataSource {
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0  {
            if self.evaluvationGroupList.count > 0 {
                
                return evaluvationGroupList.count
            }
            
        }
        if pickerView.tag == 1 {
            if self.evaluvationGroupList.count > 0 {
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        return pos.evaluvationCategoryList.count
                    }
                }
            }
        }
        
        return 0
    }
}

extension progresscardVC: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView.tag == 0 {
         if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == row {
                    print(pos.evaluvationGroupName)
                    return pos.evaluvationGroupName
                }
            }
         }
            
        }
        
        if pickerView.tag == 1 {
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition == row {
                              return sec.categoryName
                            }
                        }
                        
                    }
                }
            }
        }
        return ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
           currentEvaluvationGroup = row
           currentSection = 0
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == row {
                        evaluvationGroupTextField.text = pos.evaluvationGroupName
                        if pos.evaluvationCategoryList.count == 1 {
                            evaluvationCategoryTextField.isUserInteractionEnabled = false
                        }
                        else{
                            evaluvationCategoryTextField.isUserInteractionEnabled = true
                        }
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition ==  currentSection{
                              evaluvationCategoryTextField.text = sec.categoryName
                            }
                        }
                        
                        
                    }
                }
            }
           evaluvationCategoryPicker.selectRow(0, inComponent: 0, animated: false)
           progresscardSpreadsheetView.reloadData()
        }
        if pickerView.tag == 1 {
            currentSection = row
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition ==  row{
                                evaluvationCategoryTextField.text = sec.categoryName
                            }
                        }
                        
                        
                    }
                }
            }
            progresscardSpreadsheetView.reloadData()
        }
        
    }
    
}
// MARK: spreadsheetViewDataSource

extension progresscardVC {
    
    enum Colors {
        static let border = UIColor.lightGray
        static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                           return sec.categoryHeaderArray.count
                        }
                    }
                }
            }
            
        }
        return 0
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        if self.evaluvationGroupList.count > 0{
            for pos in self.evaluvationGroupList {
                if pos.groupPosition == self.currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                            return sec.categoryRowCount+1
                        }
                    }
                }
             }
            
        }
        return 0
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 100
        }
        return 100
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return 44
        }
        return 44
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if self.evaluvationGroupList.count > 0{
            return 1
        }
        return 0
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        if self.evaluvationGroupList.count > 0{
            return 1
        }
        return 0
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SpreadSheetCell.self), for: indexPath) as! SpreadSheetCell
        cell.label.textColor = UIColor.black
        if indexPath.column == 0 && indexPath.row == 0 {
            cell.label.text = "Subjects"
            cell.label.backgroundColor = Colors.headerBackground
        }
        else if indexPath.column == 0 && indexPath.row > 0 {
            cell.label.backgroundColor = Colors.headerBackground
            if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                            for sub in sec.categorySubjectArray {
                                if sub.subjectPosition == indexPath.row-1 {
                                    cell.label.text = sub.subjectName
                                }
                            }
                        }
                    }
                }
             }
                
            }
        }
        else if indexPath.column > 0 && indexPath.row == 0 {
            cell.label.backgroundColor = Colors.headerBackground
                if evaluvationGroupList.count > 0{
                    for pos in evaluvationGroupList {
                        if pos.groupPosition == currentEvaluvationGroup {
                            for sec in pos.evaluvationCategoryList {
                                if sec.categoryPosition == currentSection {
                                      cell.label.text =  sec.categoryHeaderArray[indexPath.column]
                                }
                            }
                        }
                    }
                    
                }
        }
        else{
             if indexPath.column == 0 {
                cell.label.textAlignment = .left
             }else{
                cell.label.textAlignment = .center
             }

            if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.row-1][indexPath.column] != "Bold"{

                                cell.label.text = displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.row-1][indexPath.column]

                            }
                            else{
                                cell.label.text = ""

                            }

                            if evaluvationGroupList.count > 0{
                                for pos in evaluvationGroupList {
                                    if pos.groupPosition == currentEvaluvationGroup {
                                        for sec in pos.evaluvationCategoryList {
                                            if sec.categoryPosition == currentSection {



                                        if indexPath.column < pos.evaluvationCategoryList[currentSection].categoryHeaderArray.count{
                                            print(self.currentSection)

                                        }
                                       }
                                        }
                                    }
                                }
                            }

                            if showOnlyProgressCard {
                                if self.tempDisplayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.row-1][indexPath.column] == "Header" {
                                    cell.backgroundColor = Colors.headerBackground
                                }
                            }


                            if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.row-1][indexPath.column] == "AB"{
                                cell.label.textColor = studentAbsentcolur
                            }
                            else if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.row-1][indexPath.column] == "NA"{
                                cell.label.textColor = studentNotAssignedColor
                            }
                            else{
                                cell.label.textColor = UIColor.black
                            }
            cell.label.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
            }
        
        
        return cell
        
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
    }
    
 
}
