//
//  LeaveTC.swift
//  parentEye
//
//  Created by Emvigo on 7/25/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class LeaveTC: UITableViewCell {

    @IBOutlet var descriptionText: UILabel!
    @IBOutlet var descriptionTextHeight: NSLayoutConstraint!
    @IBOutlet var btnSubmitWidth: NSLayoutConstraint!
    @IBOutlet var lblStatusHeight: NSLayoutConstraint!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnSubmit: UIButton!
    var id = NSIndexPath()
    var user:userModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        getUserDataFromDB()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        if user?.currentTypeOfUser == .employee{
           btnSubmit.isHidden = true
        }
        
    }
    
    @IBAction func submitButtonPressed(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: id)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierForPopUp"), object: nil)
    }
}
