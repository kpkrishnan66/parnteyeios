//
//  StudentTimetableVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 03/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import SpreadsheetView

class StudentTimetableVC:UIViewController,SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    @IBOutlet var classTeacherProfilepic: UIImageView!
    @IBOutlet var nameOfClassTeacher: UILabel!
    @IBOutlet var noTimetableButton: UIButton!
    @IBOutlet var spreadsheetView: SpreadsheetView!
    
    var user:userModel?
    var columnCount = 0
    var weeksRowcount = 0
    var profilePic = ""
    var weeks = [String]()
    var timeTableList = [String:AnyObject]()
    var emptyTimetable = false
    lazy var filteredTimetableListingDictionary = [String:[TimetableModelClass]]()
    lazy var timeTableListingDictionary         = [String:[TimetableModelClass]]()
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setupUI()
        getTimetableFromAPIForClassTimetable()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    func setupUI() {
        noTimetableButton.isHidden = true

        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self
        spreadsheetView.register(SpreadSheetCell.self, forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        spreadsheetView.register(UINib(nibName: String(describing: SpreadSheetCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        spreadsheetView.backgroundColor = .white
        
        // Do any additional setup after loading the view.
        
        nameOfClassTeacher.text =  (user?.Students![(user?.currentOptionSelected)!].classTeacher)!
        nameOfClassTeacher.text =  (user?.Students![(user?.currentOptionSelected)!].classTeacher)!
        
    }
    
    func getTimetableFromAPIForClassTimetable(){
        self.timeTableList.removeAll()
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getTimeTableByClassId( classId: (user?.Students![(user?.currentOptionSelected)!].classId)!, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            let resultTuple =  parseTimetable.parseFromDictionary(dictionary: result)
            if let err =  resultTuple.1{
                AlertController.showToastAlertWithMessage(message: err.localizedDescription, stateOfMessage: .failure)
            }else {
                weakSelf!.addEntriesToDictionary(resultTuple.0)
            }
            
            if let a = result["schoolClass"] as? NSDictionary{
                
                
                
                if let classinchrg = a["classInCharge"] as? [String : AnyObject]{// get the root key
                    
                    if let name = classinchrg["name"] as? String{
                        self.nameOfClassTeacher.text = name
                        
                    }
                    
                    if let prfpic = classinchrg["profilePicUrl"] as? String{
                        
                        let profileImage = self.classTeacherProfilepic
                        if prfpic == "" {
                            self.classTeacherProfilepic.image = UIImage(named: "avatar")
                        }
                        else{
                            ImageAPi.fetchImageForUrl(urlString: prfpic, oneSuccess: { (image) in
                                if profileImage != nil {
                                    
                                    profileImage!.image = image
                                    
                                }
                            }) { (error) in
                                
                            }
                        }
                    }
                    
                }
            }
            
            if let a = result["schoolClass"] as? NSDictionary{
                
                if let tempResult = a["timeTable"] as? NSDictionary{// get the root key
                    
                    weakSelf!.timeTableList = tempResult as! [String : AnyObject]
                    
                    self.weeks.removeAll()
                    for (key,dateSeparatedDiary) in tempResult{
                        self.weeks.append(key as! String)
                        if let diaryArray = dateSeparatedDiary as? [[String:AnyObject]]{
                            self.columnCount = diaryArray.count
                            
                        }
                    }
                    
                    
                    self.weeksRowcount = self.weeks.count
                    self.sortDate()
                    self.spreadsheetView.reloadData()
                    
                    
                    
                }
                else{
                    self.noTimetableButton.isHidden = false
                    self.emptyTimetable = true
                    self.spreadsheetView.reloadData()
                }
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
        
    }
    
    private func sortDate(){
        var tempArray = [String]()
        var days = [Int]()
        
        for day in weeks {
            
            let formatterWeekday = DateFormatter()
            
            formatterWeekday.dateFormat = "e"
            
            let weekday = formatterWeekday.date(from: day)
            
            let weekdayString = formatterWeekday.string(from: weekday!)
            
            var dayInt = Int(weekdayString)
            dayInt = dayInt! - 1
            days.append(dayInt!)
            
        }
        
        days.sort()
        
        let dateFormatter = DateFormatter()
        tempArray.removeAll()
        weeks.removeAll()
        for dayIndex in days
        {
            tempArray.append(dateFormatter.weekdaySymbols[dayIndex])
            
        }
        let capitalizedArray = tempArray.map { $0.uppercased()}
        weeks = capitalizedArray
        print(weeks)
    }
    
    private func addEntriesToDictionary(_ dictionary:[String:[TimetableModelClass]]){
        filteredTimetableListingDictionary.addEntriesFromDictionary(otherDictionary: dictionary)
        timeTableListingDictionary = filteredTimetableListingDictionary
    }
  
    
}
  extension StudentTimetableVC{

        enum Colors {
            static let border = UIColor.lightGray
            static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
        }
   }
    // MARK: spreadsheetViewDataSource
    
    extension StudentTimetableVC{
        
        func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
            if emptyTimetable == true{
                return 0
            }
            return columnCount+1
        }
        
        func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
            if emptyTimetable == true{
                return 0
            }
            return weeksRowcount+1
        }
        
        func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
            if column == 0 {
                return 100
            }
            return 100
        }
        
        func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
            if row == 0 {
                return 44
            }
            return 44
        }
        
        func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
            if timeTableList.count > 0 {
              return 1
            }
            return 0
        }
        
        func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
            if timeTableList.count > 0 {
                return 1
            }
            return 0
        }
        
        func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
           let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SpreadSheetCell.self), for: indexPath) as! SpreadSheetCell
            
            if indexPath.column == 0 && indexPath.row == 0 {
                cell.label.text = ""
                cell.label.backgroundColor = Colors.headerBackground
            }
            else if indexPath.column == 0 && indexPath.row > 0 {
                cell.label.backgroundColor = Colors.headerBackground
                if weeks.count > 0{
                  let strNumber: NSString = weeks[indexPath.row - 1] as NSString
                    cell.label.text = (strNumber.substring(with: NSRange(location: 0, length:3)))

                }else{
                    cell.label.text = ""
                }
            }
            else if indexPath.column > 0 && indexPath.row == 0 {
                cell.label.backgroundColor = Colors.headerBackground
                cell.label.text = String(indexPath.column)
            }
            else{
                if timeTableListingDictionary.count > 0 {
                  cell.label.backgroundColor = UIColor.white
                  cell.label.text = timeTableListingDictionary[weeks[indexPath.row-1]]![indexPath.column-1].subjectNickName
                    
                }
            }
            
            return cell
            
        }
        
        func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
            print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
        }


}
