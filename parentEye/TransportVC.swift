//
//  TransportVC.swift
//  parentEye
//
//  Created by Emvigo on 7/9/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import GoogleMaps

class TransportVC: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,GMSMapViewDelegate,SelectedSchoolpopOverDelegate {
    
    var user:userModel?
    var busArrivalStatus:String?
    var profilePic = ""
    var studentDetailedListArray = [EmployeeDetailedStudentProfile]()
    lazy var locationArray = [Transport]()
    var timer = Timer()
    var zoomLevel: Float = 0.0
    var bearing: Double = 0.0
    var schoolId:Int = 0
    var studentId = ""
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet weak var lblSchoolBusName: UILabel!
    @IBOutlet weak var lblSchoolBusPlate: UILabel!
    @IBOutlet weak var imgOnTimeIndicator: UIImageView!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblBusAssistant: UILabel!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet weak var lastUpdationLabel: UILabel!
    @IBOutlet weak var horizontalLine1: UIView!
    @IBOutlet weak var horizontalLine2: UIView!
    @IBOutlet weak var refreshButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpHeader ()
        getUserDataFromDB()
        map.isMyLocationEnabled = true
        map.settings.myLocationButton = true
        self.map.delegate = self
        var transportType = ""
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.getStudentCurrentlySelectedSchool()?.schoolId)!
        }
        if user?.currentTypeOfUser == .guardian{
            transportType = (user?.getStudentCurrentlySelectedStudent()!.transportType)!
        }else{
            if studentDetailedListArray.count > 0{
                transportType = studentDetailedListArray[0].transportType
            }
        }
        if transportType == "SchoolBus"
        {
            getLocations()
            scheduledTimerWithTimeInterval()
        }else{
            lastUpdationLabel.isHidden = true
            refreshButton.isHidden = true
            horizontalLine1.isHidden = true
            horizontalLine2.isHidden = true
        }
        
        
        // Do any additional setup after loading the view.
    }
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        setupTransportPage()
    }
    
    func scheduledTimerWithTimeInterval(){
        // Scheduling timer to Call the function **Countdown** with the interval of 1 seconds
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(TransportVC.getLocations), userInfo: nil, repeats: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    override func viewWillAppear(_ animated: Bool) {
//        user = DBController().getUserFromDB()
//
//    }
    /* override func viewDidDisappear(animated: Bool) {
     self.navigationController?.popToRootViewControllerAnimated(false)
     
     }*/
    
    @IBAction func refreshButtonClicked(_ sender: AnyObject) {
        getLocations()
    }
    
    @objc fileprivate func getLocations(){
        weak var weakSelf = self
        
        if user?.currentTypeOfUser == .guardian{
            studentId = user!.getStudentCurrentlySelectedStudent()!.id
        }
        WebServiceApi.getBusLocation(correspondingTostudentID: String(studentId), onSuccess: {(result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.locationArray = parseTransportListArray.parseThisDictionaryForLocationRecords(dictionary: result)
            if self.locationArray.count > 0{
                self.markLocation(self.locationArray[0])
            }
            if let errorMessage = result["error"] as? [String:Any]{
                if let message = errorMessage["message"] as? String{
                    self.lastUpdationLabel.text = message
                }
            }
            
        }, onFailure: {(error) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }, duringProgress: {(progress) in
            
        })
        
        
        
    }
    
    fileprivate func markLocation(_ location: Transport){
        map.clear()
        let myLocation = CLLocationCoordinate2D(latitude: CLLocationDegrees(location.latitude)!,longitude: CLLocationDegrees( location.longitude)!)
        
        if zoomLevel == 0.0{
            zoomLevel = 15.0
            bearing = 0
            map.camera = GMSCameraPosition(target: myLocation, zoom: zoomLevel, bearing: bearing, viewingAngle: 0)
        }else{
            zoomLevel = map.camera.zoom
            bearing = map.camera.bearing
        }
        self.lastUpdationLabel.text = "Last Updated On: "+location.lastUpdatedTime
        let marker = GMSMarker(position: myLocation)
        marker.tracksViewChanges = true
        marker.map = map
    }
    
    
    //MARK:- Heder delegates
    fileprivate func setUpHeader (){
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        header.profileChangeButton.isHidden = true
        header.profileImageRightSeparator.isHidden = true
        header.downArrowButton.isHidden = true
        header.multiSwitchButton.isHidden = true
        header.rightLabel.isHidden = true
        header.screenShowingLabel.text = ConstantsInUI.transportDetails
        
    }
    // MARK: header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
        if user!.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        
    }
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        header.multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: UIControlState())
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.maketabBarRoot()
    }
    
    
    
    @IBAction func callButtonTapped(_ sender: AnyObject) {
        
        callNumber1(lblPhoneNumber.text!)
    }
}
// MARK: UIViewEvents
extension TransportVC{
    fileprivate func callNumber(_ phoneNumber:String) {
        
        let formatedNumber = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        let phoneUrl       = "tel://\(formatedNumber)"
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: "", titleOfAlert:formatedNumber, oktitle: ConstantsInUI.call, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            let url:URL      = URL(string: phoneUrl)!
            UIApplication.shared.openURL(url)
            
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    
    //this is added by jithin on 27-03-2018 for making call on transport
    fileprivate func callNumber1(_ phoneNumber:String){
        let formatedNumber = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        
        if let url = URL(string: "tel://\(  formatedNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func setupTransportPage(){
        
        if user?.currentTypeOfUser == .guardian{
            if user?.Students![0].transportType == "SchoolBus"{
                lblSchoolBusName.text  = user?.Students![0].busNo
                lblPhoneNumber.text    = user?.Students![0].busContact
                lblSchoolBusPlate.text = user?.Students![0].busPlate
                if busArrivalStatus == "onTime"{
                    imgOnTimeIndicator.image = UIImage(named: "ontime_icon")
                }
                else{
                    imgOnTimeIndicator.image = UIImage(named: "tdelay_icon")
                }
            }
            else{
                lblSchoolBusPlate.isHidden  = true
                imgOnTimeIndicator.isHidden = true
                lblPhoneNumber.isHidden     = true
                lblBusAssistant.isHidden    = true
                btnCall.isHidden            = true
                lblSchoolBusName.text     = user?.Students![0].transportType
                
            }
            
        }
        else{
            if studentDetailedListArray.count > 0{
                if studentDetailedListArray[0].transportType == "SchoolBus"{
                    lblSchoolBusName.text  = studentDetailedListArray[0].busNo
                    lblPhoneNumber.text    = studentDetailedListArray[0].contactNo
                    lblSchoolBusPlate.text = studentDetailedListArray[0].regNo
                    if busArrivalStatus == "onTime"{
                        imgOnTimeIndicator.image = UIImage(named: "ontime_icon")
                    }
                    else{
                        imgOnTimeIndicator.image = UIImage(named: "tdelay_icon")
                    }
                }
                else{
                    lblSchoolBusPlate.isHidden  = true
                    imgOnTimeIndicator.isHidden = true
                    lblPhoneNumber.isHidden     = true
                    lblBusAssistant.isHidden    = true
                    btnCall.isHidden            = true
                    lblSchoolBusName.text     = studentDetailedListArray[0].transportType
                }
                
            }
        }
    }
    
}
