//
//  GraphVC.swift
//  parentEye
//
//  Created by Emvigo on 7/19/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Charts

class GraphVC:UIViewController,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,UITextFieldDelegate {
    
   
    private let dirSelectorHeight: CGFloat = 5
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet weak var subjectPickerRef: UIPickerView!
    @IBOutlet weak var termsPickerRef: UIPickerView!
    @IBOutlet var txtsubject: UITextField!
    @IBOutlet var graphPresentingView: HorizontalBarChartView!
    @IBOutlet var txtExam: UITextField!
    
   
    
    var user:userModel?
    var graphResults = [AnyObject]()
    var defaultResults = [AnyObject]()
    var subjectArray = [String]()
    var termArray  = [String]()
    var selectedTerm = Int()
    var selectedSubject = String()
    var studentId = 0
    var passStudentId = ""
    var examPicker = UIPickerView()
    var subjectPicker = UIPickerView()
    var months: [String]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedSubject = "All Subjects"
        txtsubject.delegate = self
        txtExam.delegate = self
        
        getUserDataFromDB()
        setUpHeader()
        }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       
    }
    private func setUpHeader(){
        
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        header.profileChangeButton.isHidden = true
        header.profileImageRightSeparator.isHidden = true
        header.downArrowButton.isHidden = true
        header.multiSwitchButton.isHidden = true
        header.rightLabel.isHidden = true
        header.screenShowingLabel.text = ConstantsInUI.graph
        
    }
    
    
}
// MARK: PickerView Delegates
extension GraphVC:UIPickerViewDelegate,UIPickerViewDataSource{
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if pickerView == examPicker{
            return termArray.count
        }
       return subjectArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == examPicker{
            return termArray[row]
        }else{
            return subjectArray[row]
        }
     }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        label?.font = UIFont(name: "Times New Roman", size: 17.0)
        if pickerView == examPicker{
            label?.text = termArray[row]
        }else{
            label?.text = subjectArray[row]
        }
        label?.textAlignment = NSTextAlignment.center
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if  pickerView == examPicker{
            if row == 0{
                selectedTerm = 0
                filterData()
            }
            else{
                selectedTerm = row
                filterData()
            }
            txtExam.text = termArray[row]
        }else{
            if row == 0{
                selectedSubject = "All Subjects"
                filterData()
            }else{
                selectedSubject = subjectArray[row]
                filterData()
            }
            txtsubject.text = selectedSubject

        }
    }
    
    
}
//// MARK: UIView Events

extension GraphVC{
    func getGraphReport(){
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        if user?.currentTypeOfUser == .guardian{
            passStudentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        else{
            let defaults = UserDefaults.standard
            
            let  studentd = defaults.integer(forKey: "selectedStudentId")
            passStudentId = String(studentd)
            
           // passStudentId = String(studentId)
        }
       
        
        WebServiceApi.getProgressGraph(studentId: passStudentId,graphID: "1", onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let tempResult = result["graphAnalysis"] as? NSArray {
                if tempResult.count > 0{
                    weakSelf!.graphResults = tempResult as [AnyObject]
                    weakSelf!.defaultResults = weakSelf!.graphResults
                    weakSelf!.graphResults.remove(at: 0)
                    debugPrint("Graph result",tempResult)
                    weakSelf!.configPickerView()
                }
                
            }
        }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }) { (progress) in
            
        }
        
    }
    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        getGraphReport()
    }
    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showSwitchSibling(fromButton:UIButton) {
        //PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        
    }
    func configGraphView() {
      parseArrayToGraphData(graphResults as NSArray)
    }
    
    func filterData() {
        
        if selectedSubject == "All Subjects" && selectedTerm == 0 {
            parseArrayToGraphData(graphResults as NSArray)
        }
        else{
        filteringGraphData(array: graphResults as NSArray,subject: selectedSubject,postion: selectedTerm)
        }
    }
    
    func configPickerView(){
        var filterarray = defaultResults
        termArray = (filterarray[0] as? [String])!
        termArray[0] = "All Exams"
        filterarray.remove(at: 0)
        for item in filterarray{
            let element = item as? NSArray//(item[0] as? String)!
            let value = element![0] as? NSArray
            subjectArray.append((value![0] as? String)!)
            
        }
        debugPrint("Terms..",termArray)
        debugPrint("Subjects",subjectArray)
        subjectArray.insert("All Subjects", at: 0)
        txtExam.text = termArray[0]
        txtsubject.text = subjectArray[0]
        configGraphView()
        loadPickerView()
    }
    func loadPickerView() {
        examPicker.delegate = self
        examPicker.dataSource = self
        examPicker.backgroundColor = UIColor.clear
        txtExam.inputView = examPicker
        examPicker.reloadAllComponents()
        
        subjectPicker.delegate = self
        subjectPicker.delegate = self
        txtsubject.inputView = subjectPicker
        subjectPicker.reloadAllComponents()
    }
    func filteringGraphData(array:NSArray,subject:String,postion:Int) {
        var titleArray = [String]()
        var thisPointArray:[Double] = []
        var index = 0
        for item in array {
            
            let totalItems = (item as AnyObject).count
            let firstItem = item as? NSArray
            let values = firstItem![0] as? NSArray
            if postion != 0 && subject == "All Subjects" {
               titleArray.append(values![1] as! String)
            }
            else{
                if index == 0{
                 titleArray.append(values![1] as! String)
                 index = 1
                }
            }

            if values![0] as! String == subject || subject == "All Subjects"{
             if let totalItems = totalItems {
              for grade in 1..<totalItems {
                if postion == 0 || grade == postion{
                    if postion == 0 {
                      let x = firstItem![grade] as! Double
//                      let newBarItem = (max:Double(round(1000*x)/1000))
//                      thisPointArray.append(newBarItem)
                        let roundvalue = round(1000*x)
                        let orgValue = roundvalue/1000
                        thisPointArray.append(orgValue)
                      }
                    else{
                        if postion == grade{
                        let x = firstItem![grade] as! Double
//                        let newBarItem = (max:Double(round(1000*x)/1000))
//                        thisPointArray.append(newBarItem)
                            let roundvalue = round(1000*x)
                            let orgValue = roundvalue/1000
                            thisPointArray.append(orgValue)
//
                        break
                        }

                    }
                }else if grade != postion{
                    if postion == 0{
                    let x = firstItem![grade] as! Double
                        
//                    let newBarItem = (max:Double(round(1000*x)/1000))
//
//                    thisPointArray.append(newBarItem)
                        let roundvalue = round(1000*x)
                        let orgValue = roundvalue/1000
                        thisPointArray.append(orgValue)
                    }
                    else{
                        if postion == grade{
                            
                        let x = firstItem![grade] as! Double
                            
//                        let newBarItem = (max:Double(round(1000*x)/1000))
//                        thisPointArray.append(newBarItem)
                            let roundvalue = round(1000*x)
                            let orgValue = roundvalue/1000
                            thisPointArray.append(orgValue)
                        break
                        }
                    }
                }
            }
            }
            }
            
            
        }
        
       setChart(dataPoints: titleArray,values: thisPointArray)
        
        
    }
    func parseArrayToGraphData(_ array:NSArray){
        var titleArray = [String]()
        var thisPointArray:[Double] = []
        for j in 1..<termArray.count {
            for item in array {
                
                let totalItems = (item as AnyObject).count
                let arrayOfItem = item as? NSArray
                if let totalItems = totalItems { //optional binding to unwrap optional variable
                 for grade in 1..<totalItems {
                    if j == grade {
                        let x = arrayOfItem![grade] as! Double
                        let roundvalue = round(1000*x)
                        let orgValue = roundvalue/1000
                        thisPointArray.append(orgValue)
                        break
                    }
                 }
                }
                let value = arrayOfItem![0] as? NSArray
                if  j == 1 {
                    titleArray.append(value![1] as! String)
                }
            }
        }
        setChart(dataPoints: titleArray,values: thisPointArray)
    }

func setChart(dataPoints: [String], values: [Double]) {
    graphPresentingView.noDataText = "You need to provide data for the chart."
    graphPresentingView.leftAxis.axisMinimum = 0.00
    graphPresentingView.rightAxis.axisMinimum = 0.00
    
    if selectedTerm != 0  {
        print("i am")
        
        graphPresentingView!.removeFromSuperview()
        graphPresentingView  =  HorizontalBarChartView(frame: CGRect(x: 0, y: 160, width:
            self.view.frame.size.width, height: self.view.frame.size.height - 250))
        self.view.addSubview(graphPresentingView!)
        
        setInitialChartAxis()
        graphPresentingView.isUserInteractionEnabled = false
        
        var dataEntriesValues: [BarChartDataEntry] = []

        for i in 0..<dataPoints.count {
            let dataEntryVal = BarChartDataEntry(x:Double(i),y:values[i])
            dataEntriesValues.append(dataEntryVal)
        }

        let chartDataSetValue = BarChartDataSet(values: dataEntriesValues, label: termArray[selectedTerm])

        let chartDataValue = BarChartData(dataSet: chartDataSetValue)
        chartDataSetValue.colors = colorsForGraph.colorArray[0]
        
        let formatter = CustomLabelsAxisValueFormatter()
        formatter.labels = dataPoints
        graphPresentingView.xAxis.valueFormatter = formatter
        graphPresentingView.xAxis.setLabelCount(dataPoints.count, force: false)
        graphPresentingView.xAxis.labelPosition = .bottom
        
        graphPresentingView.data = chartDataValue

        
    }

    else{

        print("iiiammmnnooottt")
        setInitialChartAxis()
        graphPresentingView.isUserInteractionEnabled = true
        
        var dataEntries = [[BarChartDataEntry]]()
        var chartDataSetArray:[BarChartDataSet] = []
        var index = 0
        var dataEntriesPos = 0

        for i in 1 ..< termArray.count{
            var singleBarchartDataEntry = [BarChartDataEntry]()
            for _ in 0..<dataPoints.count {
                let dataEntry = BarChartDataEntry(x:Double(i) , y: values[index])
                singleBarchartDataEntry.append(dataEntry)
                index = index+1
            }
            dataEntries.append(singleBarchartDataEntry)
        }

            dataEntriesPos = 0
           for pos in 1 ..< termArray.count {
            let chartDataSet = BarChartDataSet(values: dataEntries[dataEntriesPos], label: termArray[pos])
            chartDataSet.colors = colorsForGraph.colorArray[dataEntriesPos]
            chartDataSetArray.append(chartDataSet)
            dataEntriesPos += 1
           }


            let chartData = BarChartData(dataSets: chartDataSetArray)


        let groupSpace = 0.4
        let barSpace = 0.03
        let barWidth = 0.2

       chartData.barWidth = barWidth

        graphPresentingView.xAxis.axisMinimum = 0.0
        graphPresentingView.xAxis.axisMaximum = 0.0 + chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace) * Double(dataPoints.count)

        chartData.groupBars(fromX: 0.0, groupSpace: groupSpace, barSpace: barSpace)
        graphPresentingView.xAxis.granularity = graphPresentingView.xAxis.axisMaximum / Double(dataPoints.count)



        let formatter = CustomLabelsAxisValueFormatter()
        formatter.labels = dataPoints
        graphPresentingView.xAxis.valueFormatter = formatter
        graphPresentingView.xAxis.setLabelCount(dataPoints.count, force: false)
        graphPresentingView.xAxis.labelPosition = .bottom
        graphPresentingView.data = chartData
    }
   
    
    graphPresentingView.chartDescription?.text = ""
    graphPresentingView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
    graphPresentingView.setVisibleXRangeMinimum(0.00)
    
 }
    func setInitialChartAxis(){
        let leftAxis = graphPresentingView.leftAxis;
        leftAxis.drawAxisLineEnabled = true;
        leftAxis.drawGridLinesEnabled = true;
        leftAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        let rightAxis = graphPresentingView.rightAxis
        rightAxis.enabled = true;
        
        rightAxis.drawAxisLineEnabled = true;
        rightAxis.drawGridLinesEnabled = false;
        rightAxis.axisMinimum = 0.0; // this replaces startAtZero = YES
        
        graphPresentingView.fitBars = true;
   }
}
class CustomLabelsAxisValueFormatter : NSObject, IAxisValueFormatter {
    
    var labels: [String] = []
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        let count = self.labels.count
        
        guard let axis = axis, count > 0 else {
            
            return ""
        }
        
        let factor = axis.axisMaximum / Double(count)
        
        let index = Int((value / factor).rounded())
        
        if index >= 0 && index < count {
            
            return self.labels[index]
        }
        
        return ""
    }
}
