//
//  PhotoGallery.swift
//  parentEye
//
//  Created by Vivek on 03/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Photos

private let reuseIdentifier = "RPPhotoLibrary"

protocol attachmentimagelibraryProtocol:class  {
    
    func SelectedImagesWithArray(selectedImages:[Int:UIImage])
}

class  PhotoGallery:UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource ,UIImagePickerControllerDelegate{
    
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var photoGalleryCollectionView: UICollectionView!
    
    // By default make locating album false
    var assetCollection: PHAssetCollection!
    var photosAsset: PHFetchResult<PHAsset>!
    var assetThumbnailSize: CGSize!
    var selection = [Int: Bool]()
    var indexpathArray = [Int:Int]()
    var selectedImages = [Int:UIImage]()
    var selectedDataImages = [Int:NSData]()
    var photoAssets = [PHAsset]()
    var profilePic  = ""
    
    var count : Int = 0
    weak var delegate:attachmentimagelibraryProtocol?
    
    // =================== THIS BUTTON TAKES A PHOTO =====================================================
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*let fetchOptions = PHFetchOptions()
         
         let collection:PHFetchResult = PHAssetCollection.fetchAssetCollectionsWithType(.Album, subtype: .Any, options: fetchOptions)
         
         if let first_Obj:AnyObject = collection.firstObject{
         //found the album
         self.assetCollection = first_Obj as! PHAssetCollection
         }*/
        alterHeader()
        photoAssets = []
        
        let options = PHFetchOptions()
        options.sortDescriptors = [ NSSortDescriptor(key: "creationDate", ascending: false) ]
        options.predicate =  NSPredicate(format: "mediaType = %i", PHAssetMediaType.image.rawValue)
        let assetCollections = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.smartAlbumUserLibrary, options: nil)
        for i in 0 ..< assetCollections.count
        {
            if let assetCollection = assetCollections[i] as? PHAssetCollection
            {
                photosAsset   = PHAsset.fetchAssets(in: assetCollection, options: options)
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Get size of the collectionView cell for thumbnail image
        if let layout = photoGalleryCollectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            let cellSize = layout.itemSize
            
            self.assetThumbnailSize = CGSize(width:cellSize.width, height:cellSize.height)
        }
        
        //fetch the photos from collection
        //self.photosAsset = PHAsset.fetchAssetsInAssetCollection(self.assetCollection, options: nil)
        
        
        
        photoGalleryCollectionView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.chooseImage, withProfileImageUrl: profilePic)
        header.multiSwitchButton.isHidden = true
        header.leftButton.isHidden = true
    }
    
    
    // MARK: UICollectionViewDataSource
    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        var count: Int = 0
        
        if(self.photosAsset != nil){
            count = self.photosAsset.count
        }
        
        return count;
    }
    
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PhotoThumbnail = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! PhotoThumbnail
        
        //Modify the cell
        let asset: PHAsset = self.photosAsset[indexPath.item] 
        
        PHImageManager.default().requestImage(for: asset, targetSize: self.assetThumbnailSize, contentMode: .aspectFill, options: nil, resultHandler: {(result, info)in
            if let image = result {
                cell.setThumbnailImage(thumbNailImage: image)
                
            }
        })
        if selection[indexPath.row] == true
        {
            cell.backgroundColor = UIColor.orange
            
        }
        else{
            cell.backgroundColor = UIColor.clear
            
        }
        
        return cell
    }
    
    
     func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
        _ = false
        var rowvalue:Int
        let cell = collectionView.cellForItem(at: indexPath as IndexPath)
        rowvalue = indexPath.row
        
        if let selected = cell?.isSelected, selected  {
            
            if selection[rowvalue] == false{
                if count == 3 && selection[rowvalue] == false{
                    showAlert()
                    
                }
                else{
                    cell?.backgroundColor = UIColor.orange
                    selection[rowvalue] = true
                    count = count + 1
                }
                
            }
            else{
                if count == 3 && (selection[rowvalue] == false ||  selection[rowvalue] == nil){
                    
                    showAlert()
                    
                }
                    
                else {
                    
                    cell?.backgroundColor = UIColor.clear
                    selection[rowvalue] = false
                    count = count - 1
                }
                
            }
            
            collectionView.deselectItem(at: indexPath as IndexPath, animated: false)
            
            return false
            
        }
        
        if selection[rowvalue] == true
        {
            if count == 3 && selection[rowvalue] == false{
                showAlert()
                
                
            }
            else{
                cell?.backgroundColor = UIColor.clear
                selection[rowvalue] = false
                count = count - 1
            }
            
        }
        else{
            if count == 3 && (selection[rowvalue] == false || selection[rowvalue] == nil){
                showAlert()
                
            }
            else{
                cell?.backgroundColor = UIColor.orange
                selection[rowvalue] = true
                count = count + 1
            }
        }
        return true
        
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //Preform selection logic
        
    }
    
    
    
    @IBAction func showSenderList(_ sender: UIButton) {
    
        var position:Int = 0
        if photosAsset != nil {
            for i in 0 ..< photosAsset.count {
                if selection[i] == true{
                    indexpathArray[position] = i
                    position = position + 1
                }
            }
        }
        
        for i in 0 ..< indexpathArray.count {
            
            let asset: PHAsset = self.photosAsset[indexpathArray[i]!] 
            let option = PHImageRequestOptions()
            option.isSynchronous = true
            
            //option.resizeMode = PHImageRequestOptionsResizeMode.Exact
            //option.deliveryMode = PHImageRequestOptionsDeliveryMode.HighQualityFormat
            //option.version =  PHImageRequestOptionsVersion.Original
            
            
            PHImageManager.default().requestImage(for: asset, targetSize: self.assetThumbnailSize, contentMode: .aspectFill, options: option, resultHandler: {(result, info)in
                if let image = result {
                    self.selectedImages [i] = image
                }
            })
            
        }
        self.delegate?.SelectedImagesWithArray(selectedImages: selectedImages)
        self.dismiss(animated: false, completion: nil)

    }
    
    
    
    
    @IBAction func dismissMe(_ sender: UIButton) {
    
    self.dismiss(animated: false, completion: nil)
        
    }
    
    // UIImagePickerControllerDelegate Methods
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDelegate
    
    /*
     // Uncomment this method to specify if the specified item should be selected
     override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     */
    
    
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
     func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
     func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }
    
     func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        
        self.dismiss(animated: false, completion: nil)
        
    }
    

    private func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.maxAttachmentalert, titleOfAlert: ConstantsInUI.maxAttachment, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    
}
