//
//  TeacherTimetableActivityPopupVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 01/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit



class TeacherTimetableActivityPopupVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate
{

    @IBOutlet var doneButton: UIButton!
    @IBOutlet var noActivities: UILabel!
    @IBOutlet var totalNoofStudents: UILabel!
    
    @IBOutlet var noOfAbsentStudents: UILabel!
    
    @IBOutlet var TeacherActivityTable: UITableView!
    
    var user:userModel?
    var timeTableId = -1
    var schoolClassId = -1
    var baseModelArray = [BaseModelListing]()
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        initializeTableview()
        getTeacherActivityList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    private func initializeTableview()
    {
        TeacherActivityTable.register(UINib(nibName: "TeacherTimetableActivityPopupCell", bundle: nil), forCellReuseIdentifier: "TeacherTimetableActivityPopupCell")
        TeacherActivityTable.estimatedRowHeight = 76.0
        TeacherActivityTable.rowHeight          = UITableViewAutomaticDimension
        TeacherActivityTable.allowsSelection = false
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if baseModelArray.count > 0{
            
        noActivities.isHidden = true
        return baseModelArray.count
        }
        else{
            
        noActivities.isHidden = false
        return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
                let cell =  tableView.dequeueReusableCell(withIdentifier: "TeacherTimetableActivityPopupCell") as! TeacherTimetableActivityPopupCell
        
        
                let event = baseModelArray[indexPath.row]
                cell.dayLabel.text     = event.day
                cell.yearLabel.text    = event.year
                cell.monthLabel.text   = event.month
                cell.employeeUpcomingContent.text = event.title
                cell.backgroundColor = UIColor.clear
                return cell
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    
        }
    
    func getTeacherActivityList(){
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getTeachersActivityList(timeTableId: timeTableId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            if let tempResult = result["schoolClass"] as? [String:AnyObject]{
                
                if let noOfAbsent = tempResult["noOfAbsent"] as? String{// get the root key
                    self.noOfAbsentStudents.text = "No Of Absent Students: " + noOfAbsent
                }
                if let totalNoOfStudent = tempResult["totalNoOfStudent"] as? String{// get the root key
                    self.totalNoofStudents.text = "Total No Of Student: " + totalNoOfStudent
                }
                if let clsId = tempResult["schoolClassId"] as? Int{// get the root key
                    self.schoolClassId = clsId
                }
                
                if let actArray = tempResult["activities"] as? [AnyObject]{
                    if actArray.count > 0{
                        
                        self.noActivities.isHidden = true
                        self.noActivities.text = ""
                        
                           for eventEntry  in actArray {
                                if let obj =  BaseModelListing.init(fromEventDicto: eventEntry as! [String : AnyObject]){
                                    self.baseModelArray.append(obj)
                                }
                            }
                        print(self.baseModelArray.count)
                        print(self.baseModelArray)
                        self.TeacherActivityTable.reloadData()
                    }
                    else{
                        
                       self.noActivities.isHidden = false
                       self.noActivities.text = "No Activities"
                    }
                }
            
                
            }
            
            
            //self.classEmployeeListArray = parseEmployeeListForSubstitution.parseThisDictionaryForEmployeePopup(result)
            //self.alterTeacherTableview.reloadData()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
    }
    
    @IBAction func dismissMe(_ sender: Any) {
    
        self.dismiss(animated: false, completion: nil)
    }
    
}
