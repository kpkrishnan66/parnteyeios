//
//  TimeTableVC.swift
//  parentEye
//
//  Created by Emvigo on 8/3/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import SpreadsheetView
import Firebase

class TimeTableVC: UIViewController,UITextFieldDelegate,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,UINavigationControllerDelegate,popOverDelegate,SelectedSchoolpopOverDelegate,SpreadsheetViewDataSource, SpreadsheetViewDelegate{
    
    @IBOutlet var teacherTimetableWidth: NSLayoutConstraint!
    @IBOutlet var txtTeacherListHeight: NSLayoutConstraint!
    @IBOutlet var classTeacherProfilePic: UIImageView!
    @IBOutlet var className: UILabel!
    @IBOutlet var nameOfClassTeacher: UILabel!
    @IBOutlet var classTimetableButton: CustomButton!
    @IBOutlet var teacherTimetableButton: CustomButton!
    @IBOutlet var profileIMageviewHeightConstant: NSLayoutConstraint!
    @IBOutlet var classTeacherProfileViewHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var txtTeachersList: customTextfield!
    @IBOutlet weak var btnTeachersList: UIButton!
    @IBOutlet weak var btnClassTimeTable: UIButton!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var noTimetableButton: UIButton!
    @IBOutlet var spreadSheetView: SpreadsheetView!
    
    
    var user:userModel?
    var listOfEmployee = [EmployeeTimetableAlternateSameClass]()
    //var timeTableList = [String:[TimetableModelClass]]()
    var employeeId = ""
    var weeks = [String]()
    lazy var filteredTimetableListingDictionary = [String:[TimetableModelClass]]()
    lazy var timeTableListingDictionary         = [String:[TimetableModelClass]]()
    var emptyTimetable = false
    var columnCount = 0
    var weeksRowcount = 0
    var profilePic = ""
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var timeTableId = -1
    var classId     = -1
    var schoolId:Int = 0
    var classTimetableButtonSelectedclassId = -1
    var ClassesList:[EmployeeClassModel] = []
    var teacherTimetableButtonSelected = true
    var classTimetableButtonSelected = false
    var fromTeacher = false
    let picker = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        if user?.currentTypeOfUser == .CorporateManager{
           schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        setupRoles()
        setupUI()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUI(){
        noTimetableButton.isHidden = true
        txtTeachersList.delegate = self
        spreadSheetView.dataSource = self
        spreadSheetView.delegate = self
        spreadSheetView.register(SpreadSheetCell.self, forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        spreadSheetView.register(UINib(nibName: String(describing: SpreadSheetCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        spreadSheetView.backgroundColor = .white
        
        // Do any additional setup after loading the view.
        classTeacherProfileViewHeightConstant.constant = 0
        profileIMageviewHeightConstant.constant = 0
    }
    
    func textFieldDidEndEditing(_ textField: UITextField){
        if teacherTimetableButtonSelected == true {
            txtTeachersList.endEditing(true)
            getTimetableFromAPIForTeacherTimetable()
            txtTeachersList.endEditing(false)
        }
        else{
            txtTeachersList.endEditing(true)
            getTimetableFromAPIForClassTimetable()
            txtTeachersList.endEditing(false)
            
        }
        
    }
    
    
    
    @IBAction func classTimetableButtonTapped(_ sender: UIButton) {
        //view.endEditing(true)
        //self.timeTableListingDictionary.removeAll()
        emptyTimetable = true
        teacherTimetableButton.backgroundColor = UIColor.white
        teacherTimetableButton.setTitleColor(UIColor.black, for: .normal)
        teacherTimetableButton.layer.borderWidth = 1
        teacherTimetableButton.layer.borderColor = UIColor.black.cgColor
        
        classTimetableButton.backgroundColor = colur
        classTimetableButton.setTitleColor(UIColor.white, for: .normal)
        classTimetableButton.layer.borderWidth = 0
        classTimetableButton.layer.borderColor = UIColor.white.cgColor
        teacherTimetableButtonSelected = false
        classTimetableButtonSelected = true
        txtTeacherListHeight.constant = 30
        ClassesList = (user?.Classes)!
        configViewWithDataForClassTimetableSelected()
    }
    
    @IBAction func teacherTimetableButtonTapped(_ sender: UIButton) {
        //view.endEditing(true)
        //self.timeTableListingDictionary.removeAll()
        emptyTimetable = true
        classTimetableButton.backgroundColor = UIColor.white
        classTimetableButton.setTitleColor(UIColor.black, for: .normal)
        classTimetableButton.layer.borderWidth = 1
        classTimetableButton.layer.borderColor = UIColor.black.cgColor
        
        teacherTimetableButton.backgroundColor = colur
        teacherTimetableButton.setTitleColor(UIColor.white, for: .normal)
        teacherTimetableButton.layer.borderWidth = 0
        teacherTimetableButton.layer.borderColor = UIColor.white.cgColor
        teacherTimetableButtonSelected = true
        classTimetableButtonSelected = false
        setupRoles()
        
    }
    
    func setupRoles(){
        
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{

              if webServiceRolesGroupForDifferentActivities.timeTableshiftingOptionGroup.contains((user?.employeeType)!)||user?.currentTypeOfUser == .CorporateManager{
                 employeeId = String(user!.employeeId )
                 fromTeacher = false
                 getEmployeeListFromAPI()
            }
            if webServiceRolesGroupForDifferentActivities.timeTableActivitiesOptionGroup.contains((user?.employeeType)!){
                fromTeacher = false
                employeeId = String(user!.employeeId)
                getEmployeeListFromAPI()
                
                
            }
        if webServiceRolesGroupForDifferentActivities.timeTableClassTimetableOptionGroup.contains((user?.employeeType)!){
            
                teacherTimetableButton.isHidden = true
                classTimetableButtonSelected = true
                teacherTimetableButtonSelected = false
                txtTeacherListHeight.constant = 30
                teacherTimetableWidth.constant = 0
                classTimetableButton.backgroundColor = colur
                classTimetableButton.setTitleColor(UIColor.white, for: .normal)
                ClassesList = (user?.Classes)!
                configViewWithDataForClassTimetableSelected()

        }
            
        }
        
        
    }
    
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.timeTableScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        header.multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popViewController(animated: false)
        }
        
        
        
        
    }
    
    //MARK:- POP over delegates
    
    // Returns .none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popOverDidSelectRow(rowSelected: Int) {
        
    }
  // MARK: UIEvents
    @IBAction func teacherTimeTableButtonTapped(_ sender: Any) {
        btnClassTimeTable.backgroundColor = UIColor.clear
    }
    
    @IBAction func classTimeTableTapped(_ sender: Any) {
        btnTeachersList.backgroundColor = UIColor.clear
    }
    
}

// MARK: UIEvents Handlers
extension TimeTableVC{
    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        loadPickerView()
    }
    func loadPickerView() {
        //view.endEditing(false)
        picker.delegate = self
        picker.dataSource = self
        picker.selectRow(0, inComponent: 0, animated:false)
        picker.backgroundColor = UIColor.clear
        txtTeachersList.inputView = picker
    }
    
    func getEmployeeListFromAPI(){
        var schoolID                     = ""
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        if user?.currentTypeOfUser == .employee {
        schoolID = String(user!.schoolId)
        }
        if user?.currentTypeOfUser == .CorporateManager {
         schoolID = String(schoolId)
        }
        
        WebServiceApi.getEmployeeList(schoolId: schoolID, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let tempResult = result["employeeList"] {
                debugPrint(tempResult)
                weakSelf!.listOfEmployee = parseEmployeeListForSubstitution.parseThisDictionaryForEmployeePopup(dictionary: result)
                weakSelf!.configViewWithDataForTeacherTimetableSelected()
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
    }
    
    func configViewWithDataForTeacherTimetableSelected(){
        if listOfEmployee.count > 0{
            
            
            if user!.currentTypeOfUser == .employee || user!.currentTypeOfUser == .CorporateManager{
                var currentUser = [EmployeeTimetableAlternateSameClass]()
                for index in 0 ..< listOfEmployee.count{
                    if String(listOfEmployee[index].employeeId) == employeeId{
                        currentUser.append(listOfEmployee[index])
                        listOfEmployee.remove(at: index)
                        listOfEmployee.insert(currentUser[0], at: 0)
                        break
                    }
                }
                txtTeachersList.text = listOfEmployee[0].employeeName
                if webServiceRolesGroupForDifferentActivities.timeTableshiftingOptionGroup.contains((user!.employeeType))||user!.currentTypeOfUser == .CorporateManager{
                    
                  }

                if webServiceRolesGroupForDifferentActivities.timeTableActivitiesOptionGroup.contains((user!.employeeType)){
                    
                    
                    
                }
            }
            
            loadPickerView()
            getTimetableFromAPIForTeacherTimetable()
         }
        }
    
    func configViewWithDataForClassTimetableSelected(){
        if ClassesList.count > 0{
            txtTeachersList.text = ClassesList[0].className
            classTimetableButtonSelectedclassId = ClassesList[0].classId
            loadPickerView()
            getTimetableFromAPIForClassTimetable()
        }
    }
    
    func getTimetableFromAPIForTeacherTimetable(){
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        emptyTimetable = false
        self.noTimetableButton.isHidden = true
        WebServiceApi.getTimeTableByEmployeeId(employeeId: employeeId,fromTeacher: fromTeacher, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
           
            self.classTeacherProfileViewHeightConstant.constant = 0
            self.profileIMageviewHeightConstant.constant = 0
            self.timeTableListingDictionary.removeAll()
            let resultTuple =  parseTimetableForEmployee.parseFromDictionary(dictionary: result)
            if let _ =  resultTuple.1{
                self.noTimetableButton.isHidden = false
                weakSelf!.configTimeTable()
            }else {
                weakSelf!.addEntriesToDictionary(resultTuple.0)
            }
            
            
            if let tempResult = result["timeTable"] as? [String:Any]{// get the root key
                
                self.weeks.removeAll()
                for (key,dateSeparatedDiary) in tempResult{
                    self.weeks.append(key)
                    if let diaryArray = dateSeparatedDiary as? [[String:Any]]{
                        self.columnCount = diaryArray.count
                        
                    }
                }
                self.weeksRowcount = self.weeks.count
                self.sortDate()
                weakSelf!.configTimeTable()
            }else{
                self.noTimetableButton.isHidden = false
                self.emptyTimetable = true
                weakSelf!.configTimeTable()
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
        
    }
    
    private func addEntriesToDictionary(_ dictionary:[String:[TimetableModelClass]]){
        filteredTimetableListingDictionary.addEntriesFromDictionary(otherDictionary: dictionary)
        timeTableListingDictionary = filteredTimetableListingDictionary
    }
    
    func getTimetableFromAPIForClassTimetable(){
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        emptyTimetable = false
        self.noTimetableButton.isHidden = true
        self.weeksRowcount = 0
        self.columnCount = 0
        
        WebServiceApi.getTimeTableByClassId(classId: classTimetableButtonSelectedclassId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.classTeacherProfileViewHeightConstant.constant = 0.0
            self.profileIMageviewHeightConstant.constant = 0.0

           if let a = result["schoolClass"] as? NSDictionary{
                
               
                
                if let classinchrg = a["classInCharge"] as? [String : AnyObject]{// get the root key
                    
                     if let name = classinchrg["name"] as? String{
                        self.nameOfClassTeacher.text = name
                        
                     }
                   
                    if let prfpic = classinchrg["profilePicUrl"] as? String{
                        
                     let profileImage = self.classTeacherProfilePic
                       if prfpic == "" {
                        self.classTeacherProfilePic.image = UIImage(named: "avatar")
                        }
                    else{
                        ImageAPi.fetchImageForUrl(urlString: prfpic, oneSuccess: { (image) in
                            if profileImage != nil {
                                
                                profileImage!.image = image
                                
                            }
                        }) { (error) in
                            
                        }
                     }
                    }
                   
                }
            }
            
            if let timetableResult = result["schoolClass"] as? NSDictionary{
                
               
                    
                self.timeTableListingDictionary.removeAll()
                let resultTuple =  parseTimetableForEmployee.parseFromDictionary(dictionary: timetableResult as! [String : AnyObject])
                    if let _ =  resultTuple.1{
                        self.noTimetableButton.isHidden = false
                        
                        weakSelf!.configTimeTable()
                    }else {
                        weakSelf!.addEntriesToDictionary(resultTuple.0)
                    }
                
                self.weeks.removeAll()
                if let tempResult = timetableResult["timeTable"] as? NSDictionary{// get the root key
                 for (k,dateSeparatedDiar) in tempResult{
                    self.weeks.append(k as! String)
                    if let diaryArra = dateSeparatedDiar as? [[String:AnyObject]]{
                        print(diaryArra.count)
                        self.columnCount = diaryArra.count
                        
                    }
                 }
                
                    
                self.weeksRowcount = self.weeks.count
                self.sortDate()
                weakSelf!.configTimeTable()
                }else{
                    self.noTimetableButton.isHidden = false
                    self.emptyTimetable = true
                    weakSelf!.configTimeTable()
                }
                
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
            }
        
    }
    
    private func sortDate(){
        var tempArray = [String]()
        var days = [Int]()
    
            for day in weeks {
    
              let formatterWeekday = DateFormatter()
    
              formatterWeekday.dateFormat = "e"
    
              let weekday = formatterWeekday.date(from: day)
    
              let weekdayString = formatterWeekday.string(from: weekday!)
    
              var dayInt = Int(weekdayString)
              dayInt = dayInt! - 1
              days.append(dayInt!)
    
            }
    
        days.sort()
    
    let dateFormatter = DateFormatter()
    tempArray.removeAll()
    weeks.removeAll()
    for dayIndex in days
    {
    tempArray.append(dateFormatter.weekdaySymbols[dayIndex])
        
    }
    let capitalizedArray = tempArray.map { $0.uppercased()}
    weeks = capitalizedArray
        print(weeks)
}

    func configTimeTable(){
        spreadSheetView.reloadData()
    }
    
    
    private func popupTeacherTimetable(){
    
    FireBaseKeys.FirebaseKeysRuleFour(value: "timeTable_substitution")
    let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
    let msRecipVC = storyboard.instantiateViewController(withIdentifier: "TeacherTimetablePopupVC") as! TeacherTimetablePopupVC ;
    msRecipVC.classId = classId
    msRecipVC.timeTableId = timeTableId
    prepareOverlayVC(overlayVC: msRecipVC)
    self.present(msRecipVC, animated: false, completion: nil)
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    private func popupTeacherTimetableForActivities(){
        FireBaseKeys.FirebaseKeysRuleFour(value: "timeTable_clickforActivity")
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "TeacherTimetableActivityPopupVC") as! TeacherTimetableActivityPopupVC ;
        msRecipVC.timeTableId = timeTableId
        prepareOverlayVC(overlayVC: msRecipVC)
        self.present(msRecipVC, animated: false, completion: nil)
    }
    
}
extension TimeTableVC: UIPickerViewDataSource {
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if teacherTimetableButtonSelected == true {
         if listOfEmployee.count > 0{
          return listOfEmployee.count
         }
        }
        else {
            if ClassesList.count > 0{
                return ClassesList.count
            }
        }
        
        
        return 0
    }
}

extension TimeTableVC: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if teacherTimetableButtonSelected == true {
            
          if listOfEmployee.count > 0{
            
            return (listOfEmployee[row].employeeName)
          }
        }
        else {
            if ClassesList.count > 0{
                
                return ClassesList[row].className
            }
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if teacherTimetableButtonSelected == true {
            if listOfEmployee.count > 0{
                txtTeachersList.text = listOfEmployee[row].employeeName
                employeeId = String(listOfEmployee[row].employeeId)
            }
       }
        else{
               if ClassesList.count > 0{
                txtTeachersList.text = ClassesList[row].className
                classTimetableButtonSelectedclassId = ClassesList[row].classId
                }
            }
        }
    
    }

// MARK: spreadsheetViewDataSource

extension TimeTableVC{
    
    enum Colors {
        static let border = UIColor.lightGray
        static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if emptyTimetable == true{
            return 0
        }
        return columnCount+1
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        if emptyTimetable == true{
            return 0
        }
        return weeksRowcount+1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 100
        }
        return 100
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return 44
        }
        return 44
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if timeTableListingDictionary.count > 0 {
            return 1
        }
        return 0
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        if timeTableListingDictionary.count > 0 {
            return 1
        }
        return 0
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SpreadSheetCell.self), for: indexPath) as! SpreadSheetCell
        cell.label.textColor = UIColor.black
        if indexPath.column == 0 && indexPath.row == 0 {
            cell.label.text = ""
            cell.label.backgroundColor = Colors.headerBackground
        }
        else if indexPath.column == 0 && indexPath.row > 0 {
            cell.label.backgroundColor = Colors.headerBackground
            if weeks.count > 0{
                let strNumber: NSString = weeks[indexPath.row - 1] as NSString
                cell.label.text = (strNumber.substring(with: NSRange(location: 0, length:3)))
                
            }else{
                cell.label.text = ""
            }
        }
        else if indexPath.column > 0 && indexPath.row == 0 {
            cell.label.backgroundColor = Colors.headerBackground
            cell.label.text = String(indexPath.column)
        }
        else{
            if timeTableListingDictionary.count > 0 {
                cell.label.backgroundColor = UIColor.white
                var textLabel = timeTableListingDictionary[weeks[indexPath.row-1]]![indexPath.column-1].className
                if textLabel.isEmpty && teacherTimetableButtonSelected{
                    cell.label.textColor = colur
                    cell.label.text = "NA"
                }else{
                    if teacherTimetableButtonSelected{
                       textLabel += " "+timeTableListingDictionary[weeks[indexPath.row-1]]![indexPath.column-1].subjectNickName
                    }else{
                        textLabel = timeTableListingDictionary[weeks[indexPath.row-1]]![indexPath.column-1].subjectNickName
                    }
                    cell.label.text = textLabel
                }
            }
        }
        
        return cell
        
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
        
        if indexPath.row > 0 && indexPath.column > 0 && teacherTimetableButtonSelected {
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            if webServiceRolesGroupForDifferentActivities.timeTableshiftingOptionGroup.contains((user?.employeeType)!)||user?.currentTypeOfUser == .CorporateManager{
                if timeTableListingDictionary.count > 0  {
                    let id = timeTableListingDictionary[weeks[indexPath.row - 1]]![indexPath.column - 1].id
                    let stringClassid = timeTableListingDictionary[weeks[indexPath.row - 1]]![indexPath.column - 1].classId
                    if id > 0 && stringClassid != "0"{
                        timeTableId = id
                        classId = Int(stringClassid)!
                        popupTeacherTimetable()
                    }
                    
                }
                
            }
            if webServiceRolesGroupForDifferentActivities.timeTableActivitiesOptionGroup.contains((user?.employeeType)!){
                if timeTableListingDictionary.count > 0 {
                    let id = timeTableListingDictionary[weeks[indexPath.row - 1]]![indexPath.column - 1].id
                    if id > 0{
                        timeTableId = id
                        popupTeacherTimetableForActivities()
                    }
                }
            }
         }
        }
        
    }
    
    
}
