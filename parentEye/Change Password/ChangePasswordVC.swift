//
//  ChangePasswordVC.swift
//  parentEye
//
//  Created by Martin Jacob on 11/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController,CommonHeaderProtocol,UITextFieldDelegate {

    @IBOutlet weak var existingPaswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reEnterPasswordTextField: UITextField!
    
    @IBOutlet var passwordIconImage: UIImageView!
    
    @IBOutlet var passwordiconView: UIView!
    @IBOutlet weak var header: CommonHeader!
    var isPassword:Bool = false
    var isForgotPassword:Bool = false
    var readOTP:String = ""
    var mobNo:String = ""
    
    override func viewDidLoad() {
        if(isPassword){
          existingPaswordTextField.isHidden = true
          passwordiconView.isHidden = true
          passwordIconImage.isHidden = true
           header.rightLabel.isUserInteractionEnabled = true
           let tap = UITapGestureRecognizer(target: self,  action: #selector(skipButtonTapped))
            header.rightLabel.addGestureRecognizer(tap)

        }
        if(isForgotPassword)
        {
            existingPaswordTextField.isHidden = true
            passwordiconView.isHidden = true
            passwordIconImage.isHidden = true
            
        }
        setUpHeader()
        setUpTextFields()
        
    }
    
    
    private func setUpTextFields(){
        existingPaswordTextField.placeholder = ConstantsInUI.existingPasswordPlaceHolder
        newPasswordTextField.placeholder     = ConstantsInUI.newPasswordPlaceHolder
        reEnterPasswordTextField.placeholder = ConstantsInUI.reEnterNewPasswordPlaceHolder
        
        existingPaswordTextField.delegate = self
        newPasswordTextField.delegate     = self
        reEnterPasswordTextField.delegate = self
        
//        addBorderToTextField(existingPaswordTextField)
//        addBorderToTextField(newPasswordTextField)
//        addBorderToTextField(reEnterPasswordTextField)
        
        existingPaswordTextField.inputAccessoryView = UIView()
        newPasswordTextField.inputAccessoryView     = UIView()
        reEnterPasswordTextField.inputAccessoryView = UIView()
    }
    private func setUpHeader(){
        if(isPassword)
        {
            header.isPassword = true
        }
        header.SetupForChangePassword()
        header.delegate = self
    }
    
    
    @IBAction func changePassword(_ sender: UIButton) {
    
      let result = validateFields()
        
        if result.0 {
            weak var weakSelf = self
            if let user = DBController().getUserFromDB(){
                hudControllerClass.showNormalHudToViewController(viewController: self)
                WebServiceApi.changePasswordForUserName(userName: user.mobileNo ,
                                                          havingOldPassword: existingPaswordTextField.text!, toNewPassword: newPasswordTextField.text!,
                                                          confirmedNewPassword: reEnterPasswordTextField.text!,
                                                          isPassword: isPassword,
                                                          isForgotPassword: isForgotPassword,
                                                          otp:readOTP,
                    
                                                          onSuccess: { (result) in
                                                            
                                                            hudControllerClass.hideHudInViewController(viewController: self)
                                                            
                                                            let rt = RootClass(fromDictionary: result as NSDictionary)
                                                            if rt?.failureReason == nil {
                                                             AlertController.showToastAlertWithMessage(message: ConstantsInUI.passwordChangedSuccesFully, stateOfMessage: .success)
                                                           // weakSelf!.dismissViewControllerAnimated(true, completion: nil)
                                                                let app = UIApplication.shared.delegate as! AppDelegate
                                                                app.maketabBarRoot()

                                                            }else {
                                                            AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .failure)
                                                            
                                                            }
                                                           
                                                         
                    },
                                                          
                                                          
                                                          onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                            
                    },
                                                          duringProgress: { (progress) in
                                                            
                })
            }
            
            
        }else {
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
    }
    /**
     Function validate the text fields
     
     - returns: a tuple that has the result and the error string
     */
    private func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
        
        
        if existingPaswordTextField.isHidden == false && existingPaswordTextField.text == "" {
            result   = false
            ErrorMsg = ConstantsInUI.existingPasswordWordFieldEmpty
            }
        else if (existingPaswordTextField.isHidden == false && (existingPaswordTextField.text?.characters.count)!<6) {
            result   = false
            ErrorMsg = ConstantsInUI.passwordLenghtNotCorrect
            }
        else if (newPasswordTextField.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.newPasswordFieldEmpty
        } else if (reEnterPasswordTextField.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.passwordReentryFieldEmpty
        }
            
        else if ((newPasswordTextField.text?.characters.count)!<6) {
            result   = false
            ErrorMsg = ConstantsInUI.passwordLenghtNotCorrect
        }
        else if ((reEnterPasswordTextField.text?.characters.count)!<6) {
            result   = false
            ErrorMsg = ConstantsInUI.passwordLenghtNotCorrect
        } else if reEnterPasswordTextField.text != newPasswordTextField.text{
            result = false
            ErrorMsg = ConstantsInUI.passwordsDoesNotMatch
        }
        return (result,ErrorMsg)
    }
    
    //Header delegates 
    
    func showHamburgerMenuOrPerformOther() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSwitchSibling(fromButton: UIButton) {
        
    }
    //Web service
    
    private func makeWebApiCallToChangePassword(){
    
        
    
    }
    
    //MARK:- text field
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    private func addBorderToTextField(textField:UITextField){
        textField.layer.backgroundColor = UIColor.white.cgColor
        textField.layer.borderColor = UIColor.gray.cgColor
        textField.layer.borderWidth = 0.0
        textField.layer.cornerRadius = 5
        textField.layer.masksToBounds = false
        textField.layer.shadowRadius = 2.0
        textField.layer.shadowColor = UIColor.black.cgColor
        textField.layer.shadowOffset = CGSize(width:1.0, height:1.0)
        textField.layer.shadowOpacity = 1.0
        textField.layer.shadowRadius = 1.0
    }
    
    @objc func skipButtonTapped(sender:UITapGestureRecognizer){
        weak var weakSelf = self
        
    let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.skipchangePassword+"\(mobNo)", titleOfAlert: "", oktitle:ConstantsInUI.ok , cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
        
    }) { (action) in
        //weakSelf?.navigationController?.popViewControllerAnimated(true)
        if let user = DBController().getUserFromDB(){
            
        WebServiceApi.changePasswordWhenSkipped(mobNo: self.mobNo,userName: user.mobileNo ,
                                                
                                                onSuccess: { (result) in
                                                    
                                                    hudControllerClass.hideHudInViewController(viewController: self)
                                                   /* if let changePasswordResponse = result["response"] as? NSDictionary{
                                                        if let message = changePasswordResponse["message"] as? String{
                                                        
                                                        AlertController.showToastAlertWithMessage(ConstantsInUI.passwordChangedSuccesFully, stateOfMessage: .success)
                                                        
                                                        let app = UIApplication.sharedApplication().delegate as! AppDelegate
                                                        app.maketabBarRoot()

                                                    }
                                                    }
                                                    if let PasswordResponse = result["error"] as? NSDictionary{
                                                        if let errorMessage = PasswordResponse["errorMessage"] as? String{
                                                            
                                                            AlertController.showToastAlertWithMessage(errorMessage, stateOfMessage: .success)
                                                        }}   */
                                                    
                                                    let app = UIApplication.shared.delegate as! AppDelegate
                                                    app.maketabBarRoot()
  
                                               },
                                                onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                    
                                              },
                                                duringProgress: { (progress) in
                                                    
                                            })
        }
        
}
    self.present(alert, animated: false, completion: nil)

    }


}
