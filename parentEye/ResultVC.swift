//
//  ResultVC.swift
//  parentEye
//
//  Created by Emvigo on 7/13/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import SpreadsheetView




class ResultVC: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    
    @IBOutlet var txtExams: UITextField!
    @IBOutlet var btnNutshell: UIButton!
    @IBOutlet var lblNumberOfStudents: UILabel!
    @IBOutlet var lblTotalStudents: UILabel!
    @IBOutlet var lblProgress: UILabel!
    @IBOutlet var btnGraph: UIButton!
    @IBOutlet var dataGridViewHeight: NSLayoutConstraint!
    @IBOutlet var ResultViewHeight: NSLayoutConstraint!
    @IBOutlet var lblNoResult: UILabel!
    @IBOutlet var resultProgresscardView: SpreadsheetView!
    @IBOutlet var lblProgressHeight: NSLayoutConstraint!
    @IBOutlet var btnNutshellHeight: NSLayoutConstraint!
    @IBOutlet var btnGraphHeight: NSLayoutConstraint!
    @IBOutlet var txtExamsHeight: NSLayoutConstraint!
    @IBOutlet var lblNumberOfStudentsHeight: NSLayoutConstraint!
    @IBOutlet var lblNoResultHeight: NSLayoutConstraint!
    @IBOutlet var lblTotalStudentsHeight: NSLayoutConstraint!
    
    
    var user:userModel?
    var terms = [String]()
    var nutShellIsOn:Bool?
    var studentId = 0
    var passStudentId = ""
    var gotoDetailedStudentProfileGraph = false
    var examPicker = UIPickerView()
    var moveToGraph = false
    var progressCardHeaderArray = [String]()
    var rowCount = 0
    var mainSubCount = 0
    var subjectList = [String:AnyObject]()
    var mark = [String]()
    let examMarkList = ExamMarklist()
    var MarkList = [String] ()
    var mainSubCountArray = [Int]()
    var displayProgressCardArray = Array<Array<String>>()
    var abc = Array<Array<Array<String>>>()
    var displayPCArrayRowPosition = 0
    var displayPCArrayColPosition = 0
    let studentAbsentcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let studentNotAssignedColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    var PCinner = false
    var nutshellResult = [String:AnyObject]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hidePcElements()
        getUserDataFromDB()
        setupUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        self.hidePcElements()
        setinitialIndexPathArrayPosition()
        lblProgress.text = "Progress Card"
        btnNutshell.setTitle("Nutshell", for: UIControlState())
        getProgressReport()
        
    }
    
    func setupUI(){
        resultProgresscardView.dataSource = self
        resultProgresscardView.delegate = self
        resultProgresscardView.register(SpreadSheetCell.self, forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        resultProgresscardView.register(UINib(nibName: String(describing: SpreadSheetCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        resultProgresscardView.backgroundColor = .white
    }
    func selectedStudentid(_ studentid:Int){
        studentId = studentid
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

// MARK: UIEvents

extension ResultVC{
    
    @IBAction func graphButtonTapped(_ sender: AnyObject) {
        /*let defaults = NSUserDefaults.standardUserDefaults()
         moveToGraph = true
         defaults.setBool(moveToGraph, forKey: "navigationToGraph")*/
        
        self.performSegue(withIdentifier: "segueToGraphPage", sender: nil)
        
    }
    
    @IBAction func nutshellButtonTapped(_ sender: UIButton) {
        if nutShellIsOn == false{
            nutShellIsOn = true
            sender.setTitle("Progress Card", for: UIControlState())
            lblTotalStudents.isHidden = false
            lblNumberOfStudents.isHidden = false
            txtExams.isHidden = false
            lblProgress.text = "Nutshell"
            txtExamsHeight.constant = 30
            lblNumberOfStudentsHeight.constant = 9
            lblTotalStudentsHeight.constant = 18
            
            getNutShellReport()
        }
        else{
            nutShellIsOn = false
            sender.setTitle("Nutshell", for: UIControlState())
            lblTotalStudents.isHidden = true
            lblNumberOfStudents.isHidden = true
            txtExamsHeight.constant = 0
            lblNumberOfStudentsHeight.constant = 0
            lblTotalStudentsHeight.constant = 0
            
            lblProgress.text = "Progress Card"
            getProgressReport()
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToGraphPage"{
            
            gotoDetailedStudentProfileGraph = true
            let svc = segue.destination as? GraphVC
            svc?.studentId = studentId
            
        }
        
    }
}
// MARK: UI Events Functions
extension ResultVC{
    
    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    func getProgressReport(){
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        if user?.currentTypeOfUser == .guardian{
            passStudentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        else{
            let defaults = UserDefaults.standard
            
            let  studentd = defaults.integer(forKey: "selectedStudentId")
            passStudentId = String(studentd)
        }
        WebServiceApi.getProgressReport(studentId: passStudentId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.nutShellIsOn = false
            self.rowCount = 0
            self.mainSubCount = 0
            self.displayPCArrayRowPosition = 0
            self.displayPCArrayColPosition = 0
            self.PCinner = false
            print(result)
            var examSubjectsList = [ExamMarklist]()
            if let tempResult = result["progressCard"] as? [String:AnyObject] {
                print(tempResult)
                if let header = tempResult["headers"] as? NSArray{
                    
                    self.progressCardHeaderArray.removeAll()
                    for key in header{
                        self.progressCardHeaderArray.append(key as! String)
                        
                    }
                }
                
                self.subjectList.removeAll()
                var examSubjectsList = [examSubject]()
                if let mkList = tempResult["markList"] as? [String:AnyObject]{
                    for (subject,_) in mkList{
                        let examSubjectObject = examSubject()
                        examSubjectObject.setExamSubjctName(SubjectName: subject)
                        if let subList = mkList[subject] as? [String:AnyObject] {
                            
                            if let position = subList["position"] as? Int{
                                self.displayPCArrayColPosition = 0
                                self.displayProgressCardArray[position][self.displayPCArrayColPosition]   = subject
                                self.abc[0][position][self.displayPCArrayColPosition]   = subject
                                self.displayPCArrayRowPosition = position
                                
                            }
                            if let val = subList["value"] as? [String:AnyObject] {
                                
                                
                                examSubjectObject.setBranched(branchedornot: true)
                                
                                for _ in 1..<self.progressCardHeaderArray.count {
                                    
                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                    self.displayProgressCardArray[self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = "Bold"
                                }
                                
                                self.rowCount = self.rowCount + 1
                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                
                                self.mainSubCount = self.mainSubCount + 1
                                var examSubjects = [examSubject]()
                                examSubjects = self.getInnerSubjectForProgressCard(val)
                                examSubjectObject.setExamSubjectList(subjectList: examSubjects)
                            }
                            //}
                            //if let subList = mkList[subject] as? NSArray {
                            if let subList = subList["value"] as? NSArray {
                                
                                examSubjectObject.setBranched(branchedornot: false)
                                self.displayPCArrayColPosition = 0
                                self.rowCount = self.rowCount + 1
                                self.mainSubCount = self.mainSubCount + 1
                                self.mark.removeAll()
                                self.displayPCArrayColPosition = 0
                                
                                self.displayProgressCardArray[self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = subject
                                
                                for pos in subList{
                                    self.mark.append(pos as! String)
                                    examSubjectObject.examSubjectMarkList.append(pos as! String)
                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                    self.displayProgressCardArray[self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = pos as! String
                                    
                                }
                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                
                                
                            }
                        }
                        examSubjectsList.append(examSubjectObject)
                        self.mainSubCountArray.append(self.mainSubCount-1)
                        self.mainSubCount = 0
                        
                    }
                    self.examMarkList.setExamSubjectList(subjectList: examSubjectsList)
                    self.examMarkList.setexamNames(names: self.progressCardHeaderArray)
                }
                
                
                if self.PCinner == true {
                    self.hidePcElements()
                }
                else{
                    self.showPcElements()
                }
                
                if self.rowCount != 0 {
                    self.lblNoResult.isHidden = true
                    self.lblNoResultHeight.constant = 0
                    self.resultProgresscardView.reloadData()
                }
                else{
                    self.resultProgresscardView.isHidden = true
                    self.lblNoResult.isHidden = false
                    self.lblNoResultHeight.constant = 21
                    self.lblProgressHeight.constant = 0
                    self.btnNutshellHeight.constant = 0
                    self.btnGraphHeight.constant = 0
                    self.txtExamsHeight.constant = 0
                    self.lblNumberOfStudentsHeight.constant = 0
                    self.lblTotalStudentsHeight.constant = 0
                    
                }
            }
            else{
                self.lblNoResult.isHidden = false
                self.txtExams.isHidden = true
                self.lblTotalStudents.isHidden = true
                
            }
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            weakSelf!.lblNoResult.isHidden = false
            weakSelf!.txtExams.isHidden = true
            weakSelf!.btnGraph.isHidden = true
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }) { (progress) in
            
        }
    }
    
    
    func getInnerSubjectForProgressCard(_ subList:[String:AnyObject]) -> [examSubject] {
        PCinner = true
        var examSubjects = [examSubject]()
        var examInnerSubjects = [examSubject]()
        
        for(key,_) in subList{
            let examSubjectObject = examSubject()
            examSubjectObject.setExamSubjctName(SubjectName: key)
            if let subSubList = subList[key] as? [String:AnyObject] {
                if let position = subSubList["position"] as? Int{
                    self.displayPCArrayColPosition = 0
                    self.displayProgressCardArray[position][self.displayPCArrayColPosition]   = key
                    self.displayPCArrayRowPosition = position
                    
                }
                if let val = subSubList["value"] as? [String:AnyObject] {
                    
                    examSubjectObject.setBranched(branchedornot: true)
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    examInnerSubjects = getInnerSubjectForProgressCard(val)
                    examSubjectObject.setExamSubjectList(subjectList: examInnerSubjects)
                }
                
                if let subSubList = subSubList["value"] as? NSArray {
                    examSubjectObject.setBranched(branchedornot: false)
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    self.mark.removeAll()
                    displayPCArrayColPosition = 0
                    
                    displayProgressCardArray[displayPCArrayRowPosition][displayPCArrayColPosition]   = "      "+key
                    
                    for pos in subSubList{
                        self.mark.append(pos as! String)
                        examSubjectObject.examSubjectMarkList.append(pos as! String)
                        displayPCArrayColPosition = displayPCArrayColPosition + 1
                        displayProgressCardArray[displayPCArrayRowPosition][displayPCArrayColPosition]   = pos as! String
                        
                    }
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    
                }
            }
            examSubjects.append(examSubjectObject)
        }
        return examSubjects
    }
    
    func setinitialIndexPathArrayPosition(){
        
        displayProgressCardArray.removeAll()
        for _ in 0..<100 {
            displayProgressCardArray.append(Array(repeating: String(), count: 20))
        }
        
        for row in 0..<100{
            for col in 0..<20{
                displayProgressCardArray[row][col] = ""
            }
        }
    }
    
    func hidePcElements() {
        
        lblNumberOfStudents.isHidden = true
        lblTotalStudents.isHidden = true
        lblProgress.isHidden = true
        txtExams.isHidden = true
        btnGraph.isHidden = true
        lblNoResult.isHidden = true
        
        lblProgressHeight.constant = 0
        btnNutshellHeight.constant = 0
        btnGraphHeight.constant = 0
        txtExamsHeight.constant = 0
        lblNumberOfStudentsHeight.constant = 0
        lblTotalStudentsHeight.constant = 0
        lblNoResultHeight.constant = 0
        
    }
    func showPcElements() {
        
        self.lblNumberOfStudents.isHidden = true
        lblTotalStudents.isHidden = true
        lblProgress.isHidden = false
        txtExams.isHidden = true
        btnGraph.isHidden = false
        
        lblProgressHeight.constant = 20
        btnNutshellHeight.constant = 30
        btnGraphHeight.constant = 30
        txtExamsHeight.constant = 0
        lblNumberOfStudentsHeight.constant = 0
        lblTotalStudentsHeight.constant = 0
    }
    
    
    func getNutShellReport(){
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        if user?.currentTypeOfUser == .guardian{
            passStudentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        else{
            
            let defaults = UserDefaults.standard
            let  studentd = defaults.integer(forKey: "selectedStudentId")
            passStudentId = String(studentd)
        }
        
        WebServiceApi.getNutShellReport(studentId: passStudentId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.displayPCArrayRowPosition = 0
            self.displayPCArrayColPosition = 0
            self.terms.removeAll()
            if let tempResult = result["nutshell"] as? [String:AnyObject] {
                self.nutshellResult = tempResult
                if let header = tempResult["headers"] as? NSArray{
                    
                    self.progressCardHeaderArray.removeAll()
                    for key in header{
                        self.progressCardHeaderArray.append(key as! String)
                        
                    }
                }
                for (subject,_) in tempResult{
                    if subject != "headers" {
                        if (tempResult[subject] as? NSArray) != nil {
                            self.terms.append(subject)
                            
                        }
                    }
                    
                }
                print(self.displayProgressCardArray)
                if let totStudent = tempResult["totalStudents"] as? String{
                    self.lblNumberOfStudents.text = totStudent
                }
                self.addPickerView()
                
            }
            else{
                self.lblNoResult.isHidden = false
                self.txtExams.isHidden = true
                self.lblTotalStudents.isHidden = true
                
            }
            
        }, onFailure: { (error) in
            weakSelf!.lblNoResult.isHidden = false
            weakSelf!.txtExams.isHidden = true
            weakSelf!.btnGraph.isHidden = true
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }) { (progress) in
            
        }
        
    }
    
    
    func addPickerView(){
        let examPicker = UIPickerView()
        examPicker.delegate = self
        txtExams.inputView = examPicker
        if terms.count != 0 {
            self.txtExams.text = terms[0]
            examPicker.reloadAllComponents()
            configNutshell(terms[0])
        }
    }
    
    func configNutshell(_ key:String){
        
        self.displayPCArrayRowPosition = 0
        self.displayPCArrayColPosition = 0
        rowCount = 0
        if let subList = nutshellResult[key] as? NSArray {
            print(subList)
            for subSubList in subList {
                if let pos = subSubList as? [String] {
                    self.displayPCArrayColPosition = 0
                    for i in 0..<pos.count{
                        
                        self.displayProgressCardArray[self.displayPCArrayRowPosition][self.displayPCArrayColPosition] = pos[i]
                        self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                    }
                    self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                    rowCount = rowCount + 1
                }
                
            }
            
        }
        resultProgresscardView.reloadData()
    }
    
}

// MARK: PickerView Delegates
extension ResultVC{
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if nutShellIsOn == true{
            return terms.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if nutShellIsOn == true{
            return terms[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if nutShellIsOn == true{
            txtExams.text = terms[row]
            configNutshell(terms[row])
        }
    }
}
// MARK: UICollectionViewDataSource

extension ResultVC{

    enum Colors {
        static let border = UIColor.lightGray
        static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if progressCardHeaderArray.count > 0 {
            return progressCardHeaderArray.count
        }
        return 0
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        if rowCount == 0{
            return 0
        }
        return rowCount
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 100
        }
        return 100
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return 44
        }
        return 44
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if progressCardHeaderArray.count > 0{
            return 1
        }
        return 0
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        if rowCount > 0{
            return 1
        }
        return 0
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SpreadSheetCell.self), for: indexPath) as! SpreadSheetCell
        cell.label.textColor = UIColor.black
        if indexPath.column == 0 && indexPath.row == 0 {
            cell.label.text = "Subjects"
            cell.label.backgroundColor = Colors.headerBackground
        }
        else if indexPath.column == 0 && indexPath.row > 0 {
            cell.label.backgroundColor = Colors.headerBackground
            cell.label.text = ""
        }
        else if indexPath.column > 0 && indexPath.row == 0 {
            cell.label.backgroundColor = Colors.headerBackground
            if progressCardHeaderArray.count > 0{
               cell.label.text = progressCardHeaderArray[indexPath.column]
            }else{
                cell.label.text = ""
            }
        }
        else{
            if indexPath.column == 0 {
                cell.label.textAlignment = .left
            }else{
                cell.label.textAlignment = .center
            }
            
            if displayProgressCardArray[indexPath.row][indexPath.column] != "Bold"{
                cell.label.text = displayProgressCardArray[indexPath.row][indexPath.column]
            }
            else{
                cell.label.text = ""
            }
            
            if indexPath.column < progressCardHeaderArray.count{
                if displayProgressCardArray[indexPath.row][indexPath.column + 1] == "Bold" || displayProgressCardArray[indexPath.row][indexPath.column] == "Bold"{
                        cell.backgroundColor = Colors.headerBackground
                }
            }
            
            if displayProgressCardArray[indexPath.row][indexPath.column] == "AB"{
                cell.label.textColor =  studentAbsentcolur
            }
            else if displayProgressCardArray[indexPath.row][indexPath.column] == "NA"{
                cell.label.textColor = studentNotAssignedColor
            }
            else{
                cell.label.textColor = UIColor.black
            }
            
            cell.label.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
        }
        
        return cell
        
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
    }
}
