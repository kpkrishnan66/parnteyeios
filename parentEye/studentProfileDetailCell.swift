//
//  studentProfileDetailCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 27/06/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class studentProfileDetailCell: UITableViewCell {
    
    @IBOutlet var studentGuardianName: UILabel!
    @IBOutlet var studentAdmnno: UILabel!
    @IBOutlet var studentAddress: UILabel!
    @IBOutlet var studentDob: UILabel!
    @IBOutlet var studentMobno: UILabel!
    @IBOutlet var hide: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
}
    @IBAction func hideAction(sender: AnyObject) {
    }
}
