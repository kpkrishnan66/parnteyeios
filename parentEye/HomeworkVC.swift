//
//  HomeworkVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 19/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import CCBottomRefreshControl
import Firebase

class HomeworkVC: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SelectedSchoolpopOverDelegate
{
    
    @IBOutlet var noHomeworks: UIButton!
    @IBOutlet var homeWorkClassListShowViewHeight: NSLayoutConstraint!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var homeworkTableView: UITableView!
    @IBOutlet var homeworkCollectionView: UICollectionView!
    @IBOutlet var homeworkClassListView: UIView!
    @IBOutlet var addNewhomeworkButton: UIButton!
    @IBOutlet var addNewhomeworkButtonHeight: NSLayoutConstraint!
    @IBOutlet var homeworkCollectionViewHeight: NSLayoutConstraint!
    
    
    
    var user:userModel?
    var studentId:String = ""
    var classId:Int = 0
    var schoolId:Int = 0
    var page = 0
    let backColur = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    var ClassesList:[EmployeeClassModel] = []
    var defaultClassesList:[EmployeeClassModel] = []
    var selectedRow = 0
    var ClassIsSelected = [Int: Bool]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var initialIndexpath = NSIndexPath()
    let reuseIdentifier = "cell"
    var homeworkDetailedClass = false
    var classList : [classSubjectList]!
    var Classes:[EmployeeClassModel]?
    
    
    /*************************************************************************/
    /// Aray for storing the search results
    lazy var filteredHomeworkEntryListingDictionary = [String:[HomeWorkModel]]()
    lazy var homeWorkEntryListingDictionary         = [String:[HomeWorkModel]]()
    lazy var keyArray = [String]()
    /*************************************************************************/
    
    

    //MARK:- View delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        getUserDataFromDB() // syncing user from db
        alterHeader()
        noHomeworks.isHidden = true
        homeworkDetailedClass = false
        initializeTableView()
        FireBaseKeys.FirebaseKeysRuleTwo(value: "homePage_homework")
        if user?.currentTypeOfUser == .guardian{
         setUpUIforGuardian()
        }
        else if user?.currentTypeOfUser == .employee{
            setupCountOfItems()
        }
        else{
            setupUIforCorporateManager()
            setupCountOfItems()
        }
    }
    
    private func shouldGotoNextScreen() ->(Bool,HomeWorkModel?){
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadHomework
        tabBar.shouldLoadHomework = false
        return (result,tabBar.homeworkToBeLoaded)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        let result = shouldGotoNextScreen()
        if result.0 {
            performSegue(withIdentifier: segueConstants.segueForLoadingHomeworkDetailedScene, sender: result.1)
        }else {
            
        //clearDataForThisPage() // clearing data on page
        getUserDataFromDB() // syncing user from db
        alterHeader() // changing the header
        noHomeworks.isHidden = true
        clearDataForThisPage()
        if user?.currentTypeOfUser == .guardian{
            Analytics.logEvent("P_homePage_homework", parameters: nil)
        }
        if user?.currentTypeOfUser == .CorporateManager{
                header.multiSwitchButton.setTitle(user!.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
        }

        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            if  homeworkDetailedClass == false{
                if user?.currentTypeOfUser == .employee{
                    setupCountOfItems()
                }
                else{
                    setupUIforCorporateManager()
                    setupCountOfItems()
                }
                FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_homework")
                homeworkCollectionView.reloadData()
                homeworkCollectionView.scrollToItem(at: initialIndexpath as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
            }
        }
            if user?.currentTypeOfUser == .CorporateManager {
                if schoolId != user!.schools![(user?.currentSchoolSelected)!].schoolId {
                    getClassListForCorporateManager()
                    
                }
                else{
                    makeWebApiCallToGetHomeWorkEntries()// creating service to get data fresh
                }
            }
            else{
                makeWebApiCallToGetHomeWorkEntries()// creating service to get data fresh
            }

         homeworkDetailedClass = false
     }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- DB interactions
    /**
     Function to get user data from DB
     */
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    
    private func setUpUIforGuardian(){
        
        homeWorkClassListShowViewHeight.constant = 0.0
        addNewhomeworkButtonHeight.constant = 0.0
        addNewhomeworkButton.isHidden = true
        homeworkCollectionViewHeight.constant = 0.0
    }
    
    private func setupCountOfItems()
    {
        
        
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[0])
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        
    }
    private func setupUIforCorporateManager() {
        addNewhomeworkButtonHeight.constant = 0.0
        addNewhomeworkButton.isHidden = true
        homeWorkClassListShowViewHeight.constant = 69.0
        homeworkCollectionViewHeight.constant = 44.0
        
    }
    
    
    //MARK:- Header
    /**
     Function to update the header accoding to the screen we are in .
     */
    private func alterHeader(){
        
        header!.delegate = self
        if user?.currentTypeOfUser == .guardian{
            header.setUpHeaderForRootScreensWithNameOfScreen(name: ConstantsInUI.homeworkScreenHeading, withProfileImageUrl: user!.getSelectedEmployeeOrStudentProfileImageUrl())
        }
        else{
            if user?.currentTypeOfUser == .employee {
                if user!.switchAs == ""{
                    header.setUPHeaderForEmployee(name: ConstantsInUI.homeworkScreenHeading)
                }
                else if user!.switchAs == "Guardian"{
                    header.setUPHeaderForMultiRole(name: ConstantsInUI.homeworkScreenHeading)
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                header.setupHeaderForCorporateManager(name: (user?.getStudentCurrentlySelectedSchool()?.schoolName)!)
                
            }
            
        }
    }
    
    //MARK: tableView methods
    
    private func initializeTableView(){
        homeworkTableView.register(UINib(nibName: "HomeworkListingCell", bundle: nil), forCellReuseIdentifier: "HomeworkListingCell")
        homeworkTableView.estimatedRowHeight = 129.0
        homeworkTableView.rowHeight          = UITableViewAutomaticDimension
      //  homeworkTableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        let bottomRefreshControl                         = UIRefreshControl()
        bottomRefreshControl.triggerVerticalOffset       = 100.0
        bottomRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromBottomRefresh), for: .valueChanged)
        self.homeworkTableView.bottomRefreshControl = bottomRefreshControl
        
        let topRefreshControl                   = UIRefreshControl()
        topRefreshControl.triggerVerticalOffset = 100.0
        topRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromTopRefreshControl), for: .valueChanged)
        homeworkTableView.addSubview(topRefreshControl)
    }
    
    
    //MARK: Webservice call
    
    @objc private func loadWebServiceFromBottomRefresh(){
        homeworkTableView.bottomRefreshControl!.endRefreshing()
        self.makeWebApiCallToGetHomeWorkEntries()
    }
    @objc private func loadWebServiceFromTopRefreshControl(refreshControl:UIRefreshControl){
        refreshControl.endRefreshing()
        clearDataForThisPage()
        self.makeWebApiCallToGetHomeWorkEntries()
    }
    
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            return 1
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        initialIndexpath  = NSIndexPath(row: 0, section: 0)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! HomeworkEmployeeClassListCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: .touchUpInside)
        
        return cell
    }
    
    
    
    //MARK:- TAbleview delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredHomeworkEntryListingDictionary.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = keyArray[section]
        return (filteredHomeworkEntryListingDictionary[key]?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier:"HomeworkListingCell") as! HomeworkListingCell
        
        
        let key        = keyArray[indexPath.section]
        let homeworkEntry = filteredHomeworkEntryListingDictionary[key]![indexPath.row]
        
        
        if indexPath.row != 0 {
            cell.dateView.isHidden = true
        }else {
            cell.dateView.isHidden        = false
            cell.dayLabel.text          = homeworkEntry.dayString
            cell.monthAndYearLabel.text = homeworkEntry.monthString + " " + homeworkEntry.yearString
        }
        
        
        if homeworkEntry.AudioArray.count > 0 {
            cell.seperatorView.isHidden = false
            cell.attachmentImage.isHidden = false
            cell.attachmentImage.image = UIImage(named: "audioAttachmentIcon")
            cell.attachmentImageWidth.constant = 15
        }
        else if homeworkEntry.imageArray.count>0 || homeworkEntry.pdfArray.count>0 {
            cell.seperatorView.isHidden = false
            cell.attachmentImage.isHidden = false
            cell.attachmentImage.image = UIImage(named: "attachmentImage")
            cell.attachmentImageWidth.constant = 10
        }
        else{
            cell.seperatorView.isHidden = true
            cell.attachmentImage.isHidden = true
            cell.attachmentImageWidth.constant = 0
        }
        
        
        cell.homeworkTitle.text = homeworkEntry.title
        cell.homeworkSubject.text = homeworkEntry.subject
        cell.homeworkSubmitDate.text = homeworkEntry.submissionDate
        cell.homeworkAddedName.text = homeworkEntry.from
        
        setDateVisibilityForCell(cell: cell, forindex: indexPath as NSIndexPath, andArrayCount: filteredHomeworkEntryListingDictionary[key]!.count)
        
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let key          = keyArray[indexPath.section]
            
            let homework = filteredHomeworkEntryListingDictionary[key]![indexPath.row]
            self.performSegue(withIdentifier: segueConstants.segueForLoadingHomeworkDetailedScene, sender: homework)
        
       
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0 {
            return nil
        }else {
            let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.width, height:10))
            headerView.backgroundColor = backColur
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0{
            return 0.0
        }else {
            return 10
        }
    }
   
    
    
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        self.tabBarController?.slideMenuController()?.openLeft()
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!) // reflecting the change in DB
        clearDataForThisPage()
        makeWebApiCallToGetHomeWorkEntries()
        
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        if schoolId != user?.schools![(user?.currentSchoolSelected)!].schoolId {
            clearDataForThisPage()
            getClassListForCorporateManager()
        }
    }
    
    private func getClassListForCorporateManager() {
        var schoolId:Int = 0
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        schoolId = user!.schools![(user?.currentSchoolSelected)!].schoolId
        WebServiceApi.getClassListOfCorporateManager(schoolId: String(schoolId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let classSubjectListArray = result["classSubjectList"] as? [[String:AnyObject]]{
                self.classList = [classSubjectList]()
                for dic in classSubjectListArray{
                    let value = classSubjectList(fromDictionary: dic as NSDictionary)
                    self.classList.append(value)
                }
            }
            if let employeeclassList = self.classList as [classSubjectList]?{
                self.Classes?.removeAll()
                for classListOfEmployee in employeeclassList{
                    if let cls =  EmployeeClassModel.init(fromClassList: classListOfEmployee) {
                        if self.Classes == nil{
                            self.Classes = [EmployeeClassModel]()
                        }
                        self.Classes?.append(cls)
                    }
                }
            }
            self.user?.Classes?.removeAll()
            self.user?.Classes = self.Classes
            DBController().addUserToDB(self.user!)
            self.user = DBController().getUserFromDB()
            self.clearDataForThisPage()
            self.setupCountOfItems()
            self.homeworkCollectionView.reloadData()
            self.makeWebApiCallToGetHomeWorkEntries()
            let tab = self.tabBarController as! ParentEyeTabbar
            tab.getUnreadCount()
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }

    private func makeWebApiCallToGetHomeWorkEntries(){
        var selectedUser:String = ""
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            selectedUser = "Employee"
        }
        else{
            studentId = (user?.getSelectedEmployeeOrStudentId())!
            selectedUser = "Guardian"
        }
        if !studentId.isEmpty || classId != 0
        {
            
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.getHomeWorkEntry(studentId: studentId, classId: classId ,selectedUser: selectedUser,onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
        
                let resultTuple =  ParseHomeWork.parseFromDictionary(dictionary: result)
                if let err =  resultTuple.1{
                    AlertController.showToastAlertWithMessage(message: err.localizedDescription, stateOfMessage: .failure)
                }else {
                    weakSelf!.addEntriesToDictionary(dictionary: resultTuple.0)
                }
                
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                }, duringProgress: { (progress) in
                    
                }, forPage: page)
        }
    }
    
    
    
    private func clearDataForThisPage(){
        
        filteredHomeworkEntryListingDictionary.removeAll()
        homeWorkEntryListingDictionary.removeAll()
        keyArray.removeAll()
        if filteredHomeworkEntryListingDictionary.count == 0{
            
            noHomeworks.isHidden = false
           
        }
        else{
            noHomeworks.isHidden = true
            
        }
        
        homeworkTableView.reloadData()
        alterHeader()
     
    }
    
    
    //MARK:- array
    private func addEntriesToDictionary(dictionary:[String:[HomeWorkModel]]){
        
        filteredHomeworkEntryListingDictionary.addEntriesFromDictionary(otherDictionary: dictionary)
        homeWorkEntryListingDictionary = filteredHomeworkEntryListingDictionary
        keyArray = [String](filteredHomeworkEntryListingDictionary.keys)
        keyArray.sortDateStringArrayBasedOnDateWithDateFormat(.ddMMyyyyWithMonthNameWithHyphen)
        if filteredHomeworkEntryListingDictionary.count == 0{
            
            noHomeworks.isHidden = false
           
        }
        else{
            noHomeworks.isHidden = true
           
        }
        page = page+20
        homeworkTableView.reloadData()
    }
    
    private func setDateVisibilityForCell(cell:HomeworkListingCell, forindex indexPath:NSIndexPath ,andArrayCount count:Int){
        
        cell.TopLineView.isHidden = false
        cell.BottomLineView.isHidden = false
        
        if indexPath.row == 0 {
            cell.TopLineView.isHidden = true
            if count == 1 {
                cell.BottomLineView.isHidden = true
            }
        } else {
            cell.TopLineView.isHidden = false
            cell.BottomLineView.isHidden = false
            if indexPath.row+1 == count {
                cell.BottomLineView.isHidden = true
            }
        }
        
    }
    
    @IBAction func showComposeHomeworkScreen() {
        self.performSegue(withIdentifier: segueConstants.segueForLoadingComposeHomework, sender: nil)
    }
    
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        homeworkCollectionView.reloadData()
        clearDataForThisPage()
        makeWebApiCallToGetHomeWorkEntries()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == segueConstants.segueForLoadingHomeworkDetailedScene {
                let homeworkDetailed        = segue.destination as! HomeworkDetailesVC
                let Hw                 = sender as! HomeWorkModel
                homeworkDetailed.id = Hw.id
                homeworkDetailed.classId = classId
                homeworkDetailed.Hwtitle = Hw.title
                homeworkDetailed.HwFrom = Hw.from
                homeworkDetailed.reference = Hw.reference
                homeworkDetailed.Subject = Hw.subject
                homeworkDetailed.createdOn = Hw.createdOn
                homeworkDetailed.Hwdescription = Hw.description
                homeworkDetailed.dayString = Hw.dayString
                homeworkDetailed.monthString = Hw.monthString
                homeworkDetailed.yearString = Hw.yearString
                homeworkDetailed.submissionDate = Hw.submissionDate
                homeworkDetailed.imageArray   = Hw.imageArray
                homeworkDetailed.pdfArray     = Hw.pdfArray
                homeworkDetailed.AudioArray   = Hw.AudioArray
                homeworkDetailedClass = true
                FireBaseKeys.FirebaseKeysRuleOne(value: "homeWork_homeworkView")
                
            
           }
        
        else if segue.identifier == segueConstants.segueForLoadingComposeHomework {
            let composeHomework        = segue.destination as! composeHomeworkVC
            composeHomework.defaultClassSelected = defaultClassesList
            
            composeHomework.classId    = classId
            homeworkDetailedClass = true
            Analytics.logEvent("TP_homeWork_compose", parameters: nil)
        }
        
        
    }
    
}
