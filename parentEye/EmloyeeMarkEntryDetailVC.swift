//
//  EmloyeeMarkEntryDetailVC.swift
//  parentEye
//
//  Created by scientia on 17/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift

protocol dismissVCProtocol:class {
    func dismissFromSavedViewController()->Void
}

class EmloyeeMarkEntryDetailVC: UIViewController,CommonHeaderProtocol,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate {
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var monthAndYearLabel: UILabel!
    @IBOutlet var examName: UILabel!
    @IBOutlet var subjectName: UILabel!
    @IBOutlet var defaultOrGenderwiseTextField: UITextField!
    @IBOutlet var maxMarkLabel: UILabel!
    @IBOutlet var studentSearchBar: UISearchBar!
    @IBOutlet var markEntryTableView: UITableView!
    @IBOutlet var saveMarkButton: UIButton!
    @IBOutlet var searchButtonHeight: NSLayoutConstraint!
    @IBOutlet var backActionButton: UIButton!
    
    var user:userModel?
    var profilePic = ""
    var className:String = ""
    var classId:Int = 0
    var maxMark:Int = 0
    var evaluationActivitySubjectId:Int = 0
    var isclassTest:Bool = false
    var fromClassTestListingVC:Bool = false
    var sortByCategoryPicker = UIPickerView()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greencolur = UIColor(red: 99/255.0, green: 198/255.0, blue: 116/255.0, alpha: 1.0)
    var evaluationActivitySubjectResoponse = [String:AnyObject]()
    
    var examclassTest = classTest()
    var sortByCategoryListArray = ["Default","GenderWise"]
    lazy var evaluationActivitySubjectList = [evaluationActivitySubject]()
    lazy var studentList = [studentExamList]()
    lazy var tempStudentListArray = [studentExamList]()
    lazy var savedStudentListArray = [studentExamList]()
    weak var delegate:dismissVCProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        addPickerView()
        sortDefaultStudentList()
        initializeTableView()
    }
    
    
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    func setupUI () {
        var monthString = ""
        if isclassTest {
            
            let dateStringArray = examclassTest.date.components(separatedBy:resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
                if  dateStringArray.count >= 3{
                    dayLabel.text = dateStringArray[2]
                    
                    // This checking is used to check if the month format is a number (eg:8) or string (eg:august)
                    let letters = NSCharacterSet.letters
                    let range = dateStringArray[1].rangeOfCharacter(from: letters)
                    
                    if range != nil {
                        monthString = dateStringArray[1]
                    }
                    else {
                        let dateFormatter: DateFormatter = DateFormatter()
                        let months = dateFormatter.shortMonthSymbols
                        let monthSymbol = months?[Int(dateStringArray[1])!-1]
                        monthString = monthSymbol!
                    }
                    
                    monthAndYearLabel.text  = monthString+" - "+dateStringArray[0]
                }
                evaluationActivitySubjectId = examclassTest.id
            
                if fromClassTestListingVC{
                     subjectName.isHidden = false
                     subjectName.text = examclassTest.name
                     examName.text = "Exam : "+examclassTest.evaluationActivityName
                }
                else{
                    examName.text = "Exam : "+examclassTest.name
                    subjectName.isHidden = true
                 }
                maxMarkLabel.text = "Max mark : "+String(maxMark)
  
        }
        else{
        if evaluationActivitySubjectList.count > 0 {
            let dateStringArray = evaluationActivitySubjectList[0].date.components(separatedBy:resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
        if  dateStringArray.count >= 3{
            dayLabel.text = dateStringArray[2]
            
            // This checking is used to check if the month format is a number (eg:8) or string (eg:august)
            let letters = NSCharacterSet.letters
            let range = dateStringArray[1].rangeOfCharacter(from:letters)
            
            if range != nil {
                monthString = dateStringArray[1]
            }
            else {
                let dateFormatter: DateFormatter = DateFormatter()
                let months = dateFormatter.shortMonthSymbols
                let monthSymbol = months?[Int(dateStringArray[1])!-1]
                monthString = monthSymbol!
            }
            
            monthAndYearLabel.text  = monthString+" - "+dateStringArray[0]
        }
        evaluationActivitySubjectId = evaluationActivitySubjectList[0].id
        examName.text = "Exam : "+evaluationActivitySubjectList[0].evaluationActivityName
        subjectName.isHidden = false
        subjectName.text = "Subject : "+evaluationActivitySubjectList[0].subject
        maxMarkLabel.text = "Max mark : "+String(maxMark)
        
        }
        }
        
        for index in studentList {
            index.edit = false
            index.isValidation = true
            index.savedExamLabelText = ""
        }
        studentSearchBar.delegate = self
        studentSearchBar.returnKeyType = .done
        studentSearchBar.enablesReturnKeyAutomatically = false
        searchButtonHeight.constant = 0
        if fromClassTestListingVC {
            backActionButton.isHidden = true
        }
        else{
            backActionButton.isHidden = false
        }
        view.endEditing(false)
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysShow
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        studentSearchBar.endEditing(true)
    }
    
    func addPickerView(){
        
        sortByCategoryPicker.delegate = self
        sortByCategoryPicker.dataSource = self
        defaultOrGenderwiseTextField.inputView = sortByCategoryPicker
        defaultOrGenderwiseTextField.text = sortByCategoryListArray[0]
        sortByCategoryPicker.reloadAllComponents()
    }
    
    func sortDefaultStudentList() {
       studentList.sort(by: { $0.studentName < $1.studentName })
       tempStudentListArray.removeAll()
       tempStudentListArray = studentList
        markEntryTableView.reloadData()
    }
    
    func sortGenderwise() {
        var Genderval = 1
        var val = 0
        var maleListArray = [studentExamList]()
        var femaleListArray = [studentExamList]()
        
        
            maleListArray.removeAll()
            femaleListArray.removeAll()
            for i in 0 ..< studentList.count {
                if studentList[i].gender == "Male" {
                    maleListArray.append(studentList[i])
                }
                if studentList[i].gender == "Female" {
                    femaleListArray.append(studentList[i])
                }
            }
            studentList.removeAll()
            for k in 0 ..< maleListArray.count {
                studentList.append(maleListArray[k])
                //studentList[k].gendRollNo = Genderval
                Genderval = Genderval + 1
            }
            val = Genderval - 1
            for k in 0 ..< femaleListArray.count {
                studentList.append(femaleListArray[k])
                //studentList[val].gendRollNo = val
                val = val + 1
            }
        tempStudentListArray.removeAll()
        tempStudentListArray = studentList
        markEntryTableView.reloadData()
    }

    
    func initializeTableView () {
        markEntryTableView.register(UINib(nibName: "MarkEntryStudentListCell",bundle: nil), forCellReuseIdentifier: "MarkEntryStudentListCell")
        markEntryTableView.estimatedRowHeight = UITableViewAutomaticDimension
    }

    
    private func alterHeader () {
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.markEntryHeading+"  "+className, withProfileImageUrl: profilePic)
        header.delegate = self
    }

    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        EditStudentMarkExamAlert()
    }
    func showSwitchSibling(fromButton:UIButton) {
        
    }
    
    
    
    @IBAction func backSettingAction(_ sender: UIButton) {
    EditStudentMarkExamAlert()
}
    
    //Mark pickerview datasource and delegates
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sortByCategoryListArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sortByCategoryListArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        defaultOrGenderwiseTextField.text = sortByCategoryListArray[row]
        if row == 0 {
          sortDefaultStudentList()
        }
        else{
          sortGenderwise()
        }
        
    }
    
    // MARK: TableView Delegate and DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarkEntryStudentListCell", for: indexPath as IndexPath) as! MarkEntryStudentListCell
        cell.selectionStyle = .none
        if indexPath.row < studentList.count {
         cell.nameLabel.text = studentList[indexPath.row].studentName
         cell.savedExamLabel.text = studentList[indexPath.row].savedExamLabelText
           if studentList[indexPath.row].mark != "" {
              cell.markTextField.text = studentList[indexPath.row].mark
           }
           else{
            cell.markTextField.text = ""
            let attributes = [
                NSAttributedStringKey.foregroundColor: UIColor.lightGray,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 15)! // Note the !
            ]
            
            cell.markTextField.attributedPlaceholder = NSAttributedString(string: " Mark", attributes:attributes)
           }
        
        let border = CALayer()
        let width = CGFloat(2.0)
        border.borderColor = colur.cgColor
        border.frame = CGRect(x: 0, y: cell.markTextField.frame.size.height - width, width:  cell.markTextField.frame.size.width, height: cell.markTextField.frame.size.height)
        
        border.borderWidth = width
        cell.markTextField.layer.addSublayer(border)
        cell.markTextField.layer.masksToBounds = true
        cell.markTextField.tag = indexPath.row
            //cell.markTextField.tag = studentList[indexPath.row].studentId
        print(indexPath.row)
//        cell.markTextField.addTarget(self, action: #selector(MarkEntryVC.textFieldDidBeginEditing(_:)), forControlEvents: UIControlEvents.EditingDidBegin)
        cell.markTextField.addTarget(self, action: #selector(self.textFieldDidChange), for: UIControlEvents.editingDidEnd)
        cell.selectAbsentOrNonApplicableButton.tag = indexPath.row
        cell.selectAbsentOrNonApplicableButton.addTarget(self,action: #selector(self.selectAbsentOrNonApplicableButtonAction),for: UIControlEvents.touchUpInside)
        }
        return cell
    }
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //contactArrayTemp = contactArray
        if searchText == ""{
            studentList.removeAll()
            studentList = tempStudentListArray
        }else{
            let filtered = tempStudentListArray.filter { $0.studentName.contains(searchText) }
            studentList = filtered
        }
        markEntryTableView.reloadData()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        print("scrollll")
        view.endEditing(true)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
         print("stop")
        view.endEditing(false)
    }

    @objc func textFieldDidChange(textField: UITextField) {
        print("textfield tag ===\(textField.tag)")
        view.endEditing(false)
        let indexPath = IndexPath(row: textField.tag, section: 0)
        let cell = markEntryTableView.cellForRow(at: indexPath) as! MarkEntryStudentListCell!
        
//        let indexPath = NSIndexPath(forRow:textField.tag, inSection:0)
//        let cell : MarkEntryStudentListCell? = markEntryTableView.cellForRowAtIndexPath(indexPath) as! MarkEntryStudentListCell?
        
        //if cell != nil {
        if cell!.markTextField.text! == "" {
            cell!.savedExamLabel.text = ""
            studentList[textField.tag].savedExamLabelText = ""
            for k in 0 ..< studentList.count {
                if k == textField.tag {
                    studentList[k].mark = textField.text!
                    studentList[k].edit = true
                    studentList[k].isValidation = true
                    break
                }
            }
        }
        
        if let enteredmark = NumberFormatter().number(from: cell!.markTextField.text!) {
                
                let maxmarkvalue = CGFloat(maxMark)
                let enteredmarkvalue = CGFloat(enteredmark)
                if enteredmarkvalue > maxmarkvalue {
                    cell!.savedExamLabel.text! = "mark greater than max"
                    cell!.savedExamLabel.textColor = colur
                    studentList[textField.tag].isValidation = false
                    studentList[textField.tag].mark = textField.text!
                    studentList[textField.tag].edit = true
                    studentList[textField.tag].savedExamLabelText = "mark greater than max"
                }
                else{
                    cell!.savedExamLabel.text = ""
                    studentList[textField.tag].savedExamLabelText = ""
                    for k in 0 ..< studentList.count {
                        if k == textField.tag {
                            studentList[k].mark = textField.text!
                            studentList[k].edit = true
                            studentList[k].isValidation = true
                            break
                        }
                    }
                }
                
            }
        //}
    }

    
    @objc func selectAbsentOrNonApplicableButtonAction(button:UIButton) {
        
        let indexPath = IndexPath(row: button.tag, section: 0)
        let cell = markEntryTableView.cellForRow(at: indexPath) as! MarkEntryStudentListCell!
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .alert)
        
        let absentAction = UIAlertAction(title: ConstantsInUI.markEntryAbsent, style:.destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            cell?.markTextField.text = "AB"
            self.studentList[button.tag].mark = "AB"
            self.studentList[button.tag].edit = true
            self.studentList[button.tag].savedExamLabelText = ""
            self.studentList[button.tag].isValidation = true
            cell?.savedExamLabel.text = ""
        })
        
        let NotApplicableAction = UIAlertAction(title: ConstantsInUI.markEntryNotApplicable, style:.destructive, handler: {
            (alert: UIAlertAction!) -> Void in
            cell?.markTextField.text = "NA"
            self.studentList[button.tag].mark = "NA"
            self.studentList[button.tag].edit = true
            self.studentList[button.tag].savedExamLabelText = ""
            self.studentList[button.tag].isValidation = true
            cell?.savedExamLabel.text = ""
        })
        
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(absentAction)
        optionMenu.addAction(NotApplicableAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.width / 2.0 , y: self.view.bounds.height / 2.0, width: 1.0, height: 1.0)
        self.present(optionMenu, animated: false, completion: nil)
        
        
    }
    
    private func validateMaxMark() -> (Bool,String) {
        var result:Bool = true
        var errorMessage:String = ""
        var nullMark:Bool = true
        var edit:Bool = false
        for k in 0 ..< studentList.count {
            if studentList[k].isValidation == false {
                result = false
                errorMessage = ConstantsInUI.maxmarkError
                break
            }
            
        }
        if result {
            for k in 0 ..< studentList.count {
                if studentList[k].mark != "" {
                    nullMark = false
                    break
                }
            }
            if nullMark {
                result = false
                errorMessage = ConstantsInUI.nullMarkError
            }
        }
        if result {
            for k in 0 ..< studentList.count {
                if studentList[k].edit {
                    edit = true
                    break
                }
            }
            if !edit{
                result = false
                errorMessage = ConstantsInUI.nullMarkError
            }
        }
        
        return (result,errorMessage)
    }

    
    @IBAction func saveMarkEntryAction(_ sender: UIButton) {
    
        let result = validateMaxMark()
        if result.0 {
            savedStudentListArray.removeAll()
            for pos in studentList {
                if pos.edit == true && pos.mark != ""{
                    savedStudentListArray.append(pos)
                }
            }
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.saveMarkEntry(classId: String(classId),evaluationActivitySubjectId: String(evaluationActivitySubjectId),studentExamListArray: savedStudentListArray,onSuccess: { (result) in
                
                print(result)
                hudControllerClass.hideHudInViewController(viewController: self)
                if ((result["error"] as? [String:AnyObject]) == nil){
                    self.savedStudentMarkExamAlert()
                }
                else{
                   self.savedStudentMarkExamErrorAlert()
                }
                
                },
                onFailure: { (error) in
                                            
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                            
                },
                
                duringProgress: { (progress) in
            })
            
        }
        else{
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }

    }

    private func EditStudentMarkExamAlert(){
        var flag:Int = 0
        var markEntrySaveMessage:String = ""
        for i in 0 ..< self.studentList.count {
            if self.studentList[i].edit == true {
                flag = 1
                break
            }
            
        }
        if flag == 1 {
            markEntrySaveMessage = ConstantsInUI.markentryediteddiscardmessage
        
        
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: markEntrySaveMessage, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            
            self.dismiss(animated: true, completion: nil)
        }
        self.present(alert, animated: false, completion: nil)
        }
        else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    private func savedStudentMarkExamAlert(){
            
       
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.markentrysaveddiscardmessage, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
           // self.navigationController?.popToRootViewControllerAnimated(false)
            //self.dismissViewControllerAnimated(true, completion: nil)
            //self.navigationController?.popViewControllerAnimated(true)
            if !self.fromClassTestListingVC {
               self.delegate?.dismissFromSavedViewController()
            }
            self.dismiss(animated: true, completion: nil)
        }
        
        for index in studentList {
            index.edit = false
        }
        self.present(alert, animated: false, completion: nil)
        
        
    }
    
    private func savedStudentMarkExamErrorAlert(){
        let alert = AlertController.getSingleOptionAlertControllerWithMessage(mesasage: ConstantsInUI.markentrysaveerrormessage, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, OkButtonBlock:{ (action)
            in
           
        })
        self.present(alert,animated: false,completion: nil)
    }


}

