//
//  composeHomeworkVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 23/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import Firebase
import IQKeyboardManagerSwift

class composeHomeworkVC: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,dateSelectedButtonPressedProtocol,UIPickerViewDelegate,UIPickerViewDataSource, UITextFieldDelegate,HomeworkComposeClassListProtocol,HomeworkComposeClassStudentListProtocol,attachmentDeleteButtonPressedProtocol,UIImagePickerControllerDelegate,attachmentimagelibraryProtocol,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIDocumentPickerDelegate
   
{
    
    @IBOutlet var header: CommonHeader!
    
    @IBOutlet var subjectListTextField: UITextField!
    
    @IBOutlet var subjectListShowDropdownIcon: UIImageView!
    
    @IBOutlet var creationDateShowLabel: UILabel!
    
    @IBOutlet var addTitleTextField: UITextField!
   
    @IBOutlet var submissionDateTextfield: UITextField!
    
    
    @IBOutlet var addDescriptionTextview: UITextView!
    
    @IBOutlet var addReferenceTextfield: UITextView!
    
    @IBOutlet var addAttachmentsImageView: UIImageView!
    
    @IBOutlet var addAttachmentView: ExpansiveView!
   
    @IBOutlet var addAttachmentViewHeight: NSLayoutConstraint!
    
    @IBOutlet var mainViewHeight: NSLayoutConstraint!
    
    @IBOutlet var classesShowTextfield: UITextField!
    
    @IBOutlet var studentListShowTextfield: UITextField!
    
    @IBOutlet weak var documentAttachmentTableView: UITableView!
    
    @IBOutlet weak var documentAttachmentTableViewHeight: NSLayoutConstraint!
    
    var user:userModel?
    var profilePic = ""
    var homeworkAddDate = Foundation.Date()
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var classId:Int = 0
    lazy var defaultClassSelected  = [EmployeeClassModel]()//Array to store all the receipients
    var subjectList = [employeeSubjectList]()
    var defaultclassDisplay:Bool = false
    lazy var messageReciepientArray  = [EmployeeSubjectSelectedClassList]()//Array to store all the receipients
    lazy var usersArray  = [EmployeeSubjectSelectedClassList]()//Array to store all the receipients
    lazy var temprecipientArray  = [EmployeeSubjectSelectedClassList]()
    var subjectId = -1
    var classListArray = [EmployeeSubjectSelectedClassList] ()
    var studentListArray = [composeDiaryStudentPopupList]()
    var studentListRecipientArray = [composeDiaryStudentPopupList]()
    lazy var uploadAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    lazy var copyuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var tempuploadAttachmentArray = [uploadAttachmentModel]()
    var uploadAttachmentFailure:Bool = false
    var uploadedcount:Int = 0
    var cameraSelected = false
    var attachmentImages = [Int:UIImage]()
    var toUploadAttachments = [Int:UIImage]()
    var cameraImages = [Int:UIImage]()
    var activeSelection = false
    var deleteAttachment = false
    var classListString = ""
    var studentListString = ""
    var attachmentListString = ""
    var imageArray = [UIImageView]()
    var tagCount = 0
    var posCount = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        setupTable()
        makeWebApiCallToGetEmployeeSubjectList()
        addPickerView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    fileprivate func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.addNewHomework, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    
    fileprivate func setupUI() {
        
        addTitleTextField.layer.borderWidth = CGFloat(1)
        submissionDateTextfield.layer.borderWidth = CGFloat(1)
        addDescriptionTextview.layer.borderWidth = CGFloat(1)
        addReferenceTextfield!.layer.borderWidth = CGFloat(1)
        classesShowTextfield.layer.borderWidth = CGFloat(1)
        studentListShowTextfield.layer.borderWidth = CGFloat(1)
        
        
        
        addTitleTextField.autocapitalizationType = UITextAutocapitalizationType.sentences
        addDescriptionTextview.autocapitalizationType = UITextAutocapitalizationType.sentences
        
        
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        addTitleTextField.layer.borderColor = color.cgColor
        submissionDateTextfield.layer.borderColor = color.cgColor
        addDescriptionTextview.layer.borderColor = color.cgColor
        addReferenceTextfield!.layer.borderColor = color.cgColor
        classesShowTextfield.layer.borderColor = color.cgColor
        studentListShowTextfield.layer.borderColor = color.cgColor
        
        creationDateShowLabel.text = DateApi.convertThisDateToString(homeworkAddDate, formatNeeded: .ddMMyyyyWithHypen)
        //submissionDateTextfield.userInteractionEnabled = false
        submissionDateTextfield.delegate = self
        classesShowTextfield.text = defaultClassSelected[0].className
        classesShowTextfield!.delegate = self
        defaultclassDisplay = true
        studentListShowTextfield.delegate = self
        addAttachmentView.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(composeHomeworkVC.addAttachmentImageTapped(_:)))
        addAttachmentsImageView.isUserInteractionEnabled = true
        addAttachmentsImageView.addGestureRecognizer(tapGestureRecognizer)
        
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
        
    }
    
    fileprivate func setupTable() -> Void {
        
        documentAttachmentTableView.register(UINib(nibName: "attachmentDocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "attachmentDocumentTableViewCell")
        documentAttachmentTableView.estimatedRowHeight = 68.0
    }
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        showAlert()
    }
    
    func showSwitchSibling(fromButton: UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popOverDidSelectRow(rowSelected: Int) {
        
    }
    
    //MARK: -UITextfield deleagte to dismiss keyboard
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == submissionDateTextfield{
            submissionDateTextfield.resignFirstResponder()
            selectSubmissionDate()
            return false
        }else if textField == classesShowTextfield {
            classesShowTextfield.resignFirstResponder()
            showClassListForEmployeeSelectedSubject()
            return false
        }else if textField == studentListShowTextfield {
            studentListShowTextfield.resignFirstResponder()
            showStudentListPopup(textField)
            return false
        }

        return true;
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    
    fileprivate func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @IBAction func showHomeworkDatePicker(_ sender: UIButton) {
        
        selectSubmissionDate()
        
    }
    
    @IBAction func submissionDateUITextfieldButtonTapped(_ sender: UITextField) {
        
        selectSubmissionDate()
        
    }
    fileprivate func prepareOverlayVC(_ overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    fileprivate func selectSubmissionDate(){
        
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        datePickerVC.delegate = self
        
        datePickerVC.viewLoadedCompletion = {(datePicker)->Void in
            
            datePicker.minimumDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: 0, to: Foundation.Date(), options: [])
            
        }
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            weakSelf!.homeworkAddDate = date as Foundation.Date
            weakSelf!.submissionDateTextfield.text = DateApi.convertThisDateToString(weakSelf!.homeworkAddDate, formatNeeded: .ddMMyyyyWithHypen)
            
        }
        prepareOverlayVC(datePickerVC)
        
        self.present(datePickerVC, animated: false, completion: nil)
        
        //}
    }

    
    func ButtonPressed(){
        
    }
    
    func addPickerView(){
        let employeeSubjectPicker = UIPickerView()
        employeeSubjectPicker.delegate = self
        employeeSubjectPicker.dataSource = self
        subjectListTextField.inputView = employeeSubjectPicker
        
        if subjectList.count != 0{
            subjectListTextField.text = subjectList[0].subjectName
            subjectId = subjectList[0].subjectId
        }
        employeeSubjectPicker.reloadAllComponents()
    }
    
    // MARK: PickerView Datasources
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjectList.count
    }
    
    // MARK: PickerView Delegates
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if subjectList.count != 0 {
            return subjectList[row].subjectName
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if subjectList.count != 0{
            subjectListTextField.text = subjectList[row].subjectName
            subjectId = subjectList[row].subjectId
        }
        
    }
    
    fileprivate func makeWebApiCallToGetEmployeeSubjectList(){
        
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getHomeworkEmployeeStudentList(userRoleId: (user?.currentUserRoleId)!, classId: (defaultClassSelected[0].classId) ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.subjectList = parsesubjectsArray.parseThisDictionaryForAttachments(dictionary: result)
            self.addPickerView()
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
        
    }
    
    @IBAction func addClassAction(_ sender: UIButton) {
        showClassListForEmployeeSelectedSubject()
    }
    
    @IBAction func showClassListPopup(_ sender: Any) {
        
       showClassListForEmployeeSelectedSubject()
        
    }
    
    
    
    @IBAction func showStudentListPopup(_ sender: UITextField) {
        
        if !(classesShowTextfield.text?.isEmpty)!{
            var idString = ""
            if messageReciepientArray.count == 0{
                idString = String(defaultClassSelected[0].classId)
                
            }
                
            else{
                for classes in messageReciepientArray {
                    idString = idString+String(classes.classId) + ","
                }
                idString = idString.substring(to: idString.characters.index(before: idString.endIndex))
            }
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            
            WebServiceApi.getEligibleStudentListToComposeDiary(schoolClassIdList: idString, onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
                self.showClassStudentListPopUp()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
            })
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectantclass, stateOfMessage: .failure)
            
        }
        
    }
    
    
    fileprivate func showClassListForEmployeeSelectedSubject(){
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getEligibleClassListToComposeHomework(userRoleId: (user?.currentUserRoleId)!,subjectId: subjectId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            weakSelf!.classListArray = parseSubjectSelectesdClassArray.parseThisDictionaryForComposeHomework(dictionary: result)
            self.usersArray = self.classListArray
            self.showClassList()
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
    }
    
    fileprivate func showClassList(){
        
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "HomeworkComposeClassListPopUpVC") as!  HomeworkComposeClassListPopUpVC ;
        if defaultclassDisplay == true{
            messageReciepientArray.removeAll()
            
            for k in 0 ..< usersArray.count {
                if classId == usersArray[k].classId{
                    usersArray[k].selectedInUI = true
                    messageReciepientArray.append(usersArray[k])
                    break
                }
            }
            
        }
        msRecipVC.recipientArray = usersArray
        msRecipVC.searchrecipientArray = usersArray
        msRecipVC.previousEntryArray = messageReciepientArray
        defaultclassDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    func updateSelectedWithclassListArray(receipientArray: [EmployeeSubjectSelectedClassList]) {
        
        classesShowTextfield!.text = ""
        
        if receipientArray.count == 0{
            classesShowTextfield!.text = ""
        }
            
        else if receipientArray.count == 1{
            classesShowTextfield!.text = receipientArray[0].className
        }
        else if receipientArray.count == 2{
            classesShowTextfield!.text = receipientArray[0].className+","+receipientArray[1].className
        }
        else if receipientArray.count == 3{
            classesShowTextfield!.text = receipientArray[0].className+","+receipientArray[1].className+","+receipientArray[2].className
        }
        else if receipientArray.count == user?.Classes?.count{
            classesShowTextfield!.text = "All"
        }
        else{
            classesShowTextfield!.text = receipientArray[0].className+","+receipientArray[1].className+","+receipientArray[2].className+" + "+String(receipientArray.count - 3)
            
        }
        
        messageReciepientArray.removeAll()
        messageReciepientArray = receipientArray
    }
    
    
    func showClassStudentListPopUp(){
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "HomeworkComposeClassStudentListPopUpVC") as! HomeworkComposeClassStudentListPopUpVC ;
        if studentListShowTextfield.text == "All"{
            studentListRecipientArray = studentListArray
        }
        
        msRecipVC.recipientArray = studentListArray
        msRecipVC.searchrecipientArray = studentListArray
        msRecipVC.previousEntryArray = studentListRecipientArray
        msRecipVC.delegate = self
        prepareOverlayVC(msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
        
    }
    
    
    func updateSelectedWithStudentArray(receipientArray: [composeDiaryStudentPopupList]) {
        
        studentListShowTextfield!.text = ""
        
        if receipientArray.count == 0{
            studentListShowTextfield!.text = ""
        }
            
        else if receipientArray.count == 1{
            studentListShowTextfield!.text = receipientArray[0].studentName
        }
        else if receipientArray.count == studentListArray.count {
            studentListShowTextfield!.text = "All"
        }
        else
        {
            
            studentListShowTextfield!.text = receipientArray[0].studentName+" + "+String(receipientArray.count - 1)
        }
        
        
        studentListRecipientArray.removeAll()
        studentListRecipientArray = receipientArray
    }
    
    @IBAction func selectAllStudentButtonTapped(_ sender: UIButton) {
        studentListShowTextfield!.text = "All"
    }
    
    @objc func addAttachmentImageTapped(_ img: AnyObject) {
        
        FireBaseKeys.FirebaseKeysRuleFour(value: "composehomeWork_addAttach")
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate               = self
        imagePicker.allowsEditing          = false
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
        let popper                         = imagePicker.popoverPresentationController
        popper?.delegate                   = self
        popper?.sourceView                 = self.view
        popper?.sourceRect                 = CGRect(x: self.view.bounds.width / 2.0 , y: self.view.bounds.height / 2.0, width: 1.0, height: 1.0)
        
        
        
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ConstantsInUI.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: ConstantsInUI.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "photoGalleryId") as! PhotoGallery
            vc.delegate = self
            
            self.toUploadAttachments.removeAll()
            self.uploadedcount = 0
            
            self.activeSelection = false
            self.deleteAttachment = false
            
            self.present(vc, animated: true, completion: nil)
        })
        
        let shareDocumentAction = UIAlertAction(title: ConstantsInUI.shareDocument, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF as NSString) , String(kUTTypeContent)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(shareDocumentAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.width / 2.0 , y: self.view.bounds.height / 2.0, width: 1.0, height: 1.0)
        self.present(optionMenu, animated: false, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var documentName:String = ""
        print(url)
        
        if url.pathExtension == "pdf" || url.pathExtension == "doc" || url.pathExtension == "docx" {
            if(FileManager.default.fileExists(atPath: url.path)) {
                
                let dictoryUrl = url.absoluteString
                if let range = dictoryUrl.range(of: "/", options: .backwards) {
                    _ = dictoryUrl.substring(from: range.upperBound)
                }
                if let docName = dictoryUrl.components(separatedBy: "/").last {
                    print(docName)
                    documentName = docName
                }
                
                let fileData = try? Data(contentsOf: url)
                UploadDocumentAttachment(fileData!,docName: documentName)
            }
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectFormat, stateOfMessage: .failure)
        }
    }
    
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated: true, completion: nil) //5
        cameraImages[0] = chosenImage
        cameraSelected = true
        SelectedImagesWithArray(selectedImages: cameraImages)
    }
    func reuploadImage(){
        
    }
    
    func SelectedImagesWithArray(selectedImages:[Int:UIImage])
    {
        toUploadAttachments = selectedImages
        uploadedcount = 0
        tempuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray = uploadAttachmentArray
        uploadAttachmentFailure = false
        startInitialUpload(selectedImages)
        
    }
    func check(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var attachmentImagesArrayPosition:Int = 0
        attachmentImagesArrayPosition = attachmentImages.count
        print(uploadAttachmentArray.count)
        print(tempuploadAttachmentArray.count)
        print(attachmentImages)
        
        if uploadAttachmentArray.count != 0 || documentAttachmentArray.count != 0{
            if uploadAttachmentFailure == true {
                imageArray.removeAll()
                tagCount = 0
                posCount = 0
                for i in 0 ..< uploadAttachmentArray.count {
                    if uploadAttachmentArray[i].attachmentType == "PDF" || uploadAttachmentArray[i].attachmentType == "DOC" || uploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:URL = URL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:Data = try! Data(contentsOf: url)
                        let imagea =  UIImage(data:data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                        
                        
                    }
                }
            }
            else{
                for i in 0 ..< tempuploadAttachmentArray.count {
                    if tempuploadAttachmentArray[i].attachmentType == "PDF" || tempuploadAttachmentArray[i].attachmentType == "DOC" || tempuploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:URL = URL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:Data = try! Data(contentsOf: url)
                        let imagea =  UIImage(data:data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                    }
                }
            }
            
            addAttachmentViewHeight.constant = 0
            mainViewHeight.constant = 660
            documentAttachmentTableView.reloadData()
            
            while let subview = addAttachmentView.subviews.last {
                subview.removeFromSuperview()
            }
            
            deleteAttachment = false
            addAttachmentViewHeight.constant = addAttachmentView.addImagesToMeFromArrayForAttachments(imageArray: imageArray)
            documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            mainViewHeight.constant = mainViewHeight.constant + (addAttachmentViewHeight.constant - 50) + documentAttachmentTableViewHeight.constant
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            
        }
        else {
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            addAttachmentViewHeight.constant = 0
            mainViewHeight.constant = 660
            while let subview = addAttachmentView.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
    }
    
    func startInitialUpload(_ selectedImages:[Int:UIImage]){
        
        for k in 0 ..< selectedImages.count {
            
            upLoadImage(selectedImages[k]!)
            
        }
    }
    
    
    func upLoadImage(_ image:UIImage){
        weak var weakSelf = self
        addAttachmentViewHeight.constant = 0
        documentAttachmentTableViewHeight.constant = 0
        mainViewHeight.constant = 660
        while let subview = addAttachmentView.subviews.last {
            subview.removeFromSuperview()
        }
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadHomeworkAttachmentImage(image: image, cameraSelected: cameraSelected,onSuccess: { (result) in
            weakSelf!.addEntriesToAttachmentArray(parseAttachmentArrayForHomework.parseThisDictionaryForAttachments(dictionary: result))
            self.uploadedcount = self.uploadedcount + 1
            if self.toUploadAttachments.count == self.uploadedcount{
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                self.deleteAttachment = false
                self.check()
                
            }
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.check()
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    func UploadDocumentAttachment(_ documentData:Data,docName:String){
        var selectedTab:String = ""
        weak var weakSelf = self
        addAttachmentViewHeight.constant = 0
        documentAttachmentTableViewHeight.constant = 0
        while let subview = addAttachmentView.subviews.last {
            subview.removeFromSuperview()
        }
        selectedTab = "Homework"
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadAttachmentDocument(uploadDocumentData: documentData as NSData,docName: docName,selectedTab: selectedTab,
                                               onSuccess: { (result) in
                                                weakSelf!.addEntriesToDocumentAttachmentArray(parseAttachmentArrayForHomework.parseThisDictionaryForAttachments(dictionary: result))
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                self.deleteAttachment = false
                                                self.check()
                                                
                                                
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.uploadAttachmentArray.removeAll()
            self.uploadAttachmentArray = self.copyuploadAttachmentArray
            self.uploadAttachmentFailure = true
            self.tempuploadAttachmentArray.removeAll()
            print("failed")
            self.check()
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    
    open func addEntriesToAttachmentArray(_ array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            
            uploadAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    open func addEntriesToDocumentAttachmentArray(_ array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            tempuploadAttachmentArray.removeAll()
            documentAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    internal func SelectedAttachmentTag(selectedAttachment currentTag : Int){
        
        var tempimageArray = [UIImageView]()
        var pos = 0
        var tagTagForImage = 0
        
        
        activeSelection = true
        uploadAttachmentArray.remove(at: currentTag)
        imageArray.remove(at: currentTag)
        deleteAttachment = true
        for k in 0 ..< imageArray.count {
            
            if imageArray[k].image != nil{
                imageArray[k].contentMode = .scaleAspectFit
                imageArray[k].tag = tagTagForImage
                tempimageArray.append(imageArray[k])
                tagTagForImage = tagTagForImage + 1
                pos = pos + 1
            }
        }
        tagCount = tagTagForImage
        posCount = pos
        if uploadAttachmentArray.count != 0 && imageArray.count != 0  {
            addAttachmentViewHeight.constant = 0
            mainViewHeight.constant = 660
            while let subview = addAttachmentView.subviews.last {
                subview.removeFromSuperview()
            }
            
            deleteAttachment = false
            
            addAttachmentViewHeight.constant = addAttachmentView.addImagesToMeFromArrayForAttachments(imageArray: tempimageArray)
            documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            
            mainViewHeight.constant = mainViewHeight.constant + (addAttachmentViewHeight.constant - 50)+documentAttachmentTableViewHeight.constant
            
        }
        else {
            
            addAttachmentViewHeight.constant = 0
            mainViewHeight.constant = 660
            while let subview = addAttachmentView.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
        
    }
    
    @IBAction func composeHomeworkAction(_ sender: UIButton) {
        
        FireBaseKeys.FirebaseKeysRuleFour(value: "composehomeWork_publish")
        let result = validateFields()
        if result.0 {
            
            if messageReciepientArray.count == 0{
                classListString = String(defaultClassSelected[0].classId)
                
            }
            else{
                for classArray in messageReciepientArray{
                    classListString = classListString+String(classArray.classId) + ","
                }
                classListString = classListString.substring(to: classListString.characters.index(before: classListString
                    .endIndex))
            }
            if studentListShowTextfield.text == "All"{
                studentListString = "All"
            }
            else{
                
                for studentArray in studentListRecipientArray{
                    studentListString = studentListString+String(studentArray.studentId) + ","
                }
                studentListString = studentListString.substring(to: studentListString.characters.index(before: studentListString
                    .endIndex))
            }
            
            if uploadAttachmentArray.count > 0 {
                for attachmentArray in uploadAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex))
                
            }
            if documentAttachmentArray.count > 0 {
                if uploadAttachmentArray.count != 0 {
                    attachmentListString = attachmentListString + ","
                }
                for attachmentArray in documentAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex))
            }
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            WebServiceApi.postHomeworkWithTitle(homeworkTitle: addTitleTextField.text!,classList: classListString,
                                                homeworkstudentList: studentListString,
                                                idOfDiaryAddingPerson: (user?.currentUserRoleId)!,
                                                homeworkReference: addReferenceTextfield.text,
                                                homeworkSubjectid: subjectId,
                                                homeworkSubmissionDate: submissionDateTextfield.text!,
                                                homeworkDescription: addDescriptionTextview.text,
                                                attachmentIdList: attachmentListString,
                                                onSuccess: { (result) in
                                                    
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    if (result["error"] as? [String:AnyObject]) == nil{
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.publishSuccess, stateOfMessage: .success)
                                                        
                                                        weakSelf?.clearAllData()
                                                        _ = weakSelf!.navigationController?.popViewController(animated: true)
                                                        
                                                    }else{
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.homeworkFailed, stateOfMessage: .failure)
                                                    }
                                                    
                                                    
                                                    
                                                    
            },
                                                onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.publishSuccess, stateOfMessage: .failure)
                                                    
            },
                                                duringProgress: { (progress) in
                                                    
            })
            
            
            
        }
        else {
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
        
    }
    
    
    fileprivate func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
        
        if (subjectListTextField.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.addNewHomeworkSubject
        }
        else if addTitleTextField.text == "" {
            result   = false
            ErrorMsg = ConstantsInUI.addNewHomeworkTitle
        }
            
        else if (submissionDateTextfield.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.addNewHomeworkSubmissionDate
        }
            
        else if (addDescriptionTextview.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.addNewHomeworkdescription
        }
            
        else if (classesShowTextfield.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.selectclass
        }
            
        else if (studentListShowTextfield.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.selectstudent
        }
        
        
        
        
        return (result,ErrorMsg)
    }
    
    fileprivate func clearAllData(){
        
        addTitleTextField.text   = ""
        submissionDateTextfield.text        = ""
        addDescriptionTextview.text = ""
        addReferenceTextfield.text = ""
        classesShowTextfield.text = ""
        subjectListTextField.text = ""
        
        
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentAttachmentArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "attachmentDocumentTableViewCell") as! attachmentDocumentTableViewCell
        cell.documentUniqueName.text = documentAttachmentArray[indexPath.row].attachmentName
        cell.selectionStyle = .none
        if documentAttachmentArray[indexPath.row].attachmentType == "PDF" {
            cell.documentIcon.image = UIImage(named: "pdfIcon")
        }
        if documentAttachmentArray[indexPath.row].attachmentType == "DOCX" || documentAttachmentArray[indexPath.row].attachmentType == "DOC" {
            cell.documentIcon.image = UIImage(named: "wordIcon")
            
        }
        cell.documentCloseButton.tag = indexPath.row
        cell.documentCloseButton.addTarget(self, action: #selector(composeHomeworkVC.deleteDocumentAttachment(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc  fileprivate func deleteDocumentAttachment(_ sender:UIButton) {
        for k in 0 ..< documentAttachmentArray.count {
            if k == sender.tag {
                documentAttachmentArray.remove(at: k)
            }
        }
        documentAttachmentTableView.reloadData()
        documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
        if uploadAttachmentArray.count != 0 {
            mainViewHeight.constant = mainViewHeight.constant - 40
        }
    }
    
    
    
}
