//
//  SecondViewController.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import Firebase

class NotificationListing: UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,SelectedSchoolpopOverDelegate{
    
    var user:userModel?
    var page = 0
    var isViewByPhoto = false // varible to find if table listing is based on
    var noticeDtailedOrCompose:Bool = false
    
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var noticeListingTable: UITableView!
    @IBOutlet weak var listTypeChangingButton:UIButton!
    @IBOutlet var composeNotice: UIButton!
    @IBOutlet var composeNoticeButtonHeight: NSLayoutConstraint!
    @IBOutlet var composeNoticeHeight: NSLayoutConstraint!
 
    lazy var notificationsEntryListingDictionary = [String:[NotificationModel]]()
    lazy var filterednotificationsEntryListingDictionary = [String:[NotificationModel]]()
    lazy var dateKeysArray = [String]()
    lazy var pdfViewer = PdfController()
    var classList : [classSubjectList]!
    var Classes:[EmployeeClassModel]?
    var schoolId:Int = 0
    
    //MARK:- view delegates 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        self.tabBarController?.tabBar.isHidden = false
        FireBaseKeys.FirebaseKeysRuleTwo(value: "homePage_notice")
        
        if user?.currentTypeOfUser == .guardian{
          composeNoticeButtonHeight.constant = CGFloat(0)
            composeNoticeHeight.constant = CGFloat(0)
            composeNotice.setTitle("", for: .normal)
        }
        initializeTableView()
        }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    private func shouldGotoNextScreen() ->(Bool,NotificationModel?){
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadNotification
        tabBar.shouldLoadNotification = false
        return (result,tabBar.noticeToBeLoaded)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        let result = shouldGotoNextScreen()
        if result.0 {
            performSegue(withIdentifier: "showDetailedNotificationWithDesc", sender: result.1)
        }else {
            
            if user?.currentTypeOfUser == .guardian{
                Analytics.logEvent("P_homePage_notice", parameters: nil)
                
            }
            if noticeDtailedOrCompose == true{
              noticeDtailedOrCompose = false
            }
            else{
              FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_notice")
            }
            //if shouldCallWebService() == true {
            getUserDataFromDB()
            clearDataForThisPage()
            alterHeader()
            makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: isViewByPhoto)
            //}
        }
       
     
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        noticeListingTable.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    //MARK:- Header
    /**
     Function to update the header accoding to the screen we are in .
     */
    private func alterHeader(){
        
        header!.delegate = self
        if user?.currentTypeOfUser == .guardian{
            header.setUpHeaderForRootScreensWithNameOfScreen(name: ConstantsInUI.notificationScreenHeading, withProfileImageUrl: user!.getSelectedEmployeeOrStudentProfileImageUrl())
        }
        else{
            if user?.currentTypeOfUser == .employee {
                if user!.switchAs == ""{
                    header.setUPHeaderForEmployee(name: ConstantsInUI.notificationScreenHeading)
                }
                else if user!.switchAs == "Guardian"{
                    header.setUPHeaderForMultiRole(name: ConstantsInUI.notificationScreenHeading)
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                header.setupHeaderForCorporateManager(name: (user?.getStudentCurrentlySelectedSchool()?.schoolName)!)
                
            }
        }
    }
    
    
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        self.tabBarController?.slideMenuController()?.openLeft()
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
        
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        if schoolId != user?.schools![(user?.currentSchoolSelected)!].schoolId {
        clearDataForThisPage()
        self.makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: self.isViewByPhoto)
        }
    }
    
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        clearDataForThisPage()
        self.makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: isViewByPhoto)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
    }
    
    private func clearDataForThisPage(){
        page = 0
        filterednotificationsEntryListingDictionary.removeAll()
        notificationsEntryListingDictionary.removeAll()
        dateKeysArray.removeAll()
        alterHeader()
        noticeListingTable.reloadData()
    }
    
    /**
     Function to get user data from DB
     */
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    //MARK: tableView methods
    
    private func initializeTableView(){
        noticeListingTable.register(UINib(nibName: "CommonListingcell", bundle: nil), forCellReuseIdentifier: "CommonListingcell")
        noticeListingTable.estimatedRowHeight = 129.0
        noticeListingTable.rowHeight = UITableViewAutomaticDimension
        
        let bottomRefreshControl                         = UIRefreshControl()
        bottomRefreshControl.triggerVerticalOffset       = 100.0
        bottomRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromBottomRefresh), for: .valueChanged)
        self.noticeListingTable.bottomRefreshControl = bottomRefreshControl
        
        let topRefreshControl                   = UIRefreshControl()
        topRefreshControl.triggerVerticalOffset = 100.0
        topRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromTopRefreshControl), for: .valueChanged)
        noticeListingTable.addSubview(topRefreshControl)
        
    }
    
    //MARK:- TAbleview delegate and data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let key = dateKeysArray[section]
        return (filterednotificationsEntryListingDictionary[key]?.count)!
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return filterednotificationsEntryListingDictionary.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell         = tableView.dequeueReusableCell(withIdentifier: "CommonListingcell") as! CommonListingcell
        let key          = dateKeysArray[indexPath.section]
        
        let notification = filterednotificationsEntryListingDictionary[key]![indexPath.row]
        
        /*if notification.pdfArray.count > 0{
            cell.downLoadAttachmentButton.isHidden = false
            cell.downLoadButtonHeight.constant = 22.0
            cell.downLoadAttachmentButton.setTitle(notification.pdfArray[0].name, forState: .Normal)
            cell.downLoadAttachmentButton.name = notification.pdfArray[0].name
            cell.downLoadAttachmentButton.downloadUrlString = notification.pdfArray[0].url
            cell.downLoadAttachmentButton.addTarget(self, action: #selector(loadPdf(_:)), forControlEvents: .TouchUpInside)
            
        }else {
            cell.downLoadAttachmentButton.isHidden = true
            cell.downLoadButtonHeight.constant = 0.0
        }*/
        cell.downLoadAttachmentButton.isHidden = true
        cell.downLoadButtonHeight.constant = 0.0

        
        setDateVisibilityForCell(cell: cell, forindex: indexPath as NSIndexPath, andNotiFication: notification, andArrayCount:filterednotificationsEntryListingDictionary[key]!.count )
        cell.setEntryRead(isread: notification.isread)
        cell.contentLabel.text = nil
        cell.titleLabel.text = notification.title
        cell.designatedUpdatedUponLabel.text = notification.addedbyName
        if notification.hasAttachment{
            cell.setDiaryCellForAttachment()
        }else {
            cell.setDiaryCellForNoAttachments()
        }
        
        cell.setcolorforupcoming(isUpcoming: notification.isUpComing)
        setCellAttachment(cell: cell, withNotiFication: notification)
        setCellForRemainder(cell: cell, withNotiFication: notification)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0 {
            return nil
        }else {
            let headerView = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.width, height:10))
            headerView.backgroundColor = UIColor.clear
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0{
            return 0.0
        }else {
            return 10
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let key          = dateKeysArray[indexPath.section]
        
        let notification = filterednotificationsEntryListingDictionary[key]![indexPath.row]
        performSegue(withIdentifier: "showDetailedNotificationWithDesc", sender: notification)
    }
    /**
     Function to set visibilty of reminader
     
     - parameter cell:         notification cell
     - parameter notification: notification
     */
    private func setCellForRemainder(cell:CommonListingcell,withNotiFication notification:NotificationModel){
        if notification.hasReminder{
            cell.reminderButton.isHidden = true
            cell.reminderButtonWidth.constant = 0
            cell.reminderButton.genericThing = notification
            
            
            cell.reminderButton.addTarget(self, action: #selector(self.addreminder), for: .touchUpInside)
            
        }else {
            cell.reminderButton.isHidden = true
            cell.reminderButton.genericThing = nil
        }
    }
    /**
     Function to show and hide attacment icon
     
     - parameter cell:         notification cell
     - parameter notification: notification
     */
    private func setCellAttachment(cell:CommonListingcell,withNotiFication notification:NotificationModel){
        
        cell.removeAllImagesFromImageContainerView()
        if notification.hasAttachment {
            var imageArray = [UIImageView]()
            for urlString in notification.imageArray{
                let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
                weak var weakImageVC  = imagv
                
                ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                    if weakImageVC != nil { weakImageVC!.image = image }
                    
                    }, onFailure: { (error) in
                        
                })
                imagv.contentMode = .scaleAspectFit
                imageArray.append(imagv)
                
            }
            
            for urlString in notification.pdfArray{
                var image = UIImage()
                if urlString.type == "PDF" {
                    image = UIImage(named: "pdfIcon")!
                }
                if urlString.type == "DOC" || urlString.type == "DOCX"  {
                    image = UIImage(named: "wordIcon")!
                }
                let imagv = CustomImageView(image: image)
                
                weak var weakImageVc  = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                    if weakImageVc != nil { weakImageVc!.image = image }
                    }, onFailure: { (error) in
                        
                })
                imagv.contentMode = .scaleAspectFit
                imagv.url = urlString.url
                imagv.name = urlString.name
                imageArray.append(imagv)
                
            }
            
         cell.imageContainerHeight.constant = cell.imageContainerView.addImagesToMeFromArray(imageArray: imageArray)
        }else {
          cell.imageContainerHeight.constant = 0
        }
    }
    
    /**
     Function to set the visiblity of data view and fill it with data
     
     - parameter cell:         cell
     - parameter indexPath:    indexpath for cell
     - parameter notification: notification
     */
    private func setDateVisibilityForCell(cell:CommonListingcell, forindex indexPath:NSIndexPath, andNotiFication notification:NotificationModel ,andArrayCount count:Int){
        
        if indexPath.row == 0 {
            cell.dateView.isHidden = false
            cell.dayLabel.text = notification.dayString
            cell.monthAndYearLabel.text = notification.monthString + " " + notification.yearString
            if notification.isUpComing{
                cell.upComingLabel.text = ConstantsInUI.upComing
            }else {
                cell.upComingLabel.text = ConstantsInUI.past
            }
        }else {
            cell.dateView.isHidden    = true
            
        }
        
        cell.topLineView.isHidden = false
        cell.bottomLineView.isHidden = false
        
        if indexPath.row == 0 {
            cell.topLineView.isHidden = true
            if count == 1 {
                cell.bottomLineView.isHidden = true
            }
        } else {
            cell.topLineView.isHidden = false
            cell.bottomLineView.isHidden = false
            if indexPath.row+1 == count {
                cell.bottomLineView.isHidden = true
            }
        }

    }
    
    //MARK: Web service 
    
    @objc private func loadWebServiceFromBottomRefresh(){
        noticeListingTable.bottomRefreshControl!.endRefreshing()
        self.makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: isViewByPhoto)
    }
    @objc private func loadWebServiceFromTopRefreshControl(refreshControl:UIRefreshControl){
        refreshControl.endRefreshing()
        clearDataForThisPage()
        self.makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: isViewByPhoto)
    }
    
    private func makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto isViewedByPhoto:Bool){
        var studentoruserRoleid:String = ""
        var selectedUser:String = ""
        var corporateId = ""
       if user?.currentTypeOfUser == .guardian{
            studentoruserRoleid = (user?.getSelectedEmployeeOrStudentId())!
            selectedUser = "Guardian"
        }
       else if user?.currentTypeOfUser == .employee{
           studentoruserRoleid = (user?.currentUserRoleId)!
           selectedUser = "Employee"
        }
       else{
        selectedUser = "CorporateManager"
        corporateId = String(describing: user?.corporateId)
        schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        studentoruserRoleid = (user?.currentUserRoleId)!
        }
        
         weak var weakSelf = self
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getNoticesForUser(employeeORstudentId: (studentoruserRoleid),SelectedUser: selectedUser, forPage: String(page), shouldBeViewedByPhoto: isViewedByPhoto,corporateId: corporateId,schoolId: String(schoolId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            let res = NotificationModel.makeDictionaryOfNotificationsFromDictionary(dictionary: result)
            
            if (res.1 != nil){
                AlertController.showToastAlertWithMessage(message: (res.1?.localizedDescription)!, stateOfMessage: .failure)
            }else {
                weakSelf!.addEntriesToDictionaryAndReload(dictionary: res.0)
            }
            
            
            }, onFailure: { (error) in
             hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }) { (progress) in
            
          }
        
        
    }
    
    @objc private func addreminder(){
    
    }
    
    //MARK:- Array
    
    private func addEntriesToDictionaryAndReload(dictionary:[String:[NotificationModel]]){
        
        
        filterednotificationsEntryListingDictionary.addEntriesFromDictionary(otherDictionary: dictionary)
        notificationsEntryListingDictionary = filterednotificationsEntryListingDictionary
        dateKeysArray = [String](filterednotificationsEntryListingDictionary.keys)
        dateKeysArray.sortDateStringArrayBasedOnDateWithDateFormat(.ddMMyyyyWithHypen)
        page = page+20
        noticeListingTable.reloadData()
    }
    
    
    
    //MARK: IB actions 
    
    
    @IBAction func changeListData(_ sender: UIButton) {
        isViewByPhoto = !isViewByPhoto
        clearDataForThisPage()
        if isViewByPhoto{
            sender.setTitle(ConstantsInUI.basicView, for: .normal)
            makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: true)
        }else {
            sender.setTitle(ConstantsInUI.viewByPhotos, for: .normal)
            makeWebApiCalltoGetNotificationsWithPagination(shouldBeViewedByPhoto: false)
        }
        
        //iterateArraytoShowListWithPhotos(isViewByPhoto)
      
    }
    
   //MARK: Array Iterations
    /**
     function to iterate the array to find the images with attachments
     
     - parameter shouldShowPhotos: bool showing whether the photos should be shown
     */
    private func iterateArraytoShowListWithPhotos(shouldShowPhotos:Bool){
       filterednotificationsEntryListingDictionary.removeAll()
        
        if shouldShowPhotos {
            for (key, value) in notificationsEntryListingDictionary{
                
                let filteredAray = value.filter{$0.hasAttachment}
                self.filterednotificationsEntryListingDictionary[key] = filteredAray
            }
        }else {
            filterednotificationsEntryListingDictionary = notificationsEntryListingDictionary
        }
        
    }
    
    //MARK:- NAvigation :- 
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueForLoadingNotificationDetailed{
         self.noticeDtailedOrCompose = true
         let resp = segue.destination as! DetailedNotificationWithDesc
        let noti = sender as! NotificationModel
        resp.notification = noti
        FireBaseKeys.FirebaseKeysRuleOne(value: "notice_noticeView")
        }
        else if segue.identifier == segueConstants.segueForLoadingComposeNotice {
            let composeNotice        = segue.destination as! ComposeNotification
            composeNotice.userId     = (user?.currentUserRoleId)!
            self.noticeDtailedOrCompose = true
            Analytics.logEvent("TP_Notice_compose", parameters: nil)
            }
        }
    
   //MARK:- misc
    /**
     private function to find out if call of web service is needed
     
     - returns: returns whether web service call is nessesary
     */
    private func shouldCallWebService () ->Bool{
        var shoulCallWebSevice = false
        
        if user != nil {
            // checcking if we have changed users student or employee some where else.
            if  let newUer = DBController().getUserFromDB(){
                if user?.currentOptionSelected != newUer.currentOptionSelected{
                    shoulCallWebSevice = true
                }else  if page == 0 {
                    shoulCallWebSevice = true
                }else if newUer.Students![newUer.currentOptionSelected].profilePic != user!.Students![newUer.currentOptionSelected].profilePic {
                    shoulCallWebSevice = true
                }
            }
        }
        return shoulCallWebSevice
    }
    
 @objc private func addReminder(sender:ReminderButton){
    
        let notifi = sender.genericThing as! NotificationModel
        let dateString = String(stringInterpolation: "\(notifi.dayString)-\(notifi.monthString)-\(notifi.yearString)")
        let date =  DateApi.convertThisStringToDate(dateString: dateString, formatNeeded: .ddMMyyyyWithHypen)
        EventController().addReminderWithTitle(title: notifi.title, Ondate: date, onSuccess: { (result) in
            
            }) { (error) in
                
    }
    }
    
    //MARK: - load Pdf 
    
    @objc private func loadPdf(sender:UIButton){
        
        let button = sender as! ButtonForDownLoadImg
        pdfViewer.showPdfInViewController(vc: self, pdfHavingUrl: button.downloadUrlString, withName: button.name)
    }
    
    @IBAction func showComposeNoticeScreen() {
        self.performSegue(withIdentifier: segueConstants.segueForLoadingComposeNotice, sender: nil)
    }
}

