//
//  progresscardVC.swift
//  parentEye
//
//  Created by scientia on 06/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import GlyuckDataGrid

class progresscardVC:UIViewController,DataGridViewDataSource, DataGridViewDelegate,SpreadSheetCellDelegate,UITextFieldDelegate,CommonHeaderProtocol {
    
    @IBOutlet var progresscardDatagridView: DataGridView!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var evaluvationCategoryTextField: UITextField!
    @IBOutlet var evaluvationGroupTextField: UITextField!
    @IBOutlet var evaluvationGroupTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var evaluvationCategoryTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var lblNoResult: UILabel!
    
    var user:userModel?
    var profilePic = ""
    var evaluvationGroupList = [evaluvationGroup]()
    var displayProgressCardArray = Array<Array<Array<Array<String>>>>()
    var tempDisplayProgressCardArray = Array<Array<Array<Array<String>>>>()
    var displayPCArrayRowPosition = 0
    var displayPCArrayColPosition = 0
    var displayPCArrayGroupPosition = 0
    var displayPCArrayCatrgoryPosition = 0
    var rowCount = 0
    var mainSubCount = 0
    var studentId  = ""
    let examMarkList = ExamMarklist()
    var MarkList = [String] ()
    var mainSubCountArray = [Int]()
    var mark = [String]()
    var PCinner = false
    var currentEvaluvationGroup:Int = 0
    var currentSection = 0
    var datagridViewLoad = false
    let evaluvationGroupPicker = UIPickerView()
    let evaluvationCategoryPicker = UIPickerView()
    let studentAbsentcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let studentNotAssignedColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    var showOnlyProgressCard:Bool = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        evaluvationGroupTextField.delegate = self
        evaluvationCategoryTextField.delegate = self
        setUpHeader()
        setupUI()
        setinitialIndexPathArrayPosition()
        getProgressReport()
    }
    
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }

    private func setUpHeader(){
        
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), forState: UIControlState.Normal)
        header.profileChangeButton.hidden = true
        header.profileImageRightSeparator.hidden = true
        header.downArrowButton.hidden = true
        header.multiSwitchButton.hidden = true
        header.rightLabel.hidden = true
        header.screenShowingLabel.text = ConstantsInUI.progressCard
        
    }
    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    func showSwitchSibling(fromButton:UIButton) {
        
    }
    
    private func setupUI() {
        lblNoResult.hidden = true
        evaluvationGroupList.removeAll()
    }
    
    
    func getProgressReport(){
        
        if user?.currentTypeOfUser == .guardian{
            studentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        hudControllerClass.showNormalHudToViewController(self)
        weak var weakSelf = self
        
        WebServiceApi.getProgressReport(studentId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(weakSelf!)
            self.evaluvationGroupList.removeAll()
            //self.progressCardHeaderArray.removeAll()
            self.populateGroupandCategoryEvaluvation(result)
            self.displayPCArrayRowPosition = 0
            if let studentProgressCardResult = result["studentProgressCard"] as? [String:[String:AnyObject]] {
                for(_,evaluvation_Group_List) in studentProgressCardResult {
                    
                    if let evaluvationGroupPosition = evaluvation_Group_List["position"] as? Int {
                        self.displayPCArrayGroupPosition = evaluvationGroupPosition
                    }
                    
                    for(evaluvation_Category,evaluvation_Category_list) in evaluvation_Group_List {
                        if evaluvation_Category != "position" {
                        if let evaluvationCategoryPosition = evaluvation_Category_list["position"] as? Int {
                            self.displayPCArrayCatrgoryPosition = evaluvationCategoryPosition
                        }
                        
                        if let headerArray = evaluvation_Category_list["header"] as? NSArray {
                                for _ in headerArray {
                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = "Bold"
                                }
                            
                        }
                            
                        var examSubjectsList = [examSubject]()
                        if let evaluvation_Subject_List = evaluvation_Category_list as? [String:AnyObject] {
                            for(evaluvationSubject,_) in evaluvation_Subject_List {
                                let examSubjectObject = examSubject()
                                examSubjectObject.setExamSubjctName(evaluvationSubject)
                               if let subList = evaluvation_Subject_List[evaluvationSubject] as? [String:AnyObject] {
                                print("exammm")
                                
                                    if let position = subList["position"] as? Int{
                                                self.displayPCArrayColPosition = 0
                                               self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = evaluvationSubject
                                        if self.showOnlyProgressCard {
                                            self.tempDisplayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = "Header"
                                            
                                        }
                                        
                                        
                                                self.displayPCArrayRowPosition = position
                                                
                                            }
                                            if let val = subList["value"] as? [String:AnyObject] {
                                                self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = evaluvationSubject
                                                
                                                examSubjectObject.setBranched(true)
                                                
                                                
                                                self.rowCount = self.rowCount + 1
                                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                                
                                                self.mainSubCount = self.mainSubCount + 1
                                                var examSubjects = [examSubject]()
                                                examSubjects = self.getInnerSubjectForProgressCard(val)
                                                examSubjectObject.setExamSubjectList(examSubjects)
                                            }
                                
                                            if let subList = subList["value"] as? NSArray {
                                                
                                                examSubjectObject.setBranched(false)
                                                self.displayPCArrayColPosition = 0
                                                self.rowCount = self.rowCount + 1
                                                self.mainSubCount = self.mainSubCount + 1
                                                self.mark.removeAll()
                                                self.displayPCArrayColPosition = 0
                                                
                                                //self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = evaluvationSubject
                                                
                                                for pos in subList{
                                                    self.mark.append(pos as! String)
                                                    examSubjectObject.examSubjectMarkList.append(pos as! String)
                                                    
                                                    self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                                                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][self.displayPCArrayRowPosition][self.displayPCArrayColPosition]   = pos as! String
                                                    
                                                    
                                                }
                                                self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                                                
                                                
                                            }
                                        
                                                                        
                                }
                                examSubjectsList.append(examSubjectObject)
                                self.mainSubCountArray.append(self.mainSubCount-1)
                                self.mainSubCount = 0
                            }
                            self.examMarkList.setExamSubjectList(examSubjectsList)
                            //self.examMarkList.setexamNames(self.progressCardArray)
                        }
                            if self.evaluvationGroupList.count > 0{
                                for pos in self.evaluvationGroupList {
                                    if pos.groupPosition == self.displayPCArrayGroupPosition {
                                        for sec in pos.evaluvationCategoryList {
                                            if sec.categoryPosition == self.displayPCArrayCatrgoryPosition {
                                                sec.categoryRowCount = self.rowCount
                                            }
                                        }
                                    }
                                }
                            }
                            print("row count=\(self.rowCount)")
                            self.rowCount = 0
                        }

                     }
                        
                        
              }
            }
            if self.evaluvationGroupList.count >= 1 {
                self.evaluvationGroupTextFieldHeight.constant = 30
                self.evaluvationCategoryTextFieldHeight.constant = 30
                self.lblNoResult.hidden = true
                self.loadPickerView()
            }
            else{
                self.evaluvationGroupTextFieldHeight.constant = 0
                self.evaluvationCategoryTextFieldHeight.constant = 0
                self.lblNoResult.hidden = false
            }

           //self.progresscardView.reloadData()
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(weakSelf!)
            AlertController.showToastAlertWithMessage(error.localizedDescription, stateOfMessage: .failure)
            debugPrint(error.localizedDescription)
            self.evaluvationGroupTextFieldHeight.constant = 0
            self.evaluvationCategoryTextFieldHeight.constant = 0
        }) { (progress) in
            
        }
    }
    
    func populateGroupandCategoryEvaluvation(result:Dictionary<String, AnyObject>) {
        
        if let studentProgressCardResult = result["studentProgressCard"] as? [String:[String:AnyObject]] {
            
            for(evaluvation_Group,evaluvation_Group_List) in studentProgressCardResult {
                print(evaluvation_Group)
                if let groupPosition = evaluvation_Group_List["position"] as? Int {
                    if groupPosition == 0 {
                        self.currentEvaluvationGroup = groupPosition
                    }
                    let evaluvation_groupObject = evaluvationGroup()
                    evaluvation_groupObject.evaluvationGroupName = evaluvation_Group
                    evaluvation_groupObject.groupPosition = groupPosition
                    var evaluvationCategoryList = [evaluvationCategory]()
                       for(evaluvation_Category,evaluvation_category_list) in evaluvation_Group_List {
                        if evaluvation_Category != "position" {
                         
                         let evaluvationCategoryObject = evaluvationCategory()
                            if let categoryPosition = evaluvation_category_list["position"] as? Int {
                              evaluvationCategoryObject.categoryName = evaluvation_Category
                              evaluvationCategoryObject.categoryPosition = categoryPosition
                            }
                           var categoryHeaderList = [String]()
                           if let headerArray = evaluvation_category_list["header"] as? NSArray {
                              for header in headerArray {
                                 categoryHeaderList.append(header as! String)
                               }
                               evaluvationCategoryObject.categoryHeaderArray = categoryHeaderList
                           }
                            if let categorySubjectList = evaluvation_category_list as? [String:AnyObject] {
                                var evaluvationSubjectList = [evaluvationSubject]()
                                for(subject,subList) in categorySubjectList{
                                
                                    if subject != "position" && subject != "header"{
                                       let evaluvationSubjectObject = evaluvationSubject()
                                        if let subjectPosition = subList["position"] as? Int {
                                            evaluvationSubjectObject.subjectName = subject
                                            evaluvationSubjectObject.subjectPosition = subjectPosition
                                        }
                                         evaluvationSubjectList.append(evaluvationSubjectObject)
                                    }
                                    evaluvationCategoryObject.categorySubjectArray = evaluvationSubjectList
                                }
                            }
                            
                        
                         evaluvationCategoryList.append(evaluvationCategoryObject)
                         
                      }
                     evaluvation_groupObject.evaluvationCategoryList = evaluvationCategoryList
                        
                    }
                    evaluvationGroupList.append(evaluvation_groupObject)
                }
               
                
            }
            
            
            
            
        }
        else{
            
        }
    }
    
    func setinitialIndexPathArrayPosition(){
        
        displayProgressCardArray.removeAll()
        for _ in 0..<20 {
             //displayProgressCardArray.append(Array(count:20, repeatedValue:String()))
            displayProgressCardArray.append(Array(count:20,repeatedValue:Array(count:100, repeatedValue:Array(count:20, repeatedValue:String()))))
        }
        if user?.currentTypeOfUser == .guardian{
           if user?.Students![(user?.currentOptionSelected)!].showOnlyProgressCard != 0 {
              showOnlyProgressCard = true
            }
            else{
              showOnlyProgressCard = false
            }
        }
        if showOnlyProgressCard {
            tempDisplayProgressCardArray.removeAll()
            for _ in 0..<20 {
                tempDisplayProgressCardArray.append(Array(count:20,repeatedValue:Array(count:100, repeatedValue:Array(count:20, repeatedValue:String()))))
            }
        }
        
        
        
        for group in 0..<20{
            for category in 0..<20{
                for row in 0..<100 {
                    for col in 0..<20 {
                       displayProgressCardArray[group][category][row][col] = ""
                          if showOnlyProgressCard {
                            tempDisplayProgressCardArray[group][category][row][col] = ""
                          }
                    }
                }
               
            }
        }
    }
    
    func getInnerSubjectForProgressCard(subList:[String:AnyObject]) -> [examSubject] {
        PCinner = true
        var examSubjects = [examSubject]()
        var examInnerSubjects = [examSubject]()
        
        for(key,_) in subList{
            let examSubjectObject = examSubject()
            examSubjectObject.setExamSubjctName(key)
            if let subSubList = subList[key] as? [String:AnyObject] {
                if let position = subSubList["position"] as? Int{
                    self.displayPCArrayColPosition = 0
                    self.displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][position][self.displayPCArrayColPosition]   = key
                    self.displayPCArrayRowPosition = position
                    
                }
                if let val = subSubList["value"] as? [String:AnyObject] {
                    
                    examSubjectObject.setBranched(true)
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    examInnerSubjects = getInnerSubjectForProgressCard(val)
                    examSubjectObject.setExamSubjectList(examInnerSubjects)
                }
                
                if let subSubList = subSubList["value"] as? NSArray {
                    examSubjectObject.setBranched(false)
                    self.rowCount = self.rowCount + 1
                    self.mainSubCount = self.mainSubCount + 1
                    self.mark.removeAll()
                    displayPCArrayColPosition = 0
                    
                    displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][displayPCArrayRowPosition][displayPCArrayColPosition]   = "      "+key
                    
                    for pos in subSubList{
                        self.mark.append(pos as! String)
                        examSubjectObject.examSubjectMarkList.append(pos as! String)
                        displayPCArrayColPosition = displayPCArrayColPosition + 1
                        displayProgressCardArray[self.displayPCArrayGroupPosition][self.displayPCArrayCatrgoryPosition][displayPCArrayRowPosition][displayPCArrayColPosition]   = pos as! String
                        
                    }
                    displayPCArrayRowPosition = displayPCArrayRowPosition + 1
                    
                }
            }
            examSubjects.append(examSubjectObject)
        }
        return examSubjects
    }

    
    func loadPickerView() {
        
        evaluvationGroupPicker.tag = 0
        evaluvationGroupPicker.delegate = self
        evaluvationGroupPicker.dataSource  = self
        evaluvationGroupTextField.inputView = evaluvationGroupPicker
        evaluvationGroupPicker.backgroundColor = UIColor.clearColor()
        if evaluvationGroupList.count == 1 {
         evaluvationGroupTextField.userInteractionEnabled = false
        }
        else{
         evaluvationGroupTextField.userInteractionEnabled = true
        }
        self.evaluvationGroupPicker.reloadAllComponents()
        
        evaluvationCategoryPicker.tag = 1
        evaluvationCategoryPicker.delegate = self
        evaluvationCategoryPicker.dataSource = self
        evaluvationCategoryTextField.inputView = evaluvationCategoryPicker
        self.evaluvationCategoryPicker.reloadAllComponents()
        currentSection = 0
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == 0 {
                    evaluvationGroupTextField.text = pos.evaluvationGroupName
                    if pos.evaluvationCategoryList.count == 1{
                       evaluvationCategoryTextField.userInteractionEnabled = false
                    }
                    else{
                        evaluvationCategoryTextField.userInteractionEnabled = true
                    }
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                           evaluvationCategoryTextField.text = sec.categoryName
                        }
                    }

                    
                    
                }
            }
        }
        //progresscardDatagridView.rowHeaderWidth = 60
        progresscardDatagridView.columnHeaderHeight = 40
        progresscardDatagridView.rowHeight = 44
        self.currentSection = 0
        progresscardDatagridView.dataSource = self
        progresscardDatagridView.delegate = self
        progresscardDatagridView.registerNib(UINib(nibName: "SpreadSheetCell", bundle: nil), forCellWithReuseIdentifier: "DataCell")

        progresscardDatagridView.reloadData()
        
    }
}

extension progresscardVC: UIPickerViewDataSource {
    
    func numberOfComponentsInPickerView(colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0  {
            if self.evaluvationGroupList.count > 0 {
                
                return evaluvationGroupList.count
            }
            
        }
        if pickerView.tag == 1 {
            if self.evaluvationGroupList.count > 0 {
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        return pos.evaluvationCategoryList.count
                    }
                }
            }
        }
        
        return 0
    }
}

extension progresscardVC: UIPickerViewDelegate {
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if pickerView.tag == 0 {
         if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == row {
                    print(pos.evaluvationGroupName)
                    return pos.evaluvationGroupName
                }
            }
         }
            
        }
        
        if pickerView.tag == 1 {
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition == row {
                              return sec.categoryName
                            }
                        }
                        
                    }
                }
            }
        }
        return ""
    }
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
           currentEvaluvationGroup = row
           currentSection = 0
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == row {
                        evaluvationGroupTextField.text = pos.evaluvationGroupName
                        if pos.evaluvationCategoryList.count == 1 {
                            evaluvationCategoryTextField.userInteractionEnabled = false
                        }
                        else{
                            evaluvationCategoryTextField.userInteractionEnabled = true
                        }
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition ==  currentSection{
                              evaluvationCategoryTextField.text = sec.categoryName
                            }
                        }
                        
                        
                    }
                }
            }
           evaluvationCategoryPicker.selectRow(0, inComponent: 0, animated: false)
           progresscardDatagridView.reloadData()
        }
        if pickerView.tag == 1 {
            currentSection = row
            if evaluvationGroupList.count > 0{
                for pos in evaluvationGroupList {
                    if pos.groupPosition == currentEvaluvationGroup {
                        for sec in pos.evaluvationCategoryList {
                            if sec.categoryPosition ==  row{
                                evaluvationCategoryTextField.text = sec.categoryName
                            }
                        }
                        
                        
                    }
                }
            }
            progresscardDatagridView.reloadData()
        }
        
    }
    
}

extension progresscardVC{
    
    enum Colors {
        static let border = UIColor.lightGrayColor()
        static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    enum Constants {
        static let numberOfRows = 6
        static let numberOfLetters = 8
        static let charCodeForA = 65
    }
    
       
    static override func initialize() {
        super.initialize()
        let dataGridAppearance = DataGridView.glyuck_appearanceWhenContainedIn(self)
        dataGridAppearance.row1BackgroundColor = nil
        dataGridAppearance.row2BackgroundColor = nil
        
        let columnHeaderAppearance = DataGridViewColumnHeaderCell.glyuck_appearanceWhenContainedIn(self)
        columnHeaderAppearance.backgroundColor = Colors.headerBackground
        columnHeaderAppearance.borderTopWidth = 1 / UIScreen.mainScreen().scale
        columnHeaderAppearance.borderBottomWidth = 1 / UIScreen.mainScreen().scale
        columnHeaderAppearance.borderRightWidth = 1 / UIScreen.mainScreen().scale
        columnHeaderAppearance.borderTopColor = Colors.border
        columnHeaderAppearance.borderBottomColor = Colors.border
        columnHeaderAppearance.borderRightColor = Colors.border
        
        let cellAppearance = DataGridViewContentCell.glyuck_appearanceWhenContainedIn(self)
        cellAppearance.borderRightWidth = 1 / UIScreen.mainScreen().scale
        cellAppearance.borderRightColor = UIColor(white: 0.73, alpha: 1)
        cellAppearance.borderBottomWidth = 1 / UIScreen.mainScreen().scale
        cellAppearance.borderBottomColor = UIColor(white: 0.73, alpha: 1)
        
        
        columnHeaderAppearance.backgroundColor = UIColor(white: 0.95, alpha: 1)
        let labelAppearance = UILabel.glyuck_appearanceWhenContainedIn(self)
        if #available(iOS 8.2, *) {
            labelAppearance.appearanceFont = UIFont.systemFontOfSize(12, weight: UIFontWeightLight)
        } else {
            // Fallback on earlier versions
        }
        labelAppearance.appearanceTextAlignment = .Center
    }
}
// MARK: DataGridViewDataSource

extension progresscardVC{
    
    func numberOfColumnsInDataGridView(dataGridView: DataGridView) -> Int {
        //if datagridViewLoad == true {
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                           return sec.categoryHeaderArray.count
                        }
                    }
                }
            }//}
            
        }
        return 0
        
    }
    
    func numberOfRowsInDataGridView(dataGridView: DataGridView) -> Int {
        //if datagridViewLoad == true {
        if self.evaluvationGroupList.count > 0{
            for pos in self.evaluvationGroupList {
                if pos.groupPosition == self.currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                            return sec.categoryRowCount
                        }
                    }
                }//}
             }
            
        }
        return 0
        
    }
    
    func dataGridView(dataGridView: DataGridView, titleForHeaderForColumn column: Int) -> String {
       // if datagridViewLoad == true {
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                          return sec.categoryHeaderArray[column]
                        }
                    } // }
                }
            }
            
        }
        return ""
    }
    
    func dataGridView(dataGridView: DataGridView, titleForHeaderForRow row: Int) -> String {
        
        
//        if evaluvationGroupList.count > 0{
//            for pos in evaluvationGroupList {
//                if pos.groupPosition == currentEvaluvationGroup {
//                    for sec in pos.evaluvationCategoryList {
//                        if sec.categoryPosition == currentSection {
//                            for sub in sec.categorySubjectArray {
//                                if sub.subjectPosition == row {
//                                    return sub.subjectName
//                                }
//                            }
//                        }
//                    } // }
//                }
//            }
//            
//        }
        return ""
    }
    
    func dataGridView(dataGridView: DataGridView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        
          let cell = dataGridView.dequeueReusableCellWithReuseIdentifier("DataCell", forIndexPath: indexPath) as! SpreadSheetCell
        datagridViewLoad = false
        cell.delegate = self
        cell.border.bottomWidth = 1 / UIScreen.mainScreen().scale
        cell.border.rightWidth = 1 / UIScreen.mainScreen().scale
        cell.border.bottomColor = Colors.border
        cell.border.rightColor = Colors.border
        
        if indexPath.dataGridColumn == 0 {
            cell.lblClassName.contentHorizontalAlignment = .Left
            
            
        }
        else{
            cell.lblClassName.contentHorizontalAlignment = .Center
            
        }
        
        if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn] != "Bold"{
            
            cell.lblClassName.setTitle(displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn]
                ,forState: .Normal)
        }
        else{
            cell.lblClassName.setTitle("",forState: .Normal)
            
        }
        
        if evaluvationGroupList.count > 0{
            for pos in evaluvationGroupList {
                if pos.groupPosition == currentEvaluvationGroup {
                    for sec in pos.evaluvationCategoryList {
                        if sec.categoryPosition == currentSection {
                            
                    
                    
                    if indexPath.dataGridColumn < pos.evaluvationCategoryList[currentSection].categoryHeaderArray.count{
                        print(self.currentSection)
                        if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn + 1] == "Bold" || displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn] == "Bold"{
                            //cell.backgroundColor = Colors.headerBackground
                            //cell.lblClassName.titleLabel!.font =  UIFont(name: "Roboto-Regular", size: 12)
                            
                        }
                        else{
                            /*if #available(iOS 8.2, *) {
                             cell.lblClassName.titleLabel!.font = UIFont.systemFontOfSize(12, weight: UIFontWeightLight)
                             } else {
                             // Fallback on earlier versions
                             }*/
                            
                        }
                    }
                   }
                    }
                }
            }
        }
        
        if showOnlyProgressCard {
            if self.tempDisplayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn] == "Header" {
                cell.backgroundColor = Colors.headerBackground
            }
        }

        
        if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn] == "AB"{
            cell.lblClassName.setTitleColor(studentAbsentcolur, forState: .Normal)
        }
        else if displayProgressCardArray[currentEvaluvationGroup][currentSection][indexPath.dataGridRow][indexPath.dataGridColumn] == "NA"{
            cell.lblClassName.setTitleColor(studentNotAssignedColor, forState: .Normal)
        }
        else{
            cell.lblClassName.setTitleColor(UIColor.blackColor(), forState: .Normal)
        }

        
        return cell
    }
    
    func dataGridView(dataGridView: DataGridView, didSelectRow row: Int){
        
    }
    // MARK: DataGridViewDelegate
    
    func dataGridView(dataGridView: DataGridView, widthForColumn column: Int) -> CGFloat {
        
        return 150
    }
    
    func dataGridView(dataGridView: DataGridView, shouldSelectRow row: Int) -> Bool {
        return true
    }
    // MARK: SpreadSheetCellDelegate
    
    func spreadSheetCell(cell: SpreadSheetCell, didUpdateData data: String, atIndexPath indexPath: NSIndexPath) {
       
    }
    
}
