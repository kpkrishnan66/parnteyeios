//
//  SpreadSheetCell.swift
//  GlyuckDataGrid
//
//  Created by Vladimir Lyukov on 16/11/15.
//  Copyright © 2015 CocoaPods. All rights reserved.
//

import UIKit
import SpreadsheetView

class SpreadSheetCell: Cell {
    
    @IBOutlet var lblClassName: UILabel!
    
    let label = UILabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        label.frame = bounds
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.backgroundColor = .black
        label.font = UIFont(name:"Roboto-Light",size:13)
        label.textColor = .black
        label.textAlignment = .center
        label.numberOfLines = 2
        addSubview(label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
   var title = "" 
}



class subjectLabel: UILabel{

    var subjectSelectedRow: Int = 0
    var subjectSelectedColumn: Int = 0

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}


