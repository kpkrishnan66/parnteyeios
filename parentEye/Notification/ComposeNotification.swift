//
//  ComposeNotification.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 10/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import Firebase
import IQKeyboardManagerSwift

class ComposeNotification: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,attachmentimagelibraryProtocol,attachmentDeleteButtonPressedProtocol,UITableViewDataSource,UITableViewDelegate,UIDocumentPickerDelegate,UITextFieldDelegate,DiaryComposeSchoolListProtocol,UIPickerViewDelegate,UIPickerViewDataSource
{
    var user:userModel?
    var attachmentImages = [Int:UIImage]()
    var cameraImages = [Int:UIImage]()
    var execteCompletion:Bool = true
    var userId:String = ""
    var profilePic = ""
    var noticeAddDate = Foundation.Date()
    var noticeType:String = ""
    var addedById:String = ""
    var selectedTag = 0
    var activeSelection = false
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    lazy var uploadAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    lazy var copyuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var tempuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var schoolListArray = [corporateManagerSchoolModel]()
    lazy var schoolRecipientListArray = [corporateManagerSchoolModel]()
    var uploadedcount:Int = 0
    var toUploadAttachments = [Int:UIImage]()
    var deleteAttachment = false
    var cameraSelected = false
    var defaultSchoolDisplay:Bool = false
    var uploadAttachmentFailure:Bool = false
    var imageArray = [UIImageView]()
    var tagCount = 0
    var posCount = 0
    let forStaffOrForAllPicker = UIPickerView()
    var SendToNoticeArray = ["For All","For Staff"]
    
    
    
    @IBOutlet var attachmentviewContainerHeight: NSLayoutConstraint!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var addNoticeViewHeight: NSLayoutConstraint!
    @IBOutlet var mainView: UIView!
    @IBOutlet var attachmentViewContainer: ExpansiveView!
    @IBOutlet var addNoticeView: UIView!
    @IBOutlet var addAttachmentImage: UIImageView!
    @IBOutlet var DateView: UITextField!
    @IBOutlet var DescriptionView: UITextView!
    @IBOutlet var NotceNameView: UITextField!
    @IBOutlet var header: CommonHeader!
    @IBOutlet weak var attachmentDocumentTableView: UITableView!
    @IBOutlet weak var attachmentDocumentTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolNameTexField: UITextField!
    @IBOutlet weak var forStaffOrForAllTextfield: UITextField!
    @IBOutlet weak var schoolLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolAllButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolNameTexFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var sendToHeight: NSLayoutConstraint!
    @IBOutlet weak var forStaffOrForAllTextfieldHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setupUI()
        alterHeader()
        setupTable()
        attachmentViewContainer.delegate = self
        DateView.isUserInteractionEnabled = false
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.addAttachmentImageTapped))
        addAttachmentImage.isUserInteractionEnabled = true
        addAttachmentImage.addGestureRecognizer(tapGestureRecognizer)
        
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.composeNotice, withProfileImageUrl: profilePic)
        header.multiSwitchButton.isHidden = true
        header.delegate = self
        
    }
    private func setupUI() {
        
        NotceNameView.layer.borderWidth = CGFloat(1)
        DateView.layer.borderWidth = CGFloat(1)
        DescriptionView.layer.borderWidth = CGFloat(1)
        schoolNameTexField.layer.borderWidth = CGFloat(1)
        forStaffOrForAllTextfield.layer.borderWidth = CGFloat(1)
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        NotceNameView.layer.borderColor = color.cgColor
        DateView.layer.borderColor = color.cgColor
        DescriptionView.layer.borderColor = color.cgColor
        schoolNameTexField.layer.borderColor = color.cgColor
        forStaffOrForAllTextfield.layer.borderColor = color.cgColor
        
        addAttachmentImage.image = UIImage(named: "AddAttachmentIcon")
        DateView.text = DateApi.convertThisDateToString(noticeAddDate, formatNeeded: .ddMMyyyyWithHypen)
        
        /*Auto capital words*/
        NotceNameView.autocapitalizationType = UITextAutocapitalizationType.sentences
        DescriptionView.autocapitalizationType = UITextAutocapitalizationType.sentences
        
        changeVisibilityBasedOnRoles()
        
        if user?.currentTypeOfUser == .CorporateManager {
        schoolNameTexField.text = user?.schools![(user?.currentSchoolSelected)!].schoolName
        schoolNameTexField.delegate = self
        schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
            
        }
        
        forStaffOrForAllTextfield.inputView = forStaffOrForAllPicker
        forStaffOrForAllTextfield.text =  SendToNoticeArray[0]
        
        forStaffOrForAllPicker.delegate = self
        forStaffOrForAllPicker.dataSource = self
        forStaffOrForAllTextfield.inputView = forStaffOrForAllPicker
        forStaffOrForAllTextfield.text =  SendToNoticeArray[0]
        noticeType = SendToNoticeArray[0]
        forStaffOrForAllPicker.reloadAllComponents()
        
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
    }
    
    func changeVisibilityBasedOnRoles() {
        
        if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
            addNoticeViewHeight.constant = 556
            schoolLabelHeight.constant = 0
            schoolNameTexFieldHeight.constant = 0
            schoolAllButtonHeight.constant = 0
        }
        else if user?.currentTypeOfUser == .CorporateManager {
            addNoticeViewHeight.constant = 616
            schoolLabelHeight.constant = 20
            schoolNameTexFieldHeight.constant = 40
            schoolAllButtonHeight.constant = 30
            forStaffOrForAllTextfieldHeight.constant = 40
            sendToHeight.constant = 20
        }
        else{
            addNoticeViewHeight.constant = 493
            schoolLabelHeight.constant = 0
            schoolNameTexFieldHeight.constant = 0
            schoolAllButtonHeight.constant = 0
            forStaffOrForAllTextfieldHeight.constant = 0
            sendToHeight.constant = 0
        }
    }
    
    private func setupTable() -> Void {
        
        attachmentDocumentTableView.register(UINib(nibName: "attachmentDocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "attachmentDocumentTableViewCell")
        attachmentDocumentTableView.estimatedRowHeight = 68.0
    }
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        showAlert()
    }
    func showSwitchSibling(fromButton: UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        
        let user = DBController().getUserFromDB()
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    //MARK: -UITextfield deleagte to dismiss keyboard
    
    func textFieldShouldBeginEditing(_ DateView: UITextField) -> Bool {
        DateView.resignFirstResponder()
        return true;
    }
    private func textFieldShouldendEditing(DateView: UITextField) -> Bool {
        DateView.resignFirstResponder()
        return true;
    }
    
    @IBAction func showSchoolListPopUp(_ sender: UITextField) {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeSchoolListPopUpVC") as! DiaryComposeSchoolListPopUpVC ;
        schoolListArray = (user?.schools)!
        if defaultSchoolDisplay == true{
            schoolRecipientListArray.removeAll()
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
        }
        
        
        if schoolNameTexField.text == "All"{
            schoolRecipientListArray = schoolListArray
        }
        
        msRecipVC.recipientArray = schoolListArray
        msRecipVC.searchrecipientArray = schoolListArray
        msRecipVC.previousEntryArray = schoolRecipientListArray
        defaultSchoolDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    @IBAction func AllSchoolSelectedAction(_ sender: UIButton) {
        schoolNameTexField.text = "All"
        schoolRecipientListArray = (user?.schools)!
   }
    
    func  updateSelectedWithSchoolArray(receipientArray recipientArray: [corporateManagerSchoolModel]) {
        schoolNameTexField.text = ""
        if recipientArray.count == 0{
            schoolNameTexField!.text = ""
        }
            
        else if recipientArray.count == 1{
            schoolNameTexField!.text = recipientArray[0].schoolName
        }
        else if recipientArray.count == 2{
            schoolNameTexField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName
        }
        else if recipientArray.count == user?.Classes?.count{
            schoolNameTexField!.text = "All"
        }
        else {
            schoolNameTexField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName+","+recipientArray[2].schoolName+" + "+String(recipientArray.count - 2)
            
        }
        schoolRecipientListArray.removeAll()
        schoolRecipientListArray = recipientArray
        if schoolRecipientListArray.count == user?.schools?.count {
            schoolNameTexField.text = "All"
        }
        
    }
    
    
    @IBAction func showDatePickerForNotice(_ sender: UIButton) {
    
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            weakSelf!.noticeAddDate = date as Foundation.Date
            weakSelf!.DateView.text = DateApi.convertThisDateToString(weakSelf!.noticeAddDate as Foundation.Date, formatNeeded: .ddMMyyyyWithHypen)
            
        }
        prepareOverlayVC(overlayVC: datePickerVC)
        
        self.present(datePickerVC, animated: false, completion: nil)
        
    }
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    
    // MARK: PickerView Datasources
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return SendToNoticeArray.count
    }
    
    // MARK: PickerView Delegates
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        if SendToNoticeArray.count != 0 {
            return SendToNoticeArray[row]
        }
        return ""
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if SendToNoticeArray.count != 0{
            forStaffOrForAllTextfield.text = " "+SendToNoticeArray[row]
            noticeType = SendToNoticeArray[row]
        }
    }
    
    
    @IBAction func composeNoticeAction(_ sender: UIButton) {
        FireBaseKeys.FirebaseKeysRuleThree(value: "composeNotice_send")
        var idString = ""
        var schoolListString = ""
        let result = validateFields()
        if result.0 {
            
            if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) || user?.currentTypeOfUser == .CorporateManager{
                
            }
            else{
                noticeType = "For All"
            }
            
            if user?.currentTypeOfUser == .CorporateManager {
                for schoolArray in schoolRecipientListArray{
                    schoolListString = schoolListString+String(schoolArray.schoolId) + ","
                }
                schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString
                    .endIndex))
            }
            
            if uploadAttachmentArray.count > 0 {
                for attachmentArray in uploadAttachmentArray {
                    idString = idString+String(attachmentArray.attachmentId) + ","
                }
                idString = idString.substring(to: idString.characters.index(before: idString.endIndex))
                
            }
            if documentAttachmentArray.count > 0 {
                if uploadAttachmentArray.count != 0 {
                    idString = idString + ","
                }
                for attachmentArray in documentAttachmentArray {
                    idString = idString+String(attachmentArray.attachmentId) + ","
                }
                idString = idString.substring(to: idString.characters.index(before: idString.endIndex))
                
            }
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            WebServiceApi.postNoticeWithTitle(title: NotceNameView.text!,
                                              contentOfNotice: DescriptionView.text,
                                              dateofnotice: DateView.text!,
                                              typeofnotice: noticeType,
                                              idOfNoticeAddingPerson: (user?.currentUserRoleId)!,
                                              attachmentIdList: idString,
                                              corporateId:String(user!.corporateId),
                                              recipientSchoolIdList:schoolListString,
                                              onSuccess: { (result) in
                                                
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                if (result["error"] as? [String:AnyObject]) == nil{
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.noticePostingSucces, stateOfMessage: .success)
                                                    weakSelf?.clearAllData()
                                                    _ = weakSelf!.navigationController?.popViewController(animated: true)
                                                    
                                                }else{
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.noticeFailed, stateOfMessage: .failure)
                                                }
                                                
                                                
                                                
                                                
            },
                                              onFailure: { (error) in
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.noticeFailed, stateOfMessage: .failure)
                                                
            },
                                              duringProgress: { (progress) in
                                                
            })
            
            
            
            
            
        }
        else {
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
        
    }
    private func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
        
        if  schoolNameTexField.text == "" && user?.currentTypeOfUser == .CorporateManager {
            result = false
            ErrorMsg = ConstantsInUI.selectSchool
            
        }
        
        else if NotceNameView.text == "" {
            result   = false
            ErrorMsg = ConstantsInUI.noticeName
        }
        else if (DateView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.noticedate
        }else if (DescriptionView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.noticedescription
        }
        
        return (result,ErrorMsg)
    }
    
    private func clearAllData(){
        
        NotceNameView.text   = ""
        DateView.text        = ""
        DescriptionView.text = ""
        
    }
    private func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    
    
    @objc func addAttachmentImageTapped(img: AnyObject) {
        FireBaseKeys.FirebaseKeysRuleFour(value: "composeNotice_addAttach")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate               = self
        imagePicker.allowsEditing          = false
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
        let popper                         = imagePicker.popoverPresentationController
        popper?.delegate                   = self
        popper?.sourceView                 = self.view
        popper?.sourceRect                 = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        
        
        
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ConstantsInUI.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: ConstantsInUI.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "photoGalleryId") as! PhotoGallery
            vc.delegate = self
            
            self.toUploadAttachments.removeAll()
            self.uploadedcount = 0
            
            self.activeSelection = false
            self.deleteAttachment = false
            
            self.present(vc, animated: true, completion: nil)
        })
        
        let shareDocumentAction = UIAlertAction(title: ConstantsInUI.shareDocument, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF as NSString) , String(kUTTypeContent)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        })
        
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(shareDocumentAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        self.present(optionMenu, animated: false, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var documentName:String = ""
        print(url)
        
        if url.pathExtension == "pdf" || url.pathExtension == "doc" || url.pathExtension == "docx" {
            if(FileManager.default.fileExists(atPath: url.path)) {
                
                let dictoryUrl = url.absoluteString
                if let range = dictoryUrl.range(of: "/", options: .backwards) {
                    _ = dictoryUrl.substring(from: range.upperBound)
                }
                if let docName = dictoryUrl.components(separatedBy: "/").last {
                    print(docName)
                    documentName = docName
                }
                
                let fileData = try? Data(contentsOf: url)
                UploadDocumentAttachment(documentData: fileData! as NSData,docName: documentName)
            }
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectFormat, stateOfMessage: .failure)
        }
    }
    
    
    
    private func imagePickerController(
        picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated: true, completion: nil) //5
        cameraImages[0] = chosenImage
        cameraSelected = true
        
        SelectedImagesWithArray(selectedImages: cameraImages)
    }
    func reuploadImage(){
        
    }
    
    func SelectedImagesWithArray(selectedImages:[Int:UIImage])
    {
        toUploadAttachments = selectedImages
        uploadedcount = 0
        tempuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray = uploadAttachmentArray
        uploadAttachmentFailure = false
        startInitialUpload(selectedImages: selectedImages)
        
    }
    func check(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        _ = attachmentImages.count
        print(uploadAttachmentArray.count)
        print(tempuploadAttachmentArray.count)
        print(attachmentImages)
        
        if uploadAttachmentArray.count != 0 || documentAttachmentArray.count != 0{
            if uploadAttachmentFailure == true {
                imageArray.removeAll()
                tagCount = 0
                posCount = 0
                for i in 0 ..< uploadAttachmentArray.count {
                    if uploadAttachmentArray[i].attachmentType == "PDF" || uploadAttachmentArray[i].attachmentType == "DOC" || uploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                        
                        
                    }
                }
            }
            else{
                for i in 0 ..< tempuploadAttachmentArray.count {
                    if tempuploadAttachmentArray[i].attachmentType == "PDF" || tempuploadAttachmentArray[i].attachmentType == "DOC" || tempuploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                    }
                }
            }
            
            attachmentviewContainerHeight.constant = 0
            if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
                addNoticeViewHeight.constant = 556
            }
            else if user?.currentTypeOfUser == .CorporateManager {
               addNoticeViewHeight.constant = 616
            }
            else{
            addNoticeViewHeight.constant = 493
            }
            attachmentDocumentTableView.reloadData()
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
            deleteAttachment = false
            attachmentviewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: imageArray)
            attachmentDocumentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            addNoticeViewHeight.constant = addNoticeViewHeight.constant + (attachmentviewContainerHeight.constant - 50) + attachmentDocumentTableViewHeight.constant
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            
        }
        else {
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            attachmentviewContainerHeight.constant = 0
            if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
                addNoticeViewHeight.constant = 556
            }
            else if user?.currentTypeOfUser == .CorporateManager {
                addNoticeViewHeight.constant = 616
            }
            else{
                addNoticeViewHeight.constant = 493
            }
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
        
    }
    
    func startInitialUpload(selectedImages:[Int:UIImage]){
        
        for k in 0 ..< selectedImages.count {
            
            upLoadImage(image: selectedImages[k]!)
            
        }
    }
    
    
    func upLoadImage(image:UIImage){
        weak var weakSelf = self
        attachmentviewContainerHeight.constant = 0
        attachmentDocumentTableViewHeight.constant = 0
        if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
            addNoticeViewHeight.constant = 556
        }
        else if user?.currentTypeOfUser == .CorporateManager {
            addNoticeViewHeight.constant = 616
        }
        else{
            addNoticeViewHeight.constant = 493
        }
        while let subview = attachmentViewContainer.subviews.last {
            subview.removeFromSuperview()
        }
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadNoticeAttachmentImage(image: image, cameraSelected: cameraSelected,
                                                  onSuccess: { (result) in
            weakSelf!.addEntriesToAttachmentArray(array: parseAttachmentArray.parseThisDictionaryForAttachments(dictionary: result))
            self.uploadedcount = self.uploadedcount + 1
            if self.toUploadAttachments.count == self.uploadedcount{
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                self.deleteAttachment = false
                self.check()
                
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    func UploadDocumentAttachment(documentData:NSData,docName:String){
        var selectedTab:String = ""
        weak var weakSelf = self
        attachmentviewContainerHeight.constant = 0
        attachmentDocumentTableViewHeight.constant = 0
        while let subview = attachmentViewContainer.subviews.last {
            subview.removeFromSuperview()
        }
        selectedTab = "Notification"
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadAttachmentDocument(uploadDocumentData: documentData,docName: docName,selectedTab: selectedTab,
                                               onSuccess: { (result) in
                                                weakSelf!.addEntriesToDocumentAttachmentArray(array: parseAttachmentArray.parseThisDictionaryForAttachments(dictionary: result))
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                self.deleteAttachment = false
                                                self.check()
                                                
                                                
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.uploadAttachmentArray.removeAll()
                self.uploadAttachmentArray = self.copyuploadAttachmentArray
                self.uploadAttachmentFailure = true
                self.tempuploadAttachmentArray.removeAll()
                print("failed")
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    
    public func addEntriesToAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            
            uploadAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    public func addEntriesToDocumentAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            tempuploadAttachmentArray.removeAll()
            documentAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    
    internal func SelectedAttachmentTag(selectedAttachment currentTag : Int){
        
    var tempimageArray = [UIImageView]()
    var pos = 0
    var tagTagForImage = 0
    
    
    activeSelection = true
    uploadAttachmentArray.remove(at: currentTag)
    imageArray.remove(at: currentTag)
    deleteAttachment = true
    for k in 0 ..< imageArray.count {
    
    if imageArray[k].image != nil{
    imageArray[k].contentMode = .scaleAspectFit
    imageArray[k].tag = tagTagForImage
    tempimageArray.append(imageArray[k])
    tagTagForImage = tagTagForImage + 1
    pos = pos + 1
    }
    }
    tagCount = tagTagForImage
    posCount = pos
    if uploadAttachmentArray.count != 0 && imageArray.count != 0  {
    attachmentviewContainerHeight.constant = 0
        if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
            addNoticeViewHeight.constant = 556
        }
        else if user?.currentTypeOfUser == .CorporateManager {
            addNoticeViewHeight.constant = 616
        }
        else{
            addNoticeViewHeight.constant = 493
        }
        while let subview = attachmentViewContainer.subviews.last {
    subview.removeFromSuperview()
    }
    
    deleteAttachment = false
    
    attachmentviewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: tempimageArray)
    attachmentDocumentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
    
    addNoticeViewHeight.constant = addNoticeViewHeight.constant + (attachmentviewContainerHeight.constant - 50)+attachmentDocumentTableViewHeight.constant
    
    }
    else {
    
    attachmentviewContainerHeight.constant = 0
        if webServiceRolesGroupForDifferentActivities.addNewNoticeForStaffOrForAllOptionGroup.contains((user?.employeeType)!) {
            addNoticeViewHeight.constant = 556
        }
        else if user?.currentTypeOfUser == .CorporateManager {
            addNoticeViewHeight.constant = 616
        }
        else{
            addNoticeViewHeight.constant = 493
        }
        while let subview = attachmentViewContainer.subviews.last {
    subview.removeFromSuperview()
    }
    
    }
    
    }
    
    //MARK: Table view delegate and data source
    
     func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentAttachmentArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "attachmentDocumentTableViewCell") as! attachmentDocumentTableViewCell
        cell.documentUniqueName.text = documentAttachmentArray[indexPath.row].attachmentName
        cell.selectionStyle = .none
        if documentAttachmentArray[indexPath.row].attachmentType == "PDF" {
            cell.documentIcon.image = UIImage(named: "pdfIcon")
        }
        if documentAttachmentArray[indexPath.row].attachmentType == "DOCX" || documentAttachmentArray[indexPath.row].attachmentType == "DOC" {
            cell.documentIcon.image = UIImage(named: "wordIcon")
            
        }
        cell.documentCloseButton.tag = indexPath.row
        cell.documentCloseButton.addTarget(self, action: #selector(ComposeNotification.deleteDocumentAttachment), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc  private func deleteDocumentAttachment(sender:UIButton) {
        for k in 0 ..< documentAttachmentArray.count {
            if k == sender.tag {
                documentAttachmentArray.remove(at: k)
            }
        }
        attachmentDocumentTableView.reloadData()
        attachmentDocumentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
        addNoticeViewHeight.constant = addNoticeViewHeight.constant - 40
    }
    

}
