//
//  DetailedNotificationWithDesc.swift
//  parentEye
//
//  Created by Martin Jacob on 16/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class DetailedNotificationWithDesc: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,SelectedSchoolpopOverDelegate {

    var notification:NotificationModel?
    var user:userModel?
    var profilePic = ""
    var isExpandedImageList = false
    var isExtraImagesLoaded = false
    var schoolId:Int = 0
    var imageStringArray = [AttachmentModel]()
    var pdfViewer = PdfController()
    var goingToDetailed = false // variable to check whether we are going to detailed screen/changing tabbar (used in viewdiddisapear)
    
    @IBOutlet weak var header:CommonHeader!
    @IBOutlet weak var DescrptionLabel: UILabel!
    @IBOutlet weak var expandingView: ExpansiveView!
    @IBOutlet weak var notificationHeading: CommonNotificationHeading!
    @IBOutlet weak var slidingCollection: SliderBaseView!
    @IBOutlet weak var expandingViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addMoreImageButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var addMoreImageButton: UIButton!
    
    //MARK:- View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for img in (notification?.imageArray)!{
           imageStringArray.append(img)
        }
        for pdf in (notification?.pdfArray)!{
            imageStringArray.append(pdf)
        }
        setUpHeader()
        
        setUpUI()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        user = DBController().getUserFromDB()
        
        
       
        
        setUpHeader()
        setUpHeading()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if goingToDetailed == false {
                _ = self.navigationController?.popViewController(animated: false)
        }
        
    }
    override func viewDidLayoutSubviews() {
        
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
//        if previousTraitCollection != nil {
//            if isExpandedImageList == true{
//                expandingViewHeight.constant =  makeMultipleLine()
//            } else if isExpandedImageList == false {
//                expandingViewHeight.constant =  makeImageArray()
//            }
//        }
        
    }
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        slidingCollection.invalidateLayoutAndReload()
    }
    
    //MARK:- Heder delegates
    private func setUpHeader (){
        if user?.currentTypeOfUser == .guardian{
            
            profilePic =  (user?.Students![(user?.currentOptionSelected)!].profilePic)!
        }
        else{
            profilePic = ""
        }
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.notificationScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
        
        
    }
    
    
    private func setUpUI (){
        
        var notificationAndAttachmentArrayCount:Int = 0
        notificationAndAttachmentArrayCount = (notification?.imageArray.count)! + (notification?.pdfArray.count)!
        
        if notificationAndAttachmentArrayCount < 4{
            addMoreImageButton.isHidden = true
        }
        
        
        DescrptionLabel.text = notification?.content
        slidingCollection.imageArray = imageStringArray
        slidingCollection.shouldShowDownloadAndShare = true
        slidingCollection.reloadSlidingView()
        let size = makeImageArray()
        expandingViewHeight.constant = size
        
        if notification?.hasAttachment == false {
            addMoreImageButtonHeight.constant = 0.0
            slidingCollection.setForNoIMages()
        }
        
        weak var weakSelf = self
        slidingCollection.shareButtonClicked  = {(indexClicked)->Void in
            
            if weakSelf != nil {
                
                if weakSelf!.imageStringArray[indexClicked] is ModelImage {
                    let imgUrlToShare = weakSelf!.imageStringArray[indexClicked].url
                    ImageAPi.fetchImageForUrl(urlString: imgUrlToShare, oneSuccess: { (image) in
                        if weakSelf != nil {
                            let share = ShareAPI(activityItems: [image], applicationActivities: nil)
                            weakSelf!.present(share, animated: true, completion: nil)
                        }
                        
                    }, onFailure: { (error) in
                        
                    })
                } else if weakSelf!.imageStringArray[indexClicked] is PdfModel {
                    let urlToShare = weakSelf!.imageStringArray[indexClicked].url
                    let share = ShareAPI(activityItems: [urlToShare], applicationActivities: nil)
                    weakSelf!.present(share, animated: true, completion: nil)
                }
                
                
                
            }
            
        }
        
        
        slidingCollection.downloadButtonClicked = { (button)->Void in
            FireBaseKeys.FirebaseKeysRuleOne(value: "notice_attachDwnld")
            
            let btn = button as! ButtonForDownLoadImg
            hudControllerClass.showNormalHudToViewController(viewController: weakSelf!)
            if btn.imageForDownloading != nil {
                
                
                if weakSelf!.imageStringArray[btn.tag] is ModelImage {
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    ImageAPi.fetchImageForUrl(urlString: weakSelf!.imageStringArray[btn.tag].url, oneSuccess: { (image) in
                        UIImageWriteToSavedPhotosAlbum(image, weakSelf!, #selector(self.savedImage(_:didFinishSavingWithError:contextInfo:)), nil)
                    }) { (error) in
                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadFailed, stateOfMessage: .failure)
                    }
                } else {
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    let pdf = weakSelf!.imageStringArray[btn.tag]
                    weakSelf!.pdfViewer.showPdfInViewController(vc: weakSelf!, pdfHavingUrl: pdf.url, withName: pdf.name)
                    weakSelf?.goingToDetailed = false
                    
                }
                
                
            }
        }
        
        slidingCollection.indexClickedClosure =  {(indexClickedIn)->Void in
            
            weakSelf!.performSegue(withIdentifier: segueConstants.segueForLoadingExtraDetails, sender: indexClickedIn)
        }
        
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        slidingCollection.fromSreeMaharshiLogin = false
    }
    
    private func makeImageArray()->CGFloat{
        
        expandingView.removeAllSubviews()
        var imageArray = [UIImageView]()
        var tagTagForImage = 0
        for urlString in imageStringArray{
            var image = UIImage()
            
            if urlString.type == "PDF" {
                image = UIImage(named: "pdfIcon")!
            }
            else if urlString.type == "DOC" || urlString.type == "DOCX"  {
                image = UIImage(named: "wordIcon")!
            }
            else{
                image = UIImage(named: "ApplicationLogo")!
            }
            let imagv = UIImageView(image: image)
            
                weak var imgView = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                    if imgView != nil { imgView!.image = image }
                    
                    }, onFailure: { (error) in
                        debugPrint("Download failed")
                })
            
                imagv.contentMode = .scaleAspectFit
                imageArray.append(imagv)
                imagv.tag = tagTagForImage
                tagTagForImage = tagTagForImage + 1
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.performSegueFromExpandingView))
                imagv.addGestureRecognizer(tap)
                imagv.isUserInteractionEnabled = true
            
           
        }

        return expandingView.makeOnlineImageArrayFromImageArray(imageArray: imageArray)
        
        
    }
    
    private func makeMultipleLine()->CGFloat{
        expandingView.removeAllSubviews()
        
        var imageArray = [UIImageView]()
        var tagTagForImage = 0
        for urlString in imageStringArray{
            let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
            if urlString is PdfModel {
                if urlString.type == "PDF" {
                    imagv.image = UIImage(named: "pdfIcon")!
                }
                else if urlString.type == "DOC" || urlString.type == "DOCX"  {
                    imagv.image = UIImage(named: "wordIcon")!
                }
                else{
                    imagv.image = UIImage(named: "ApplicationLogo")!
                }
                }
                imagv.contentMode = .scaleAspectFit
                weak var imgView = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                    if imgView != nil { imgView!.image = image }
                    }, onFailure: { (error) in
                        debugPrint("Download failed")
                })
                
                imagv.contentMode = .scaleAspectFit
                imageArray.append(imagv)
                imagv.tag = tagTagForImage
                tagTagForImage = tagTagForImage + 1
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.performSegueFromExpandingView))
                imagv.addGestureRecognizer(tap)
                imagv.isUserInteractionEnabled = true
            
        }
      return  expandingView.addImagesToMeFromArray(imageArray: imageArray)
    }
    @objc private func performSegueFromExpandingView(gesture:UITapGestureRecognizer){
        self.performSegue(withIdentifier: segueConstants.segueForLoadingExtraDetails, sender: gesture.view?.tag)
    }
    
    private func setUpHeading(){
        notificationHeading.dayLabel.text                    = notification?.dayString
        notificationHeading.designationLabel.text            = notification?.addedbyName
        notificationHeading.updatedUponDescriptionLabel.text = notification?.lastUpdatedDate
        notificationHeading.yearMonthLabel.text              = "\(notification!.monthString) \(notification!.yearString)"
        notificationHeading.headingLabel.text                = notification?.title
        if notification?.hasReminder == false {
            notificationHeading.reminderButtonHeight.constant = 0.0
            notificationHeading.reminderButton.isHidden         = true
        }
        if notification?.isUpComing == true{
            notificationHeading.pastOrUpComingLabel.text = ConstantsInUI.upComing
        }else {
            let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
             notificationHeading.pastOrUpComingLabel.text = ConstantsInUI.past
             notificationHeading.pastOrUpComingLabel.textColor = colur
        }

    }
    
    private func setExpansiveView(){
//       let size = expandingView.addOnelineImagesToMe((notification?.imageArray)!)
//        expandingViewHeight.constant = size
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func getMoreImages(_ sender: UIButton) {
       
        if isExtraImagesLoaded == false {
            loadExtraImages()
        }else if isExpandedImageList == true{
            isExpandedImageList = false
            addMoreImageButton.setBackgroundImage(UIImage(named: "messageAddIcon"), for: .normal)
            expandingViewHeight.constant =  makeImageArray()
        } else if isExpandedImageList == false {
            isExpandedImageList = true
            addMoreImageButton.setBackgroundImage(UIImage(named: "roundCollapse"), for: .normal)
            expandingViewHeight.constant =  makeMultipleLine()
        }
    }
    // MARK: header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager || user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    @objc func savedImage(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        if error != nil{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadFailed, stateOfMessage: .success)
        }else {
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadedSuccesFuly, stateOfMessage: .success)
        }
        
        
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let extraDetailed = segue.destination as! ExtraDetailed
        extraDetailed.rowToload = sender as! Int
        extraDetailed.notification = self.notification
        extraDetailed.imageStringArray = imageStringArray
        goingToDetailed = true
    }
    
    // MARK:webService 
    
    private func loadExtraImages(){
    
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getMoreAttachmentForNotificationWithID(notificationId: (notification?.id)!, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            if let attachments = result[resultsJsonConstants.notificationConstants.noticeAttachmentForImage] as? [[String:AnyObject]]{
                
            let noti = NotificationModel.getAttachmentsFromDictionary(attachmentArrayDictioanry: attachments)
                for img in (noti.imageArray){
                    self.imageStringArray.append(img)
                }
                for pdf in (noti.pdfArray){
                    self.imageStringArray.append(pdf)
                }
            }
            
            weakSelf!.expandingViewHeight.constant =  weakSelf!.makeMultipleLine()
            weakSelf!.isExpandedImageList = true
            weakSelf!.isExtraImagesLoaded = true
            weakSelf!.slidingCollection.imageArray = weakSelf!.imageStringArray
            weakSelf!.slidingCollection.reloadSlidingView()
            
            weakSelf!.addMoreImageButton.setBackgroundImage(UIImage(named: "roundCollapse"), for: .normal)
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
        }) { (progress) in
        }
    }
    
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
    }
}
