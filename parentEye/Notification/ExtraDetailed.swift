//
//  ExtraDetailed.swift
//  parentEye
//
//  Created by Martin Jacob on 18/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

class ExtraDetailed: UIViewController,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,SelectedSchoolpopOverDelegate {
    
    var notification:NotificationModel?
    var rowToload = 0
    var user:userModel?
    var imageStringArray = [AttachmentModel]()
    var pdfViewer = PdfController()
    var shouldGotoRoot = true
    var profilePic = ""
    var schoolId:Int = 0
    
    @IBOutlet weak var collection: SliderBaseView!
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var notificationHeader: CommonNotificationHeading!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        user = DBController().getUserFromDB()
        setUpHeading()
        setUpUI()
        setUpHeader()
        // Do any additional setup after loading the view.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        collection.scrollToPosition(position: rowToload)
    }
    override func viewDidDisappear(_ animated: Bool) {
        if shouldGotoRoot == true {
            _ = self.navigationController?.popToRootViewController(animated: false)
        }else{
            shouldGotoRoot = true
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    private func setUpHeading(){
        notificationHeader.dayLabel.text                    = notification?.dayString
        notificationHeader.designationLabel.text            = notification?.addedbyName
        notificationHeader.updatedUponDescriptionLabel.text = notification?.lastUpdatedDate
        notificationHeader.yearMonthLabel.text              = "\(notification!.monthString) \(notification!.yearString)"
        notificationHeader.headingLabel.text                = notification?.title
        if notification?.hasReminder == false {
            notificationHeader.reminderButtonHeight.constant = 0.0
            notificationHeader.reminderButton.isHidden         = true
        }
        if notification?.isUpComing == true{
            notificationHeader.pastOrUpComingLabel.text = ConstantsInUI.upComing
        }else {
            let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
            notificationHeader.pastOrUpComingLabel.text = ConstantsInUI.past
            notificationHeader.pastOrUpComingLabel.textColor = colur
           
        }
        
    }
    
    private func setUpUI (){
        collection.imageArray = imageStringArray
        collection.shouldShowDownloadAndShare = true
        collection.reloadSlidingView()
        
        weak var weakSelf = self
        collection.shareButtonClicked  = {(indexClicked)->Void in
            
            if weakSelf != nil {
                
                if weakSelf!.imageStringArray[indexClicked] is ModelImage {
                    let imgUrlToShare = weakSelf!.imageStringArray[indexClicked].url
                    ImageAPi.fetchImageForUrl(urlString: imgUrlToShare, oneSuccess: { (image) in
                        if weakSelf != nil {
                            let share = ShareAPI(activityItems: [image], applicationActivities: nil)
                            weakSelf!.present(share, animated: true, completion: nil)
                        }
                        
                        }, onFailure: { (error) in
                            
                    })
                } else if weakSelf!.imageStringArray[indexClicked] is PdfModel {
                    let urlToShare = weakSelf!.imageStringArray[indexClicked].url
                    let share = ShareAPI(activityItems: [urlToShare], applicationActivities: nil)
                    weakSelf!.present(share, animated: true, completion: nil)
                }
                
                
                
            }
        
        }
        
        
        collection.downloadButtonClicked = { (button)->Void in
            FireBaseKeys.FirebaseKeysRuleOne(value: "notice_attachDwnld")
            
            let btn = button as! ButtonForDownLoadImg
            hudControllerClass.showNormalHudToViewController(viewController: weakSelf!)
            if btn.imageForDownloading != nil {
                
                
                if weakSelf!.imageStringArray[btn.tag] is ModelImage {
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    ImageAPi.fetchImageForUrl(urlString: weakSelf!.imageStringArray[btn.tag].url, oneSuccess: { (image) in
                        UIImageWriteToSavedPhotosAlbum(image, weakSelf!, #selector(self.savedImage(_:didFinishSavingWithError:contextInfo:)), nil)
                    }) { (error) in
                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadFailed, stateOfMessage: .failure)
                    }
                } else {
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    let pdf = weakSelf!.imageStringArray[btn.tag]
                    weakSelf!.pdfViewer.showPdfInViewController(vc: weakSelf!, pdfHavingUrl: pdf.url, withName: pdf.name)
                    weakSelf?.shouldGotoRoot = false
                    
                }
                
                
            }
        }
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
    }
    
    private func setUpHeader(){
        if user?.currentTypeOfUser == .guardian{
            
            profilePic =  (user?.Students![(user?.currentOptionSelected)!].profilePic)!
        }
        else{
            profilePic = ""
        }
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.notificationScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
        
        
    }
    // MARK: header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager || user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    @objc func savedImage(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo:UnsafeRawPointer){
        if error != nil{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadFailed, stateOfMessage: .success)
        }else {
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.imageDownloadedSuccesFuly, stateOfMessage: .success)
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
        
    }
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
}
