//
//  EmployeeStudentProfileCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 03/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class EmployeeStudentProfileCell: UITableViewCell {
    
    @IBOutlet var studentTeacherName: UILabel!
    @IBOutlet var studentGrade: UILabel!
    @IBOutlet var viewMore: UIButton!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var studentProfile: UIImageView!
    
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var previousButton: UIButton!
    
    @IBOutlet var admnNo: UILabel!
    
    @IBOutlet var mobNo: UILabel!
    @IBOutlet var dateOfBirth: UILabel!
    @IBOutlet var adress: UILabel!
    @IBOutlet var guardianName: UILabel!
    
    @IBOutlet var callButton: UIButton!
    
    
        override func awakeFromNib() {
            super.awakeFromNib()
            ImageAPi.makeImageViewRound(imageView: studentProfile)
            // Initialization code
        }
        
    @IBAction func viewMoreAction(sender: AnyObject) {
    }
    
    func alterCellWithProfileImageUrl(_ url:String) -> Void {
        studentProfile!.image = UIImage(named:"avatar.png")
        if(url.isEmpty){
            studentProfile!.image = UIImage(named:"avatar.png")
        }
        else{
            weak var weakProfileImage = self.studentProfile
            ImageAPi.fetchImageForUrl(urlString: url, oneSuccess: { (image) in
                if weakProfileImage != nil {
                    weakProfileImage!.image = image
                }else{
                    weakProfileImage!.image = UIImage(named:"avatar.png")
                }
            }) { (error) in
                weakProfileImage!.image = UIImage(named:"avatar.png")
            }
        }
        
    }

}

