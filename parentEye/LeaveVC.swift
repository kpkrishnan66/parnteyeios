//
//  LeaveVC.swift
//  parentEye
//
//  Created by Emvigo on 7/25/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

class LeaveVC: UIViewController,UITableViewDelegate,UITableViewDataSource,leaveSendProtocol {

    @IBOutlet weak var tblLeaveStatus: UITableView!
    @IBOutlet var absentDaysCount: UILabel!
    @IBOutlet var addNewLeaveButtonHeight: NSLayoutConstraint!
    @IBOutlet var leaveToSubmitButtonHeight: NSLayoutConstraint!
    @IBOutlet var lblNoResult: UILabel!
    @IBOutlet var btnSubmitWidth: NSLayoutConstraint!
    @IBOutlet var addNewLeave: UIButton!
    @IBOutlet var leaveToSubmit: UIButton!
    @IBOutlet var totalAbsentDaysLabel: UILabel!
    
    var user:userModel?
    var leave:userModel?
    var leaveReport = [StudentAbsentRecord]()
    var leaveListArray  = [EmployeeDetailedStudentProfile]()
    var lines  = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "LeaveHeaderView", bundle: nil)
        tblLeaveStatus.register(nib, forHeaderFooterViewReuseIdentifier: "LeaveHeaderView")
        NotificationCenter.default.addObserver(self, selector: #selector(methodOFReceivedNotication), name:NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
        getUserDataFromDB()
        tblLeaveStatus.rowHeight = 46
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
        let userDefaults = UserDefaults.standard
        let decoded  = userDefaults.object(forKey: "leaves") as! NSData
        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as! [EmployeeDetailedStudentProfile]
         leaveListArray = decodedTeams
        }
        if user?.currentTypeOfUser == .guardian       {
          addNewLeaveButtonHeight.constant = 30
          //leaveToSubmitButtonHeight.constant = 30
        }
        else{
            addNewLeaveButtonHeight.constant = 0
            //leaveToSubmitButtonHeight.constant = 0
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    // MARK: UIEvents
    @IBAction func newLeaveButtonPressed(_ sender: Any) {
            if user?.currentTypeOfUser == .guardian{
                Analytics.logEvent("P_studentprofile_addnewLeave", parameters: nil)
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifier"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationIdentifierForPopUp"), object: nil)
        }
}


// MARK: UIEvents handlers
extension LeaveVC{

    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        if user?.currentTypeOfUser == .guardian{
          getLeaveStatusReport()
        }
        else{
            addNewLeave.isHidden = true
           // leaveToSubmit.isHidden = true
        }
       
    }
    func getLeaveStatusReport() {
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getLeaveStatus(studentId: (user?.Students![(user?.currentOptionSelected)!].id)!, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let tempResult = result["studentAbsentRecord"] as? NSArray {
                debugPrint("leave result",tempResult)
                self.leaveReport = parseAbsentRecordArray.parseThisDictionaryForAbsentRecords(dictionary: result)
                weakSelf!.configLeavePage()
            }
            else{
                self.lblNoResult.isHidden = false
                self.lblNoResult.text = "There are no leave records found"
                self.tblLeaveStatus.isHidden = true
                self.absentDaysCount.text = "0" 
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
        }) { (progress) in
            
        }
    }
    
    func configLeavePage(){
        var noofabsentdays = 0
        if leaveReport.count == 0 {
            lblNoResult.isHidden = false
            lblNoResult.text = "There are no leave records found"
            tblLeaveStatus.isHidden = true
            absentDaysCount.text = String(noofabsentdays)
        }
        else{
            lblNoResult.isHidden = true
            for index in 0 ..< leaveReport.count {
                   if self.leaveReport[index].leaveStatus == "Not Submitted" ||    self.leaveReport[index].leaveStatus == "Submitted and Absent" {
                        noofabsentdays += 1
                    }
            }
            absentDaysCount.text = String(noofabsentdays)
            
        }
        tblLeaveStatus.reloadData()
    }
    
    @objc func methodOFReceivedNotication(notification: NSNotification){
//        self.performSegueWithIdentifier("segueToLeaveForm", sender: self)
        
        let leaveFornView:LeaveFormVC        = LeaveFormVC(nibName: "LeaveForm", bundle: nil)
        if let indexpath                     = notification.object as? NSIndexPath{
            leaveFornView.dateOfLeave        = self.leaveReport[indexpath.row].date
        }
        let overlayTransitioningDelegate     = OverlayTransitioningDelegate()
        leaveFornView.transitioningDelegate  = overlayTransitioningDelegate
        leaveFornView.modalPresentationStyle = .custom
        leaveFornView.delegate = self
        self.present(leaveFornView, animated: false, completion: nil)
    }
    func updatedLeave(){
        if user?.currentTypeOfUser == .guardian{
        print("leave got")
        tblLeaveStatus.isHidden = false
        getLeaveStatusReport()
        }
    }

    

}

// MARK: TableView DataSource and Delegate
extension LeaveVC{
    
    func numberOfSections(in tableView: UITableView) -> Int  {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
        return leaveListArray.count
       }
        return leaveReport.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell              = tableView.dequeueReusableCell(withIdentifier: "LeaveCell") as! LeaveTC
        cell.id               = indexPath as NSIndexPath
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            if leaveListArray.count != 0{
            cell.lblDate.text     = leaveListArray[indexPath.row].date
            cell.btnSubmit.isHidden = true
            //cell.btnSubmitWidth.constant = 0
            }
        }
        else{
            if leaveReport.count != 0{
                
                cell.lblDate.text     = self.leaveReport[indexPath.row].date
                cell.lblStatus.text   = self.leaveReport[indexPath.row].reason
              
                
                cell.descriptionText.text   = self.leaveReport[indexPath.row].description
                
                
                if cell.descriptionText.text != nil{
                    lines = countLabelLines(label: cell.descriptionText)
                    print(lines)
                    if lines == 1 {
                      cell.descriptionTextHeight.constant = 21
                        tblLeaveStatus.rowHeight = 60
                    }
                    else if lines == 2{
                      cell.descriptionTextHeight.constant = 42
                        tblLeaveStatus.rowHeight = 80
                    }
                    else{
                      cell.descriptionTextHeight.constant = 63
                        tblLeaveStatus.rowHeight = 100
                    }
                    cell.descriptionText.numberOfLines = 0
                    cell.descriptionText.sizeToFit()
                }
                else{
                   tblLeaveStatus.rowHeight = 46
                }
                
            
            }
        }
        if user?.currentTypeOfUser == .guardian{
        if cell.lblStatus.text == "Leave Not Submitted"{
            cell.btnSubmit.isHidden = false
        }
        else{
            cell.btnSubmit.isHidden = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // Dequeue with the reuse identifier
     
        return Bundle.main.loadNibNamed("LeaveHeaderView", owner: nil, options: nil)![0] as? UIView
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if user?.currentTypeOfUser == .guardian {
            if leaveReport.count == 0 {
                return 0.0
            }
        }
        return 45.0
    }

    
    func countLabelLines(label:UILabel)->Int{
        
        if let text = label.text{
            // cast text to NSString so we can use sizeWithAttributes
            let myText = text as NSString
            
            //Set attributes
            let attributes = [NSAttributedStringKey.font : label.font]
            let bounds = UIScreen.main.bounds
            let screenWidth = bounds.size.width
            print(screenWidth)
            //Calculate the size of your UILabel by using the systemfont and the paragraph we created before. Edit the font and replace it with yours if you use another
            let labelSize = myText.boundingRect(with: CGSize(width:(screenWidth - 180), height:CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes:attributes, context: nil)
            //Now we return the amount of lines using the ceil method
            let lines = ceil(CGFloat(labelSize.height) / label.font.lineHeight)
            return Int(lines)
        }
        
        return 0
        
    }
    
}

