//
//  FirstViewController.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import Firebase
import AVFoundation


class SchoolDiaryListing: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,CommonHeaderProtocol,popOverDelegate,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,passSendClassListIdProtocol,SelectedSchoolpopOverDelegate,audioPlayBackProtocol {

    /*******************************************/
    @IBOutlet var diaryTopviewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var diaryEntryListingTableheightConstraint: NSLayoutConstraint!
    @IBOutlet var composeDiaryHeightConstraint: NSLayoutConstraint!
    @IBOutlet var diaryTopView: UIView!
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var diaryEntryListingTable: UITableView!
    @IBOutlet var diaryCollectionView: UICollectionView!
    @IBOutlet var diaryTopViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var composenewDiary: UIButton!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var containerView: UIView!
    /*******************************************/
    
    let reuseIdentifier = "cell"
    var page = 0
    var user:userModel?
    var classId:Int = 0
    var schoolId:Int = 0
    var className:String = ""
    var studentId:String = ""
    var ClassesList:[EmployeeClassModel] = []
    var defaultClassesList:[EmployeeClassModel] = []
    var selectedRow = 0
    var ClassIsSelected = [Int: Bool]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var initialIndexpath = IndexPath()
    var diaryDetailedClass = false
    var diaryReciepientArray:[EmployeeClassModel] = []
    var moveToPdf:Bool = false
    var classList : [classSubjectList]!
    var Classes:[EmployeeClassModel]?
    var audioPlayView  = audioPlay()
    var destinationUrl = NSURL()
    var replacedString:String = ""
    
    
    
    
    
    /*************************************************************************/
    /// Aray for storing the search results
    lazy var filteredDiaryEntryListingDictionary = [String:[DiaryModelClass]]()
    lazy var diaryEntryListingDictionary         = [String:[DiaryModelClass]]()
    lazy var keyArray = [String]()
    /*************************************************************************/
    
    var pdfViewer = PdfController()
    
    
    
    //MARK:- View delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        initializeTableView()
        getUserDataFromDB()
        diaryDetailedClass = false
        FireBaseKeys.FirebaseKeysRuleTwo(value: "homePage_diary")
        if user?.currentTypeOfUser == .guardian{
            setUpUIforGuardian()
        }
        else{
            setupCountOfItems()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadDiary
        if result{
            tabBar.shouldLoadDiary = false
            page = 0
            makeWebApiCallToGetDiaryEntries()
            
        }
        if moveToPdf == false {
            getUserDataFromDB() // syncing user from db
            clearDataForThisPage() // clearing data on page
            alterHeader() // changing the header
            if user?.currentTypeOfUser == .guardian {
                Analytics.logEvent("P_homePage_diary", parameters: nil)
            }

            if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
                if user?.currentTypeOfUser == .CorporateManager{
                    header.multiSwitchButton.setTitle(user!.schools![(user?.currentSchoolSelected)!].schoolName, for: UIControlState())


                }

                if  diaryDetailedClass == false{
                    FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_diary")
                    setupCountOfItems()
                    diaryCollectionView.reloadData()
                    diaryCollectionView.scrollToItem(at: initialIndexpath, at: UICollectionViewScrollPosition.left, animated: true)
                }

            }
            diaryDetailedClass = false
            if user?.currentTypeOfUser == .CorporateManager {
                if schoolId != user!.schools![(user?.currentSchoolSelected)!].schoolId {
                    getClassListForCorporateManager()

                }
                else{
                    makeWebApiCallToGetDiaryEntries()// creating service to get data fresh
                }
            }
            else{
                makeWebApiCallToGetDiaryEntries()// creating service to get data fresh
            }
        }
        else{
            moveToPdf = false
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        diaryEntryListingTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- DB interactions
    /**
     Function to get user data from DB
     */
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    
    
    private func setupCountOfItems()
    {
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        className = ClassesList[0].className
        defaultClassesList.removeAll()
        
        defaultClassesList.append(ClassesList[0])
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        
    }
    
    private func setUpUIforGuardian(){
        
        composeDiaryHeightConstraint.constant = 0.0
        diaryTopViewHeightConstraint.constant = 0.0
        composenewDiary.isHidden = true
    }
    
    
    //MARK: tableView methods
    
    private func initializeTableView(){
        diaryEntryListingTable.register(UINib(nibName: "CommonListingcell", bundle: nil), forCellReuseIdentifier: "CommonListingcell")
        diaryEntryListingTable.estimatedRowHeight = 129.0
        diaryEntryListingTable.rowHeight          = UITableViewAutomaticDimension
        
        let bottomRefreshControl                         = UIRefreshControl()
        bottomRefreshControl.triggerVerticalOffset       = 100.0
        bottomRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromBottomRefresh), for: .valueChanged)
        self.diaryEntryListingTable.bottomRefreshControl = bottomRefreshControl
        
        let topRefreshControl                   = UIRefreshControl()
        topRefreshControl.triggerVerticalOffset = 100.0
        topRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromTopRefreshControl(_:)), for: .valueChanged)
        diaryEntryListingTable.addSubview(topRefreshControl)
    }
    
    /**
     Function to set the visiblity of data view and fill it with data
     
     - parameter cell:         cell
     - parameter indexPath:    indexpath for cell
     - parameter notification: notification
     */
    private func setDateVisibilityForCell(cell:CommonListingcell, forindex indexPath:IndexPath ,andArrayCount count:Int){
        
        cell.topLineView.isHidden = false
        cell.bottomLineView.isHidden = false
        
        if indexPath.row == 0 {
            cell.topLineView.isHidden = true
            if count == 1 {
                cell.bottomLineView.isHidden = true
            }
        } else {
            cell.topLineView.isHidden = false
            cell.bottomLineView.isHidden = false
            if indexPath.row+1 == count {
                cell.bottomLineView.isHidden = true
            }
        }
        
    }
    
    
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            return 1
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        initialIndexpath  = IndexPath(row: 0, section: 0)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SchoolDiaryClassListCell
        if ClassIsSelected[indexPath.row] == false{
            cell.employeeSelectedClass.backgroundColor = UIColor.clear
            cell.employeeSelectedClass.setTitleColor(UIColor.black, for: UIControlState())
            cell.employeeSelectedClass.layer.borderWidth = 1
            cell.employeeSelectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.employeeSelectedClass.backgroundColor = colur
            cell.employeeSelectedClass.setTitleColor(UIColor.white, for: UIControlState())
            cell.employeeSelectedClass.layer.borderWidth = 0
        }
        cell.employeeSelectedClass.setTitle(ClassesList[indexPath.row].className, for: UIControlState())
        cell.employeeSelectedClass.layer.cornerRadius = 18
        cell.employeeSelectedClass.layer.borderWidth = 1
        cell.employeeSelectedClass.layer.borderColor = UIColor.black.cgColor
        cell.employeeSelectedClass.tag = indexPath.row
        cell.employeeSelectedClass.addTarget(self, action: #selector(SchoolDiaryListing.selectedclassAction(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    
    
    //MARK:- TAbleview delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let key = keyArray[section]
        return (filteredDiaryEntryListingDictionary[key]?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return filteredDiaryEntryListingDictionary.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "CommonListingcell") as! CommonListingcell
        
        
        let key        = keyArray[indexPath.section]
        let diaryEntry = filteredDiaryEntryListingDictionary[key]![indexPath.row]
        
        
        if indexPath.row != 0 {
            cell.dateView.isHidden = true
        }else {
            cell.dateView.isHidden        = false
            cell.dayLabel.text          = diaryEntry.dayString
            cell.monthAndYearLabel.text = diaryEntry.monthString + " " + diaryEntry.yearString
        }
        cell.setCellForDiary() // setting the cell for diary
        
        
        cell.titleLabel.text = diaryEntry.subject
        if user?.currentTypeOfUser == .guardian{
            cell.designatedUpdatedUponLabel.text = diaryEntry.from
        }
        else{
            cell.designatedUpdatedUponLabel.text = diaryEntry.from + " | To: " + diaryEntry.recipientDetails
            
        }
        cell.removeAllImagesFromImageContainerView() //  removing all the images from image containerview
        
        if diaryEntry.hasAttachement{
            cell.setDiaryCellForAttachment()
        }else {
            cell.setDiaryCellForNoAttachments()
        }
        
        /*if diaryEntry.hasPdfAttachment {
         cell.downLoadAttachmentButton.hidden = false
         cell.downLoadButtonHeight.constant = 22.0
         
         cell.downLoadAttachmentButton.setTitle(diaryEntry.pdfArray[0].name, forState: .Normal)
         cell.downLoadAttachmentButton.name = diaryEntry.pdfArray[0].name
         cell.downLoadAttachmentButton.downloadUrlString = diaryEntry.pdfArray[0].url
         cell.downLoadAttachmentButton.addTarget(self, action: #selector(loadPdf(_:)), forControlEvents: .TouchUpInside)
         
         }else {
         cell.downLoadAttachmentButton.hidden = true
         cell.downLoadButtonHeight.constant = 0.0
         }*/
        cell.downLoadAttachmentButton.isHidden = true
        cell.downLoadButtonHeight.constant = 0.0
        
        if diaryEntry.AudioArray.count > 0 {
            cell.attachmentShowingImage.image = UIImage(named:"audioAttachmentIcon")
            cell.attachmentShowingImageWidthConstraint.constant = 20
        }
        else if diaryEntry.pdfArray.count > 0 || diaryEntry.imageArray.count > 0 {
            cell.attachmentShowingImage.image = UIImage(named:"attachmentImage")
            cell.attachmentShowingImageWidthConstraint.constant = 10
        }
            
        else{
            cell.attachmentShowingImageWidthConstraint.constant = 0
        }
        
        if user?.currentTypeOfUser == .guardian{
            if diaryEntry.isExpanded {
                cell.contentLabel.text = diaryEntry.contentString
                var imageArray = [UIImageView]()
                
                for urlValue in diaryEntry.AudioArray{
                    var image = UIImage()
                    if urlValue.type == "Audio" {
                        image = UIImage(named: "audioIcon")!
                    }
                    
                    let imagv = CustomImageView(image: image)
                    
                    weak var weakImageVc  = imagv
                    ImageAPi.fetchImageForUrl(urlString: urlValue.url, oneSuccess: { (image) in
                        if weakImageVc != nil { weakImageVc!.image = image }
                    }, onFailure: { (error) in
                        
                    })
                    imagv.contentMode = .scaleAspectFit
                    imagv.url = urlValue.url
                    imagv.name = urlValue.name
                    imageArray.append(imagv)
                    
                    let audioTap = UITapGestureRecognizer(target: self, action: #selector(self.loadAudio(_:)))
                    imagv.addGestureRecognizer(audioTap)
                    imagv.isUserInteractionEnabled = true
                }
                
                
                for urlString in diaryEntry.imageArray{
                    //let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
                    let image = UIImage(named: "ApplicationLogo")
                    let imagv = CustomImageView(image: image)
                    
                    weak var weakImageVc  = imagv
                    ImageAPi.fetchImageForUrl(urlString: urlString, oneSuccess: { (image) in
                        if weakImageVc != nil { weakImageVc!.image = image }
                    }, onFailure: { (error) in
                        
                    })
                    imagv.contentMode = .scaleAspectFit
                    imageArray.append(imagv)
                    
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageExpanded(_:)))
                    imagv.addGestureRecognizer(tap)
                    imagv.isUserInteractionEnabled = true
                }
                
                for urlString in diaryEntry.pdfArray{
                    var image = UIImage()
                    if urlString.type == "PDF" {
                        image = UIImage(named: "pdfIcon")!
                    }
                    if urlString.type == "DOC" || urlString.type == "DOCX"  {
                        image = UIImage(named: "wordIcon")!
                    }
                    
                    
                    let imagv = CustomImageView(image: image)
                    
                    weak var weakImageVc  = imagv
                    ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                        if weakImageVc != nil { weakImageVc!.image = image }
                    }, onFailure: { (error) in
                        
                    })
                    imagv.contentMode = .scaleAspectFit
                    imagv.url = urlString.url
                    imagv.name = urlString.name
                    imageArray.append(imagv)
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.loadPdf(_:)))
                    imagv.addGestureRecognizer(tap)
                    imagv.isUserInteractionEnabled = true
                    
                }
                
                let height = cell.imageContainerView.addImagesToMeFromArray(imageArray: imageArray)
                cell.imageContainerHeight.constant = height
                cell.downLoadButtonHeight.constant = 22.0
                
                
            }else {
                cell.contentLabel.text             = nil
                cell.imageContainerHeight.constant = 0
                cell.downLoadButtonHeight.constant = 0
            }
        }
        else{
            cell.contentLabel.text             = nil
            var imageArray = [UIImageView]()
            
            for urlValue in diaryEntry.AudioArray{
                var image = UIImage()
                if urlValue.type == "Audio" {
                    image = UIImage(named: "audioIcon")!
                }
                
                let imagv = CustomImageView(image: image)
                
                weak var weakImageVc  = imagv
                ImageAPi.fetchImageForUrl(urlString: urlValue.url, oneSuccess: { (image) in
                    if weakImageVc != nil { weakImageVc!.image = image }
                }, onFailure: { (error) in
                    
                })
                imagv.contentMode = .scaleAspectFit
                imagv.url = urlValue.url
                imagv.name = urlValue.name
                imageArray.append(imagv)
                
                let audioTap = UITapGestureRecognizer(target: self, action: #selector(self.loadAudio(_:)))
                imagv.addGestureRecognizer(audioTap)
                imagv.isUserInteractionEnabled = true
            }
            
            
            
            for urlString in diaryEntry.imageArray{
                //let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
                let image = UIImage(named: "ApplicationLogo")
                let imagv = CustomImageView(image: image)
                
                weak var weakImageVc  = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString, oneSuccess: { (image) in
                    if weakImageVc != nil { weakImageVc!.image = image }
                }, onFailure: { (error) in
                    
                })
                imagv.contentMode = .scaleAspectFit
                imageArray.append(imagv)
                
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageExpanded(_:)))
                imagv.addGestureRecognizer(tap)
                imagv.isUserInteractionEnabled = true
            }
            for urlString in diaryEntry.pdfArray{
                var image = UIImage()
                if urlString.type == "PDF" {
                    image = UIImage(named: "pdfIcon")!
                }
                if urlString.type == "DOC" || urlString.type == "DOCX"  {
                    image = UIImage(named: "wordIcon")!
                }
                
                let imagv = CustomImageView(image: image)
                
                weak var weakImageVc  = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                    if weakImageVc != nil { weakImageVc!.image = image }
                }, onFailure: { (error) in
                    
                })
                imagv.contentMode = .scaleAspectFit
                imagv.url = urlString.url
                imagv.name = urlString.name
                imageArray.append(imagv)
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.loadPdf(_:)))
                imagv.addGestureRecognizer(tap)
                imagv.isUserInteractionEnabled = true
                
            }
            let height = cell.imageContainerView.addImagesToMeFromArray(imageArray: imageArray)
            cell.imageContainerHeight.constant = height
            cell.downLoadButtonHeight.constant = 22.0
            
            
        }
        
        setDateVisibilityForCell(cell: cell, forindex: indexPath, andArrayCount: filteredDiaryEntryListingDictionary[key]!.count)
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            cell.titleLabel.textColor = UIColor.black
        }
        else{
            cell.setEntryRead(isread: diaryEntry.isRead)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if user?.currentTypeOfUser == .guardian{
            Analytics.logEvent("P_diary_diaryView", parameters: nil)
            let key = keyArray[indexPath.section]
            let diaryEntry = filteredDiaryEntryListingDictionary[key]![indexPath.row]
            if diaryEntry.isExpanded == true {
                diaryEntry.isExpanded = false
            }else {
                diaryEntry.isExpanded = true
            }
            if diaryEntry.isRead == false {
                diaryEntry.isRead = true
                makeApiCallMakeDiaryWithIdRead(diaryEntry.diaryId)
            }
            
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
            
        else {
            let key          = keyArray[indexPath.section]
            
            let diary = filteredDiaryEntryListingDictionary[key]![indexPath.row]
            self.performSegue(withIdentifier: segueConstants.segueForLoadingDiaryDetails, sender: diary)
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0 {
            return nil
        }else {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
            headerView.backgroundColor = UIColor.clear
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //zero returned here because as per the design the first section does not need a section header.
        if section == 0{
            return 0.0
        }else {
            return 10
        }
    }
    
    //MARK:- Show Expanded Image
    
    @objc func showImageExpanded(_ tapgesture:UITapGestureRecognizer) -> Void {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        
        let parentImageView =  tapgesture.view as! UIImageView
        
        let array = Bundle.main.loadNibNamed("PhotoShowingView", owner: self, options: nil)
        let photoView             = array![0] as! PhotoShowingView
        photoView.imageView.image = parentImageView.image
        photoView.frame           = self.view.frame
        self.view.addSubview(photoView)
    }
    
    
    //MARK: Webservice call
    
    @objc private func loadWebServiceFromBottomRefresh(){
        diaryEntryListingTable.bottomRefreshControl!.endRefreshing()
        self.makeWebApiCallToGetDiaryEntries()
    }
    @objc private func loadWebServiceFromTopRefreshControl(_ refreshControl:UIRefreshControl){
        refreshControl.endRefreshing()
        clearDataForThisPage()
        self.makeWebApiCallToGetDiaryEntries()
    }
    
    private func makeApiCallMakeDiaryWithIdRead(_ diaryReadId:String){
        
        
        var unreadnum = DBController().getDiaryUnreadNumber()
        if unreadnum != 0 {
            unreadnum = unreadnum - 1
            _ = DBController().updateDiaryUnread(String(unreadnum))
        }
        
        let tabbar = self.tabBarController as! ParentEyeTabbar
        tabbar.setDiaryReadNumber()
        
        
        let studentId = user?.getSelectedEmployeeOrStudentId()
        if studentId != nil {
            WebServiceApi.makeDiaryEntryRead(diaryId: diaryReadId, onSuccess: { (result) in
                
            }, onFailure: { (error) in
                
            }, duringProgress: { (progress) in
                
            })
        }
    }
    
    private func makeWebApiCallToGetDiaryEntries(){
        var selectedUser:String = ""
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            selectedUser = "Employee"
        }
        else{
            studentId = (user?.getSelectedEmployeeOrStudentId())!
            selectedUser = "Guardian"
        }
        if !studentId.isEmpty || classId != 0
        {
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.getDiaryEntry(studentId: studentId, classId: classId ,selectedUser: selectedUser,onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                
                let resultTuple =  parseDiary.parseFromDictionary(dictionary: result)
                if let err =  resultTuple.1{
                    AlertController.showToastAlertWithMessage(message: err.localizedDescription, stateOfMessage: .failure)
                }else {
                    weakSelf!.addEntriesToDictionary(resultTuple.0)
                }
                
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
            }, forPage: page)
        }
    }
    
    
    @objc private func loadPdf(_ tapgesture:UITapGestureRecognizer) {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        moveToPdf = true
        pdfViewer.showPdfInViewController(vc: self, pdfHavingUrl: parentImageView.url, withName: parentImageView.name)
        
    }
    
    @objc private func loadAudio(_ tapgesture:UITapGestureRecognizer) {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        print("audio tapped")
        
        
        if let audioUrl = URL(string: parentImageView.url) {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileName = parentImageView.url
            if fileName.range(of: "opus") != nil{
                print("exists")
                replacedString = (fileName as NSString).replacingOccurrences(of: "opus", with: "mp3")
            }
            
            
            let fileUrl = URL(fileURLWithPath: replacedString as String)
            // lets create your destination file url
            destinationUrl = documentsDirectoryURL.appendingPathComponent(fileUrl.lastPathComponent ?? "audio.mp3") as NSURL
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager().fileExists(atPath: destinationUrl.path!) {
                print("The file already exists at path")
                self.setupAudioPlay()
                self.audioPlayView.toPass = self.destinationUrl
                self.audioPlayView.isFileExistAtLocal = true
                // if the file doesn't exist
            } else {
                
                self.audioPlayView.isFileExistAtLocal = false
                //let webServiceUrl = NSURL(string: webServiceUrls.baseURl+"convertAudioPlay/uniqueName/"+parentImageView.name)
                weak var weakSelf = self
                hudControllerClass.showNormalHudToViewController(viewController: self)
                WebServiceApi.getAudioPlayUrl(name: parentImageView.name,onSuccess: { (result) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    if let playUrl = result["convertAudioPlay"] as? String {
                        print("inside audioplay")
                        print(playUrl)
                        
                        //self.audioPlayView.downloadUrl = playUrl
                        //self.audioPlayView.destinationUrl = self.destinationUrl
                        let url = URL(string: playUrl)
                        self.setupAudioPlay()
                        self.downloadFileFromURL(url!)
                    }
                    
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                    
                }) { (progress) in
                    
                }
            }
            
        }
        
        // to check if it exists before downloading it
        //print(webServiceUrl!)
        
        
        
    }
    
    
    func downloadFileFromURL(_ url:URL){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            guard URL != nil && error == nil else {
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                print("failed")
                return
            }
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self?.play(url: URL!)
        })
        
        downloadTask.resume()
        
    }
    
    func play(url:URL) {
        
        do {
            try FileManager().moveItem(at: url, to: self.destinationUrl as URL)
            
            print("File moved to documents folder")
            print(url)
            print(self.destinationUrl)
            self.audioPlayView.toPass = self.destinationUrl
            self.audioPlayView.destinationUrl = self.destinationUrl
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
        }
    }
    
    
    func setupAudioPlay() {
        let array = Bundle.main.loadNibNamed("audioPlay", owner: self, options: nil)
        audioPlayView             = array![0] as! audioPlay
        
        audioPlayView.center = CGPoint(x: self.view.frame.size.width  / 2,
                                       y: self.view.frame.size.height / 2);
        audioPlayView.frame           = self.view.frame
        audioPlayView.delegate = self
        audioPlayView.soundSlider.value = 0.0
        audioPlayView.playAudioButton.setImage(UIImage(named: "playIcon"), for: UIControlState())
        audioPlayView.initialLoad = true
        audioPlayView.playTimeText.text = "00.00"
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.view.addSubview(audioPlayView)
    }
    
    //MARK: text field delegates
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        searchDiaryWithText(sender.text!)
        
    }
    
    
    //MARK:- Header
    /**
     Function to update the header accoding to the screen we are in .
     */
    private func alterHeader(){
        
        header!.delegate = self
        if user?.currentTypeOfUser == .guardian{
            header.setUpHeaderForRootScreensWithNameOfScreen(name: ConstantsInUI.diaryScreenHeading, withProfileImageUrl: user!.getSelectedEmployeeOrStudentProfileImageUrl())
        }
        else{
            if user?.currentTypeOfUser == .employee {
                if user!.switchAs == ""{
                    header.setUPHeaderForEmployee(name: ConstantsInUI.diaryScreenHeading)
                }
                else if user!.switchAs == "Guardian"{
                    header.setUPHeaderForMultiRole(name: ConstantsInUI.diaryScreenHeading)
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                header.setupHeaderForCorporateManager(name: (user?.getStudentCurrentlySelectedSchool()?.schoolName)!)
                
            }
        }
    }
    
    
    /**
     function used here for initiating the search
     */
    //MARK:Search
    
    private func searchDiaryWithText(_ searchText:String){
        
        self.filteredDiaryEntryListingDictionary.removeAll()
        if searchText == ""{
            self.filteredDiaryEntryListingDictionary = diaryEntryListingDictionary
            self.diaryEntryListingTable.reloadData()
            return
        }
        
        let dayPredicate     = NSPredicate(format: "dayString CONTAINS[cd] %@", searchText)
        let monthPredicate   = NSPredicate(format: "monthString CONTAINS[cd] %@", searchText)
        let yearPredicate    = NSPredicate(format: "yearString CONTAINS[cd] %@", searchText)
        let titlePredicate   = NSPredicate(format: "title CONTAINS[cd] %@", searchText)
        let contentPredicate = NSPredicate(format: "contentString CONTAINS[cd] %@", searchText)
        
        let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [dayPredicate,monthPredicate,yearPredicate,titlePredicate,contentPredicate])
        
        
        for (key, value) in diaryEntryListingDictionary{
            
            let filteredAray = value.filter{compoundPredicate.evaluate(with: $0)}
            self.filteredDiaryEntryListingDictionary[key] = filteredAray
        }
        self.diaryEntryListingTable.reloadData()
        
    }
    
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        self.tabBarController?.slideMenuController()?.openLeft()
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
        
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!) // reflecting the change in DB
        clearDataForThisPage()
        makeWebApiCallToGetDiaryEntries()
        
        let tab = self.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        if schoolId != user?.schools![(user?.currentSchoolSelected)!].schoolId {
            clearDataForThisPage()
            getClassListForCorporateManager()
        }
    }
    
    
    private func getClassListForCorporateManager() {
        var schoolId:Int = 0
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        schoolId = user!.schools![(user?.currentSchoolSelected)!].schoolId
        WebServiceApi.getClassListOfCorporateManager(schoolId: String(schoolId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let classSubjectListArray = result["classSubjectList"] as? [[String:AnyObject]]{
                self.classList = [classSubjectList]()
                for dic in classSubjectListArray{
                    let value = classSubjectList(fromDictionary: dic as NSDictionary)
                    self.classList.append(value)
                }
            }
            if let employeeclassList = self.classList as [classSubjectList]?{
                self.Classes?.removeAll()
                for classListOfEmployee in employeeclassList{
                    if let cls =  EmployeeClassModel.init(fromClassList: classListOfEmployee) {
                        if self.Classes == nil{
                            self.Classes = [EmployeeClassModel]()
                        }
                        self.Classes?.append(cls)
                    }
                }
            }
            self.user?.Classes?.removeAll()
            self.user?.Classes = self.Classes
            DBController().addUserToDB(self.user!)
            self.user = DBController().getUserFromDB()
            self.clearDataForThisPage()
            self.setupCountOfItems()
            self.diaryCollectionView.reloadData()
            self.makeWebApiCallToGetDiaryEntries()
            let tab = self.tabBarController as! ParentEyeTabbar
            tab.getUnreadCount()
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }
    
    //MARK:- array
    private func addEntriesToDictionary(_ dictionary:[String:[DiaryModelClass]]){
        
        filteredDiaryEntryListingDictionary.addEntriesFromDictionary(otherDictionary: dictionary)
        diaryEntryListingDictionary = filteredDiaryEntryListingDictionary
        keyArray = [String](filteredDiaryEntryListingDictionary.keys)
        keyArray.sortDateStringArrayBasedOnDateWithDateFormat(.ddMMyyyyWithMonthNameWithHyphen)
        
        let totalAndunread = getUnreadAndReadMessage()
        page = totalAndunread.total
        if user?.currentTypeOfUser == .guardian{
         if totalAndunread.unRead > DBController().getDiaryUnreadNumber(){
            let unreadCount = String(totalAndunread.unRead)
            _ = DBController().updateDiaryUnread(unreadCount)
            (self.tabBarController as! ParentEyeTabbar).setDiaryReadNumber()
         }
        }
        diaryEntryListingTable.reloadData()
    }
    
    /**
     Function to get the total read an unread messages in filtered array
     
     - returns: A tuple having read and unread messages
     */
    
    func getUnreadAndReadMessage() -> (total:Int,unRead:Int) {
        var totalMessages = 0
        var totalRead     = 0
        
        for (_,array) in filteredDiaryEntryListingDictionary {
            let readArray = array.filter({$0.isRead})
            totalMessages = totalMessages + array.count
            totalRead     = totalRead + readArray.count
        }
        return (page+20, totalMessages-totalRead)
    }
    //MARK:- micellanious
    /**
     Clears data in this page
     */
    private func clearDataForThisPage(){
        filteredDiaryEntryListingDictionary.removeAll()
        diaryEntryListingDictionary.removeAll()
        keyArray.removeAll()
        diaryEntryListingTable.reloadData()
        alterHeader()
        page = 0
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueConstants.segueForLoadingComposeDiary {
            let composeDiary        = segue.destination as! ComposeDiary
            composeDiary.userId     = (user?.currentUserRoleId)!
            composeDiary.defaultClassSelected = defaultClassesList
            diaryDetailedClass = true
            Analytics.logEvent("TP_diary_compose", parameters: nil)
            
        }
        else if segue.identifier == segueConstants.segueForLoadingDiaryDetails {
            if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
                let Diarydetails        = segue.destination as! SchoolDiaryDetailsVC
                let dia                 = sender as! DiaryModelClass
                Diarydetails.diaryId   =  dia.diaryId
                Diarydetails.defaultClassId = classId
                Diarydetails.className = className
                Diarydetails.content     = dia.contentString
                Diarydetails.subject     = dia.subject
                Diarydetails.imageArray   = dia.imageArray
                Diarydetails.pdfArray     = dia.pdfArray
                Diarydetails.AudioArray     = dia.AudioArray
                Diarydetails.submissionDate = dia.dateString
                Diarydetails.attachmentArray = dia.attachmentArray
                Diarydetails.diaryReciepientArray = defaultClassesList
                
                diaryDetailedClass = true
                FireBaseKeys.FirebaseKeysRuleFour(value: "diary_diaryView")
            }
        }
    }
    
    @objc private func selectedclassAction(_ sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState())
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState())
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        className = ClassesList[sender.tag].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        diaryCollectionView.reloadData()
        clearDataForThisPage()
        makeWebApiCallToGetDiaryEntries()
    }
    
    @IBAction func showComposeDiaryScreen() {
        self.performSegue(withIdentifier: segueConstants.segueForLoadingComposeDiary, sender: nil)
    }
    func SelectedClassListId(messageReciepientArray:[EmployeeClassModel]){
        diaryReciepientArray = messageReciepientArray
    }
    
    func exitSubView() {
        audioPlayView.removeFromSuperview()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    
}


