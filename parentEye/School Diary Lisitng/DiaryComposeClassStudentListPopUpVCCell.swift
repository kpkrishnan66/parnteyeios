//
//  DiaryComposeClassStudentListPopUpVCCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class DiaryComposeClassStudentListPopUpVCCell: UITableViewCell {
    
    var isTeacherSelected = false
    
    @IBOutlet var studentName: UILabel!
    @IBOutlet weak var classAndSchoolName: UILabel!
    
    @IBOutlet var profilePic: UIImageView!
    @IBOutlet var selectionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.profilePic?.image = UIImage(named: "avatar")
        self.classAndSchoolName?.text = ""
        
    }
    
    //function to set the value externally
    func setTeacherSelected(selected:Bool){
        isTeacherSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isTeacherSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
        
        isTeacherSelected = !isTeacherSelected
    }
    
    
}


