//
//  schoolDiaryDeliveryReportCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 26/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class schoolDiaryDeliveryReportCell: UITableViewCell {

    
    @IBOutlet var diaryDetailedProfilePic: UIImageView!
    @IBOutlet var callIcon: UIButton!

    @IBOutlet var nameofStudent: UILabel!

    @IBOutlet var className: UILabel!
    @IBOutlet var diaryDate: UILabel!
    @IBOutlet var diaryTime: UILabel!

    @IBOutlet weak var readViaSms: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.diaryDetailedProfilePic?.image = UIImage(named: "avatar")
        self.nameofStudent?.text = ""
        self.className?.text = ""
        self.diaryDate?.text = ""
        self.diaryTime?.text = ""
        self.readViaSms?.text = ""
    }
}
