//
//  DiaryComposeClassListPopUpVCCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 21/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class DiaryComposeClassListPopUpVCCell: UITableViewCell {
    
    var isTeacherSelected = false
    
    @IBOutlet var className: UILabel!
    @IBOutlet var selectionButton: UIButton!
    
    @IBOutlet weak var schoolName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //function to set the value externally
    func setTeacherSelected(selected:Bool){
        isTeacherSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isTeacherSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
        
        isTeacherSelected = !isTeacherSelected
    }
    

}

