//
//  DiaryComposeClassStudentListPopUpVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

import UIKit

protocol diaryComposeClassStudentListProtocol:class  {
    
    func updateSelectedWithStudentArray(receipientArray:[composeDiaryStudentPopupList])
}


class DiaryComposeClassStudentListPopUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate
{
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var classListTable: UITableView!
    @IBOutlet var AllorNone: UIButton!
    @IBOutlet var numberOfClassSelected: UILabel!
    @IBOutlet var submitButton: UIButton!
    
    var recipientArray = [composeDiaryStudentPopupList]()
    var searchrecipientArray = [composeDiaryStudentPopupList]()
    var temprecipientArray = [composeDiaryStudentPopupList]()
    var previousEntryArray = [composeDiaryStudentPopupList]()// an array to store the previous entered details
    var search: Bool = false
    weak var delegate:diaryComposeClassStudentListProtocol?
    var canDismiss = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        canDismiss = false
        searchrecipientArray = recipientArray
        temprecipientArray = recipientArray
        iterateArrayForFindingSelected()
        classListTable.register(UINib(nibName: "DiaryComposeClassStudentListPopUpVCCell", bundle: nil), forCellReuseIdentifier: "DiaryComposeClassStudentListPopUpVCCell")
        classListTable.estimatedRowHeight = 76.0
        classListTable.rowHeight          = UITableViewAutomaticDimension
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.enablesReturnKeyAutomatically = false
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if recipientArray.count == 0{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.recipientNone, stateOfMessage: .failure)
        }
        return recipientArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DiaryComposeClassStudentListPopUpVCCell") as! DiaryComposeClassStudentListPopUpVCCell
        let user = recipientArray[indexPath.row]
        
        cell.setTeacherSelected(selected: user.selectedInUI)
        cell.studentName.text = user.studentName
        cell.classAndSchoolName.text = user.classNameToDisplay
        
        weak var profileImage = cell.profilePic
        
        if user.compressedProfilePic == ""{
            cell.profilePic.image = UIImage(named: "avatar")
            cell.profilePic.layer.masksToBounds = true
            cell.profilePic.layer.cornerRadius = 0
            cell.profilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: user.compressedProfilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.profilePic.layer.masksToBounds = false
                    cell.profilePic.layer.cornerRadius = cell.profilePic.frame.height/2
                    cell.profilePic.clipsToBounds = true
                    
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        
        cell.selectionButton?.addTarget(self, action: #selector(DiaryComposeClassStudentListPopUpVC.changeSelection(_:)), for: .touchUpInside)
        cell.selectionButton?.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let button = UIButton()
        button.tag = indexPath.row
        changeSelection(button)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- updateSelected in UI
    
    @objc fileprivate func changeSelection(_ sender:UIButton){
        
        if recipientArray[sender.tag].selectedInUI == true {
            recipientArray[sender.tag].selectedInUI = false
        }else {
            recipientArray[sender.tag].selectedInUI = true
        }
        
        classListTable.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .middle)
        alterElementsInPreviousArrayToSelected(!recipientArray[sender.tag].selectedInUI , userTobeAltered: recipientArray[sender.tag])
        setAddedLabel()
        
    }
    
    /**
     function to add / alter elements to the previous array
     
     - parameter isSelected: check if the entry is selected
     - parameter user:       user object that is to be altered
     */
    fileprivate func alterElementsInPreviousArrayToSelected(_ isSelected:Bool, userTobeAltered user:composeDiaryStudentPopupList){
        
        var index:Int?
        for i in 0 ..< previousEntryArray.count {
            if previousEntryArray[i].studentId == user.studentId{
                index = i
            }
            
        }
        
        if let ind = index {
            
            if !isSelected{
                previousEntryArray[ind].selectedInUI = isSelected
            }else {
                previousEntryArray.remove(at: ind)
            }
        }else {
            user.selectedInUI = true
            previousEntryArray.append(user)
        }
        
    }
    
    fileprivate func iterateArrayForFindingSelected(){
        var count:Int = 0
        for i in 0 ..< recipientArray.count {
            for j in 0 ..< previousEntryArray.count {
                if previousEntryArray[j].studentId == recipientArray[i].studentId{
                    previousEntryArray[j].selectedInUI = true
                    recipientArray[i].selectedInUI = true
                    count = count + 1
                }
                
            }
        }
        if recipientArray.count == count{
            AllorNone.setTitle("None", for: UIControlState())
        }
        else{
            AllorNone.setTitle("All", for: UIControlState())
        }
        
        setAddedLabel()
        
    }
    
    
    
    fileprivate func setAddedLabel(){
        if search == false{
            let unreadArray = recipientArray.filter{$0.selectedInUI}
            numberOfClassSelected.text = String(unreadArray.count)
        }
        else{
            let unreadArray = temprecipientArray.filter{$0.selectedInUI}
            numberOfClassSelected.text = String(unreadArray.count)
            
        }
        
    }
    
    override func dismiss(animated flag: Bool,
                          completion: (() -> Void)?)
    {
        if canDismiss == true{
            canDismiss = false
            super.dismiss(animated: flag, completion:completion)
        }
        
        // Your custom code here...
    }
    
    
    
    @IBAction func selectAllorNone(_ sender: AnyObject) {
        
        if recipientArray.count != 0
        {
            if AllorNone.currentTitle == "All"  {
                
                for i in 0 ..< recipientArray.count {
                    if recipientArray[i].selectedInUI != true
                    {
                        recipientArray[i].selectedInUI = true
                        alterElementsInPreviousArrayToSelected(!recipientArray[i].selectedInUI , userTobeAltered: recipientArray[i])
                    }
                }
                AllorNone.setTitle("None", for: UIControlState())
                setAddedLabel()
                self.classListTable.reloadData()
                
            }
            else{
                
                for i in 0 ..< recipientArray.count {
                    if recipientArray[i].selectedInUI != false{
                        recipientArray[i].selectedInUI = false
                        alterElementsInPreviousArrayToSelected(!recipientArray[i].selectedInUI , userTobeAltered: recipientArray[i])
                    }
                    
                }
                AllorNone.setTitle("All", for: UIControlState())
                setAddedLabel()
                self.classListTable.reloadData()
            }
            
            
        }
        
    }
    
    @IBAction func dismissMe(_ sender: AnyObject) {
        
        canDismiss = true
        self.delegate?.updateSelectedWithStudentArray(receipientArray: previousEntryArray)
        self.dismiss(animated: false, completion: nil)
    }

    //search events
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            recipientArray = temprecipientArray
        }else{
            let filtered = temprecipientArray.filter { $0.studentName.contains(searchText,options: .caseInsensitive) }
            recipientArray = filtered
        }
        classListTable.reloadData()
    }
    
}

