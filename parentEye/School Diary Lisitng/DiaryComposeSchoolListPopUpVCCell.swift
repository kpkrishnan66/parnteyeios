//
//  DiaryComposeSchoolListPopUpVCCell.swift
//  parentEye
//
//  Created by Vivek on 09/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class DiaryComposeSchoolListPopUpVCCell: UITableViewCell {
    
    var isSchoolSelected = false
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var selectionButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //function to set the value externally
    func setTeacherSelected(_ selected:Bool){
        isSchoolSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: UIControlState())
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: UIControlState())
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isSchoolSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: UIControlState())
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: UIControlState())
        }
        
        isSchoolSelected = !isSchoolSelected
    }
    
    
}

