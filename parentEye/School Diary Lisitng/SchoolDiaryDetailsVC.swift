 //
//  SchoolDiaryDetailsVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 25/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class  SchoolDiaryDetailsVC: UIViewController,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,SelectedSchoolpopOverDelegate,audioPlayBackProtocol {
    
    var user:userModel?
    var profilePic = ""
    var diaryDetailsArray = [DiaryModelClass]()
    var content = ""
    var subject = ""
    var submissionDate = ""
    var imageArrayDetailed       = [UIImageView]()
    var imageArray               = [String]()
    var pdfArray                 = [PdfModel]()
    var classId:Int = 0
    var schoolId:Int = 0
    var className:String = ""
    var mobileNumber = ""
    var lines  = 0
    let reuseIdentifier = "cell"
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var ClassIsSelected = [Int: Bool]()
    var diaryId = ""
    var defaultClassId = 0
    lazy var diaryDetaileddeliveryReportArray = [DiaryModelClass]()
    lazy var readArray = [DiaryModelClass]()
    lazy var unreadArray = [DiaryModelClass]()
    
    lazy var diaryDetailedclassListArray = [DiaryModelClass]()
    var sendButtonPressed = true
    var readButtonPressed = false
    var unReadButtonPressed = false
    var pdfViewer = PdfController()
    var audioPlayView  = audioPlay()
    var destinationUrl = NSURL()
    var replacedString:String = ""
    var AudioArray = [AudioModel]()
    
    
    lazy var attachmentArray = [uploadAttachmentModel]()
    lazy var imageAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    var diaryReciepientArray:[EmployeeClassModel] = []
    
    @IBOutlet var unReadButton: UIButton!
    @IBOutlet var readButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var DiaryDetailedTableview: UITableView!
    @IBOutlet var diaryDetailedCollectionView: UICollectionView!
    @IBOutlet var diaryDetailedClassCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var diarySubject: UILabel!
    @IBOutlet var diaryContent: UILabel!
    @IBOutlet var diaryViewHeight: NSLayoutConstraint!
    @IBOutlet var forwardButtonHeight: NSLayoutConstraint!
    @IBOutlet var forwardDiaryButton: UIButton!
    @IBOutlet var diaryDetailedAttachmentView: ExpansiveView!
    @IBOutlet var diaryAttachmentViewHeight: NSLayoutConstraint!
    @IBOutlet var diaryMainViewHeight: NSLayoutConstraint!
    
    //MARK:- View delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        makeWebApiCallToGetDiaryDetailedEntries()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        initializeTableView()
    }
    override func viewDidDisappear(_ animated: Bool) {
        print("disappear")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    fileprivate func setupUI() -> Void {
        diaryContent.text = content
        diarySubject.text = subject
        diaryContent.numberOfLines = 0
        diaryContent.lineBreakMode = NSLineBreakMode.byWordWrapping
        diaryContent.sizeToFit()
        lines = countLabelLines(diaryContent)
        
        diaryViewHeight.constant = diaryViewHeight.constant + (CGFloat(lines * 20) - 20)
        diaryMainViewHeight.constant = diaryMainViewHeight.constant + diaryViewHeight.constant
        if user?.currentTypeOfUser == .guardian{
            forwardButtonHeight.constant=0
        }
        forwardDiaryButton.addTarget(self, action: #selector(SchoolDiaryDetailsVC.forwardDiary), for: .touchUpInside)
        
        
        for urlValue in self.AudioArray{
            var image = UIImage()
            if urlValue.type == "Audio" {
                image = UIImage(named: "audioIcon")!
            }
            
            let imagv = CustomImageView(image: image)
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlValue.url, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imagv.url = urlValue.url
            imagv.name = urlValue.name
            imageArrayDetailed.append(imagv)
            
            let audioTap = UITapGestureRecognizer(target: self, action: #selector(self.loadAudio(_:)))
            imagv.addGestureRecognizer(audioTap)
            imagv.isUserInteractionEnabled = true
        }
        
        for urlString in self.imageArray{
            let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlString, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imageArrayDetailed.append(imagv)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageExpandedForDetailedDiary(_:)))
            imagv.addGestureRecognizer(tap)
            imagv.isUserInteractionEnabled = true
            
        }
        for urlString in pdfArray{
            var image = UIImage()
            if urlString.type == "PDF" {
                image = UIImage(named: "pdfIcon")!
            }
            if urlString.type == "DOC" || urlString.type == "DOCX"  {
                image = UIImage(named: "wordIcon")!
            }
            
            let imagv = CustomImageView(image: image)
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imagv.url = urlString.url
            imagv.name = urlString.name
            imageArrayDetailed.append(imagv)
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.loadPdf(_:)))
            imagv.addGestureRecognizer(tap)
            imagv.isUserInteractionEnabled = true
            
        }
        
        if imageArrayDetailed.count > 0 {
            diaryAttachmentViewHeight.constant = diaryDetailedAttachmentView.addImagesToMeFromArrayForDiaryDetailed(imageArray: imageArrayDetailed)
            diaryViewHeight.constant = diaryViewHeight.constant + (diaryAttachmentViewHeight.constant)
            diaryMainViewHeight.constant = diaryMainViewHeight.constant + diaryViewHeight.constant
        }
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        
    }
    @objc fileprivate func forwardDiary()
    {
        self.performSegue(withIdentifier: segueConstants.segueForDiaryForward, sender: nil)
    }
    
    
    fileprivate func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.diaryScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    
    fileprivate func initializeTableView(){
        DiaryDetailedTableview.register(UINib(nibName: "schoolDiaryDeliveryReportCell", bundle: nil), forCellReuseIdentifier: "schoolDiaryDeliveryReportCell")
        DiaryDetailedTableview.estimatedRowHeight = 65.0
        DiaryDetailedTableview.rowHeight          = UITableViewAutomaticDimension
    }
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    //collectionview delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        if diaryDetailedclassListArray.count > 1 {
            return 1
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return diaryDetailedclassListArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SchoolDiaryDetailedClassCollectionView
        
        if ClassIsSelected[indexPath.row] == false{
            cell.classSelected.backgroundColor = UIColor.clear
            cell.classSelected.setTitleColor(UIColor.black, for: UIControlState())
            cell.classSelected.layer.borderWidth = 1
            cell.classSelected.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.classSelected.backgroundColor = colur
            cell.classSelected.setTitleColor(UIColor.white, for: UIControlState())
            cell.classSelected.layer.borderWidth = 0
        }
        cell.classSelected.setTitle(diaryDetailedclassListArray[indexPath.row].diaryclassName, for: UIControlState())
        cell.classSelected.layer.cornerRadius = 18
        cell.classSelected.layer.borderWidth = 1
        cell.classSelected.layer.borderColor = UIColor.black.cgColor
        cell.classSelected.tag = indexPath.row
        cell.classSelected.addTarget(self, action: #selector(SchoolDiaryDetailsVC.selectedclassAction(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    //MARK:- TAbleview delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if readButtonPressed == true{
            return readArray.count
        }
        if unReadButtonPressed == true{
            return unreadArray.count
        }
        
        
        return diaryDetaileddeliveryReportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "schoolDiaryDeliveryReportCell") as! schoolDiaryDeliveryReportCell
        if readButtonPressed == true{
            let read = readArray[indexPath.row]
            cell.nameofStudent.text = read.deliveryReportstudentName
            cell.className.text = className
            weak var profileImage = cell.diaryDetailedProfilePic
            
            if read.deliveryReportprofilePic == ""{
                cell.diaryDetailedProfilePic.image = UIImage(named: "avatar")
                cell.diaryDetailedProfilePic.layer.masksToBounds = true
                cell.diaryDetailedProfilePic.layer.cornerRadius = 0
                cell.diaryDetailedProfilePic.clipsToBounds = false
                
            }
            else{
                ImageAPi.fetchImageForUrl(urlString: read.deliveryReportprofilePic, oneSuccess: { (image) in
                    if profileImage != nil {
                        
                        cell.diaryDetailedProfilePic.layer.masksToBounds = false
                        cell.diaryDetailedProfilePic.layer.cornerRadius = cell.diaryDetailedProfilePic.frame.height/2
                        cell.diaryDetailedProfilePic.clipsToBounds = true
                        
                        profileImage!.image = image
                        
                    }
                }) { (error) in
                    
                }
            }
            cell.diaryDate.text = read.deliveryReportupdatedDate
            cell.diaryTime.text = read.deliveryReportupdatedTime
            cell.readViaSms.text = read.readVia
            
            
        }
        else if unReadButtonPressed == true{
            let unRead = unreadArray[indexPath.row]
            cell.nameofStudent.text = unRead.deliveryReportstudentName
            cell.className.text = className
            weak var profileImage = cell.diaryDetailedProfilePic
            
            if unRead.deliveryReportprofilePic == ""{
                cell.diaryDetailedProfilePic.image = UIImage(named: "avatar")
                cell.diaryDetailedProfilePic.layer.masksToBounds = true
                cell.diaryDetailedProfilePic.layer.cornerRadius = 0
                cell.diaryDetailedProfilePic.clipsToBounds = false
                
            }
            else{
                ImageAPi.fetchImageForUrl(urlString: unRead.deliveryReportprofilePic, oneSuccess: { (image) in
                    if profileImage != nil {
                        
                        cell.diaryDetailedProfilePic.layer.masksToBounds = false
                        cell.diaryDetailedProfilePic.layer.cornerRadius = cell.diaryDetailedProfilePic.frame.height/2
                        cell.diaryDetailedProfilePic.clipsToBounds = true
                        profileImage!.image = image
                        
                    }
                }) { (error) in
                    
                }
            }
            cell.diaryDate.text = unRead.deliveryReportaddedDate
            cell.diaryTime.text = unRead.deliveryReportaddedDate
            cell.readViaSms.text = unRead.readVia
            
            
        }
        else{
            cell.nameofStudent.text = diaryDetaileddeliveryReportArray[indexPath.row].deliveryReportstudentName
            cell.className.text = className
            weak var profileImage = cell.diaryDetailedProfilePic
            
            if diaryDetaileddeliveryReportArray[indexPath.row].deliveryReportprofilePic == ""{
                cell.diaryDetailedProfilePic.image = UIImage(named: "avatar")
                cell.diaryDetailedProfilePic.layer.masksToBounds = true
                cell.diaryDetailedProfilePic.layer.cornerRadius = 0
                cell.diaryDetailedProfilePic.clipsToBounds = false
                
            }
            else{
                ImageAPi.fetchImageForUrl(urlString: diaryDetaileddeliveryReportArray[indexPath.row].deliveryReportprofilePic, oneSuccess: { (image) in
                    if profileImage != nil {
                        
                        cell.diaryDetailedProfilePic.layer.masksToBounds = false
                        cell.diaryDetailedProfilePic.layer.cornerRadius = cell.diaryDetailedProfilePic.frame.height/2
                        cell.diaryDetailedProfilePic.clipsToBounds = true
                        profileImage!.image = image
                        
                    }
                }) { (error) in
                    
                }
            }
            cell.diaryDate.text = diaryDetaileddeliveryReportArray[indexPath.row].deliveryReportaddedDate
            cell.diaryTime.text = diaryDetaileddeliveryReportArray[indexPath.row].deliveryReportaddedTime
            cell.readViaSms.text = diaryDetaileddeliveryReportArray[indexPath.row].readVia
            
            
        }
        
        cell.diaryDate.lineBreakMode = NSLineBreakMode.byTruncatingTail
        cell.diaryDate.adjustsFontSizeToFitWidth = false
        cell.diaryTime.lineBreakMode = NSLineBreakMode.byTruncatingTail
        cell.diaryTime.adjustsFontSizeToFitWidth = false
        cell.callIcon.tag = indexPath.row
        cell.callIcon.addTarget(self, action: #selector(SchoolDiaryDetailsVC.callIconTapped(_:)), for: UIControlEvents.touchUpInside)
        
        
        
        return cell
    }
    
    
    
    
    
    
    
    func countLabelLines(_ label:UILabel)->Int{
        
        if let text = label.text{
            // cast text to NSString so we can use sizeWithAttributes
            let myText = text as NSString
            
            //Set attributes
            let attributes = [NSAttributedStringKey.font : label.font]
            let bounds = UIScreen.main.bounds
            let screenWidth = bounds.size.width
            
            //Calculate the size of your UILabel by using the systemfont and the paragraph we created before. Edit the font and replace it with yours if you use another
            let labelSize = myText.boundingRect(with: CGSize(width: (screenWidth - 48), height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
            //Now we return the amount of lines using the ceil method
            let lines = ceil(CGFloat(labelSize.height) / label.font.lineHeight)
            return Int(lines)
        }
        
        return 0
        
    }
    @objc fileprivate func selectedclassAction(_ sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState())
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState())
                sender.layer.borderWidth = 0
                
            }
        }
        classId = diaryDetailedclassListArray[sender.tag].diaryClassId
        className = diaryDetailedclassListArray[sender.tag].diaryclassName
        diaryDetailedCollectionView.reloadData()
        makeWebApiCallToGetSelectedClassDiaryDetailedEntries()
        
    }
    
    fileprivate func makeWebApiCallToGetDiaryDetailedEntries(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getDiaryDetailedEntryList(diaryId: diaryId, classId: defaultClassId,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            weakSelf!.addEntriesTodeliveryReportArray(parseDiary.parseFromDictionaryForDetailedDeliveryReport(dictionary: result))
            weakSelf!.addEntriesToclassListArray(parseDiary.parseFromDictionaryForDetailedClassList(dictionary: result))
            
            
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
    }
    
    
    fileprivate func makeWebApiCallToGetSelectedClassDiaryDetailedEntries(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getDiaryDetailedStudentEntryList(diaryId: diaryId, classId: classId,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            weakSelf!.addEntriesToSelectedClassStudentDeliveryReportArray(parseDiary.parseFromDictionaryForDetailedSelectedClass(dictionary: result))
            
            
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueForDiaryForward {
            hudControllerClass.showNormalHudToViewController(viewController: self)
            let composeDiary        = segue.destination as! ComposeDiary
            //composeDiary.userID     = (user?.currentUserRoleId)!
            //let msg               = messageThreadArray[0]
            composeDiary.diarySubject   = subject
            composeDiary.diaryContent = content
            composeDiary.diarySubmissionDate = submissionDate
            composeDiary.defaultClassSelected = diaryReciepientArray
            composeDiary.diaryDetailsSelf = self
            imageAttachmentArray.removeAll()
            documentAttachmentArray.removeAll()
            for attachment in attachmentArray{
                if attachment.attachmentType == "Audio"{
                    documentAttachmentArray.append(attachment)
                }
                else if attachment.attachmentType == "Image"{
                    imageAttachmentArray.append(attachment)
                }else if attachment.attachmentType == "DOCX" || attachment.attachmentType == "PDF"{
                    documentAttachmentArray.append(attachment)
                }
            }
            if imageAttachmentArray.count > 0{
                composeDiary.uploadAttachmentArray = imageAttachmentArray
            }
            if documentAttachmentArray.count > 0{
                composeDiary.documentAttachmentArray = documentAttachmentArray
            }
            composeDiary.uploadAttachmentFailure = true
            composeDiary.forwardingDiary = true
            if user?.currentTypeOfUser == .guardian{
                //composeDiary.studentID  = (user?.Students![(user?.currentOptionSelected)!].id)!
                //composeMsg.schoolID   = (user?.Students![(user?.currentOptionSelected)!].schoolId)!
                //composeDiary.profilePic = (user?.Students![(user?.currentOptionSelected)!].profilePic)!
            }
            else{
                //composeDiary.profilePic = (user?.profilePic)!
                
            }
            
        }
    }
    
    
    
    
    @objc func showImageExpandedForDetailedDiary(_ tapgesture:UITapGestureRecognizer) -> Void {
        let parentImageView =  tapgesture.view as! UIImageView
        
        let array = Bundle.main.loadNibNamed("PhotoShowingView", owner: self, options: nil)
        let photoView             = array![0] as! PhotoShowingView
        photoView.imageView.image = parentImageView.image
        photoView.frame           = self.view.frame
        self.view.addSubview(photoView)
    }
    @objc fileprivate func loadPdf(_ tapgesture:UITapGestureRecognizer) {
        let parentImageView =  tapgesture.view as! CustomImageView
        //moveToPdf = true
        pdfViewer.showPdfInViewController(vc: self, pdfHavingUrl: parentImageView.url, withName: parentImageView.name)
        
    }
    
    
    @objc fileprivate func loadAudio(_ tapgesture:UITapGestureRecognizer) {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        print("audio tapped")
        
        if URL(string: parentImageView.url) != nil {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileName = parentImageView.url
            if fileName.range(of: "opus") != nil{
                print("exists")
                replacedString = (fileName as NSString).replacingOccurrences(of: "opus", with: "mp3")
            }
            
            
            let fileUrl = URL(fileURLWithPath: replacedString as String)
            // lets create your destination file url
            destinationUrl = documentsDirectoryURL.appendingPathComponent(fileUrl.lastPathComponent ) as NSURL
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager().fileExists(atPath: destinationUrl.path!) {
                print("The file already exists at path")
                self.setupAudioPlay()
                self.audioPlayView.toPass = self.destinationUrl
                self.audioPlayView.isFileExistAtLocal = true
                // if the file doesn't exist
            } else {
                
                self.audioPlayView.isFileExistAtLocal = false
                //let webServiceUrl = NSURL(string: webServiceUrls.baseURl+"convertAudioPlay/uniqueName/"+parentImageView.name)
                weak var weakSelf = self
                hudControllerClass.showNormalHudToViewController(viewController: self)
                WebServiceApi.getAudioPlayUrl(name: parentImageView.name,onSuccess: { (result) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    if let playUrl = result["convertAudioPlay"] as? String {
                        print("inside audioplay")
                        print(playUrl)
                        
                        //self.audioPlayView.downloadUrl = playUrl
                        //self.audioPlayView.destinationUrl = self.destinationUrl
                        let url = URL(string: playUrl)
                        self.setupAudioPlay()
                        self.downloadFileFromURL(url!)
                    }
                    
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                    
                }) { (progress) in
                    
                }
            }
            
        }
        
        // to check if it exists before downloading it
        //print(webServiceUrl!)
        
        
        
    }
    
    
    func downloadFileFromURL(_ url:URL){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            guard URL != nil && error == nil else {
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                print("failed")
                return
            }
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self?.play(URL!)
        })
        
        downloadTask.resume()
        
    }
    
    func play(_ url:URL) {
        
        do {
            try FileManager().moveItem(at: url, to: self.destinationUrl as URL)
            
            print("File moved to documents folder")
            print(url)
            print(self.destinationUrl)
            self.audioPlayView.toPass = self.destinationUrl
            self.audioPlayView.destinationUrl = self.destinationUrl
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
        }
    }
    
    
    func setupAudioPlay() {
        let array = Bundle.main.loadNibNamed("audioPlay", owner: self, options: nil)
        audioPlayView             = array![0] as! audioPlay
        
        audioPlayView.center = CGPoint(x: self.view.frame.size.width  / 2,
                                       y: self.view.frame.size.height / 2);
        audioPlayView.frame           = self.view.frame
        audioPlayView.delegate = self
        audioPlayView.soundSlider.value = 0.0
        audioPlayView.playAudioButton.setImage(UIImage(named: "playIcon"), for: UIControlState())
        audioPlayView.initialLoad = true
        audioPlayView.playTimeText.text = "00.00"
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.view.addSubview(audioPlayView)
    }
    
    
    fileprivate func addEntriesTodeliveryReportArray(_ array:[DiaryModelClass]){
        diaryDetaileddeliveryReportArray.append(contentsOf: array)
    }
    fileprivate func addEntriesToclassListArray(_ array:[DiaryModelClass]){
        var rowvalue = 0
        diaryDetailedclassListArray.append(contentsOf: array)
        if diaryDetailedclassListArray.count == 1
        {
            diaryDetailedClassCollectionViewHeight.constant = 0
            diaryDetailedCollectionView.isHidden = true
        }
        else{
            
            for k in 0 ..< diaryDetailedclassListArray.count {
                ClassIsSelected[k] = false
                if  diaryDetailedclassListArray[k].diaryClassId == defaultClassId{
                    rowvalue = k
                }
                
            }
            ClassIsSelected[rowvalue] = true
            
        }
        diaryDetailedCollectionView.reloadData()
        if diaryDetailedclassListArray.count > 1
        {
            
            diaryDetailedCollectionView.scrollToItem(at: IndexPath(row: rowvalue, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
        
        DiaryDetailedTableview.reloadData()
        
        
    }
    fileprivate func addEntriesToSelectedClassStudentDeliveryReportArray(_ array:[DiaryModelClass]){
        diaryDetaileddeliveryReportArray.removeAll()
        diaryDetaileddeliveryReportArray.append(contentsOf: array)
        readButton.backgroundColor = UIColor.white
        readButton.setTitleColor(UIColor.black, for: UIControlState())
        unReadButton.backgroundColor = UIColor.white
        unReadButton.setTitleColor(UIColor.black, for: UIControlState())
        sendButton.backgroundColor = colur
        sendButton.setTitleColor(UIColor.white, for: UIControlState())
        readButtonPressed = false
        unReadButtonPressed = false
        
        DiaryDetailedTableview.reloadData()
    }
    @objc  
    func callIconTapped(_ sender:UIButton){
        
        for k in 0 ..< diaryDetaileddeliveryReportArray.count {
            if k == sender.tag{
                mobileNumber = diaryDetaileddeliveryReportArray[k].deliveryReportmobileNo
                
            }
        }
        
        showAlert()
        
    }
    
// this is done by jithin on 28-03-2018 for making call.
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        
        readButton.backgroundColor = UIColor.white
        readButton.setTitleColor(UIColor.black, for: UIControlState())
        unReadButton.backgroundColor = UIColor.white
        unReadButton.setTitleColor(UIColor.black, for: UIControlState())
        sendButton.backgroundColor = colur
        sendButton.setTitleColor(UIColor.white, for: UIControlState())
        readButtonPressed = false
        unReadButtonPressed = false
        
        DiaryDetailedTableview.reloadData()
    }
    
    @IBAction func readButtonTapped(_ sender: UIButton) {
        sendButton.backgroundColor = UIColor.white
        sendButton.setTitleColor(UIColor.black, for: UIControlState())
        unReadButton.backgroundColor = UIColor.white
        unReadButton.setTitleColor(UIColor.black, for: UIControlState())
        readButton.backgroundColor = colur
        readButton.setTitleColor(UIColor.white, for: UIControlState())
        readButtonPressed = true
        unReadButtonPressed = false
        readArray.removeAll()
        for k in 0 ..< diaryDetaileddeliveryReportArray.count {
            if diaryDetaileddeliveryReportArray[k].deliveryReportstatus == "Read"{
                readArray.append(diaryDetaileddeliveryReportArray[k])
                
            }
        }
        
        DiaryDetailedTableview.reloadData()
        
    }
    
    @IBAction func unReadButtonTapped(_ sender: UIButton) {
        
        sendButton.backgroundColor = UIColor.white
        sendButton.setTitleColor(UIColor.black, for: UIControlState())
        readButton.backgroundColor = UIColor.white
        readButton.setTitleColor(UIColor.black, for: UIControlState())
        unReadButton.backgroundColor = colur
        unReadButton.setTitleColor(UIColor.white, for: UIControlState())
        readButtonPressed = false
        unReadButtonPressed = true
        unreadArray.removeAll()
        for k in 0 ..< diaryDetaileddeliveryReportArray.count {
            if diaryDetaileddeliveryReportArray[k].deliveryReportstatus == "Unread"{
                unreadArray.append(diaryDetaileddeliveryReportArray[k])
                
            }
        }
        
        DiaryDetailedTableview.reloadData()
        
    }
    
    func exitSubView() {
        audioPlayView.removeFromSuperview()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    
 }
 
 
 
 
 
