//
//  ComposeDiary.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 20/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import MobileCoreServices
import Firebase
import IQKeyboardManagerSwift

protocol passSendClassListIdProtocol:class  {
    
    func SelectedClassListId(messageReciepientArray:[EmployeeClassModel])
}

class ComposeDiary: UIViewController,CommonHeaderProtocol,popOverDelegate, UIPopoverPresentationControllerDelegate,UINavigationControllerDelegate,diaryComposeClassListProtocol,diaryComposeClassStudentListProtocol,DiaryComposeSchoolListProtocol,UITextFieldDelegate,UITextViewDelegate,UIImagePickerControllerDelegate,attachmentimagelibraryProtocol,attachmentDeleteButtonPressedProtocol,dismissKeyBoardDelegateProtocol,infoButtonDelegateProtocol,UITableViewDelegate,UITableViewDataSource,UIDocumentPickerDelegate
{
    var user:userModel?
    var userId:String = ""
    var profilePic = ""
    var defaultClassName = ""
    var defaultclassDisplay:Bool = false
    var defaultSchoolDisplay:Bool = false
    var diaryAddDate = Foundation.Date()
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    lazy var messageReciepientArray  = [EmployeeClassModel]()//Array to store all the receipients
    lazy var usersArray  = [EmployeeClassModel]()//Array to store all the receipients
    lazy var temprecipientArray  = [EmployeeClassModel]()
    lazy var defaultClassSelected  = [EmployeeClassModel]()//Array to store all the receipients
    lazy var studentListArray = [composeDiaryStudentPopupList]()
    lazy var studentListRecipientArray = [composeDiaryStudentPopupList]()
    lazy var schoolListArray = [corporateManagerSchoolModel]()
    lazy var schoolRecipientListArray = [corporateManagerSchoolModel]()
    lazy var tempuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var copyuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    var uploadAttachmentFailure:Bool = false
    var attachmentImages = [Int:UIImage]()
    var toUploadAttachments = [Int:UIImage]()
    var cameraImages = [Int:UIImage]()
    var activeSelection = false
    var deleteAttachment = false
    var getAllStudentList = false
    var modifyClassSelection:Bool = false
    var modifyStudentSelection:Bool = false
    lazy var uploadAttachmentArray = [uploadAttachmentModel]()
    var uploadedcount:Int = 0
    var schoolListString = ""
    var classListString = ""
    var studentListString = ""
    var attachmentListString = ""
    var cameraSelected = false
    weak var delegate:passSendClassListIdProtocol?
    var myView :UIView = UIView()
    var maximumCountView:maxCountView = maxCountView()
    var characterCount:Int = 0
    var maxCharacterCout:Int = 0
    var InfoButtonId = 0
    var imageArray = [UIImageView]()
    var tagCount = 0
    var posCount = 0
    
    var diarySubject:String = ""
    var diaryContent:String = ""
    var diarySubmissionDate:String = ""
    var forwardingDiary = false
    
    
    @IBOutlet var addDiaryViewHeight: NSLayoutConstraint!
    @IBOutlet var attachmentViewContainerHeight: NSLayoutConstraint!
    @IBOutlet var attachmentViewContainer: ExpansiveView!
    @IBOutlet var header: CommonHeader!
    
    @IBOutlet var addAttachmentImage: UIImageView!
    @IBOutlet var addAttachmentView: UIImageView!
    @IBOutlet var descriptionView: UITextView!
    @IBOutlet var subjectView: UITextField!
    @IBOutlet var dateView: UITextField!
    @IBOutlet var classView: UITextField!
    @IBOutlet var studentListView: UITextField!
    
    @IBOutlet weak var infoIcon: UIButton!
    @IBOutlet weak var messageExceedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var messageExceedView: UIView!
    @IBOutlet weak var messageExceedLabel: UILabel!
    @IBOutlet weak var descriptionCharacterCount: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var documentAttachmentTableView: UITableView!
    @IBOutlet weak var documentAttachmentTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var schoolLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolListTextField: UITextField!
    @IBOutlet weak var schoolListTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var SchoolAllButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var SchoolAllButton: UIButton!
    
    var diaryDetailsSelf:SchoolDiaryDetailsVC = SchoolDiaryDetailsVC()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getUserDataFromDB()
        setupTable()
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
        if user?.smsExceeded == 1 {
         setCommonMaxCountView()
         maxCharacterCout = Int((user?.smsCharacterLimit)!)!
            if user?.currentTypeOfUser == .CorporateManager {
             addDiaryViewHeight.constant = 701
             schoolListTextFieldHeight.constant = 40
             schoolLabelHeight.constant = 20
             SchoolAllButtonHeight.constant = 30
            }
            else{
            addDiaryViewHeight.constant = 633
            schoolListTextFieldHeight.constant = 0
            schoolLabelHeight.constant = 0
            SchoolAllButtonHeight.constant = 0
            }
         messageExceedViewHeight.constant = 0
         messageExceedLabel.isHidden = true
         descriptionCharacterCount.isHidden = true
         descriptionLabel.isHidden = true
         infoIcon.isHidden = true

        }
        else{
         messageExceedViewHeight.constant = 0
         messageExceedLabel.isHidden = true
         descriptionCharacterCount.isHidden = true
         descriptionLabel.isHidden = true
         infoIcon.isHidden = true
            if user?.currentTypeOfUser == .CorporateManager {
                addDiaryViewHeight.constant = 701
                schoolListTextFieldHeight.constant = 40
                schoolLabelHeight.constant = 20
                SchoolAllButtonHeight.constant = 30
            }
            else{
                addDiaryViewHeight.constant = 633
                schoolListTextFieldHeight.constant = 0
                schoolLabelHeight.constant = 0
                SchoolAllButtonHeight.constant = 0
            }
        }
        
        self.navigationController?.isNavigationBarHidden = true
        if user?.currentTypeOfUser == .employee{
        classView.text = defaultClassSelected[0].className
        }
        else{
        schoolListTextField.text = user?.schools![(user?.currentSchoolSelected)!].schoolName
        schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
        }
        //classView!.delegate = self
        studentListView.delegate = self
        dateView.delegate = self
        descriptionView.delegate = self
        schoolListTextField.delegate = self
	    subjectView.text = diarySubject
        descriptionView.text = diaryContent
        dateView.text = diarySubmissionDate
        defaultclassDisplay = true
        defaultSchoolDisplay = true
        setUpUsersArray()
        setupUI()
        alterHeader()
        attachmentViewContainer.delegate = self
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.addAttachmentImageTapped))
        addAttachmentImage.isUserInteractionEnabled = true
        addAttachmentImage.addGestureRecognizer(tapGestureRecognizer)
        messageReciepientArray.append(defaultClassSelected[0])
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if(forwardingDiary){
            forwardingDiary = false
            check()
            hudControllerClass.hideHudInViewController(viewController: diaryDetailsSelf)
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.composeDiary, withProfileImageUrl: profilePic)
        if user?.currentTypeOfUser == .CorporateManager{
        header.multiSwitchButton.setTitle("", for: .normal)
        }
        
        header.delegate = self
    }
    
    private func setupUI() {
        
        descriptionView.layer.borderWidth = CGFloat(1)
        subjectView.layer.borderWidth = CGFloat(1)
        dateView.layer.borderWidth = CGFloat(1)
        classView!.layer.borderWidth = CGFloat(1)
        studentListView.layer.borderWidth = CGFloat(1)
        messageExceedView.layer.borderWidth = CGFloat(1)
        schoolListTextField.layer.borderWidth = 1
        
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        descriptionView.layer.borderColor = color.cgColor
        subjectView.layer.borderColor = color.cgColor
        dateView.layer.borderColor = color.cgColor
        classView!.layer.borderColor = color.cgColor
        studentListView.layer.borderColor = color.cgColor
        messageExceedView.layer.borderColor = color.cgColor
        schoolListTextField.layer.borderColor = color.cgColor
        addAttachmentView.image = UIImage(named: "AddAttachmentIcon")
        dateView.text = DateApi.convertThisDateToString(diaryAddDate, formatNeeded: .ddMMyyyyWithHypen)
        
        /*Auto capital words*/
        subjectView.autocapitalizationType = UITextAutocapitalizationType.sentences
        descriptionView.autocapitalizationType = UITextAutocapitalizationType.sentences
        
        
        
        }
    
    private func setupTable() -> Void {
        
        documentAttachmentTableView.register(UINib(nibName: "attachmentDocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "attachmentDocumentTableViewCell")
        documentAttachmentTableView.estimatedRowHeight = 68.0
    }
    
     private func setCommonMaxCountView() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        maximumCountView = maxCountView(frame: CGRect(x:0, y:0, width:screenWidth, height:75))
        maximumCountView.countdelegate = self
        maximumCountView.infoButtonDelegate = self
        maximumCountView.messageExceedLabel.isHidden = true
        maximumCountView.descriptionLabel.isHidden = true
        maximumCountView.infoIcon.isHidden = true
        self.descriptionView.inputAccessoryView = maxCountView(frame: CGRect(x:0, y:0, width:screenWidth, height:75))
        self.myView = maximumCountView
        self.descriptionView.inputAccessoryView?.addSubview(self.myView)
     }
    
    @objc func textView(_ textView: UITextView,shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        maximumCountView.characterCount.text = String(numberOfChars)+"/"+String(maxCharacterCout)
        descriptionCharacterCount.text = String(numberOfChars)+"/"+String(maxCharacterCout)
        messageExceedLabel.isHidden = true
        descriptionCharacterCount.isHidden = true
        descriptionLabel.isHidden = true
        infoIcon.isHidden = true
        messageExceedViewHeight.constant = 0
        characterCount = numberOfChars
        if numberOfChars > maxCharacterCout {
            maximumCountView.messageExceedLabel.isHidden = false
            maximumCountView.descriptionLabel.isHidden = true
            maximumCountView.infoIcon.isHidden = false
            messageExceedLabel.isHidden = false
            descriptionLabel.isHidden = true
            infoIcon.isHidden = false
            
            
        }
        if numberOfChars <= maxCharacterCout {
            maximumCountView.messageExceedLabel.isHidden = true
            maximumCountView.descriptionLabel.isHidden = true
            maximumCountView.infoIcon.isHidden = true
            messageExceedLabel.isHidden = true
            descriptionLabel.isHidden = true
            infoIcon.isHidden = true
            
        }
        return true
        
    }
    
   
    
    //MARK: -UITextview deleagte to dismiss keyboard
    func didPressButton() {
        
        view.endEditing(true)
        descriptionView.selectedTextRange = nil
        if characterCount > maxCharacterCout {
         messageExceedViewHeight.constant = 41
         messageExceedLabel.isHidden = false
         descriptionCharacterCount.isHidden = false
         descriptionLabel.isHidden = true
         infoIcon.isHidden = false
           if uploadAttachmentArray.count == 0 {
              addDiaryViewHeight.constant = 673
           }
        }
        if characterCount <= maxCharacterCout {
         messageExceedViewHeight.constant = 20
         messageExceedLabel.isHidden = true
         descriptionCharacterCount.isHidden = false
         descriptionLabel.isHidden = true
         infoIcon.isHidden = true
            if uploadAttachmentArray.count == 0 {
                addDiaryViewHeight.constant = 653
            }
        }
    }
    
    //MARK: -UIButton deleagte to identify info Button Pressed or Not
    func didPressedButton() {
        
         if  InfoButtonId%2 == 0 {
            maximumCountView.descriptionLabel.isHidden = false
            let startPosition = descriptionView.position(from: descriptionView.beginningOfDocument, offset: 0)
            let endPosition = descriptionView.position(from: descriptionView.beginningOfDocument, offset: maxCharacterCout)

        
             if startPosition != nil && endPosition != nil {
              descriptionView.selectedTextRange = descriptionView.textRange(from: startPosition!, to: endPosition!)
            }
             InfoButtonId += 1
          }
         else{
            maximumCountView.descriptionLabel.isHidden = true
            let newPosition = descriptionView.endOfDocument
            descriptionView.selectedTextRange = descriptionView.textRange(from: newPosition, to: newPosition)
            InfoButtonId += 1
        }
        
    }
    
    @IBAction func infoIconClicked(_ sender: UIButton) {
        view.endEditing(false)
        descriptionView.becomeFirstResponder()
        if  InfoButtonId%2 == 0 {
            descriptionLabel.isHidden = false
            let startPosition = descriptionView.position(from: descriptionView.beginningOfDocument, offset: 0)
            let endPosition = descriptionView.position(from: descriptionView.beginningOfDocument, offset: maxCharacterCout)
            
            if startPosition != nil && endPosition != nil {
                 descriptionView.selectedTextRange = descriptionView.textRange(from: startPosition!, to: endPosition!)
            }
            InfoButtonId += 1
        }
        else{
            descriptionLabel.isHidden = true
            let newPosition = descriptionView.endOfDocument
            descriptionView.selectedTextRange = descriptionView.textRange(from: newPosition, to: newPosition)
            InfoButtonId += 1
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if user?.smsExceeded == 1 {
         descriptionCharacterCount.isHidden = true
         messageExceedLabel.isHidden = true
         descriptionLabel.isHidden = true
         infoIcon.isHidden = true
         descriptionView.selectedTextRange = nil
        }
        
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if user?.smsExceeded == 1 {
         descriptionCharacterCount.isHidden = false
         messageExceedLabel.isHidden = false
         descriptionLabel.isHidden = false
         infoIcon.isHidden = false
        }
    }
    
    private func setUpUsersArray() {
        usersArray = (user?.Classes)!
        
    }
    //MARK: -UITextfield deleagte to dismiss keyboard
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == schoolListTextField{
            schoolListTextField.resignFirstResponder()
            if user?.currentTypeOfUser == .CorporateManager {
               self.showSchoolListPopUp(textField)
            }
            return false
        }else if textField == classView {
            classView.resignFirstResponder()
            self.showClassListPopUp(textField)
            return false
        }else if textField == studentListView{
            studentListView.resignFirstResponder()
            self.showStudentListPopup(textField)
            return false
        }else if textField == dateView{
            dateView.resignFirstResponder()
            self.showDatePickerForDiary(infoIcon)
            return false
        }
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        showAlert()
    }
    func showSwitchSibling(fromButton: UIButton) {
        //PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    func popOverDidSelectRow(rowSelected: Int) {
        
    }
    
    @IBAction func showDatePickerForDiary(_ sender: UIButton) {
    
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            weakSelf!.diaryAddDate = date as Foundation.Date
            weakSelf!.dateView.text = DateApi.convertThisDateToString(weakSelf!.diaryAddDate, formatNeeded: .ddMMyyyyWithHypen)
            
        }
        prepareOverlayVC(overlayVC: datePickerVC)
        self.present(datePickerVC, animated: false, completion: nil)

     
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    
    private func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName , oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @IBAction func showSchoolListPopUp(_ sender: UITextField) {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeSchoolListPopUpVC") as! DiaryComposeSchoolListPopUpVC ;
        schoolListArray = (user?.schools)!
        if defaultSchoolDisplay == true{
            schoolRecipientListArray.removeAll()
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
        }
        

        if schoolListTextField.text == "All"{
            schoolRecipientListArray = schoolListArray
        }
        
        msRecipVC.recipientArray = schoolListArray
        msRecipVC.searchrecipientArray = schoolListArray
        msRecipVC.previousEntryArray = schoolRecipientListArray
        defaultSchoolDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    
    @IBAction func showClassListPopUp(_ sender: UITextField) {
        if user?.currentTypeOfUser == .CorporateManager {
            if !(schoolListTextField.text?.isEmpty)!{
                getClassListForCorporateManager()
            }
            else{
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectAnySchool, stateOfMessage: .failure)
            }
        }
        else{
            presentClassListView()
        }
    }
    
    private func presentClassListView() {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeClassListPopUpVC") as! DiaryComposeClassListPopUpVC ;
         if defaultclassDisplay == true{
            messageReciepientArray.removeAll()
            if user?.currentTypeOfUser == .employee {
              messageReciepientArray.append(defaultClassSelected[0])
            }
         }
        
        if classView.text == "All"{
            messageReciepientArray = usersArray
        }
        msRecipVC.recipientArray = usersArray
        msRecipVC.searchrecipientArray = usersArray
        msRecipVC.previousEntryArray = messageReciepientArray
        defaultclassDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    private func getClassListForCorporateManager() {
        var schoolIdListString:String = ""
        weak var weakSelf = self
        if schoolRecipientListArray.count > 0 {
            for schoolArray in schoolRecipientListArray {
                schoolIdListString = schoolIdListString+String(schoolArray.schoolId) + ","
            }
            schoolIdListString = schoolIdListString.substring(to: schoolIdListString.characters.index(before: schoolIdListString.endIndex))
            
        }
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getClassListForCompose(schoolId: schoolIdListString,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.usersArray.removeAll()
            weakSelf!.usersArray = parseSchoolsList.parseThisDictionaryForSchoolListPopup(dictionary: result)
            if self.modifyClassSelection == false {
               self.presentClassListView()
            }
            else{
                self.modifyClassSelection = false
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }
    
    
    @IBAction func showStudentListPopup(_ sender: UITextField) {
        
        if !(classView.text?.isEmpty)!{
            var idString = ""
            if messageReciepientArray.count == 0{
              idString = String(defaultClassSelected[0].classId)
                
            }
            
            else{
            for classes in messageReciepientArray {
                idString = idString+String(classes.classId) + ","
            }
            idString = idString.substring(to: idString.characters.index(before: idString.endIndex))
            }
            
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getEligibleStudentListToComposeDiary(schoolClassIdList: idString, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
            if self.modifyStudentSelection == false{
            self.showClassStudentListPopUp()
            }
            else{
            self.modifyStudentSelection = false
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
            })
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectantclass, stateOfMessage: .failure)
            
        }
        
    }
    
    func showClassStudentListPopUp(){
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeClassStudentListPopUpVC") as! DiaryComposeClassStudentListPopUpVC ;
        if studentListView.text == "All"{
            studentListRecipientArray = studentListArray
        }
        
        msRecipVC.recipientArray = studentListArray
        msRecipVC.searchrecipientArray = studentListArray
        msRecipVC.previousEntryArray = studentListRecipientArray
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
        
    }

        
    func  updateSelectedWithSchoolArray(receipientArray recipientArray: [corporateManagerSchoolModel]) {
        schoolListTextField.text = ""
        if recipientArray.count == 0{
            schoolListTextField!.text = ""
        }
            
        else if recipientArray.count == 1{
            schoolListTextField!.text = recipientArray[0].schoolName
        }
        else if recipientArray.count == 2{
            schoolListTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName
        }
        else if recipientArray.count == user?.Classes?.count{
            schoolListTextField!.text = "All"
        }
        else {
            schoolListTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName+","+recipientArray[2].schoolName+" + "+String(recipientArray.count - 2)
            
        }
        schoolRecipientListArray.removeAll()
        schoolRecipientListArray = recipientArray
        if schoolRecipientListArray.count == user?.schools?.count {
            schoolListTextField.text = "All"
        }
        if classView.text != "" {
            self.getClassListForCorporateManager()
            modifyClassSelection = true
        }
        if studentListView.text != "" {
            modifyStudentSelection = true
            showStudentListPopup(studentListView)
        }
    }
    
    
    
    func updateSelectedWithArray(receipientArray: [EmployeeClassModel]) {
        
        classView!.text = ""
        
        if receipientArray.count == 0{
            classView!.text = ""
        }
            
         else if receipientArray.count == 1{
            classView!.text = receipientArray[0].className
        }
        else if receipientArray.count == 2{
            classView!.text = receipientArray[0].className+","+receipientArray[1].className
        }
        else if receipientArray.count == 3{
            classView!.text = receipientArray[0].className+","+receipientArray[1].className+","+receipientArray[2].className
        }
        else if receipientArray.count == user?.Classes?.count{
           classView!.text = "All"
        }
        else{
            classView!.text = receipientArray[0].className+","+receipientArray[1].className+","+receipientArray[2].className+" + "+String(receipientArray.count - 3)
 
        }
        
        messageReciepientArray.removeAll()
        messageReciepientArray = receipientArray
        if messageReciepientArray.count == usersArray.count {
            classView.text = "All"
        }
    }
    
    func updateSelectedWithStudentArray(receipientArray: [composeDiaryStudentPopupList]) {
        
        studentListView!.text = ""
        
        if receipientArray.count == 0{
            studentListView!.text = ""
        }
            
        else if receipientArray.count == 1{
            studentListView!.text = receipientArray[0].studentName
        }
        else if receipientArray.count == studentListArray.count {
            studentListView!.text = "All"
        }
        else
        {
            
            studentListView!.text = receipientArray[0].studentName+" + "+String(receipientArray.count - 1)
        }
        
        
        studentListRecipientArray.removeAll()
        studentListRecipientArray = receipientArray
        
        if studentListArray.count == studentListRecipientArray.count {
            studentListView.text = "All"
        }
    }
    
    @IBAction func SelectAllSchoolButtonAction(_ sender: UIButton) {
        schoolListTextField.text = "All"
        schoolRecipientListArray = (user?.schools)!
    }
    
    @IBAction func selectAllClassButtonAction(_ sender: UIButton) {
        classView.text = "All"
        messageReciepientArray = usersArray
    }
    
    
    @IBAction func selectAllStudentButtonAction(_ sender: UIButton) {
        self.studentListView.text = "All"
        
    }
    
    @objc func addAttachmentImageTapped(img: AnyObject) {
        FireBaseKeys.FirebaseKeysRuleFour(value: "composeDiary_addAttach")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate               = self
        imagePicker.allowsEditing          = false
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
        let popper                         = imagePicker.popoverPresentationController
        popper?.delegate                   = self
        popper?.sourceView                 = self.view
        popper?.sourceRect                 = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        
        
        
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ConstantsInUI.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: ConstantsInUI.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "photoGalleryId") as! PhotoGallery
            vc.delegate = self
            
            self.toUploadAttachments.removeAll()
            self.uploadedcount = 0
            
            self.activeSelection = false
            self.deleteAttachment = false
            
            self.present(vc, animated: true, completion: nil)
        })
        let shareDocumentAction = UIAlertAction(title: ConstantsInUI.shareDocument, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF as NSString) , String(kUTTypeContent)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        })
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(shareDocumentAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        self.present(optionMenu, animated: false, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL){
        var documentName:String = ""
        print(url)
        
        if url.pathExtension == "pdf" || url.pathExtension == "doc" || url.pathExtension == "docx" {
            if(FileManager.default.fileExists(atPath: url.path)) {
                
                let dictoryUrl = url.absoluteString
                if let range = dictoryUrl.range(of: "/", options: .backwards) {
                    _ = dictoryUrl.substring(from: range.upperBound)
                }
                if let docName = dictoryUrl.components(separatedBy: "/").last {
                    print(docName)
                    documentName = docName
                }
                
                let fileData = try? Data(contentsOf: url as URL)
                UploadDocumentAttachment(documentData: fileData! as NSData,docName: documentName)
            }
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectFormat, stateOfMessage: .failure)
        }
    }
    
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated: true, completion: nil) //5
        cameraImages[0] = chosenImage
        cameraSelected = true
        
        SelectedImagesWithArray(selectedImages: cameraImages)
    }
    func reuploadImage(){
        
    }
    
    func SelectedImagesWithArray(selectedImages:[Int:UIImage])
    {
        
        toUploadAttachments = selectedImages
        uploadedcount = 0
        tempuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray = uploadAttachmentArray
        uploadAttachmentFailure = false
        startInitialUpload(selectedImages: selectedImages)
        
    }
    
    func check(){
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var attachmentImagesArrayPosition:Int = 0
        attachmentImagesArrayPosition = attachmentImages.count
        print(uploadAttachmentArray.count)
        print(tempuploadAttachmentArray.count)
        print(attachmentImages)
        
        if uploadAttachmentArray.count != 0 || documentAttachmentArray.count != 0{
            if uploadAttachmentFailure == true {
                imageArray.removeAll()
                tagCount = 0
                posCount = 0
                for i in 0 ..< uploadAttachmentArray.count {
                    if uploadAttachmentArray[i].attachmentType == "Audio" || uploadAttachmentArray[i].attachmentType == "PDF" || uploadAttachmentArray[i].attachmentType == "DOC" || uploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                        
                        
                    }
                }
            }
            else{
                for i in 0 ..< tempuploadAttachmentArray.count {
                    if tempuploadAttachmentArray[i].attachmentType == "Audio" || tempuploadAttachmentArray[i].attachmentType == "PDF" || tempuploadAttachmentArray[i].attachmentType == "DOC" || tempuploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                    else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                    }
                }
            }
            
            attachmentViewContainerHeight.constant = 0
            documentAttachmentTableView.reloadData()
            
            if user?.smsExceeded == 1 {
                if characterCount > maxCharacterCout {
                    if user?.currentTypeOfUser == .employee {
                     addDiaryViewHeight.constant = 673
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                     addDiaryViewHeight.constant = 741
                    }
                }
                else{
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 653
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 721
                    }
                 
                }
            }
            else{
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 633
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 701
                }
                
            }
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
            
            deleteAttachment = false
            attachmentViewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: imageArray)
            documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            addDiaryViewHeight.constant = addDiaryViewHeight.constant + (attachmentViewContainerHeight.constant - 50) + documentAttachmentTableViewHeight.constant
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            
        }
        else {
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            attachmentViewContainerHeight.constant = 0
            if user?.smsExceeded == 1 {
                if characterCount > maxCharacterCout {
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 673
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 741
                    }
                }
                else{
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 653
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 721
                    }
                    
                }
            }
            else{
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 633
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 701
                }
                
            }
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
    }
    
    func startInitialUpload(selectedImages:[Int:UIImage]){
        
        for k in 0 ..< selectedImages.count {
            
            upLoadImage(image: selectedImages[k]!)
            
        }
    }
    
    
    func upLoadImage(image:UIImage){
        weak var weakSelf = self
        attachmentViewContainerHeight.constant = 0
        documentAttachmentTableViewHeight.constant = 0
        if user?.smsExceeded == 1 {
            if characterCount > maxCharacterCout {
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 673
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 741
                }
            }
            else{
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 653
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 721
                }
                
            }
        }
        else{
            if user?.currentTypeOfUser == .employee {
                addDiaryViewHeight.constant = 633
            }
            if user?.currentTypeOfUser == .CorporateManager {
                addDiaryViewHeight.constant = 701
            }
            
        }
        while let subview = attachmentViewContainer.subviews.last {
            subview.removeFromSuperview()
        }
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadDiaryAttachmentImage(image: image, cameraSelected: cameraSelected,
                                                  onSuccess: { (result) in
                                                    weakSelf!.addEntriesToAttachmentArray(array: parseAttachmentArrayForDiary.parseThisDictionaryForAttachments(dictionary: result))
                                                    self.uploadedcount = self.uploadedcount + 1
                                                    if self.toUploadAttachments.count == self.uploadedcount{
                                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                        self.deleteAttachment = false
                                                        self.check()
                                                        
                                                    }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.uploadAttachmentArray.removeAll()
                self.uploadAttachmentArray = self.copyuploadAttachmentArray
                self.uploadAttachmentFailure = true
                self.tempuploadAttachmentArray.removeAll()
                print("failed")
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    func UploadDocumentAttachment(documentData:NSData,docName:String){
        var selectedTab:String = ""
        weak var weakSelf = self
        attachmentViewContainerHeight.constant = 0
        documentAttachmentTableViewHeight.constant = 0
        while let subview = addAttachmentView.subviews.last {
            subview.removeFromSuperview()
        }
        selectedTab = "Diary"
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadAttachmentDocument(uploadDocumentData: documentData,docName: docName,selectedTab: selectedTab,
                                               onSuccess: { (result) in
                                                weakSelf!.addEntriesToDocumentAttachmentArray(array: parseAttachmentArrayForDiary.parseThisDictionaryForAttachments(dictionary: result))
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                self.deleteAttachment = false
                                                self.check()
                                                
                                                
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.uploadAttachmentArray.removeAll()
                self.uploadAttachmentArray = self.copyuploadAttachmentArray
                self.uploadAttachmentFailure = true
                self.tempuploadAttachmentArray.removeAll()
                print("failed")
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }

    public func addEntriesToAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            
            uploadAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    internal func addEntriesToDocumentAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            tempuploadAttachmentArray.removeAll()
            documentAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    internal func SelectedAttachmentTag(selectedAttachment currentTag : Int){
        var tempimageArray = [UIImageView]()
        var pos = 0
        var tagTagForImage = 0
        
        
        activeSelection = true
        uploadAttachmentArray.remove(at: currentTag)
        imageArray.remove(at: currentTag)
        deleteAttachment = true
        for k in 0 ..< imageArray.count {
            
            if imageArray[k].image != nil{
                imageArray[k].contentMode = .scaleAspectFit
                imageArray[k].tag = tagTagForImage
                tempimageArray.append(imageArray[k])
                tagTagForImage = tagTagForImage + 1
                pos = pos + 1
            }
        }
        tagCount = tagTagForImage
        posCount = pos
        if uploadAttachmentArray.count != 0 && imageArray.count != 0  {
            attachmentViewContainerHeight.constant = 0
            if user?.smsExceeded == 1 {
                if characterCount > maxCharacterCout {
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 673
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 741
                    }
                }
                else{
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 653
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 721
                    }
                    
                }
            }
            else{
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 633
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 701
                }
                
            }
            if user?.currentTypeOfUser == .employee {
            addDiaryViewHeight.constant = 673
            }
            else{
            addDiaryViewHeight.constant = 741
            }
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
            deleteAttachment = false
            attachmentViewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: tempimageArray)
            documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            addDiaryViewHeight.constant = addDiaryViewHeight.constant + (attachmentViewContainerHeight.constant - 50) + documentAttachmentTableViewHeight.constant
            
            
        }
        else {
            
            attachmentViewContainerHeight.constant = 0
            if user?.smsExceeded == 1 {
                if characterCount > maxCharacterCout {
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 673
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 741
                    }
                }
                else{
                    if user?.currentTypeOfUser == .employee {
                        addDiaryViewHeight.constant = 653
                    }
                    if user?.currentTypeOfUser == .CorporateManager {
                        addDiaryViewHeight.constant = 721
                    }
                    
                }
            }
            else{
                if user?.currentTypeOfUser == .employee {
                    addDiaryViewHeight.constant = 633
                }
                if user?.currentTypeOfUser == .CorporateManager {
                    addDiaryViewHeight.constant = 701
                }
                
            }
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
    }
    
    
    @IBAction func composeDiaryAction(_ sender: UIButton) {
 FireBaseKeys.FirebaseKeysRuleThree(value: "composeDiary_send")
        let result = validateFields()
        if result.0 {
            
            schoolListString = ""
            classListString = ""
            studentListString = ""
            attachmentListString = ""
            
            if user?.currentTypeOfUser == .employee {
             if user?.currentTypeOfUser == .CorporateManager {
                for schoolArray in schoolRecipientListArray{
                    schoolListString = schoolListString+String(schoolArray.schoolId) + ","
                }
                schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString
                    .endIndex))

             }
             else{
                let schoolId = user?.schoolId
                if schoolId != 0 {
                 schoolListString = String(schoolId!)
                }
             }
            }
            
            if classView.text == "All"{
                classListString = "All"
            }
            else{
                for classArray in messageReciepientArray{
                    classListString = classListString+String(classArray.classId) + ","
                }
                classListString = classListString.substring(to: classListString.characters.index(before: classListString
                    .endIndex))
            }
            
            if studentListView.text == "All"{
                if !(classView.text?.isEmpty)!{
                    
                    self.studentListString = "All"
                }
                else{
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectantclass, stateOfMessage: .failure)
                }
            }
            
            else {
                    for studentArray in studentListRecipientArray{
                    studentListString = studentListString+String(studentArray.studentId) + ","
                    }
                studentListString = studentListString.substring(to: studentListString.characters.index(before: studentListString
                    .endIndex))
                   self.getAllStudentList = true
            }
        
        
            if uploadAttachmentArray.count > 0 {
                for attachmentArray in uploadAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex))
                
            }
            
            if documentAttachmentArray.count > 0 {
                if uploadAttachmentArray.count != 0 {
                    attachmentListString = attachmentListString + ","
                }
                for attachmentArray in documentAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex))

            }
            
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            WebServiceApi.postDiaryWithTitle(classList: classListString,
                                             diarystudentList:studentListString,
                                              dateofdiary: dateView.text!,
                                              diarysubject: subjectView.text!,
                                              diarycontent: descriptionView.text!,
                                              idOfDiaryAddingPerson: (user?.currentUserRoleId)!,
                                              attachmentIdList: attachmentListString,
                                              corporateId:String(user!.corporateId),
                                              recipientSchoolIdList:schoolListString,
                                              onSuccess: { (result) in
                                                
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                if (result["error"] as? [String:AnyObject]) == nil{
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.diaryPostingSucces, stateOfMessage: .success)
                                                    self.delegate?.SelectedClassListId(messageReciepientArray: self.messageReciepientArray)
                                                    
                                                    weakSelf?.clearAllData()
                                                    _ = weakSelf!.navigationController?.popViewController(animated: true)
                                                    
                                                }else{
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.diarySendFailed, stateOfMessage: .failure)
                                                }
                                                
                                                
                                                
                                                
                },
                                              onFailure: { (error) in
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.diarySendFailed, stateOfMessage: .failure)
                                                
                },
                                              duringProgress: { (progress) in
                                                
            })
            
    }
        else {
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
            
            
    }
        
    
    
    private func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
        
        if  schoolListTextField.text == "" && user?.currentTypeOfUser == .CorporateManager {
            result   = false
            ErrorMsg = ConstantsInUI.selectSchool
        }
        else if classView.text == "" {
            result   = false
            ErrorMsg = ConstantsInUI.selectclass
        }
        else if (studentListView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.selectstudent
        }else if (dateView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.date
        }
        else if (subjectView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.subject
        }
        else if (descriptionView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.description
        }
        
        
        return (result,ErrorMsg)
    }
    
    private func clearAllData(){
        
        classView.text   = ""
        subjectView.text        = ""
        dateView.text = ""
        subjectView.text = ""
        descriptionView.text = ""
        
        
    }

    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return documentAttachmentArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "attachmentDocumentTableViewCell") as! attachmentDocumentTableViewCell
        cell.documentUniqueName.text = documentAttachmentArray[indexPath.row].attachmentName
        cell.selectionStyle = .none
        if documentAttachmentArray[indexPath.row].attachmentType == "Audio" {
            cell.documentIcon.image = UIImage(named: "audioIcon")
        }
        if documentAttachmentArray[indexPath.row].attachmentType == "PDF" {
            cell.documentIcon.image = UIImage(named: "pdfIcon")
        }
        if documentAttachmentArray[indexPath.row].attachmentType == "DOCX" || documentAttachmentArray[indexPath.row].attachmentType == "DOC" {
            cell.documentIcon.image = UIImage(named: "wordIcon")
            
        }
        cell.documentCloseButton.tag = indexPath.row
        cell.documentCloseButton.addTarget(self, action: #selector(self.deleteDocumentAttachment), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc  private func deleteDocumentAttachment(sender:UIButton) {
        for k in 0 ..< documentAttachmentArray.count {
            if k == sender.tag {
                documentAttachmentArray.remove(at: k)
            }
        }
        documentAttachmentTableView.reloadData()
        documentAttachmentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
        addDiaryViewHeight.constant = addDiaryViewHeight.constant - 40
    }
    
   
    
        
    }
    
    
    






