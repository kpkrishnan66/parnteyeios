//
//  GroupRoleListPopupCell.swift
//  parentEye
//
//  Created by Vivek on 11/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class GroupRoleListPopupCell:UITableViewCell {
    
    @IBOutlet weak var rightArrowIcon: UIButton!
    @IBOutlet weak var groupIcon: UIButton!
    @IBOutlet weak var roleName: UILabel!
}
