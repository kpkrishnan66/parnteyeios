//
//  profileHeaderCell.swift
//  parentEye
//
//  Created by Martin Jacob on 21/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class profileHeaderCell: UITableViewCell {

    @IBOutlet weak var profileImageView:imageVCForUploadProcess!
    @IBOutlet weak var nameLabel:UILabel!
    @IBOutlet weak var gradeAndClassLabel:UILabel!
    
    @IBOutlet var studentImage: UIImageView!
    @IBOutlet var studentProfileButton: UIButton!
    @IBOutlet var studentProfileView: UIView!
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet var studentprofileviewheight: NSLayoutConstraint!
    @IBOutlet var feePaymentButton: UIButton!
    @IBOutlet var feePaymentImage: UIImageView!
    @IBOutlet var feePaymentView: UIView!
    
    @IBOutlet var studentResultView: UIView!
    @IBOutlet var studentResultImageView: UIImageView!
    @IBOutlet var studentResultButton: UIButton!
    
    var user:userModel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        getUserDataFromDB()
        ImageAPi.makeImageViewRound(imageView: profileImageView)
        
        // Initialization code
    }

    override func didMoveToSuperview() {
        //ImageAPi.makeImageViewRound(profileImageView)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func alterCellWithProfileImageUrl(url:String, nameOfStudent name:String, gradeOfStudent grade:String, classOfStudent:String,initailLoad:Bool) -> Void {
        
        if(url.isEmpty){
            profileImageView!.image = UIImage(named:"avatar.png")
        }
        else{
        weak var weakProfileImage = self.profileImageView
        ImageAPi.fetchImageForUrl(urlString: url, oneSuccess: { (image) in
                if weakProfileImage != nil { weakProfileImage!.image = image }
            }) { (error) in
                
            } }
        
        //WebServiceApi.setImgWithUrlString(url, forImageView: profileImageView)
        nameLabel.text = name
        if initailLoad == true{
        setClassGradeLabelWithString(className: grade)
        }
        else{
           setClassGradeLabelSwitchWithString(grade: grade,classOfStudent: classOfStudent)
        }
        //gradeAndClassLabel.text = grade
        
    }
    
     
    
    private func setClassGradeLabelWithString(className:String){
    
        let studentId = user?.getSelectedEmployeeOrStudentId()
        if studentId != nil {
            
         let classGrade = user?.getSelectedEmployeeOrStudentGrade()
         let classTeacher = user?.getSelectedEmployeeOrStudentClassTeacher()
            
        let classGradettleFnt = UIFont(name: "Roboto-Thin", size: gradeAndClassLabel.font.pointSize)
        let classGradecolur = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            
            let classGradetitString = StringClassAttri(withString: "Grade: ", andAttributes: [NSAttributedStringKey.font: classGradettleFnt!,NSAttributedStringKey.foregroundColor:classGradecolur])
            
        let classGradefnt = UIFont(name: "Roboto-Medium", size: gradeAndClassLabel.font.pointSize)
            let classGradestring = StringClassAttri(withString: classGrade!, andAttributes: [NSAttributedStringKey.font: classGradefnt!,NSAttributedStringKey.foregroundColor:classGradecolur])
            
            
        let classTeachertitleFont = UIFont(name: "Roboto-Thin", size: gradeAndClassLabel.font.pointSize)
            let classTeachertitString = StringClassAttri(withString: "Class Teacher: ", andAttributes: [NSAttributedStringKey.font: classTeachertitleFont!,NSAttributedStringKey.foregroundColor:classGradecolur])
         
        let classTeacherfnt = UIFont(name: "Roboto-Medium", size: gradeAndClassLabel.font.pointSize)
        let classTeachercolur = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            let classTeacherstring = StringClassAttri(withString: classTeacher!, andAttributes: [NSAttributedStringKey.font: classTeacherfnt!,NSAttributedStringKey.foregroundColor:classTeachercolur])
        let completeString   = " | "
            let appendString        = StringClassAttri(withString: completeString, andAttributes: [NSAttributedStringKey.font:classTeachertitleFont!])
            
        gradeAndClassLabel.attributedText = String().createAttributedStringFromArray(stringArray: [classGradetitString,classGradestring,appendString,classTeachertitString,classTeacherstring])
        }
    }
    
    private func setClassGradeLabelSwitchWithString(grade:String, classOfStudent:String){
        
            
        
            let classGradettleFnt = UIFont(name: "Roboto-Thin", size: gradeAndClassLabel.font.pointSize)
            let classGradecolur = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
            
        let classGradetitString = StringClassAttri(withString: "Grade: ", andAttributes: [NSAttributedStringKey.font: classGradettleFnt!,NSAttributedStringKey.foregroundColor:classGradecolur])
            
            let classGradefnt = UIFont(name: "Roboto-Medium", size: gradeAndClassLabel.font.pointSize)
        let classGradestring = StringClassAttri(withString: grade, andAttributes: [NSAttributedStringKey.font: classGradefnt!,NSAttributedStringKey.foregroundColor:classGradecolur])
            
        
            let classTeachertitleFont = UIFont(name: "Roboto-Thin", size: gradeAndClassLabel.font.pointSize)
        let classTeachertitString = StringClassAttri(withString: "Class Teacher: ", andAttributes: [NSAttributedStringKey.font: classTeachertitleFont!,NSAttributedStringKey.foregroundColor:classGradecolur])
            
            let classTeacherfnt = UIFont(name: "Roboto-Medium", size: gradeAndClassLabel.font.pointSize)
            let classTeachercolur = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
        let classTeacherstring = StringClassAttri(withString: classOfStudent, andAttributes: [NSAttributedStringKey.font: classTeacherfnt!,NSAttributedStringKey.foregroundColor:classTeachercolur])
            let completeString   = " | "
        let appendString        = StringClassAttri(withString: completeString, andAttributes: [NSAttributedStringKey.font:classTeachertitleFont!])
            
            gradeAndClassLabel.attributedText = String().createAttributedStringFromArray(stringArray: [classGradetitString,classGradestring,appendString,classTeachertitString,classTeacherstring])
        
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
}
