//
//  ChooseAbsenteesStudentList.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 31/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

import UIKit

protocol chooseAbsenteesStudentListProtocol:class  {
    
    func updateSelectedWithStudentArray(receipientArray:[composeDiaryStudentPopupList])
}


class ChooseAbsenteesStudentList: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var numberOfAddedLabel: UILabel!
    @IBOutlet var receipientsTable: UITableView!
    
    var recipientArray = [composeDiaryStudentPopupList]()
    var searchrecipientArray = [composeDiaryStudentPopupList]()
    var temprecipientArray = [composeDiaryStudentPopupList]()
    var previousEntryArray = [composeDiaryStudentPopupList]()// an array to store the previous entered details
    var search: Bool = false
    weak var delegate:chooseAbsenteesStudentListProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchrecipientArray = recipientArray
        temprecipientArray = recipientArray
        iterateArrayForFindingSelected()
        receipientsTable.register(UINib(nibName: "ChooseAbsenteesStudentListCell", bundle: nil), forCellReuseIdentifier: "ChooseAbsenteesStudentListCell")
        receipientsTable.estimatedRowHeight = 76.0
        receipientsTable.rowHeight          = UITableViewAutomaticDimension
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.enablesReturnKeyAutomatically = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if recipientArray.count == 0{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.recipientNone, stateOfMessage: .failure)
        }
        return recipientArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseAbsenteesStudentListCell") as! ChooseAbsenteesStudentListCell
        let user = recipientArray[indexPath.row]
        
        cell.setTeacherSelected(selected: user.selectedInUI)
        cell.studentName.text        = user.studentName
        
        weak var profileImage = cell.profilePic
        
        if recipientArray[indexPath.row].rollNo != "" {
            cell.alphabetLabel.text = recipientArray[indexPath.row].rollNo
            cell.profilePic.image = UIImage(named: "circleImage")
        }
        else{
        cell.alphabetLabel.text = ""
        if user.compressedProfilePic == ""{
            cell.profilePic.image = UIImage(named: "avatar")
            cell.profilePic.layer.masksToBounds = true
            cell.profilePic.layer.cornerRadius = 0
            cell.profilePic.clipsToBounds = false
            
        }
        
        else{
            ImageAPi.fetchImageForUrl(urlString: user.compressedProfilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.profilePic.layer.masksToBounds = false
                    cell.profilePic.layer.cornerRadius = cell.profilePic.frame.height/2
                    cell.profilePic.clipsToBounds = true
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
          }
        }
        
        
        
        cell.selectionButton?.addTarget(self, action: #selector(self.changeSelection), for: .touchUpInside)
        cell.selectionButton?.tag = indexPath.row
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let button = UIButton()
        button.tag = indexPath.row
        changeSelection(sender: button)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- updateSelected in UI
    
    @objc private func changeSelection(sender:UIButton){
        
        if recipientArray[sender.tag].selectedInUI == true {
            recipientArray[sender.tag].selectedInUI = false
        }else {
            recipientArray[sender.tag].selectedInUI = true
        }
        
        receipientsTable.reloadRows(at: [NSIndexPath(row: sender.tag, section: 0) as IndexPath], with: .middle)
        alterElementsInPreviousArrayToSelected(isSelected: !recipientArray[sender.tag].selectedInUI , userTobeAltered: recipientArray[sender.tag])
        setAddedLabel()
        
    }
    
    /**
     function to add / alter elements to the previous array
     
     - parameter isSelected: check if the entry is selected
     - parameter user:       user object that is to be altered
     */
    private func alterElementsInPreviousArrayToSelected(isSelected:Bool, userTobeAltered user:composeDiaryStudentPopupList){
        
        var index:Int?
        for i in 0 ..< previousEntryArray.count {
            if previousEntryArray[i].studentId == user.studentId{
                index = i
            }
            
        }
        
        if let ind = index {
            
            if !isSelected{
                previousEntryArray[ind].selectedInUI = isSelected
            }else {
                previousEntryArray.remove(at: ind)
            }
        }else {
            user.selectedInUI = true
            previousEntryArray.append(user)
        }
        
    }
    
    private func iterateArrayForFindingSelected(){
        var count:Int = 0
        for i in 0 ..< recipientArray.count {
            for j in 0 ..< previousEntryArray.count {
                if previousEntryArray[j].studentId == recipientArray[i].studentId{
                    previousEntryArray[j].selectedInUI = true
                    recipientArray[i].selectedInUI = true
                    count = count + 1
                }
                
            }
        }
        
        
        setAddedLabel()
        
    }
    
    
    
    private func setAddedLabel(){
        if search == false{
            let unreadArray = recipientArray.filter{$0.selectedInUI}
            numberOfAddedLabel.text = String(unreadArray.count)
        }
        else{
            let unreadArray = temprecipientArray.filter{$0.selectedInUI}
            numberOfAddedLabel.text = String(unreadArray.count)
            
        }
        
    }
    
    
    
    
    
    @IBAction func dismissMe(_ sender: Any) {
    
        self.dismiss(animated: false, completion: nil)
        self.delegate?.updateSelectedWithStudentArray(receipientArray: previousEntryArray)
        
    }
    

    //search events
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            recipientArray = temprecipientArray
        }else{
            let filtered = temprecipientArray.filter { $0.studentName.contains(searchText,options: .caseInsensitive) }
            recipientArray = filtered
        }
        receipientsTable.reloadData()
    }
    
}

