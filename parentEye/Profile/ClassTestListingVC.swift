//
//  ClassTestListingVC.swift
//  parentEye
//
//  Created by scientia on 17/07/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ClassTestListingVC: UIViewController,CommonHeaderProtocol,UITableViewDelegate,UITableViewDataSource,UICollectionViewDataSource,UICollectionViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,dateSelectedButtonPressedProtocol {
  
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var classListAndComposeSubView: UIView!
    @IBOutlet var classListAndComposeSubViewHeight: NSLayoutConstraint!
    @IBOutlet var classListCollectionView: UICollectionView!
    @IBOutlet var addNewClassTestButton: UIButton!
    @IBOutlet var addNewClassTestButtonHeight: NSLayoutConstraint!
    @IBOutlet var classTestListTableView: UITableView!
    @IBOutlet var noClassTestLabel: UILabel!
    
    
    let reuseIdentifier = "cell"
    var user:userModel?
    var profilePic = ""
    var classId:Int = 0
    var schoolId:Int = 0
    var className:String = ""
    var studentId:String = ""
    var manageClassId:Bool = false
       let redcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let backColur = UIColor(red: 242.0/255.0, green: 242.0/255.0, blue: 242.0/255.0, alpha: 1.0)
    let greencolur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    let bluecolor = UIColor(red: 0.00/255.0, green:122.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    var studentOrClassId:String = ""
    var selectSubjectPicker = UIPickerView()
    var classTestAddDate = Foundation.Date()
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    
    var ClassesList:[EmployeeClassModel] = []
    var defaultClassesList:[EmployeeClassModel] = []
    var ClassIsSelected = [Int: Bool]()
    lazy var classTestListingArray = [classTest]()
    lazy var studentList = [studentExamList]()
    var classTestView = createClassTestView()
    var subjectList = [employeeSubjectList]()

    let attributes = [
        NSAttributedStringKey.foregroundColor: UIColor.lightGray,
        NSAttributedStringKey.font : UIFont(name: "Roboto-Medium", size: 14)! // Note the !
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        initializeTableView()
        getClassTestList()
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- DB interactions
    /**
     Function to get user data from DB
     */
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    
    private func setupUI() {
        noClassTestLabel.isHidden = true
        if user?.currentTypeOfUser == .guardian {
            setUpUIforGuardian()
        }
        else{
            setupUIforEmployee()
        }

    }
    
    private func setUpUIforGuardian(){
        
        addNewClassTestButtonHeight.constant = 0.0
        classListAndComposeSubViewHeight.constant = 0.0
        addNewClassTestButton.isHidden = true
        studentOrClassId = (user?.Students![(user?.currentOptionSelected)!].id)!
    }
    
    private func setupUIforEmployee() {
       
        addNewClassTestButtonHeight.constant = 40.0
        classListAndComposeSubViewHeight.constant = 113.0
        addNewClassTestButton.isHidden = false
        populateClassList()
        if classId != 0 {
          studentOrClassId = String(classId)
        }
    }
    
    func populateClassList () {
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        manageClassId = true
        className = ClassesList[0].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[0])
        
    }

    private func initializeTableView(){
        classTestListTableView.register(UINib(nibName: "ClassTestListingCell", bundle: nil), forCellReuseIdentifier: "ClassTestListingCell")
        classTestListTableView.estimatedRowHeight = 100.0
        classTestListTableView.rowHeight          = UITableViewAutomaticDimension
    }
    
    private func alterHeader () {
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        header.profileChangeButton.isHidden = true
        header.profileImageRightSeparator.isHidden = true
        header.downArrowButton.isHidden = true
        header.multiSwitchButton.isHidden = true
        header.rightLabel.isHidden = true
        header.screenShowingLabel.text = ConstantsInUI.classTestListHeading
    }
    
    
    //commonHeader Delegates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showSwitchSibling(fromButton: UIButton){
        
    }
    
    private func getClassTestList() {
        self.classTestListingArray.removeAll()
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
       
        WebServiceApi.getClassTestList(studentOrClassId: studentOrClassId,user: user!,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.classTestListingArray = parseclassTestArray.parseThisDictionaryForclassTest(dictionary: result)
            
            if self.user!.currentTypeOfUser == .guardian {
                self.addNewClassTestButton.isHidden = true
            }
            else{
                self.addNewClassTestButton.isHidden = false
            }
            
            if self.classTestListingArray.count == 0 {
                self.noClassTestLabel.isHidden = false
                
            }
            else{
                self.noClassTestLabel.isHidden = true
                
            }
            
            if self.user?.currentTypeOfUser != .guardian {
                self.getSubjectList()
            }
            self.classTestListTableView.reloadData()
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.classTestListFailed, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }
    
    func getSubjectList() {
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        subjectList.removeAll()
        WebServiceApi.getSubjectListForClassTestCreation(userRoleId: (user?.currentUserRoleId)!,classId: String(studentOrClassId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.subjectList = parsesubjectsArray.parseThisDictionaryForAttachments(dictionary: result)
            if self.subjectList.count > 0 {
             let employeeSubjectListObject = employeeSubjectList()
             employeeSubjectListObject.subjectName = "Select Subject"
             self.subjectList.insert(employeeSubjectListObject, at: 0)
            }
            print(self.subjectList.count)
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.classTestListFailed, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }

    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = redcolur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        studentOrClassId = String(ClassesList[sender.tag].classId)
        manageClassId = true
        className = ClassesList[sender.tag].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        classListCollectionView.reloadData()
        getClassTestList()
        
    }
    
    //collectionView Delegates and Datasources
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ClassesList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "classTestCollectionViewCell", for: indexPath as IndexPath) as! classTestCollectionViewCell
        
        if ClassIsSelected[indexPath.row] == false{
            cell.className.backgroundColor = UIColor.clear
            cell.className.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.className.layer.borderWidth = 1
            cell.className.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.className.backgroundColor = redcolur
            cell.className.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.className.layer.borderWidth = 0
        }
        cell.className.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.className.layer.cornerRadius = 18
        cell.className.layer.borderWidth = 1
        cell.className.layer.borderColor = UIColor.black.cgColor
        cell.className.tag = indexPath.row
        cell.className.addTarget(self, action: #selector(ClassTestListingVC.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }

    
    //MARK:- TAbleview delegate and data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if classTestListingArray.count>0{
         return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if classTestListingArray.count>0{
         return classTestListingArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ClassTestListingCell") as! ClassTestListingCell
        cell.selectionStyle = .none
        cell.HeaderName.text = classTestListingArray[indexPath.row].name
        cell.createdByname.text = classTestListingArray[indexPath.row].addedByName
        cell.topicName.text = "Topic :"+classTestListingArray[indexPath.row].topic
        if user?.currentTypeOfUser == .guardian {
         if classTestListingArray[indexPath.row].mark == "" {
            cell.mark.text = "No mark entered"
         }
         else{
            cell.mark.text = "Mark obtained - "+classTestListingArray[indexPath.row].mark+"/"+String(classTestListingArray[indexPath.row].maximumMark)
         }
        }
        cell.dayLabel.text = classTestListingArray[indexPath.row].dayString
        cell.monthAndYearLabel.text = classTestListingArray[indexPath.row].monthString+"-"+classTestListingArray[indexPath.row].yearString
        
        if classTestListingArray[indexPath.row].upcoming == "Upcoming" {
            cell.upcomingLabel.text = "Upcoming"
            cell.upcomingLabel.textColor = greencolur
        }
        else if classTestListingArray[indexPath.row].upcoming == "Today" {
            cell.upcomingLabel.text = "Today"
            cell.upcomingLabel.textColor = bluecolor
        }
        else{
            cell.upcomingLabel.text = "Past"
            cell.upcomingLabel.textColor = redcolur
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if classTestListingArray.count > 0 {
            if user?.currentTypeOfUser == .guardian {
               return 100+CGFloat(classTestListingArray[indexPath.row].topicNewlineCout*15)
            }
            else{
                return 80+CGFloat(classTestListingArray[indexPath.row].topicNewlineCout*15)
            }
         
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if user?.currentTypeOfUser == .guardian {
            
        }
        else{
            presentMarkEntryDetailedVc(position: indexPath.row)
        }
    }
    
    //Mark:- pickerview data sources and delegates
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if subjectList.count > 0 {
            return subjectList.count
        }
        else{
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       return subjectList[row].subjectName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        classTestView.selectSubjectOrTitleTextField.text = subjectList[row].subjectName
    }
    
     func presentMarkEntryDetailedVc(position:Int) {
        var classTestId:String = ""
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        self.studentList.removeAll()
        classTestId = String(self.classTestListingArray[position].id)
        WebServiceApi.getStudentWithMarkforClassTestForMarkEntry(classTestId: classTestId,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.studentList = parseStudentExamListArray.parseThisDictionaryForsclassTestMarkListRecords(dictionary: result)
            print(self.studentList.count)
            if self.studentList.count > 0{
             let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
             let empMarkVC = storyboard.instantiateViewController(withIdentifier: "EmloyeeMarkEntryDetailVC") as! EmloyeeMarkEntryDetailVC ;
             empMarkVC.isclassTest = true
             empMarkVC.examclassTest = self.classTestListingArray[position]
             empMarkVC.maxMark = self.classTestListingArray[position].maximumMark
             empMarkVC.fromClassTestListingVC = true
             empMarkVC.classId = Int(self.studentOrClassId)!
             empMarkVC.className = self.className
             empMarkVC.studentList = self.studentList
             self.present(empMarkVC, animated: true, completion: nil)
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
            }, duringProgress: { (progress) in
                
        })
        
        
        
        // add segue , pass vars
    }
    
    @IBAction func createClasstestButtonTapped(_ sender: UIButton) {
        Analytics.logEvent("T_homePage_classtest", parameters: nil)
        let array = Bundle.main.loadNibNamed("createClassTestView", owner:self, options:nil)
        classTestView = array![0] as! createClassTestView
        classTestView.center = CGPoint(x:self.view.frame.size.width  / 2,
                                       y:self.view.frame.size.height / 2);
        classTestView.frame = self.view.frame
        setupUIForCreateClassTest()
        classTestView.dateSelectionButton.addTarget(self, action: #selector(popupDatePicker), for: .touchUpInside)
        classTestView.cancelButton.addTarget(self, action: #selector(createClasstestViewClosed), for: .touchUpInside)
        classTestView.dateofClassTestTexfield.addTarget(self, action: #selector(popupDatePicker), for: .touchUpInside)
        classTestView.createClassTestButton.addTarget(self, action: #selector(createClassTestButtonViewTapped), for: .touchUpInside)
        //classTestView.selectSubjectOrTitleTextField.addTarget(self, action: #selector(addPickerview), forControlEvents: .EditingDidBegin)
        if subjectList.count == 0 {
            classTestView.subjectOrTitleButton.isHidden = true
            classTestView.selectSubjectOrTitleTextField.inputView = nil
            classTestView.selectSubjectOrTitleTextField.reloadInputViews()
            classTestView.selectSubjectOrTitleTextField.keyboardType = .default
            classTestView.selectSubjectOrTitleTextField.attributedPlaceholder = NSAttributedString(string: "Enter title",attributes: attributes)
            
        }
        else{
             classTestView.subjectOrTitleButton.isHidden = false
             classTestView.subjectOrTitleButton.setTitle("Add Title", for: .normal)
             addPickerview()
             classTestView.selectSubjectOrTitleTextField.attributedPlaceholder = NSAttributedString(string: "Select Subject",attributes: attributes)
             classTestView.subjectOrTitleButton.addTarget(self, action: #selector(changeSubjectOrTitleTextField), for: .touchUpInside)
        }
        
        self.view.addSubview(classTestView)
        

    }
    
    func setupUIForCreateClassTest() {
        
        classTestView.selectSubjectOrTitleTextField.layer.borderWidth = CGFloat(1)
        classTestView.maxMarkTextField.layer.borderWidth = CGFloat(1)
        classTestView.dateofClassTestTexfield.layer.borderWidth = CGFloat(1)
        classTestView.classTestTopicsTextview.layer.borderWidth = CGFloat(1)
        
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        classTestView.selectSubjectOrTitleTextField.layer.borderColor = color.cgColor
        classTestView.maxMarkTextField.layer.borderColor = color.cgColor
        classTestView.dateofClassTestTexfield.layer.borderColor = color.cgColor
        classTestView.classTestTopicsTextview.layer.borderColor = color.cgColor
        
        classTestView.classTestHeader.text = "Class Test "+className
        
        classTestView.maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Max Mark",attributes: attributes)
        classTestView.dateofClassTestTexfield.attributedPlaceholder = NSAttributedString(string: "Select date",attributes: attributes)
        classTestView.dateofClassTestTexfield.isUserInteractionEnabled = false
        
    }
    
    func addPickerview() {
        selectSubjectPicker.delegate = self
        selectSubjectPicker.dataSource = self
        classTestView.selectSubjectOrTitleTextField.inputView = selectSubjectPicker
        selectSubjectPicker.reloadAllComponents()
       
    }
    
    @objc private func changeSubjectOrTitleTextField() {
        
        if classTestView.subjectOrTitleButton.titleLabel?.text == "Add Title" {
           classTestView.selectSubjectOrTitleTextField.inputView = nil
           classTestView.selectSubjectOrTitleTextField.reloadInputViews()
           classTestView.selectSubjectOrTitleTextField.keyboardType = .default
           classTestView.subjectOrTitleButton.setTitle("Add Subject", for: .normal)
           classTestView.selectSubjectOrTitleTextField.text = ""
           classTestView.selectSubjectOrTitleTextField.attributedPlaceholder = NSAttributedString(string: "Enter Title",attributes: attributes)
           
        }
        else{
            classTestView.subjectOrTitleButton.setTitle("Add Title", for: .normal)
            addPickerview()
            classTestView.selectSubjectOrTitleTextField.reloadInputViews()
            selectSubjectPicker.selectRow(0, inComponent: 0, animated: false)
            classTestView.selectSubjectOrTitleTextField.text = ""
            classTestView.selectSubjectOrTitleTextField.attributedPlaceholder = NSAttributedString(string: "Select Subject",attributes: attributes)

        }
    }
    
    @objc private func createClasstestViewClosed() {
        classTestView.removeFromSuperview()
    }
    
    @objc private func popupDatePicker(){
        
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        datePickerVC.delegate = self
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            weakSelf!.classTestAddDate = date as Foundation.Date
            weakSelf!.classTestView.dateofClassTestTexfield.text = DateApi.convertThisDateToString(weakSelf!.classTestAddDate, formatNeeded: .ddMMyyyyWithHypen)
            
        }
        prepareOverlayVC(overlayVC: datePickerVC)
        
        self.present(datePickerVC, animated: false, completion: nil)

    }

    func ButtonPressed(){
        
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    @objc private func createClassTestButtonViewTapped() {
        let result = validateclassTest()
        if result.0 {
            var subjectId:String = ""
            var classTestName:String = ""
            if classTestView.subjectOrTitleButton.isHidden == false{
              if classTestView.subjectOrTitleButton.titleLabel?.text == "Add Title" {
               let rowValue = selectSubjectPicker.selectedRow(inComponent: 0)
                 if rowValue != 0{
                 subjectId = String(subjectList[rowValue].subjectId)
                 }
              }
              else{
                classTestName = classTestView.selectSubjectOrTitleTextField.text!
                subjectId = ""
               }
            }
            else{
                classTestName = classTestView.selectSubjectOrTitleTextField.text!
                subjectId = ""
            }
            
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            WebServiceApi.postNewClassTest(subjectId: subjectId,
                                           classTestName: classTestName,
                                           currentUserRoleId: (user?.currentUserRoleId)! ,
                                           date: classTestView.dateofClassTestTexfield.text!,
                                           classId: String(studentOrClassId) ,
                                           Topics: classTestView.classTestTopicsTextview.text ,
                                           maxMark: classTestView.maxMarkTextField.text! ,
                                             onSuccess: { (result) in
                                                
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                if (result["error"] as? [String:AnyObject]) == nil{
                                                    self.classTestView.removeFromSuperview()
                                                    self.getClassTestList()
                                                    
                                                    
                                                }else{
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.classTestCreationFailed, stateOfMessage: .failure)
                                                }
                                                
                                                
                                                
                                                
                },
                                             onFailure: { (error) in
                                                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.diarySendFailed, stateOfMessage: .failure)
                                                
                },
                                             duringProgress: { (progress) in
                                                
            })
            
  
        }
        else{
          AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
    }
    
    func validateclassTest() -> (Bool,String){
        var result:Bool = true
        var errorMessage = ""
        if classTestView.selectSubjectOrTitleTextField.text  == "" || classTestView.selectSubjectOrTitleTextField.text == "Select Subject" {
            errorMessage = ConstantsInUI.classtestSubjectSelectionFailureMessage
            result = false
        }
        else if classTestView.dateofClassTestTexfield.text == "" {
            errorMessage = ConstantsInUI.classtestdateError
            result = false
        }
        else if classTestView.maxMarkTextField.text == "" {
            errorMessage = ConstantsInUI.classtestmaxmarkError
            result = false
        }
        return (result,errorMessage)
    }
    
    
}
