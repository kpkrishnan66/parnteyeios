//
//  AttendanceDetailsCell.swift
//  parentEye
//
//  Created by scientia on 18/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
class AttendanceDetailsCell: UITableViewCell {

    @IBOutlet var studentImage: UIImageView!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var leaveStatus: UILabel!
    
    @IBOutlet weak var callIcon: UIButton!
}
