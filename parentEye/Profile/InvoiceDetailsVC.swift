//
//  InvoiceDetailsVC.swift
//  parentEye
//
//  Created by scientia on 25/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import Alamofire

class invoiceDetailsVC:UIViewController,CommonHeaderProtocol,popOverDelegate,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,paymentConfirmedProtocol{
    
    @IBOutlet weak var schoolAddress: UILabel!
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var parentName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var payAmout: UILabel!
    @IBOutlet weak var payNowButton: UIButton!
    @IBOutlet weak var invoiceDetailsTableView: UITableView!
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var paymentIdViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paymentIdView: UIView!
    @IBOutlet weak var PaymentDate: UILabel!
    @IBOutlet weak var paymentSelectedIconHeight: NSLayoutConstraint!
    @IBOutlet var paymentIdLabel: UILabel!
    
    
    var user:userModel?
    var profilePic = ""
    var schoolNameValue:String = ""
    var schoolAddressValue:String = ""
    var totalAmountValue:Float = 0
    var invoiceDetailsVCArray = [invoiceList]()
    var invoiceFeeListArray = [Int]()
    var feeListArray = [feeList]()
    var selectedInvoice:String = ""
    let redColur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    let lightredColur = UIColor(red: 255.0/255.0, green: 229.0/255.0, blue: 229.0/255.0, alpha: 1.0)
    var responseString:String = ""
    var responseHeader:String = ""
    var responseContent:String = ""
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var pdfViewer = PdfController()
    var payNowButtonClicked:Bool = false
    var paymentId:String = ""
    var newAdmissionArray = [newAdmission]()
    var paymentFeeDetailsArray = [feeDetails]()
    var sucessResponse:String = "0"
    var fromFeePaymentDetails:Bool = false
    var canDownloadReceipt:Bool = false
    var admsnVC = AdmissionPayPopupVC()
    var atomResponse = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Added to adjust the TABLE HEADER
        self.tabBarController?.tabBar.isHidden = true
        setupTable()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        //self.aut
        //populateInvoiceDetailsTableView()
        //getinvoiceList()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    fileprivate func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.invoice, withProfileImageUrl: profilePic)
        header.downArrowButton.isHidden = true
        header.profileChangeButton.isHidden = true
        header.delegate = self
    }
    
    fileprivate func setupTable(){
        invoiceDetailsTableView.register(UINib(nibName: "invoiceDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "invoiceDetailsTableViewCell")
        invoiceDetailsTableView.estimatedRowHeight = 44.0
        
        invoiceDetailsTableView.register(UINib(nibName: "invoiceSectionDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "invoiceSectionDetailsTableViewCell")
        invoiceDetailsTableView.estimatedRowHeight = 44.0
        
        //invoiceDetailsTableView.rowHeight          = UITableViewAutomaticDimension
        
    }
    
    fileprivate func setupUI() {
        subView.layer.borderWidth = 1
        subView.layer.borderColor = UIColor.lightGray.cgColor
        schoolName.text = schoolNameValue
        schoolAddress.text = schoolAddressValue
        parentName.text = ":  "+(user?.username)!
        if totalAmountValue != 0 {
            payAmout.text = String(totalAmountValue)
        }
        date.text = ":  "+DateApi.convertThisDateToString(Foundation.Date(), formatNeeded: .ddMMyyyyWithHypen)
        
        if selectedInvoice == "due" {
            paymentIdViewHeight.constant = 0
            paymentSelectedIconHeight.constant = 0
            payNowButton.setTitle("Pay Now",for: UIControlState())
            payNowButton.backgroundColor = greenColur
        }
        else{
            paymentIdViewHeight.constant = 50
            paymentSelectedIconHeight.constant = 35
            payNowButton.setTitle("Receipt",for: UIControlState())
            payNowButton.backgroundColor = redColur
            if invoiceDetailsVCArray.count > 0 {
                if invoiceDetailsVCArray[0].canDownloadReceipt == false {
                    payNowButton.backgroundColor = lightredColur
                    payNowButton.isUserInteractionEnabled = false
                    canDownloadReceipt = false
                }else{
                    payNowButton.backgroundColor = redColur
                    payNowButton.isUserInteractionEnabled = true
                    canDownloadReceipt = true
                }
                paymentIdLabel.text = invoiceDetailsVCArray[0].paymentId
                PaymentDate.text = invoiceDetailsVCArray[0].paidOn
            }
        }
        
        for pos in invoiceDetailsVCArray {
            invoiceFeeListArray.append(pos.feeListArray.count)
            for k in pos.feeListArray{
                feeListArray.append(k)
            }
        }
        //Getting a list of the docs directory
        let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last) as URL!
        do {
            //put the contents in an array.
            let contents = try (FileManager.default.contentsOfDirectory(at: docURL!, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
            print("HAI")
            print(contents)
            
        } catch let error as NSError {
            //print(error.localizedDescription)
        }
        
    }
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        if responseString == ConstantsInUI.atomSuccessResponse && fromFeePaymentDetails == false {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func showSwitchSibling(fromButton: UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.maketabBarRoot()
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return invoiceDetailsVCArray.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "invoiceSectionDetailsTableViewCell") as! invoiceSectionDetailsTableViewCell
        headerCell.invoiceName.text = invoiceDetailsVCArray[section].name
        headerCell.studentName.text = invoiceDetailsVCArray[section].studentName
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return invoiceDetailsVCArray[section].name
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoiceFeeListArray[section]
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "invoiceDetailsTableViewCell") as! invoiceDetailsTableViewCell
        cell.selectionStyle = .none
        cell.feeAmountOrStudentName.text = invoiceDetailsVCArray[indexPath.section].feeListArray[indexPath.row].amount
        cell.feeNameOrInvoiceName.text = invoiceDetailsVCArray[indexPath.section].feeListArray[indexPath.row].name
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    @IBAction func payNowButtonClicked(_ sender: UIButton) {
        if responseString == ConstantsInUI.atomSuccessResponse && canDownloadReceipt == true {
            downloadReciept()
        }
        else{
            let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
            admsnVC = storyboard.instantiateViewController(withIdentifier: "AdmissionPayPopupVC") as! AdmissionPayPopupVC;
            prepareOverlayVC(admsnVC)
            admsnVC.delegate = self
            admsnVC.fromInvoiceDetailsvc = true
            admsnVC.totalAmountValue = String(totalAmountValue)
            admsnVC.invoiceDetailsVCArray = invoiceDetailsVCArray
            Analytics.logEvent("P_feeInvoice_payNowBtn", parameters: nil)
            self.atomResponse = true
            self.present(admsnVC, animated: false, completion: nil)
        }
    }
    
    fileprivate func prepareOverlayVC(_ overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    func getAtomResponse(feeResponse:String,paymentId id:String,paymentFeeDetailsArray array:[feeDetails]) {
        self.admsnVC.dismiss(animated: false, completion: nil)
        paymentFeeDetailsArray = array
        responseString = feeResponse
        paymentId = id
        payNowButtonClicked = true
        if atomResponse == true {
            self.atomResponse = false
            sendPaymentStatus()
        }
    }
    
    func admissionPayPopupVCDismissed(feeVersionChangedAlert message:String) {
        
        if responseString == ConstantsInUI.atomSuccessResponse {
            responseHeader = ConstantsInUI.paymentSuccessHeaderAlert
            responseContent = ConstantsInUI.paymentsucessContentAlert
            paymentIdViewHeight.constant = 50
            paymentSelectedIconHeight.constant = 35
            payNowButton.setTitle("Receipt",for: UIControlState())
            payNowButton.backgroundColor = redColur
            payNowButton.isUserInteractionEnabled = true
            if paymentFeeDetailsArray.count > 0 {
                paymentIdLabel.text = paymentFeeDetailsArray[0].paymentId
                PaymentDate.text = paymentFeeDetailsArray[0].paidOn
            }
            payNowButtonClicked = true
            sucessResponse = "1"
            fromFeePaymentDetails = false
            canDownloadReceipt = true
            sucesssAlert()
        }
        else if responseString == ConstantsInUI.atomFailedResponse {
            responseHeader = ConstantsInUI.paymentFailedHeaderAlert
            responseContent = ConstantsInUI.paymentFailedContentAlert
            payNowButtonClicked = false
            failedAlert()
        }
        else{
            //responseHeader = ConstantsInUI.paymentFailedHeaderAlert
            //responseContent = ConstantsInUI.paymentFailedContentAlert
            payNowButtonClicked = false
            //failedAlert()
        }
        
    }
    
    func  sucesssAlert() {
        
        let alrt =  AlertController.getSingleOptionAlertControllerWithMessage(mesasage: responseContent, titleOfAlert: responseHeader, oktitle: ConstantsInUI.done, OkButtonBlock: nil)
        self.present(alrt, animated: false, completion: nil)
        
        
    }
    
    func failedAlert() {
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: responseContent, titleOfAlert: responseHeader, oktitle: ConstantsInUI.tryagain, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            self.payNowButtonClicked(self.payNowButton)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    
    func sendPaymentStatus () {
        var paymentType:String = ""
        paymentType = "FeePaymentDetails"
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.postAdmissionPaymentStatus(response: responseString ,
                                                 id: paymentId,paymentType: paymentType,
                                                 onSuccess: { (result) in
                                                    
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    print(result)
                                                    if let ErrorResponse = result["error"] as? NSDictionary{
                                                        if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                            
                                                            AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                                                        }
                                                    }
                                                    else{
                                                        
                                                        //self.admsnVC.viewDidDisappear(true)
                                                        //self.admsnVC.dismissViewControllerAnimated(false, completion: nil)
                                                    }
                                                    
                                                    
                                                    
                                                    
        },
                                                 onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                    
        },
                                                 duringProgress: { (progress) in
                                                    
        })
        
        
        
    }
    
    
    func downloadReciept () {
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.downloadReciept(invoiceDetailsVCArray: invoiceDetailsVCArray,
                                      user: user,paymentFeeDetailsArray: paymentFeeDetailsArray,sucessResponse: sucessResponse,
                                      onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        print(result)
                                        if let ErrorResponse = result["error"] as? NSDictionary{
                                            if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                
                                                AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                                            }
                                        }
                                        else{
                                            if let url = result["feeInvoiceDownload"] as? String {
                                                self.pdfViewer.addPdfToVC(vc: weakSelf!, urlToBeLoaded:url)
                                                /*let name = self.paymentId+".pdf"
                                                 self.pdfViewer.showPdfInViewController(weakSelf!,pdfHavingUrl: url,withName: name)*/
                                            }
                                            
                                        }
                                        
                                        
                                        
                                        
        },
                                      onFailure: { (error) in
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
        },
                                      duringProgress: { (progress) in
                                        
        })
        
        
        
    }
    
    func payStackResponse(feeResponse:String,paymentId referenceId: String,paymentFeeDetailsArray: [feeDetails]) {
        if referenceId == "" {
            self.responseString = ConstantsInUI.atomFailedResponse
            self.admsnVC.dismiss(animated: false, completion: nil)
        }
        else{
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.checkForSuccessPayment(referenceId: referenceId,
                                                 onSuccess: { (result) in
                                                    
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    print(result)
                                                    if let ErrorResponse = result["error"] as? NSDictionary{
                                                        if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                            
                                                            AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                                                        }
                                                    }
                                                    else{
                                                        if let response = result["checkForSuccessPayment"] as? String {
                                                            if response == "success" {
                                                                self.responseString = ConstantsInUI.atomSuccessResponse
                                                            }
                                                            else{
                                                                self.responseString = ConstantsInUI.atomFailedResponse
                                                            }
                                                            
                                                            self.getAtomResponse(feeResponse: self.responseString,paymentId: referenceId,paymentFeeDetailsArray: paymentFeeDetailsArray)
                                                            
                                                        }
                                                    }
            },
                                                 onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                    
            },
                                                 duringProgress: { (progress) in
                                                    
            })
        }
    }
}
