//
//  StudentShuffleTableViewCell.swift
//  parentEye
//
//  Created by scientia on 08/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class StudentShuffleTableViewCell: UITableViewCell {
    
    
    @IBOutlet var selectionButton: UIButton!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var fromClassName: UILabel!
    @IBOutlet var studentProfilePic: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.studentProfilePic?.image = UIImage(named: "avatar")
        self.studentName?.text = ""
        self.fromClassName?.text = ""
    }

    
}
