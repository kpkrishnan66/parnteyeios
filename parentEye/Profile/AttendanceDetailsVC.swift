//
//  AttendanceDetailsVC.swift
//  parentEye
//
//  Created by scientia on 18/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit


class AttendanceDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet var studentListTableView: UITableView!
    @IBOutlet var noAbsenteesLabel: UILabel!
    @IBOutlet var className: UILabel!
    @IBOutlet var closeBtn: UIButton!
    
    var date = ""
    var schoolClassId = 0
    var classNameText = ""
    var attendanceArray = [AttendanceClassReport]()
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentListTableView.register(UINib(nibName: "AttendanceDetailsCell", bundle: nil), forCellReuseIdentifier: "AttendanceDetailsCell")
        studentListTableView.estimatedRowHeight = 76.0
        studentListTableView.rowHeight          = UITableViewAutomaticDimension
        studentListTableView.dataSource = self
        studentListTableView.delegate = self
        //studentListTableView.text = String(usersArray.count)
        closeBtn.addTarget(self, action: #selector(closePopup), for: .touchUpInside)
        makeWebApiCallToGetAttendanceReportOfClass()
        className.text = classNameText
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return attendanceArray.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AttendanceDetailsCell") as! AttendanceDetailsCell
        
        let attendance = attendanceArray[indexPath.row]
        cell.selectionStyle = .none
        cell.studentName.text        = attendance.studentName
        cell.leaveStatus.text = attendance.leaveReason
        
        //this is added by jithin on 10-04-2018 for making a call from the student profile
        cell.callIcon.tag = indexPath.row
 
        cell.callIcon.addTarget(self, action: #selector(self.callIconTapped(_:)), for: UIControlEvents.touchUpInside)
        
        let profileImage = cell.studentImage
        
        if attendance.studentProfilePic == ""{
            cell.studentImage.image = UIImage(named: "avatar")
            cell.studentImage.layer.masksToBounds = true
            cell.studentImage.layer.cornerRadius = 0
            cell.studentImage.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: attendance.studentProfilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.studentImage.layer.masksToBounds = false
                    cell.studentImage.layer.cornerRadius = cell.studentImage.frame.height/2
                    cell.studentImage.clipsToBounds = true
                    profileImage?.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        
        
        return cell
    }
    
    
    // this is added by jithin on 10-04-2018 for making a call from attendence
    
//    @objc func callIconTapped(_ sender:UIButton){
//        print("INSIDE callIconTapped")
//        showAlert()
//    }
    
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    @objc func callIconTapped(_ sender:AnyObject){
        
        mobileNumber = ""
        for k in 0 ..< attendanceArray.count {
            if k == sender.tag {
                mobileNumber = attendanceArray[k].mobileNo
                
            }
        }
        showAlert()
//        if let url = URL(string: "tel://\(mobileNumber)") {
//            UIApplication.shared.openURL(url)
//        }
    }
    
    
    @IBAction func closePopup(_ sender: AnyObject) {
        self.dismiss(animated: false, completion: nil)
        
    }
    
    fileprivate func makeWebApiCallToGetAttendanceReportOfClass(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getAttendanceReportOfClass(date: date, schoolClassId: schoolClassId ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            weakSelf!.attendanceArray = parseAttendanceClassReportArray.parseThisDictionaryForAttendanceClassRecords(dictionary: result)
            if self.attendanceArray.count == 0 {
                self.noAbsenteesLabel.isHidden  = false
            }
            else{
                self.noAbsenteesLabel.isHidden = true
            }
            self.studentListTableView.reloadData()
            
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
        
    }
}
