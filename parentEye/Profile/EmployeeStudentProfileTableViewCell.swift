//
//  EmployeeStudentProfileTableViewCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 02/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class EmployeeStudentProfileTableViewCell: UITableViewCell {
    
    
    @IBOutlet var nameofStudent: UILabel!
    @IBOutlet var studentProfilepic: UIImageView!
    
    @IBOutlet var studentCallIcon: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.studentProfilepic?.image = UIImage(named: "avatar")
        self.nameofStudent?.text = ""
    }
}
