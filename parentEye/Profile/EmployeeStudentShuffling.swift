//
//  EmployeeStudentShuffling.swift
//  parentEye
//
//  Created by scientia on 07/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class EmployeeStudentShuffling: UIViewController,CommonHeaderProtocol,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var classListTextField: UITextField!
    @IBOutlet var classListCollectionView: UICollectionView!
    @IBOutlet var studentListTableView: UITableView!
    @IBOutlet var moveToButton: UIButton!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var classListCollectionViewHeight: NSLayoutConstraint!
    
    
    var profilePic  = ""
    var user:userModel? // Used tovarore the userdata.
    var ClassesList:[EmployeeClassModel] = []
    var initialIndexpath = NSIndexPath()
    var ClassIsSelected = [Int: Bool]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var defaultClassesList:[EmployeeClassModel] = []
    var classId:Int = 0
    var className = ""
    let picker = UIPickerView()
    var studentListArray = [composeDiaryStudentPopupList]()
    var tempStudentListArray = [composeDiaryStudentPopupList]()
    


    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setupUI()
        alterHeader()
        initializeTableView()
        //makeWebApiCallToGetstudentPromotionList()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    private func alterHeader() {
       
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentPromotion+" - "+className, withProfileImageUrl: profilePic)
        classListCollectionViewHeight.constant = 44
        header.delegate = self
    }
    
    private func initializeTableView(){
        studentListTableView.register(UINib(nibName: "StudentShuffleTableViewCell", bundle: nil), forCellReuseIdentifier: "StudentShuffleTableViewCell")
    }
    
    private func setupUI() {
        
        var pos:Int = 0
        ClassesList.removeAll()
        
        for k in 0 ..< (user?.Classes)!.count {
            ClassesList.append((user?.Classes![k])!)
            ClassIsSelected[pos] = false
            pos = pos + 1
        }
        
        if ClassesList.count > 0 {
            classId = ClassesList[0].classId
            className = ClassesList[0].className
            ClassIsSelected[0] = true
            defaultClassesList.removeAll()
            defaultClassesList.append(ClassesList[0])
        }
        
        classListTextField.delegate = self
        classListTextField.inputView = picker
        picker.delegate = self
        picker.dataSource = self
        
        if ClassesList.count > 0 {
         classListTextField.text = ClassesList[0].className
         className = ClassesList[0].className
         classId = ClassesList[0].classId
        }
        
        classListTextField.layer.borderColor = UIColor.lightGray.cgColor
        classListTextField.layer.borderWidth = 1
        
    }
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
    }
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        initialIndexpath  = IndexPath(row: 0, section: 0) as NSIndexPath
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StudentShufflingCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    private func makeWebApiCallToGetstudentPromotionList() {
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getAllStudentListForPromotion(schoolClassId: String(classId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let PasswordResponse = result["error"] as? NSDictionary{
                if let errorMessage = PasswordResponse["errorMessage"] as? String{
                    AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                }}
            else{
                
                self.studentListArray.removeAll()
                weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
                self.studentListTableView.reloadData()
                
           }
        
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }
    
    //tableview delegate and datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return studentListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentShuffleTableViewCell") as! StudentShuffleTableViewCell
        cell.studentName.text = studentListArray[indexPath.row].studentName
        
        weak var profileImage = cell.studentProfilePic
        if studentListArray[indexPath.row].compressedProfilePic == ""{
            cell.studentProfilePic.image = UIImage(named: "avatar")
            cell.studentProfilePic.layer.masksToBounds = true
            cell.studentProfilePic.layer.cornerRadius = 0
            cell.studentProfilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: studentListArray[indexPath.row].compressedProfilePic , oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.studentProfilePic.layer.masksToBounds = false
                    cell.studentProfilePic.layer.cornerRadius = cell.studentProfilePic.frame.height/2
                    cell.studentProfilePic.clipsToBounds = true
                    
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }

        
        return cell
    }
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }


    //pickerView Delegate and Datasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ClassesList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ClassesList[row].className
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        classListTextField.text = ClassesList[row].className
    }
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        className = ClassesList[sender.tag].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        alterHeader()
        classListCollectionView.reloadData()
        //studentRecordArray.removeAll()
        //studentPromotionListingTable.reloadData()
        //makeWebApiCallToGetstudentPromotionList()
        
    }

    
}
