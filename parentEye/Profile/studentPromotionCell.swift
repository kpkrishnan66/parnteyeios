//
//  studentPromotionCell.swift
//  parentEye
//
//  Created by scientia on 14/03/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class studentPromotionCell: UITableViewCell {
    
    @IBOutlet var studentName: UILabel!
    @IBOutlet var callIcon: UIButton!
    @IBOutlet var studentProfilePic: UIImageView!
    @IBOutlet var studentPromotionStatus: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.studentProfilePic?.image = UIImage(named: "avatar")
        self.studentName?.text = ""
        self.studentPromotionStatus?.text = ""
    }
}
