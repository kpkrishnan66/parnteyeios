//
//  ClassTestListingCell.swift
//  parentEye
//
//  Created by scientia on 17/07/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ClassTestListingCell: UITableViewCell {
    
    @IBOutlet var HeaderName: UILabel!
    
    @IBOutlet var createdByname: UILabel!
    
    @IBOutlet var topicName: UILabel!
    
    @IBOutlet var mark: UILabel!
    
    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var upcomingLabel: UILabel!
    
    @IBOutlet var monthAndYearLabel: UILabel!
}
