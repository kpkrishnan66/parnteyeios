//
//  studentPromotionVCCell.swift
//  parentEye
//
//  Created by scientia on 14/03/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class studentPromotionVCCell: UITableViewCell {
    
    @IBOutlet var studentProfilePic: UIImageView!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var studentStatusButton: UIButton!
}
