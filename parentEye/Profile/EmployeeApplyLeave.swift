//
//  EmployeeApplyLeave.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 29/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class EmployeeApplyLeave: UIViewController,CommonHeaderProtocol,UITextFieldDelegate,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,dateSelectedButtonPressedProtocol,chooseAbsenteesStudentListProtocol{

    
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet weak var noAbsenteesButton: UIButton!
    @IBOutlet weak var takAttendanceTableViewLabel: UILabel!
    @IBOutlet var chooseAbsentees: UIButton!
    @IBOutlet var dateView: UITextField!
    @IBOutlet var applyLeaveCollectionView: UICollectionView!
    @IBOutlet var applyLeaveTableView: UITableView!
    
    let reuseIdentifier = "cell"
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var applyLeaveAddDate = Foundation.Date()
    var ClassesList:[EmployeeClassModel] = []
    var studentRecordArray:[StudentAbsentRecord] = []
    var receipientArray = [composeDiaryStudentPopupList]()
    var noAbsenteesButtonTapped:Bool = false
    var user:userModel?
    var ClassIsSelected = [Int: Bool]()
    var defaultClassesList:[EmployeeClassModel] = []
    var classId:Int = 0
    var className = ""
    var dateOfAbsent = ""
    var isleaveSubmissionPushNotification = false
    var initialIndexpath = NSIndexPath()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var profilePic = ""
    var studentListArray = [composeDiaryStudentPopupList]()
    var tempStudentListArray = [composeDiaryStudentPopupList]()
    var absentStudentListArray = [composeDiaryStudentPopupList]()
    var nonabsentStudentListArray = [composeDiaryStudentPopupList]()
    var studentListString = ""
    var currentDate = ""
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateView.delegate = self
        getUserDataFromDB()
        alterHeader()
        initializeTableView()
        if isleaveSubmissionPushNotification{
          setUpUIForPushNotification()
        }else{
          setUpUi()
        }
        makeWebApiCallToGetEmployeeAttendanceReport()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    

    private func setUpUi(){
        chooseAbsentees.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -50.0, bottom: 0.0, right: -50.0)
        dateView.text = DateApi.convertThisDateToString(applyLeaveAddDate, formatNeeded: .ddMMyyyyWithHypen)
        currentDate = dateView.text!
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        className = ClassesList[0].className
        defaultClassesList.removeAll()
        
        defaultClassesList.append(ClassesList[0])
    }
    
    private func setUpUIForPushNotification(){
        chooseAbsentees.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -50.0, bottom: 0.0, right: -50.0)
        dateView.text = dateOfAbsent
        noAbsenteesButton.isHidden = true
        chooseAbsentees.isHidden = true
        ClassesList = (user?.Classes)!
        var rowvalue = 0
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
            if  ClassesList[k].classId == classId{
                rowvalue = k
            }
            
        }
        ClassIsSelected[rowvalue] = true
        applyLeaveCollectionView.reloadData()
        if ClassesList.count > 1
        {
            applyLeaveCollectionView.scrollToItem(at: IndexPath(row: rowvalue, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
    }
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    private func alterHeader() {
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.TakeAttendanceScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    
    private func initializeTableView(){
        applyLeaveTableView.register(UINib(nibName: "AbsendStudentCell", bundle: nil), forCellReuseIdentifier: "AbsendStudentCell")
        applyLeaveTableView.estimatedRowHeight = 129.0
        applyLeaveTableView.rowHeight          = UITableViewAutomaticDimension
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == dateView{
            dateView.resignFirstResponder()
            showDatePicker()
         }
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        initialIndexpath  = NSIndexPath(row: 0, section: 0)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! EmployeeApplyLeaveCollectionViewClassListCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return studentRecordArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AbsendStudentCell") as! AbsendStudentCell
            let record = studentRecordArray[indexPath.row]
            cell.nameOfStudent.text = record.studentName
            weak var profileImage = cell.studentProfileView
            
            if record.profilePic == ""{
                cell.studentProfileView.image = UIImage(named: "avatar")
                cell.studentProfileView.layer.masksToBounds = true
                cell.studentProfileView.layer.cornerRadius = 0
                cell.studentProfileView.clipsToBounds = false
                
            }
            else{
                ImageAPi.fetchImageForUrl(urlString: record.profilePic, oneSuccess: { (image) in
                    if profileImage != nil {
                        
                        cell.studentProfileView.layer.masksToBounds = false
                        cell.studentProfileView.layer.cornerRadius = cell.studentProfileView.frame.height/2
                        cell.studentProfileView.clipsToBounds = true
                        
                        profileImage!.image = image
                        
                    }
                }) { (error) in
                    
                }
            }
        
        
            cell.leaveReason.text = record.reason
            cell.reasonLabel.text = "Reason"
            cell.callIcon.tag = indexPath.row
            cell.callIcon.addTarget(self, action: #selector(self.callIconTapped), for: UIControlEvents.touchUpInside)
        
        
            return cell
    }
    
    
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
         _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
                
    }
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    @IBAction func showDatePicker(_ sender: UIButton) {
        showDatePicker()
    }
    
    func showDatePicker(){
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        datePickerVC.delegate = self
        datePickerVC.viewLoadedCompletion = {(datePicker)->Void in
            
           // datePicker.minimumDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: -30, to: Foundation.Date(), options: [])
            datePicker.date  = DateApi.convertThisStringToDate(dateString:self.dateView.text!,formatNeeded: .ddMMyyyyWithHypen) as Foundation.Date
            print("zzzzzzz")
           // datePicker.maximumDate = Foundation.Date()
            
        }
        
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            
            weakSelf!.applyLeaveAddDate = date as Foundation.Date
            weakSelf!.dateView.text = DateApi.convertThisDateToString(weakSelf!.applyLeaveAddDate, formatNeeded: .ddMMyyyyWithHypen)
            if self.currentDate == weakSelf!.dateView.text!{
                self.noAbsenteesButton.isHidden = false
                self.chooseAbsentees.isHidden = false
            }else if self.currentDate > weakSelf!.dateView.text!{
                self.noAbsenteesButton.isHidden = true
                self.chooseAbsentees.isHidden = false
            }
            else{
                self.noAbsenteesButton.isHidden = true
                self.chooseAbsentees.isHidden = true
            }
        }
        
        prepareOverlayVC(overlayVC: datePickerVC)
        self.present(datePickerVC, animated: false, completion: nil)
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    private func makeWebApiCallToGetEmployeeAttendanceReport(){
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.getAttendanceStatus(date: dateView.text!, classId: classId ,onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                
                if let studentAbsentRecordRoot = result[resultsJsonConstants.StudentAbsentRecord.studentAbsentRecord] as? [String:AnyObject]{
                    if let attendanceResult = studentAbsentRecordRoot[resultsJsonConstants.StudentAbsentRecord.attendanceResult] as? String{
                        self.studentRecordArray.removeAll()
                        self.tempStudentListArray.removeAll()
                        self.applyLeaveTableView.reloadData()
                      if attendanceResult == "Attendance not taken" {
                       self.takAttendanceTableViewLabel.text = "Attendance is not marked yet "+self.className
//                       self.noAbsenteesButton.isHidden = false
//                        self.chooseAbsentees.isHidden = false
                      }
                      if attendanceResult == "No Absentese" {
//                             self.noAbsenteesButton.isHidden = true
//                             self.chooseAbsentees.isHidden = false
                             self.takAttendanceTableViewLabel.text = "Attendance Marked.Full Present"
                      }
                    }
                }
                else{
                self.noAbsenteesButton.isHidden = true
                self.takAttendanceTableViewLabel.text = ""
                weakSelf!.addEntriesTostudentRecordArray(array: parseAbsentRecordArray.parseThisDictionaryForAbsentRecords(dictionary: result))
                }
                
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                }, duringProgress: { (progress) in
                    
                })
        
    }
    
    private func addEntriesTostudentRecordArray(array:[StudentAbsentRecord]){
        studentRecordArray.removeAll()
        studentRecordArray = array
        self.tempStudentListArray.removeAll()
        applyLeaveTableView.reloadData()
    }
    
    @objc func callIconTapped(sender:UIButton){
        
        for k in 0 ..< studentRecordArray.count {
            if k == sender.tag{
                mobileNumber = studentRecordArray[k].mobileNo
                
            }
        }
        
        showAlert()
    }
    
    // this is added by jithin on 28-03-2018 for making call.
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
     func ButtonPressed(){
        makeWebApiCallToGetEmployeeAttendanceReport()
    }
    
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        className = ClassesList[sender.tag].className
        dateView.text = DateApi.convertThisDateToString(Foundation.Date(), formatNeeded: .ddMMyyyyWithHypen)
        self.noAbsenteesButton.isHidden = false
        self.chooseAbsentees.isHidden = false
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        applyLeaveCollectionView.reloadData()
        studentRecordArray.removeAll()
        applyLeaveTableView.reloadData()
        makeWebApiCallToGetEmployeeAttendanceReport()
        
    }
    
    @IBAction func noAbsenteesAction(_ sender: UIButton) {
        noAbsenteesButtonTapped = true
        receipientArray.removeAll()
        postAbsentees()
    }
    
    @IBAction func chooseAbsenteesAction(_ sender: UIButton) {
        
        if currentDate == dateView.text{
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: weakSelf!)
            WebServiceApi.getAllStudentListToChooseAbsentees(schoolClassId: String(classId),date: dateView.text!, onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if let PasswordResponse = result["error"] as? NSDictionary{
                    if let errorMessage = PasswordResponse["errorMessage"] as? String{
                        
                        AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                    }}
                else{
                weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
                self.showStudentListPopUp()
                }
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
                }, duringProgress: { (progress) in
                    
            })
        
        }
        else{
         AlertController.showToastAlertWithMessage(message: ConstantsInUI.TakeAttendanceAlert, stateOfMessage: .failure)
        }
    
  }
    
    
    
    func showStudentListPopUp(){
        sortSelectedStudents()
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "ChooseAbsenteesStudentList") as! ChooseAbsenteesStudentList ;
        
        
        msRecipVC.recipientArray = tempStudentListArray
        msRecipVC.searchrecipientArray = tempStudentListArray
        msRecipVC.previousEntryArray = absentStudentListArray
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
        
    }
    private func sortSelectedStudents(){
        tempStudentListArray.removeAll()
        for k in 0 ..< studentListArray.count {
            if studentListArray[k].isAbsent{
            tempStudentListArray.append(studentListArray[k])
            }
        }
        absentStudentListArray.removeAll()
        absentStudentListArray = tempStudentListArray
        nonabsentStudentListArray.removeAll()
        for k in 0 ..< studentListArray.count {
            if !studentListArray[k].isAbsent{
                nonabsentStudentListArray.append(studentListArray[k])
            }
        }
        for k in 0 ..< nonabsentStudentListArray.count {
            tempStudentListArray.append(nonabsentStudentListArray[k])
        }
    }
    
    func updateSelectedWithStudentArray(receipientArray array: [composeDiaryStudentPopupList]) {
        receipientArray = array
        postAbsentees()
    }
    
    func postAbsentees() {
        
        studentListString = ""
        if receipientArray.count > 0{
        for studentArray in receipientArray{
            studentListString = studentListString+String(studentArray.studentId) + ","
        }
            studentListString = studentListString.substring(to:studentListString.characters.index(before: studentListString.endIndex) )
        }
        
    if noAbsenteesButtonTapped == false {
      let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.absent, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.confirm, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            
            self.leaveStatus()
            
        }
        self.present(alert, animated: false, completion: nil)
    }
    else{
        self.leaveStatus()
    }

    }
    
        
    func leaveStatus() {
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.postAbsentees(date: self.dateView.text!,
                                    userRoleId: (self.user?.currentUserRoleId)!,
                                    studentIdList:self.studentListString ,
                                    classId: String(self.classId),
                                    onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        if (result["error"] as? [String:AnyObject]) == nil{
                                            AlertController.showToastAlertWithMessage(message: ConstantsInUI.leaveSubmitsuccess, stateOfMessage: .success)
                                            self.makeWebApiCallToGetEmployeeAttendanceReport()
                                        }else{
                                            AlertController.showToastAlertWithMessage(message: ConstantsInUI.leaveSubmitFailed, stateOfMessage: .failure)
                                        }
                                        
                                        
                                        
                                        
            },
                                    onFailure: { (error) in
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
            },
                                    duringProgress: { (progress) in
                                        
        })
    }

}
