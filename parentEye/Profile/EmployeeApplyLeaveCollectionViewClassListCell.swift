//
//  EmployeeApplyLeaveCollectionViewClassListCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 30/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class EmployeeApplyLeaveCollectionViewClassListCell: UICollectionViewCell {

    @IBOutlet var selectedClass: UIButton!
    
}