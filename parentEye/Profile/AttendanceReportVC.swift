//
//  AttendanceReportVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 09/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class AttendanceReportVC: UIViewController,CommonHeaderProtocol,UITextFieldDelegate,dateSelectedButtonPressedProtocol,UITableViewDataSource,UITableViewDelegate,SelectedSchoolpopOverDelegate,UIPopoverPresentationControllerDelegate{
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var dateDetailedView: UITextField!
    @IBOutlet var classAbsenteesTableView: UITableView!
    @IBOutlet var countOfAbsentees: UILabel!
    @IBOutlet var dateView: UITextField!
    @IBOutlet var todayLabel: UILabel!
    @IBOutlet var todayLabelWidth: NSLayoutConstraint!
    @IBOutlet var weekDay: UILabel!
    
    
    var user:userModel?
    var profilePic = ""
    var attendanceAddDate = Foundation.Date()
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var attendanceReportArray = [AttendanceReport]()
    var currentDate = ""
    var schoolId:Int = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        initializeTableView()
        makeWebApiCallToGetEmployeeAttendanceReport()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
            _ = navigationController?.popViewController(animated: true)
    }
        
    
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.TakeAttendanceScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    private func setupUI()
    {
        dateView.delegate = self
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        dateView.layer.borderWidth = CGFloat(1)
        dateView.layer.borderColor = color.cgColor
        dateView.text = DateApi.convertThisDateToString(attendanceAddDate, formatNeeded: .ddMMyyyyWithHypen)
        currentDate = dateView.text!
        countOfAbsentees.isUserInteractionEnabled = false
        todayLabel.isUserInteractionEnabled = false
        weekDay.isUserInteractionEnabled = false
        dateView.isUserInteractionEnabled = false
        dateDetailedView.isUserInteractionEnabled = false
    }
    
    private func initializeTableView(){
        classAbsenteesTableView.register(UINib(nibName: "AttendanceReportCell", bundle: nil), forCellReuseIdentifier: "AttendanceReportCell")
       // classAbsenteesTableView.estimatedRowHeight = 129.0
       // classAbsenteesTableView.rowHeight          = UITableViewAutomaticDimension
    }
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        header.multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
        self.tabBarController?.selectedIndex = 2
        _ = self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return attendanceReportArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "AttendanceReportCell") as! AttendanceReportCell
        let record = attendanceReportArray[indexPath.row]
        cell.selectionStyle = .none
        cell.className.text = record.className
        cell.attendance.text = /*record.noOfAbsentees + "/" + record.totalStudents*/record.message
        cell.teacherName.text = record.teacherName
        let tapAttendanceInfo = UITapGestureRecognizer(target: self, action: #selector(self.attendanceInfoButtonTapped))
        
        cell.attendanceInfoButton.addGestureRecognizer(tapAttendanceInfo)
        return cell
    }
    
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record = attendanceReportArray[indexPath.row]
        attendanceInfoButtonTapped(schoolClassId: record.schoolClassId,className: record.className)
    }
    
    @objc private func attendanceInfoButtonTapped(schoolClassId:Int,className:String)
    {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let attendanceDetails = storyboard.instantiateViewController(withIdentifier: "AttendanceDetailsVc") as! AttendanceDetailsVC ;
        attendanceDetails.date = dateView.text!
        attendanceDetails.schoolClassId = schoolClassId
        attendanceDetails.classNameText = className
        //attendanceDetails.delegate = self
        prepareOverlayVC(overlayVC: attendanceDetails)
        
        self.present(attendanceDetails, animated: false, completion: nil)

        
        
    }
    
    
    @IBAction func datePickerForAttendance(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
        weak var weakSelf = self
        datePickerVC.delegate = self
        datePickerVC.viewLoadedCompletion = {(datePicker)->Void in
            
            datePicker.minimumDate = (Calendar.current as NSCalendar).date(byAdding: .day, value: -30, to: Foundation.Date(), options: [])
            datePicker.maximumDate = Foundation.Date()
        }
        
        
        datePickerVC.valueChangedCompletion = {(date)->Void in
            
            
            weakSelf!.attendanceAddDate = date
            weakSelf!.dateView.text = DateApi.convertThisDateToString(weakSelf!.attendanceAddDate, formatNeeded: .ddMMyyyyWithHypen)
            
        }
        prepareOverlayVC(overlayVC: datePickerVC)
        
        self.present(datePickerVC, animated: false, completion: nil)
        
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    func ButtonPressed(){
      makeWebApiCallToGetEmployeeAttendanceReport()
    }
    
    private func makeWebApiCallToGetEmployeeAttendanceReport(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        if user?.currentTypeOfUser == .CorporateManager {
           schoolId = (user?.getStudentCurrentlySelectedSchool()?.schoolId)!
        }
        else{
          schoolId = (user?.schoolId)!
        }
        WebServiceApi.getAttendanceReportStatus(date: dateView.text!, schoolId: schoolId ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            weakSelf!.addEntriesTostudentRecordArray(array: parseAttendanceReportArray.parseThisDictionaryForAttendanceRecords(dictionary: result))
            
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    private func addEntriesTostudentRecordArray(array:[AttendanceReport]){
    
        var noOfAbsentees = 0
        var totalStudents = 0
        
        let date = formatDate(attendanceAddDate as Foundation.Date)
        attendanceReportArray.removeAll()
        attendanceReportArray = array
        
        for k in 0 ..< attendanceReportArray.count {
            noOfAbsentees = noOfAbsentees + Int(attendanceReportArray [k].noOfAbsentees)!
            totalStudents = totalStudents + Int(attendanceReportArray [k].totalStudents)!
        }
        countOfAbsentees.text = String(noOfAbsentees) + "/" + String(totalStudents)
        
        if currentDate == dateView.text{
            todayLabelWidth.constant = 41
            weekDay.text = dateView.text! + " " + date
        }
        else{
            todayLabelWidth.constant = 0
            weekDay.text = dateView.text! + " " + date
        }
        
        classAbsenteesTableView.reloadData()
        
        
    }
    
    func formatDate(_ date: Foundation.Date) -> String {
        let format = "EEEE"
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: date)
    }
    
    
}
