//
//  EmployeeHomeHeaderCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 29/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//


import UIKit

protocol searchStudentClickedProtocol:class  {
    
    func updateSearchSelectedStudent(student:composeDiaryStudentPopupList)
}

class EmployeeHomeHeaderCell: UITableViewCell,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var profileImageView: imageVCForUploadProcess!
    
    @IBOutlet var studentSearch: UISearchBar!
    @IBOutlet var studentprofileviewheight: NSLayoutConstraint!
    @IBOutlet var nameLabel: UILabel!
    var user:userModel?
    @IBOutlet var gradeAndClassLabel: UILabel!
    
    @IBOutlet var studentSearchSuggesionTable: UITableView!
    
    @IBOutlet var studentSuggesionHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolName: UILabel!
    
    weak var delegate:searchStudentClickedProtocol?

    var studentArray = [composeDiaryStudentPopupList]()
        override func awakeFromNib() {
        super.awakeFromNib()
        getUserDataFromDB()
        setupUI()
        ImageAPi.makeImageViewRound(imageView: profileImageView)
            studentSearchSuggesionTable.delegate = self
        studentSearchSuggesionTable.register(UINib(nibName: "StudentSearchSuggesion", bundle: nil), forCellReuseIdentifier: "StudentSearchSuggesion")
            
        // Initialization code
    }
    
    override func didMoveToSuperview() {
        //ImageAPi.makeImageViewRound(profileImageView)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
     
    
    func alterEmployeeCellWithProfileImageUrl(url:String) -> Void {
        nameLabel.text = user?.username
        gradeAndClassLabel.text = user?.userDesignation
        if url.isEmpty{
            profileImageView!.image = UIImage(named:"avatar.png")
        }
        else{
            weak var weakProfileImage = self.profileImageView
            ImageAPi.fetchImageForUrl(urlString: url, oneSuccess: { (image) in
                if weakProfileImage != nil {
                    weakProfileImage!.image = image }
            }) { (error) in
                
            } }
        
    }
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    private func setupUI() -> Void {
        
    
        _ = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Thin", size: 17)! // Note the !
        ]
        /*let placeholder = NSAttributedString(string: "Search Student", attributes: [NSForegroundColorAttributeName :UIColor.blackColor()])
        searchStudentText.attributedPlaceholder = placeholder
        searchStudentText.textColor = UIColor.blackColor()*/
        
        
    }
    //Mark:textfield delegate
    /*func textField(textFieldToChange: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        // limit to 4 characters
        let characterCountLimit = 4
        
        // We need to figure out how many characters would be in the string after the change happens
        let startingLength = textFieldToChange.text?.characters.count ?? 0
        let lengthToAdd = string.characters.count
        let lengthToReplace = range.length
        
        let newLength = startingLength + lengthToAdd - lengthToReplace
        
        return newLength <= characterCountLimit
    }*/
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return studentArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "StudentSearchSuggesion") as! StudentSearchSuggesion
        cell.studentName.text = studentArray[indexPath.row].studentName
        //let tap = UITapGestureRecognizer(target: self, action: #selector(HomeVC.studentClicked))
        //HomeVC.self.setStudentClicked(studentArray[indexPath.section])
        //cell.contentView.addGestureRecognizer(tap)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let std = studentArray[indexPath.row]
        //print("****")
    
        self.delegate?.updateSearchSelectedStudent(student: studentArray[indexPath.row])
        
        
        //let homevc        = HomeVC()
        //homevc.selectedStudent = studentArray
        //homevc.index = indexPath.section
        //homevc.setStudentClicked(studentArray,index: indexPath.section)
        //homevc.studentClicked()
    }
    
}
