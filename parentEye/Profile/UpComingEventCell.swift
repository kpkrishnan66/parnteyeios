//
//  UpComingEventCell.swift
//  parentEye
//
//  Created by Martin Jacob on 15/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class UpComingEventCell: UITableViewCell {

    @IBOutlet weak var bckImage:UIImageView!
    @IBOutlet weak var monthLabel:UILabel!
    @IBOutlet weak var yearLabel:UILabel!
    @IBOutlet weak var dayLabel:UILabel!
    @IBOutlet weak var contentLabel:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func didMoveToSuperview() {
        //ImageAPi.makeImageViewRound(bckImage)
    }
    
}
