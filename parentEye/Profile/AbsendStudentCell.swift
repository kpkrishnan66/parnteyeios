//
//  AbsendStudentCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 30/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class AbsendStudentCell: UITableViewCell {
    
    
    @IBOutlet var nameOfStudent: UILabel!
    
    @IBOutlet var callIcon: UIButton!
    @IBOutlet var studentProfileView: UIImageView!
    @IBOutlet var reasonLabel: UILabel!
    @IBOutlet var leaveReason: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.studentProfileView?.image = UIImage(named: "avatar")
        self.reasonLabel?.text = ""
        self.leaveReason?.text = ""
    }
}
