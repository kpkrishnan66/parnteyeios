//
//  studentPromotionPopupVC.swift
//  parentEye
//
//  Created by scientia on 15/03/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol savedStudentPromotionStatusProtocol:class {
    func savedStudentPromotionStatus()
}

class studentPromotionPopupVC: UIViewController,UITextFieldDelegate,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var studentPromotionPopupStudentTableView: UITableView!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var allPassButton: UIButton!
    @IBOutlet var passOrFailStatus: UILabel!
   
    var studentListArray = [composeDiaryStudentPopupList]()
    var searchrecipientArray = [composeDiaryStudentPopupList]()
    var temprecipientArray = [composeDiaryStudentPopupList]()
    var tempStudentList = [composeDiaryStudentPopupList]()
    var passStudentList = [composeDiaryStudentPopupList]()
    var failedStudentList = [composeDiaryStudentPopupList]()
    var notPublishedStudentList = [composeDiaryStudentPopupList]()
    let redColur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    var passedStudentsCount:Int = 0
    var failedStudentsCount:Int = 0
    var notPublishedStudentsCount:Int = 0
    var editStudent:Bool = false
    var canDismiss:Bool = false
    var user:userModel?
    weak var delegate:savedStudentPromotionStatusProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserFromDB()
        orderStudentListArray()
        setupUI()
        studentPromotionPopupStudentTableView.reloadData()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getUserFromDB () {
        user = DBController().getUserFromDB()
    }

    private func setupUI() {
        canDismiss = false
        searchrecipientArray = studentListArray
        temprecipientArray = studentListArray
        studentPromotionPopupStudentTableView.register(UINib(nibName: "studentPromotionVCCell", bundle: nil), forCellReuseIdentifier: "studentPromotionVCCell")
        studentPromotionPopupStudentTableView.estimatedRowHeight = 76.0
        studentPromotionPopupStudentTableView.rowHeight          = UITableViewAutomaticDimension
        if notPublishedStudentsCount == 0 {
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"
        }
        else{
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"+"\n"+String(notPublishedStudentsCount )+" Not published"
        }
        editStudent = false
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.enablesReturnKeyAutomatically = false
    }

    func orderStudentListArray() {
        passStudentList.removeAll()
        failedStudentList.removeAll()
        notPublishedStudentList.removeAll()
        tempStudentList.removeAll()
        tempStudentList = studentListArray

        for index in tempStudentList {
            if index.result == "Passed" {
                passStudentList.append(index)
            }
            else if index.result == "Failed" {
                failedStudentList.append(index)
            }
            else{
                notPublishedStudentList.append(index)
            }

        }
        studentListArray.removeAll()
        studentListArray = notPublishedStudentList
        for index in failedStudentList {
            studentListArray.append(index)
        }
        for index in passStudentList {
            studentListArray.append(index)
        }
        for k in 0 ..< studentListArray.count {
            studentListArray[k].isEdit = false
        }
    }

    //MARK: Table view delegate and data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if studentListArray.count == 0{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.recipientNone, stateOfMessage: .failure)
        }
        return studentListArray.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentPromotionVCCell") as! studentPromotionVCCell
        cell.selectionStyle = .none
        let student = studentListArray[indexPath.row]
        cell.studentName.text = student.studentName
        if student.result == "Passed" {
            cell.studentStatusButton.setTitle("P", for: UIControlState.normal)
            cell.studentStatusButton.setTitleColor(greenColur, for: .normal)
            cell.studentName.textColor = greenColur
        }
        else if student.result == "Failed" {
            cell.studentStatusButton.setTitle("F", for: .normal)
            cell.studentStatusButton.setTitleColor(redColur, for: .normal)
            cell.studentName.textColor = redColur
        }
        else{
            cell.studentStatusButton.setTitle("", for: .normal)
            cell.studentName.textColor = UIColor.black
        }

        weak var profileImage = cell.studentProfilePic
        if student.compressedProfilePic == ""{
            cell.studentProfilePic.image = UIImage(named: "avatar")
            cell.studentProfilePic.layer.masksToBounds = true
            cell.studentProfilePic.layer.cornerRadius = 0
            cell.studentProfilePic.clipsToBounds = false

        }
        else{
            ImageAPi.fetchImageForUrl(urlString: student.compressedProfilePic , oneSuccess: { (image) in
                if profileImage != nil {

                    cell.studentProfilePic.layer.masksToBounds = false
                    cell.studentProfilePic.layer.cornerRadius = cell.studentProfilePic.frame.height/2
                    cell.studentProfilePic.clipsToBounds = true

                    profileImage!.image = image

                }
            }) { (error) in

            }
        }

        cell.studentStatusButton?.tag = indexPath.row
        cell.studentName.tag = indexPath.row
        cell.studentProfilePic.tag = indexPath.row

        cell.studentStatusButton?.addTarget(self, action: #selector(self.changeSelection), for: .touchUpInside)


        let tap = UITapGestureRecognizer(target: self, action: #selector(self.changeSelectionByStudentName))
        cell.studentName.addGestureRecognizer(tap)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.changeSelectionByStudentProfilepic))
        cell.studentProfilePic.isUserInteractionEnabled = true
        cell.studentProfilePic.addGestureRecognizer(tapGestureRecognizer)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let button = UIButton()
        button.tag = indexPath.row
        changeindividualStatus(tagValue: button.tag)
    }

    @objc private func changeSelection(sender:UIButton){
        changeindividualStatus(tagValue: sender.tag)
    }
    @objc private func changeSelectionByStudentName(sender:UITapGestureRecognizer){
        changeindividualStatus(tagValue: (sender.view?.tag)!)
    }
    @objc private func changeSelectionByStudentProfilepic(sender:UITapGestureRecognizer){
        changeindividualStatus(tagValue: (sender.view?.tag)!)
    }


    private func changeindividualStatus(tagValue:Int) {

        let indexPath = NSIndexPath(row: tagValue, section: 0)
        let cell = studentPromotionPopupStudentTableView.cellForRow(at: indexPath as IndexPath) as! studentPromotionVCCell!
        self.editStudent = true
        studentListArray[tagValue].isEdit = true

        if studentListArray[tagValue].result == "Passed" {
            studentListArray[tagValue].result = "Failed"
            cell?.studentStatusButton.setTitle("F", for: .normal)
            cell?.studentStatusButton.setTitleColor(redColur, for: .normal)
            cell?.studentName.textColor = redColur
            passedStudentsCount = passedStudentsCount - 1
            failedStudentsCount = failedStudentsCount + 1
        }
        else if studentListArray[tagValue].result == "Failed" {
            studentListArray[tagValue].result = ""
            cell?.studentStatusButton.setTitle("", for: .normal)
            cell?.studentStatusButton.setTitleColor(UIColor.black, for: .normal)
            cell?.studentName.textColor = UIColor.black
            failedStudentsCount = failedStudentsCount - 1
            notPublishedStudentsCount = notPublishedStudentsCount + 1
        }
        else{
            studentListArray[tagValue].result = "Passed"
            cell?.studentStatusButton.setTitle("P", for: .normal)
            cell?.studentStatusButton.setTitleColor(greenColur, for: .normal)
            cell?.studentName.textColor = greenColur
            passedStudentsCount = passedStudentsCount + 1
            notPublishedStudentsCount = notPublishedStudentsCount - 1
        }
        if notPublishedStudentsCount == 0 {
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"
        }
        else{
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"+"\n"+String(notPublishedStudentsCount )+" Not published"
        }
    }


    

    @IBAction func allPassButtonAction(_ sender: UIButton) {
    self.editStudent = true
        passedStudentsCount = 0
        failedStudentsCount = 0
        notPublishedStudentsCount = 0
        if allPassButton.titleLabel?.text == "All Pass" {
            allPassButton.setTitle("None", for: UIControlState.normal)
            passedStudentsCount = studentListArray.count
            for k in 0 ..< studentListArray.count {
                studentListArray[k].result = "Passed"
                studentListArray[k].isEdit = true
            }

        }
        else{
            Analytics.logEvent("T_studentPromotion_chooseAll", parameters: nil)
            allPassButton.setTitle("All Pass", for: .normal)
            notPublishedStudentsCount = studentListArray.count
            for k in 0 ..< studentListArray.count {
                studentListArray[k].result = "None"
                studentListArray[k].isEdit = true
            }
        }

        studentPromotionPopupStudentTableView.reloadData()
        if notPublishedStudentsCount == 0 {
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"
        }
        else{
            passOrFailStatus.text = String(passedStudentsCount)+" Pass"+"\n"+String(failedStudentsCount)+" Fail"+"\n"+String(notPublishedStudentsCount )+" Not published"
        }
    }

    override func dismiss(animated flag: Bool,
                          completion: (() -> Void)?)
    {
        if canDismiss == true  {
            super.dismiss(animated: flag, completion:completion)
        }

        // Your custom code here...
    }


    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        Analytics.logEvent("T_studentPromotion_done", parameters: nil)
        self.canDismiss = true

        if editStudent == true {
            let confirmMessage = "Publishing the following results\n\n"+"\u{2022} "+String(passedStudentsCount)+" passed\t\t\n"+"\u{2022} "+String(failedStudentsCount)+" failed\t\t\t\n"+"\u{2022} "+String(notPublishedStudentsCount)+" not published\t\n\nPress Confirm to intimate school administrator for further steps."
            let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: confirmMessage, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.confirm, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
                Analytics.logEvent("T_studentPromotion_helpIcon", parameters: nil)
                self.delegate?.savedStudentPromotionStatus()
                self.dismiss(animated: false, completion: nil)
            }) { (action) in
                Analytics.logEvent("T_studentPromotion_confirm", parameters: nil)
                self.promotionStatus()

            }
            self.present(alert, animated: false, completion: nil)

        }
        else{
            self.dismiss(animated: false, completion: nil)
        }


    }


    func promotionStatus() {
        weak var weakSelf = self
        /*for k in 0 ..< studentListArray.count {
         if studentListArray[k].result == "" {
         studentListArray[k].result = "None"
         }
         }*/
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.postStudentPromotionStatus(studentListArray: self.studentListArray,
                                                 userRoleId: (user?.currentUserRoleId)!,
                                                 onSuccess: { (result) in

                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    if (result["error"] as? [String:AnyObject]) == nil{
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.promotionsucess, stateOfMessage: .success)
                                                        self.delegate?.savedStudentPromotionStatus()
                                                        // self.canDismiss = true
                                                        self.dismiss(animated: false, completion: nil)

                                                    }else{
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.promotionFailed, stateOfMessage: .failure)
                                                    }




        },
                                                 onFailure: { (error) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)

        },
                                                 duringProgress: { (progress) in

        })
    }



    //search events
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        if searchText == ""{
            studentListArray = temprecipientArray
        }else{
            let filtered = temprecipientArray.filter { $0.studentName.contains(searchText,options: .caseInsensitive) }
            studentListArray = filtered
        }
        studentPromotionPopupStudentTableView.reloadData()
    }
    
}


