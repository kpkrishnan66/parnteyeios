//
//  studentPromotion.swift
//  parentEye
//
//  Created by scientia on 14/03/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class studentPromotion:UIViewController,CommonHeaderProtocol,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,savedStudentPromotionStatusProtocol,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet var chooseStudentButtonHeight: NSLayoutConstraint!
    @IBOutlet var viewDemoButtonHeight: NSLayoutConstraint!
    @IBOutlet var chooseStudentsButton: UIButton!
    @IBOutlet var mainView: UIView!
    @IBOutlet var viewDemoButton: UIButton!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var studentPromotionListingTable: UITableView!
    @IBOutlet var classListCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet var classListCollectionView: UICollectionView!
    @IBOutlet var statusLabel: UILabel!
    
    var user:userModel? // Used tovarore the userdata.
    var profilePic  = ""
    var ClassesList:[EmployeeClassModel] = []
    var initialIndexpath = NSIndexPath()
    var ClassIsSelected = [Int: Bool]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    var defaultClassesList:[EmployeeClassModel] = []
    var studentListArray = [composeDiaryStudentPopupList]()
    var tempStudentListArray = [composeDiaryStudentPopupList]()
    var passedStudentListArray = [composeDiaryStudentPopupList]()
    var failedStudentListArray = [composeDiaryStudentPopupList]()
    var notPublishedStudentListArray = [composeDiaryStudentPopupList]()
    var passedStudentsCount:Int = 0
    var failedStudentsCount:Int = 0
    var notPublishedStudentsCount:Int = 0
    var resultPublishedStudentList = [composeDiaryStudentPopupList]()
    var classId:Int = 0
    var className = ""
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    let redColur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    let grayColor = UIColor(red: 158.0/255.0, green: 158.0/255.0, blue: 158.0/255.0, alpha: 1.0)
    var mobileNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setupUI()
        alterHeader()
        initializeTableView()
        makeWebApiCallToGetstudentPromotionList()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    private func alterHeader() {
        if ClassesList.count == 1 {
            header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentPromotion+" - "+ClassesList[0].className, withProfileImageUrl: profilePic)
            classListCollectionViewHeight.constant = 0
        }
        else{
            header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentPromotion+" - "+className, withProfileImageUrl: profilePic)
            classListCollectionViewHeight.constant = 44
        }
        header.delegate = self
    }
    
    private func initializeTableView(){
        studentPromotionListingTable.register(UINib(nibName: "studentPromotionCell", bundle: nil), forCellReuseIdentifier: "studentPromotionCell")
    }
    
    private func setupUI() {
        var pos:Int = 0
        ClassesList.removeAll()
        for k in 0 ..< (user?.Classes)!.count {
            if user?.Classes![k].classInChargeEmployeeId == user!.employeeId {
                ClassesList.append((user?.Classes![k])!)
                ClassIsSelected[pos] = false
                pos = pos + 1
            }
        }
        if ClassesList.count > 0 {
            classId = ClassesList[0].classId
            className = ClassesList[0].className
            ClassIsSelected[0] = true
            defaultClassesList.removeAll()
            defaultClassesList.append(ClassesList[0])
        }
        statusLabel.text = ""
        chooseStudentButtonHeight.constant = 0
        viewDemoButtonHeight.constant = 0
        
    }
    
    @IBAction func viewdemoAction(_ sender: Any) {
        Analytics.logEvent("T_studentPromotion_helpIcon", parameters: nil)
        showDemoAlert()
    }
    
    private func showDemoAlert() {
        let message = "\n"+"\u{2022} "+"Tap once to mark as Passed\t\n"+"\u{2022} "+"Tap twice to mark as Failed\t\n"+"\u{2022} "+"Tap again to deselect\t\t\t\n\n"+"NB:Choosing students will not inform the parents. Once you submit the result, a message will be sent to the school administrator, to communicate the result with the parents."
        
        let alrt =  AlertController.getSingleOptionAlertControllerWithMessage(mesasage: message, titleOfAlert: "How to choose students ?", oktitle: ConstantsInUI.gotit, OkButtonBlock: nil)
        self.present(alrt, animated: true, completion: nil)
    }
    
    
    
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
    }
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func makeWebApiCallToGetstudentPromotionList() {
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getAllStudentListForPromotion(schoolClassId: String(classId),onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let PasswordResponse = result["error"] as? NSDictionary{
                if let errorMessage = PasswordResponse["errorMessage"] as? String{
                    AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                }}
            else{
                self.studentListArray.removeAll()
                weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
                self.seperateResultPublishedStudentList()
                if self.studentListArray.count > 0 {
                    if self.studentListArray[0].addedByAdmin == true {
                        self.statusLabel.text = "To make changes to the promotion list , please contact school administrator"
                        self.statusLabel.textColor = self.grayColor
                        self.chooseStudentsButton.isHidden = true
                        self.viewDemoButton.isHidden = true
                        self.chooseStudentButtonHeight.constant = 0
                        self.viewDemoButtonHeight.constant = 0
                    }
                        
                    else if self.resultPublishedStudentList.count == self.notPublishedStudentListArray.count {
                        self.showDemoAlert()
                        self.resultPublishedStudentList.removeAll()
                        self.statusLabel.text = " No Students promoted yet"
                        self.statusLabel.textColor = self.redColur
                        self.chooseStudentsButton.isHidden = false
                        self.viewDemoButton.isHidden = false
                        self.chooseStudentButtonHeight.constant = 30
                        self.viewDemoButtonHeight.constant = 30
                        
                    }
                    else{
                        self.statusLabel.text = "To publish the result to parents contact school administrator"
                        self.statusLabel.textColor = self.grayColor
                        self.chooseStudentsButton.isHidden = false
                        self.viewDemoButton.isHidden = false
                        self.chooseStudentButtonHeight.constant = 30
                        self.viewDemoButtonHeight.constant = 30
                        
                    }
                }
                else{
                    self.statusLabel.text = "Sorry.No Students to list"
                    self.statusLabel.textColor = self.redColur
                    self.chooseStudentsButton.isHidden = true
                    self.viewDemoButton.isHidden = true
                    self.chooseStudentButtonHeight.constant = 0
                    self.viewDemoButtonHeight.constant = 0
                    
                    
                }
                self.studentPromotionListingTable.reloadData()
            }
            
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        
    }
    
    private func seperateResultPublishedStudentList () {
        self.resultPublishedStudentList.removeAll()
        self.passedStudentListArray.removeAll()
        self.failedStudentListArray.removeAll()
        self.notPublishedStudentListArray.removeAll()
        passedStudentsCount = 0
        failedStudentsCount = 0
        notPublishedStudentsCount = 0
        for index in studentListArray {
            if index.result == "Passed" {
                passedStudentListArray.append(index)
                passedStudentsCount = passedStudentsCount + 1
            }
            else if index.result == "Failed" {
                failedStudentListArray.append(index)
                failedStudentsCount = failedStudentsCount + 1
            }
            else{
                notPublishedStudentListArray.append(index)
                notPublishedStudentsCount = notPublishedStudentsCount + 1
            }
            
        }
        resultPublishedStudentList = notPublishedStudentListArray
        for index in failedStudentListArray {
            resultPublishedStudentList.append(index)
        }
        for index in passedStudentListArray {
            resultPublishedStudentList.append(index)
        }
        
    }
    
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        initialIndexpath  = NSIndexPath(row: 0, section: 0)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ClassListCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return resultPublishedStudentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "studentPromotionCell") as! studentPromotionCell
        cell.selectionStyle = .none
        cell.studentName.text = resultPublishedStudentList[indexPath.row].studentName
        
        
        if resultPublishedStudentList[indexPath.row].result == "Passed" {
            cell.studentPromotionStatus.text = "Passed"
            cell.studentPromotionStatus.textColor = greenColur
        }
        else if resultPublishedStudentList[indexPath.row].result == "Failed"{
            cell.studentPromotionStatus.text = "Failed"
            cell.studentPromotionStatus.textColor = redColur
        }
        else{
            cell.studentPromotionStatus.text = "Not published"
            cell.studentPromotionStatus.textColor = grayColor
        }
        
        weak var profileImage = cell.studentProfilePic
        if resultPublishedStudentList[indexPath.row].compressedProfilePic == ""{
            cell.studentProfilePic.image = UIImage(named: "avatar")
            cell.studentProfilePic.layer.masksToBounds = true
            cell.studentProfilePic.layer.cornerRadius = 0
            cell.studentProfilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: resultPublishedStudentList[indexPath.row].compressedProfilePic , oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.studentProfilePic.layer.masksToBounds = false
                    cell.studentProfilePic.layer.cornerRadius = cell.studentProfilePic.frame.height/2
                    cell.studentProfilePic.clipsToBounds = true
                    
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        cell.callIcon.tag = indexPath.row
        cell.callIcon.addTarget(self, action: #selector(self.callIconTapped), for: UIControlEvents.touchUpInside)
        return cell
        
        
    }
    
    
    
    
    @objc func callIconTapped(sender:UIButton){
        
        for k in 0 ..< resultPublishedStudentList.count {
            if k == sender.tag{
                mobileNumber = resultPublishedStudentList[k].mobileNo
            }
        }
        
        showAlert()
    }
    // this is added by jithin on 28-03-2018 for making call.
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        className = ClassesList[sender.tag].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        alterHeader()
        classListCollectionView.reloadData()
        //studentRecordArray.removeAll()
        //studentPromotionListingTable.reloadData()
        makeWebApiCallToGetstudentPromotionList()
        
    }
    
    @IBAction func showStudentListPopUp(_ sender: UIButton) {
        //  Analytics.logEvent(withName: "T_promotion_chooseStudents", parameters: nil)
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "studentPromotionPopupVC") as! studentPromotionPopupVC ;
        msRecipVC.studentListArray = studentListArray
        msRecipVC.passedStudentsCount = passedStudentsCount
        msRecipVC.failedStudentsCount = failedStudentsCount
        msRecipVC.notPublishedStudentsCount = notPublishedStudentsCount
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        self.present(msRecipVC, animated: false, completion: nil)
    }
    
    func savedStudentPromotionStatus() {
        self.makeWebApiCallToGetstudentPromotionList()
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    
}

