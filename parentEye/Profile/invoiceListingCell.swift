//
//  invoiceListingCell.swift
//  parentEye
//
//  Created by scientia on 25/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class invoiceListingCell:UITableViewCell {
   
    
    @IBOutlet var invoiceName: UILabel!
    @IBOutlet var invoiceStudentName: UILabel!
    @IBOutlet var infoStatusImageButton: UIButton!
    @IBOutlet var invoiceDate: UILabel!
    @IBOutlet var paymentIdOrPendingInfo: UILabel!
    @IBOutlet var invoiceAmount: UILabel!
    @IBOutlet var invoiceSelectionImageButton: UIButton!
    @IBOutlet var paymentDate: UILabel!
    @IBOutlet var paymentIdAndDateEmbedView: UIView!
    
       var selection:Bool = false
}
