//
//  ChooseAbsenteesStudentListCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 31/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ChooseAbsenteesStudentListCell: UITableViewCell {
    
    var isTeacherSelected = false
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    
    @IBOutlet var studentName: UILabel!
    
    @IBOutlet var selectionButton: UIButton!
    @IBOutlet var profilePic: UIImageView!
    
    @IBOutlet var alphabetLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //function to set the value externally
    func setTeacherSelected(selected:Bool){
        isTeacherSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
            studentName.textColor = colur
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
            studentName.textColor = UIColor.black
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isTeacherSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
            studentName.textColor = colur
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
            studentName.textColor = UIColor.black
        }
        
        isTeacherSelected = !isTeacherSelected
    }
    
    
}


