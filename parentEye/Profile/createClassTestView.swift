//
//  createClassTestView.swift
//  parentEye
//
//  Created by scientia on 24/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class createClassTestView:UIView,UITextViewDelegate {
    
    @IBOutlet var classTestHeader: UILabel!
    @IBOutlet var selectSubjectOrTitleTextField: customTextfield!
    @IBOutlet var maxMarkTextField: UITextField!
    @IBOutlet var dateofClassTestTexfield: UITextField!
    @IBOutlet var classTestTopicsTextview: UITextView!
    @IBOutlet var dateSelectionButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var createClassTestButton: UIButton!
    @IBOutlet var subjectOrTitleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        classTestTopicsTextview.delegate = self
        classTestTopicsTextview.text = "Enter the topics..."
        classTestTopicsTextview.textColor = UIColor.lightGray
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if classTestTopicsTextview.textColor == UIColor.lightGray {
            classTestTopicsTextview.text = ""
            classTestTopicsTextview.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if classTestTopicsTextview.text.isEmpty {
            classTestTopicsTextview.text = "Enter the topics..."
            classTestTopicsTextview.textColor = UIColor.lightGray
        }
    }
}
