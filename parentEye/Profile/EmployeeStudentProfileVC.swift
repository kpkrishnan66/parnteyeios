//
//  EmployeeStudentProfileVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 02/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class EmployeeStudentProfileVC: UIViewController,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,EmployeeDetailedStudentprofileVCProtocol,SelectedSchoolpopOverDelegate{
    
    
    
    @IBOutlet var searchStudent: UITextField!
    @IBOutlet var studentNameLabel: UILabel!
    @IBOutlet var classStudentStrengthLabel: UILabel!
    @IBOutlet var EmployeeStudentProfileTableView: UITableView!
    @IBOutlet var studentinGradeLabel: UILabel!
    @IBOutlet var EmployeeStudentProfileCollectionView: UICollectionView!
    @IBOutlet var header: CommonHeader!
    
    
    var user:userModel?
    var profilePic = ""
    var ClassesList:[EmployeeClassModel] = []
    var ClassIsSelected = [Int: Bool]()
    var defaultClassesList:[EmployeeClassModel] = []
    var classId:Int = 0
    let reuseIdentifier = "EmployeeStudentProfileCollectionViewCell"
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var studentListArray = [composeDiaryStudentPopupList]()
    var tempStudentListArray = [composeDiaryStudentPopupList]()
    var searchStudentListArray = [composeDiaryStudentPopupList]()
    var studentId = 0
    var schoolId:Int = 0
    var search: Bool = false
    var gotoDetailed = false
    var mobileNumber = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setUpUI()
        initializeTableView()
        makeWebApiCallToGetEmployeeStudentProfile()
    }
    override func viewDidAppear(_ animated: Bool) {
        setUpClassList()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if gotoDetailed == false{
        _ = self.navigationController?.popViewController(animated: false)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentProfileScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        header.multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popViewController(animated: false)
        }
        
    }
    
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    private func setUpUI(){
        searchStudent.layer.borderWidth = CGFloat(1)
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        searchStudent.layer.borderColor = color.cgColor
        let placeholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.foregroundColor : UIColor.black])
        searchStudent.attributedPlaceholder = placeholder
        
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        defaultClassesList.removeAll()
        
        defaultClassesList.append(ClassesList[0])
        //classStudentStrengthLabel.text = "Total Strength: " + String(studentListArray.count)
        if(ClassesList[0].classInCharge != ""){
            studentNameLabel.text = "Class in Charge: " + ClassesList[0].classInCharge
        }else{
            studentNameLabel.text = "Class in Charge: Not Assigned"
        }
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.getStudentCurrentlySelectedSchool()?.schoolId)!
        }
        
        searchStudent.addTarget(self, action: #selector(EmployeeStudentProfileVC.textFieldDidChange), for: UIControlEvents.editingChanged)
    }
    
    private func setUpClassList(){
        var rowvalue = 0
        
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
            if  ClassesList[k].classId == classId{
                rowvalue = k
                ClassIsSelected[rowvalue] = true
                
            }
            
        }
        classId = ClassesList[rowvalue].classId

        
        defaultClassesList.removeAll()
        
        defaultClassesList.append(ClassesList[rowvalue])
        
        EmployeeStudentProfileCollectionView.reloadData()
        if ClassesList.count > 1
        {
            
            EmployeeStudentProfileCollectionView.scrollToItem(at: NSIndexPath(row: rowvalue, section: 0) as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
            
            if(ClassesList[rowvalue].classInCharge != ""){
                studentNameLabel.text = "Class in Charge: " + ClassesList[rowvalue].classInCharge
            }else{
                studentNameLabel.text = "Class in Charge: Not Assigned"
                
            }
            
        }
       makeWebApiCallToGetEmployeeStudentProfile()

    }
    
    private func clearSearchTextField(){
        searchStudent.layer.borderWidth = CGFloat(1)
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        searchStudent.layer.borderColor = color.cgColor
        searchStudent.text = ""
        let placeholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.foregroundColor : UIColor.black])
        searchStudent.attributedPlaceholder = placeholder
    }
    
    
    private func initializeTableView(){
        EmployeeStudentProfileTableView.register(UINib(nibName: "EmployeeStudentProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "EmployeeStudentProfileTableViewCell")
        EmployeeStudentProfileTableView.estimatedRowHeight = 129.0
        EmployeeStudentProfileTableView.rowHeight          = UITableViewAutomaticDimension
    }
    
    //MARK:- Collectionview delegate and data source
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! EmployeeStudentProfileCollectionViewCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    // Mark:- TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return studentListArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "EmployeeStudentProfileTableViewCell") as! EmployeeStudentProfileTableViewCell
        let record = studentListArray[indexPath.row]
        cell.nameofStudent.text = record.studentName
        
       /* if studentListArray.count > 0{
            if studentListArray[indexPath.row].isAbsent == true {
              cell.nameofStudent.textColor = colur
            }
            else{
              cell.nameofStudent.textColor = UIColor.blackColor()
            }
        }*/
        weak var profileImage = cell.studentProfilepic
        if record.profilePic == ""{
            cell.studentProfilepic.image = UIImage(named: "avatar")
            cell.studentProfilepic.layer.masksToBounds = true
            cell.studentProfilepic.layer.cornerRadius = 0
            cell.studentProfilepic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: record.profilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.studentProfilepic.layer.masksToBounds = false
                    cell.studentProfilepic.layer.cornerRadius = cell.studentProfilepic.frame.height/2
                    cell.studentProfilepic.clipsToBounds = true
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        cell.studentCallIcon.tag = indexPath.row
        cell.studentCallIcon.addTarget(self, action: #selector(EmployeeStudentProfileVC.callIconTapped), for: UIControlEvents.touchUpInside)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let std = studentListArray[indexPath.row]
        
        self.performSegue(withIdentifier: segueConstants.segueForLoadingEmployeeStudentProfileDetails, sender: std)
    }
    
    
    @objc func callIconTapped(sender:UIButton){
        
        
        for k in 0 ..< studentListArray.count {
            if k == sender.tag{
                mobileNumber = studentListArray[k].mobileNo
                
            }
        }
       showAlert()
    }
    //this is added by jithin on 27-03-2018 for making call
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        if(ClassesList[sender.tag].classInCharge != ""){
            studentNameLabel.text = "Class in Charge: " + ClassesList[sender.tag].classInCharge
        }
        else{
            studentNameLabel.text = "Class in Charge: Not Assigned"
        }
        
        classStudentStrengthLabel.text = "Total Strength: " + String(studentListArray.count)
        tempStudentListArray.removeAll()
        searchStudentListArray.removeAll()
        search = false
        clearSearchTextField()
        EmployeeStudentProfileCollectionView.reloadData()
        makeWebApiCallToGetEmployeeStudentProfile()
        
    }
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    private func makeWebApiCallToGetEmployeeStudentProfile(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getEmployeeSelectedClassStudentList(classId: classId ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            weakSelf!.studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
            self.tempStudentListArray.removeAll()
            self.searchStudentListArray.removeAll()
            self.tempStudentListArray = self.studentListArray
            self.searchStudentListArray = self.studentListArray
            self.classStudentStrengthLabel.text = "Total Strength: " + String(self.studentListArray.count)
            self.EmployeeStudentProfileTableView.reloadData()
            
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    @IBAction func searchpopupAction(sender: UITextField) {
        
        
        
    }
    @objc func textFieldDidChange(textField: UITextField) {
        searchRecipientListWithText(searchText: textField.text!)
    }
    private func searchRecipientListWithText(searchText:String){
        self.studentListArray.removeAll()
        if searchText == ""{
            search = false
            self.studentListArray = tempStudentListArray
            self.EmployeeStudentProfileTableView.reloadData()
            return
        }
        
        let find = (self.searchStudentListArray).filter(){$0.studentName.contains(searchText,options: .caseInsensitive) }

        self.studentListArray =  find
        searchStudentListArray = tempStudentListArray
        search = true
        self.EmployeeStudentProfileTableView.reloadData()
        
    }
    
    //MARK:- Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueForLoadingEmployeeStudentProfileDetails{
            let empDetailed                  = segue.destination as! EmployeeDetailedStudentprofileVC
            empDetailed.delegate = self
            let std                               = sender as! composeDiaryStudentPopupList
            empDetailed.classId              = classId
            empDetailed.studentId            = std.studentId
            empDetailed.studentName          = std.studentName
            gotoDetailed = true
        }
    }
    
    func selectedClassid(classId classid:Int){
       classId = classid
        
    }
}
