//
//  EmployeeHomeCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 12/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class EmployeeHomeCell: UITableViewCell {
    
    
    
    @IBOutlet var takeAttendance: UIButton!
    @IBOutlet var timeTable: UIButton!
    @IBOutlet var studentProfile: UIButton!
    @IBOutlet var attendanceView: UIView!
    @IBOutlet var attendanceIcon: UIImageView!
    @IBOutlet var timetableIcon: UIImageView!
    @IBOutlet var studentProfileIcon: UIImageView!
    @IBOutlet var timetableView: UIView!
    @IBOutlet var studentProfileView: UIView!
    @IBOutlet var promotionView: UIView!
    @IBOutlet var promotion: UIButton!
    @IBOutlet var promotionIcon: UIImageView!
    @IBOutlet var shuffleIcon: UIImageView!
    @IBOutlet var shuffle: UIButton!
    @IBOutlet var shuffleView: UIView!
    @IBOutlet var classTestView: UIView!
    @IBOutlet var classTestButton: UIButton!
    @IBOutlet var classTestIcon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        takeAttendance.titleLabel!.numberOfLines = 0
        takeAttendance.titleLabel!.lineBreakMode = NSLineBreakMode.byTruncatingTail
        studentProfile.titleLabel!.numberOfLines = 0
        studentProfile.titleLabel!.lineBreakMode = NSLineBreakMode.byTruncatingTail


        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func didMoveToSuperview() {
        //ImageAPi.makeImageViewRound(bckImage)
    }
    
    
   
}
