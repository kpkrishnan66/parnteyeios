//
//  FeePaymentDetailsVC.swift
//  parentEye
//
//  Created by scientia on 25/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class FeePaymentDetailsVC : UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,popOverDelegate {
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var invoiceTableView: UITableView!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var subView: UIView!
    @IBOutlet var cashLabel: UILabel!
    
    var user:userModel?
    var profilePic = ""
    var invoiceListArray = [invoiceList]()
    var dueInvoiceListArray = [invoiceList]()
    var paidInvoiceListArray = [invoiceList]()
    var invoiceDetailsVCArray = [invoiceList]()
    var paidDetailsVCArray = [invoiceList]()
    var selectedDueInvoiceListArray = [invoiceList]()
    let sectionArray = ["Due","Paid"]
    let redColur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    let lightGreenColur = UIColor(red: 197/255.0, green: 225.0/255.0, blue: 165.0/255.0, alpha: 1.0)
    var totalAmount:Float = 0
    var totalPaidAmount:Float = 0
    var schoolId:Int = 0
    var schoolName:String = ""
    var schoolAdress:String = ""
    var selectedInvoice:String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Added to adjust the TABLE HEADER
        self.tabBarController?.tabBar.isHidden = true
        setupTable()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        getinvoiceList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    fileprivate func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.feePayment, withProfileImageUrl: profilePic)
        header.downArrowButton.isHidden = true
        header.profileChangeButton.isHidden = true
        header.delegate = self
    }
    
    fileprivate func setupTable(){
        invoiceTableView.register(UINib(nibName: "invoiceListingCell", bundle: nil), forCellReuseIdentifier: "invoiceListingCell")
        invoiceTableView.estimatedRowHeight = 133.0
        //invoiceTableView.allowsMultipleSelection = true
        //invoiceTableView.rowHeight          = UITableViewAutomaticDimension
    }
    
    fileprivate func setupUI() {
        subView.layer.borderWidth = 1
        subView.layer.borderColor = UIColor.lightGray.cgColor
        nextButton.backgroundColor = lightGreenColur
        cashLabel.text = "0.0"
    }
    
    
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton: UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.maketabBarRoot()
    }
    
    fileprivate func getinvoiceList() {
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getinvoiceList(Students: (user?.Students)!,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: self)
            print(result)
            weakSelf!.invoiceListArray.removeAll()
            weakSelf?.dueInvoiceListArray.removeAll()
            weakSelf?.paidInvoiceListArray.removeAll()
            weakSelf?.selectedDueInvoiceListArray.removeAll()
            weakSelf?.paidDetailsVCArray.removeAll()
            weakSelf!.invoiceListArray = parseInvoiceListArray.parseThisDictionaryForInvoiceRecords(dictionary: result)
            if self.invoiceListArray.count == 0 {
                self.showFailedAlert()
            }
            else{
                self.createDueAndInvoiceListArray()
                print(self.invoiceListArray)
                weakSelf!.invoiceTableView.reloadData()
            }
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.showFailedAlert()
            
        }) { (progress) in
            
        }
    }
    
    func createDueAndInvoiceListArray() {
        
        var tempInvoiceListArray = [invoiceList]()
        tempInvoiceListArray.removeAll()
        
        
        for invoice in invoiceListArray {
            if invoice.status != "Paid" {
                if invoice.status == "InProgress" {
                    dueInvoiceListArray.append(invoice)
                }
                else{
                    tempInvoiceListArray.append(invoice)
                }
            }
            else{
                paidInvoiceListArray.append(invoice)
            }
        }
        // inprogress duelist must be on top.
        for invoice in tempInvoiceListArray {
            dueInvoiceListArray.append(invoice)
        }
        
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if dueInvoiceListArray.count == 0 && paidInvoiceListArray.count == 0 {
            return 0
        }
        
        if dueInvoiceListArray.count == 0 || paidInvoiceListArray.count == 0 {
            return 1
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if dueInvoiceListArray.count == 0 {
            return sectionArray[1]
        }
        else if paidInvoiceListArray.count == 0 {
            return sectionArray[0]
        }
        return sectionArray[section]
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 && dueInvoiceListArray.count > 0{
            return dueInvoiceListArray.count
        }
        else{
            return paidInvoiceListArray.count
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "invoiceListingCell") as! invoiceListingCell
        
        cell.selectionStyle = .none
        cell.invoiceSelectionImageButton.isHidden = true
        
        if indexPath.row < dueInvoiceListArray.count {
            cell.isSelected = dueInvoiceListArray[indexPath.row].selected
        }
        else{
            cell.isSelected = false
        }
        
        if cell.isSelected == true && indexPath.section == 0 {
            cell.backgroundColor = lightGreenColur
            cell.invoiceSelectionImageButton.isHidden = false
        }
        else{
            cell.backgroundColor = UIColor.clear
            cell.invoiceSelectionImageButton.isHidden = true
        }
        
        if indexPath.section == 0 && dueInvoiceListArray.count > 0 {
            if indexPath.row < dueInvoiceListArray.count {
                cell.invoiceName.text = dueInvoiceListArray[indexPath.row].name
                cell.invoiceDate.text = dueInvoiceListArray[indexPath.row].dueDate
                cell.invoiceAmount.text = dueInvoiceListArray[indexPath.row].amount
                cell.invoiceStudentName.text = dueInvoiceListArray[indexPath.row].studentName
                cell.invoiceDate.textColor = redColur
                cell.invoiceDate.font = UIFont(name: "Roboto-Regular", size: 13)
                cell.infoStatusImageButton.setImage(UIImage(named: "calendarRed"), for: UIControlState())
                
                if dueInvoiceListArray[indexPath.row].status == "Late" || dueInvoiceListArray[indexPath.row].status == "Due"{
                    cell.paymentIdOrPendingInfo.isHidden = true
                    cell.paymentDate.isHidden = true
                    cell.paymentIdAndDateEmbedView.isHidden = true
                    
                }
                else if dueInvoiceListArray[indexPath.row].status == "InProgress" {
                    cell.paymentIdOrPendingInfo.isHidden = false
                    cell.paymentIdAndDateEmbedView.isHidden = false
                    cell.paymentIdOrPendingInfo.backgroundColor = lightGreenColur
                    cell.paymentIdAndDateEmbedView.backgroundColor = lightGreenColur
                    cell.paymentIdOrPendingInfo.text = ConstantsInUI.paymentProgress
                }
                else{
                }
                
            }
        }
        else{
            if indexPath.row < paidInvoiceListArray.count {
                cell.invoiceName.text = paidInvoiceListArray[indexPath.row].name
                cell.invoiceDate.text = paidInvoiceListArray[indexPath.row].dueDate
                cell.invoiceAmount.text = paidInvoiceListArray[indexPath.row].amount
                cell.invoiceStudentName.text = paidInvoiceListArray[indexPath.row].studentName
                cell.invoiceDate.textColor = UIColor.black
                cell.paymentIdOrPendingInfo.isHidden = false
                cell.paymentDate.isHidden = false
                cell.paymentIdAndDateEmbedView.isHidden = false
                cell.paymentIdAndDateEmbedView.backgroundColor = lightGreenColur
                cell.paymentIdOrPendingInfo.backgroundColor = lightGreenColur
                cell.paymentDate.backgroundColor = lightGreenColur
                if paidInvoiceListArray[indexPath.row].status == "Paid" {
                    cell.invoiceDate.font = UIFont(name: "Roboto-Thin", size: 13)
                    cell.infoStatusImageButton.setImage(UIImage(named: "calendar"), for: UIControlState())
                    cell.paymentIdOrPendingInfo.text = "Payment ID: "+paidInvoiceListArray[indexPath.row].paymentId
                    cell.paymentDate.text = paidInvoiceListArray[indexPath.row].paidOn
                }
                
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("selected row = \(indexPath.row),section = \(indexPath.section)")
        
        if indexPath.section == 0 && dueInvoiceListArray.count > 0{
            if indexPath.row < dueInvoiceListArray.count {
                if dueInvoiceListArray[indexPath.row].status != "InProgress" {
                    let indexPath = tableView.indexPathForSelectedRow!
                    let cell = tableView.cellForRow(at: indexPath)! as! invoiceListingCell
                    
                    if dueInvoiceListArray[indexPath.row].selected == false && (schoolId == 0 || dueInvoiceListArray[indexPath.row].schoolId == schoolId) {
                        cell.invoiceSelectionImageButton.isHidden = false
                        nextButton.backgroundColor = greenColur
                        if dueInvoiceListArray[indexPath.row].amount != "" {
                            totalAmount = totalAmount + Float(dueInvoiceListArray[indexPath.row].amount)!
                        }
                        if totalAmount != 0 {
                            cashLabel.text = String(totalAmount)
                        }
                        schoolId = dueInvoiceListArray[indexPath.row].schoolId
                        schoolName = dueInvoiceListArray[indexPath.row].schoolName
                        schoolAdress = dueInvoiceListArray[indexPath.row].schoolAddress
                        selectedDueInvoiceListArray.append(dueInvoiceListArray[indexPath.row])
                        
                        // Reload tableview to manage scroll selection
                        dueInvoiceListArray[indexPath.row].selected = true
                        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                        
                    }
                    else if dueInvoiceListArray[indexPath.row].selected == true && dueInvoiceListArray[indexPath.row].schoolId == schoolId{
                        cell.invoiceSelectionImageButton.isHidden = true
                        cell.backgroundColor = UIColor.clear
                        if dueInvoiceListArray[indexPath.row].amount != "" {
                            totalAmount = totalAmount - Float(dueInvoiceListArray[indexPath.row].amount)!
                        }
                        if totalAmount != 0 {
                            cashLabel.text = String(totalAmount)
                        }
                        else{
                            schoolId = 0
                            schoolName = ""
                            schoolAdress = ""
                            nextButton.backgroundColor = lightGreenColur
                            cashLabel.text = String(totalAmount)
                        }
                        
                        //Remove the selected due list
                        let id = dueInvoiceListArray[indexPath.row].id
                        for pos in 0 ..< selectedDueInvoiceListArray.count {
                            if pos < selectedDueInvoiceListArray.count {
                                if selectedDueInvoiceListArray[pos].id == id {
                                    selectedDueInvoiceListArray.remove(at: pos)
                                }
                            }
                        }
                        dueInvoiceListArray[indexPath.row].selected = false
                        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                    }
                }
            }
        }
        else{
            self.selectedInvoice = "paid"
            if indexPath.row < paidInvoiceListArray.count {
                schoolId = paidInvoiceListArray[indexPath.row].schoolId
                schoolName = paidInvoiceListArray[indexPath.row].schoolName
                schoolAdress = paidInvoiceListArray[indexPath.row].schoolAddress
                self.totalPaidAmount = Float(paidInvoiceListArray[indexPath.row].amount)!
                self.paidDetailsVCArray.removeAll()
                self.paidDetailsVCArray.append(paidInvoiceListArray[indexPath.row])
            }
            self.performSegue(withIdentifier: segueConstants.segueToInvoiceDetailsPage, sender: nil)
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            if indexPath.row < dueInvoiceListArray.count {
                if dueInvoiceListArray[indexPath.row].status == "Late" || dueInvoiceListArray[indexPath.row].status == "Due"{
                    return 90.0
                }
                else{
                    return 133.0
                }
            }
        }
        else {
            return 133.0
        }
        return 133.0
    }
    
    func showFailedAlert() {
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.failedInvoiceDetails, titleOfAlert: "Sorry!!", oktitle: ConstantsInUI.tryagain, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            _ = self.navigationController?.popViewController(animated: true)
        }) { (action) in
            self.getinvoiceList()
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @IBAction func showInvoiceDetails(_ sender: UIButton) {
        
    }
    // inprogress dis msg,late red mark
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueToInvoiceDetailsPage{
            let invoiceDetails  = segue.destination as! invoiceDetailsVC
            invoiceDetails.schoolNameValue = schoolName
            invoiceDetails.schoolAddressValue = schoolAdress
            invoiceDetails.selectedInvoice = selectedInvoice
            if selectedInvoice == "due" {
                invoiceDetails.totalAmountValue = totalAmount
                invoiceDetails.invoiceDetailsVCArray = invoiceDetailsVCArray
                invoiceDetails.responseString = ""
                invoiceDetails.fromFeePaymentDetails = true
            }
            else{
                invoiceDetails.totalAmountValue = totalPaidAmount
                invoiceDetails.invoiceDetailsVCArray = paidDetailsVCArray
                invoiceDetails.responseString = ConstantsInUI.atomSuccessResponse
                invoiceDetails.fromFeePaymentDetails = true
            }
        }
    }
    
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        if schoolId != 0 {
            Analytics.logEvent("P_feePaymentHome_nextBtn", parameters: nil)
            checkInvoice()
        }
    }
    
    fileprivate func checkInvoice() {
        var dueDateListArray = [String]()
        var feeNameListArray = [String]()
        var studentIdListArray = [String]()
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        for k in 0 ..< selectedDueInvoiceListArray.count {
            dueDateListArray.append(selectedDueInvoiceListArray[k].dueDate)
            feeNameListArray.append(selectedDueInvoiceListArray[k].name)
            if !studentIdListArray.contains(String(selectedDueInvoiceListArray[k].studentId)) {
                studentIdListArray.append(String(selectedDueInvoiceListArray[k].studentId))
            }
        }
        
        WebServiceApi.checkInvoiceList(studentIdListArray: studentIdListArray,dueDateListArray: dueDateListArray,feeNameListArray: feeNameListArray,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: self)
            print(result)
            if let checkInvoice = result["checkInvoice"] as? NSDictionary{
                if let canProceed = checkInvoice["canProceed"] as? Bool {
                    if canProceed == true {
                        self.invoiceDetailsVCArray.removeAll()
                        for pos in self.dueInvoiceListArray {
                            if pos.selected == true {
                                self.invoiceDetailsVCArray.append(pos)
                            }
                        }
                        self.selectedInvoice = "due"
                        self.performSegue(withIdentifier: segueConstants.segueToInvoiceDetailsPage, sender: nil)
                        
                    }
                    else{
                        if let message = checkInvoice["message"] as? String {
                            AlertController.showToastAlertWithMessage(message: message, stateOfMessage: .failure)
                        }
                        
                    }
                }
            }
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }) { (progress) in
            
        }
    }
    
}
