//
//  EmployeeDetailedStudentprofileVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 03/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol EmployeeDetailedStudentprofileVCProtocol:class  {
    
    func selectedClassid(classId:Int)
}


class EmployeeDetailedStudentprofileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,SelectedSchoolpopOverDelegate {
    
    @IBOutlet var searchIconHeight: NSLayoutConstraint!
    @IBOutlet var mainViewHeight: NSLayoutConstraint!
    @IBOutlet var searchStudentHeight: NSLayoutConstraint!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var searchStudent: UITextField!
    @IBOutlet var EmployeeDetailedStudentProfileCollectionview: UICollectionView!
    @IBOutlet var tblViewHeightLayout: NSLayoutConstraint!
    @IBOutlet var subSelectionSgmntCtrl: UISegmentedControl!
    @IBOutlet var EmployeeStudentDetailedListingTable: UITableView!
    @IBOutlet var transportView: UIView!
    @IBOutlet var transportButton: UIButton!
    @IBOutlet var transportImageView: UIImageView!
    @IBOutlet var resultView: UIView!
    @IBOutlet var resultImageView: UIImageView!
    @IBOutlet var resultButton: UIButton!
    @IBOutlet var centerView: UIView!
    @IBOutlet var leaveListTableView: UITableView!
    @IBOutlet var totalAbsentDays: UILabel!
    @IBOutlet var leavelblNoLeave: UILabel!
    
    
    var user:userModel?
    var numberOfRow = Int()
    var selected:Bool = false
    var profilePic = ""
    var ClassesList:[EmployeeClassModel] = []
    var ClassIsSelected = [Int: Bool]()
    var defaultClassesList:[EmployeeClassModel] = []
    var classId:Int = 0
    var className:String = ""
    var studentId:Int = 0
    var schoolId:Int = 0
    var studentName = ""
    let reuseIdentifier = "EmployeeDetailedStudentProfileCollectionViewCell"
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var studentDetailedListArray = [EmployeeDetailedStudentProfile]()
    var tempStudentDetailedListArray = [EmployeeDetailedStudentProfile]()
    var leaveListArray           = [EmployeeDetailedStudentProfile]()
    weak var delegate:EmployeeDetailedStudentprofileVCProtocol?
    var leaveVC = false
    var isFromHomeSearchVC:Bool = false
    let viewControllerIdentifiers = ["Result", "Leave"]
    var gotoDetailed = false
    var busArrivalStatus:String?
    var rowvalue = 0
    var mobileNumber = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        getUserDataFromDB()
        alterHeader()
        setupUI()
        makeWebApiCallToGetEmployeeStudentDetailedProfile()
    }
    override func viewDidLayoutSubviews() {
       self.EmployeeDetailedStudentProfileCollectionview.reloadData()
       self.EmployeeDetailedStudentProfileCollectionview.scrollToItem(at: IndexPath(item: rowvalue,section: 0), at: UICollectionViewScrollPosition.left, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   /* override func viewDidDisappear(animated: Bool) {
        //if gotoDetailed == false{
            self.navigationController?.popToRootViewControllerAnimated(false)
        //}
    }*/
    
    
    
    
    private func setupTable(){
        EmployeeStudentDetailedListingTable.register(UINib(nibName: "EmployeeStudentProfileCell", bundle: nil), forCellReuseIdentifier: "EmployeeStudentProfileCell")
        EmployeeStudentDetailedListingTable.register(UINib(nibName: "studentProfileDetailCell", bundle: nil), forCellReuseIdentifier: "studentProfileDetailCell")
        EmployeeStudentDetailedListingTable.dataSource = self
        EmployeeStudentDetailedListingTable.delegate = self
        
        EmployeeStudentDetailedListingTable.estimatedRowHeight = 75.0
        EmployeeStudentDetailedListingTable.rowHeight = UITableViewAutomaticDimension
        numberOfRow = 1
        EmployeeStudentDetailedListingTable.allowsSelection = false
        leaveListTableView.register(UINib(nibName: "leaveStatusView", bundle: nil), forCellReuseIdentifier: "leaveStatusView")
        let nib = UINib(nibName: "LeaveHeaderView", bundle: nil)
        leaveListTableView.register(nib, forHeaderFooterViewReuseIdentifier: "LeaveHeaderView")
        EmployeeStudentDetailedListingTable.register(nib, forHeaderFooterViewReuseIdentifier: "LeaveHeaderView")
        leaveListTableView.allowsSelection = false
    }
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        EmployeeStudentDetailedListingTable.reloadData()
        
    }
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentProfileScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    
    private func setupUI(){
      
        
    searchStudentHeight.constant = 0
    searchIconHeight.constant = 0
    mainViewHeight.constant = mainViewHeight.constant - 35
    searchStudent.layer.borderWidth = CGFloat(1)
    let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    searchStudent.layer.borderColor = color.cgColor
        let placeholder = NSAttributedString(string: "Search Students", attributes: [NSAttributedStringKey.foregroundColor :UIColor.black])
    searchStudent.attributedPlaceholder = placeholder
        
    ClassesList = (user?.Classes)!
    if !isFromHomeSearchVC {
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
            if  ClassesList[k].classId == classId{
                 rowvalue = k
                 ClassIsSelected[rowvalue] = true
            }
        }
        
    }
    else{
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
            if ClassesList[k].className == className{
                rowvalue = k
                ClassIsSelected[rowvalue] = true
            }
        }
    }
    classId = ClassesList[rowvalue].classId
    defaultClassesList.removeAll()
    defaultClassesList.append(ClassesList[rowvalue])
        
    //self.EmployeeDetailedStudentProfileCollectionview.reloadData()
//        if ClassesList.count > 1
//        {
        print("\(rowvalue)")
            //EmployeeDetailedStudentProfileCollectionview.scrollToItem(at: NSIndexPath(row: rowvalue, section: 0) as IndexPath, at: UICollectionViewScrollPosition.left, animated: true)
        //EmployeeDetailedStudentProfileCollectionview.scrollToItem(at: NSIndexPath(row: rowvalue, section: 0) as IndexPath, at: UICollectionViewScrollPosition.left , animated: true)
        //self.EmployeeDetailedStudentProfileCollectionview.scrollToItem(at: IndexPath(item: rowvalue,section: 0), at: UICollectionViewScrollPosition.left , animated: true)
        //}
    
     if user?.currentTypeOfUser == .CorporateManager {
        schoolId = (user?.getStudentCurrentlySelectedSchool()?.schoolId)!
     }
        //centerView.isHidden = true
        transportView.layer.cornerRadius = 10;
        transportView.layer.masksToBounds = true;
        transportView.layer.borderWidth = 1
        transportView.layer.borderColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0).cgColor
        resultView.layer.cornerRadius = 10;
        resultView.layer.masksToBounds = true;
        resultView.layer.borderWidth = 1
        resultView.layer.borderColor = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0).cgColor
        
        let transportImagetap = UITapGestureRecognizer(target: self, action: #selector(self.employeeTransportButtonTapped))
        let transportViewtap = UITapGestureRecognizer(target: self, action:#selector(self.employeeTransportButtonTapped))
        let resultImagetap = UITapGestureRecognizer(target: self, action: #selector(self.employeeProgresscardButtonTapped))
        let resultViewtap = UITapGestureRecognizer(target: self, action:#selector(self.employeeProgresscardButtonTapped))
        transportView.addGestureRecognizer(transportViewtap)
        transportImageView.addGestureRecognizer(transportImagetap)
        resultView.addGestureRecognizer(resultViewtap)
        resultImageView.addGestureRecognizer(resultImagetap)
        transportButton.addTarget(self, action: #selector(self.employeeTransportButtonTapped), for: .touchUpInside)
        resultButton.addTarget(self, action: #selector(self.employeeProgresscardButtonTapped), for: .touchUpInside)
        leavelblNoLeave.isHidden = true
        
   }

    
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        header.multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    //MARK:- POP over delegates

   // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
    return UIModalPresentationStyle.none
    }
    
    //MARK:- Collectionview delegate and data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return ClassesList.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! EmployeeDetailedStudentProfileCollectionViewCell
        if ClassIsSelected[indexPath.row] == false{
            cell.selectedClass.backgroundColor = UIColor.clear
            cell.selectedClass.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 1
            cell.selectedClass.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.selectedClass.backgroundColor = colur
            cell.selectedClass.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.selectedClass.layer.borderWidth = 0
        }
        cell.selectedClass.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.selectedClass.layer.cornerRadius = 18
        cell.selectedClass.layer.borderWidth = 1
        cell.selectedClass.layer.borderColor = UIColor.black.cgColor
        cell.selectedClass.tag = indexPath.row
        cell.selectedClass.addTarget(self, action: #selector(self.selectedclassAction), for: .touchUpInside)
        return cell
    }
    
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         if section == 0{
             if tableView == self.EmployeeStudentDetailedListingTable {
               return 2
             }
             else{
                return 0
            }
         }
         else{
            if leaveListArray.count > 0 {
                if tableView == self.leaveListTableView {
                    return leaveListArray.count
                }
                else{
                    return 0
                }
            }
            else{
                return 0
            }
         }
    }
    
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()

        if indexPath.section == 0 {
            if tableView == self.EmployeeStudentDetailedListingTable {
                var cellToDisplay = UITableViewCell()
                if indexPath.row == 0{
                    let viewmorecell =  tableView.dequeueReusableCell(withIdentifier: "EmployeeStudentProfileCell") as! EmployeeStudentProfileCell
                    if studentDetailedListArray.count > 0{
                    viewmorecell.alterCellWithProfileImageUrl(studentDetailedListArray[indexPath.row].profilePic)

                        viewmorecell.studentName.text  = studentDetailedListArray[indexPath.row].studentName
                        viewmorecell.studentGrade.text  = studentDetailedListArray[indexPath.row].className
                        viewmorecell.studentTeacherName.text  = studentDetailedListArray[indexPath.row].classTeacher
                        viewmorecell.guardianName.text
                            = "    "+studentDetailedListArray[0].parentName
                        viewmorecell.admnNo.text = studentDetailedListArray[0].AdmnNo
                        viewmorecell.adress.text = studentDetailedListArray[0].address
                        viewmorecell.dateOfBirth.text = studentDetailedListArray[0].dob
                        viewmorecell.mobNo.text = studentDetailedListArray[0].mobileNo
                        //this is added by jithin on 26-03-2018 for making a call from the student profile.
                        mobileNumber = studentDetailedListArray[0].mobileNo
                    }
                    
                    //this is added by jithin on 24-03-2018 for making a call from the student profile
                    viewmorecell.callButton.addTarget(self, action: #selector(self.callIconTapped(_:)), for: UIControlEvents.touchUpInside)
                    
                    viewmorecell.backgroundColor = UIColor.clear
                    viewmorecell.previousButton.addTarget(self, action: #selector(self.loadPreviousStudent(_:)), for: .touchUpInside)
                    viewmorecell.nextButton.addTarget(self, action: #selector(self.loadNextStudent(_:)), for: .touchUpInside)

                    cellToDisplay = viewmorecell
                }
                if indexPath.row == 1{
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "studentProfileDetailCell") as! studentProfileDetailCell
                    cell.backgroundColor = UIColor.clear
                    cell.hide.addTarget(self, action: #selector(self.visibilityHide(_:)), for: .touchUpInside)

                    cellToDisplay = cell
                }

                return cellToDisplay
            }
        }
        else{
            if indexPath.section == 1 {
                if leaveListArray.count > 0 {
                 if tableView == self.leaveListTableView {
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "leaveStatusView") as! leaveStatusView
                    cell.dateLabel.text = leaveListArray[indexPath.row].date
                    cell.statusLabel.text = leaveListArray[indexPath.row].reason
                    return cell
                 }
                }
            }
        }
        return cell
    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.section == 0 {
           return 233 //UITableViewAutomaticDimension
        }
        else{
           return 70
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        // Dequeue with the reuse identifier
        
          return Bundle.main.loadNibNamed("LeaveHeaderView", owner: nil, options: nil)![0] as? UIView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 1 {
            if leaveListArray.count == 0 {
                return CGFloat(0.0)
            }
          return CGFloat(45.0)
        }
        else{
          return CGFloat(0.0)
        }
        
    }
    
    // this is added by jithin on 24-03-2018 for making a call from student profile
    
    @objc func callIconTapped(_ sender:UIButton){
        print("INSIDE callIconTapped")
        showAlert()
    }
    
    fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

    
    func visibilityShow(_ sender:UIButton)
    {
        selected = true
        numberOfRow = 2
        EmployeeStudentDetailedListingTable.reloadData()
        
    }
    @objc func loadNextStudent(_ sender:UIButton)
    {
        makeWebApiCallToGetEmployeeNextStudentDetailedProfile()
    }
    
    @objc func loadPreviousStudent(_ sender:UIButton)
    {
        makeWebApiCallToGetEmployeePreviousStudentDetailedProfile()
    }
    
    
    @objc func visibilityHide(_ sender:UIButton)
    {
        selected = false
        numberOfRow = 1
        EmployeeStudentDetailedListingTable.reloadData()
    }
    
    @objc private func selectedclassAction(sender:UIButton){
        classId = ClassesList[sender.tag].classId
        self.delegate?.selectedClassid(classId: classId)
        _ = self.navigationController?.popViewController(animated: true)
    }
    private func makeWebApiCallToGetEmployeeStudentDetailedProfile(){
        weak var weakSelf = self
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getEmployeeSelectedClassDetailedStudentProfileList(studentId: studentId ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.studentDetailedListArray.removeAll()
            weakSelf!.studentDetailedListArray = parseEmployeeDetailedStudentProfileList.parseThisDictionaryForDetailedStudentProfileList(dictionary: result)
            
            self.leaveListArray.removeAll()
            weakSelf!.leaveListArray = parseEmployeeDetailedStudentProfileLeaveList.parseThisDictionaryForDetailedStudentProfileLeaveList(dictionary: result)
            
            let userDefaults = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.leaveListArray)
            userDefaults.set(encodedData, forKey: "leaves")
            userDefaults.synchronize()
            self.setTotalAbsentDaysCount()
            if self.leaveListArray.count == 0{
                self.leavelblNoLeave.text = "There are no leave records found"
                self.leavelblNoLeave.isHidden = false
            }
            else{
                self.leavelblNoLeave.text = ""
                self.leavelblNoLeave.isHidden = true
            }
            self.EmployeeStudentDetailedListingTable.reloadData()
            self.leaveListTableView.reloadData()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
  
    private func makeWebApiCallToGetEmployeeNextStudentDetailedProfile(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getEmployeeSelectedClassDetailedNextStudentProfileList(classId: classId,studentName: studentName,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.tempStudentDetailedListArray = self.studentDetailedListArray
            weakSelf!.studentDetailedListArray = parseEmployeeDetailedStudentProfileList.parseThisDictionaryForDetailedStudentProfileList(dictionary: result)
            if self.studentDetailedListArray.count == 0{
                self.studentDetailedListArray = self.tempStudentDetailedListArray
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.noMoreStudents, stateOfMessage: .failure)
            }
                
            else{
            self.leaveListArray.removeAll()
            weakSelf!.leaveListArray = parseEmployeeDetailedStudentProfileLeaveList.parseThisDictionaryForDetailedStudentProfileLeaveList(dictionary: result)
            
            let userDefaults = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.leaveListArray)
            userDefaults.set(encodedData, forKey: "leaves")
            userDefaults.synchronize()
            
            if self.studentDetailedListArray.count > 0{
            let defaults = UserDefaults.standard
            defaults.set(self.studentDetailedListArray[0].studentId, forKey: "selectedStudentId")
            }
            
            
            self.setTotalAbsentDaysCount()
            
            self.studentName = self.studentDetailedListArray[0].studentName
            self.studentId = self.studentDetailedListArray[0].studentId
                if self.leaveListArray.count == 0{
                    self.leavelblNoLeave.text = "There are no leave records found"
                    self.leavelblNoLeave.isHidden = false
                }
                else{
                    self.leavelblNoLeave.text = ""
                    self.leavelblNoLeave.isHidden = true
                }
                self.EmployeeStudentDetailedListingTable.reloadData()
                self.leaveListTableView.reloadData()
               
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    private func makeWebApiCallToGetEmployeePreviousStudentDetailedProfile(){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getEmployeeSelectedClassDetailedPreviousStudentProfileList(classId: classId,studentName: studentName,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            self.tempStudentDetailedListArray = self.studentDetailedListArray
            weakSelf!.studentDetailedListArray = parseEmployeeDetailedStudentProfileList.parseThisDictionaryForDetailedStudentProfileList(dictionary: result)
            if self.studentDetailedListArray.count == 0{
                self.studentDetailedListArray = self.tempStudentDetailedListArray
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.noMoreStudents, stateOfMessage: .failure)
            }
                
            else{
            self.leaveListArray.removeAll()
            weakSelf!.leaveListArray = parseEmployeeDetailedStudentProfileLeaveList.parseThisDictionaryForDetailedStudentProfileLeaveList(dictionary: result)
            
            let userDefaults = UserDefaults.standard
            let encodedData = NSKeyedArchiver.archivedData(withRootObject: self.leaveListArray)
            userDefaults.set(encodedData, forKey: "leaves")
            userDefaults.synchronize()
            self.setTotalAbsentDaysCount()
            
            if self.studentDetailedListArray.count > 0{
            let defaults = UserDefaults.standard
            defaults.set(self.studentDetailedListArray[0].studentId, forKey: "selectedStudentId")
            }
            
            
            
                self.studentName = self.studentDetailedListArray[0].studentName
                self.studentId = self.studentDetailedListArray[0].studentId
                
                if self.leaveListArray.count == 0{
                    self.leavelblNoLeave.text = "There are no leave records found"
                    self.leavelblNoLeave.isHidden = false
                }
                else{
                    self.leavelblNoLeave.text = ""
                    self.leavelblNoLeave.isHidden = true
                }
                self.EmployeeStudentDetailedListingTable.reloadData()
                self.leaveListTableView.reloadData()

            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    func setTotalAbsentDaysCount () {
        if leaveListArray.count == 0 {
            self.totalAbsentDays.text = "0"
        }
        else{
            var noofabsentdays = 0
            for index in 0 ..< leaveListArray.count {
                let status = leaveListArray[index].leaveStatus
                if status == "Not Submitted" || status == "Submitted and Absent" {
                    noofabsentdays = noofabsentdays + 1
                }
            }
            self.totalAbsentDays.text = String(noofabsentdays)
            
        }
    }
}

    extension EmployeeDetailedStudentprofileVC{
        
        

        override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {

            
            if segue.identifier == "employeeTransportPageSegue"{
                
                let svc = segue.destination as? TransportVC
                 svc?.busArrivalStatus = busArrivalStatus
                 svc?.studentDetailedListArray = studentDetailedListArray
                 svc?.studentId = String(studentId)
                
            }
            
            if segue.identifier  == segueConstants.segueToProgresscardFromStudentProfile{
                let pcvc = segue.destination as? progresscardVC
                pcvc?.studentId = String(studentId)
                if studentDetailedListArray.count > 0{
                    pcvc?.showOnlyProgressCard = studentDetailedListArray[0].showOnlyProgressCard
                }

            }
            
        }
        
        
        
         @objc func employeeTransportButtonTapped(_sender: AnyObject) {
           if studentDetailedListArray.count > 0{
              if studentDetailedListArray[0].transportType == "SchoolBus"{
                 gettingBusStatus()
              }else{
                segueTransition()
                }
            }else{
                segueTransition()
            }
        }
        
        @objc func segueTransition(){
            self.performSegue(withIdentifier: "employeeTransportPageSegue", sender: nil)
        }
        
         @objc func employeeProgresscardButtonTapped(_sender: AnyObject) {
            Analytics.logEvent("T_ProgressCard", parameters: nil)
            performSegue(withIdentifier: segueConstants.segueToProgresscardFromStudentProfile, sender: nil)
        }
}
        extension EmployeeDetailedStudentprofileVC{
        
        func gettingBusStatus(){
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            WebServiceApi.getBusTime(correspondingTobusID: String(studentDetailedListArray[0].busId), onSuccess: {(result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if let busDelay = result["busDelay"]{
                    weakSelf!.busArrivalStatus = busDelay["delayReport"] as? String
                }
                debugPrint("bus",result)
                self.segueTransition()
                }, onFailure: {(error) in
                    
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                    self.segueTransition()
                }, duringProgress: {(progress) in
                    
            })
        }


}


