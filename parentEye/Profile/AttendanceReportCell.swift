//
//  AttendanceReportCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 09/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class AttendanceReportCell: UITableViewCell {
    
    
    @IBOutlet var className: UILabel!
    
    @IBOutlet var teacherName: UILabel!
    
    @IBOutlet var attendance: UILabel!

    @IBOutlet var attendanceInfoButton: UIImageView!
}