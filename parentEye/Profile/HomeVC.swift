//
//  HomeVC.swift
//  parentEye
//
//  Created by Martin Jacob on 15/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,popOverDelegate,imageUpLoaderProtocol,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,SelectedSchoolpopOverDelegate,UISearchBarDelegate,searchStudentClickedProtocol,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var listingTable:UITableView!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    
    
    var user:userModel? // Used tovarore the userdata.
    lazy var eventArray = [BaseModelListing]()
    var passStudentId = ""
    var initailLoad = true
    var isClassTeacher:Bool = false
    var classList : [classSubjectList]!
    var Classes:[EmployeeClassModel]?
    var employeeHeaderCell = EmployeeHomeHeaderCell()
    var selectedStudent = [composeDiaryStudentPopupList]()
    var index:Int = 0
    var newImage = UIImage(named: "ApplicationLogo")
    var homeResultView = HomeResultButtonView()
    let greencolur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Added to adjust the TABLE HEADER
        
        /* let tabBarControllerItems = self.tabBarController?.tabBar.items
         
         if let arrayOfTabBarItems = tabBarControllerItems as! AnyObject as? NSArray{
         
         let tabBarItemONE = arrayOfTabBarItems[3] as! UITabBarItem
         tabBarItemONE.enabled = false
         } */
        
        
        self.automaticallyAdjustsScrollViewInsets = true
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        getUserDataFromDB()
        setupTable()
        
//        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//            AnalyticsParameterItemID: "id-\(String(describing: user?.username))" as NSObject,
//            AnalyticsParameterItemName: (user?.username)! as NSObject,
//            AnalyticsParameterContentTypeAnalyticsParameterContentType: "cont" as NSObject
//            ])
        
        // Do any additional setup after loading the view.
        
       
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        let studentProfileResult = shouldGotoStudentProfileScreen()
        let takeAttendanceResult = shouldGotoTakeAttendanceScreen()
        if studentProfileResult{
            performSegue(withIdentifier: "studentProfileSegue", sender: nil)
        }else if takeAttendanceResult.0{
            performSegue(withIdentifier: segueConstants.EmployeeApplyLeaveSegue, sender: takeAttendanceResult.1)
        }
        else {
        getUserDataFromDB()
        initailLoad = false
        //sync.checkForSync()
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            employeeHeaderCell.studentSearch.text = nil
            employeeHeaderCell.studentSuggesionHeight.constant = 0
	     }
        if user?.currentTypeOfUser == .CorporateManager {
            header.multiSwitchButton.setTitle(user!.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
            listingTable.reloadData()
            getClassListForCorporateManager()
        }
        initailLoad = false
        setMeUp()
        if(user?.currentTypeOfUser == .guardian){
            getUpcomingEventsFromWeb()
        }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func shouldGotoStudentProfileScreen() ->Bool{
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadLeaveRecord
        tabBar.shouldLoadLeaveRecord = false
        return result
    }
    
    private func shouldGotoTakeAttendanceScreen() ->(Bool,StudentAbsentRecord?){
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadStudentLeaveSubmit
        tabBar.shouldLoadStudentLeaveSubmit = false
        return (result,tabBar.StudentLeaveSubmitToBeLoaded)
    }
   /* func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        let cell = listingTable.cellForRowAtIndexPath(indexPath) as! profileHeaderCell!
        if touch.view == cell.studentProfileView || touch.view == cell.feePaymentView  {
            
            return true
            
        }
        return false
    }*/
    
    /**
     Function to setup the UI
     */
    func setMeUp() -> Void {
        header.delegate = self
        header.view.backgroundColor = UIColor.clear
        header.leftButton.setImage(UIImage(named: "hamburgerMenuImage"), for: .normal)
    }
    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        isClassTeacher = false
        if user?.currentTypeOfUser == .employee {
         for index in (user?.Classes)! {
            if index.classInChargeEmployeeId == user?.employeeId {
               isClassTeacher = true
            }
         }
        }
    }
    //MARK: -Table view Methods
    
    private func setupTable(){
        listingTable.register(UINib(nibName: "UpComingEventCell", bundle: nil), forCellReuseIdentifier: "UpComingEventCell")
        listingTable.register(UINib(nibName: "profileHeaderCell", bundle: nil), forCellReuseIdentifier: "profileHeaderCell")
        listingTable.register(UINib(nibName: "EmployeeHomeCellCopy", bundle: nil), forCellReuseIdentifier: "EmployeeHomeCell")
        listingTable.register(UINib(nibName: "EmployeeHomeHeaderCell", bundle: nil), forCellReuseIdentifier: "EmployeeHomeHeaderCell")
        
        
        
        listingTable.estimatedRowHeight = 10.0
        //listingTable.rowHeight = UITableViewAutomaticDimension
        if user?.currentTypeOfUser == .employee{
            listingTable.allowsSelection = false
        }
        
        //listingTable.tableHeaderView = NSBundle.mainBundle().loadNibNamed("ProfileViewForHomeTable", owner: self, options: nil)[0] as? UIView
    }
    
    private func getClassListForCorporateManager() {
            var schoolId:Int = 0
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            schoolId = user!.schools![(user?.currentSchoolSelected)!].schoolId
            WebServiceApi.getClassListOfCorporateManager(schoolId: String(schoolId),onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if let classSubjectListArray = result["classSubjectList"] as? [[String:AnyObject]]{
                    self.classList = [classSubjectList]()
                    for dic in classSubjectListArray{
                        let value = classSubjectList(fromDictionary: dic as NSDictionary)
                        self.classList.append(value)
                    }
                }
                if let employeeclassList = self.classList as [classSubjectList]?{
                    self.Classes?.removeAll()
                    for classListOfEmployee in employeeclassList{
                        if let cls =  EmployeeClassModel.init(fromClassList: classListOfEmployee) {
                            if self.Classes == nil{
                                self.Classes = [EmployeeClassModel]()
                            }
                            self.Classes?.append(cls)
                        }
                    }
                }
                self.user?.Classes?.removeAll()
                self.user?.Classes = self.Classes
                DBController().addUserToDB(self.user!)
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }) { (progress) in
                
            }
    
    }
    
    private func alterProfileCell(cell:profileHeaderCell){
        
        header.screenShowingLabel.isHidden = true
        if let stdnts = user?.Students{
            
            let student = stdnts[(user?.currentOptionSelected)!]
            
            cell.alterCellWithProfileImageUrl(url: student.profilePic, nameOfStudent: student.name, gradeOfStudent: student.className, classOfStudent: student.classTeacher,initailLoad: initailLoad)
            header.setUpHeaderForRootScreensWithNameOfScreen(name: ConstantsInUI.switchToMe, withProfileImageUrl: student.profilePic)
            cell.profileImageView.delegate = self
        }
        
    }
    
    private func alterProfileCellForEmployee(cell:EmployeeHomeHeaderCell){
        
        cell.alterEmployeeCellWithProfileImageUrl(url: (user?.profilePic)!)
        cell.profileImageView.delegate = self
        if user!.switchAs == ""{
            if user?.currentTypeOfUser == .CorporateManager {
                header.setupHeaderForCorporateManager(name: (user?.getStudentCurrentlySelectedSchool()?.schoolName)!)
            }
            else{
                header.setUPHeaderForEmployee(name: "")
            }
        }
        else{
            if user!.switchAs == "Guardian"{
                if user?.currentTypeOfUser == .CorporateManager {
                }
                else{
                header.setUPHeaderForMultiRole(name: "")
                }
            
            }
            
        }
        
    }
    
    @objc private func studentProfileClicked() {
        FireBaseKeys.FirebaseKeysRuleThree(value: " P_homePage_profile")
        performSegue(withIdentifier: "studentProfileSegue", sender: nil)
       
    }
    
    @objc private func takeAttendanceButtonTapped()
    {
        FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_takeattendance")
        performSegue(withIdentifier: segueConstants.EmployeeApplyLeaveSegue, sender: nil)
        
        
    }
    @objc private func takeEmployeeStudentProfileButtonTapped()
    {
        FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_studentprofile")
        performSegue(withIdentifier: segueConstants.showEmployeeStudentProfileSegue, sender: nil)
        
        
    }
    
    @objc private func attendanceReportButtonTapped()
    {
        FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_attendanceReport")
        performSegue(withIdentifier: segueConstants.attendanceReportSegue , sender: nil)
        
        
    }
    
    @objc private func takeEmployeeTimetableButtonTapped()
    {
        FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_timeTable")
        performSegue(withIdentifier: segueConstants.segueToTimeTable, sender: nil)
        
        
    }
    
    @objc private func sudentPromotionButtonTapped() {
        Analytics.logEvent("T_homePage_studentpromotion", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToSudentPromotion, sender: nil)
 
    }
    
    @objc private func studentShuffleButtonTapped() {
        Analytics.logEvent("T_homePage_studentshuffle", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToStudentShuffle, sender: nil)
   }
    
    
    @objc private func feePaymentButtonTapped()
    {
        if (user?.Students![(user?.currentOptionSelected)!].feePayment)! == "SHOW" {
            Analytics.logEvent("P_homePage_payFeeLayout", parameters: nil)
            print("freepressed")
            performSegue(withIdentifier: segueConstants.segueToFeePaymentDetailsPage, sender: nil)
        }
        else if (user?.Students![(user?.currentOptionSelected)!].feePayment)! == "DISABLED" {
            showAlert()
        }
        else{
            
        }
    }
    
    
    @objc private func employeeClassTestButtonTapped() {
        Analytics.logEvent("T_homePage_studentshuffle", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToClassTestListingVC, sender: nil)
    }
    
    @objc private func guardianClasstestButtonTapped() {
        Analytics.logEvent("P_ClassTestList", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToClassTestListingVC, sender: nil)
        
    }
    
    @objc private func guardianProgresscardButtonTapped() {
        Analytics.logEvent("P_ProgressCard", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToProgresscard, sender: nil)
    }
    
    @objc private func guardianNutshellButtonTapped() {
        Analytics.logEvent("P_Nutshell", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToNutshell, sender: nil)
    }
    
    @objc private func guardianGraphButtonTapped() {
        Analytics.logEvent("P_Graph", parameters: nil)
        performSegue(withIdentifier: segueConstants.segueToGraph, sender: nil)
    }
    
    @objc private func studentResultButtonTapped() {
            Analytics.logEvent("T_homePage_StudentResult", parameters: nil)
            let array = Bundle.main.loadNibNamed("HomeResultButtonView", owner:self, options:nil)

            homeResultView = array![0] as! HomeResultButtonView
            homeResultView.center = CGPoint(x:self.view.frame.size.width  / 2,
                                        y:self.view.frame.size.height / 2);
            homeResultView.frame = self.view.frame
            let indexPath = NSIndexPath(row: 0, section: 0)
            _ = self.listingTable.cellForRow(at: indexPath as IndexPath)! as! profileHeaderCell


        homeResultView.horizontalView.isHidden = true
        homeResultView.verticalView.isHidden = true

        if user?.Students![(user?.currentOptionSelected)!].showOnlyProgressCard == 0 {
            homeResultView.graphImageView.isHidden = false
            homeResultView.graphLabel.isHidden = false
            homeResultView.nutshellLabel.isHidden = false
            homeResultView.nutshellImageView.isHidden = false
        }
        else{
            homeResultView.graphImageView.isHidden = true
            homeResultView.graphLabel.isHidden = true
            homeResultView.nutshellLabel.isHidden = true
            homeResultView.nutshellImageView.isHidden = true

        }

        homeResultView.progresscardImageView.backgroundColor = greencolur
        homeResultView.progresscardLabel.textColor = UIColor.white
        homeResultView.progresscardImageView.layer.masksToBounds = false
        homeResultView.progresscardImageView.layer.cornerRadius = homeResultView.progresscardImageView.frame.height/2
        homeResultView.progresscardImageView.clipsToBounds = true
        homeResultView.progresscardImageView.contentMode = .center
        let progressCardImageViewtap = UITapGestureRecognizer(target: self,action: #selector(self.guardianProgresscardButtonTapped))
        let progressCardLabelTap = UITapGestureRecognizer(target: self,action: #selector(self.guardianProgresscardButtonTapped))
        homeResultView.progresscardImageView.addGestureRecognizer(progressCardImageViewtap)
        homeResultView.progresscardLabel.addGestureRecognizer(progressCardLabelTap)

        homeResultView.classTestImageView.backgroundColor = greencolur
        homeResultView.classTestLabel.textColor = UIColor.white
        homeResultView.classTestImageView.layer.masksToBounds = false
        homeResultView.classTestImageView.layer.cornerRadius = homeResultView.classTestImageView.frame.height/2
        homeResultView.classTestImageView.clipsToBounds = true
        homeResultView.classTestImageView.contentMode = .center
        let classTestImageViewtap = UITapGestureRecognizer(target: self,action: #selector(self.guardianClasstestButtonTapped))
        let classTestLabelTap = UITapGestureRecognizer(target: self,action: #selector(self.guardianClasstestButtonTapped))
        homeResultView.classTestImageView.addGestureRecognizer(classTestImageViewtap)
        homeResultView.classTestLabel.addGestureRecognizer(classTestLabelTap)

        homeResultView.graphImageView.backgroundColor = greencolur
        homeResultView.graphLabel.textColor = UIColor.white
        homeResultView.graphImageView.layer.masksToBounds = false
        homeResultView.graphImageView.layer.cornerRadius = homeResultView.graphImageView.frame.height/2
        homeResultView.graphImageView.clipsToBounds = true
        homeResultView.graphImageView.contentMode = .center

        homeResultView.nutshellImageView.backgroundColor = greencolur
        homeResultView.nutshellLabel.textColor = UIColor.white
        homeResultView.nutshellImageView.layer.masksToBounds = false
        homeResultView.nutshellImageView.layer.cornerRadius = homeResultView.nutshellImageView.frame.height/2
        homeResultView.nutshellImageView.clipsToBounds = true
        homeResultView.nutshellImageView.contentMode = .center
        let nutshellImageViewtap = UITapGestureRecognizer(target: self,action: #selector(self.guardianNutshellButtonTapped))
        let nutshellLabelTap = UITapGestureRecognizer(target: self,action: #selector(self.guardianNutshellButtonTapped))
        homeResultView.nutshellImageView.addGestureRecognizer(nutshellImageViewtap)
        homeResultView.nutshellLabel.addGestureRecognizer(nutshellLabelTap)

        let graphImageViewtap = UITapGestureRecognizer(target: self,action: #selector(self.guardianGraphButtonTapped))
        let graphLabelTap = UITapGestureRecognizer(target: self,action: #selector(self.guardianGraphButtonTapped))
        homeResultView.graphImageView.addGestureRecognizer(graphImageViewtap)
        homeResultView.graphLabel.addGestureRecognizer(graphLabelTap)




        self.homeResultView.alpha = 0.0
        self.view.addSubview(self.homeResultView)

        UIView.animate(withDuration: 0.5, animations: {

            self.homeResultView.alpha = 1.0

        })
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch? = touches.first
        //location is relative to the current view
        // do something with the touched point
        if touch?.view == homeResultView {
            //homeResultView.isHidden = true
            UIView.animate(withDuration: 0.5, animations: {
                self.homeResultView.alpha = 0.0
            })
        }
    }
    
    
    func showAlert() {
        let alrt =  AlertController.getSingleOptionAlertControllerWithMessage(mesasage: ConstantsInUI.feeDisabledMessage, titleOfAlert: ConstantsInUI.featurenotEabled, oktitle: ConstantsInUI.ok, OkButtonBlock: nil)
        self.present(alrt, animated: false, completion: nil)
    }

    
    func updateSearchSelectedStudent(student:composeDiaryStudentPopupList){
        performSegue(withIdentifier: segueConstants.segueToSearchStudent, sender: student)
    }
    
    
    @objc internal func studentClicked() {
        
        print(String(self.selectedStudent.count)+"####"+String(self.index))
        
        performSegue(withIdentifier: segueConstants.segueToSearchStudent, sender: self.selectedStudent[self.index])
        
    }
    
    @objc internal func setStudentClicked(studentList:[composeDiaryStudentPopupList], index:Int) {
        self.selectedStudent = studentList
        self.index = index
        studentClicked()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueToSearchStudent{
            let empDetailed                  = segue.destination as! EmployeeDetailedStudentprofileVC
            //empDetailed.delegate = self
            let std                               = sender as! composeDiaryStudentPopupList
            
            empDetailed.classId              = 383
            empDetailed.className            = std.className
            empDetailed.studentId            = std.studentId
            empDetailed.studentName          = std.studentName
            empDetailed.isFromHomeSearchVC = true
            //gotoDetailed = true
        }
        if segue.identifier == segueConstants.EmployeeApplyLeaveSegue {
            let employeeApplyLeavevc = segue.destination as! EmployeeApplyLeave
            if sender != nil{
                let sar                = sender as! StudentAbsentRecord
                employeeApplyLeavevc.isleaveSubmissionPushNotification = true
                employeeApplyLeavevc.dateOfAbsent = sar.date
                employeeApplyLeavevc.classId = sar.schoolClassId
            }
        }
    }
    
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //if user?.currentTypeOfUser == .employee{
        //   return 1
        //}
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user?.currentTypeOfUser == .guardian{
            
            if section == 0 {
                return 1
            }
            
        }
        else{
            if section == 0 {
                return 1
            }
            
            if section == 1 {
                return 1
            }
        }
        return eventArray.count
        
    }
    
     func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell  {
        
        if indexPath.section == 0 {
            if user?.currentTypeOfUser == .guardian{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "profileHeaderCell") as! profileHeaderCell
                cell.backgroundColor = UIColor.clear
                alterProfileCell(cell: cell)
                cell.selectionStyle = .none
                
               cell.studentProfileView.layer.cornerRadius = 10;
               cell.studentProfileView.layer.masksToBounds = true;
               cell.studentProfileView.layer.borderWidth = 1
               cell.studentProfileView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
               cell.studentProfileButton.addTarget(self, action: #selector(self.studentProfileClicked), for: .touchUpInside)
               cell.studentProfileButton.isUserInteractionEnabled = true
               let studentImagetap = UITapGestureRecognizer(target: self, action: #selector(self.studentProfileClicked))
               let studentViewtap = UITapGestureRecognizer(target: self, action:#selector(self.studentProfileClicked))
               cell.studentProfileView.addGestureRecognizer(studentViewtap)
               cell.studentImage.addGestureRecognizer(studentImagetap)
                
                if user?.Students![(user?.currentOptionSelected)!].feePayment == "SHOW"  || user?.Students![(user?.currentOptionSelected)!].feePayment == "DISABLED" {
                    cell.feePaymentView.isHidden = false
                    cell.feePaymentImage.isHidden = false
                    cell.feePaymentButton.isHidden = false
                }
                else{
                    cell.feePaymentView.isHidden = true
                    cell.feePaymentImage.isHidden = true
                    cell.feePaymentButton.isHidden = true
                }
                
                cell.feePaymentView.layer.cornerRadius = 10;
                cell.feePaymentView.layer.masksToBounds = true;
                cell.feePaymentView.layer.borderWidth = 1
                cell.feePaymentView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                let feeImageTap = UITapGestureRecognizer(target: self, action: #selector(self.feePaymentButtonTapped))
                let feeViewTap = UITapGestureRecognizer(target: self, action: #selector(self.feePaymentButtonTapped))
                cell.feePaymentView.addGestureRecognizer(feeViewTap)
                cell.feePaymentImage.addGestureRecognizer(feeImageTap)
                cell.feePaymentButton.addTarget(self, action: #selector(self.feePaymentButtonTapped), for: .touchUpInside)
                
                
                cell.studentResultView.layer.cornerRadius = 10;
                cell.studentResultView.layer.masksToBounds = true;
                cell.studentResultView.layer.borderWidth = 1
                cell.studentResultView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                let classTestImageTap = UITapGestureRecognizer(target: self, action: #selector(self.studentResultButtonTapped))
                let classTestViewTap = UITapGestureRecognizer(target: self, action: #selector(self.studentResultButtonTapped))
                cell.studentResultView.addGestureRecognizer(classTestViewTap)
                cell.studentResultImageView.addGestureRecognizer(classTestImageTap)
                cell.studentResultButton.addTarget(self, action: #selector(self.studentResultButtonTapped), for: .touchUpInside)
                
                cell.schoolName.text = user?.getStudentCurrentlySelectedStudent()?.schoolName
                
                return cell
            }
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "EmployeeHomeHeaderCell") as! EmployeeHomeHeaderCell
                employeeHeaderCell = cell
		        cell.delegate = self
                cell.studentSearch.endEditing(true)
                cell.studentSearch.delegate = self
                cell.studentSearch.placeholder = "Search Students"
                cell.studentSuggesionHeight.constant = 0
                cell.backgroundColor = UIColor.clear
                cell.studentSearch.returnKeyType = .done
                cell.studentSearch.enablesReturnKeyAutomatically = false
                if user?.currentTypeOfUser == .employee {
                cell.schoolName.text = user?.schoolName
                }
                if user?.currentTypeOfUser == .CorporateManager {
                cell.schoolName.text =  user!.getStudentCurrentlySelectedSchool()?.schoolName
                }
                alterProfileCellForEmployee(cell: cell)
                return cell
                
            }
        }
        else {
            if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "EmployeeHomeCell") as! EmployeeHomeCell
                cell.attendanceView.layer.cornerRadius = 10;
                cell.attendanceView.layer.masksToBounds = true;
                cell.attendanceView.layer.borderWidth = 1
                cell.attendanceView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
 
                cell.studentProfileView.layer.cornerRadius = 10;
                cell.studentProfileView.layer.masksToBounds = true;
                cell.studentProfileView.layer.borderWidth = 1
                cell.studentProfileView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                
                cell.timetableView.layer.cornerRadius = 10;
                cell.timetableView.layer.masksToBounds = true;
                cell.timetableView.layer.borderWidth = 1
                cell.timetableView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                
                cell.shuffleView .layer.cornerRadius = 10;
                cell.shuffleView.layer.masksToBounds = true;
                cell.shuffleView.layer.borderWidth = 1
                cell.shuffleView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                
                cell.shuffleView.isHidden = true
                cell.shuffleIcon.isHidden = true
                cell.shuffle.isHidden = true
                
                cell.classTestView.layer.cornerRadius = 10;
                cell.classTestView.layer.masksToBounds = true;
                cell.classTestView.layer.borderWidth = 1
                cell.classTestView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
                
                if user!.studentPromotionStatus == "SHOW" && isClassTeacher == true {
                  cell.promotionView.isHidden = false
                  cell.promotion.isHidden = false
                  cell.promotionIcon.isHidden = false
                }
                else{
                  cell.promotionView.isHidden = true
                  cell.promotion.isHidden = true
                  cell.promotionIcon.isHidden = true
                }
                cell.promotionView.layer.cornerRadius = 10;
                cell.promotionView.layer.masksToBounds = true;
                cell.promotionView.layer.borderWidth = 1
                cell.promotionView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
       
                if !webServiceRolesGroupForDifferentActivities.attendanceReportGroup.contains((user?.employeeType)!) && user?.currentTypeOfUser != .CorporateManager {
                    cell.takeAttendance.addTarget(self, action: #selector(self.takeAttendanceButtonTapped), for: .touchUpInside)
                    let tapAttendance = UITapGestureRecognizer(target: self, action: #selector(self.takeAttendanceButtonTapped))
                    
                    cell.attendanceIcon.addGestureRecognizer(tapAttendance)
                    cell.attendanceView.addGestureRecognizer(tapAttendance)
                    

            
                }
                else{
                    cell.takeAttendance.setTitle("Attendance Report", for:.normal)
                    cell.takeAttendance.addTarget(self, action: #selector(self.attendanceReportButtonTapped), for: .touchUpInside)
                    let tapAttendance = UITapGestureRecognizer(target: self, action: #selector(self.attendanceReportButtonTapped))
                    
                    cell.attendanceIcon.addGestureRecognizer(tapAttendance)
                    cell.attendanceView.addGestureRecognizer(tapAttendance)
                    

                    
                }
                
                cell.studentProfile.addTarget(self, action: #selector(self.takeEmployeeStudentProfileButtonTapped), for: .touchUpInside)
                cell.timeTable.addTarget(self, action: #selector(self.takeEmployeeTimetableButtonTapped), for: .touchUpInside)
                cell.promotion.addTarget(self,action: #selector(self.sudentPromotionButtonTapped), for: .touchUpInside)
                cell.shuffle.addTarget(self,action:#selector(self.studentShuffleButtonTapped), for: .touchUpInside)
                cell.classTestButton.addTarget(self,action:#selector(self.employeeClassTestButtonTapped), for: .touchUpInside)
                
                let tapStudentProfile = UITapGestureRecognizer(target: self, action: #selector(self.takeEmployeeStudentProfileButtonTapped))
                let tapTimeTable = UITapGestureRecognizer(target: self, action: #selector(self.takeEmployeeTimetableButtonTapped))
                let tapPromotion = UITapGestureRecognizer(target:self,action: #selector(self.sudentPromotionButtonTapped))
                let tapShuffle   = UITapGestureRecognizer(target: self, action: #selector(self.studentShuffleButtonTapped))
                let tapClasstesticon = UITapGestureRecognizer(target: self, action: #selector(self.employeeClassTestButtonTapped))
                let tapClasstestviewicon = UITapGestureRecognizer(target: self, action: #selector(self.employeeClassTestButtonTapped))
                
                cell.studentProfileIcon.addGestureRecognizer(tapStudentProfile)
                cell.studentProfileView.addGestureRecognizer(tapStudentProfile)
                
                cell.promotionView.addGestureRecognizer(tapPromotion)
                cell.promotionIcon.addGestureRecognizer(tapPromotion)
                
                cell.timetableIcon.addGestureRecognizer(tapTimeTable)
                cell.timetableView.addGestureRecognizer(tapTimeTable)
                
                cell.shuffleIcon.addGestureRecognizer(tapShuffle)
                cell.shuffleView.addGestureRecognizer(tapShuffle)
                
                cell.classTestIcon.addGestureRecognizer(tapClasstesticon)
                cell.classTestView.addGestureRecognizer(tapClasstestviewicon)
                
                return cell
            }
            else{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "UpComingEventCell") as! UpComingEventCell
                
                let event = eventArray[indexPath.row]
                cell.dayLabel.text     = event.day
                cell.yearLabel.text    = event.year
                cell.monthLabel.text   = event.month
                cell.contentLabel.text = event.title
                cell.backgroundColor = UIColor.clear
                return cell
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if user?.currentTypeOfUser == .guardian{
            
            if indexPath.section == 0 {
                return 265
            }
            
        }
        else {
            if indexPath.section == 0 {	
                return 275
            }
            else{
                return 60
            }
        }
        return CGFloat(75)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0.00001
    }

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //searchActive = false;
        searchBar.endEditing(true)
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //contactArrayTemp = contactArray
        let schoolId = user?.schoolId
        let weakSelf = self
        var studentListArray = [composeDiaryStudentPopupList]()
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        if searchText.characters.count >= 3{
            searchBar.setImage(.none, for: .search, state: .disabled)
            indicator.center =  CGPoint(x: 22, y: 15)
            searchBar.addSubview(indicator)
            indicator.startAnimating()
            
            WebServiceApi.getStudentSuggessionForSearch(schoolId: schoolId! ,nameLike: searchText,onSuccess: { (result) in
                
                //hudControllerClass.hideHudInViewController(weakSelf!)
                studentListArray = parseStudentList.parseThisDictionaryForStudentPopup(dictionary: result)
                if(studentListArray.count>0){
                    self.employeeHeaderCell.studentArray = studentListArray
                    self.employeeHeaderCell.studentSearchSuggesionTable.reloadData()
                    self.employeeHeaderCell.studentSuggesionHeight.constant=90
                    self.tableHeight.constant = self.tableHeight.constant + 90
                }else{
                    
                    self.employeeHeaderCell.studentArray = [composeDiaryStudentPopupList]()
                    self.employeeHeaderCell.studentSearchSuggesionTable.reloadData()
                    self.employeeHeaderCell.studentSuggesionHeight.constant=0
                }
                indicator.stopAnimating()
                
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                    indicator.stopAnimating()
                }, duringProgress: { (progress) in
                
            })
        }else{
            self.employeeHeaderCell.studentArray = [composeDiaryStudentPopupList]()
            self.employeeHeaderCell.studentSearchSuggesionTable.reloadData()
            self.employeeHeaderCell.studentSuggesionHeight.constant=0
            indicator.stopAnimating()
        }
        //ContactListTableView.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tabBar = self.tabBarController as! ParentEyeTabbar
        
        if user?.currentTypeOfUser == .guardian{
            if indexPath.section == 1 {
                let event = eventArray[indexPath.row]
                if event.entryType == .diary {
                    tabBar.selectedIndex = 0
                }else if event.entryType == .notice {
                    tabBar.shouldLoadNotification = true
                    tabBar.noticeToBeLoaded = event.notices
                    tabBar.selectedIndex = 1
                }else if event.entryType == .homeWork {
                    tabBar.shouldLoadHomework = true
                    tabBar.homeworkToBeLoaded = event.homeworks
                    tabBar.selectedIndex = 3
                }
                else if event.entryType == .classTest {
                   guardianClasstestButtonTapped()
                }
                
            }
            else {
                //performSegueWithIdentifier("studentProfileSegue", sender: nil)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // We just make a label an send it as the session header.
        if user?.currentTypeOfUser == .guardian{
            if section == 1 {
                let headerLabel = UILabel(frame: CGRect(x:40, y:10, width:100, height:40))
                headerLabel.text      = "    Upcoming Events"
                headerLabel.textColor = UIColor(red: 70.0/255.0, green: 70.0/255.0, blue: 70.0/255.0, alpha: 1.0)
                headerLabel.font      = UIFont(name: "Roboto-Medium", size: 15.0)
                return headerLabel
            }
        }
        return nil
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("search")
    }
    
    func pressed(sender:UIButton)
    {
        self.performSegue(withIdentifier: segueConstants.segueForLoadingStudentProfile, sender: nil)
    }
    
    
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        self.tabBarController?.slideMenuController()?.openLeft()
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        initailLoad = false
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        getUpcomingEventsFromWeb()
        listingTable.reloadData()
        let tab = self.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        initailLoad = false
        DBController().addUserToDB(user!)
        listingTable.reloadData()
        getClassListForCorporateManager()
        let tab = self.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
    }
    
    //MARK: imageUploaderProtocol
    // Here we create a action sheet for the choice gallery / camera
    //TODO :- change the image picker from here to generic ones
    func initiatePhotoChange() {
        FireBaseKeys.FirebaseKeysRuleThree(value: "homePage_changeProPic")
        let imagePicker = UIImagePickerController()
        imagePicker.delegate               = self
        imagePicker.allowsEditing          = false
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
        let popper                         = imagePicker.popoverPresentationController
        popper?.delegate                   = self
        popper?.sourceView                 = self.view
        popper?.sourceRect                 = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        
        
        
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ConstantsInUI.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: ConstantsInUI.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                imagePicker.delegate = self
                imagePicker.allowsEditing = true
                popper?.delegate          = self
                
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self.present(imagePicker, animated: true, completion: nil)
                
                // imagePicker.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
                //presentViewController(imagePicker, animated: true, completion: nil)
            }
        })
        
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        self.present(optionMenu, animated: false, completion: nil)
    }
    
    func reuploadImage() {
        
    }
    
    
    // MARK: image picker delegate
    /*func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
     picker.dismissViewControllerAnimated(true, completion: nil)
     upLoadImage(image)
     
     }*/
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated: true, completion: nil) //5
        upLoadImage(image: chosenImage)
        
    }
    
    
    //MARK: Web service
    
    /**
     Function to upload image to webservice
     
     - parameter image: the image to be uploaded
     */
    func upLoadImage(image:UIImage){
        
        var userType = ""
        if user?.currentTypeOfUser == .guardian{
            passStudentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
            userType = "Guardian"
        }
        else{
            passStudentId = (user?.userId)!
            userType = "Employee"
        }
        
        weak var weakSelf = self
        newImage = image
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadStudentProfileImage(image: image, forStudentWithId: (passStudentId), userType: userType,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            let parseResult = weakSelf!.validateJsonFromUploadingImage(dictionary: result)
            if parseResult.0 {
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.profileImageChangeSuccess, stateOfMessage: .success)
                weakSelf!.changeUserProfileWithImageUrl(imageUrl: parseResult.1,toImage: weakSelf!.newImage!)
            }else {
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.profileImageChangeFailed, stateOfMessage: .failure)
            }
            
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    /**
     
     Function to getup coming events from web
     
     */
    func getUpcomingEventsFromWeb() -> Void {
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getUpComingEventsFromWebForUseID(useriD: (user?.currentUserRoleId)!, correspondingToStudentID: (user?.Students![(user?.currentOptionSelected)!].id)!, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: self)
            weakSelf!.eventArray.removeAll()
            weakSelf!.eventArray = BaseModelListing.makeBaseClassForListingFromDictionary(dictionary: result)
            debugPrint("home",result)
            weakSelf!.listingTable.reloadData()
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
        }) { (progress) in
            
        }
    }
    
    
    
    
    /**
     function add new image to cache and remove from cache
     
     - parameter imageUrl: url of the image to be added as key
     - parameter image:    image to be added
     */
    private func changeUserProfileWithImageUrl(imageUrl:String, toImage image:UIImage){
        
        if user?.currentTypeOfUser == .guardian{
            
            ImageAPi.removeImageForURl(urlString: (user?.Students![(user?.currentOptionSelected)!].profilePic)!)
            
            user?.Students![(user?.currentOptionSelected)!].profilePic = imageUrl
            DBController().addUserToDB(user!)
            
            weak var weakSelf = self
            ImageAPi.setImage(image: ImageAPi.resizeImage(image: newImage!), forUrlString: imageUrl) { (image) in
                weakSelf!.listingTable.reloadData()
                weakSelf?.newImage = nil
            }
        }
            
        else{
            ImageAPi.removeImageForURl(urlString: (user?.profilePic)!)
            
            user?.profilePic = imageUrl
            DBController().addUserToDB(user!)
            
            weak var weakSelf = self
            ImageAPi.setImage(image: ImageAPi.resizeImage(image: newImage!), forUrlString: imageUrl) { (image) in
                weakSelf!.listingTable.reloadData()
                weakSelf?.newImage = nil
            }
            
        }
    }
    
    
    private func validateJsonFromUploadingImage(dictionary:[String:AnyObject]) ->(Bool,String){
        
        var result = true
        var ImageUrl = ""
        
        if user?.currentTypeOfUser == .guardian{
            if let resp = dictionary[resultsJsonConstants.profileImageChange.response] as? [String:AnyObject]{
                if let imgDicto = resp[resultsJsonConstants.profileImageChange.studentImg] as? [String:AnyObject]{
                    
                    if let ImgeUrl = imgDicto[resultsJsonConstants.profileImageChange.imageUrl] as? String{
                        ImageUrl = ImgeUrl
                    }
                    
                }else{
                    result = false
                }
            }else {
                result = false
            }
            
            return(result,ImageUrl)
        }
        else{
            if let resp = dictionary[resultsJsonConstants.profileImageChange.response] as? [String:AnyObject]{
                if let imgDicto = resp[resultsJsonConstants.profileImageChange.userImage] as? [String:AnyObject]{
                    
                    if let ImgeUrl = imgDicto[resultsJsonConstants.profileImageChange.imageUrl] as? String{
                        ImageUrl = ImgeUrl
                    }
                    
                }else{
                    result = false
                }
            }else {
                result = false
            }
            
            return(result,ImageUrl)
            
        }
    }
    
    
    func searchAction(textField: UITextField) {
        print("searching")
    }
    
}
