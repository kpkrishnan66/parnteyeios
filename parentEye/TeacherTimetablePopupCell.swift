//
//  TeacherTimetablePopupCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 29/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class TeacherTimetablePopupCell: UITableViewCell {

    @IBOutlet var employeeProfilePic: UIImageView!

    @IBOutlet var employeeSubstitute: UIButton!
    @IBOutlet var employeeDesignation: UILabel!
    @IBOutlet var employeeName: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.employeeProfilePic?.image = UIImage(named: "avatar")
        self.employeeName?.text = ""
        self.employeeDesignation?.text = ""
       
    }

}
