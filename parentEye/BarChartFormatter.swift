//
//  BarChartFormatter.swift
//  parentEye
//
//  Created by scientia on 16/02/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Charts

@objc(BarChartFormatter)
public class BarChartFormatter: NSObject, IAxisValueFormatter{
    
     var labels: [String] = []
    
    init(labels: [String]) {
        super.init()
        self.labels = labels
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return labels[Int(value)]
    }
}
