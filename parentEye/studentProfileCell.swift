//
//  studentProfileCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 27/06/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class studentProfileCell: UITableViewCell {
    
    @IBOutlet var studentTeacherName: UILabel!
    @IBOutlet var studentGrade: UILabel!
    @IBOutlet var studentProfile: UIImageView!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var viewMore: UIButton!
    
    @IBOutlet var studentGuardianName: UILabel!
    @IBOutlet var studentAdmnno: UILabel!
    @IBOutlet var studentAddress: UILabel!
    @IBOutlet var studentDob: UILabel!
    @IBOutlet var studentMobno: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        ImageAPi.makeImageViewRound(imageView: studentProfile)
        // Initialization code
}

    @IBAction func viewMoreAction(sender: AnyObject) {
        
    }
    
    func alterCellWithProfileImageUrl(_ url:String) -> Void {
        studentProfile!.image = UIImage(named:"avatar.png")
        if(url.isEmpty){
            studentProfile!.image = UIImage(named:"avatar.png")
        }
        else{
            weak var weakProfileImage = self.studentProfile
            ImageAPi.fetchImageForUrl(urlString: url, oneSuccess: { (image) in
                if weakProfileImage != nil {
                    weakProfileImage!.image = image
                }else{
                    weakProfileImage!.image = UIImage(named:"avatar.png")
                }
            }) { (error) in
                weakProfileImage!.image = UIImage(named:"avatar.png")
            }
        }
        
    }


}
