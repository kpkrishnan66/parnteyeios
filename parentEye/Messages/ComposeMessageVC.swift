//
//  ComposeMessageVC.swift
//  parentEye
//
//  Created by Martin Jacob on 14/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import KSTokenView
import MobileCoreServices
import Firebase
import IQKeyboardManagerSwift

class ComposeMessageVC: UIViewController,CommonHeaderProtocol,UITextFieldDelegate,UITextViewDelegate,popOverDelegate,UIPopoverPresentationControllerDelegate,messageAddRecipientsProtocol,KSTokenViewDelegate,UITableViewDelegate,UITableViewDataSource,passGroupIdProtocol,dismissKeyBoardDelegateProtocol,infoButtonDelegateProtocol,UIImagePickerControllerDelegate,attachmentimagelibraryProtocol,attachmentDeleteButtonPressedProtocol,UINavigationControllerDelegate,UIDocumentPickerDelegate,UIGestureRecognizerDelegate,DiaryComposeSchoolListProtocol {
    
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    lazy var messageReciepientArray  = [userModel]()//Array to store all the receipients
    lazy var tempMessageRecipientArray = [userModel]()
    lazy var groupMembersArray = [userModel]()
    lazy var uploadAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    lazy var tempuploadAttachmentArray = [uploadAttachmentModel]()
    lazy var schoolListArray = [corporateManagerSchoolModel]()
    lazy var schoolRecipientListArray = [corporateManagerSchoolModel]()
    var profilePic = ""
    var studentID                    = ""
    var userID                       = ""
    var schoolID                     = ""
    var msgTitle                     = ""
    var msgContent                   = ""
    var user:userModel?
    var RoleList = [recipientRoles]()
    var recipientTypeValueList = [String]()
    var recipientTypeNameList = [String]()
    var groupList = [String]()
    var groupIdList = [Int]()
    var selectAllButtonTapped:Bool = true
    var addButtonTapped:Bool = true
    var initialLoad:Bool = false
    var roleSelected:Bool = false
    var moveToNewGroup:Bool = false
    var canEdit:Bool = false
    var editGroupEnable:Bool = false
    var selectedRolePosition:Int = 0
    var maxCharacterCout:Int = 0
    var characterCount:Int = 0
    var InfoButtonId = 0
    var recipientRole = ""
    var groupId:String = ""
    var maximumCountView:maxCountView = maxCountView()
    var tempMainViewHeight:CGFloat = 0
    var beforeAttachmentMainViewHeight:CGFloat = 0
    var tempGroupRoleListingTableViewHeight:CGFloat = 0
    var myView :UIView = UIView()
    lazy var copyuploadAttachmentArray = [uploadAttachmentModel]()
    var uploadAttachmentFailure:Bool = false
    var attachmentImages = [Int:UIImage]()
    var toUploadAttachments = [Int:UIImage]()
    var cameraImages = [Int:UIImage]()
    var activeSelection = false
    var deleteAttachment = false
    var getAllStudentList = false
    var defaultSchoolDisplay:Bool = false
    var uploadedcount:Int = 0
    var attachmentListString = ""
    var cameraSelected = false
    var imageArray = [UIImageView]()
    var tagCount = 0
    var posCount = 0
    let redColor = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    var red   = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
    var green = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
    var blue  = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
    var forwardingMsg = false
    var messageDetailsSelf:MessageDetailedLisiting = MessageDetailedLisiting()
    
    

    @IBOutlet weak var groupRoleListingTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var groupRoleListingTableView: UITableView!
    @IBOutlet weak var header:CommonHeader!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var tokenView: KSTokenView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var infoIcon: UIButton!
    @IBOutlet weak var descriptionCharacterCount: UILabel!
    @IBOutlet weak var messageDescriptionLabel: UILabel!
    @IBOutlet weak var messageExceedLabel: UILabel!
    @IBOutlet weak var messageExceedView: UIView!
    @IBOutlet weak var attachmentView: UIImageView!
    @IBOutlet weak var attachmentViewContainer: ExpansiveView!
    @IBOutlet weak var attachmentViewContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var messageExceedViewHeight: NSLayoutConstraint!
    @IBOutlet weak var attachmentDocumentTableView: UITableView!
    @IBOutlet weak var attachmentDocumentTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var schoolNameTextField: UITextField!
    @IBOutlet weak var schoolLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolNameTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var allSchoolButtonHeight: NSLayoutConstraint!
    @IBOutlet var allSchoolButton: UIButton!
        //MARK:- View delegates

    override func viewDidLoad() {
        getUserDataFromDB()
        setupUI()
        alterHeader()
        setUpTokenView()
        setupTable()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if moveToNewGroup == true {
           moveToNewGroup = false
        }
        if(forwardingMsg){
            forwardingMsg = false
            //if ComposeMessageVC().isViewLoaded() && (ComposeMessageVC().view.window != nil) {
            //hudControllerClass.shownormalHudToViewController(self)
            check()
            hudControllerClass.hideHudInViewController(viewController: messageDetailsSelf)
            //}
        }
    }
    
    
    
    private func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        showAlert()
    }
    func setupUI() {
        
        tokenView.layer.borderWidth = CGFloat(1)
        subjectTextField.layer.borderWidth = CGFloat(1)
        messageTextView.layer.borderWidth = CGFloat(1)
        messageExceedView.layer.borderWidth = CGFloat(1)
        schoolNameTextField.layer.borderWidth = CGFloat(1)
        
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        tokenView.layer.borderColor = color.cgColor
        subjectTextField.layer.borderColor = color.cgColor
        messageTextView.layer.borderColor = color.cgColor
        messageExceedView.layer.borderColor = color.cgColor
        schoolNameTextField.layer.borderColor = color.cgColor
        subjectTextField.text = msgTitle
        messageTextView.text = msgContent
        tokenView.isUserInteractionEnabled = false
        if (user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager) && user?.smsExceeded == 1 {
            setCommonMaxCountView()
            maxCharacterCout = Int((user?.smsCharacterLimit)!)!
            
        }
        groupRoleListingTableViewHeight.constant = 0
        messageExceedViewHeight.constant = 0
        if user?.currentTypeOfUser == .CorporateManager {
            self.mainViewHeight.constant = 575
            schoolLabelHeight.constant = 20
            schoolNameTextFieldHeight.constant = 40
            allSchoolButtonHeight.constant = 30
            allSchoolButton.isHidden = false
            
        }
        else{
            self.mainViewHeight.constant = 515
            schoolLabelHeight.constant = 0
            schoolNameTextFieldHeight.constant = 0
            allSchoolButtonHeight.constant = 0
            allSchoolButton.isHidden = true
        }
        
        beforeAttachmentMainViewHeight = mainViewHeight.constant
        messageDescriptionLabel.isHidden = true
        messageExceedLabel.isHidden = true
        infoIcon.isHidden = true
        descriptionCharacterCount.isHidden = true
        
        messageTextView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil);

        
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "dd-MM-yyyy"
        dateLabel.text = dateFormat.string(from: Foundation.Date())
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.addAttachmentImageTapped))
        attachmentView.isUserInteractionEnabled = true
        attachmentView.addGestureRecognizer(tapGestureRecognizer)
        attachmentViewContainer.delegate = self
        

        /*Auto capital words*/
        subjectTextField.autocapitalizationType = UITextAutocapitalizationType.sentences
        messageTextView.autocapitalizationType = UITextAutocapitalizationType.sentences
        
        if user?.currentTypeOfUser == .CorporateManager {
            schoolNameTextField.text = user?.schools![(user?.currentSchoolSelected)!].schoolName
            schoolNameTextField.delegate = self
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
            
        }
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysHide
    }
    
    private func setCommonMaxCountView() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        
        maximumCountView = maxCountView(frame: CGRect(x:0, y:0, width:screenWidth, height:75))
        maximumCountView.countdelegate = self
        maximumCountView.infoButtonDelegate = self
        maximumCountView.messageExceedLabel.isHidden = true
        maximumCountView.descriptionLabel.isHidden = true
        maximumCountView.infoIcon.isHidden = true
        self.messageTextView.inputAccessoryView = maxCountView(frame: CGRect(x:0, y:0, width:screenWidth, height:75))
        self.myView = maximumCountView
        self.messageTextView.inputAccessoryView?.addSubview(self.myView)
    }
    
    @objc func textView(_ textView: UITextView,shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        maximumCountView.characterCount.text = String(numberOfChars)+"/"+String(maxCharacterCout)
        descriptionCharacterCount.text = String(numberOfChars)+"/"+String(maxCharacterCout)
        messageExceedLabel.isHidden = true
        descriptionCharacterCount.isHidden = true
        messageDescriptionLabel.isHidden = true
        infoIcon.isHidden = true
        messageExceedViewHeight.constant = 0
        characterCount = numberOfChars
        if numberOfChars > maxCharacterCout {
            maximumCountView.messageExceedLabel.isHidden = false
            maximumCountView.descriptionLabel.isHidden = true
            maximumCountView.infoIcon.isHidden = false
            messageExceedLabel.isHidden = false
            messageDescriptionLabel.isHidden = true
            infoIcon.isHidden = false
            
            
        }
        if numberOfChars <= maxCharacterCout {
            maximumCountView.messageExceedLabel.isHidden = true
            maximumCountView.descriptionLabel.isHidden = true
            maximumCountView.infoIcon.isHidden = true
            messageExceedLabel.isHidden = true
            messageDescriptionLabel.isHidden = true
            infoIcon.isHidden = true
            
        }
        return true
        
    }
    
    //MARK: -UITextview deleagte to dismiss keyboard
    func didPressButton() {
        
        view.endEditing(true)
        messageTextView.selectedTextRange = nil
         mainViewHeight.constant = tempMainViewHeight
         beforeAttachmentMainViewHeight = tempMainViewHeight
         groupRoleListingTableViewHeight.constant = tempGroupRoleListingTableViewHeight
        
        if characterCount > maxCharacterCout {
            messageExceedViewHeight.constant = 41
            messageExceedLabel.isHidden = false
            descriptionCharacterCount.isHidden = false
            messageDescriptionLabel.isHidden = true
            infoIcon.isHidden = false
            
        }
        if characterCount <= maxCharacterCout {
            messageExceedViewHeight.constant = 20
            messageExceedLabel.isHidden = true
            descriptionCharacterCount.isHidden = false
            messageDescriptionLabel.isHidden = true
            infoIcon.isHidden = true
            
        }
    }
    
    //MARK: -UIButton deleagte to identify info Button Pressed or Not
    func didPressedButton() {
        
        if  InfoButtonId%2 == 0 {
            maximumCountView.descriptionLabel.isHidden = false
            let startPosition = messageTextView.position(from: messageTextView.beginningOfDocument, offset: 0)
            let endPosition = messageTextView.position(from: messageTextView.beginningOfDocument, offset:
                maxCharacterCout)
            if startPosition != nil && endPosition != nil {
                messageTextView.selectedTextRange = messageTextView.textRange(from: startPosition!, to: endPosition!)
            }
            InfoButtonId += 1
        }
        else{
            maximumCountView.descriptionLabel.isHidden = true
            let newPosition = messageTextView.endOfDocument
            messageTextView.selectedTextRange = messageTextView.textRange(from: newPosition, to: newPosition)
            InfoButtonId += 1
        }
        
    }
    
    @IBAction func infoIconClicked(_ sender: UIButton) {
    
    
            view.endEditing(false)
            messageTextView.becomeFirstResponder()
            if  InfoButtonId%2 == 0 {
                messageDescriptionLabel.isHidden = false
                let startPosition = messageTextView.position(from: messageTextView.beginningOfDocument, offset: 0)
                let endPosition = messageTextView.position(from: messageTextView.beginningOfDocument, offset: maxCharacterCout)
                
                if startPosition != nil && endPosition != nil {
                    messageTextView.selectedTextRange = messageTextView.textRange(from: startPosition!, to: endPosition!)
                }
                InfoButtonId += 1
            }
            else{
                messageDescriptionLabel.isHidden = true
                let newPosition = messageTextView.endOfDocument
                messageTextView.selectedTextRange = messageTextView.textRange(from: newPosition, to: newPosition)
                InfoButtonId += 1
            }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if user?.smsExceeded == 1 {
            descriptionCharacterCount.isHidden = true
            messageExceedLabel.isHidden = true
            messageDescriptionLabel.isHidden = true
            infoIcon.isHidden = true
            messageTextView.selectedTextRange = nil
            tempMainViewHeight = mainViewHeight.constant
            tempGroupRoleListingTableViewHeight = groupRoleListingTableViewHeight.constant
        }
        
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        if user?.smsExceeded == 1 {
            descriptionCharacterCount.isHidden = false
            messageExceedLabel.isHidden = false
            messageDescriptionLabel.isHidden = false
            infoIcon.isHidden = false
            mainViewHeight.constant = tempMainViewHeight
            beforeAttachmentMainViewHeight = tempMainViewHeight
        }
    }
    
    //MARK: -UITextfield deleagte to dismiss keyboard
    
    func textFieldShouldBeginEditing(subjectTextField: UITextField) -> Bool {
        subjectTextField.resignFirstResponder()
        return true;
    }
    func textFieldShouldendEditing(subjectTextField: UITextField) -> Bool {
        subjectTextField.resignFirstResponder()
        return true;
    }
    
    private func setupTable() -> Void {
        
        groupRoleListingTableView.register(UINib(nibName: "GroupRoleListPopupCell", bundle: nil), forCellReuseIdentifier: "GroupRoleListPopupCell")
        groupRoleListingTableView.estimatedRowHeight = 68.0
        //groupRoleListingTableView.rowHeight = UITableViewAutomaticDimension
        
        
        groupRoleListingTableView.register(UINib(nibName: "groupRoleListHeaderSection", bundle: nil), forCellReuseIdentifier: "groupRoleListHeaderSection")
        groupRoleListingTableView.estimatedRowHeight = 68.0
        //groupRoleListingTableView.rowHeight = UITableViewAutomaticDimension
        
        
        groupRoleListingTableView.register(UINib(nibName: "groupRoleListFooterSection", bundle: nil), forCellReuseIdentifier: "groupRoleListFooterSection")
        groupRoleListingTableView.estimatedRowHeight = 68.0
       // groupRoleListingTableView.rowHeight = UITableViewAutomaticDimension
        
        attachmentDocumentTableView.register(UINib(nibName: "attachmentDocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "attachmentDocumentTableViewCell")
        attachmentDocumentTableView.estimatedRowHeight = 68.0
        
        
    }
    
    //MARK:-
    
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.composeMessage, withProfileImageUrl: profilePic)
        if user?.currentTypeOfUser == .CorporateManager{
            header.multiSwitchButton.setTitle("", for: .normal)
        }
        header.delegate = self
    }
    
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        showAlert()
        
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        
        let user = DBController().getUserFromDB()
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    
    
    @IBAction func showSchoolListPopUp(_ sender: UITextField) {
    
    schoolNameTextField.endEditing(true)
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeSchoolListPopUpVC") as! DiaryComposeSchoolListPopUpVC ;
        schoolListArray = (user?.schools)!
        if defaultSchoolDisplay == true{
            schoolRecipientListArray.removeAll()
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
        }
        
        
        if schoolNameTextField.text == "All"{
            schoolRecipientListArray = schoolListArray
        }
        
        msRecipVC.recipientArray = schoolListArray
        msRecipVC.searchrecipientArray = schoolListArray
        msRecipVC.previousEntryArray = schoolRecipientListArray
        defaultSchoolDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    
    @IBAction func AllSchoolSelectedAction(_ sender: UIButton) {
    schoolNameTextField.text = "All"
        schoolRecipientListArray = (user?.schools)!
    }
    
    func  updateSelectedWithSchoolArray(receipientArray recipientArray: [corporateManagerSchoolModel]) {
        schoolNameTextField.text = ""
        if recipientArray.count == 0{
            schoolNameTextField!.text = ""
        }
            
        else if recipientArray.count == 1{
            schoolNameTextField!.text = recipientArray[0].schoolName
        }
        else if recipientArray.count == 2{
            schoolNameTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName
        }
        else if recipientArray.count == user?.Classes?.count{
            schoolNameTextField!.text = "All"
        }
        else {
            schoolNameTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName+","+recipientArray[2].schoolName+" + "+String(recipientArray.count - 2)
            
        }
        schoolRecipientListArray.removeAll()
        schoolRecipientListArray = recipientArray
        if schoolRecipientListArray.count == user?.schools?.count {
            schoolNameTextField.text = "All"
        }
        
    }
    
    
    @IBAction func loadDynamicRoleList(_ sender: UIButton) {
    if addButtonTapped == true && initialLoad == false{
        addButton.setTitle("Done",for: .normal)
        addButtonTapped = false
        let currentUserRoleId = user?.currentUserRoleId
        WebServiceApi.getEligibleRolesForSendingMessage(userRoleId: currentUserRoleId!, onSuccess: { (result) in
            
            self.RoleList = recipientRolesListArray.parseThisDictionaryForContactListRecords(dictionary: result)
            
            if self.RoleList.count != 0 {
                self.changeHeight()
                self.groupRoleListingTableView.reloadData()
                
            }else {
                
                //self.permissionView.removeFromSuperview()
            }
            }, onFailure: { (error) in
                
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        }
        else if addButtonTapped == true && initialLoad == true{
            addButton.setTitle("Done",for: .normal)
            addButtonTapped = false
             if self.RoleList.count != 0 {
              self.changeHeight()
              self.groupRoleListingTableView.reloadData()
            
            }
        }
     else{
        
        addButton.setTitle("Add",for: .normal)
        addButtonTapped = true
        initialLoad = true
        self.mainViewHeight.constant = ((self.mainViewHeight.constant - self.groupRoleListingTableViewHeight.constant) )
        beforeAttachmentMainViewHeight = mainViewHeight.constant
        self.groupRoleListingTableViewHeight.constant = 0
        updateRecipientAndGroupTokens()
        
     }

    }

    func changeHeight() {
        if user!.canCreateGroup == 1{
        self.groupRoleListingTableViewHeight.constant = CGFloat(((self.RoleList.count*68)+136))
        self.mainViewHeight.constant = ((self.mainViewHeight.constant + self.groupRoleListingTableViewHeight.constant) )
        beforeAttachmentMainViewHeight = mainViewHeight.constant
        }
        else {
            self.groupRoleListingTableViewHeight.constant = CGFloat(((self.RoleList.count*68)+68))
            self.mainViewHeight.constant = ((self.mainViewHeight.constant + self.groupRoleListingTableViewHeight.constant) )
            beforeAttachmentMainViewHeight = mainViewHeight.constant
        }
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
      if tableView == groupRoleListingTableView {
        if user!.canCreateGroup == 1{
            return 3
        }
        else if user!.canCreateGroup == 0{
            return 2
        }
        else{
            return 3
        }
     }
      else{
        return 1
        }
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      if tableView == groupRoleListingTableView {
            
        if section == 1 {
         return RoleList.count
        }
        else{
            return 1
        }
        }
        else{
            return documentAttachmentArray.count
      }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == groupRoleListingTableView {
            
        if indexPath.section == 1 {
          let cell =  tableView.dequeueReusableCell(withIdentifier: "GroupRoleListPopupCell") as! GroupRoleListPopupCell
          cell.roleName.text = RoleList[indexPath.row].name
          cell.selectionStyle = .none
            
            if RoleList[indexPath.row].selected == false {
              cell.groupIcon.setImage(UIImage(named: "groupIcon"), for: .normal)
            }
            else{
               cell.groupIcon.setImage(UIImage(named: "commonSelectedIcon"), for: .normal)
            }
            cell.rightArrowIcon.tag = indexPath.row
            cell.groupIcon.tag = indexPath.row
            cell.rightArrowIcon.addTarget(self, action: #selector(self.getEligibleRecipientsForSendingMessageForRole), for: UIControlEvents.touchUpInside)
            cell.groupIcon.addTarget(self, action: #selector(self.changeGroupIcon), for: UIControlEvents.touchUpInside)
            
            
          return cell
        }
        else if indexPath.section == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "groupRoleListHeaderSection") as! groupRoleListHeaderSection
            cell.selectAllorNoneButton.tag = indexPath.row
            cell.selectionStyle = .none
            if selectAllButtonTapped == true{
                cell.selectAllorNoneButton.setTitle("Select All", for: .normal)
                cell.selectAllorNoneButton.setTitleColor(greenColur, for: .normal)
            }
            else{
                
                cell.selectAllorNoneButton.setTitle("None", for: .normal)
                cell.selectAllorNoneButton.setTitleColor(redColor, for: .normal)
            }
            
            cell.selectAllorNoneButton.addTarget(self, action: #selector(self.selectAllorNoneButtonTapped), for: UIControlEvents.touchUpInside)
            return cell

        }
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "groupRoleListFooterSection") as! groupRoleListFooterSection
            cell.addNewGroupButton.addTarget(self, action: #selector(self.addNewGroupButtonTapped), for: UIControlEvents.touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
      }
        else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "attachmentDocumentTableViewCell") as! attachmentDocumentTableViewCell
            cell.documentUniqueName.text = documentAttachmentArray[indexPath.row].attachmentName
            cell.selectionStyle = .none
            if documentAttachmentArray[indexPath.row].attachmentType == "Audio" {
                cell.documentIcon.image = UIImage(named: "audioIcon")
            }
            if documentAttachmentArray[indexPath.row].attachmentType == "PDF" {
                cell.documentIcon.image = UIImage(named: "pdfIcon")
            }
            if documentAttachmentArray[indexPath.row].attachmentType == "DOCX" || documentAttachmentArray[indexPath.row].attachmentType == "DOC" {
                cell.documentIcon.image = UIImage(named: "wordIcon")
                
            }
            cell.documentCloseButton.tag = indexPath.row
            cell.documentCloseButton.addTarget(self, action: #selector(self.deleteDocumentAttachment), for: UIControlEvents.touchUpInside)
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == groupRoleListingTableView {
            
        if indexPath.section == 1 {
            let indexPath = tableView.indexPathForSelectedRow!
            let cell = tableView.cellForRow(at: indexPath)! as! GroupRoleListPopupCell
            
            if RoleList[indexPath.row].selected == false {
                cell.groupIcon.setImage(UIImage(named:"commonSelectedIcon"), for: .normal)
                RoleList[indexPath.row].selected = true
                 if RoleList[indexPath.row].id == 0 {
                    recipientTypeValueList.append(RoleList[indexPath.row].value)
                    recipientTypeNameList.append(RoleList[indexPath.row].name)
                 }
                 else{
                    groupList.append(RoleList[indexPath.row].name)
                    groupIdList.append(RoleList[indexPath.row].id)
                 }
            }
            else{
                cell.groupIcon.setImage(UIImage(named:"groupIcon"), for: .normal)
                RoleList[indexPath.row].selected = false
                if RoleList[indexPath.row].id == 0 {
                    for value in 0 ..< recipientTypeValueList.count {
                        if recipientTypeValueList[value] == RoleList[indexPath.row].value {
                            recipientTypeValueList.remove(at: value)
                            recipientTypeNameList.remove(at: value)
                            break
                        }
                    }
                }
                else {
                    for value in 0 ..< groupIdList.count {
                        if groupIdList[value] == RoleList[indexPath.row].id {
                            groupIdList.remove(at: value)
                            groupList.remove(at: value)
                            break
                        }
                    }
                    
                }
            }
            
            
        }
        
        if indexPath.section == 0 {
            let indexPath = tableView.indexPathForSelectedRow!
            let cell = tableView.cellForRow(at: indexPath)! as! groupRoleListHeaderSection
            recipientTypeNameList.removeAll()
            recipientTypeValueList.removeAll()
            groupList.removeAll()
            groupIdList.removeAll()
            
         if selectAllButtonTapped == true{
            cell.selectAllorNoneButton.setTitle("Select All", for: .normal)
            cell.selectAllorNoneButton.setTitleColor(greenColur, for: .normal)
            selectAllButtonTapped = false
            for i in 0 ..< RoleList.count {
                RoleList[i].selected = true
                if RoleList[i].id == 0 {
                    recipientTypeNameList.append(RoleList[i].name)
                    recipientTypeValueList.append(RoleList[i].value)
                } 
                else{
                    groupIdList.append(RoleList[i].id)
                    groupList.append(RoleList[i].name)
                }
            }
         }
          else{
            cell.selectAllorNoneButton.setTitle("None", for: .normal)
            cell.selectAllorNoneButton.setTitleColor(redColor, for: .normal)
            selectAllButtonTapped = true
            for i in 0 ..< RoleList.count {
                RoleList[i].selected = false
            }
            }
            groupRoleListingTableView.reloadData()
            updateRecipientAndGroupTokens()
        }
        
        if indexPath.section == 2 {
        //let msg = filteredMessageLstingArray[indexPath.row]
        editGroupEnable = false
        self.performSegue(withIdentifier: segueConstants.segueToAddNewGroup, sender: nil)
        }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == groupRoleListingTableView {
            
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.1, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
                UIView.animate(withDuration: 0.1, animations: {
                    cell.layer.transform = CATransform3DMakeScale(1,1,1)
                })
        })
        }
    }
    
    @objc private func selectAllorNoneButtonTapped(sender:UIButton){
        recipientTypeNameList.removeAll()
        recipientTypeValueList.removeAll()
        groupList.removeAll()
        groupIdList.removeAll()
        messageReciepientArray.removeAll()
        
        if selectAllButtonTapped == true{
         selectAllButtonTapped = false
            for i in 0 ..< RoleList.count {
                RoleList[i].selected = true
                if RoleList[i].id == 0 {
                    recipientTypeNameList.append(RoleList[i].name)
                    recipientTypeValueList.append(RoleList[i].value)
                }
                else{
                    groupIdList.append(RoleList[i].id)
                    groupList.append(RoleList[i].name)
                }
            }
          }
        else{
         selectAllButtonTapped = true
            for i in 0 ..< RoleList.count {
                RoleList[i].selected = false
            }
            
        }
        groupRoleListingTableView.reloadData()
        updateRecipientAndGroupTokens()
    }
    
    @objc private func addNewGroupButtonTapped(sender:UIButton){
        print("tapped")
        editGroupEnable = false
        groupId = "0"
        self.performSegue(withIdentifier: segueConstants.segueToAddNewGroup, sender: nil)
    }
    
    @objc private func getEligibleRecipientsForSendingMessageForRole(sender:UIButton){
        webServiceToGetRecipient(sender: sender)
    }
    
    @objc private func changeGroupIcon(sender:UIButton){
        let indexPath = NSIndexPath(row: sender.tag, section: 1)
        let cell = groupRoleListingTableView.cellForRow(at: indexPath as IndexPath) as! GroupRoleListPopupCell!
        cell?.backgroundColor = UIColor.white
        
        if RoleList[sender.tag].selected == false {
            cell?.groupIcon.setImage(UIImage(named:"commonSelectedIcon"), for: .normal)
            RoleList[indexPath.row].selected = true
             if RoleList[indexPath.row].id == 0 {
                recipientTypeValueList.append(RoleList[indexPath.row].value)
                recipientTypeNameList.append(RoleList[indexPath.row].name)
             }
             else{
                groupList.append(RoleList[indexPath.row].name)
                groupIdList.append(RoleList[indexPath.row].id)
             }
            
        }
        else{
            cell?.groupIcon.setImage(UIImage(named:"groupIcon"), for: .normal)
            RoleList[indexPath.row].selected = false
             if RoleList[indexPath.row].id == 0 {
                for value in 0 ..< recipientTypeValueList.count {
                    if recipientTypeValueList[value] == RoleList[indexPath.row].value {
                        recipientTypeValueList.remove(at: value)
                        recipientTypeNameList.remove(at: value)
                        break
                    }
                }
             }
             else {
                for value in 0 ..< groupIdList.count {
                    if groupIdList[value] == RoleList[indexPath.row].id {
                        groupIdList.remove(at: value)
                        groupList.remove(at: value)
                        break
                    }
                }
                
            }
        }
            
    }
    
    
    
    
    //MARK:- Webservice

      func webServiceToGetRecipient(sender:UIButton){
        var userType:String = ""
        var schoolListString = ""
        var flag:Int = 0
        
        selectedRolePosition = sender.tag
        roleSelected = false
        for i in 0 ..< RoleList.count {
            if i == sender.tag{
            recipientRole = RoleList[i].name
            groupId = String(RoleList[i].id)
            canEdit = RoleList[i].canEdit
              if RoleList[i].selected == true {
                roleSelected = true
              }
            break
            }
            
        }
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        if user?.currentTypeOfUser == .guardian {
           userType = "Guardian"
        }
        else if user?.currentTypeOfUser == .employee {
        
           userType = "Employee"
            
        }
        else{
           userType = "CorporateManager"
                if schoolRecipientListArray.count > 0 {
                    for schoolArray in schoolRecipientListArray{
                        schoolListString = schoolListString+String(schoolArray.schoolId) + ","
                    }
                    schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString.endIndex) )
                }
                else{
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectSchool, stateOfMessage:.failure)
                }

        }
        
        WebServiceApi.getEligibleRecipientsForSendingMessageForRole(recipientRole: recipientRole,userRoleId:userID,userType:userType,groupId:groupId,corporateId: String(user!.corporateId),schoolListString: schoolListString,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                var rst = userModel.makeArrayOfUserFromRecipientDictionary(dicto: result)
                let messageReciepientArrayCount = self.messageReciepientArray.count
                 self.tempMessageRecipientArray = self.messageReciepientArray
                    if rst.1 != nil{
                        AlertController.showToastAlertWithMessage(message: (rst.1?.localizedDescription)!, stateOfMessage: .failure)
                        
                    }else {
                        if self.roleSelected == true {
                            if self.messageReciepientArray.count == 0 {
                                self.messageReciepientArray = rst.0
                            }
                            else{
                            for pos in 0 ..< rst.0.count {
                               rst.0[pos].selectedInUI = true
                                flag = 0
                                for prepos in 0 ..< messageReciepientArrayCount {
                                    if self.messageReciepientArray[prepos].userId == rst.0[pos].userId {
                                        self.messageReciepientArray[prepos].selectedInUI = true
                                        flag = 1
                                        break
                                    }
                                 
                                }
                                if flag == 0 && self.messageReciepientArray.count != 0{
                                 self.messageReciepientArray.append(rst.0[pos])
                                }
                              }
                            }
                        }
                        else {
                            for pos in 0 ..< rst.0.count {
                                rst.0[pos].selectedInUI = false
                            }
                        }
                        weakSelf!.showRecipientSelectionScreenWithUsers(usersArray: rst.0)
                }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
            }, forStudentID: studentID)
        
    }
    
    
    private func showRecipientSelectionScreenWithUsers(usersArray:[userModel]) {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "MessageAddReceipientsVC") as! MessageAddReceipientsVC;
        msRecipVC.recipientArray = usersArray
        msRecipVC.searchrecipientArray = usersArray
        groupMembersArray = usersArray
        msRecipVC.previousEntryArray = messageReciepientArray
        msRecipVC.isroleSelected = roleSelected
        msRecipVC.canEdit = canEdit
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        self.present(msRecipVC, animated: false, completion: nil)
        
        
    
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    
    //MARK:- messageAddRecipient Delegates
    
    func updateSelectedWithArray(previousArray receipientArray: [userModel],usersArray userArray:[userModel]) {
        var flag:Int = 0
        messageReciepientArray.removeAll()
        
        /* chek if all recipients are selected or not */
        for pos in 0 ..< userArray.count {
            if userArray[pos].selectedInUI == true {
               flag = 0  // set if all recipients are true
               
            }
            else{
                flag = 1 // set if any recipient is false
                break
            }
        }
        
        if flag != 0 {
         for k in 0 ..< userArray.count {
            if userArray[k].selectedInUI == false {
                flag = 1  // set all recipients are false
            
            }
            else{
                flag = 2 // set if any recipient is true
                break
            }
         }
        }
        
        if flag == 0 {
           messageReciepientArray = tempMessageRecipientArray
            if userArray.count != 0 {
            for i in 0 ..< RoleList.count {
                if i == selectedRolePosition {
                    RoleList[i].selected = true
                    if roleSelected == false {
                        if RoleList[i].id == 0 {
                            recipientTypeNameList.append(RoleList[selectedRolePosition].name)
                            recipientTypeValueList.append(RoleList[selectedRolePosition].value)
                        }
                        else{
                           groupIdList.append(RoleList[selectedRolePosition].id)
                           groupList.append(RoleList[selectedRolePosition].name)
                        }
                    }
                    break
                }
              }
            }
            
            groupRoleListingTableView.reloadData()
        }
         else {
            for i in 0 ..< RoleList.count {
                if i == selectedRolePosition {
                    RoleList[i].selected = false
                }
            }
            for value in 0 ..< recipientTypeValueList.count {
                if recipientTypeValueList[value] == RoleList[selectedRolePosition].value {
                    recipientTypeValueList.remove(at: value)
                    recipientTypeNameList.remove(at: value)
                    break
                }
            }
            for value in 0 ..< groupIdList.count {
                if groupIdList[value] == RoleList[selectedRolePosition].id {
                    groupIdList.remove(at: value)
                    groupList.remove(at: value)
                    break
                }
            }
            if flag == 1 {
                
               // messageReciepientArray = tempMessageRecipientArray
                messageReciepientArray = receipientArray
            }
            if flag == 2 {
                messageReciepientArray = receipientArray
            }
            groupRoleListingTableView.reloadData()
            
        }
            
        updateRecipientAndGroupTokens()
        
    }
    
    private func updateRecipientAndGroupTokens() {
        
        tokenView.deleteAllTokens()
        
        for receipient in messageReciepientArray {
            let token = KSToken(title: receipient.username, object: receipient)
            token.tokenBackgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            red                  = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            green                = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            blue                 = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            token.tokenTextColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            tokenView.addToken(token)
        }
        
        for receipient in recipientTypeNameList {
            let token = KSToken(title: receipient, object: receipient as AnyObject?)
            token.tokenBackgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            red                  = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            green                = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            blue                 = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            token.tokenTextColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            tokenView.addToken(token)
        }
        
        for group in groupList {
            let token = KSToken(title: group, object: group as AnyObject?)
            token.tokenBackgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            red                  = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            green                = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            blue                 = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
            token.tokenTextColor = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
            
            tokenView.addToken(token)
        }
        
    }
    
    private func setUpTokenView(){
        //tokenView.delegate = self
        //tokenView.promptText = ""
    }
    
    
     //MARK:- kstoken delegates
    func tokenView(_ token: KSTokenView, performSearchWithString string: String, completion: ((_ results: Array<AnyObject>) -> Void)?) {
        
        WebServiceApi.searchForTokenName(name: string, userRoleId: userID, onSuccess: { (result) in
            let users =  userModel.makeArrayOfUserFromRecipientDictionary(dicto:result)
            if users.1 != nil{
            }else {
                completion!(users.0)
            }
            }, onFailure: { (error) in
                completion!([userModel]())
            }) { (progress) in
                
        }
    }
    
    func tokenView(_ token: KSTokenView, displayTitleForObject object: AnyObject) -> String {
        
        let user = object as! userModel
        return user.username
    }
    
    func tokenView(tokenView: KSTokenView, shouldAddToken token: KSToken) -> Bool {
        //Checking here is done so that simple text added from keyboard should nit be added.
        if (token.object as? userModel) != nil{
            return true
        }
        return false
    }
    
    func tokenView(tokenView: KSTokenView, didDeleteToken token: KSToken) {
        //Removing from data source
        let user = token.object as! userModel
        messageReciepientArray = messageReciepientArray.filter{$0.userId != user.userId}
    }
    
    func tokenView(token: KSTokenView, withObject object: AnyObject, tableView: UITableView,    cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
            let cell             = UITableViewCell(style: .default, reuseIdentifier: "")
            let user             = object as! userModel
            cell.textLabel?.text = user.username
        return cell
    }
    
    func tokenView(tokenView: KSTokenView, didAddToken token: KSToken) {
        //Checking here is done so that we do not add duplicate entries ie we add from two different screens and when a token is added from the add receipients UI this function is called.
       let users = token.object as! userModel
        let filteredArray = messageReciepientArray.filter{$0.userId == users.userId}
        
        if filteredArray.count == 0 {
            messageReciepientArray.append(users)
        }
    }
    
    //MARK:- IBactions
    
    @IBAction func sendMasterMesasge(_ sender: UIButton) {
    
    FireBaseKeys.FirebaseKeysRuleOne(value: "composeMessage_send")
    let result = validateFields()
        if result.0 {
            var idString = ""
            var recipientKeysIdString = ""
            var groupIdString = ""
            var role = ""
            var studentIdValue = ""
            var schoolListString = ""
            
            if user?.currentTypeOfUser == .CorporateManager {
                for schoolArray in schoolRecipientListArray{
                    schoolListString = schoolListString+String(schoolArray.schoolId) + ","
                }
                schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString.endIndex) )
            }
            
            for user in messageReciepientArray {
                idString = idString+user.userId + ","
            }
            if messageReciepientArray.count != 0 {
                idString = idString.substring(to: idString.characters.index(before: idString.endIndex) )
            }
            
            for value in recipientTypeValueList {
                recipientKeysIdString = recipientKeysIdString+value+","
            }
            if recipientTypeValueList.count != 0 {
                recipientKeysIdString = recipientKeysIdString.substring(to: recipientKeysIdString.characters.index(before: recipientKeysIdString.endIndex) )
            }
            
            for id in groupIdList {
                groupIdString = groupIdString+String(id)+","
            }
            if groupIdList.count != 0 {
                groupIdString = groupIdString.substring(to: groupIdString.characters.index(before: groupIdString.endIndex) )
            }
            
              if uploadAttachmentArray.count > 0 {
                for attachmentArray in uploadAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex) )
                
               }
              if documentAttachmentArray.count > 0 {
                if uploadAttachmentArray.count != 0 {
                attachmentListString = attachmentListString + ","
                }
                for attachmentArray in documentAttachmentArray {
                    attachmentListString = attachmentListString+String(attachmentArray.attachmentId) + ","
                }
                attachmentListString = attachmentListString.substring(to: attachmentListString.characters.index(before: attachmentListString.endIndex) )
                
              }
            
            
            if user?.currentTypeOfUser == .guardian{
                schoolID = (user?.getSelectedEmployeeOrStudentschoolId())!
                role = "Guardian"
                studentIdValue = (user?.getStudentCurrentlySelectedStudent()!.id)!
            }
            else{
              schoolID = String(user!.schoolId)
                role = "Employee"
            }
            
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            
            WebServiceApi.postMessageWithTitle(title: subjectTextField.text!,
                                               contentOfMessage: messageTextView.text,
                                               idOfSchool: schoolID,
                                               recipientIdList: idString,
                                               recipientKeysIdList: recipientKeysIdString,
                                               groupIdList: groupIdString,
                                               idOfMesssageAddingPerson: userID,
                                               studentId:studentIdValue,
                                               Role:role,
                                               attachmentIdList: attachmentListString,
                                               corporateId:String(user!.corporateId),
                                               recipientSchoolIdList:schoolListString,
                                               onSuccess: { (result) in
                                                
                                                 hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                if (result["message"] as? [String:AnyObject]) != nil{
                                               AlertController.showToastAlertWithMessage(message: ConstantsInUI.messagePostingSucces, stateOfMessage: .success)
                                                    weakSelf?.clearAllData()
                                                _ = weakSelf!.navigationController?.popViewController(animated: true)
                                                    
                                                    
                                                }else{
                                                AlertController.showToastAlertWithMessage(message: ConstantsInUI.mssageFailed, stateOfMessage: .failure)
                                                }
                                                
                   
                    
                                                
                },
                                               onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                  AlertController.showToastAlertWithMessage(message: ConstantsInUI.mssageFailed, stateOfMessage: .failure)
                                                
                },
                                               duringProgress: { (progress) in
                                                
            })
        }else {
         AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
    }
    /**
     Function validate the text fields
     
     - returns: It returns a tuple with bool and Error String
     */
    private func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
       
        
        if  schoolNameTextField.text == "" && user?.currentTypeOfUser == .CorporateManager {
                result = false
                ErrorMsg = ConstantsInUI.selectSchool
        }
        else if messageReciepientArray.count == 0 && recipientTypeValueList.count == 0 && groupIdList.count == 0{
            result   = false
            ErrorMsg = ConstantsInUI.noReciepientsAdded
        } else if (subjectTextField.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.noSujectAlert
        } else if (messageTextView.text == "") {
            result   = false
            ErrorMsg = ConstantsInUI.noMessageAlert
        }
        
        return (result,ErrorMsg)
    }
    
    func editButtonTapped() {
        print("edit")
        editGroupEnable = true
        self.performSegue(withIdentifier: segueConstants.segueToAddNewGroup, sender: nil)
        
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueToAddNewGroup{
            let newGroup = segue.destination as! AddNewGroupVC
            newGroup.delegate = self
            moveToNewGroup = true
            newGroup.groupId = groupId
            if editGroupEnable == true {
               newGroup.messageReciepientArray = groupMembersArray
                newGroup.selectedGroupName = recipientRole
            }
            else{
                newGroup.selectedGroupName  = ""
            }
        }
    }
    
    /**
     function to clear data
     */
    private func clearAllData(){
        
        messageTextView.text  = ""
        subjectTextField.text = ""
        messageReciepientArray.removeAll()
        //tokenView.deleteAllTokens()
    }
    
    
    private func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    func passgroupIdFromGroupVC(groupId:String,groupName:String) {
        var recipients = [recipientRoles]()
        var flag = 0
        print(groupId)
        
        for pos in 0 ..< RoleList.count {
            if RoleList[pos].id == Int(groupId) {
                flag = 1
                RoleList[pos].selected = true
                RoleList[pos].name = groupName
                break
            }
        }
        if flag == 0 {
         if let atcObj = recipientRoles(afterAddOrEditNewGroup:Int(groupId)!,name: groupName,selection: true){
             recipients.append(atcObj)
         }
         if recipients.count > 0 {
           RoleList.append(recipients[0])
         }
           self.groupRoleListingTableViewHeight.constant = self.groupRoleListingTableViewHeight.constant + 68
           self.mainViewHeight.constant = self.mainViewHeight.constant + 68
           beforeAttachmentMainViewHeight = mainViewHeight.constant
           
        }
        if groupIdList.contains(Int(groupId)!) {
            
            for k in 0 ..< groupIdList.count {
                if groupIdList[k] == Int(groupId) {
                    if groupList.count > k {
                    groupList[k] = groupName
                    }
                }
            }
        }
        else{
            groupIdList.append(Int(groupId)!)
            groupList.append(groupName)
        }
        messageReciepientArray = tempMessageRecipientArray
        updateRecipientAndGroupTokens()
        groupRoleListingTableView.reloadData()
    }
    
    @objc func addAttachmentImageTapped(img: AnyObject) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate               = self
        imagePicker.allowsEditing          = false
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
        let popper                         = imagePicker.popoverPresentationController
        popper?.delegate                   = self
        popper?.sourceView                 = self.view
        popper?.sourceRect                 = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        
        
        
        
        let optionMenu = UIAlertController(title: nil, message: ConstantsInUI.optionForImagePicker, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: ConstantsInUI.camera, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            //self.beforeAttachmentMainViewHeight = self.mainViewHeight.constant
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        
        let galleryAction = UIAlertAction(title: ConstantsInUI.gallery, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "photoGalleryId") as! PhotoGallery
            vc.delegate = self
            //self.beforeAttachmentMainViewHeight = self.mainViewHeight.constant
            self.toUploadAttachments.removeAll()
            self.uploadedcount = 0
            
            self.activeSelection = false
            self.deleteAttachment = false
            
            self.present(vc, animated: true, completion: nil)
        })
        
        let shareDocumentAction = UIAlertAction(title: ConstantsInUI.shareDocument, style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let documentPickerController = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF as NSString) , String(kUTTypeContent)], in: .import)
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
            
        })
        
        
        let cancelAction = UIAlertAction(title: ConstantsInUI.cancel, style: UIAlertActionStyle.cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        optionMenu.addAction(cameraAction)
        optionMenu.addAction(galleryAction)
        optionMenu.addAction(shareDocumentAction)
        optionMenu.addAction(cancelAction)
        
        
        optionMenu.popoverPresentationController?.sourceView = self.view
        optionMenu.popoverPresentationController?.sourceRect = CGRect(x:self.view.bounds.width / 2.0 , y:self.view.bounds.height / 2.0, width:1.0, height:1.0)
        self.present(optionMenu, animated: false, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        var documentName:String = ""
        print(url)
        
        if url.pathExtension == "pdf" || url.pathExtension == "doc" || url.pathExtension == "docx" {
         if(FileManager.default.fileExists(atPath: url.path)) {
            
            let dictoryUrl = url.absoluteString
            if let range = dictoryUrl.range(of: "/", options: .backwards) {
                _ = dictoryUrl.substring(from: range.upperBound)
            }
            if let docName = dictoryUrl.components(separatedBy: "/").last {
                print(docName)
                documentName = docName
            }
            
          let fileData = NSData(contentsOf: url as URL)
          UploadDocumentAttachment(documentData: fileData!,docName: documentName)
         }
        }
        else{
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectFormat, stateOfMessage: .failure)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        dismiss(animated: true, completion: nil)
        cameraImages[0] = chosenImage
        cameraSelected = true
        
        SelectedImagesWithArray(selectedImages: cameraImages)
    }
    func reuploadImage(){
        
    }
    
    func SelectedImagesWithArray(selectedImages:[Int:UIImage])
    {
        
        toUploadAttachments = selectedImages
        uploadedcount = 0
        tempuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray.removeAll()
        copyuploadAttachmentArray = uploadAttachmentArray
        uploadAttachmentFailure = false
        startInitialUpload(selectedImages: selectedImages)
        
    }
    
    func check(){
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var attachmentImagesArrayPosition:Int = 0
        attachmentImagesArrayPosition = attachmentImages.count
        print(uploadAttachmentArray.count)
        print(tempuploadAttachmentArray.count)
        print(attachmentImages)
        
        if uploadAttachmentArray.count != 0 || documentAttachmentArray.count != 0{
            if uploadAttachmentFailure == true {
                imageArray.removeAll()
                tagCount = 0
                posCount = 0
                for i in 0 ..< uploadAttachmentArray.count {
                    if uploadAttachmentArray[i].attachmentType == "Audio" || uploadAttachmentArray[i].attachmentType == "PDF" || uploadAttachmentArray[i].attachmentType == "DOC" || uploadAttachmentArray[i].attachmentType == "DOCX" {
                        
                    }
                  else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                        

                  }
              }
            }
            else{
                for i in 0 ..< tempuploadAttachmentArray.count {
                    if tempuploadAttachmentArray[i].attachmentType == "Audio" || tempuploadAttachmentArray[i].attachmentType == "PDF" || tempuploadAttachmentArray[i].attachmentType == "DOC" || tempuploadAttachmentArray[i].attachmentType == "DOCX" {
                    
                   }
                   else{
                        let url:NSURL = NSURL(string: uploadAttachmentArray[posCount].attachmentUrl)!
                        let data:NSData = NSData(contentsOf: url as URL)!
                        let imagea =  UIImage(data:data as Data,scale:1.0)
                        let imagv = UIImageView(image:imagea)
                        if imagv.image != nil{
                            imagv.contentMode = .scaleAspectFit
                            imagv.tag = tagCount
                            imageArray.append(imagv)
                            tagCount = tagCount + 1
                            posCount = posCount + 1
                        }
                }
              }
            }
            
            attachmentViewContainerHeight.constant = 0
            
            mainViewHeight.constant = beforeAttachmentMainViewHeight
            attachmentDocumentTableView.reloadData()
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
            
            deleteAttachment = false
            attachmentViewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: imageArray)
            attachmentDocumentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
            mainViewHeight.constant = mainViewHeight.constant + (attachmentViewContainerHeight.constant - 50) + attachmentDocumentTableViewHeight.constant
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            
        }
        else {
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            attachmentViewContainerHeight.constant = 0
           
            mainViewHeight.constant = beforeAttachmentMainViewHeight
            
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
    }
    
    func startInitialUpload(selectedImages:[Int:UIImage]){
        
        for k in 0 ..< selectedImages.count {
            
            upLoadImage(image: selectedImages[k]!)
            
        }
    }
    
    
    func upLoadImage(image:UIImage){
        weak var weakSelf = self
        attachmentViewContainerHeight.constant = 0
        
        mainViewHeight.constant = beforeAttachmentMainViewHeight
        while let subview = attachmentViewContainer.subviews.last {
            subview.removeFromSuperview()
        }
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadMessageAttachmentImage(image: image, cameraSelected: cameraSelected,
                                                 onSuccess: { (result) in
                                                    weakSelf!.addEntriesToAttachmentArray(array: parseAttachmentArrayForMessage.parseThisDictionaryForAttachments(dictionary: result))
                                                    self.uploadedcount = self.uploadedcount + 1
                                                    if self.toUploadAttachments.count == self.uploadedcount{
                                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                        self.deleteAttachment = false
                                                        self.check()
                                                        
                                                    }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.uploadAttachmentArray.removeAll()
                self.uploadAttachmentArray = self.copyuploadAttachmentArray
                self.uploadAttachmentFailure = true
                self.tempuploadAttachmentArray.removeAll()
                print("failed")
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }
    
    func UploadDocumentAttachment(documentData:NSData,docName:String){
        
        var seletedTab:String = ""
        weak var weakSelf = self
        attachmentViewContainerHeight.constant = 0
        attachmentDocumentTableViewHeight.constant = 0
        mainViewHeight.constant = beforeAttachmentMainViewHeight
        while let subview = attachmentViewContainer.subviews.last {
            subview.removeFromSuperview()
        }
        seletedTab = "Message"
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.upLoadAttachmentDocument(uploadDocumentData: documentData,docName: docName,selectedTab: seletedTab,
                                                   onSuccess: { (result) in
                                                    weakSelf!.addEntriesToDocumentAttachmentArray(array: parseAttachmentArrayForMessage.parseThisDictionaryForAttachments(dictionary: result))
                                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.uploadsucess, stateOfMessage: .success)
                                                        self.deleteAttachment = false
                                                        self.check()
                                                        
                                                    
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.uploadAttachmentArray.removeAll()
                self.uploadAttachmentArray = self.copyuploadAttachmentArray
                self.uploadAttachmentFailure = true
                self.tempuploadAttachmentArray.removeAll()
                print("failed")
                self.check()
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
    }

    func createDocumentView() {
        
        
    }
    public func addEntriesToAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {

            uploadAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    public func addEntriesToDocumentAttachmentArray(array:[uploadAttachmentModel]){
        
        if uploadAttachmentFailure == false {
            tempuploadAttachmentArray.removeAll()
            documentAttachmentArray.append(contentsOf: array)
            tempuploadAttachmentArray.append(contentsOf: array)
            print(uploadAttachmentArray.count)
        }
        
        
    }
    
    
    internal func SelectedAttachmentTag(selectedAttachment currentTag : Int){
        var tempimageArray = [UIImageView]()
        var pos = 0
        var tagTagForImage = 0
        
        
        activeSelection = true
        uploadAttachmentArray.remove(at: currentTag)
        imageArray.remove(at: currentTag)
        deleteAttachment = true
        for k in 0 ..< imageArray.count {
            
            if imageArray[k].image != nil{
                imageArray[k].contentMode = .scaleAspectFit
                imageArray[k].tag = tagTagForImage
                tempimageArray.append(imageArray[k])
                tagTagForImage = tagTagForImage + 1
                pos = pos + 1
            }
        }
        tagCount = tagTagForImage
        posCount = pos
        if uploadAttachmentArray.count != 0 && imageArray.count != 0  {
            attachmentViewContainerHeight.constant = 0
            
            mainViewHeight.constant = beforeAttachmentMainViewHeight
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
            deleteAttachment = false
            attachmentViewContainerHeight.constant = attachmentViewContainer.addImagesToMeFromArrayForAttachments(imageArray: tempimageArray)
            mainViewHeight.constant = mainViewHeight.constant + (attachmentViewContainerHeight.constant - 50)
            
            
        }
        else {
            
            attachmentViewContainerHeight.constant = 0
            
            mainViewHeight.constant = beforeAttachmentMainViewHeight
            while let subview = attachmentViewContainer.subviews.last {
                subview.removeFromSuperview()
            }
            
        }
        
    }
    
    @objc  private func deleteDocumentAttachment(sender:UIButton) {
        for k in 0 ..< documentAttachmentArray.count {
            if k == sender.tag {
              documentAttachmentArray.remove(at: k)
            }
        }
        attachmentDocumentTableView.reloadData()
        attachmentDocumentTableViewHeight.constant = CGFloat(documentAttachmentArray.count * 40)
        mainViewHeight.constant = mainViewHeight.constant - 40
    }
    
    
}
    
    


