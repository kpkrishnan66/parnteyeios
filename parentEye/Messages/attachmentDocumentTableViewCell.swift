//
//  attachmentDocumentTableViewCell.swift
//  parentEye
//
//  Created by Vivek on 01/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class attachmentDocumentTableViewCell:UITableViewCell {
    
    @IBOutlet weak var documentUniqueName: UILabel!
    @IBOutlet weak var documentIcon: UIImageView!
    
    @IBOutlet weak var documentCloseButton: UIButton!
    
}
