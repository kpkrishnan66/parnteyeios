//
//  MessageMultiSendCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/06/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class MessageMultiSendCell: UITableViewCell {
    
    
    @IBOutlet var nameofUser: UILabel!
    @IBOutlet var designationofUser: UILabel!
    @IBOutlet var profilepicofUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.profilepicofUser?.image = UIImage(named: "avatar")
        self.nameofUser?.text = ""
        self.designationofUser?.text = ""
        
    }
}

