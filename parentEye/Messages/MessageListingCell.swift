//
//  MessageListingCell.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class MessageListingCell: UITableViewCell {

    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var dateIssuedLabel:UILabel!
    @IBOutlet weak var teacherNameAndDesignationLabel:UILabel!
    @IBOutlet weak var messageSubjectLabel:UILabel!
    @IBOutlet weak var messageContentLabel:UILabel!
    @IBOutlet weak var replyImageView:UIImageView!
    @IBOutlet weak var alphabetLabel:UILabel!
    @IBOutlet weak var replyLabelWidth: NSLayoutConstraint!
    
   
    @IBOutlet var attachmentImageWidth: NSLayoutConstraint!
    @IBOutlet var attachmentImage: UIImageView!
    
    @IBOutlet var seperatorView: UIView!
    
    override func didMoveToSuperview() {
       ImageAPi.makeImageViewRound(imageView: profileImage)
        
    }
    
    
}
