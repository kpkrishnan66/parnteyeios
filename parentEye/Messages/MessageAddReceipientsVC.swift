//
//  MessageAddReceipientsVC.swift
//  parentEye
//
//  Created by Martin Jacob on 14/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

protocol messageAddRecipientsProtocol:class  {
    
    func updateSelectedWithArray(previousArray:[userModel],usersArray:[userModel])
    func editButtonTapped()
}

class MessageAddReceipientsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {

    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet weak var popupSubViewHeight: NSLayoutConstraint!
    @IBOutlet weak var editGroupButton: UIButton!
    @IBOutlet var AllOrNone: UIButton!
    @IBOutlet weak var receipientsTable: UITableView!
    @IBOutlet weak var numberOfAddedLabel: UILabel!
    @IBOutlet weak var editGroupButtonHeight: NSLayoutConstraint!
    
    var search: Bool = false
    var recipientArray = [userModel]()
    var searchrecipientArray = [userModel]()
    var temprecipientArray = [userModel]()
    var previousEntryArray = [userModel]()// an array to store the previous entered details
    var isroleSelected:Bool = false
    var recipientArrayCount:Int = 0
    var canEdit:Bool = false
    var canDismiss = false
    
    weak var delegate:messageAddRecipientsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        canDismiss = false
        searchrecipientArray = recipientArray
        temprecipientArray = recipientArray
        recipientArrayCount = recipientArray.count
        iterateArrayForFindingSelected()
        receipientsTable.register(UINib(nibName: "MessageAddReciepientsCell", bundle: nil), forCellReuseIdentifier: "MessageAddReciepientsCell")
        receipientsTable.estimatedRowHeight = 76.0
        receipientsTable.rowHeight          = UITableViewAutomaticDimension
        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.enablesReturnKeyAutomatically = false
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func setupUI() {
        
        if canEdit == false {
            editGroupButtonHeight.constant = 0
            popupSubViewHeight.constant = 100
            editGroupButton.isHidden = true
        }
        else{
            editGroupButtonHeight.constant = 35
            popupSubViewHeight.constant = 143
            editGroupButton.isHidden = false
        }
    }
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return recipientArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageAddReciepientsCell") as! MessageAddReciepientsCell
        
        let user = recipientArray[indexPath.row]
        
        cell.setTeacherSelected(selected: user.selectedInUI)
        cell.nameOfUser.text        = user.username
        cell.designationOfUser.text = user.userDesignation
        
        weak var profileImage = cell.profilePictureOfUser
        
        if user.profilePic == ""{
            cell.profilePictureOfUser.image = UIImage(named: "avatar")
            cell.profilePictureOfUser.layer.masksToBounds = true
            cell.profilePictureOfUser.layer.cornerRadius = 0
            cell.profilePictureOfUser.clipsToBounds = false
            
        }
        else{
        ImageAPi.fetchImageForUrl(urlString: user.profilePic, oneSuccess: { (image) in
            if profileImage != nil {
            
                cell.profilePictureOfUser.layer.masksToBounds = false
                cell.profilePictureOfUser.layer.cornerRadius = cell.profilePictureOfUser.frame.height/2
                cell.profilePictureOfUser.clipsToBounds = true
                profileImage!.image = image
                
            }
            }) { (error) in
                
            }
        }
        
        
        
        cell.selectionButton?.addTarget(self, action: #selector(changeSelection), for: .touchUpInside)
        cell.selectionButton?.tag = indexPath.row
        
        
        return cell
    }

   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let button = UIButton()
        button.tag = indexPath.row
        changeSelection(sender: button)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    //MARK:- updateSelected in UI 
    
    @objc private func changeSelection(sender:UIButton) {
        
        if recipientArray[sender.tag].selectedInUI == true {
            recipientArray[sender.tag].selectedInUI = false
        }else {
            recipientArray[sender.tag].selectedInUI = true
        }
        
        receipientsTable.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .middle)
        alterElementsInPreviousArrayToSelected(isSelected: !recipientArray[sender.tag].selectedInUI , userTobeAltered: recipientArray[sender.tag])
        setAddedLabel()
        
    }
    
    /**
     function to add / alter elements to the previous array
     
     - parameter isSelected: check if the entry is selected
     - parameter user:       user object that is to be altered
     */
    private func alterElementsInPreviousArrayToSelected(isSelected:Bool, userTobeAltered user:userModel){
    
        var index:Int?
        for i in 0 ..< previousEntryArray.count {
            if previousEntryArray[i].userId == user.userId{
                index = i
            }
            
        }
        
        if let ind = index {
            
            if !isSelected{
                previousEntryArray[ind].selectedInUI = isSelected
            }else {
                previousEntryArray.remove(at: ind)
            }
        }else {
            user.selectedInUI = true
            previousEntryArray.append(user)
        }
        
    }
    
    private func iterateArrayForFindingSelected(){
        var count:Int = 0
        for i in 0 ..< recipientArray.count {
            for j in 0 ..< previousEntryArray.count {
                if previousEntryArray[j].userId == recipientArray[i].userId{
                    previousEntryArray[j].selectedInUI = true
                    recipientArray[i].selectedInUI = true
                    count = count + 1
                }
                
            }
        }
        if recipientArray.count == count || isroleSelected == true{
            AllOrNone.setTitle("None", for: .normal)
        }
        else{
            AllOrNone.setTitle("All", for: .normal)
        }
        
        setAddedLabel()
        
    }
    
    
    
        
    @IBAction func editGroupButtonTapped(_ sender: UIButton) {
    
    canDismiss = true
        self.dismiss(animated: false, completion: nil)
        self.delegate?.editButtonTapped()
        
    }
    
    private func setAddedLabel(){
        if search == false{
        let unreadArray = recipientArray.filter{$0.selectedInUI}
        numberOfAddedLabel.text = String(unreadArray.count)
        }
        else{
        let unreadArray = temprecipientArray.filter{$0.selectedInUI}
        numberOfAddedLabel.text = String(unreadArray.count)
            
        }
        
    }
    
    
    
    @IBAction func dismissMe(_ sender: Any) {
    
    canDismiss = true
        if search{
            recipientArray.removeAll()
            recipientArray = temprecipientArray
        }
        self.delegate?.updateSelectedWithArray(previousArray: previousEntryArray,usersArray: recipientArray)
        self.dismiss(animated: false, completion: nil)
    }
    
    // select All or None
    
    @IBAction func selectAllOrNone(_ sender: Any) {
    
        if recipientArray.count != 0
        {
            if AllOrNone.currentTitle == "All"  {
                
                for i in 0 ..< recipientArray.count {
                       if recipientArray[i].selectedInUI != true
                       {
                       recipientArray[i].selectedInUI = true
                        alterElementsInPreviousArrayToSelected(isSelected: !recipientArray[i].selectedInUI , userTobeAltered: recipientArray[i])
                        }
                    }
                AllOrNone.setTitle("None", for: .normal)
                setAddedLabel()
                self.receipientsTable.reloadData()
                
        }
            else{
                
                    for i in 0 ..< recipientArray.count {
                        if recipientArray[i].selectedInUI != false{
                        recipientArray[i].selectedInUI = false
                        alterElementsInPreviousArrayToSelected(isSelected: !recipientArray[i].selectedInUI , userTobeAltered: recipientArray[i])
                        }
                        
                    }
                AllOrNone.setTitle("All", for: .normal)
                setAddedLabel()
                self.receipientsTable.reloadData()
            }
            
                
            }
        }
    
    
    @IBAction func searchpopupAction(_ sender: Any) {
       
    }
    
    override func dismiss(animated flag: Bool,
                                                  completion: (() -> Void)?)
    {
        if canDismiss == true{
            canDismiss = false
            super.dismiss(animated: flag, completion:completion)
        }
        
        // Your custom code here...
    }
    
    func textFieldDidChange(textField: UITextField) {
        searchRecipientListWithText(searchText: textField.text!)
    }
    
    private func searchRecipientListWithText(searchText:String){
       self.recipientArray.removeAll()
        if searchText == ""{
            search = false
            self.recipientArray = temprecipientArray
            self.receipientsTable.reloadData()
            return
        }
        
        let namePredicate            = NSPredicate(format: "username CONTAINS[cd] %@", searchText)
        let designationPredicate     = NSPredicate(format: "userDesignation CONTAINS[cd] %@", searchText)
        
        
        let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: [namePredicate,designationPredicate])
        
        
            let find = (self.searchrecipientArray as NSArray).filtered(using: compoundPredicate)
            //let filteredAray = value.filter{compoundPredicate.evaluateWithObject($0)}
            self.recipientArray =  find as! [userModel]
        searchrecipientArray = temprecipientArray
        search = true
        setAddedLabel()
        self.receipientsTable.reloadData()
        
    }
    //search events
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.endEditing(true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            recipientArray = temprecipientArray
        }else{
            let filtered = temprecipientArray.filter { $0.username.contains(searchText,options: .caseInsensitive) ||
                $0.userDesignation.contains(searchText,options: .caseInsensitive)
            }
            recipientArray = filtered
        }
        receipientsTable.reloadData()
    }
    
}

