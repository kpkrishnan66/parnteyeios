//
//  MessageDetailedCell.swift
//  parentEye
//
//  Created by Martin Jacob on 02/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//
import Foundation
import UIKit

 class MessageDetailedCell: UITableViewCell {

    @IBOutlet weak var shortUpSeparatorView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var longSeparatorView: UIView!
    @IBOutlet weak var roundImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var horizontalSeparatorView: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var detailedLabel: UILabel!
    @IBOutlet weak var spacingBetweenProfileImageAndRoundImage: NSLayoutConstraint!
    @IBOutlet weak var spaceBetweenShortSeparatorAndMainView: NSLayoutConstraint!
    @IBOutlet weak var bottomSeparatorView:UIView!
    @IBOutlet weak var messageAddedByMeLabel:UILabel!
    @IBOutlet weak var alphabetLabel:UILabel!
     @IBOutlet weak var profileImageWidth: NSLayoutConstraint!
   
    @IBOutlet weak var forwardButton: UIButton!
    @IBOutlet weak var forwardButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var attachmentView: ExpansiveView!
    @IBOutlet weak var attachmentViewHeight: NSLayoutConstraint!
     @IBOutlet var nameLabel: UIButton!
    @IBOutlet weak var profileImageHeight: NSLayoutConstraint!
    /**
     Function to set the cell for showing as header and as normal cell
     
     - parameter shouldSetupForHeader: true or false
     */
    
    
    func shouldSetUpForHeader(shouldSetupForHeader:Bool)  {
        if shouldSetupForHeader == true{
            horizontalSeparatorView.isHidden                   = false
            roundImageView.isHidden                            = true
            spaceBetweenShortSeparatorAndMainView.constant   = 10.0
            spacingBetweenProfileImageAndRoundImage.constant = 10.0
            shortUpSeparatorView.isHidden                      = true
            longSeparatorView.isHidden                         = true
            profileImageWidth.constant                       = 44.0
            profileImageHeight.constant                      = 44.0
            bottomSeparatorView.isHidden                       = false
        }else {
            horizontalSeparatorView.isHidden                   = true
            roundImageView.isHidden                            = false
            spaceBetweenShortSeparatorAndMainView.constant   = 20.0
            spacingBetweenProfileImageAndRoundImage.constant = 20.0
            shortUpSeparatorView.isHidden                      = false
            longSeparatorView.isHidden                         = false
            profileImageWidth.constant                       = 32.0
            profileImageHeight.constant                      = 32.0
            bottomSeparatorView.isHidden                       = true
            headingLabel.text = nil
        }
    }
    
    override  func didMoveToSuperview() {
         ImageAPi.makeViewRealRound(view: profileImageView)
    }
    /*override  func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.userInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(MessageDetailedCell.bigButtonTapped(_:)))
        nameLabel.addGestureRecognizer(tap)
    }*/
 
    
    
}
