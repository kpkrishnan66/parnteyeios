////
//  MessageDetailedLisiting.swift
//  parentEye
//
//  Created by Martin Jacob on 02/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import AVFoundation

class MessageDetailedLisiting: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,CommonHeaderProtocol,popOverDelegate,SelectedSchoolpopOverDelegate,audioPlayBackProtocol {
  
    
    @IBOutlet weak var messaggeDetailedListingTable:UITableView!
    @IBOutlet weak var header: CommonHeader!
    
    var user:userModel?
    var threadId                = ""
    var heading                 = ""
    var initialMessage          = ""
    var nameOfSender            = ""
    var designationOfSender     = ""
    var dateOfInitialMessage    = ""
    var schoolId                = ""
    var corporateSchoolId       = 0
    var receipientID            = ""
    var masterMessageSenderId   = ""
    var isThisThreadStartedByMe = false
    var userRoleID              = ""
    var personImage             = ""
    var profilePic = ""
    var rootMessageId           = ""
    var messageType             = ""
    var imageArray              = [String]()
    var imageArrayDetailed      = [UIImageView]()
    var firstTime               = 0
    var shouldShowReplyView     = false
    var shouldHideRplyView:Bool = false
    var manageReply:Bool        = false
    var readViaPush:Bool        = false
    lazy var messageThreadArray = [messageForListing]()
    weak var msgSendingFooter:messageSendingFooter? // to keep a reference to msgSending footer
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    var pdfViewer = PdfController()
    lazy var imageAttachmentArray = [uploadAttachmentModel]()
    lazy var documentAttachmentArray = [uploadAttachmentModel]()
    var audioPlayView  = audioPlay()
    var destinationUrl = NSURL()
    var replacedString:String = ""
    
    
    
    override func viewDidLoad() {
        print("messge detailed viewdidload")
        setupTable()
        makeWebCallToGetThreadFromMessageWithId(idOfMessage: threadId)
        getUserDataFromDB()
        setUpHeader()
        if user?.currentTypeOfUser == .CorporateManager {
            corporateSchoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
       //messagedeailedcell?.cellDelegate = self
        firstTime = 0
        
    }
    
    
    //MARK:Header functions
    
    private func setUpHeader (){
        if user?.currentTypeOfUser == .guardian{
            
        profilePic =  (user?.Students![(user?.currentOptionSelected)!].profilePic)!
        }
        else{
            profilePic = ""
        }
        if messageType == "Broadcast"{
            nameOfSender = "Broadcast"
        }
        
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: nameOfSender, withProfileImageUrl: profilePic)
        header.delegate = self
        
        
    }
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    
    
    //MARK:Table functions
    private func setupTable() -> Void {
        messaggeDetailedListingTable.register(UINib(nibName: "MessageDetailedCell", bundle: nil), forCellReuseIdentifier: "MessageDetailedCell")
        
        messaggeDetailedListingTable.register(UINib(nibName: "messageSendingFooter", bundle: nil), forHeaderFooterViewReuseIdentifier: "messageSendingFooter")
        messaggeDetailedListingTable.register(UINib(nibName: "HeaderViewWithButton", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderViewWithButton")
        
        messaggeDetailedListingTable.estimatedRowHeight           = 174.0
        messaggeDetailedListingTable.estimatedSectionFooterHeight = 140.0
        messaggeDetailedListingTable.estimatedSectionHeaderHeight = 40.0
        messaggeDetailedListingTable.rowHeight                    = UITableViewAutomaticDimension
        messaggeDetailedListingTable.sectionFooterHeight          = UITableViewAutomaticDimension
        messaggeDetailedListingTable.sectionHeaderHeight          = UITableViewAutomaticDimension
        
        
        messaggeDetailedListingTable.scrollToRow(at: NSIndexPath(row: NSNotFound, section: 1) as IndexPath, at: .bottom, animated: false)
        
    }
    
    @objc private func showReplyScreen(){
        shouldShowReplyView = !shouldShowReplyView
        self.manageReply = false
        messaggeDetailedListingTable.reloadData()
        messaggeDetailedListingTable.scrollToRow(at: NSIndexPath(row: NSNotFound, section: 1) as IndexPath, at: .none, animated: false)
        
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
           return self.messageThreadArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "MessageDetailedCell") as! MessageDetailedCell
       cell.nameLabel.addTarget(self, action: #selector(self.showSenderList), for: .touchUpInside)
        
        let msg                    = messageThreadArray[indexPath.row]
        
        cell.alphabetLabel.isHidden = false
        cell.selectionStyle = .none
        if msg.name != "" {
         cell.alphabetLabel.text = msg.name.getFirstString()
        }
        else{
         cell.alphabetLabel.text = ""
        }
        if indexPath.row == 0 {
            cell.headingLabel.text     = msg.title
            cell.detailedLabel.text    = msg.content
            cell.nameLabel.setTitle(msg.name, for: UIControlState.normal)
            cell.designationLabel.text = msg.designation
            cell.dateLabel.text        = msg.date
            cell.forwardButtonHeight.constant = 19
            if isThisThreadStartedByMe {
                cell.messageAddedByMeLabel.text = "Message Added By Me"
            }else {
                cell.messageAddedByMeLabel.text = nil
            }
            cell.shouldSetUpForHeader(shouldSetupForHeader: true)
            if(firstTime == 0){
                firstTime=1
                
                for urlValue in msg.AudioArray{
                    var image = UIImage()
                    if urlValue.type == "Audio" {
                        image = UIImage(named: "audioIcon")!
                    }
                    
                    let imagv = CustomImageView(image: image)
                    
                    weak var weakImageVc  = imagv
                    ImageAPi.fetchImageForUrl(urlString: urlValue.url, oneSuccess: { (image) in
                        if weakImageVc != nil { weakImageVc!.image = image }
                        }, onFailure: { (error) in
                            
                    })
                    imagv.contentMode = .scaleAspectFit
                    imagv.url = urlValue.url
                    imagv.name = urlValue.name
                    imageArrayDetailed.append(imagv)
                    
                    let audioTap = UITapGestureRecognizer(target: self, action: #selector(self.loadAudio))
                    imagv.addGestureRecognizer(audioTap)
                    imagv.isUserInteractionEnabled = true
                }

            for urlString in msg.imageArray{
                let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
                
                weak var weakImageVc  = imagv
                ImageAPi.fetchImageForUrl(urlString: urlString, oneSuccess: { (image) in
                    if weakImageVc != nil { weakImageVc!.image = image }
                    }, onFailure: { (error) in
                        
                })
                imagv.contentMode = .scaleAspectFit
                imageArrayDetailed.append(imagv)
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageExpandedForMessage))
                imagv.addGestureRecognizer(tap)
                imagv.isUserInteractionEnabled = true
                }
                
                for urlString in msg.pdfArray{
                    var image = UIImage()
                    if urlString.type == "PDF" {
                        image = UIImage(named: "pdfIcon")!
                    }
                    if urlString.type == "DOC" || urlString.type == "DOCX"  {
                        image = UIImage(named: "wordIcon")!
                    }
                    let imagv = CustomImageView(image: image)
                    
                    weak var weakImageVc  = imagv
                    ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                        if weakImageVc != nil { weakImageVc!.image = image }
                        }, onFailure: { (error) in
                            
                    })
                    imagv.contentMode = .scaleAspectFit
                    imagv.url = urlString.url
                    imagv.name = urlString.name
                    imageArrayDetailed.append(imagv)
                    
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.loadPdf))
                    imagv.addGestureRecognizer(tap)
                    imagv.isUserInteractionEnabled = true
                    
                }
 
            }
            
            
            if imageArrayDetailed.count > 0 {
                cell.attachmentViewHeight.constant = cell.attachmentView.addImagesToMeFromArrayForDiaryDetailed(imageArray: imageArrayDetailed)
                //cell.attachmentViewHeight.constant = cell.attachmentViewHeight.constant + (cell.AttachmentView.constant)
                //cell.constant = cell.constant + cell.AttachmentView.constant
            }
            
        }else {
            cell.shouldSetUpForHeader(shouldSetupForHeader: false)
            cell.forwardButtonHeight.constant = 0
            cell.detailedLabel.text    = msg.content
            cell.nameLabel.setTitle(msg.name, for: UIControlState.normal)
            cell.designationLabel.text = msg.designation
            cell.dateLabel.text        = msg.date
            cell.messageAddedByMeLabel.text = nil
        }
        if indexPath.row == 1 {
            cell.shortUpSeparatorView.isHidden = true
        }
        if indexPath.row == messageThreadArray.count-1{
            cell.longSeparatorView.isHidden = true
        }
        
        
        
        
        weak var profileImage = cell.profileImageView
        weak var weakCell = cell
        ImageAPi.fetchImageForUrl(urlString: msg.senderProfilePic, oneSuccess: { (image) in
            if profileImage != nil {
                
                if let img = ImageAPi.getCircleImageFromImage(image: image){
                    weakCell!.profileImageView.image = img
                }
                profileImage!.image = image
                weakCell?.alphabetLabel.isHidden = true
            }
        }) { (error) in
            
        }
        
        cell.forwardButton.addTarget(self, action: #selector(MessageDetailedLisiting.forwardMessage), for: .touchUpInside)
        return cell
    }
    
    @objc func showImageExpandedForMessage(tapgesture:UITapGestureRecognizer) -> Void {
        let parentImageView =  tapgesture.view as! UIImageView
        
        let array = Bundle.main.loadNibNamed("PhotoShowingView", owner: self, options: nil)
        let photoView             = array![0] as! PhotoShowingView
        photoView.imageView.image = parentImageView.image
        photoView.frame           = self.view.frame
        self.view.addSubview(photoView)
    }
    
    @objc private func loadPdf(tapgesture:UITapGestureRecognizer) {
        let parentImageView =  tapgesture.view as! CustomImageView
        //moveToPdf = true
        pdfViewer.showPdfInViewController(vc: self, pdfHavingUrl: parentImageView.url, withName: parentImageView.name)
    }
    
    @objc private func loadAudio(tapgesture:UITapGestureRecognizer) {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        print("audio tapped")
        
        if NSURL(string: parentImageView.url) != nil {
            
            // then lets create your document folder url
             let documentsDirectoryURL =  FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileName = parentImageView.url
            if fileName.range(of: "opus") != nil{
                print("exists")
                replacedString = (fileName as NSString).replacingOccurrences(of: "opus", with: "mp3")
            }
            
            
            let fileUrl = NSURL(fileURLWithPath: replacedString as String)
            // lets create your destination file url
            destinationUrl = documentsDirectoryURL.appendingPathComponent(fileUrl.lastPathComponent ?? "audio.mp3") as NSURL
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager().fileExists(atPath: destinationUrl.path!) {
                print("The file already exists at path")
                self.setupAudioPlay()
                self.audioPlayView.toPass = self.destinationUrl
                self.audioPlayView.isFileExistAtLocal = true
                // if the file doesn't exist
            } else {
                
                self.audioPlayView.isFileExistAtLocal = false
                //let webServiceUrl = NSURL(string: webServiceUrls.baseURl+"convertAudioPlay/uniqueName/"+parentImageView.name)
                weak var weakSelf = self
                hudControllerClass.showNormalHudToViewController(viewController: self)
                WebServiceApi.getAudioPlayUrl(name: parentImageView.name,onSuccess: { (result) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    if let playUrl = result["convertAudioPlay"] as? String {
                        print("inside audioplay")
                        print(playUrl)
                        
                        //self.audioPlayView.downloadUrl = playUrl
                        //self.audioPlayView.destinationUrl = self.destinationUrl
                        let url = NSURL(string: playUrl)
                        self.setupAudioPlay()
                        self.downloadFileFromURL(url: url!)
                    }
                    
                    }, onFailure: { (error) in
                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                        AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                        
                }) { (progress) in
                    
                }
            }
            
        }
        
        // to check if it exists before downloading it
        //print(webServiceUrl!)
        
        
        
    }
    
    
    func downloadFileFromURL(url:NSURL){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { [weak self](URL, response, error) -> Void in
            guard URL != nil && error == nil else {
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                print("failed")
                return
            }
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self?.play(url: URL! as NSURL)
            })
        
        downloadTask.resume()
        
    }
    
    func play(url:NSURL) {
        
        do {
            try FileManager().moveItem(at: url as URL, to: self.destinationUrl as URL)
            
            print("File moved to documents folder")
            print(url)
            print(self.destinationUrl)
            self.audioPlayView.toPass = self.destinationUrl
            self.audioPlayView.destinationUrl = self.destinationUrl
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
        }
    }
    
    
    func setupAudioPlay() {
        let array = Bundle.main.loadNibNamed("audioPlay", owner: self, options: nil)
        audioPlayView             = array![0] as! audioPlay
        
        audioPlayView.center = CGPoint(x: self.view.frame.size.width  / 2,
                                       y: self.view.frame.size.height / 2);
        audioPlayView.frame           = self.view.frame
        audioPlayView.delegate = self
        audioPlayView.soundSlider.value = 0.0
        audioPlayView.playAudioButton.setImage(UIImage(named: "playIcon"), for: UIControlState())
        audioPlayView.initialLoad = true
        audioPlayView.playTimeText.text = "00.00"
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.view.addSubview(audioPlayView)
    }
    
    func exitSubView() {
        audioPlayView.removeFromSuperview()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let hdrWithBtn = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewWithButton") as! HeaderViewWithButton
            hdrWithBtn.sendButton.addTarget(self, action: #selector(MessageDetailedLisiting.showReplyScreen), for: .touchUpInside)
            
            
            if shouldHideRplyView {
              hdrWithBtn.sendButton.isHidden = true
              hdrWithBtn.sendReplyButtons.isHidden = true
            }
            else{
              hdrWithBtn.sendButton.isHidden = false
              hdrWithBtn.sendReplyButtons.isHidden = true
            }
            
        if !manageReply && !shouldHideRplyView {
            if shouldShowReplyView {
                hdrWithBtn.sendReplyButtons.isHidden = false
                hdrWithBtn.sendButton.isHidden = true
                hdrWithBtn.sendReplyButtons.addTarget(self, action: #selector(self.sendMessage), for:.touchUpInside)
            }else {
                hdrWithBtn.sendReplyButtons.isHidden = false
                
                hdrWithBtn.sendButton.isHidden = true
            }
        }
            
            return hdrWithBtn
        }
       return nil
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (section != 0) && (shouldShowReplyView) {
            
            let replyView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "messageSendingFooter") as! messageSendingFooter
            replyView.subjectTextField.isUserInteractionEnabled = false
            replyView.subjectTextField.text = heading
            self.msgSendingFooter = replyView
            return replyView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50.0
        }
        return 1.0
    }
    
   
    
    //MARK: common header protocol
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager || user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != corporateSchoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    //MARK: webService
    //TODO: Remove hard coded value from here..
    private func makeWebCallToGetThreadFromMessageWithId(idOfMessage:String){
        print("messge...")
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getMessagesInThreadWithId(idOfThread: idOfMessage, userRoleId: userRoleID,otherguyId: receipientID,readViaPush: readViaPush, onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            weakSelf!.messageThreadArray = parseMessageListing.parseThisDictionaryForMessages(dictionary: result)
            if self.messageThreadArray.count != 0 {
            self.manageReply = true
                if self.messageThreadArray[0].messageCanReply == true {
                    self.shouldHideRplyView = false
                }
                else{
                    //self.shouldShowReplyView = false
                    self.shouldHideRplyView = true
                }
            }
            weakSelf!.messaggeDetailedListingTable.reloadData()
            weakSelf!.messaggeDetailedListingTable.scrollToRow(at: IndexPath(row: NSNotFound, section: 1), at: .bottom, animated: false)
            
            
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }) { (progress) in
                
        }
    }
    
    
    
    
    //MARK:- Send message 
   
    @objc  private func sendMessage(sender:UIButton){
       
        
        weak var weakSelf = self
        
        let validationResut = validateBeforeSendingReply(sender: sender)
        if validationResut.0 {
            if user?.currentTypeOfUser == .guardian{
            schoolId = (user?.getSelectedEmployeeOrStudentschoolId())!
            }
            else{
                schoolId = String(user!.schoolId)
            }
            if schoolId != "" {
                
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.postReplyToMessageWithID(threadId: threadId,
                                                   titleOfMessage: validationResut.1!,
                                                   idOfSchool: schoolId,
                                                   corporateId:String(user!.corporateId),
                                                   listOfreciepient: receipientID,
                                                   addedUserId: userRoleID,
                                                   contentOfMessage: validationResut.2!,
                                                   rootmessageId: rootMessageId,
                                                   onSuccess: { (result) in
                                                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                                    //weakSelf?.makeWebCallToGetThreadFromMessageWithId((weakSelf?.threadId)!)
                                                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.messagePostingSucces, stateOfMessage: .success)
                                                    _ = weakSelf?.navigationController?.popViewController(animated: true)
                },
                                                   onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                   AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                },
                                                   duringProgress: { (progress) in
                                                    
            })
            }
        }
    }
    //MARK:- Validations 
    
    private func validateBeforeSendingReply(sender:UIButton)->(Bool,String?,String?){
        
        var result = false
       let rpyView =  self.msgSendingFooter!
        
        if rpyView.messageTextView.text.characters.count == 0{
            result = false
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.noMessageAlert, stateOfMessage: .failure)
        } else if rpyView.subjectTextField.text?.characters.count == 0 {
            result = false
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.noSujectAlert, stateOfMessage: .failure)
        }else {
            result = true
        }
        
        return (result,rpyView.subjectTextField.text,rpyView.messageTextView.text)
    }
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
       self.tabBarController?.selectedIndex = 2
       
    }
    
    @objc private func showSenderList()
    {
        if receipientID.isEmpty
        {
        
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            
            WebServiceApi.getRecipientsForMessage(rootmessageId: rootMessageId, onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                let rst = userModel.makeArrayOfUserFromRecipientDictionary(dicto: result)
                if rst.1 != nil{
                    AlertController.showToastAlertWithMessage(message: (rst.1?.localizedDescription)!, stateOfMessage: .failure)
                    
                }else {
                    weakSelf!.showMessageSenderScreenWithUsers(userslistArray: rst.0)
                }
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }) { (progress) in
                
            }
        }
    }
    @objc private func forwardMessage()
    {
        self.performSegue(withIdentifier: segueConstants.segueForForwardingMessage, sender: nil)
    }
    
    private func showMessageSenderScreenWithUsers(userslistArray:[userModel]) {
        
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msMultiRecipVC = storyboard.instantiateViewController(withIdentifier: "MessageMultiSendPopupVC") as! MessageMultiSendPopupVC;
        msMultiRecipVC.usersArray = userslistArray
        prepareOverlayVC(overlayVC: msMultiRecipVC)
        
        self.present(msMultiRecipVC, animated: false, completion: nil)
        
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueForForwardingMessage {
            hudControllerClass.showNormalHudToViewController(viewController: self)
            let composeMsg        = segue.destination as! ComposeMessageVC
            composeMsg.userID     = (user?.currentUserRoleId)!
            let msg               = messageThreadArray[0]
            composeMsg.msgTitle   = msg.title
            composeMsg.msgContent = msg.content
            composeMsg.messageDetailsSelf = self
            imageAttachmentArray.removeAll()
            documentAttachmentArray.removeAll()
            for attachment in msg.attachmentArray{
                
                if attachment.attachmentType == "Audio" {
                    documentAttachmentArray.append(attachment)
                }
                else if attachment.attachmentType == "Image"{
                        imageAttachmentArray.append(attachment)
                }else if attachment.attachmentType == "DOCX" || attachment.attachmentType == "PDF"{
                        documentAttachmentArray.append(attachment)
                }
                
            }
            if imageAttachmentArray.count > 0{
                composeMsg.uploadAttachmentArray = imageAttachmentArray
            }
            if documentAttachmentArray.count > 0{
                composeMsg.documentAttachmentArray = documentAttachmentArray
            }
            composeMsg.uploadAttachmentFailure = true
            composeMsg.forwardingMsg = true
            if user?.currentTypeOfUser == .guardian{
                composeMsg.studentID  = (user?.Students![(user?.currentOptionSelected)!].id)!
                //composeMsg.schoolID   = (user?.Students![(user?.currentOptionSelected)!].schoolId)!
                composeMsg.profilePic = (user?.Students![(user?.currentOptionSelected)!].profilePic)!
            }
            else{
                composeMsg.profilePic = (user?.profilePic)!
                
            }
            
            //self.dismissViewControllerAnimated(false, completion: nil)
        }
    }
    
    
}
