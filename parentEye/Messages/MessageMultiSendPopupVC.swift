//
//  MessageMultiSendPopupVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/06/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class MessageMultiSendPopupVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var multiSendPopupTable: UITableView!
    @IBOutlet var multiSendRecipients: UILabel!
    
    var usersArray = [userModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        multiSendPopupTable.register(UINib(nibName: "MessageMultiSendCell", bundle: nil), forCellReuseIdentifier: "MessageMultiSendCell")
        multiSendPopupTable.estimatedRowHeight = 76.0
        multiSendPopupTable.rowHeight          = UITableViewAutomaticDimension
        multiSendPopupTable.dataSource = self
        multiSendPopupTable.delegate = self
        multiSendRecipients.text = String(usersArray.count)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return usersArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageMultiSendCell") as! MessageMultiSendCell
        
        let user = usersArray[indexPath.row]
        
        cell.nameofUser.text        = user.username
        cell.designationofUser.text = user.userDesignation
        weak var profileImage = cell.profilepicofUser
        
        if user.profilePic == ""{
            cell.profilepicofUser.image = UIImage(named: "avatar")
            cell.profilepicofUser.layer.masksToBounds = true
            cell.profilepicofUser.layer.cornerRadius = 0
            cell.profilepicofUser.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: user.profilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.profilepicofUser.layer.masksToBounds = false
                    cell.profilepicofUser.layer.cornerRadius = cell.profilepicofUser.frame.height/2
                    cell.profilepicofUser.clipsToBounds = true
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        
        
        return cell
    }

    
    
    @IBAction func closePopup(_ sender: Any) {
    
    self.dismiss(animated: false, completion: nil)

    }
    
   
}
