//
//  MessageAddReciepientsCell.swift
//  parentEye
//
//  Created by Martin Jacob on 14/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class MessageAddReciepientsCell: UITableViewCell {

    var isTeacherSelected = false
    @IBOutlet weak var selectionButton:UIButton?
    @IBOutlet weak var nameOfUser:UILabel!
    @IBOutlet weak var designationOfUser:UILabel!
    @IBOutlet weak var profilePictureOfUser:UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.profilePictureOfUser?.image = UIImage(named: "avatar")
        self.nameOfUser?.text = ""
        self.designationOfUser?.text = ""
        
    }

    //function to set the value externally
    func setTeacherSelected(selected:Bool){
        isTeacherSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isTeacherSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: .normal)
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: .normal)
        }
        
        isTeacherSelected = !isTeacherSelected
    }
    
    
}
