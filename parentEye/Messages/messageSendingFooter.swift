//
//  messageSendingFooter.swift
//  parentEye
//
//  Created by Martin Jacob on 02/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class messageSendingFooter: UITableViewHeaderFooterView {

    @IBOutlet weak var subjectLabel:UILabel!
    @IBOutlet weak var subjectTextField:UITextField!
    @IBOutlet weak var messageLabel:UILabel!
    @IBOutlet weak var messageTextView:UITextView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    
    
}
