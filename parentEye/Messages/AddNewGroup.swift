//
//  AddNewGroup.swift
//  parentEye
//
//  Created by Vivek on 13/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

protocol passGroupIdProtocol:class {
    func passgroupIdFromGroupVC(groupId:String,groupName:String)
}

class AddNewGroupVC : UIViewController,CommonHeaderProtocol,popOverDelegate,UITableViewDelegate,UITableViewDataSource,UIPopoverPresentationControllerDelegate,messageAddRecipientsProtocol,DiaryComposeSchoolListProtocol,UITextFieldDelegate {
    
    @IBOutlet weak var header: CommonHeader!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var permissionView: ExpansiveView!
    @IBOutlet weak var groupMemberTableView: UITableView!
    @IBOutlet weak var permissionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolNameTextFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolNameTextField: UITextField!
    @IBOutlet weak var allSchoolButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var schoolLabelHeight: NSLayoutConstraint!
    
    var profilePic = ""
    var user:userModel?
    var permissionList = [String]()
    var studentID                    = ""
    var userID                       = ""
    var selectedGroupName            = ""
    var groupId                      = ""
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    lazy var messageReciepientArray  = [userModel]()//Array to store all the receipients
    lazy var schoolListArray = [corporateManagerSchoolModel]()
    lazy var schoolRecipientListArray = [corporateManagerSchoolModel]()
    var defaultSchoolDisplay:Bool = false
    
    
    weak var delegate:passGroupIdProtocol?
    
    override func viewDidLoad() {
        
        getUserDataFromDB()
        setupUI()
        alterHeader()
        setupTable()
        loadDynamicPermissionList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
 
    }
    private func setupUI () {
        groupNameTextField.layer.borderWidth = CGFloat(1)
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        groupNameTextField.layer.borderColor = color.cgColor
        
        groupNameTextField.text = selectedGroupName
        
        if user!.currentTypeOfUser == .employee {
            schoolNameTextFieldHeight.constant = 0
            allSchoolButtonHeight.constant = 0
            schoolLabelHeight.constant = 0
        }
        else{
            schoolNameTextFieldHeight.constant = 30
            allSchoolButtonHeight.constant = 25
            schoolLabelHeight.constant = 20
        }
        if user?.currentTypeOfUser == .CorporateManager {
            schoolNameTextField.text = user?.schools![(user?.currentSchoolSelected)!].schoolName
            schoolNameTextField.delegate = self
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
            
        }
    }
    
    private func alterHeader() {
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.addNewGroup, withProfileImageUrl: profilePic)
        if user?.currentTypeOfUser == .CorporateManager {
            header.multiSwitchButton.setTitle("", for: .normal)
        }
        header.delegate = self
    }
    
    private func setupTable() -> Void {
        
        groupMemberTableView.register(UINib(nibName: "GroupMembersListingCell", bundle: nil), forCellReuseIdentifier: "GroupMembersListingCell")
        groupMemberTableView.estimatedRowHeight = 68.0
        groupMemberTableView.rowHeight = UITableViewAutomaticDimension
    }
    //MARK:- commonHeader delegate functions
    
    func showHamburgerMenuOrPerformOther() {
        if groupNameTextField.text == "" {
         weak var weakSelf = self
         _ = weakSelf?.navigationController?.popViewController(animated: true)
        }
        else{
            showAlert()
        }
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        
        let user = DBController().getUserFromDB()
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    //MARK: -UITextfield deleagte to dismiss keyboard
    
    func textFieldShouldBeginEditing(groupNameTextField: UITextField) -> Bool {
        groupNameTextField.resignFirstResponder()
        return true;
    }
    func textFieldShouldendEditing(groupNameTextField: UITextField) -> Bool {
        groupNameTextField.resignFirstResponder()
        return true;
    }
    
    @IBAction func showSchoolListPopUp(_ sender: UITextField) {
  schoolNameTextField.endEditing(true)
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "DiaryComposeSchoolListPopUpVC") as! DiaryComposeSchoolListPopUpVC ;
        schoolListArray = (user?.schools)!
        if defaultSchoolDisplay == true{
            schoolRecipientListArray.removeAll()
            schoolRecipientListArray.append((user?.schools![(user?.currentSchoolSelected)!])!)
        }
        
        
        if schoolNameTextField.text == "All"{
            schoolRecipientListArray = schoolListArray
        }
        
        msRecipVC.recipientArray = schoolListArray
        msRecipVC.searchrecipientArray = schoolListArray
        msRecipVC.previousEntryArray = schoolRecipientListArray
        defaultSchoolDisplay = false
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
    }
    
    @IBAction func AllSchoolSelectedAction(_ sender: UIButton) {
        schoolNameTextField.text = "All"
        schoolRecipientListArray = (user?.schools)!
    }
    func  updateSelectedWithSchoolArray(receipientArray recipientArray: [corporateManagerSchoolModel]) {
        schoolNameTextField.text = ""
        if recipientArray.count == 0{
            schoolNameTextField!.text = ""
        }
            
        else if recipientArray.count == 1{
            schoolNameTextField!.text = recipientArray[0].schoolName
        }
        else if recipientArray.count == 2{
            schoolNameTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName
        }
        else if recipientArray.count == user?.Classes?.count{
            schoolNameTextField!.text = "All"
        }
        else {
            schoolNameTextField!.text = recipientArray[0].schoolName+","+recipientArray[1].schoolName+","+recipientArray[2].schoolName+" + "+String(recipientArray.count - 2)
            
        }
        schoolRecipientListArray.removeAll()
        schoolRecipientListArray = recipientArray
        if schoolRecipientListArray.count == user?.schools?.count {
            schoolNameTextField.text = "All"
        }
        
    }

    private func loadDynamicPermissionList()
    {
        let currentUserRoleId = user?.currentUserRoleId
        WebServiceApi.getEligibleRolesForSendingMessageWithoutGroups(userRoleId: currentUserRoleId!, onSuccess: { (result) in
            self.permissionList = self.makeArrayOfRolesFromDictionary(dicto: result)
            if self.permissionList.count != 0 {
                self.createDynamicButtons(permission: self.permissionList)
                
            }else {
                
                self.permissionView.removeFromSuperview()
            }
            }, onFailure: { (error) in
                
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    private func makeArrayOfRolesFromDictionary(dicto:[String:AnyObject]) -> [String]{
        var roleArray = [String]()
        if let userArray = dicto[resultsJsonConstants.messageResultContants.messageRecipientRoles] as? [AnyObject]{
            for eachRole in userArray{
                roleArray.append(eachRole as! String)
                
            }
        }
        return roleArray
    }
    
    func createDynamicButtons(permission:[String])
    {
        var x = 0
        var width:CGFloat = 0.0
        var length:Int = 0
        var y:Int = 0
        let height:Int = 30
        let bounds = UIScreen.main.bounds
        let screenWidth = bounds.size.width
        var permissionHeight:Int = 39
        
        for i in 0 ..< permission.count {
            
            length = permission[i].characters.count
            width = CGFloat(length) * 11
            
            
            if width > screenWidth - CGFloat(x) {
                x = 0
                y = y + (height + 10)
                permissionHeight = permissionHeight + 30
                
            }
            
            let button = UIButton(frame: CGRect(x: x, y: y, width: Int(width), height: height))
            button.backgroundColor = .white
            button.setTitleColor(UIColor.black, for: UIControlState.normal)
            button.setTitle(permission[i], for: .normal)
            button.tag = i
            button.addTarget(self, action: #selector(webServiceToGetRecipient), for: .touchUpInside)
            self.permissionView.addSubview(button)
            x = x + (Int(width)+5)
            length = 0
            
            
        }
        
        permissionViewHeight.constant = CGFloat(permissionHeight)
    }
    @objc func webServiceToGetRecipient(sender:UIButton){
        var recipientRole = ""
        var userType = ""
        var schoolListString = ""
        for i in 0 ..< permissionList.count {
            if i == sender.tag{
                recipientRole = permissionList[i]
                break
            }
            
        }
        if user?.currentTypeOfUser == .CorporateManager {
            userType = "CorporateManager"
        }
        else{
            userType = "Employee"
        }
        if user?.currentTypeOfUser == .CorporateManager {
            if schoolRecipientListArray.count > 0 {
                for schoolArray in schoolRecipientListArray{
                    schoolListString = schoolListString+String(schoolArray.schoolId) + ","
                }
                schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString.endIndex) )
            }
            else{
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.selectSchool, stateOfMessage:.failure)
            }
        }
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        WebServiceApi.getEligibleRecipientsForSendingMessage(recipientRole: recipientRole,userRoleId:(user?.currentUserRoleId)!,userType: userType,corporateId: String(describing: user?.corporateId),schoolIdList: schoolListString,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            let rst = userModel.makeArrayOfUserFromRecipientDictionary(dicto: result)
            if rst.1 != nil{
                AlertController.showToastAlertWithMessage(message: (rst.1?.localizedDescription)!, stateOfMessage: .failure)
                
            }else {
                weakSelf!.showRecipientSelectionScreenWithUsers(usersArray: rst.0)
            }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: (error.localizedDescription), stateOfMessage: .failure)
            }, duringProgress: { (progress) in
                
            }, forStudentID: studentID)
        
    }
    
    private func showRecipientSelectionScreenWithUsers(usersArray:[userModel]) {
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let msRecipVC = storyboard.instantiateViewController(withIdentifier: "MessageAddReceipientsVC") as! MessageAddReceipientsVC;
        msRecipVC.recipientArray = usersArray
        msRecipVC.searchrecipientArray = usersArray
        msRecipVC.previousEntryArray = messageReciepientArray
        msRecipVC.delegate = self
        prepareOverlayVC(overlayVC: msRecipVC)
        
        self.present(msRecipVC, animated: false, completion: nil)
        
    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
    }
    
    func updateSelectedWithArray(previousArray receipientArray: [userModel],usersArray: [userModel]) {
        messageReciepientArray.removeAll()
        messageReciepientArray = receipientArray
        groupMemberTableView.reloadData()
    }
    func editButtonTapped() {
        
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return messageReciepientArray.count
       
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMembersListingCell") as! GroupMembersListingCell
        cell.groupMemberName.text = messageReciepientArray[indexPath.row].username
        cell.designation.text = messageReciepientArray[indexPath.row].userDesignation
        weak var profileImage = cell.profilePic
        
        if messageReciepientArray[indexPath.row].profilePic == ""{
            cell.profilePic.image = UIImage(named: "avatar")
            cell.profilePic.layer.masksToBounds = true
            cell.profilePic.layer.cornerRadius = 0
            cell.profilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: messageReciepientArray[indexPath.row].profilePic, oneSuccess: { (image) in
                if profileImage != nil {
                    
                    cell.profilePic.layer.masksToBounds = false
                    cell.profilePic.layer.cornerRadius = cell.profilePic.frame.height/2
                    cell.profilePic.clipsToBounds = true
                    
                    profileImage!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        
        
        cell.selectionStyle = .none
        cell.selectionButton.tag = indexPath.row
        cell.selectionButton.addTarget(self, action: #selector(self.removeSelectedUser), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc private func removeSelectedUser(sender:UIButton) {
        for pos in 0 ..< messageReciepientArray.count {
            if sender.tag == pos {
                messageReciepientArray.remove(at: pos)
            }
        }
        groupMemberTableView.reloadData()
    }
    
    @IBAction func saveGroup(_ sender: UIButton) {
        let result = validateFields()
        if result.0 {
            
        var memberIdListString = ""
        var schoolListString = ""
            var userType = ""
            
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
            
        if user?.currentTypeOfUser == .CorporateManager {
             for schoolArray in schoolRecipientListArray{
               schoolListString = schoolListString+String(schoolArray.schoolId) + ","
             }
               schoolListString = schoolListString.substring(to: schoolListString.characters.index(before: schoolListString.endIndex) )
        }
            
        for id in messageReciepientArray {
            memberIdListString = memberIdListString+id.userId+","
        }
        if messageReciepientArray.count != 0 {
            memberIdListString = memberIdListString.substring(to: memberIdListString.characters.index(before: memberIdListString.endIndex) )
        }
        
        if user?.currentTypeOfUser == .employee{
         userType = "Employee"
        }
        else{
         userType = "CorporateManager"
        }
            
        WebServiceApi.saveOrEditNewGroup(groupName: groupNameTextField.text!,
                                         userRoleId: (user?.currentUserRoleId)!,
                                         memberIdList: memberIdListString,
                                         groupId: groupId,
                                         schoolIdListString: schoolListString,
                                         corporateId: String(describing: user?.corporateId),
                                         userType: userType,
                                           onSuccess: { (result) in
                                            
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            print(result)
                                            if (result["group"] as? [String:AnyObject]) != nil{
                                                //AlertController.showToastAlertWithMessage(ConstantsInUI.messagePostingSucces, stateOfMessage: .success)
                                                if let resultValue = result["group"] as? [String:AnyObject] {
                                                    var groupsId:String = ""
                                                    var groupsName:String = ""
                                                    if self.selectedGroupName != "" {
                                                        groupsId = self.groupId
                                                    }
                                                    
                                                    if let Id = resultValue["id"] as? String {
                                                        groupsId = Id
                                                        
                                                    }
                                                    if let Id = resultValue["id"] as? Int {
                                                        groupsId = String(Id)
                                                        
                                                    }
                                                    if let Name = resultValue["name"] as? String {
                                                        groupsName = Name
                                                    }
                                                    self.delegate?.passgroupIdFromGroupVC(groupId: groupsId,groupName: groupsName)
                                                }
                                                _ = weakSelf!.navigationController?.popViewController(animated: true)
                                                
                                            }else{
                                                //AlertController.showToastAlertWithMessage(ConstantsInUI.messagePostingFailure, stateOfMessage: .failure)
                                            }
                                            
                                            
                                            
                                            
        },
                                           onFailure: { (error) in
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                            
        },
                                           duringProgress: { (progress) in
                                            
        })
        }
        else{
            AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
        }
    }
    
    private func validateFields() ->(Bool,String){
        var result   = true
        var ErrorMsg = ""
        
        if  schoolNameTextField.text == "" && user?.currentTypeOfUser == .CorporateManager {
            result = false
                ErrorMsg = ConstantsInUI.selectSchool
        }
        else if groupNameTextField.text == ""{
            result   = false
            ErrorMsg = ConstantsInUI.enterGroupName
        } else if messageReciepientArray.count == 0 {
            result   = false
            ErrorMsg = ConstantsInUI.addmembersToGroup
        } 
        
        return (result,ErrorMsg)
    }
    
    private func showAlert(){
        weak var weakSelf = self
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            _ = weakSelf?.navigationController?.popViewController(animated: true)
            
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
}
