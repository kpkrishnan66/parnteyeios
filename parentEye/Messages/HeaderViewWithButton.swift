//
//  HeaderViewWithButton.swift
//  parentEye
//
//  Created by Martin Jacob on 03/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class HeaderViewWithButton: UITableViewHeaderFooterView {

    
    @IBOutlet weak var sendButton:UIButton!
    @IBOutlet weak var sendReplyButtons:UIButton!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
