//
//  GroupMembersListingCell.swift
//  parentEye
//
//  Created by Vivek on 17/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class GroupMembersListingCell:UITableViewCell {
    
    @IBOutlet weak var groupMemberName: UILabel!
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var selectionButton: UIButton!
    @IBOutlet weak var designation: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.profilePic?.image = UIImage(named: "avatar")
        self.groupMemberName?.text = ""
        self.designation?.text = ""
        
    }
}
