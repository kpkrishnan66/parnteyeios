//
//  MessagesListingVC.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

class MessagesListingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,popOverDelegate,SelectedSchoolpopOverDelegate{

    
    @IBOutlet weak var messageListingTable:UITableView!
    @IBOutlet weak var header:CommonHeader!
    
    @IBOutlet weak var showMoreMessageBtn: UIButton!
    @IBOutlet weak var showMoreMessageCountLabel: UILabel!
    //    @IBOutlet weak var messageViewMoreBtn: UIButton!
    //    @IBOutlet weak var messageViewMoreBtn: UIButton!
    /// Array for storing the full list of messages
    lazy var messageListingArray = [messageForListing]()
        /// Aray for storing the search results
    lazy var filteredMessageLstingArray = [messageForListing]()
    var page = 0
    var searching = false
    var readViaPush:Bool = false
    var messageComposeOrDetailed:Bool = false
    var schoolId:Int = 0
    var user:userModel?
    var messageMonthLimit = "1"
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    //MARK: - View delegate methods
    
    override func viewDidLoad() {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
        setupTable()
        getUserDataFromDB()
    }
    private func shouldGotoNextScreen() ->(Bool,messageForListing?){
        let tabBar = self.tabBarController as! ParentEyeTabbar
        let result = tabBar.shouldLoadMessage
        tabBar.shouldLoadMessage = false
        return (result,tabBar.messageToBeLoaded)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        let result = shouldGotoNextScreen()
        if result.0 {
            readViaPush = true
            performSegue(withIdentifier: segueConstants.segueForShowingDetailedMessage, sender: result.1)
        }else {
            readViaPush = false
            clearDataForThisPage()
            getUserDataFromDB()
            if messageComposeOrDetailed == false{
                if user?.currentTypeOfUser == .guardian {
                    Analytics.logEvent("P_homepage_message", parameters: nil)
                }
                else{
                 FireBaseKeys.FirebaseKeysRuleThree(value: "homepage_message")
                }
            }
            else{
              messageComposeOrDetailed = false
            }

            if user?.currentTypeOfUser == .CorporateManager {
             schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
            }
            makeWebApiCallToGetMessages()
            setUpHeader()
           let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
           tab.getUnreadCount()
        }
    }
    

    private func setUpHeader (){
        
        header!.delegate = self
        if user?.currentTypeOfUser == .guardian{
            header.setUpHeaderForRootScreensWithNameOfScreen(name: ConstantsInUI.messageScreenHeading, withProfileImageUrl: user!.getSelectedEmployeeOrStudentProfileImageUrl())
        }
        else{
            if user?.currentTypeOfUser == .employee {
                if user!.switchAs == ""{
                    header.setUPHeaderForEmployee(name: ConstantsInUI.messageScreenHeading)
                }
                else if user!.switchAs == "Guardian"{
                    header.setUPHeaderForMultiRole(name: ConstantsInUI.messageScreenHeading)
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                header.setupHeaderForCorporateManager(name: (user?.getStudentCurrentlySelectedSchool()?.schoolName)!)
                
            }
        }
        
        
    }
    
    
    
     //MARK:Table functions 
    
    private func setupTable() -> Void {
        messageListingTable.register(UINib(nibName: "MessageListingCell", bundle: nil), forCellReuseIdentifier: "messageCell")
        messageListingTable.estimatedRowHeight = 79.0
        messageListingTable.rowHeight = UITableViewAutomaticDimension
        
        let bottomRefreshControl                         = UIRefreshControl()
        bottomRefreshControl.triggerVerticalOffset       = 100.0
        bottomRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromBottomRefresh), for: .valueChanged)
        self.messageListingTable.bottomRefreshControl = bottomRefreshControl
        
        let topRefreshControl                   = UIRefreshControl()
        topRefreshControl.triggerVerticalOffset = 100.0
        topRefreshControl.addTarget(self, action: #selector(self.loadWebServiceFromTopRefreshControl), for: .valueChanged)
        messageListingTable.addSubview(topRefreshControl)
        
    }

    
    //MARK: Table view delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredMessageLstingArray.count > 0 {
         return filteredMessageLstingArray.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "messageCell") as! MessageListingCell
        makeCell(cell: cell, row: indexPath.row)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let msg = filteredMessageLstingArray[indexPath.row]
        
        self.performSegue(withIdentifier: segueConstants.segueForShowingDetailedMessage, sender: msg)
    }
    
    
    /**
     Function to alter the cell
     
     - parameter cell:  cell
     - parameter index: index of the cell
     */
    private func makeCell(cell:MessageListingCell, row index:Int){
        var teacherNameAndDesignationLabelcolor:UIColor
        let msg = filteredMessageLstingArray[index]
        cell.replyLabelWidth.constant = 0
        
        if msg.AudioArray.count > 0 {
            cell.seperatorView.isHidden = false
            cell.attachmentImage.isHidden = false
            cell.attachmentImage.image = UIImage(named: "audioAttachmentIcon")
            cell.attachmentImageWidth.constant = 15
        }
        else if msg.imageArray.count>0 || msg.pdfArray.count>0 {
            cell.seperatorView.isHidden = false
            cell.attachmentImage.isHidden = false
            cell.attachmentImage.image = UIImage(named: "attachmentImage")
            cell.attachmentImageWidth.constant = 10
        }
        else{
            cell.seperatorView.isHidden = true
            cell.attachmentImage.isHidden = true
            cell.attachmentImageWidth.constant = 0
        }
        
        let fnt = UIFont(name: "Roboto-Bold", size: cell.teacherNameAndDesignationLabel.font.pointSize)
        if msg.isRead{
        teacherNameAndDesignationLabelcolor  = UIColor.black
        }
        else{
        teacherNameAndDesignationLabelcolor = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
            
        }
        
        let string             = StringClassAttri(withString: msg.toFromLabel, andAttributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue): fnt!,NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):teacherNameAndDesignationLabelcolor])

        let fontForDesignation = UIFont(name: "Roboto-Bold", size: cell.teacherNameAndDesignationLabel.font.pointSize)
        let completeString     = "\(msg.name)"
        let desigString        = StringClassAttri(withString: completeString, andAttributes: [NSAttributedStringKey(rawValue: NSAttributedStringKey.font.rawValue):fontForDesignation!,NSAttributedStringKey(rawValue: NSAttributedStringKey.foregroundColor.rawValue):teacherNameAndDesignationLabelcolor])
            
            cell.teacherNameAndDesignationLabel.attributedText = String().createAttributedStringFromArray(stringArray: [string,desigString])
           
       
        if msg.isRead{
           cell.messageSubjectLabel.font  = UIFont(name: "Roboto-Regular", size: cell.messageSubjectLabel.font.pointSize)
           cell.dateIssuedLabel.textColor = UIColor.black
        }else {
           cell.messageSubjectLabel.font  = UIFont(name: "Roboto-Bold", size: cell.messageSubjectLabel.font.pointSize)
           cell.dateIssuedLabel.textColor = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
        }
        
        if msg.senderProfilePic == ""{
            cell.alphabetLabel.isHidden = false
            if msg.name != ""  {
              cell.alphabetLabel.text = msg.name.getFirstString()
            }
            else{
              cell.alphabetLabel.text = ""
            }
         cell.profileImage.image = UIImage(named: "circleImage")
        }
       else{
        
        weak var profileImage = cell.profileImage
        ImageAPi.fetchImageForUrl(urlString: msg.senderProfilePic, oneSuccess: { (image) in
            if profileImage != nil {
                cell.alphabetLabel.text = ""
                cell.alphabetLabel.isHidden = true
                profileImage!.image = image
                
            }
            else{
                cell.alphabetLabel.isHidden = false
                if msg.name != ""  {
                  cell.alphabetLabel.text = msg.name.getFirstString()
                }
                else{
                  cell.alphabetLabel.text = ""
                }
                cell.profileImage.image = UIImage(named: "circleImage")
            }
            }) { (error) in
                cell.alphabetLabel.isHidden = false
                if msg.name != ""  {
                 cell.alphabetLabel.text = msg.name.getFirstString()
                }
                else{
                  cell.alphabetLabel.text = ""
                }
                cell.profileImage.image = UIImage(named: "circleImage")
 
        }
        }
        
        cell.dateIssuedLabel.text     = msg.date
        cell.messageContentLabel.text = msg.content
        cell.messageSubjectLabel.text = msg.title
        
        
    }
    
    //MARK:- Navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueForShowingDetailedMessage{
            let messageDetailed                  = segue.destination as! MessageDetailedLisiting
            let msg                               = sender as! messageForListing


            FireBaseKeys.FirebaseKeysRuleTwo(value: "message_messageView")


            messageDetailed.threadId              = msg.threadId
            messageDetailed.heading               = msg.title
            messageDetailed.initialMessage        = msg.content
            messageDetailed.nameOfSender          = msg.name
            messageDetailed.designationOfSender   = msg.designation
            messageDetailed.dateOfInitialMessage  = msg.date
            messageDetailed.receipientID          = msg.theOtherGuysId
            messageDetailed.rootMessageId         = msg.ID
            messageDetailed.messageType           = msg.messageType
            messageDetailed.masterMessageSenderId = msg.masterMessageSenderId
            messageDetailed.userRoleID            = (user?.currentUserRoleId)!
            messageDetailed.readViaPush           = readViaPush
            print("other guy id***"+msg.theOtherGuysId+"userRoleID***"+(user?.currentUserRoleId)!)
            /*if msg.masterMessageSenderId == (user?.currentUserRoleId)!{
            messageDetailed.isThisThreadStartedByMe = true
            }*/

            messageComposeOrDetailed = true
            /*messageDetailed.schoolId             = (user?.Students![(user?.currentOptionSelected)!].schoolId)! */
            if user?.currentTypeOfUser == .guardian{
            messageDetailed.personImage          = (user?.Students![(user?.currentOptionSelected)!].profilePic)!
            }
            else{
            messageDetailed.personImage          = (user?.profilePic)!
            }
            
        } else if segue.identifier == segueConstants.segueForShowingComposeMessage {
            let composeMsg        = segue.destination as! ComposeMessageVC
            
            FireBaseKeys.FirebaseKeysRuleTwo(value: "message_compose")
            
            composeMsg.userID     = (user?.currentUserRoleId)!
            messageComposeOrDetailed = true
            if user?.currentTypeOfUser == .guardian{
            composeMsg.studentID  = (user?.Students![(user?.currentOptionSelected)!].id)!
            //composeMsg.schoolID   = (user?.Students![(user?.currentOptionSelected)!].schoolId)!
            composeMsg.profilePic = (user?.Students![(user?.currentOptionSelected)!].profilePic)!
            }
            else{
            composeMsg.profilePic = (user?.profilePic)!
             
            }
        }
    }
    
    
    //MARK:-IBACTIONs
    
    @IBAction func showComposeMessageUI(){
    
        self.performSegue(withIdentifier: segueConstants.segueForShowingComposeMessage, sender: nil)
        
    }
    
   
     //MARK: Webservice calls
    
    
    @IBAction func messageViewMoreBtnAction(_ sender: UIButton) {
        
        if showMoreMessageBtn.titleLabel?.text == "View More"{
            self.showMoreMessageBtn.setTitle("View All", for: .normal)
            self.showMoreMessageCountLabel.text = "60 Days"
            messageMonthLimit = "2"
            clearDataForThisPage()
            self.makeWebApiCallToGetMessages()
        }
        else if showMoreMessageBtn.titleLabel?.text == "View All"{
            self.showMoreMessageBtn.setTitle("View Less", for: .normal)
            self.showMoreMessageCountLabel.text = "All"
            messageMonthLimit = "All"
            clearDataForThisPage()
            self.makeWebApiCallToGetMessages()
        }
        else if showMoreMessageBtn.titleLabel?.text == "View Less"{
            self.showMoreMessageBtn.setTitle("View More", for: .normal)
            self.showMoreMessageCountLabel.text = "30 Days"
            messageMonthLimit = "1"
            clearDataForThisPage()
            self.makeWebApiCallToGetMessages()
        }
        
    }
    
    
    
    @objc private func loadWebServiceFromBottomRefresh(){
        messageListingTable.bottomRefreshControl!.endRefreshing()
        self.makeWebApiCallToGetMessages()
    }
    @objc private func loadWebServiceFromTopRefreshControl(refreshControl:UIRefreshControl){
        refreshControl.endRefreshing()
        clearDataForThisPage()
        self.makeWebApiCallToGetMessages()
    }
    
    
    private func makeWebApiCallToGetMessages(){
        
         weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        let  userID = user?.currentUserRoleId
        
        WebServiceApi.getMessagesForPage(page: page, forUserWithId: userID!,monthLimit: messageMonthLimit ,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)

            weakSelf!.addEntriesToArrayAndReload(array: parseMessageListing.parseThisDictionaryForMessages(dictionary: result))
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
            }) { (progress) in
                
        }
    }
    
    /**
     Function to get user data from DB
     */
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        self.tabBarController?.slideMenuController()?.openLeft()
    }
    func showSwitchSibling(fromButton: UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        if schoolId != user?.schools![(user?.currentSchoolSelected)!].schoolId {
            clearDataForThisPage()
            makeWebApiCallToGetMessages()
        }
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        clearDataForThisPage()
        makeWebApiCallToGetMessages()
        
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
    }
    
    //MARK:- Array
    
    private func addEntriesToArrayAndReload(array:[messageForListing]){
        
        filteredMessageLstingArray.append(contentsOf: array)
        messageListingArray.append(contentsOf: array)
        page = getUnreadAndReadMessage().total
        messageListingTable.reloadData()
        let noOfUnreadRead = getUnreadAndReadMessage()
        if noOfUnreadRead.unRead > DBController().getMessageUnreadNumber(){
            DBController().updateMessageUnread(String(noOfUnreadRead.unRead))
           (self.navigationController?.tabBarController as! ParentEyeTabbar).setMessageReadNumber()
        }
    }
    
    private func getUnreadAndReadMessage()-> (total:Int,unRead:Int) {
        
        let readArray =  filteredMessageLstingArray.filter({$0.isRead})
        
        let total = filteredMessageLstingArray.count
        let unread = filteredMessageLstingArray.count - readArray.count
        return(total,unread)
    }
    
    //MARK:- micellanious
    /**
     Clears data in this page
     */
    private func clearDataForThisPage(){
        
        filteredMessageLstingArray.removeAll()
        messageListingArray.removeAll()
        messageListingTable.reloadData()
        setUpHeader()
        page = 0
    }
    
    /**
     private function to find out if call of web service is needed
     
     - returns: returns whether web service call is nessesary
     */
    private func shouldCallWebService () ->Bool{
        var shoulCallWebSevice = false
        
        if user != nil {
            // checcking if we have changed users student or employee some where else.
            if  let newUer = DBController().getUserFromDB(){
                if user?.currentOptionSelected != newUer.currentOptionSelected{
                    shoulCallWebSevice = true
                }else  if page == 0 {
                    shoulCallWebSevice = true
                }else if newUer.Students![newUer.currentOptionSelected].profilePic != user!.Students![newUer.currentOptionSelected].profilePic {
                    shoulCallWebSevice = true
                }
            }
        }
        return shoulCallWebSevice
    }
    
    
    
}
