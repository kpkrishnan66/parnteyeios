//
//  progresscardTableViewCell.swift
//  parentEye
//
//  Created by scientia on 06/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import SpreadsheetView

class progresscardTableViewCell:UITableViewCell {
    
    @IBOutlet var progresscardDatagridView: SpreadsheetView!
    @IBOutlet var evaluvationCategoryName: UILabel!
    
}
