//
//  LeaveFormVC.swift
//  parentEye
//
//  Created by Emvigo on 7/27/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

protocol leaveSendProtocol:class  {
    
    func updatedLeave()
}


class LeaveFormVC: UIViewController,UITextFieldDelegate,CalendarViewDelegate,UIGestureRecognizerDelegate,UITextViewDelegate{

    @IBOutlet weak var txtDate: UITextField!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var calenderHolderView: UIView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet var discardButton: UIButton!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var descriptionText: UITextView!
    @IBOutlet var descriptionTextHeight: NSLayoutConstraint!
    @IBOutlet var descriptionheaderHeight: NSLayoutConstraint!
    
    var locationRect = CGRect()
    var user:userModel?
    var dateOfLeave:String?
    var leaveDate:String?
    var reason:String?
    var reasonList = [String]()
    var reasonPicker = UIPickerView()
    var leaveReasonList = [String]()
    weak var delegate:leaveSendProtocol?
    
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadLeaveFormView()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool{
        if textField == txtDate {
            return false
        }
        return true
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        calenderHolderView.isHidden = true
        btnDone.isHidden = true
        sendButton.isHidden = false
        discardButton.isHidden = false
        descriptionText.isHidden = false
        descriptionLabel.isHidden = false
    }
    @IBAction func addNewButtonTapped(_ sender: Any) {
       configCalenderView()
    }
    @IBAction func discardButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        if txtDate.text?.isEmpty == true {
            AlertController.showToastAlertWithMessage(message: "Date field is empty", stateOfMessage: .failure)
        }else if txtReason.text?.isEmpty == true{
            AlertController.showToastAlertWithMessage(message: "Reason field is empty", stateOfMessage: .failure)
        }
        else if descriptionText.text?.isEmpty == true &&  user?.Students![(user?.currentOptionSelected)!].leaveDescription == "Mandatory"{
            AlertController.showToastAlertWithMessage(message: "Description  field is empty", stateOfMessage: .failure)
        }
        else{
            debugPrint("good")
            leaveSubmitServiceCall()
        }
    }
    
}

// MARK: Gesture delegates
extension LeaveFormVC{
    
    /*func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool{
     let p = gestureRecognizer.locationInView(self.calenderHolderView)
     if (CGRectContainsPoint(locationRect, p)) {
     return false
     }
     return true
     }*/
    
    
}

extension LeaveFormVC{
    func loadLeaveFormView(){
        getUserDataFromDB()
        txtReason.delegate                            = self
        txtDate.delegate                              = self
        txtDate.text                                  = dateOfLeave
        txtReason.text                                = reason
        let tapGestureToHideCalender                  = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        tapGestureToHideCalender.delegate             = self
        tapGestureToHideCalender.cancelsTouchesInView = false
        tapGestureToHideCalender.numberOfTapsRequired = 2
        locationRect                                  = calenderHolderView.frame
        //self.view.addGestureRecognizer(tapGestureToHideCalender)
        
        if (user?.Students![(user?.currentOptionSelected)!].leaveDescription) == "Hide"{
            descriptionheaderHeight.constant = 0
            descriptionTextHeight.constant = 0
            reasonList = leaveReasonList
        }else if(user?.Students![(user?.currentOptionSelected)!].leaveDescription) == "Show" || (user?.Students![(user?.currentOptionSelected)!].leaveDescription) == "Mandatory" {
            descriptionheaderHeight.constant = 21
            descriptionTextHeight.constant = 30
            reasonList = leaveReasonList
        }
        else{
            reasonList                                    = ["Medical","Personal"]
            
        }
        if reasonList.count > 0{
            txtReason.text                                = reasonList[0]
            
        }
        descriptionText.layer.borderWidth = CGFloat(1)
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        descriptionText.layer.borderColor = color.cgColor
        //descriptionText.text = "Description For Leave"
        descriptionText.layer.borderWidth = 0.5;
        descriptionText.layer.cornerRadius = 3.0;
        loadPickerView()
    }
    func loadPickerView() {
        
        reasonPicker.delegate = self
        reasonPicker.dataSource = self
        reasonPicker.backgroundColor = UIColor.clear
        txtReason.inputView = reasonPicker
        
        
        
    }
    /**
     Function to get user data from DB
     */
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        leaveReasonList = (user?.Students![(user?.currentOptionSelected)!].leaveReasonList)!
    }
    func configCalenderView(){
        btnDone.isHidden = false
        calenderHolderView.isHidden = false
        sendButton.isHidden = true
        discardButton.isHidden = true
        descriptionText.isHidden = true
        descriptionLabel.isHidden = true
        // todays date.
        let date = Foundation.Date()
        
        // create an instance of calendar view with
        // base date (Calendar shows 12 months range from current base date)
        // selected date (marked dated in the calendar)
        let calendarView                                       = CalendarView.instance(date, selectedDate: date)
        calendarView.delegate                                  = self
        calendarView.translatesAutoresizingMaskIntoConstraints = false
        calenderHolderView.addSubview(calendarView)
        
        // Constraints for calendar view - Fill the parent view.
        calenderHolderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[calendarView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["calendarView": calendarView]))
        calenderHolderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[calendarView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["calendarView": calendarView]))
    }
    
    //MARK: Calender controller delegate
    
    func didSelectDate(_ date: Foundation.Date) {
        
        let selectedDate = "\(date.day)-\(date.month)-\(date.year)"
        /*dateOfLeave = txtDate.text
         if dateOfLeave?.rangeOfString(selectedDate) != nil {
         debugPrint("its is in")
         }else{
         debugPrint(selectedDate)
         var leaveDate = self.dateOfLeave?.stringByAppendingString("\(",")\(selectedDate)")
         leaveDate = leaveDate?.stringByReplacingCharactersInRange(leaveDate!.startIndex..<leaveDate!.startIndex.successor(), withString: " ")
         txtDate.text = leaveDate
         
         }*/
        txtDate.text = selectedDate
        
        
    }

    
    
    @objc func handleTap(){
        calenderHolderView.isHidden = true
    }
    func leaveSubmitServiceCall(){
        var description = false
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: weakSelf!)
        var studentid = String()
        if user?.currentTypeOfUser == .guardian{
            studentid = (user?.Students![(user?.currentOptionSelected)!].id)!
            let dates = txtDate.text
            if(user?.Students![(user?.currentOptionSelected)!].leaveDescription) == "Show" || (user?.Students![(user?.currentOptionSelected)!].leaveDescription) == "Mandatory" {
                description = true
            }
            else{
                description = false
                descriptionText.text = ""
            }
            
            WebServiceApi.sendLeave(dateList: dates!, addedById: (user?.currentUserRoleId)!, reason: txtReason.text!, studentIdList: studentid,description: description,descriptionText: descriptionText.text,
                                    
                                    onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        if let tempresult = result["studentLeaveRecord"] as? ([String:AnyObject]){
                                            let message = tempresult["message"] as? String
                                            AlertController.showToastAlertWithMessage(message: message!, stateOfMessage: .success)
                                            self.delegate?.updatedLeave()
                                            
                                            weakSelf!.dismiss(animated: true, completion: nil)
                                        }
            },
                                    onFailure: { (error) in
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
            },
                                    duringProgress: { (progress) in
                                        
            })
        }
    }
}

extension LeaveFormVC: UIPickerViewDataSource {
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reasonList.count
    }
}

extension LeaveFormVC: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return reasonList[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtReason.text = reasonList[row]
        
    }
    
    
    
}
