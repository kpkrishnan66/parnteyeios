//
//  TeacherTimetableActivityPopupCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 01/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class TeacherTimetableActivityPopupCell: UITableViewCell {

    @IBOutlet var dayLabel: UILabel!

    @IBOutlet var monthLabel: UILabel!
    
    
    @IBOutlet var employeeUpcomingContent: UILabel!
    @IBOutlet var yearLabel: UILabel!
    
        
    
}
