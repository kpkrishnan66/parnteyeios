//
//  NutshellVC.swift
//  parentEye
//
//  Created by scientia on 13/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import SpreadsheetView

class NutshellVC: UIViewController,CommonHeaderProtocol,UIPickerViewDelegate,UIPickerViewDataSource,SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var totalStudents: UILabel!
    @IBOutlet var evaluvationActivityListTextField: UITextField!
    @IBOutlet var nutshellspreadsheetView: SpreadsheetView!
    @IBOutlet var lblNoResult: UILabel!
    
    var user:userModel?
    var profilePic = ""
    var evaluvationActivities = [String]()
    var tempEvaluationActivities = [String]()
    let cellReuseIdentifier = "DataCell"
    var nutshellHeaderArray = [String]()
    var displayPCArrayRowPosition = 0
    var displayPCArrayColPosition = 0
    var rowCount = 0
    let studentAbsentcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let studentNotAssignedColor = UIColor(red: 192.0/255.0, green: 192.0/255.0, blue: 192.0/255.0, alpha: 1.0)
    var nutshellResult = [String:AnyObject]()
    var displayProgressCardArray = Array<Array<String>>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setUpHeader()
        setupUI()
        setinitialIndexPathArrayPosition()
        getNutshellReport()
    }
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    func setupUI(){
        nutshellspreadsheetView.dataSource = self
        nutshellspreadsheetView.delegate = self
        nutshellspreadsheetView.register(SpreadSheetCell.self, forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        nutshellspreadsheetView.register(UINib(nibName: String(describing: SpreadSheetCell.self), bundle: nil), forCellWithReuseIdentifier: String(describing: SpreadSheetCell.self))
        nutshellspreadsheetView.backgroundColor = .white
        lblNoResult.isHidden = true
    }
    
    func setinitialIndexPathArrayPosition(){
        
        displayProgressCardArray.removeAll()
        for _ in 0..<100 {
            displayProgressCardArray.append(Array(repeating:String(), count:20))
        }
        
        for row in 0..<100{
            for col in 0..<20{
                displayProgressCardArray[row][col] = ""
            }
        }
        
        for _ in 0..<100 {
            tempEvaluationActivities.append("")
        }
    }
    
    
    func setUpHeader(){
        
        header.delegate = self
        header.leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        header.profileChangeButton.isHidden = true
        header.profileImageRightSeparator.isHidden = true
        header.downArrowButton.isHidden = true
        header.multiSwitchButton.isHidden = true
        header.rightLabel.isHidden = true
        header.screenShowingLabel.text = ConstantsInUI.NtshellResult
        
    }
    
    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showSwitchSibling(fromButton:UIButton) {
        
    }
    
    
    func getNutshellReport(){
        
        var studentId  = ""
        if user?.currentTypeOfUser == .guardian{
            studentId =  (user?.Students![(user?.currentOptionSelected)!].id)!
        }
        self.totalStudents.isHidden = false
        self.evaluvationActivityListTextField.isHidden = false
        self.nutshellspreadsheetView.isHidden = false
        self.lblNoResult.isHidden = true

        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        
        
        WebServiceApi.getNutshellResult(studentId: studentId, onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.displayPCArrayRowPosition = 0
            self.displayPCArrayColPosition = 0
            if let tempResult = result["nutshell"] as? [String:AnyObject] {
                self.nutshellResult = tempResult
                if let header = tempResult["headers"] as? NSArray{
                    
                    self.nutshellHeaderArray.removeAll()
                    for key in header{
                        self.nutshellHeaderArray.append(key as! String)
                        
                    }
                }
                for (subject,_) in tempResult{
                    if subject != "headers" {
                        print("activity===\(subject)")
                        if let subList = tempResult[subject] as? [String:AnyObject] {
                            if let evaluvation_activity_position = subList["position"] as? Int {
                               self.tempEvaluationActivities[evaluvation_activity_position] = subject
                            }
                        }
                        
                    }
                }
                
                if let totStudent = tempResult["totalStudents"] as? String{
                    self.totalStudents.text = "Total Students "+totStudent
                }
                print("tempEvaluationActivities==\(self.tempEvaluationActivities)")
                self.createEvaluationActivityList()
                self.addPickerView()
                
            }
            else{
                self.totalStudents.isHidden = true
                self.evaluvationActivityListTextField.isHidden = true
                self.nutshellspreadsheetView.isHidden = false
                self.lblNoResult.isHidden = false
                
                                
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                weakSelf!.lblNoResult.isHidden = true
                weakSelf!.evaluvationActivityListTextField.isHidden = true
                weakSelf!.totalStudents.isHidden = true
                weakSelf!.nutshellspreadsheetView.isHidden = true
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                debugPrint(error.localizedDescription)
                
        }) { (progress) in
            
        }
    }
    
    func createEvaluationActivityList() {
        evaluvationActivities.removeAll()
        for index in tempEvaluationActivities {
            if index != "" {
                evaluvationActivities.append(index)
            }
        }
    }

    
    func addPickerView(){
        let evaluvationActivityPicker = UIPickerView()
        evaluvationActivityPicker.delegate = self
        evaluvationActivityListTextField.inputView = evaluvationActivityPicker
        if evaluvationActivities.count != 0 {
            self.evaluvationActivityListTextField.text = evaluvationActivities[0]
            evaluvationActivityPicker.reloadAllComponents()
            configNutshell(key: evaluvationActivities[0])
        }
    }
    
    func configNutshell(key:String){
        
        self.displayPCArrayRowPosition = 0
        self.displayPCArrayColPosition = 0
        rowCount = 0
        
        if let subList = nutshellResult[key] as? [String:AnyObject] {
            print(subList)
            if let sublistValue = subList["value"] as? NSArray {
            for subSubList in sublistValue {
                if let pos = subSubList as? [String] {
                    self.displayPCArrayColPosition = 0
                    for i in 0..<pos.count{
                        
                        self.displayProgressCardArray[self.displayPCArrayRowPosition][self.displayPCArrayColPosition] = pos[i]
                        self.displayPCArrayColPosition = self.displayPCArrayColPosition + 1
                    }
                    self.displayPCArrayRowPosition = self.displayPCArrayRowPosition + 1
                    rowCount = rowCount + 1
                 }
                }
            }
            
        }
        nutshellspreadsheetView.reloadData()
    }
}

// MARK: PickerView Delegates
extension NutshellVC{
    // returns the number of 'columns' to display.
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    // returns the # of rows in each component..
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if evaluvationActivities.count > 0 {
            return evaluvationActivities.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         if evaluvationActivities.count > 0 {
            return evaluvationActivities[row]
        }else{
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if evaluvationActivities.count > 0 {
            evaluvationActivityListTextField.text = evaluvationActivities[row]
            configNutshell(key: evaluvationActivities[row])
        }
    }
}

extension NutshellVC {
    
    enum Colors {
        static let border = UIColor.lightGray
        static let headerBackground = UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if nutshellHeaderArray.count > 0 {
            return nutshellHeaderArray.count
        }
        return 0
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        if rowCount == 0{
            return 0
        }
        return rowCount+1
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if column == 0 {
            return 100
        }
        return 100
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        if row == 0 {
            return 44
        }
        return 44
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        if nutshellHeaderArray.count > 0{
            return 1
        }
        return 0
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        if self.rowCount > 0{
            return 1
        }
        return 0
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: SpreadSheetCell.self), for: indexPath) as! SpreadSheetCell
        cell.label.textColor = UIColor.black
        if indexPath.column == 0 && indexPath.row == 0 {
            if nutshellHeaderArray.count > 0 {
              cell.label.text = nutshellHeaderArray[0]
              cell.label.backgroundColor = Colors.headerBackground
            }
        }
        else if indexPath.column == 0 && indexPath.row > 0 {
            cell.label.backgroundColor = Colors.headerBackground
            if displayProgressCardArray.count > 0{
               cell.label.text =  displayProgressCardArray[indexPath.row-1][indexPath.column]
            }else{
               cell.label.text = ""
            }
        }
        else if indexPath.column > 0 && indexPath.row == 0 {
            cell.label.backgroundColor = Colors.headerBackground
            if nutshellHeaderArray.count > 0{
                cell.label.text =  nutshellHeaderArray[indexPath.column]
            }else{
                cell.label.text =  ""
            }
        }
        else{
           
            cell.label.textAlignment = .center
            
            cell.label.text = displayProgressCardArray[indexPath.row-1][indexPath.column]
            
            
            if displayProgressCardArray[indexPath.row-1][indexPath.column] == "AB"{
                 cell.label.textColor =  studentAbsentcolur
            }
            else if displayProgressCardArray[indexPath.row-1][indexPath.column] == "NA"{
                 cell.label.textColor = studentNotAssignedColor
            }
            else{
                cell.label.textColor = UIColor.black
            }

            
            cell.label.backgroundColor = UIColor.white
            cell.backgroundColor = UIColor.white
        }
        
        
        return cell
        
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
    }
    
    
}


