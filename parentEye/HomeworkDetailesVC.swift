//
//  HomeworkDetailesVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import AVFoundation

class  HomeworkDetailesVC: UIViewController,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,popOverDelegate,SelectedSchoolpopOverDelegate,audioPlayBackProtocol
 {
    
    var user:userModel?
    var profilePic = ""
    var id = "-1"
    var Hwtitle            = ""
    var Hwdescription      = ""
    var reference        = ""
    var from             = ""
    var Subject           = ""
    var createdOn        = ""
    var submissionDate   = ""
    var HwFrom             = ""
    var dayString        = ""//String to store the day
    var monthString      = ""//String to store the  Month
    var yearString       = ""//String to store the year
    var classId:Int = 0
    var schoolId:Int = 0
    var lines  = 0
    var imageArray       = [String]()
    var pdfArray         = [PdfModel]()
    var AudioArray       = [AudioModel]()
    var imageArrayDetailed = [UIImageView]()
    var pdfViewer = PdfController()
    var moveToPdf = false
    var audioPlayView  = audioPlay()
    var destinationUrl = NSURL()
    var replacedString:String = ""
    
    
    
    @IBOutlet var homeworkNumberofStudentsViewHeight: NSLayoutConstraint!
    @IBOutlet var descriptionView: UILabel!
    @IBOutlet var header: CommonHeader!
    @IBOutlet var referenceLabel: UITextView!
    @IBOutlet var MainViewHeight: NSLayoutConstraint!
    @IBOutlet var referenceLabelHeight: NSLayoutConstraint!
    @IBOutlet var referenceView: UILabel!
    @IBOutlet var descriptionLabelHeight: NSLayoutConstraint!
    @IBOutlet var tempLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var tobeSubmittedonorBefore: UILabel!
    @IBOutlet var homeWorkFrom: UILabel!
    @IBOutlet var homeWorkSubject: UILabel!
    @IBOutlet var homeWorkTitle: UILabel!
    @IBOutlet var monthAndYearLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var toHomeworkLabel: UILabel!
    @IBOutlet var imageContainerViewHeight: NSLayoutConstraint!
    @IBOutlet var imageContainerView: ExpansiveView!
    
    
    //MARK:- View delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setUpHeader()
        setUpUI()
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            makeWebApiCallToGetHomeWorkDetailedEntries()
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if moveToPdf == false {
            _ = self.navigationController?.popViewController(animated: false)
        }
        else{
            moveToPdf = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func setUpUI(){
        tempLabel.isHidden = true
        if user?.currentTypeOfUser == .guardian{
            homeworkNumberofStudentsViewHeight.constant = 0
            toHomeworkLabel.isHidden = true
        }
        dayLabel.text = dayString
        monthAndYearLabel.text = monthString+" "+yearString
        homeWorkTitle.text = Hwtitle
        homeWorkSubject.text = Subject
        homeWorkFrom.text = HwFrom
        tobeSubmittedonorBefore.text = submissionDate
        
        if !Hwdescription.isEmpty{
            descriptionLabel.text = Hwdescription
            descriptionLabel.numberOfLines = 0
            descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            descriptionLabel.sizeToFit()
            lines = countLabelLines(descriptionLabel)
            descriptionLabelHeight.constant = descriptionLabelHeight.constant + (CGFloat(lines * 20) - 20)
            MainViewHeight.constant = MainViewHeight.constant + descriptionLabelHeight.constant
        }
        else{
            descriptionView.isHidden = true
        }
        if !reference.isEmpty{
            
            referenceLabel.text = reference
            let numLines = referenceLabel.text.sizeForWidth(referenceLabel.contentSize.width, font: referenceLabel.font!).height / referenceLabel.font!.lineHeight
            referenceLabelHeight.constant = referenceLabelHeight.constant + (CGFloat(numLines * 30))
            MainViewHeight.constant = MainViewHeight.constant + referenceLabelHeight.constant
            referenceLabel.dataDetectorTypes = UIDataDetectorTypes.all
        }
        else{
            referenceView.isHidden = true
        }
        
        for urlValue in self.AudioArray{
            var image = UIImage()
            if urlValue.type == "Audio" {
                image = UIImage(named: "audioIcon")!
            }
            
            let imagv = CustomImageView(image: image)
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlValue.url, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imagv.url = urlValue.url
            imagv.name = urlValue.name
            imageArrayDetailed.append(imagv)
            
            let audioTap = UITapGestureRecognizer(target: self, action: #selector(self.loadAudio(_:)))
            imagv.addGestureRecognizer(audioTap)
            imagv.isUserInteractionEnabled = true
        }
        
        
        for urlString in self.imageArray{
            let imagv         = UIImageView(image: UIImage(named: "ApplicationLogo"))
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlString, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imageArrayDetailed.append(imagv)
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.showImageExpandedForDetailedHomework(_:)))
            imagv.addGestureRecognizer(tap)
            imagv.isUserInteractionEnabled = true
            
        }
        for urlString in pdfArray{
            var image = UIImage()
            if urlString.type == "PDF" {
                image = UIImage(named: "pdfIcon")!
            }
            if urlString.type == "DOC" || urlString.type == "DOCX"  {
                image = UIImage(named: "wordIcon")!
            }
            let imagv = CustomImageView(image: image)
            
            weak var weakImageVc  = imagv
            ImageAPi.fetchImageForUrl(urlString: urlString.url, oneSuccess: { (image) in
                if weakImageVc != nil { weakImageVc!.image = image }
            }, onFailure: { (error) in
                
            })
            imagv.contentMode = .scaleAspectFit
            imagv.url = urlString.url
            imagv.name = urlString.name
            imageArrayDetailed.append(imagv)
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.loadPdf(_:)))
            imagv.addGestureRecognizer(tap)
            imagv.isUserInteractionEnabled = true
            
            
        }
        
        if imageArrayDetailed.count > 0 {
            imageContainerViewHeight.constant = imageContainerView.addImagesToMeFromArrayForDiaryDetailed(imageArray: imageArrayDetailed)
            //diaryViewHeight.constant = diaryViewHeight.constant + (diaryAttachmentViewHeight.constant)
            MainViewHeight.constant = MainViewHeight.constant + imageContainerViewHeight.constant
        }
        
        if user?.currentTypeOfUser == .CorporateManager {
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        
    }
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    
    //MARK:- Heder delegates
    fileprivate func setUpHeader (){
        if user?.currentTypeOfUser == .guardian{

            profilePic =  (user?.Students![(user?.currentOptionSelected)!].profilePic)!
        }
        else{
            profilePic = ""
        }
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.homeworkScreenHeading, withProfileImageUrl: profilePic)
        header.delegate = self
        
        
    }
    //MARK: common header protocol
    
    func showHamburgerMenuOrPerformOther() {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .guardian {
            PopOverView.addPopOverToViewController(self, fromButton: fromButton)
        }
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        //DBController().changePrefferedOptionForUserTo(rowSelected)
        user?.currentOptionSelected = rowSelected
        // reflecting the change in DB
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        self.tabBarController?.selectedIndex = 2
    }
    
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        let tab = self.navigationController?.tabBarController as! ParentEyeTabbar
        tab.getUnreadCount()
        if user?.schools![(user?.currentSchoolSelected)!].schoolId != schoolId {
            self.tabBarController?.selectedIndex = 2
            _ = self.navigationController?.popToRootViewController(animated: false)
        }
        
    }
    
    fileprivate func makeWebApiCallToGetHomeWorkDetailedEntries(){
        
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.getHomeWorkDetailsEntry(homeWorkId: id, classId: classId ,onSuccess: { (result) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            if let studentHomeWorks = result[resultsJsonConstants.homeWorkConstants.homeworkStudentDetails] as? String{
                self.toHomeworkLabel.text = "To:" + studentHomeWorks
            }
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }, duringProgress: { (progress) in
            
        })
        
    }
    func countLabelLines(_ label:UILabel)->Int{
        
        if let text = label.text{
            // cast text to NSString so we can use sizeWithAttributes
            let myText = text as NSString
            
            //Set attributes
            let attributes = [NSAttributedStringKey.font : label.font]
            let bounds = UIScreen.main.bounds
            let screenWidth = bounds.size.width
            
            //Calculate the size of your UILabel by using the systemfont and the paragraph we created before. Edit the font and replace it with yours if you use another
            let labelSize = myText.boundingRect(with: CGSize(width: (screenWidth - 16), height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
            //Now we return the amount of lines using the ceil method
            let lines = ceil(CGFloat(labelSize.height) / label.font.lineHeight)
            return Int(lines)
        }
        
        return 0
        
    }
    
    @objc func showImageExpandedForDetailedHomework(_ tapgesture:UITapGestureRecognizer) -> Void {
        FireBaseKeys.FirebaseKeysRuleOne(value: "homework_attachDwnld")
        let parentImageView =  tapgesture.view as! UIImageView
        
        let array = Bundle.main.loadNibNamed("PhotoShowingView", owner: self, options: nil)
        let photoView             = array![0] as! PhotoShowingView
        photoView.imageView.image = parentImageView.image
        photoView.frame           = self.view.frame
        self.view.addSubview(photoView)
    }
    
    @objc fileprivate func loadPdf(_ tapgesture:UITapGestureRecognizer) {
        
        FireBaseKeys.FirebaseKeysRuleOne(value: "homework_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        moveToPdf = true
        pdfViewer.showPdfInViewController(vc: self, pdfHavingUrl: parentImageView.url, withName: parentImageView.name)
        
    }
    
    
    @objc fileprivate func loadAudio(_ tapgesture:UITapGestureRecognizer) {
        FireBaseKeys.FirebaseKeysRuleOne(value: "diary_attachDwnld")
        let parentImageView =  tapgesture.view as! CustomImageView
        print("audio tapped")
        
        if URL(string: parentImageView.url) != nil {
            
            // then lets create your document folder url
            let documentsDirectoryURL =  FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileName = parentImageView.url
            if fileName.range(of: "opus") != nil{
                print("exists")
                replacedString = (fileName as NSString).replacingOccurrences(of: "opus", with: "mp3")
            }
            
            
            let fileUrl = URL(fileURLWithPath: replacedString as String)
            // lets create your destination file url
            destinationUrl = documentsDirectoryURL.appendingPathComponent(fileUrl.lastPathComponent ) as NSURL
            print(destinationUrl)
            
            // to check if it exists before downloading it
            if FileManager().fileExists(atPath: destinationUrl.path!) {
                print("The file already exists at path")
                self.setupAudioPlay()
                self.audioPlayView.toPass = self.destinationUrl
                self.audioPlayView.isFileExistAtLocal = true
                // if the file doesn't exist
            } else {
                
                self.audioPlayView.isFileExistAtLocal = false
                //let webServiceUrl = NSURL(string: webServiceUrls.baseURl+"convertAudioPlay/uniqueName/"+parentImageView.name)
                weak var weakSelf = self
                hudControllerClass.showNormalHudToViewController(viewController: self)
                WebServiceApi.getAudioPlayUrl(name: parentImageView.name,onSuccess: { (result) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    if let playUrl = result["convertAudioPlay"] as? String {
                        print("inside audioplay")
                        print(playUrl)
                        
                        //self.audioPlayView.downloadUrl = playUrl
                        //self.audioPlayView.destinationUrl = self.destinationUrl
                        let url = URL(string: playUrl)
                        self.setupAudioPlay()
                        self.downloadFileFromURL(url!)
                    }
                    
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                    
                }) { (progress) in
                    
                }
            }
            
        }
        
        // to check if it exists before downloading it
        //print(webServiceUrl!)
        
        
        
    }
    
    
    func downloadFileFromURL(_ url:URL){
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url, completionHandler: { [weak self](URL, response, error) -> Void in
            guard URL != nil && error == nil else {
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
                print("failed")
                return
            }
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self?.play(URL!)
        })
        
        downloadTask.resume()
        
    }
    
    func play(_ url:URL) {
        
        do {
            try FileManager().moveItem(at: url, to: self.destinationUrl as URL)
            
            print("File moved to documents folder")
            print(url)
            print(self.destinationUrl)
            self.audioPlayView.toPass = self.destinationUrl
            self.audioPlayView.destinationUrl = self.destinationUrl
            
        }
        catch let error as NSError {
            print(error.localizedDescription)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
        }
    }
    
    
    func setupAudioPlay() {
        let array = Bundle.main.loadNibNamed("audioPlay", owner: self, options: nil)
        audioPlayView             = array![0] as! audioPlay
        
        audioPlayView.center = CGPoint(x: self.view.frame.size.width  / 2,
                                       y: self.view.frame.size.height / 2);
        audioPlayView.frame           = self.view.frame
        audioPlayView.delegate = self
        audioPlayView.soundSlider.value = 0.0
        audioPlayView.playAudioButton.setImage(UIImage(named: "playIcon"), for: UIControlState())
        audioPlayView.initialLoad = true
        audioPlayView.playTimeText.text = "00.00"
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        self.view.addSubview(audioPlayView)
    }
    
    func exitSubView() {
        audioPlayView.removeFromSuperview()
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    
}

extension String {
    func sizeForWidth(_ width: CGFloat, font: UIFont) -> CGSize {
        let attr = [NSAttributedStringKey.font: font]
        let height = NSString(string: self).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options:.usesLineFragmentOrigin, attributes: attr, context: nil).height
        return CGSize(width: width, height: ceil(height))
    }
}
