//
//  HomeworkComposeClassListPopUpVCCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 24/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class HomeworkComposeClassListPopUpVCCell: UITableViewCell {
    
    @IBOutlet var ClassName: UILabel!
    
    @IBOutlet var selectionButton: UIButton!
    
    var isTeacherSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //function to set the value externally
    func setTeacherSelected(_ selected:Bool){
        isTeacherSelected = selected
        if selected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: UIControlState())
        }else{
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: UIControlState())
        }
    }
    @IBAction func changeSeletionFromUI(){
        
        if isTeacherSelected == true{
            selectionButton?.setBackgroundImage(UIImage(named: "commonSelectedIcon"), for: UIControlState())
        }else {
            selectionButton?.setBackgroundImage(UIImage(named: ""), for: UIControlState())
        }
        
        isTeacherSelected = !isTeacherSelected
    }
    
    
}


