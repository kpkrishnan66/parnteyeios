//
//  EmloyeeMarkEntryVC.swift
//  parentEye
//
//  Created by scientia on 17/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import IQKeyboardManagerSwift

class EmloyeeMarkEntryVC: UIViewController,CommonHeaderProtocol,UIPickerViewDelegate,UIPickerViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,dismissVCProtocol {
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var classListCollectionView: UICollectionView!
    @IBOutlet var evaluationActivityTextField: UITextField!
    @IBOutlet var evaluationSubjectTextField: UITextField!
    @IBOutlet var maxMarkTextField: UITextField!
    @IBOutlet var classTestTextField: UITextField!
    @IBOutlet var examSubjectValidationlabel: UILabel!
    @IBOutlet var addExamButton: UIButton!
    @IBOutlet var maxMarkTextLabel: UILabel!
    @IBOutlet var examPickerDropDownIcon: UIImageView!
    @IBOutlet var subjectPickerDropDownIcon: UIImageView!
    
    
    var user:userModel?
    var profilePic = ""
    var ClassIsSelected = [Int: Bool]()
    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greencolur = UIColor(red: 99/255.0, green: 198/255.0, blue: 116/255.0, alpha: 1.0)
    var selectSubjectPicker = UIPickerView()
    var selectExamPicker = UIPickerView()
    var className:String = ""
    var classId:Int = 0
    var evaluationActivityId:Int = 0
    var classSubjectId:Int = 0
    var classTestId:Int = 0
    var classTestMaxMarkValue = 0
    var evaluationActivitySubjectMaxMark:Int = 0
    var manageClassId:Bool = false
    var initialStudentListLoad:Bool = false
    var isClassTest:Bool = false
    var evaluationActivitySubjectAdded:Bool = false
    var seguetoMarkEntryDetailedVc:Bool = false
    var ischangeClass:Bool = false
    var exam = evaluationActivity()
    var subject = classSubject()
    var examclassTest = classTest()
    var evaluationActivitySubjectResoponse = [String:AnyObject]()
    var markentry = EmloyeeMarkEntryDetailVC()
    
    lazy var ClassesList = [EmployeeClassModel]()
    lazy var defaultClassesList:[EmployeeClassModel] = []
    lazy var evaluationActivityList = [evaluationActivity]()
    lazy var classSubjectList = [classSubject]()
    lazy var classTestListingArray = [classTest]()
    lazy var classtestListString = [String]()
    lazy var evaluationActivitySubjectList = [evaluationActivitySubject]()
    lazy var studentList = [studentExamList]()
    
    let attributes = [
        NSAttributedStringKey.foregroundColor: UIColor.gray,
        NSAttributedStringKey.font : UIFont(name: "Roboto-Medium", size: 14)! // Note the !
    ]

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        setupUI()
        populateClassList()
        alterHeader()
        addPickerView()
        getEvaluationActivityList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if seguetoMarkEntryDetailedVc{
            print("classId ====\(classId)")
            print("selectExamPicker====\(selectExamPicker.selectedRow(inComponent: 0))")
            print("selectSubjectPicker====\(selectSubjectPicker.selectedRow(inComponent: 0))")
            print(isClassTest)
            if isClassTest {
              //setupNewClassTest(true)
            }
            else{
                if evaluationActivityList.count > 0 {
                    evaluationActivityTextField.text = evaluationActivityList[selectExamPicker.selectedRow(inComponent: 0)].name
                }
                if classSubjectList.count > 0 {
                    evaluationSubjectTextField.text = classSubjectList[selectSubjectPicker.selectedRow(inComponent: 0)].name
                    
                }
                getExamWithStudentsMark(isAfterExamCreation: false)
            }
            
        }
    }
    
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    func setupUI () {
        evaluationActivityTextField.layer.borderWidth = CGFloat(1)
        evaluationSubjectTextField.layer.borderWidth = CGFloat(1)
        
        let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
        evaluationActivityTextField.layer.borderColor = color.cgColor
        evaluationSubjectTextField.layer.borderColor = color.cgColor

        evaluationActivityTextField.attributedPlaceholder = NSAttributedString(string: " Select Exam", attributes:attributes)
        evaluationSubjectTextField.attributedPlaceholder = NSAttributedString(string: " Select Subject", attributes:attributes)
        maxMarkTextField.keyboardType = .numberPad
        IQKeyboardManager.sharedManager().previousNextDisplayMode = IQPreviousNextDisplayMode.alwaysShow
    }
    
    func populateClassList () {
        ClassesList = (user?.Classes)!
        for k in 0 ..< ClassesList.count {
            ClassIsSelected[k] = false
        }
        ClassIsSelected[0] = true
        classId = ClassesList[0].classId
        manageClassId = true
        className = ClassesList[0].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[0])
        
    }
    
    private func alterHeader () {
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.markEntryHeading+"  "+className, withProfileImageUrl: profilePic)
        header.delegate = self
    }
    // header delagates
    
    func showHamburgerMenuOrPerformOther() {
        self.dismiss(animated: false, completion: nil)
    }
    func showSwitchSibling(fromButton:UIButton) {
       
        
    }
    
    func addPickerView() {
        
        selectExamPicker.tag = 0
        selectExamPicker.delegate = self
        selectExamPicker.dataSource  = self
        evaluationActivityTextField.inputView = selectExamPicker
        
        selectSubjectPicker.tag = 1
        selectSubjectPicker.delegate = self
        selectSubjectPicker.dataSource = self
        evaluationSubjectTextField.inputView = selectSubjectPicker
        
    }
    
    @objc private func selectedclassAction(sender:UIButton){
        ClassIsSelected[sender.tag] = true
        for k in 0 ..< ClassIsSelected.count {
            if k != sender.tag{
                ClassIsSelected[k] = false
                sender.backgroundColor = UIColor.clear
                sender.setTitleColor(UIColor.black, for: UIControlState.normal)
                sender.layer.borderWidth = 1
                sender.layer.borderColor = UIColor.black.cgColor
                
            }
            else{
                ClassIsSelected[k] = true
                sender.backgroundColor = colur
                sender.setTitleColor(UIColor.white, for: UIControlState.normal)
                sender.layer.borderWidth = 0
                
            }
        }
        classId = ClassesList[sender.tag].classId
        manageClassId = true
        className = ClassesList[sender.tag].className
        defaultClassesList.removeAll()
        defaultClassesList.append(ClassesList[sender.tag])
        classListCollectionView.reloadData()
        self.initialStudentListLoad = true
        alterHeader()
        ischangeClass = true
        getEvaluationActivityList()
        
    }
    
    func getEvaluationActivityList () {
        if ischangeClass {
            hudControllerClass.showNormalHudToViewController(viewController: self)
        }
        weak var weakSelf = self
       
        self.evaluationActivityList.removeAll()
        isClassTest = false
        WebServiceApi.getEvaluationActivityListForMarkEntry(classId: String(classId),onSuccess: { (result) in
            if self.ischangeClass {
               hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            }
            
               self.evaluationActivityList = parseEvaluationActivityListArray.parseThisDictionaryForEvaluationActivityRecords(dictionary: result)
               if self.evaluationActivityList.count > 0 {
                 self.examModification()
                 self.evaluationActivityTextField.text = self.evaluationActivityList[0].name
                 self.evaluationActivityId = self.evaluationActivityList[0].id
                 self.selectExamPicker.reloadAllComponents()
                 self.selectExamPicker.selectRow(0, inComponent: 0, animated: false)
                 self.setUpSubjectSelectionPickerForExam(isAfterExamCreation: false,position: 0)
               }
               else{
                 self.ischangeClass = false
                 self.noExamModification()
               }
            
            }, onFailure: { (error) in
                if self.ischangeClass {
                    self.ischangeClass = false
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                }
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                self.noExamModification()
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    func setUpSubjectSelectionPickerForExam (isAfterExamCreation:Bool,position:Int) {
        
        if self.ischangeClass {
        hudControllerClass.showNormalHudToViewController(viewController: self)
        }
        weak var weakSelf = self
        
        isClassTest = false
        classSubjectList.removeAll()
        WebServiceApi.getclassSubjectForMarkEntry(classId: String(classId),onSuccess: { (result) in
            if self.ischangeClass {
              hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            }
            self.classSubjectList = parseClassSubjectListArray.parseThisDictionaryForClassSubjectRecords(dictionary: result)
                if self.classSubjectList.count > 0 {
                    self.subjectModification()
                    self.evaluationSubjectTextField.text = self.classSubjectList[0].name
                    self.selectSubjectPicker.reloadAllComponents()
                    //self.selectSubjectPicker.selectRow(0, inComponent: 0, animated: false)
                    //self.getExamWithStudentsMark()
                    if(isAfterExamCreation) {
                      self.addExamButton.setTitle("Ok", for: .normal)
                      self.selectSubjectPicker.selectRow(position, inComponent: 0, animated: false)
                        if self.isClassTest {
                            if self.ischangeClass {
                                self.ischangeClass = false
                            }
                        }
                        else{
                            if self.evaluationActivityList.count > 0 {
                                self.evaluationActivityTextField.text = self.evaluationActivityList[self.selectExamPicker.selectedRow(inComponent: 0)].name
                            }
                            if self.classSubjectList.count > 0 {
                                self.evaluationSubjectTextField.text = self.classSubjectList[self.selectSubjectPicker.selectedRow(inComponent: 0)].name
                                
                            }
                            self.getExamWithStudentsMark(isAfterExamCreation: true)
                        }
                      
                    }
                    else{
                        self.selectSubjectPicker.selectRow(0, inComponent: 0, animated: false)
                        self.getExamWithStudentsMark(isAfterExamCreation: false)
                    }
                }
                else{
                    if self.ischangeClass {
                        self.ischangeClass = false
                    }
                    self.noSubjectModification()
               }
//            if(isAfterExamCreation) {
//               self.addExamButton.setTitle("Ok", forState: .Normal)
//               self.selectSubjectPicker.selectRow(position, inComponent: 0, animated: false)
//            }
            
            }, onFailure: { (error) in
                if self.ischangeClass {
                    self.ischangeClass = false
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                }
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                self.noSubjectModification()
            }, duringProgress: { (progress) in
                
        })
        
    }

    func examModification() {
        evaluationActivityTextField.isHidden = false
        evaluationSubjectTextField.isHidden = false
        examPickerDropDownIcon.isHidden = false
        subjectPickerDropDownIcon.isHidden = false
        maxMarkTextField.isHidden = false
        maxMarkTextField.isUserInteractionEnabled = true
        addExamButton.isHidden = false
        maxMarkTextLabel.isHidden = false
        examSubjectValidationlabel.isHidden = true
        classTestTextField.isHidden = true
    }
    
    func noExamModification() {
        evaluationActivityTextField.isHidden = true
        evaluationSubjectTextField.isHidden = true
        examPickerDropDownIcon.isHidden = true
        subjectPickerDropDownIcon.isHidden = true
        maxMarkTextField.isHidden = true
        addExamButton.isHidden = true
        maxMarkTextLabel.isHidden = true
        classTestTextField.isHidden = true
        examSubjectValidationlabel.isHidden = false
        examSubjectValidationlabel.text = "Sorry No Exams Found .Please contact support"
    }
    
    func subjectModification() {
        evaluationSubjectTextField.isHidden = false
        subjectPickerDropDownIcon.isHidden = false
        maxMarkTextField.isHidden = false
        addExamButton.isHidden = false
        maxMarkTextLabel.isHidden = false
        classTestTextField.isHidden = true
        examSubjectValidationlabel.isHidden = true
    }
    
    func noSubjectModification() {
        evaluationSubjectTextField.isHidden = true
        subjectPickerDropDownIcon.isHidden = true
        maxMarkTextField.isHidden = true
        addExamButton.isHidden = true
        maxMarkTextLabel.isHidden = true
        classTestTextField.isHidden = true
        examSubjectValidationlabel.isHidden = false
        examSubjectValidationlabel.text = "Sorry No subject assigned. please contact support"
    }
    
    //collectionView Delegates and Datasources
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ClassesList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "classListCollectionViewCell", for: indexPath as IndexPath) as! classListCollectionViewCell
        
        if ClassIsSelected[indexPath.row] == false{
            cell.className.backgroundColor = UIColor.clear
            cell.className.setTitleColor(UIColor.black, for: UIControlState.normal)
            cell.className.layer.borderWidth = 1
            cell.className.layer.borderColor = UIColor.black.cgColor
            
        }
        else{
            cell.className.backgroundColor = colur
            cell.className.setTitleColor(UIColor.white, for: UIControlState.normal)
            cell.className.layer.borderWidth = 0
        }
        cell.className.setTitle(ClassesList[indexPath.row].className, for: .normal)
        cell.className.layer.cornerRadius = 18
        cell.className.layer.borderWidth = 1
        cell.className.layer.borderColor = UIColor.black.cgColor
        cell.className.tag = indexPath.row
        cell.className.addTarget(self, action: #selector(self.selectedclassAction), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    // MARK: PickerView DataSources
    
    func numberOfComponents(in colorPicker: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 0 {
            return evaluationActivityList.count
        }
        if pickerView.tag == 1{
            if isClassTest == false {
                return classSubjectList.count
            }
            else{
                return classtestListString.count
            }
        }
        return 0
    }
    
    // MARK: PickerView Delegates
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        if pickerView.tag == 0 {
            return evaluationActivityList[row].name
            
        }
        if pickerView.tag == 1 {
            if isClassTest == false {
                return classSubjectList[row].name
            }
            else{
                return classtestListString[row]
            }
        }
        
        return ""
    }

   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if pickerView.tag == 0 {
       evaluationActivityTextField.text = evaluationActivityList[row].name
       //self.evaluationActivityId = evaluationActivityList[row].id
        if evaluationActivityList[row].name == "Class Test" {
           FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_classTestFromSpnr")
           isClassTest = true
            setUpSubjectSelectionPickerForClasstest(isAfterClassTestCreation: false,position:0)
        }
        else{
            FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_examFromSpnr")
            isClassTest = false
            //self.classSubjectId = classSubjectList[row].id
            setUpSubjectSelectionPickerForExam(isAfterExamCreation: false,position: 0)
        }
    }
    if pickerView.tag == 1 {
        if isClassTest == false {
            evaluationSubjectTextField.text = classSubjectList[row].name
            //self.classSubjectId = classSubjectList[row].id
            FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_subjetFrmSpnr")
            getExamWithStudentsMark(isAfterExamCreation: false)
            
        }else{
            evaluationSubjectTextField.text = classtestListString[row]
            if evaluationSubjectTextField.text! == "Add New" {
                
                setupNewClassTest(isAfterClassTestCreation: true)
                
            }
            else{
                
                self.classTestId = classTestListingArray[row].id
                setupNewClassTest(isAfterClassTestCreation: false)
            }
        }
    }
  }
    
    func setUpSubjectSelectionPickerForClasstest(isAfterClassTestCreation:Bool,position:Int) {
        //hudControllerClass.showNormalHudToViewController(self)
        
        classTestTextField.isHidden = true
        isClassTest = true
        classTestListingArray.removeAll()
        classtestListString.removeAll()
        WebServiceApi.getclassTesttListingForMarkEntry(classId: String(classId),onSuccess: { (result) in
            //hudControllerClass.hideHudInViewController(weakSelf!)
            self.classTestListingArray = parseclassTestArray.parseThisDictionaryForclassTest(dictionary: result)
            if self.classTestListingArray.count>0 {
               self.subjectModification()
                for index in self.classTestListingArray {
                    self.classtestListString.append(index.name)
                }
                 self.classtestListString.append("Add New")
                
                 self.evaluationSubjectTextField.text = self.classTestListingArray[0].name
                 self.maxMarkTextField.text = String(self.classTestListingArray[0].maximumMark)
                 self.classTestMaxMarkValue = self.classTestListingArray[0].maximumMark
                 self.classTestId = self.classTestListingArray[0].id
                
            }
            else{
               self.classtestListString.append("Add New")
               self.evaluationSubjectTextField.text = "Add New"
               self.classTestId = 0
               self.addnewClassTestUIChange()
            }
            self.selectSubjectPicker.reloadAllComponents()
            self.selectSubjectPicker.selectRow(0, inComponent: 0, animated: false)
            if isAfterClassTestCreation {
                if self.classTestListingArray.count > 0 {
                 self.selectSubjectPicker.selectRow(position, inComponent: 0, animated: false)
                 self.evaluationSubjectTextField.text = self.classTestListingArray[position].name
                 self.setupNewClassTest(isAfterClassTestCreation: true)
                }
               
            }
            
            }, onFailure: { (error) in
                //hudControllerClass.hideHudInViewController(weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                self.noSubjectModification()
            }, duringProgress: { (progress) in
                
        })

    }
    func addnewClassTestUIChange() {
        maxMarkTextField.isUserInteractionEnabled = true
        maxMarkTextField.text = ""
        maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Mark",attributes: attributes)
        classTestTextField.isHidden = false
        classTestTextField.text = ""
        classTestTextField.attributedPlaceholder = NSAttributedString(string: "Class Test Name", attributes: attributes)
        examSubjectValidationlabel.isHidden = false
        examSubjectValidationlabel.text = "You can add it by entering  max mark and class test name"
        addExamButton.setTitle("Add Class Test", for: .normal)
    }
   
    func setupNewClassTest (isAfterClassTestCreation:Bool) {
         maxMarkTextLabel.isHidden = false
         maxMarkTextField.isHidden = false
         maxMarkTextField.text = ""
         maxMarkTextField.isUserInteractionEnabled = true
        
        if evaluationSubjectTextField.text! == "Add New" {
           addnewClassTestUIChange()
        }
        else{
            classTestTextField.isHidden = true
            addExamButton.setTitle("Ok", for: .normal)
            if classTestListingArray.count > 0 {
             let subpos = selectSubjectPicker.selectedRow(inComponent: 0)
             examclassTest = classTestListingArray[subpos]
             classTestMaxMarkValue = classTestListingArray[subpos].maximumMark
            }
            
            
            
            
            if classTestMaxMarkValue == 0 {
                maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Mark",attributes: attributes)
                examSubjectValidationlabel.isHidden = false
                examSubjectValidationlabel.text = "It seem max mark didnot set. You can set it by entering  max mark"
            }
            else{
               examSubjectValidationlabel.isHidden = true
               maxMarkTextField.isUserInteractionEnabled = false
               maxMarkTextField.text = String(classTestMaxMarkValue)
                if isAfterClassTestCreation {
                  getStudentWithMarkforClassTest(classTestObj: examclassTest)
                }
             }
            
        }

    }

    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
    
       if evaluationSubjectTextField.text! == "Add New" && isClassTest {
            if maxMarkTextField.text == "" {
                examSubjectValidationlabel.text = "Please enter a max mark"
            }
            else if maxMarkTextField.text == "0" {
                examSubjectValidationlabel.text = "Sorry! zero is not a valid number"
            }
            else if classTestTextField.text == "" {
                examSubjectValidationlabel.text = "Please enter a name for class test"
            }
            else{
                FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_createClasstest")
                self.maxMarkTextField.isUserInteractionEnabled = false
                print("class test creation true")
                createNewEvalautionSubject(isClassTestCreation: true)
            }
        }
        
       else if evaluationSubjectTextField.text! != "Add New" && isClassTest{
        if classTestMaxMarkValue == 0 {
            self.maxMarkTextField.isUserInteractionEnabled = true
            self.examSubjectValidationlabel.isHidden = false
            self.examSubjectValidationlabel.text = "Sorry! zero is not a valid number"
            FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_updtClassTest")
        }
        else{
            self.maxMarkTextField.isUserInteractionEnabled = false
            updateEvaluationActivitySubject(isClassTestUpdation: true)
            
        }
       }
        
       else if !evaluationActivitySubjectAdded && !isClassTest {
          if maxMarkTextField.text == "" {
            self.maxMarkTextField.isUserInteractionEnabled = true
            self.examSubjectValidationlabel.isHidden = false
            self.examSubjectValidationlabel.text = "It seem max mark didnot set. You can set it by entering  max mark"
          }
          else if maxMarkTextField.text == "0" {
            self.maxMarkTextField.isUserInteractionEnabled = true
            self.examSubjectValidationlabel.isHidden = false
            self.examSubjectValidationlabel.text = "Sorry! zero is not a valid number"
           }
          else {
           FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_createExam")
           self.maxMarkTextField.isUserInteractionEnabled = false
           createNewEvalautionSubject(isClassTestCreation: false)

          }
       }
        
       else if evaluationActivitySubjectAdded && !isClassTest {
          if maxMarkTextField.text == "" {
            self.maxMarkTextField.isUserInteractionEnabled = true
            self.examSubjectValidationlabel.isHidden = false
            self.examSubjectValidationlabel.text = "It seem max mark didnot set. You can set it by entering  max mark"
          }
          else if evaluationActivitySubjectMaxMark == 0 || maxMarkTextField.text == ""{
            self.maxMarkTextField.isUserInteractionEnabled = true
            self.examSubjectValidationlabel.isHidden = false
            self.examSubjectValidationlabel.text = "Sorry! zero is not a valid number"
            updateEvaluationActivitySubject(isClassTestUpdation: false)
            
          }
          else{
            self.maxMarkTextField.isUserInteractionEnabled = false
            FireBaseKeys.FirebaseKeysRuleFour(value: "markEntry_updateExam")
            setStudentsWithMarkToRecylerView(isForClassTest: false)
          }
       }
        
    }
    
    func createNewEvalautionSubject(isClassTestCreation:Bool) {
        
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        if evaluationActivityList.count > 0 {
         exam = evaluationActivityList[selectExamPicker.selectedRow(inComponent: 0)]
         evaluationActivityId = exam.id
        }
        
        let subpos = selectSubjectPicker.selectedRow(inComponent: 0)
        
        var classTestName = ""
        if isClassTestCreation {
            //examclassTest = classTestListingArray[subpos]
            //classSubjectId = examclassTest.id
            if classTestTextField.text! == "" {
              classTestName = ""
            }
            else{
              classTestName = classTestTextField.text!
            }
        }
        else{
            subject = classSubjectList[subpos]
            classSubjectId = subject.id
            classTestName = ""
        }
        
        WebServiceApi.postNewEvaluationSubjectForMarkEntry(classId: String(classId),userRoleId: (user?.currentUserRoleId)!, maxmark: maxMarkTextField.text!,classTestName: classTestName,evaluationActivityId: String(evaluationActivityId),classSubjectId: String(classSubjectId),isClassTestCreation: isClassTestCreation,onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            
            
            self.evaluationActivitySubjectResoponse.removeAll()
            self.evaluationActivitySubjectList.removeAll()
            self.evaluationActivitySubjectResoponse = result
            
            if isClassTestCreation {
                
                //self.evaluationActivitySubjectList = evaluationActivitySubjectArray.parseThisDictionaryForevaluationActivitySubjectClassTestRecords(result)
                let subpos = self.selectSubjectPicker.selectedRow(inComponent: 0)
                self.setUpSubjectSelectionPickerForClasstest(isAfterClassTestCreation: true,position: subpos)
            }
            else{
                
                self.evaluationActivitySubjectList = evaluationActivitySubjectArray.parseThisDictionaryForevaluationActivitySubjectRecords(dictionary: result)
                self.setUpSubjectSelectionPickerForExam(isAfterExamCreation: true,position: subpos)
                //add segue....
            }
            
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
            }, duringProgress: { (progress) in
                
        })
        
    }
    
    func updateEvaluationActivitySubject (isClassTestUpdation:Bool) {
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        var evaluationActivitySubjectId:Int = 0
        let subpos = selectSubjectPicker.selectedRow(inComponent: 0)
//        if classSubjectList.count > 0{
//         subject = classSubjectList[subpos]
//         classSubjectId = subject.id
//        }
        
        if isClassTest{
            if classTestListingArray.count > 0 {
                evaluationActivitySubjectId = classTestListingArray[subpos].id
                print(evaluationActivitySubjectId)
            }
        }
        else{
          if evaluationActivitySubjectList.count > 0 {
              evaluationActivitySubjectId = evaluationActivitySubjectList[0].id
              print(evaluationActivitySubjectId)
           }
        }
        
        WebServiceApi.postUpdateEvaluationSubjectForMarkEntry(maxmark: maxMarkTextField.text!,classSubjectId: String(evaluationActivitySubjectId),onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            print(result)
            
            self.evaluationActivitySubjectResoponse.removeAll()
            self.evaluationActivitySubjectList.removeAll()
            self.evaluationActivitySubjectResoponse = result
            
            if isClassTestUpdation {
                
                
                //self.evaluationActivitySubjectList = evaluationActivitySubjectArray.parseThisDictionaryForevaluationActivitySubjectClassTestRecords(result)
                let subpos = self.selectSubjectPicker.selectedRow(inComponent: 0)
                self.setUpSubjectSelectionPickerForClasstest(isAfterClassTestCreation: true,position: subpos)
            }
            else{
                
                
                self.evaluationActivitySubjectList = evaluationActivitySubjectArray.parseThisDictionaryForevaluationActivitySubjectRecords(dictionary: result)
                self.setUpSubjectSelectionPickerForExam(isAfterExamCreation: true,position: subpos)
            }
            
            }, onFailure: { (error) in
               hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                
            }, duringProgress: { (progress) in
                
        })
       
    }
    
    func getStudentWithMarkforClassTest (classTestObj:classTest) {
        
        
            
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            
            studentList.removeAll()
            
            WebServiceApi.getStudentWithMarkforClassTestForMarkEntry(classTestId: String(classTestObj.id),onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                
                self.studentList = parseStudentExamListArray.parseThisDictionaryForsclassTestMarkListRecords(dictionary: result)
                print(self.studentList.count)
                self.performSegue(withIdentifier: segueConstants.segueToMarkentryDetailvc, sender: nil)
                
                }, onFailure: { (error) in
                     hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                    
                }, duringProgress: { (progress) in
                    
            })
            
        
        
        // add segue , pass vars
    }
    
    func getExamWithStudentsMark(isAfterExamCreation:Bool) {
        
        if self.ischangeClass || self.seguetoMarkEntryDetailedVc{
            hudControllerClass.showNormalHudToViewController(viewController: self)
        }
        weak var weakSelf = self
        examModification()
        
        if evaluationActivityList.count > 0  {
         exam = evaluationActivityList[selectExamPicker.selectedRow(inComponent: 0)]
         evaluationActivityId = exam.id
        }
        if classSubjectList.count > 0 {
         subject = classSubjectList[selectSubjectPicker.selectedRow(inComponent: 0)]
         classSubjectId = subject.id
        }
        
        WebServiceApi.getstudentEvaluationSubjectListForMarkEntry(classId: String(classId),activityId: String(evaluationActivityId),subjectId: String(classSubjectId),onSuccess: { (result) in
            if self.ischangeClass || self.seguetoMarkEntryDetailedVc {
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                self.ischangeClass = false
                self.seguetoMarkEntryDetailedVc = false
            }
            self.maxMarkTextField.isUserInteractionEnabled = true
            
            
            if !self.checkIsEvaluationActivitySubjectAddedOrNot(result: result){
                self.examSubjectValidationlabel.isHidden = false
                self.examSubjectValidationlabel.text = "It seem exam didnot added for this subject. You can add it by entering  max mark"
                self.maxMarkTextLabel.isHidden = false
                self.maxMarkTextField.isHidden = false
                self.maxMarkTextField.text = ""
                self.maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Mark", attributes: self.attributes)
                self.addExamButton.setTitle("Add Exam", for: .normal)
                
            }
            else{
                
                self.evaluationActivitySubjectResoponse.removeAll()
                self.evaluationActivitySubjectList.removeAll()
                self.evaluationActivitySubjectResoponse = result
                self.evaluationActivitySubjectList = evaluationActivitySubjectArray.parseThisDictionaryForevaluationActivitySubjectRecords(dictionary: result)
                
                
                
                if self.evaluationActivitySubjectList.count > 0 {
                  self.evaluationActivitySubjectMaxMark = self.evaluationActivitySubjectList[0].maxMark
                }
                if self.evaluationActivitySubjectMaxMark == 0 {
                    self.maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Mark",attributes: self.attributes)
                    self.examSubjectValidationlabel.isHidden = false
                    self.examSubjectValidationlabel.text = "It seem max mark didnot set. You can set it by entering  max mark"
                    
                    self.maxMarkTextLabel.isHidden = false
                    self.maxMarkTextField.isHidden = false
                    self.maxMarkTextField.text = ""
                    self.maxMarkTextField.attributedPlaceholder = NSAttributedString(string: "Mark", attributes: self.attributes)
                    self.addExamButton.setTitle("Update Exam", for: .normal)

                }
                else{
                    self.examSubjectValidationlabel.isHidden = true
                    self.addExamButton.setTitle("Ok", for: .normal)
                    self.maxMarkTextLabel.isHidden = false
                    self.maxMarkTextField.isHidden = false
                    self.maxMarkTextField.text = String(self.evaluationActivitySubjectMaxMark)
                    self.maxMarkTextField.isUserInteractionEnabled = false
                    if isAfterExamCreation{
                    self.setStudentsWithMarkToRecylerView(isForClassTest: false)
                    }
                }
                

            }
            
            }, onFailure: { (error) in
                if self.ischangeClass || self.seguetoMarkEntryDetailedVc{
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    self.ischangeClass = false
                    self.seguetoMarkEntryDetailedVc = false
                }
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                self.noExamModification()
            }, duringProgress: { (progress) in
                
        })

        
    }
    
    func checkIsEvaluationActivitySubjectAddedOrNot(result:[String:AnyObject])-> Bool {
        
        if let studentEvaluationSubjectList = result["studentEvaluationSubjectList"] as? [String:AnyObject] {
            if (studentEvaluationSubjectList["examDetails"] as? [String:AnyObject]) != nil {
                evaluationActivitySubjectAdded = true
                return true
            }
            else{
                evaluationActivitySubjectAdded = false
                return false
            }
          
        }
       return false
    }
    
    func setStudentsWithMarkToRecylerView(isForClassTest:Bool) {
        self.studentList.removeAll()
//        if isForClassTest {
//            studentList = parseStudentExamListArray.parseThisDictionaryForstudentEvaluationSubjectStudentListRecords(evaluationActivitySubjectResoponse)
//        }
        //else{
          studentList = parseStudentExamListArray.parseThisDictionaryForstudentEvaluationSubjectStudentListRecords(dictionary: evaluationActivitySubjectResoponse)
       // }
        //segue to next page.....
        self.performSegue(withIdentifier: segueConstants.segueToMarkentryDetailvc, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == segueConstants.segueToMarkentryDetailvc {
            markentry        = segue.destination as! EmloyeeMarkEntryDetailVC
            markentry.classId = classId
            markentry.className = className
            markentry.studentList = studentList
            markentry.isclassTest = isClassTest
            markentry.delegate  = self
            if isClassTest {
                let subpos = selectSubjectPicker.selectedRow(inComponent: 0)
                if classTestListingArray.count > 0 {
                 examclassTest = classTestListingArray[subpos]
                 markentry.examclassTest = examclassTest
                 markentry.maxMark = classTestMaxMarkValue
                }
                
            }
            else{
                markentry.evaluationActivitySubjectResoponse = evaluationActivitySubjectResoponse
                markentry.evaluationActivitySubjectList = evaluationActivitySubjectList
                markentry.maxMark = evaluationActivitySubjectMaxMark
            }
            seguetoMarkEntryDetailedVc = true
        }
    }
    
    func dismissFromSavedViewController() {
       markentry.dismiss(animated: true, completion: nil)
      self.dismiss(animated: true, completion: nil)
    }
    
}
