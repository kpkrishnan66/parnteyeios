//
//  ForgotPasswordVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 01/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordVC: UIViewController,CommonHeaderProtocol,UITextFieldDelegate {
    
    
    @IBOutlet var forgotPasswordImage: UIImageView!
    
    @IBOutlet var submitOTPView: UIView!
    
    @IBOutlet var enterOTPTextField: MasterSubClassTextField!
    
    @IBOutlet var submitOTP: UIButton!
    
    @IBOutlet var verificationMessage: UILabel!
    
    @IBOutlet var otpTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var headerForgotPassword: CommonHeader!
    @IBOutlet var supportCallView: UIView!
    
    @IBOutlet var supportMobNo: UILabel!
    @IBOutlet var callIcon: UIImageView!
    var mobileNumber:String = ""
    
    override func viewDidLoad() {
        setupUI()
        setUpHeader()
        
    }
    //Header delegates
    
    func showHamburgerMenuOrPerformOther() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showSwitchSibling(fromButton: UIButton) {
        
    }
    private func checkIfTextFieldsAreValid()->Bool{
        var isValid = false
        
        if enterOTPTextField.text == "" {
            
            enterOTPTextField.vibrate()
            enterOTPTextField.changeTextFieldUIToShowError(true)
            isValid = false
        }
        else
        {
            isValid = true
        }
        return isValid
    }
    
    
    @IBAction func postOTPAction(_ sender: Any) {
    
        if checkIfTextFieldsAreValid(){
            WebServiceApi.postUserOTP(mobNo: mobileNumber,OTP: enterOTPTextField.text!,
                                     
                                     onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: self)
                                        let rt = RootClass.init(fromDictionary: result as NSDictionary)
                                        if rt?.failureReason == nil {
                                            // setting the user defaults to show that we are logged in
                                            
                                            if let userModel = userModel(makeUserModelFromRootObject: rt!){
                                                
                                                    if userModel.currentTypeOfUser == .guardian {
                                                        DBController().addUserToDB(userModel)
                                                        UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)
                                                        
                                                        
                                                        DBController().addUserToDB(userModel)
                                                        let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                                                        let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                                                        ChngePwdVC.isForgotPassword = true
                                                        ChngePwdVC.readOTP = self.enterOTPTextField.text!
                                                        
                                                        ChngePwdVC.mobNo = self.mobileNumber
                                                        self.present(ChngePwdVC, animated: true, completion: nil)
                                                        
                                                }
                                            }
                                            
                                            
                                        
                                        
                                        }
                                        else
                                        {       AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .failure)
                                        }
                                        
                                        
                },
                                     onFailure: { (error) in
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
                },
                                     duringProgress: { (progress) in
                                        
            })
        }
        
    }
    
    private func setUpHeader(){
        
    headerForgotPassword.SetupForForgotPassword()
    headerForgotPassword.delegate = self
    
   }
    private func setupUI(){
        
    forgotPasswordImage.image = UIImage(named: "forgotpasswordunlockicon")
    callIcon.image = UIImage(named: "callicon")
    //submitOTPView.layer.cornerRadius = CGFloat(5)
    submitOTPView.layer.borderWidth = CGFloat(1)
    let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    submitOTPView.layer.borderColor = color.cgColor
    supportCallView.layer.borderWidth = CGFloat(1)
    let colorcallicon = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
    supportCallView.layer.borderColor = colorcallicon.cgColor
    verificationMessage.text = "An OTP has been send to the registered mobile number "+mobileNumber+" please verify and proceed"
    let attributes = [
        NSAttributedStringKey.foregroundColor: UIColor.black,
        NSAttributedStringKey.font : UIFont(name: "Roboto-Thin", size: 17)! // Note the !
    ]
    
    enterOTPTextField.attributedPlaceholder = NSAttributedString(string: "OTP", attributes:attributes)
    enterOTPTextField.keyboardType = UIKeyboardType.numberPad
    let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(self.callIconTapped))
    callIcon.isUserInteractionEnabled = true
    callIcon.addGestureRecognizer(tapGestureRecognizer)
    otpTextFieldHeight.constant = 50
        
  }

    @objc func callIconTapped(img: AnyObject)
    {
        let supportNumber:String = "+918157032122"
        if let url = NSURL(string: "tel://\(supportNumber)") {
            UIApplication.shared.openURL(url as URL)
        }
    }
    
    
    
    }




