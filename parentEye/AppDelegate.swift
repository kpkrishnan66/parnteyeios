 //
 //  AppDelegate.swift
 //  parentEye
 //
 //  Created by Martin Jacob on 12/04/16.
 //  Copyright © 2016 Scientia. All rights reserved.
 //
 
 import UIKit
 import IQKeyboardManagerSwift
 import SlideMenuControllerSwift
 import GoogleMaps
 import Firebase
 import FirebaseAnalytics
 import FirebaseMessaging
 import UserNotifications
 import Crashlytics
 import Fabric
 
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var flag:Int = 0
    var notificationEnabled:Bool = false
    var noti_detail_array = [notificationDetails]()
    let googleMapsApiKey = "AIzaSyBjdXGqSFgnuvW2aQRmdxrDFTn4KCGNFoQ"
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        print("applicationdidFinishLaunchingWithOptions")
        // Override point for customization after application launch.
        //fatalError()
        
        IQKeyboardManager.sharedManager().enable = true
        
        GMSServices.provideAPIKey(googleMapsApiKey)
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // MARK: Init Notification
        appRegisterForPushNotification(application: application)
        
        // Add observer for InstanceId token refresh callback.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(_:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        if let token = InstanceID.instanceID().token(){
            print("TOKEN.."+token)
        }
        Fabric.sharedSDK().debug = true
        Fabric.with([Crashlytics.self])
        
        
        
        configureApp()
        
        if UserDefaults.standard.object(forKey: userDerfaultConstants.isLoggedIn) != nil{
            //setThisViewControllerWithIdentifierAsRoot("LoginViewController", InStoryBoard: "Main")
            maketabBarRoot()
        }else {
            application.applicationIconBadgeNumber = 0
            #if SREEMAHARSHIDEVELOPMENT
                setThisViewControllerWithIdentifierAsRoot(identifier: "sreeMaharshiLoginViewController", InStoryBoard: "Main")
            #else
                setThisViewControllerWithIdentifierAsRoot(identifier: "LoginViewController", InStoryBoard: "Main")
            #endif
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        print("applicationWillResignActive")
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("applicationDidEnterBackground")
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        print("applicationWillEnterForeground")
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        print("applicationDidBecomeActive")
        /** check if device details send or not
         */
        if UserDefaults.standard.object(forKey: userDerfaultConstants.deviceTokenRegistered) != nil{
            let status = UserDefaults.standard.object(forKey: userDerfaultConstants.deviceTokenRegistered) as! String
            if status == "false"{
                if UserDefaults.standard.object(forKey: userDerfaultConstants.deviceToken) != nil{
                    let deviceToken = UserDefaults.standard.object(forKey: userDerfaultConstants.deviceToken)
                    uploadDeviceDetail.deviceDetails(deviceToken as! String)
                }
            }
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        print("applicationWillTerminate")
        application.applicationIconBadgeNumber = 0
    }
    
 }
 
 
 extension AppDelegate{
    /**
     Register for push notification
     
     parameter application: Application instance
     */
    
    func appRegisterForPushNotification(application:UIApplication) {
        //[START register_for_notifications]
        DispatchQueue.main.async(execute:  {
            let isRegisteredNotifications = application.isRegisteredForRemoteNotifications
            if !isRegisteredNotifications{
                if UserDefaults.standard.object(forKey: userDerfaultConstants.isLoggedIn) != nil{
                   self.askPermissionForPushNotification()
                }
            }
            application.registerForRemoteNotifications()
            // Use Firebase library to configure APIs
            FirebaseApp.configure()
        })
       
        
    }
    
    @objc func tokenRefreshNotification(_ notification: Notification){
        print(#function)
        if let refreshToken = InstanceID.instanceID().token(){
            print("Notification: refresh token from FCM -> \(refreshToken)")
            UserDefaults.standard.set(refreshToken, forKey: userDerfaultConstants.deviceToken)
            UserDefaults.standard.set("false",forKey: userDerfaultConstants.deviceTokenRegistered)
        }
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Notification: Firebase FCM delegate remote message")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        print("ReceiveRemoteNotification")
        // Print full message.
        print(userInfo)
    }
   
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        
        
        // Print full message
        print("ReceiveRemoteNotification")
        print(userInfo)
        let notificationContentType = userInfo["notificationContentType"] as? String ?? ""
        let notificationContentReferenceId = userInfo["notificationContentReferenceId"] as? String ?? ""
        
        print("not_content_type ===="+notificationContentType)
        print("not_content_reference_id ===="+notificationContentReferenceId)
        completionHandler(UIBackgroundFetchResult.newData)
        if application.applicationState == .active {
            print("**active**")
            let childController = window?.rootViewController?.childViewControllers
            let tabbarVC = childController![0] as? ParentEyeTabbar
            tabbarVC?.getUnreadCount()

        }else{
            fetchAndshowNotification(notificationContentType,notificationContentReferenceId)
        }
    }
    
    // Called when APNs has assigned the device a unique token
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        
        // Persist it in your backend in case it's new
    }
    
    func fetchAndshowNotification(_ notificationContentType:String,_ notificationContentReferenceId:String){
        print("fetch and show noti")
        var user:userModel?
        user = DBController().getUserFromDB()
        let userId = user?.userId
        if userId != nil{
            WebServiceApi.getNotificationDetails(noti_content_type: notificationContentType,noti_content_reference_id:notificationContentReferenceId,user_Id:userId!,onSuccess: { (result) in
                var notificationDetailArray = [notificationDetails]()
                notificationDetailArray = parseNotificationDetailsArray.parseThisDictionaryForotificationDetailsRecords(dictionary: result)
                
                self.transmitNotificationByType(notificationDetailArray,user: user)
           
        }, onFailure: { (error) in
           
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        }
        }
    }
    
    func transmitNotificationByType(_ noti_detail_array:[notificationDetails],user:userModel?){

        let childController = window?.rootViewController?.childViewControllers
        let tabbarVC = childController![0] as? ParentEyeTabbar
        
        if noti_detail_array.count > 0{
            if noti_detail_array[0].userRoleId != user?.currentUserRoleId {
                switchUserRole(user: user,tabbarVC: tabbarVC!,noti_detail_array)
                
            }else{
                if noti_detail_array[0].entryType == .diary{
                    tabbarVC?.shouldLoadDiary = true
                    tabbarVC?.diaryToBeLoaded = noti_detail_array[0].diaryArray
                    if noti_detail_array[0].studentId != ""{
                       for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    if tabbarVC?.selectedIndex == 0{
                        let navCtrl = tabbarVC?.viewControllers![0] as! UINavigationController
                        let diaryVC = navCtrl.viewControllers[0] as! SchoolDiaryListing
                        diaryVC.viewDidAppear(false)
                        tabbarVC?.selectedIndex = 0
                    }else{
                        tabbarVC?.selectedIndex = 0
                    }
                    
                    
                    
                }else if noti_detail_array[0].entryType == .notice{
                    tabbarVC?.shouldLoadNotification = true
                    tabbarVC?.noticeToBeLoaded = noti_detail_array[0].noticeArray
                    if noti_detail_array[0].studentId != ""{
                        for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    if tabbarVC?.selectedIndex == 1{
                        let navCtrl = tabbarVC?.viewControllers![1] as! UINavigationController
                        let hwVC = navCtrl.viewControllers[0] as! NotificationListing
                        if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                            navCtrl.popToRootViewController(animated: false)
                        }
                        hwVC.viewDidAppear(false)
                        tabbarVC?.selectedIndex = 1
                    }else{
                        tabbarVC?.selectedIndex = 1
                    }
                    
                }else if noti_detail_array[0].entryType == .homeWork {
                    tabbarVC?.shouldLoadHomework = true
                    tabbarVC?.homeworkToBeLoaded = noti_detail_array[0].homeworkArray
                    if noti_detail_array[0].studentId != ""{
                        for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    if tabbarVC?.selectedIndex == 3{
                        let navCtrl = tabbarVC?.viewControllers![3] as! UINavigationController
                        let hwVC = navCtrl.viewControllers[0] as! HomeworkVC
                        if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                            navCtrl.popToRootViewController(animated: false)
                        }
                        hwVC.viewDidAppear(false)
                        tabbarVC?.selectedIndex = 3
                    }else{
                        tabbarVC?.selectedIndex = 3
                    }
                    
                }else if noti_detail_array[0].entryType == .message{
                    tabbarVC?.shouldLoadMessage = true
                    tabbarVC?.messageToBeLoaded = noti_detail_array[0].messageArray
                    if tabbarVC?.selectedIndex == 4{
                        let navCtrl = tabbarVC?.viewControllers![4] as! UINavigationController
                        let msgVC = navCtrl.viewControllers[0] as! MessagesListingVC
                        if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                            navCtrl.popToRootViewController(animated: false)
                        }
                        msgVC.viewDidAppear(false)
                        tabbarVC?.selectedIndex = 4
                    }else{
                        tabbarVC?.selectedIndex = 4
                    }
                    
                }else if noti_detail_array[0].entryType == .StudentLeaveRecord{
                    tabbarVC?.shouldLoadLeaveRecord = true
                    if noti_detail_array[0].studentId != ""{
                        for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    if let indexVal = tabbarVC?.selectedIndex{
                        let numValue = indexVal as NSNumber
                        if numValue == 2{
                            let navCtrl = tabbarVC?.viewControllers![2] as! UINavigationController
                            let hVC = navCtrl.viewControllers[0] as! HomeVC
                            if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                                navCtrl.popToRootViewController(animated: false)
                            }
                            hVC.viewDidAppear(false)
                            tabbarVC?.selectedIndex = 2
                        }else{
                            tabbarVC?.selectedIndex = 2
                    }
                }
                }else if noti_detail_array[0].entryType == .StudentLeaveSubmit{
                    tabbarVC?.shouldLoadStudentLeaveSubmit = true
                    tabbarVC?.StudentLeaveSubmitToBeLoaded = noti_detail_array[0].StudentLeaveSubmitRecordArray
                    tabbarVC?.selectedIndex = 2
                }else if noti_detail_array[0].entryType == .externalNotification{
                    tabbarVC?.shouldLoadExternalNotification = true
                    tabbarVC?.externalNotificationToBeLoaded = noti_detail_array[0].externalNotificationArray
                    tabbarVC?.selectedIndex = 2
                }
            }
        }
        
    }
    
    func switchUserRole(user:userModel?,tabbarVC:ParentEyeTabbar,_ noti_detail_array:[notificationDetails]){
        
        WebServiceApi.getMultiRoleSwitchForEmployee(userId: (user?.userId)!,switchAs: (user?.switchAs)!,
                                                    
                                                    onSuccess: { (result) in
                                                        
                                                        let rt = RootClass.init(fromDictionary: result as NSDictionary)
                                                        if rt?.failureReason == nil {
                                                            // setting the user defaults to show that we are logged in
                                                            //DBController().clearDataBse()
                                                            if let userModel = userModel(makeUserModelFromRootObject: rt!){
                                                                
                                                               
                                                                    DBController().addUserToDB(userModel)
                                                                    UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)
                                                                    
                                                                    let app = UIApplication.shared.delegate as! AppDelegate
                                                                    app.noti_detail_array = noti_detail_array
                                                                    app.notificationEnabled = true
                                                                    app.maketabBarRoot()
                                                                
                                                            }
                                                            
                                                        }
                                                        else
                                                        {       AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .success)
                                                        }
                                                        
                                                        
        },
                                                    onFailure: { (error) in
                                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                        
        },
                                                    duringProgress: { (progress) in
                                                        
        })
    }
    
    func askPermissionForPushNotification(){
        //[START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For ios 10 data message (sent via FCM)
            Messaging.messaging().remoteMessageDelegate = self
            print("Notification: registration for ios >= 10 using UNUserNotificationCenter")
        }else{
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            print("Notification: registration for ios < 10 using basic notification center")
        }
    }
 }
 
 extension AppDelegate{
    
    /**
     configuration Methods
     */
    
    func setThisViewControllerWithIdentifierAsRoot(identifier:String, InStoryBoard storyBoard:String) -> Void {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil);
        let VC = storyboard.instantiateViewController(withIdentifier: identifier)
        window?.rootViewController = VC
    }
    
    func maketabBarRoot() -> Void {
        checkForUpdates()
        let storyboard     = UIStoryboard(name: "Main", bundle: nil);
        let VCstoryboard   = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let mainController = storyboard.instantiateViewController(withIdentifier: "mainTabBar") as! ParentEyeTabbar
        let slider         = VCstoryboard.instantiateViewController(withIdentifier: "hamburger") as! HamburgerMenu
        slider.delegate = mainController
        let sliderController = hamburger.addHambugerWithLeftController(leftController: slider ,mainController:mainController)
        window?.rootViewController = sliderController
        
        /** switch user role if push notification enabled **/
        if notificationEnabled{
            notificationEnabled = false
            let user = DBController().getUserFromDB()
            if noti_detail_array.count > 0 {
                if noti_detail_array[0].entryType == .diary{
                mainController.shouldLoadDiary = true
                mainController.diaryToBeLoaded = noti_detail_array[0].diaryArray
                if noti_detail_array[0].studentId != ""{
                    for pos in 0 ..< user!.Students!.count {
                        if user!.Students![pos].id == noti_detail_array[0].studentId{
                            user?.currentOptionSelected = pos
                            DBController().addUserToDB(user!)
                        }
                    }
                    
                }
                    if  mainController.selectedIndex == 0{
                        let navCtrl =  mainController.viewControllers![0] as! UINavigationController
                        let diaryVC = navCtrl.viewControllers[0] as! SchoolDiaryListing
                        diaryVC.viewDidAppear(false)
                         mainController.selectedIndex = 0
                    }else{
                         mainController.selectedIndex = 0
                    }
                
                }else if noti_detail_array[0].entryType == .notice{
                    mainController.shouldLoadNotification = true
                    mainController.noticeToBeLoaded = noti_detail_array[0].noticeArray
                    if noti_detail_array[0].studentId != ""{
                        for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    if mainController.selectedIndex == 1{
                        let navCtrl = mainController.viewControllers![1] as! UINavigationController
                        let hwVC = navCtrl.viewControllers[0] as! NotificationListing
                        if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                            navCtrl.popToRootViewController(animated: false)
                        }
                        hwVC.viewDidAppear(false)
                        mainController.selectedIndex = 1
                    }else{
                        mainController.selectedIndex = 1
                    }
                    
                }else if noti_detail_array[0].entryType == .homeWork{
                        mainController.shouldLoadHomework = true
                        mainController.homeworkToBeLoaded = noti_detail_array[0].homeworkArray
                        if noti_detail_array[0].studentId != ""{
                            for pos in 0 ..< user!.Students!.count {
                                if user!.Students![pos].id == noti_detail_array[0].studentId{
                                    user?.currentOptionSelected = pos
                                    DBController().addUserToDB(user!)
                                }
                            }
                            
                        }
                    if mainController.selectedIndex == 3{
                        let navCtrl = mainController.viewControllers![3] as! UINavigationController
                        let hwVC = navCtrl.viewControllers[0] as! HomeworkVC
                        if (UIApplication.shared.keyWindow?.rootViewController) != nil{
                            navCtrl.popToRootViewController(animated: false)
                        }
                        hwVC.viewDidAppear(false)
                        mainController.selectedIndex = 3
                    }else{
                        mainController.selectedIndex = 3
                    }
                        
              }else if noti_detail_array[0].entryType == .message{
               mainController.shouldLoadMessage = true
               mainController.messageToBeLoaded = noti_detail_array[0].messageArray
               mainController.selectedIndex = 4
              }else if noti_detail_array[0].entryType == .StudentLeaveRecord{
                    mainController.shouldLoadLeaveRecord = true
                    if noti_detail_array[0].studentId != ""{
                        for pos in 0 ..< user!.Students!.count {
                            if user!.Students![pos].id == noti_detail_array[0].studentId{
                                user?.currentOptionSelected = pos
                                DBController().addUserToDB(user!)
                            }
                        }
                        
                    }
                    mainController.selectedIndex = 2
             }else if noti_detail_array[0].entryType == .StudentLeaveSubmit{
                    mainController.shouldLoadStudentLeaveSubmit = true
                    mainController.StudentLeaveSubmitToBeLoaded = noti_detail_array[0].StudentLeaveSubmitRecordArray
                    mainController.selectedIndex = 2
                }else if noti_detail_array[0].entryType == .externalNotification{
                    mainController.shouldLoadExternalNotification = true
                    mainController.externalNotificationToBeLoaded = noti_detail_array[0].externalNotificationArray
                    mainController.selectedIndex = 2
                }
          }
        }
        
        self.window?.makeKeyAndVisible()
        //let slider = slide
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        let newVersionNo = nsObject as! String
        //NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey:"updateUserTimes")
        if let oldVersionNo = UserDefaults.standard.object(forKey: "versionNumber") as? String {
            if newVersionNo != oldVersionNo {
                UserDefaults.standard.set(newVersionNo, forKey:"versionNumber")
                if flag == 0 {
                    sync.syncNow()
                }
            }
            
        }
        else{
            flag = 1
            sync.syncNow()
        }
        
    }
    
    func checkForUpdates(){
        WebServiceApi.checkForUpdates(onSuccess: {(result) in
            if let tempResult = result["version"] as? String {
                let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
                let existingVersionNo = nsObject as! String
                if(tempResult != existingVersionNo){
                    UserDefaults.standard.set(tempResult, forKey:"appVersionFromWeb")
                }
            }
        }, onFailure: {(error) in
            
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }, duringProgress: {(progress) in
            
        })
    }
    
    func configureApp() {
        
        #if RAJAHDEVELOPMENT
            
            //webServiceUrls.baseURl     = "http://rajah.parenteye.org/index.php/parentEyeApi/"
            webServiceUrls.baseURl   = "http://188.166.223.141/parenteye_test/index.php/parentEyeApi/"
            ConstantsInUI.appAlertName = "Rajah School"
            ConstantsInUI.appName      = "Rajah"
            
        #elseif PEEVEESDEVELOPMENT
            
            //webServiceUrls.baseURl     = "http://www.peeveesparenteye.com/index.php/parentEyeApi/"
            //            webServiceUrls.url          = "http://www.peeveesparenteye.com/"
            //            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            
            ConstantsInUI.appAlertName = "Peevees"
            ConstantsInUI.appName      = "Peevees"
            
            webServiceUrls.url          = "http://188.166.223.141/parenteye_test/"
            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            
        #elseif PARENTEYEDEVELOPMENT
            
            //webServiceUrls.url          = "http://192.168.100.8/~scientia/parenteye-progresscard/"
            webServiceUrls.url          = "http://188.166.223.141/parenteye_test/"
            //webServiceUrls.url          = "http://188.166.223.141/parenteye_test_ios/"
            //webServiceUrls.url          = "http://192.168.0.23/KP/parenteye_test_push-ios/"
            //webServiceUrls.url          = "http://www.parenteye.org/"
            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            
            
            ConstantsInUI.appAlertName  = "ParentEye"
            ConstantsInUI.appName       = "ParentEye"
            
        #elseif EXCELDEVELOPMENT
            
            webServiceUrls.url          = "http://188.166.223.141/parenteye_test/"
            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            //            webServiceUrls.baseURl      = "http://excel.parenteye.org/index.php/parentEyeApi/"
            ConstantsInUI.appAlertName  = "Excel Public School"
            ConstantsInUI.appName       = "excel"
            
        #elseif SREEMAHARSHIDEVELOPMENT
            
            //              webServiceUrls.baseURl      = "http://www.sreemaharshi.parenteye.org/index.php/parentEyeApi/"
            
            webServiceUrls.baseURl      = "http://188.166.223.141/parenteye_test_ios/index.php/parentEyeApi/"
            //webServiceUrls.baseURl      = "http://192.168.0.1/~scientia/parenteye-fee/index.php/parentEyeApi/"
            ConstantsInUI.appAlertName  = "Sree Maharshi Vidyalaya Senior Secondary School"
            ConstantsInUI.appName       = "sreeMaharshi"
            
        #elseif VIDYACOUNCILDEVELOPMENT
            
            //webServiceUrls.baseURl      = "http://www.vidya.parenteye.org/index.php/parentEyeApi/"
            webServiceUrls.baseURl      = "http://188.166.223.141/parenteye_test/index.php/parentEyeApi/"
            //webServiceUrls.baseURl      = "http://192.168.100.2/~scientia/parenteye-fee/index.php/parentEyeApi/"
            ConstantsInUI.appAlertName  = "Vidya Council"
            ConstantsInUI.appName       = "Vidya Council"
            
        #elseif TIMEDIARYDEVELOPMENT
            //webServiceUrls.url          = "http://188.166.223.141/parenteye_video/"
            webServiceUrls.url          = "http://188.166.223.141/parenteye_test/"
            //webServiceUrls.url          = "http://192.168.0.104/parenteye_test_5.1/"
            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            //            webServiceUrls.url          = "http://www.timediary.org/"
            //            webServiceUrls.baseURl      = webServiceUrls.url+"index.php/parentEyeApi/"
            
            
            ConstantsInUI.appAlertName  = "Time Diary"
            ConstantsInUI.appName       = "Time Diary"
            
        #endif
        
    }
    
 }
 
