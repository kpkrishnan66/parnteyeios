//
//  AdmissionPayViewController.swift
//  parentEye
//
//  Created by scientia on 26/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase


class AdmissionPayViewController: UIViewController,CommonHeaderProtocol,paymentConfirmedProtocol,UIPopoverPresentationControllerDelegate,admissionSucessResponseProtocol {
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet var studentName: UILabel!
    @IBOutlet var className: UILabel!
    @IBOutlet var dateofbirth: UILabel!
    @IBOutlet var fatherName: UILabel!
    @IBOutlet var motherName: UILabel!
    @IBOutlet var mobileNumber: UILabel!
    @IBOutlet var emailId: UILabel!
    @IBOutlet var address: UILabel!
    @IBOutlet var payNowResponseMessage: UILabel!
    @IBOutlet var payNowButton: UIButton!
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var newAdmissionButton: UIButton!
    
    var user:userModel?
    var profilePic = ""
    var newAdmissionArray = [newAdmission]()
    let overlayTransitioningDelegate = OverlayTransitioningDelegate()
    let redcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let greenColur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
    var paymentType:String = ""
    var paymentId:String = ""
    var pdfViewer = PdfController()
    //var payNowButtonClicked:Bool = false
    var responseString:String = ""
    var responseHeader:String = ""
    var responseContent:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getUserDataFromDB()
        alterHeader()
        setupUI()
    }
    private func getUserDataFromDB() {
       user = DBController().getUserFromDB()
    }
    private func alterHeader() {
    header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.newAdmission, withProfileImageUrl: profilePic)
        header.multiSwitchButton.isHidden = true
        header.delegate = self
    }
    private func setupUI() {
        if newAdmissionArray.count > 0{
        studentName.text = newAdmissionArray[0].studentName
        className.text = newAdmissionArray[0].className
        dateofbirth.text = newAdmissionArray[0].dateOfBirth
        fatherName.text = newAdmissionArray[0].fatherName
        motherName.text = newAdmissionArray[0].motherName
        mobileNumber.text = newAdmissionArray[0].mobileNo
        emailId.text = newAdmissionArray[0].emailId
        address.text = newAdmissionArray[0].address
        payNowResponseMessage.text = newAdmissionArray[0].messageToDisplay
        }
        
        if let response = UserDefaults.standard.string(forKey: "feeResponse") {
            if response == ConstantsInUI.atomSuccessResponse {
                payNowButton.backgroundColor = redcolur
                payNowButton.setTitle("Receipt", for: .normal)
                cancelButton.setTitle("Exit", for: .normal)
                newAdmissionButton.isHidden = false
                newAdmissionButton.setTitle("New Admission",for: .normal)
                responseString = response
                if let id = UserDefaults.standard.string(forKey: "paymentId")  {
                    paymentId = id
                }
            }
            else{
                payNowButton.backgroundColor = greenColur
                payNowButton.setTitle("PayNow", for: .normal)
                cancelButton.setTitle("Cancel", for: .normal)
                newAdmissionButton.isHidden = true
            }
        }
        else{
            payNowButton.backgroundColor = greenColur
            payNowButton.setTitle("PayNow", for: .normal)
            cancelButton.setTitle("Cancel", for: .normal)
            newAdmissionButton.isHidden = true
        }
        
    }
    
    func showHamburgerMenuOrPerformOther() {
       
         exitFromNewAdmission()
    }
    
    func showSwitchSibling(fromButton: UIButton) {
        
    }
    
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func showAlert(admissionPayCancelConstant:String) {
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: admissionPayCancelConstant, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            let app = UIApplication.shared.delegate as! AppDelegate
            app.setThisViewControllerWithIdentifierAsRoot(identifier: "sreeMaharshiLoginViewController", InStoryBoard: "Main")
            
        }
        self.present(alert, animated: false, completion: nil)
    }

    
   
    @IBAction func payNowButtonClicked(_ sender: UIButton) {
    
        if responseString == ConstantsInUI.atomSuccessResponse {
            downloadReciept()
        }
        else{
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let admsnVC = storyboard.instantiateViewController(withIdentifier: "AdmissionPayPopupVC") as! AdmissionPayPopupVC;
        prepareOverlayVC(overlayVC: admsnVC)
        admsnVC.delegate = self
        admsnVC.newAdmissionArray = newAdmissionArray
        admsnVC.fromInvoiceDetailsvc = false
        Analytics.logEvent( "P_newAdmission_sreemaharshi_payNowBtn", parameters: nil)
        self.present(admsnVC, animated: false, completion: nil)
        }

    }
    
    private func prepareOverlayVC(overlayVC: UIViewController) {
        overlayVC.transitioningDelegate = overlayTransitioningDelegate
        overlayVC.modalPresentationStyle = .custom
                
    }
    
    
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
    exitFromNewAdmission()
    }
    
    func exitFromNewAdmission() {
        
        var admissionPayCancelConstant:String = ""
        
        if responseString == ConstantsInUI.atomSuccessResponse  {
            admissionPayCancelConstant = ConstantsInUI.admissionPaidCancel
            showAlert(admissionPayCancelConstant: admissionPayCancelConstant)
        }
        else{
            admissionPayCancelConstant = ConstantsInUI.admissionPayCancel
            showAlert(admissionPayCancelConstant: admissionPayCancelConstant)
        }

    }
    
    func getAtomResponse(feeResponse:String,paymentId id:String,paymentFeeDetailsArray array:[feeDetails]) {
        
        responseString = feeResponse
        paymentId = id
        //payNowButtonClicked = true
        if responseString == ConstantsInUI.atomSuccessResponse {
         newAdmissionButton.isHidden = false
         newAdmissionButton.setTitle("New Admission",for: .normal)
        }
        UserDefaults.standard.set(feeResponse, forKey: "feeResponse")
        UserDefaults.standard.set(paymentId, forKey:"paymentId")
        sendPaymentStatus()
    }
    
    func sendPaymentStatus () {
        var paymentType:String = ""
        paymentType = "UtilityFee"
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.postAdmissionPaymentStatus(response: responseString ,
                                                 id: paymentId,paymentType: paymentType,
                                       onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        print(result)
                                        if let ErrorResponse = result["error"] as? NSDictionary{
                                            if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                
                                                AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                                            }
                                        }
                                        else{
                                           
                                        }
                                        
                                        
                                        
                                        
            },
                                       onFailure: { (error) in
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
            },
                                       duringProgress: { (progress) in
                                        
        })
        
        

    }
    
    func downloadReciept () {
        weak var weakSelf = self
        let url = webServiceUrls.utilityFeeInvoiceDownload+"paymentId/"+paymentId
        pdfViewer.addPdfToVC(vc: weakSelf!, urlToBeLoaded:url)
        //pdfViewer.insertPdfToViewController(weakSelf!, urlOfPdf:NSURL(fileURLWithPath: url))
        //pdfViewer.showPdfInViewController(weakSelf!, pdfHavingUrl:url, "")

    }
        
    func admissionPayPopupVCDismissed(feeVersionChangedAlert message:String) {
        
        if responseString == ConstantsInUI.atomSuccessResponse {
            responseHeader = ConstantsInUI.paymentSuccessHeaderAlert
            responseContent = ConstantsInUI.paymentsucessContentAlert
            payNowResponseMessage.text = ConstantsInUI.paynowSucessResponseMessage
            payNowButton.backgroundColor = redcolur
            payNowButton.setTitle("Receipt", for: .normal)
            cancelButton.setTitle("Exit", for: .normal)
            //payNowButtonClicked = true
            UserDefaults.standard.set(ConstantsInUI.atomSuccessResponse, forKey:"atomResponse")
            sucesssAlert()
        }
        else if responseString == ConstantsInUI.atomFailedResponse {
            responseHeader = ConstantsInUI.paymentFailedHeaderAlert
            responseContent = ConstantsInUI.paymentFailedContentAlert
            //payNowButtonClicked = false
            UserDefaults.standard.set(ConstantsInUI.atomFailedResponse, forKey:"atomResponse")
            failedAlert()
        }
        else if message != "" {
            responseHeader = ConstantsInUI.appAlertName
            responseContent = message
            sucesssAlert()
        }
        else{
            //responseHeader = ConstantsInUI.paymentFailedHeaderAlert
            //responseContent = ConstantsInUI.paymentFailedContentAlert
            //payNowButtonClicked = false
            UserDefaults.standard.set(ConstantsInUI.atomFailedResponse, forKey:"atomResponse")
            //failedAlert()
        }

    }
    
    func  sucesssAlert() {
        
        let alrt =  AlertController.getSingleOptionAlertControllerWithMessage(mesasage: responseContent, titleOfAlert: responseHeader, oktitle: ConstantsInUI.done, OkButtonBlock: nil)
        self.present(alrt, animated: false, completion: nil)

        
    }
    
    func failedAlert() {
        
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: responseContent, titleOfAlert: responseHeader, oktitle: ConstantsInUI.tryagain, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            self.payNowButtonClicked(self.payNowButton)
            
        }
        self.present(alert, animated: false, completion: nil)

    }
    
    
    @IBAction func editOrNewAdmissionAction(_ sender: UIButton) {
    
            let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
            let stAdmnVC = storyboard.instantiateViewController(withIdentifier: "StudentAdmission") as! admissionVC
            stAdmnVC.delegate = self
            if responseString == ConstantsInUI.atomSuccessResponse {
               stAdmnVC.editAdmission = false
            }
            else{
              stAdmnVC.editAdmission = true
              stAdmnVC.newAdmissionArray = newAdmissionArray
            }
           self.present(stAdmnVC, animated: true, completion: nil)
    }
    
    func updateSucessResponseForsreemaharshi() {
       
    }
    func payStackResponse(feeResponse:String,paymentId referenceId: String,paymentFeeDetailsArray: [feeDetails]) {
        
    }
}
