//
//  AdmissionPayPopupVC.swift
//  parentEye
//
//  Created by scientia on 26/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import MPSDKKit
import CommonCrypto

protocol paymentConfirmedProtocol:class {
    func getAtomResponse(feeResponse:String,paymentId:String,paymentFeeDetailsArray:[feeDetails])
    func admissionPayPopupVCDismissed(feeVersionChangedAlert:String)
    func payStackResponse(feeResponse:String,paymentId: String,paymentFeeDetailsArray: [feeDetails])
}

class AdmissionPayPopupVC:UIViewController,UITextViewDelegate,XMLParserDelegate,paymentWebviewProtocol,NetBankingDelegate,CreditCardDelegate {
    
    @IBOutlet var mobNoTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var termsAndConditionsTextView: UITextView!
    @IBOutlet var confirmButton: UIButton!
   
    
    var newAdmissionArray = [newAdmission]()
    let paymentMethodPicker = UIPickerView()
    let cardPicker          = UIPickerView()
    weak var delegate:paymentConfirmedProtocol?
    var confirmActionButtonPressed:Bool = false
    var paymentFeeDetailsArray = [feeDetails]()
    var feeResponse:String = ""
    var paymentId:String = ""
    var fromInvoiceDetailsvc:Bool = false
    var user:userModel?
    var totalAmountValue:String = ""
    var invoiceDetailsVCArray = [invoiceList]()
    var isPaystackAction:Bool = false
    var isAtomRespond:Bool = false
    
    
    override func viewDidLoad() {
        getUserDataFromDB()
        setupUI()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if isPaystackAction == true {
          delegate?.admissionPayPopupVCDismissed(feeVersionChangedAlert: "")
        }
        else{
        if confirmActionButtonPressed == true {
             if paymentFeeDetailsArray.count > 0 {
                if paymentFeeDetailsArray[0].currentFeeVersion != ConstantsInUI.currentFeeVersion {
                    delegate?.admissionPayPopupVCDismissed(feeVersionChangedAlert: paymentFeeDetailsArray[0].needToUpdateMessage)
                }
                else{
                     delegate?.admissionPayPopupVCDismissed(feeVersionChangedAlert: "")
                }
            }
            else{
            delegate?.admissionPayPopupVCDismissed(feeVersionChangedAlert: "")
            }
         }
        }
    }
    
    
    private func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        
    }
    
    private func setupUI() {
        
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.lightGray,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Regular", size: 15)! // Note the !
        ]
        
        mobNoTextField.attributedPlaceholder = NSAttributedString(string: "  Mobile No", attributes:attributes)
        mobNoTextField.textColor = UIColor.black
        mobNoTextField.keyboardType = UIKeyboardType.numberPad
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "  Email", attributes:attributes)
        emailTextField.textColor = UIColor.black
        emailTextField.keyboardType = UIKeyboardType.emailAddress

        let borderMob = CALayer()
        let borderEmail = CALayer()
        let width = CGFloat(2.0)
        borderMob.borderColor = UIColor.black.cgColor
        borderEmail.borderColor = UIColor.black.cgColor
        borderMob.frame = CGRect(x: 0, y: mobNoTextField.frame.size.height - width, width:  mobNoTextField.frame.size.width, height: mobNoTextField.frame.size.height)
        borderEmail.frame = CGRect(x: 0, y: emailTextField.frame.size.height - width, width:  emailTextField.frame.size.width, height: emailTextField.frame.size.height)
        borderMob.borderWidth = width
        borderEmail.borderWidth = width
        mobNoTextField.layer.addSublayer(borderMob)
        emailTextField.layer.addSublayer(borderEmail)
        mobNoTextField.layer.masksToBounds = true
        emailTextField.layer.masksToBounds = true
        if fromInvoiceDetailsvc == true {
            mobNoTextField.text = user?.mobileNo
            confirmButton.setTitle("CONFIRM \u{20B9} "+totalAmountValue, for: .normal)
        }
        if newAdmissionArray.count > 0 {
         mobNoTextField.text = newAdmissionArray[0].mobileNo
         emailTextField.text = newAdmissionArray[0].emailId
         totalAmountValue = newAdmissionArray[0].amount
         confirmButton.setTitle("CONFIRM \u{20B9} "+newAdmissionArray[0].amount, for: .normal)
        }
        
        mobNoTextField.textColor = UIColor.black
        self.termsAndConditionsTextView.dataDetectorTypes = UIDataDetectorTypes.link
        let StringOne = " terms and conditions"
        let StringTwo = NSMutableAttributedString(string:"By confirming you are agreeing")
        let attributedString = NSMutableAttributedString(string: StringOne)
        attributedString.addAttribute(NSAttributedStringKey.link, value:"https://s3.amazonaws.com/parenteyeprod/terms-and-conditions-parenteye.pdf", range:(attributedString.string as NSString).range(of: "terms and conditions"))
        StringTwo.append(attributedString)
        self.termsAndConditionsTextView.attributedText = StringTwo
        confirmActionButtonPressed = false

     }
    
    
    
    
    @IBAction func confirmAction(_ sender: UIButton) {
    
        if fromInvoiceDetailsvc == true {
            if invoiceDetailsVCArray.count > 0 {
                if invoiceDetailsVCArray[0].paymentGateway == "PAYSTACK" {
                    isPaystackAction = true
                    Analytics.logEvent("Paystack_feeInvoice_paynowConfirm", parameters: nil)
                    if ((emailTextField.text != "") && (isValidEmail(testStr: emailTextField.text!))) {
                       postFeePaymentDetails()
                    }
                    else{
                       
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.studentAdmissionValidEmailId, stateOfMessage: .failure)
                    }
                }
                else{
                    isPaystackAction = false
                    Analytics.logEvent("Atom_feeInvoice_paynowConfirm", parameters: nil)
                    postFeePaymentDetails()
                }
            }
            
        }
        else{
            Analytics.logEvent("P_newAdmission_sreemaharshi_paynowConfirm", parameters: nil)
            createOrder()
        }
       
    }
        
    func createOrder() {
        
            var paymentType = ""
            var studentRefId = ""
            let schoolCode = "SREPAL2888727589"
            if newAdmissionArray.count > 0 {
              paymentType = newAdmissionArray[0].feeType
              studentRefId = String(newAdmissionArray[0].id)
            }
        
            weak var weakSelf = self
            hudControllerClass.showNormalHudToViewController(viewController: self)
            WebServiceApi.postPaymentOrder(schoolCode: schoolCode,
                                           FeeType: paymentType,
                                           studentRefId: studentRefId ,
                                        onSuccess: { (result) in
                                            
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            print(result)
                                            if let ErrorResponse = result["error"] as? NSDictionary{
                                                if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                    
                                                    AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                                                }
                                            }
                                            else{
                                               
                                                self.parseThisDictionaryToFeeDetailsArray(array: parseFeeDetailsArray.parseThisDictionaryForFeeDetailsRecords(dictionary: result))
                                               self.AtomPaymentAction()
                                            }
                                            
                                            
                                            
                                            
                },
                                        onFailure: { (error) in
                                            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                            
                },
                                        duringProgress: { (progress) in
                                            
            })
        

        
    }
    
    func postFeePaymentDetails() {
        
        weak var weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: self)
        WebServiceApi.postFeePaymentDetails(userRoleId: (user?.currentUserRoleId)!,invoiceDetailsVCArray: invoiceDetailsVCArray,totalAmountValue: totalAmountValue,
                                       onSuccess: { (result) in
                                        
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        print(result)
                                        if let ErrorResponse = result["error"] as? NSDictionary{
                                            if let errorMessage = ErrorResponse["errorMessage"] as? String{
                                                
                                                AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .failure)
                                            }
                                        }
                                        else{
                                            
                                             self.parseThisDictionaryToFeeDetailsArray(array: parseFeeDetailsArray.parseThisDictionaryForFeeDetailsRecords(dictionary: result))
                                            if self.isPaystackAction == false {
                                             self.AtomPaymentAction()
                                            }
                                            else{
                                              self.payStackPaymentAction()
                                            }
                                        }
                                        
                                        
                                        
                                        
            },
                                       onFailure: { (error) in
                                        hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                        
            },
                                       duringProgress: { (progress) in
                                        
        })
        
        
        
    }

    
    func parseThisDictionaryToFeeDetailsArray(array:[feeDetails]) {
        paymentFeeDetailsArray = array
    }
    
    func AtomPaymentAction() {
        if paymentFeeDetailsArray.count > 0 {
            if paymentFeeDetailsArray[0].currentFeeVersion != ConstantsInUI.currentFeeVersion {
                showAlert(updateMessage: paymentFeeDetailsArray[0].needToUpdateMessage)
            }
            else{
                
                
                let netBanking = NetBanking()
                netBanking.delegate = self
//                netBanking.merchantId =  "197"
//                netBanking.loginid = "197"
//                netBanking.txnscamt = "0"
//                netBanking.password = "Test@123"
//                netBanking.txncurr = "INR"//Fixed. Must be INR
//                netBanking.clientcode = "007"
//                netBanking.custacc = "100000036600"
//                netBanking.amt = "1000.00"
//                netBanking.txnid = "013" //txnidForCard = "2365F315"
//                netBanking.date = "15/11/2018 17:27:00"
//                netBanking.bankid = "2001"//Should be valid bank id
//                netBanking.signatureRequest = "KEY123657234"
//                netBanking.signatureResponse = "KEYRESP123657234"
//                netBanking.discriminator = "All"
//                netBanking.prodid = "NSE"
//                netBanking.isLive = false
//                netBanking.ru = "https://paynetzuat.atomtech.in/mobilesdk/param"
//                paymentId = paymentFeeDetailsArray[0].paymentId
//                self.present(netBanking, animated: true, completion: nil)
               
                
                if paymentFeeDetailsArray.count > 0 {
                    netBanking.merchantId = paymentFeeDetailsArray[0].merchantId
                    netBanking.loginid = paymentFeeDetailsArray[0].loginid
                    netBanking.txnscamt   = "1000"
                    netBanking.password = paymentFeeDetailsArray[0].password
                    netBanking.txncurr = paymentFeeDetailsArray[0].txncurr
                    netBanking.clientcode = paymentFeeDetailsArray[0].clientcodeForNetBanking
                    netBanking.custacc = paymentFeeDetailsArray[0].custacc
                    netBanking.amt = totalAmountValue
                    netBanking.txnid = paymentFeeDetailsArray[0].txnidForNetBanking
                    let dataFormatter:DateFormatter = DateFormatter()
                    dataFormatter.dateFormat = "dd/MM/yyyy hh:mm:ss"
                    netBanking.date = dataFormatter.string(from: Foundation.Date())
                    netBanking.signatureRequest = paymentFeeDetailsArray[0].signatureRequest
                    netBanking.signatureResponse = paymentFeeDetailsArray[0].signatureResponse
                    netBanking.discriminator = paymentFeeDetailsArray[0].discriminator
                    netBanking.prodid = paymentFeeDetailsArray[0].prodid
                    netBanking.isLive = true
                    netBanking.ru = paymentFeeDetailsArray[0].ru
                    paymentId = paymentFeeDetailsArray[0].paymentId
                }
               self.isAtomRespond = false
               self.present(netBanking, animated: true, completion: nil)
            }
        }
    }
    
    
    
    public func secondviewcontrollerDissmissed(resultString: String, withResponseKeys: [String], andResponseValues: [String]){
        print("hello")
        
        var getResult: String
        confirmActionButtonPressed = true
        getResult = resultString
        print("received---->\(getResult)\n")
        print("rsponse  keys")
        print("response values")
        if let position = withResponseKeys.index(of: "f_code"){
            let isIndexValid = andResponseValues.indices.contains(position)
            if isIndexValid{
                if andResponseValues[position] == ConstantsInUI.atomFailedResponse{
                    feeResponse = ConstantsInUI.atomFailedResponse
                }else if andResponseValues[position] == ConstantsInUI.atomSuccessResponse{
                    feeResponse = ConstantsInUI.atomSuccessResponse
                }
            }
        }
        print("fee response value = "+feeResponse)
        delegate?.getAtomResponse(feeResponse: feeResponse,paymentId: paymentId,paymentFeeDetailsArray: paymentFeeDetailsArray)
        self.dismiss(animated: false, completion: nil)
    }
    
    override func dismiss(animated flag: Bool,
                                                completion: (() -> Void)?)
    {
        
            super.dismiss(animated: flag, completion:completion)
        
        
        // Your custom code here...
    }
    
    private func showAlert(updateMessage:String){
        let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: updateMessage, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.update, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
            
        }) { (action) in
            let url  = URL(string: "itms-apps://itunes.apple.com/app/parenteye/id998502097")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
            }
            self.dismiss(animated: false, completion: nil)
        }
        self.present(alert, animated: false, completion: nil)
        
    }
    
    @IBAction func closeButtonClicked(_ sender: UIButton) {
    self.dismiss(animated: false, completion: nil)
    }
                
    func isValidEmail(testStr:String) -> Bool {
        print("validate emilId: \(testStr)")
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    
    func payStackPaymentAction() {
        
        let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let paymentVC = storyboard.instantiateViewController(withIdentifier: "paymentWebView") as! paymentWebView;
        paymentVC.delegate = self
        self.present(paymentVC, animated: false, completion: nil)
        paymentVC.webView.scrollView.isDirectionalLockEnabled = true
        paymentVC.webView.paginationMode = .topToBottom
        paymentVC.webView.paginationBreakingMode = .page
        paymentVC.webView.pageLength = paymentVC.webView.frame.size.width
        
        //let localfilePath = NSBundle.mainBundle().URLForResource("http://188.166.223.141/parenteye_ng/home/feeios.html", withExtension: "");
        //let localfilePath = NSURL(fileURLWithPath: "http://188.166.223.141/parenteye_ng/home/feeTemp.html")
        //let localfilePath = NSURLRequest(URL: NSURL(string: "http://188.166.223.141/parenteye_ng/home/feeTemp.html")!)
        //paymentVC.webView.loadRequest(localReuest)
        //paymentVC.webView.loadRequest(NSURLRequest(URL: NSURL(string: "http://188.166.223.141/parenteye_ng/home/feeTemp.html")!))
        
        var localfilePath = NSURL()
        if paymentFeeDetailsArray.count > 0 {
        let filePath =  NSURL(string: webServiceUrls.url+paymentFeeDetailsArray[0].urlios)
            localfilePath = filePath!
        }
        
        do {
            var myHTMLString = try NSString(contentsOf: localfilePath as URL, encoding: String.Encoding.utf8.rawValue)
            if paymentFeeDetailsArray.count > 0 {
                myHTMLString = NSString(format: myHTMLString,paymentFeeDetailsArray[0].publicKey,emailTextField.text!,totalAmountValue,paymentFeeDetailsArray[0].paymentId)
                paymentVC.webView.loadHTMLString(myHTMLString as String, baseURL: nil) //Add your js file here
                //admsnVC.webView.loadRequest(myRequest)
            }
            
        }
        catch let error as NSError
        {
            print("Error: \(error.description)")
        }

    }
    
    func paymentWebviewResponse(referenceId:String) {
        delegate?.payStackResponse(feeResponse: feeResponse,paymentId: referenceId,paymentFeeDetailsArray: paymentFeeDetailsArray)
        self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewControllerAnimated(true)
        self.removeFromParentViewController()
    }
    
}
