//
//  admissionVC.swift
//  parentEye
//
//  Created by scientia on 21/02/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol admissionSucessResponseProtocol:class {
    func updateSucessResponseForsreemaharshi()
}

class admissionVC:UIViewController,CommonHeaderProtocol,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet var header: CommonHeader!
    
    @IBOutlet var studentNameTextField: UITextField!
    
    @IBOutlet var studentClassName: UITextField!
    
    @IBOutlet var studentDob: UITextField!
    
    @IBOutlet var mobileNumberTextField: UITextField!
    
    @IBOutlet var emailIdTextField: UITextField!
    
    @IBOutlet var addressView: UIView!
    
    @IBOutlet var adrressLineoneTextField: UITextField!
    
    @IBOutlet var adrressLinetwoTextField: UITextField!
    
    @IBOutlet var LivingCity: UITextField!
    
    @IBOutlet var livingPincode: UITextField!
    
    @IBOutlet var studentResidentStatus: UISwitch!
    
    @IBOutlet var submitButton: UIButton!
    
    @IBOutlet var motherNameTextField: UITextField!
    
    @IBOutlet var fatherNameTextField: UITextField!
    
    @IBOutlet var studentResidentStatusSwitchHeight: NSLayoutConstraint!
    
    @IBOutlet var mainViewHeight: NSLayoutConstraint!
    
    @IBOutlet var studentResidentStatusHeight: NSLayoutConstraint!
    
    
    
    
   
    
        var user:userModel?
        var profilePic = ""
        var classList = [String]()
        let picker = UIPickerView()
        var studentDobDate = Foundation.Date()
        let overlayTransitioningDelegate = OverlayTransitioningDelegate()
        var studentResidentStatusVal:String = "0"
        weak var delegate:admissionSucessResponseProtocol?
        var newAdmissionArray = [newAdmission]()
        var editAdmission:Bool = false
        var isKeyboardShow:Bool = false

        override func viewDidLoad() {
            super.viewDidLoad()
            getUSerFromDB()
            alterHeader()
            self.fatherNameTextField.delegate = self
            self.motherNameTextField.delegate = self
            setupUI()
            getClassList()
        }

        fileprivate func getUSerFromDB() {
            user = DBController().getUserFromDB()
        }

        //Header delegates

        func showHamburgerMenuOrPerformOther() {

            let alert = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.message, titleOfAlert: ConstantsInUI.appAlertName, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in

            }) { (action) in
                self.dismiss(animated: true, completion: nil)

            }
            self.present(alert, animated: false, completion: nil)

        }

        func showSwitchSibling(fromButton: UIButton) {

        }

        fileprivate func alterHeader() {
            header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentAdmission, withProfileImageUrl: profilePic)
            header.multiSwitchButton.isHidden = true
            header.delegate = self
        }

        fileprivate func setupUI() {
            studentNameTextField.layer.borderWidth = CGFloat(1)
            studentClassName.layer.borderWidth = CGFloat(1)
            studentDob.layer.borderWidth = CGFloat(1)
            fatherNameTextField.layer.borderWidth = CGFloat(1)
            motherNameTextField.layer.borderWidth = CGFloat(1)
            mobileNumberTextField.layer.borderWidth = CGFloat(1)
            emailIdTextField.layer.borderWidth = CGFloat(1)
            addressView.layer.borderWidth = CGFloat(1)
            adrressLineoneTextField.layer.borderWidth = CGFloat(1)
            adrressLinetwoTextField.layer.borderWidth = CGFloat(1)
            LivingCity.layer.borderWidth = CGFloat(1)
            livingPincode.layer.borderWidth = CGFloat(1)

            let color = UIColor(red: 210.0/255.0, green: 210.0/255.0, blue: 210.0/255.0, alpha: 1.0)
            studentNameTextField.layer.borderColor = color.cgColor
            studentClassName.layer.borderColor = color.cgColor
            studentDob.layer.borderColor = color.cgColor
            fatherNameTextField.layer.borderColor = color.cgColor
            motherNameTextField.layer.borderColor = color.cgColor
            mobileNumberTextField.layer.borderColor = color.cgColor
            emailIdTextField.layer.borderColor = color.cgColor
            addressView.layer.borderColor = color.cgColor
            adrressLineoneTextField.layer.borderColor = color.cgColor
            adrressLinetwoTextField.layer.borderColor = color.cgColor
            LivingCity.layer.borderColor = color.cgColor
            livingPincode.layer.borderColor = color.cgColor

            #if EXCELDEVELOPMENT

                mainViewHeight.constant = 706
                studentResidentStatusHeight.constant = 21
                studentResidentStatusSwitchHeight.constant = 31
                self.studentResidentStatus.isHidden = false

            #elseif SREEMAHARSHIDEVELOPMENT

                mainViewHeight.constant = 675
                studentResidentStatusHeight.constant = 0
                studentResidentStatusSwitchHeight.constant = 0
                self.studentResidentStatus.isHidden = true

            #endif


            picker.delegate = self
            picker.dataSource = self
            picker.backgroundColor = UIColor.clear
            studentClassName.inputView = picker

            studentDob.delegate = self
            //studentDob.text = DateApi.convertThisDateToString(studentDobDate, formatNeeded: .ddMMyyyyWithHypen)

            studentResidentStatus.setOn(false, animated:true)

            let attributes = [
                NSAttributedStringKey.foregroundColor: UIColor.gray,
                NSAttributedStringKey.font : UIFont(name: "Roboto-Medium", size: 14)! // Note the !
            ]
            self.studentClassName.attributedPlaceholder = NSAttributedString(string: "Select Class", attributes:attributes)
            self.studentDob.attributedPlaceholder = NSAttributedString(string: "Select DOB",attributes: attributes)

        }

        func keyboardWillShow(_ notification: Notification) {
            isKeyboardShow = true

        }
        func keyboardWillHide(_ notification: Notification) {
            isKeyboardShow = false
        }



        func textFieldShouldReturn(_ textField: UITextField) -> Bool {

            if textField == studentNameTextField {
                textField.resignFirstResponder()
                studentClassName.becomeFirstResponder()
            }
            return true
        }


        //MARK: -UITextfield deleagte to dismiss keyboard

         func textFieldShouldBeginEditing(studentClassName: UITextField) -> Bool {
         studentClassName.resignFirstResponder()

         return true


         }
         func textFieldShouldendEditing(studentClassName: UITextField) -> Bool {
         studentClassName.resignFirstResponder()

         return true

         }
        func textFieldShouldendEditing(textField: UITextField) -> Bool {
         if textField == studentClassName {
         view.endEditing(true)
         studentDob.resignFirstResponder()
         return false
         }
         else{
         view.endEditing(false)
         textField.resignFirstResponder()
         return true
         }
         }
    
    @IBAction func studentDateTextFieldButonTapped(_ sender: UITextField) {
       selectDobpOPuP()
    }
    
    @IBAction func showDatePicker(_ sender: UIButton) {
        selectDobpOPuP()
    }
    


        func selectDobpOPuP() {
            if isKeyboardShow == false {

                let storyboard = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                let datePickerVC = storyboard.instantiateViewController(withIdentifier: "DatePickerController") as! DatePickerController;
                weak var weakSelf = self

                datePickerVC.viewLoadedCompletion = {(datePicker)->Void in


                    datePicker.maximumDate = Foundation.Date()
                }


                datePickerVC.valueChangedCompletion = {(date)->Void in


                    weakSelf!.studentDobDate = date as Foundation.Date
                    weakSelf!.studentDob.text = DateApi.convertThisDateToString(self.studentDobDate, formatNeeded: .ddMMyyyyWithHypen)

                }
                prepareOverlayVC(datePickerVC)

                self.present(datePickerVC, animated: false, completion: nil)

            }

        }

        fileprivate func prepareOverlayVC(_ overlayVC: UIViewController) {
            overlayVC.transitioningDelegate = overlayTransitioningDelegate
            overlayVC.modalPresentationStyle = .custom
        }
    
    @IBAction func residentStatusSwitchAction(_ sender: UISwitch) {
    
    
            if studentResidentStatus.isOn {
                print("Switch is on")
                studentResidentStatusVal = "0"
                studentResidentStatus.setOn(false, animated:true)
    
            } else {
                studentResidentStatusVal = "1"
                studentResidentStatus.setOn(true, animated:true)
            }
    
        }
        func getClassList(){

            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self


            WebServiceApi.getClassListForStudentAdmission(appName: ConstantsInUI.appName, onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if let tempResult = result["classLevel"] as? NSArray!{
                    self.classList.append("Select Class")
                    for index in tempResult {
                        self.classList.append(index as! String)
                    }
                }

                self.picker.reloadAllComponents()

            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                debugPrint(error.localizedDescription)

            }) { (progress) in

            }
        }

        //pickerView Delegate And DataSource
        func numberOfComponents(in colorPicker: UIPickerView) -> Int {
            return 1
        }

        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

            if classList.count > 0 {
                return classList.count
            }
            return 0
        }




        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
        {
            if classList.count > 0 {
                return classList[row]
            }
            return ""
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if classList.count > 0 {
                if classList[row] == "Select Class" {
                    self.studentClassName.text = "Select Class"
                }
                else{
                    self.studentClassName.text = " "+classList[row]
                }
            }
        }
    
    
    @IBAction func submitStudentAdmission(_ sender: UIButton) {
         let result = validateFields()
         if result.0 {
            #if EXCELDEVELOPMENT
             Analytics.logEvent("p_excel_admission_submit", parameters: nil)
            #elseif SREEMAHARSHIDEVELOPMENT
                Analytics.logEvent("p_sreemaharshi_admission_submit", parameters: nil)
            #endif
        let trimmedString = studentClassName!.text!.trimmingCharacters(in: CharacterSet.whitespaces)
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
            WebServiceApi.submitStudentAdmission(studentName: studentNameTextField.text!,
             className: trimmedString,
             studentDob: studentDob.text!,
             fatherName: fatherNameTextField!.text!,
             motherName: motherNameTextField!.text! ,
             mobileNumber: mobileNumberTextField!.text!,
             emailId: emailIdTextField!.text!,
             addressLineone: adrressLineoneTextField!.text!,
             addressLineTwo: adrressLinetwoTextField!.text!,
             livingCity: LivingCity!.text!,
             livingPincode: livingPincode!.text!,
             studentResidentVal: studentResidentStatusVal,
             appName: ConstantsInUI.appAlertName, onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if (result["error"] as? [String:AnyObject]) == nil{
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.submitStudentAdmissionSucess , stateOfMessage: .success)
                    #if SREEMAHARSHIDEVELOPMENT
                        self.parserThisDictionaryTonewAdmissionArray(parseNewAdmissionArray.parseThisDictionaryForNewAdmissionRecords(dictionary: result))
                        let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                        let admissionpayVC = storyboard.instantiateViewController(withIdentifier: "AdmissionPayViewController") as! AdmissionPayViewController
                        admissionpayVC.newAdmissionArray = self.newAdmissionArray
                        let data = NSKeyedArchiver.archivedData(withRootObject: self.newAdmissionArray)
                        UserDefaults.standard.set(data, forKey: "newAdmissionArray")
                        UserDefaults.standard.removeObject(forKey: "feeResponse")
                        self.present(admissionpayVC, animated: true, completion: nil)
                    #else
                        weakSelf!.dismiss(animated: false, completion: nil)
                        
                    #endif
                }else{
                    AlertController.showToastAlertWithMessage(message: ConstantsInUI.submitStudentAdmissionFailed, stateOfMessage: .failure)
               }
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            }) { (progress) in
                
            }
         }else {
                    AlertController.showToastAlertWithMessage(message: result.1, stateOfMessage: .failure)
            
        }
    }
    

        func isValidEmail(_ testStr:String) -> Bool {
            print("validate emilId: \(testStr)")
            let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            let result = emailTest.evaluate(with: testStr)
            return result
        }

        func validate(_ value: String) -> Bool {
            let PHONE_REGEX = "^\\d{3}\\d{3}\\d{4}$"
            let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
            let result =  phoneTest.evaluate(with: value)
            return result
        }


        fileprivate func validateFields() ->(Bool,String){
            var result   = true
            var ErrorMsg = ""
            var validFormat:Bool = true

            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "dd/MM/yyyy"

            if dateFormatterGet.date(from: studentDob.text!) != nil  {
                print("same format")
                validFormat = true
            } else {
                print("valid format")
                validFormat = false
            }

            if  studentNameTextField.text == "" {
                result = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidName

            }
            else if studentClassName.text == "" || studentClassName.text == "Select Class"{
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidClass
            }
            else if (studentDob.text == ""||studentDob.text == "Select DOB") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidDob
            }
            else if validFormat == false{
                result   = false
                ErrorMsg = ConstantsInUI.studentAmissionValidDobFormat
            }
            else if (fatherNameTextField.text == "") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidFatherName
            }
            else if (motherNameTextField.text == "") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidMotherName
            }
            else if ((mobileNumberTextField.text == "") || (!validate(mobileNumberTextField.text!))) {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidMobno
            }
            else if ((emailIdTextField.text != "") && (!isValidEmail(emailIdTextField.text!))) {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidEmailId
            }
            else if (adrressLineoneTextField.text == "" || adrressLinetwoTextField.text == "") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidAddress
            }
            else if (LivingCity.text == "") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidCity
            }
            else if (livingPincode.text == "") {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionValidPincode
            }
            else if (livingPincode.text?.characters.count != 6) {
                result   = false
                ErrorMsg = ConstantsInUI.studentAdmissionPincodeLength
            }


            return (result,ErrorMsg)
        }

        func parserThisDictionaryTonewAdmissionArray(_ array:[newAdmission]) {
            self.newAdmissionArray.removeAll()
            self.newAdmissionArray = array
        }
}
