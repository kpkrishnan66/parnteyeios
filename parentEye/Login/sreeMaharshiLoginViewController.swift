//
//  sreeMaharshiLoginViewController.swift
//  parentEye
//
//  Created by scientia on 12/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase


class sreeMaharshiLoginViewController:UIViewController,admissionSucessResponseProtocol {
    
    @IBOutlet var loginAction: UIButton!
    @IBOutlet var sliderCollection: SliderBaseView!
    @IBOutlet var admissionSucessResponseHeight: NSLayoutConstraint!
    
    var logoImages: [UIImage] = []
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderCollection.setForsreeMaharshiLoginImages()
        logoImages += [UIImage(named: "sreeMaharshiLogo1")!, UIImage(named: "sreeMaharshiLogo2.png")!,UIImage(named: "sreeMaharshiLogo3.png")!]
        sliderCollection.logoImages = logoImages
        sliderCollection.reloadSlidingView()
        sliderCollection.startTimer()
        admissionSucessResponseHeight.constant = 0
    }
    
        
    @IBAction func loginButtonAction(_ sender: Any) {
         admissionSucessResponseHeight.constant = 0		
         self.performSegue(withIdentifier: segueConstants.segueToLogin, sender: nil)
        
    }
    
    @IBAction func admissionAction(_ sender: Any) {
        if let outData = UserDefaults.standard.data(forKey: "newAdmissionArray") {
            if let dict = NSKeyedUnarchiver.unarchiveObject(with: outData) {
                let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                let admissionpayVC = storyboard.instantiateViewController(withIdentifier: "AdmissionPayViewController") as! AdmissionPayViewController
                admissionpayVC.newAdmissionArray = dict as! [newAdmission]
                self.present(admissionpayVC, animated: true, completion: nil)
            }
        }
        
        else{
        admissionSucessResponseHeight.constant = 0
        let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "StudentAdmission") as! admissionVC
        ChngePwdVC.delegate = self
        Analytics.logEvent("p_login_sreemaharshi_admission", parameters: nil)
        self.present(ChngePwdVC, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueConstants.segueToLogin{
            let logVC = segue.destination as! LoginViewController
            logVC.isstudentAdmission = true
        }
    }
    
    func updateSucessResponseForsreemaharshi() {
       admissionSucessResponseHeight.constant = 17
    }

}
