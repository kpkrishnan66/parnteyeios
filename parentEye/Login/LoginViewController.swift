//
//  LoginViewController.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class LoginViewController: UIViewController,UITextFieldDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    
    @IBOutlet weak var logoImageView:   UIImageView!
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var passwordHiddenVieHeight: NSLayoutConstraint!
    @IBOutlet var passwordIconHeight: NSLayoutConstraint!
    @IBOutlet var passwordicon: UIImageView!
    @IBOutlet var forgotPasswordButton: UIButton!
    @IBOutlet var passwordhiddenView: UIView!
    @IBOutlet weak var passwordTextField:   PasswordTextField!
    @IBOutlet var mobileNumberTextFieldView: UIView!
    
    @IBOutlet var applicationLogoTopConstraint: NSLayoutConstraint!
    @IBOutlet var applicationLogo: UIImageView!
    @IBOutlet var backGroundImageViewHight: NSLayoutConstraint!
    @IBOutlet var backGroundImageViewWidth: NSLayoutConstraint!
    @IBOutlet var applicationLogoWidth: NSLayoutConstraint!
    @IBOutlet var applicationLogoHeight: NSLayoutConstraint!
    @IBOutlet var backGroundImageView: UIImageView!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var mobNoTextField: MasterSubClassTextField!
    @IBOutlet var passwordTextFieldHeight: NSLayoutConstraint!
    @IBOutlet var signIn: UIButton!
    @IBOutlet var mobileicon: UIImageView!
    
    
    var window: UIWindow?
    var mobileNo:String = ""
    var isPassword:Bool = false
    var ishomeScreenVisible = false
    let redcolur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    let lightgraycolur = UIColor(red: 244.0/255.0, green: 244.0/255.0, blue: 244.0/255.0, alpha: 1.0)
    let excelBgColur   = UIColor(red: 233.0/255.0, green: 48.0/255.0, blue: 55.0/255.0, alpha: 1.0)
    var isstudentAdmission:Bool = true
  
    //MARK:View delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 17)! // Note the !
        ]
        
    #if EXCELDEVELOPMENT
        uichangeForExcel()
    #elseif SREEMAHARSHIDEVELOPMENT
        mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Mobile No", attributes:attributes)
        mobNoTextField.keyboardType = UIKeyboardType.numberPad
        if isstudentAdmission {
        forgotPasswordButton.isHidden = true
        backButton.isHidden = false
        }
        else{
        forgotPasswordButton.isHidden = true 
        backButton.isHidden = true
        }
        forgotPasswordButton.setTitle("Forgot Password", for: .normal)
        forgotPasswordButton.setTitleColor(UIColor.lightGray, for: .normal)
        backButton.backgroundColor = lightgraycolur
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(UIColor.lightGray, for: .normal)
    #else
        mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Mobile No", attributes:attributes)
        mobNoTextField.keyboardType = UIKeyboardType.numberPad
        isstudentAdmission = false
        forgotPasswordButton.isHidden = true
        backButton.isHidden = true
        forgotPasswordButton.setTitle("Forgot Password", for: .normal)
        forgotPasswordButton.setTitleColor(UIColor.lightGray, for: .normal)
        backButton.backgroundColor = lightgraycolur
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(UIColor.lightGray, for: .normal)

    #endif

        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        let newVersionNo = nsObject as! String
        UserDefaults.standard.set(newVersionNo, forKey:"versionNumber")
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        
    }
    
    //MARK:- Memory Warning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func uichangeForExcel() {
        forgotPasswordButton.isHidden = false
        backButton.isHidden = false
        forgotPasswordButton.setTitle(ConstantsInUI.studentAdmissionLogin, for: .normal)
        forgotPasswordButton.setTitleColor(UIColor.white, for: .normal)
        backButton.backgroundColor = UIColor.yellow
        backButton.setTitleColor(UIColor.white, for: .normal)
        signIn.backgroundColor = UIColor.yellow
        signIn.setTitleColor(UIColor.black, for: .normal)
        backButton.setTitleColor(UIColor.black, for: .normal)
        backButton.setTitle("Admission", for: .normal)
        isstudentAdmission = true
        mainView.backgroundColor = excelBgColur
        backGroundImageView.image = UIImage(named: "excelBg")
        applicationLogo.image = UIImage(named: "excelLogo")
        signIn.layer.cornerRadius = 18
        backButton.layer.cornerRadius = 18
        applicationLogoHeight.constant = 200
        applicationLogoWidth.constant = 150
        backGroundImageViewHight.constant = 200
        backGroundImageViewWidth.constant = 250
        let attributesForExcel = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 17)! // Note the !
        ]
        
        mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Mobile No", attributes:attributesForExcel)
        mobNoTextField.keyboardType = UIKeyboardType.numberPad
        mobileNumberTextFieldView.backgroundColor = UIColor.white
        mobileicon.image = UIImage(named: "mobilePicExcel")
        applicationLogoTopConstraint.constant = -175
        mobNoTextField.textColor = UIColor.white
       

    }
    // MARK:- ibActions
    
    /**
     Function to log the user in.If the fields are valid login process is initiated. If the login is success full
     */
    @IBAction func logMeIn(){
        
        
        if checkIfTextFieldsAreValid(){
            hudControllerClass.showNormalHudToViewController(viewController: self)
            weak var weakSelf = self
            if(!isPassword){
            mobileNo = mobNoTextField.text!
            }
            WebServiceApi.logMeInServerWithUserName(userName: mobileNo,password: mobNoTextField.text!,loggedin: isPassword,onSuccess: { (result) in
                
                hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                if let PasswordResponse = result["error"] as? NSDictionary{
                    if let errorMessage = PasswordResponse["errorMessage"] as? String{
                        
                        AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                }}
                if let userloginData = result["user"] as? NSDictionary{
                    if (userloginData["message"] as? String) != nil{
                        
                        self.askForPassword()
                        
                        
                    }
                    else{
                        self.isPassword = false
                    }
                        
                }
                
                if(!self.isPassword){
                    debugPrint("result from login",result)
                   self.parseLoginResult(fromDictionary: result as NSDictionary)
                }
                
                
                
                }, onFailure: { (error) in
                    hudControllerClass.hideHudInViewController(viewController: weakSelf!)
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                    
                    
                }, duringProgress: nil)
        }
        
        
    }
    
    
    private func getPasswordOnMail (){
       /* hudControllerClass.shownormalHudToViewController(self)
        weak var weakSelf = self
        WebServiceApi.logMeInServerWithUserName(emailIdTexField.text!, andPassword: "", onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(weakSelf!)
            weakSelf!.parseJsonDictionaryFromForgotPasword(result)
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(weakSelf!)
                AlertController.showToastAlertWithMessage(error.localizedDescription, stateOfMessage: .failure)
        }) { (progress) in
            
        } */
        
    }
    
    @IBAction func sayToServerThatYouForgotPassword (){
        
        
        if isstudentAdmission {
        }
        else {
        callForgotPassword()
        }
     }
        
    
    private func callForgotPassword() {
        
        WebServiceApi.getUserOTP(mobNo: self.mobileNo,
                                 
                                 onSuccess: { (result) in
                                    
                                    hudControllerClass.hideHudInViewController(viewController: self)
                                    if let Response = result["oneTimePassword"] as? NSDictionary{
                                        if let message = Response["message"] as? String{
                                            
                                            
                                            let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                                            let ForgotPwdVC = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
                                            ForgotPwdVC.mobileNumber = self.mobileNo
                                            AlertController.showToastAlertWithMessage(message: message, stateOfMessage: .success)
                                            
                                            self.present(ForgotPwdVC, animated: true, completion: nil)
                                            
                                            
                                        }
                                    }
                                    if let PasswordResponse = result["error"] as? NSDictionary{
                                        if let errorMessage = PasswordResponse["errorMessage"] as? String{
                                            
                                            AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                                        }}
                                    
                                    
            },
                                 onFailure: { (error) in
                                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                    
            },
                                 duringProgress: { (progress) in
                                    
        })

        
    }
    
    

    
    //MARK: Validations
    
    private func checkIfTextFieldsAreValid()->Bool{
        var isValid = false
        
        if mobNoTextField.text == "" {
        
            mobNoTextField.vibrate()
            mobNoTextField.changeTextFieldUIToShowError(true)
            isValid = false
        }
        else
        {
           isValid = true
        }
        return isValid
    }
    
    
    //MARK:- json validation
    
    private func parseJsonDictionaryFromForgotPasword(dictionary:[String:AnyObject]){
        if let ulogin = dictionary[resultsJsonConstants.loginConstants.userlogin] as? [String:String]{
            if let msg = ulogin[resultsJsonConstants.loginConstants.message]{
                let alrt =  AlertController.getSingleOptionAlertControllerWithMessage(mesasage: msg, titleOfAlert: "", oktitle: ConstantsInUI.ok, OkButtonBlock: nil)
                self.present(alrt, animated: false, completion: nil)
            }
        }
    }
    
    func askForPassword()
    {
    self.mobileicon.image = UIImage(named: "passwordImageForLogin")
    self.mobNoTextField.keyboardType = UIKeyboardType.default
    let attributes = [
        NSAttributedStringKey.foregroundColor: UIColor.black,
        NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 17)! // Note the !
    ]
    self.forgotPasswordButton.isHidden = false
    self.backButton.isHidden = false
    self.isstudentAdmission = false
    self.mobNoTextField.placeholder = ""
    self.mobNoTextField.text = ""
        
    #if EXCELDEVELOPMENT
        let attributesForExcel = [
            NSAttributedStringKey.foregroundColor: UIColor.white,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 17)! // Note the !
        ]
        self.mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributesForExcel)
        self.mobileicon.image = UIImage(named: "passwordPicExcel")
        backButton.layer.cornerRadius = 0
        backButton.backgroundColor = excelBgColur
        backButton.setTitleColor(UIColor.white, for: .normal)
        backButton.setTitle("Back", for: .normal)
        forgotPasswordButton.setTitle("ForgotPassword", for: .normal)
        mobNoTextField.textColor = UIColor.white
    #else
    self.mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes:attributes)
    #endif
        
    self.isPassword = true
    self.ishomeScreenVisible = true
        
    
    }
    
    func parseLoginResult(fromDictionary result: NSDictionary){
        let rt = RootClass.init(fromDictionary: result)
        if rt?.failureReason == nil {
            // setting the user defaults to show that we are logged in
            
            if let userModel = userModel(makeUserModelFromRootObject: rt!){
                
                if(ishomeScreenVisible){
                    DBController().addUserToDB(userModel)
                    UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)
                    askPermissionForPushNotification()
                    if UserDefaults.standard.object(forKey: userDerfaultConstants.deviceTokenRegistered) != nil{
                        let status = UserDefaults.standard.object(forKey: userDerfaultConstants.deviceTokenRegistered) as! String
                        if status == "false"{
                            if UserDefaults.standard.object(forKey: userDerfaultConstants.deviceToken) != nil{
                                let deviceToken = UserDefaults.standard.object(forKey: userDerfaultConstants.deviceToken)
                                uploadDeviceDetail.deviceDetails(deviceToken as! String)
                            }
                        }
                    }
                    
                    let app = UIApplication.shared.delegate as! AppDelegate
                    app.maketabBarRoot()
                }
                else{
                        DBController().addUserToDB(userModel)
                        let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                        let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                        ChngePwdVC.isPassword = true
                        ChngePwdVC.mobNo = mobileNo
                        self.present(ChngePwdVC, animated: true, completion: nil)
                    
                    
                }
            }else {
                AlertController.showToastAlertWithMessage(message: "Data InCorrect", stateOfMessage: .failure)
            }
            //app.setThisViewControllerWithIdentifierAsRoot("mainTabBar", InStoryBoard: "Main")
            
        }else {
            AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .failure)
        }
        
    }
    
    func askPermissionForPushNotification(){
        //[START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For ios 10 data message (sent via FCM)
            Messaging.messaging().remoteMessageDelegate = self
            print("Notification: registration for ios >= 10 using UNUserNotificationCenter")
        }else{
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            print("Notification: registration for ios < 10 using basic notification center")
        }
    }
    
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("Notification: Firebase FCM delegate remote message")
    }
    
    @IBAction func backToLogin(_ sender: Any) {
    
    if isstudentAdmission {
            
           #if EXCELDEVELOPMENT
                let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
            let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "StudentAdmission") as! admissionVC
            Analytics.logEvent("p_login_excel_admission", parameters: nil)
            self.present(ChngePwdVC, animated: true, completion: nil)
            #elseif SREEMAHARSHIDEVELOPMENT
                //self.dismissViewControllerAnimated(true, completion: nil)
            let app = UIApplication.shared.delegate as! AppDelegate
            app.setThisViewControllerWithIdentifierAsRoot(identifier: "sreeMaharshiLoginViewController", InStoryBoard: "Main")
            #endif
    }
    else{
        mobNoTextField.text = ""
        self.mobileicon.image = UIImage(named: "mobilePictureForLogin")
        mobNoTextField.keyboardType = UIKeyboardType.numberPad
        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.black,
            NSAttributedStringKey.font : UIFont(name: "Roboto-Bold", size: 17)! // Note the !
        ]
        
        mobNoTextField.attributedPlaceholder = NSAttributedString(string: "Mobile No", attributes:attributes)
        mobNoTextField.textColor = UIColor.black
        
        self.forgotPasswordButton.isHidden = true
        self.isPassword = false
        self.ishomeScreenVisible = false
        
        #if SREEMAHARSHIDEVELOPMENT
            backButton.isHidden = false
            isstudentAdmission = true
        #elseif EXCELDEVELOPMENT
            backButton.isHidden = false
            isstudentAdmission = true
            uichangeForExcel()
        #else
            backButton.isHidden = true
            isstudentAdmission = true
        #endif
    }
        
  }
    
}
