//
//  PhotoThumbnail.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 13/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit


class PhotoThumbnail: UICollectionViewCell {
    
    
    
    @IBOutlet weak var thumbNail: UIImageView!
   
    func setThumbnailImage(thumbNailImage: UIImage) {
    self.thumbNail.image = thumbNailImage
    }
    
}
