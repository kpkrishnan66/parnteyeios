//
//  MPSDKKit.h
//  MPSDKKit
//
//  Created by Atom Technologies on 22/06/18.
//  Copyright © 2018 Atom Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MPSDKKit.
FOUNDATION_EXPORT double MPSDKKitVersionNumber;

//! Project version string for MPSDKKit.
FOUNDATION_EXPORT const unsigned char MPSDKKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MPSDKKit/PublicHeader.h>

