//
//  ParentEyeTabbar.swift
//  parentEye
//
//  Created by Martin Jacob on 11/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

struct tabbarViewControllers {
    static let diary     = 0
    static let notice    = 1
    static let home      = 2
    static let homework  = 3
    static let message   = 4
}

class ParentEyeTabbar: UITabBarController,hamburgerProtocol {

    var shouldLoadNotification = false
    var shouldLoadHomework = false
    var shouldLoadMessage = false
    var shouldLoadDiary = false
    var shouldLoadLeaveRecord = false
    var shouldLoadStudentLeaveSubmit = false
    var shouldLoadExternalNotification = false
    
    var noticeToBeLoaded:NotificationModel?
    var homeworkToBeLoaded:HomeWorkModel?
    var messageToBeLoaded:messageForListing?
    var diaryToBeLoaded:DiaryModelClass?
    var StudentLeaveSubmitToBeLoaded:StudentAbsentRecord?
    var externalNotificationToBeLoaded:externalNotificationModel?
    
    var diaryClassId = -1
    var diaryIdToBeExpanded:String?
    var totalUnReadCount:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 2
        makeTabBarImagesOriginalColor()
        getUnreadCount()
        
        
        // Do any additional setup after loading the view.
    }
    
    private func makeTabBarImagesOriginalColor (){
        for tbi in self.tabBar.items! {
            tbi.image = tbi.image?.withRenderingMode(.alwaysOriginal)
        }
       
        UITabBar.appearance().tintColor = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
    }
    
    func setBadges (){
        weak var weakSelf = self
        DispatchQueue.main.async {
            weakSelf!.setDiaryReadNumber()
            weakSelf!.setMessageReadNumber()
            UIApplication.shared.applicationIconBadgeNumber = self.totalUnReadCount
        }
        
    }
    
     func setDiaryReadNumber(){
        let diaryNumnber = DBController().getDiaryUnreadNumber()
        let diaryTab = self.tabBar.items![tabbarViewControllers.diary]
        if diaryNumnber == 0{
            diaryTab.badgeValue = nil
        }else{
            diaryTab.badgeValue = String(diaryNumnber)
        }
    }
    
    
    
     func setMessageReadNumber(){
        let messageUnreadNumber = DBController().getMessageUnreadNumber()
        let messageTab = self.tabBar.items![tabbarViewControllers.message]
        if messageUnreadNumber == 0{
            messageTab.badgeValue = nil
        }else{
            messageTab.badgeValue = String(messageUnreadNumber)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func logMeOut(){
        DBController().clearDataBse()
        
    }

    
    
    // MARK: Hamburger menu
    
    func showChangePasswordScreen() {
        let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
        let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
        self.present(ChngePwdVC, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if item.title == "" {
             getUnreadCount()
        }
       /* if  let arrayOfTabBarItems = tabBar.items as! AnyObject as? NSArray,tabBarItem = arrayOfTabBarItems[3] as? UITabBarItem {
            tabBarItem.enabled = false
        } */
        
    }
    
    func getUnreadCount()  {
        weak var weakSelf = self
        let user = DBController().getUserFromDB()
        if user?.currentTypeOfUser == .guardian{
            WebServiceApi.getUnreadCountForStudentId(studentId: (user?.getSelectedEmployeeOrStudentId())!, forUserRoleId: (user?.currentUserRoleId)!, onSuccess: { (result) in
                weakSelf!.parseUnreadJsonFromDictionary(dictionary: result)
                weakSelf!.setBadges()
                }, onFailure: { (error) in
                    
            }) { (progress) in
                
            }
        }
        else{
            WebServiceApi.getUnreadCountForEmployeeId(forUserRoleId: (user?.currentUserRoleId)!, onSuccess: { (result) in
                weakSelf!.parseUnreadJsonFromDictionary(dictionary: result)
                weakSelf!.setBadges()
                }, onFailure: { (error) in
                    
            }) { (progress) in
                
            }
            
        }
    }

    private func parseUnreadJsonFromDictionary(dictionary:[String:AnyObject]){
        
        if let uRDictionary = dictionary[resultsJsonConstants.readUnreadConstants.unreadCountKey] as? [String:AnyObject]{
            var count:Int = 0
            if let diaryCount = uRDictionary[resultsJsonConstants.readUnreadConstants.studentDairyCountKey]{
                _ = DBController().updateDiaryUnread(diaryCount as! String)
                if let diaCount = diaryCount as? String{
                    if diaCount != ""{
                        count = count + Int(diaCount)!
                    }
                }
            }
            if let messageCount = uRDictionary[resultsJsonConstants.readUnreadConstants.messageCountKey]{
                _ = DBController().updateMessageUnread(messageCount as! String)
                if let msgCount = messageCount as? String{
                    if msgCount != ""{
                        count = count + Int(msgCount)!
                    }
                }
            }
            totalUnReadCount = count
        }
    }
}
