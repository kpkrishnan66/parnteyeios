//
//  HomeworkListingCell.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 20/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class HomeworkListingCell: UITableViewCell {

    
    @IBOutlet var dateView: UIView!
    @IBOutlet var homeworkSubmitDate: UILabel!
    @IBOutlet var dayLabel: UILabel!
    
    @IBOutlet var homeworkAddedName: UILabel!
    @IBOutlet var homeworkSubject: UILabel!
    @IBOutlet var homeworkTitle: UILabel!

    @IBOutlet var monthAndYearLabel: UILabel!
    
    @IBOutlet var attachmentImageWidth: NSLayoutConstraint!
    @IBOutlet var attachmentImage: UIImageView!
    @IBOutlet var seperatorView: UIView!
    @IBOutlet var BottomLineView: UIView!
    @IBOutlet var TopLineView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }


}
