//
//  evaluvationCategory.swift
//  parentEye
//
//  Created by scientia on 08/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
class evaluvationCategory{
    
    var categoryName:String = ""
    var categoryPosition:Int = 0
    var categoryRowCount:Int = 0
    var categoryHeaderArray = [String]()
    var categorySubjectArray = [evaluvationSubject]()
}
