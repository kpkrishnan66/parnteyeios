//
//  PopOverDidSelectRow.swift
//  parentEye
//
//  Created by Martin Jacob on 21/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
protocol popOverDelegate:class{
 
    func popOverDidSelectRow(rowSelected:Int) -> Void
}

protocol SelectedSchoolpopOverDelegate:class{
    
    func selectedSchoolRow(rowSelected:Int) -> Void
}
