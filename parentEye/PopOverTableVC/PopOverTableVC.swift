//
//  PopOverTableVC.swift
//  parentEye
//
//  Created by Martin Jacob on 21/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class PopOverTableVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var user:userModel?
     let cellIdentifier = "popOverCell"
    
    weak var delegate:popOverDelegate?
    
    @IBOutlet weak var popOverTable:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        user = DBController().getUserFromDB()
        setUpTable()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    private func setUpTable(){
        
        popOverTable.register(UINib(nibName: "popOverCell", bundle: nil), forCellReuseIdentifier: "popOverCell")
        popOverTable.estimatedRowHeight = 75.0
        popOverTable.rowHeight = UITableViewAutomaticDimension
    }
    // MARK: Table view delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let usr =  user{
            if  usr.currentTypeOfUser == .guardian{
                if let student = usr.Students{
                    return student.count
                }
            }
        }
        
        return 0
    }
    
    //TODO :- only parents  is supported in this function .. need to make it support heads also
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! popOverCell
        
        weak var imgWeak = cell.profileImageView
        ImageAPi.fetchImageForUrl(urlString: (user?.Students![indexPath.row].profilePic)!, oneSuccess: { (image) in
            if imgWeak != nil{
                imgWeak!.image = image
                ImageAPi.makeImageViewRound(imageView: imgWeak!)
            }
        }, onFailure: { (error) in
            debugPrint("Download failed")
        })
        cell.nameLabel.text = user?.Students![indexPath.row].name
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.popOverDidSelectRow(rowSelected: indexPath.row)
        self.dismiss(animated: false, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
