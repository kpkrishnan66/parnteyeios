//
//  popOverCell.swift
//  parentEye
//
//  Created by Martin Jacob on 23/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class popOverCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel:UILabel!
    
    @IBOutlet weak var profileImageViewWidth: NSLayoutConstraint!
    @IBOutlet var switchasemployeeLabel: UILabel!
}
