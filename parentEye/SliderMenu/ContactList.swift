//
//  ContactList.swift
//  parentEye
//
//  Created by scientia on 27/12/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ContactList: UIViewController, UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate ,CommonHeaderProtocol,SelectedSchoolpopOverDelegate{
    
    @IBOutlet weak var ContactSearch: UISearchBar!
    @IBOutlet weak var ContactListTableView: UITableView!
    @IBOutlet weak var Header: CommonHeader!
    
    var user:userModel?
    
    lazy var contactArray = [Contacts]()
    lazy var contactArrayTemp = [Contacts]()
    var profilePic = ""
    var mobileNumber = ""
    var schoolId:Int = 0
    
    
    override func viewDidLoad(){
        getUserDataFromDB()
        ContactSearch.delegate = self
        alterHeader()
        setupTable()
    }
    override func viewDidAppear(_ animated: Bool){
        ContactSearch.returnKeyType = .done
        ContactSearch.enablesReturnKeyAutomatically = false
        makeContactList()
    }
    override func viewDidDisappear(_ animated: Bool) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //searchActive = false;
        searchBar.endEditing(true)
    }
    
    fileprivate func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()!
    }
    
    fileprivate func alterHeader() {
        
        Header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.contactScreenHeading, withProfileImageUrl: profilePic)
        Header.delegate = self
    }
    //Common Header Delegates
    func showHamburgerMenuOrPerformOther() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func showSwitchSibling(fromButton:UIButton) {
        if user?.currentTypeOfUser == .CorporateManager {
            PopOverView.addPopOverofSchoolsToViewController(self, fromButton: fromButton)
            
        }
    }
    func selectedSchoolRow(rowSelected: Int) {
        user?.currentSchoolSelected = rowSelected
        DBController().addUserToDB(user!)
        if schoolId != user?.schools![(user?.currentSchoolSelected)!].schoolId {
            Header.multiSwitchButton.setTitle(user!.schools![(user?.currentSchoolSelected)!].schoolName, for: UIControlState())
            makeContactList()
        }
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //contactArrayTemp = contactArray
        if searchText == ""{
            contactArray = contactArrayTemp
        }else{
            let filtered = contactArrayTemp.filter { $0.name.contains(searchText,options: .caseInsensitive) }
            contactArray = filtered
        }
        ContactListTableView.reloadData()
    }
    
    
    fileprivate func setupTable(){
        ContactListTableView.register(UINib(nibName: "ContactListTableVCCell", bundle: nil), forCellReuseIdentifier: "ContactListTableVCCell")
        
        
        
        ContactListTableView.estimatedRowHeight = 10.0
        //ContactListTableView.rowHeight = UITableViewAutomaticDimension
        
        
        //listingTable.tableHeaderView = NSBundle.mainBundle().loadNibNamed("ProfileViewForHomeTable", owner: self, options: nil)[0] as? UIView
    }
    
    fileprivate func makeContactList(){
        if user?.currentTypeOfUser == .employee{
            schoolId = (user!.schoolId)
        }
        else{
            schoolId = (user?.schools![(user?.currentSchoolSelected)!].schoolId)!
        }
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getContactListOfTeachers(schoolId: String(schoolId),userRoleId: (user!.currentUserRoleId),  onSuccess: { (result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            self.contactArray = parseContactListArray.parseThisDictionaryForContactListRecords(dictionary: result)
            self.contactArrayTemp = self.contactArray
            weakSelf!.ContactListTableView.reloadData()
            
        }, onFailure: { (error) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }) { (progress) in
            
        }
    }
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return contactArray.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: "ContactListTableVCCell") as! ContactListTableVCCell
        let employeeContact = contactArray[indexPath.row]
        cell.employeeName.text     = employeeContact.name
        cell.employeeDesignation.text = employeeContact.designation
        /*let URLString = employeeContact.profilePicUrl
         let URL = NSURL(string:URLString)!
         cell.ProfilePic.hnk_setImageFromURL(URL)
         cell.ProfilePic.layer.masksToBounds = false
         cell.ProfilePic.layer.cornerRadius = cell.ProfilePic.frame.height/2
         cell.ProfilePic.clipsToBounds = true*/
        if employeeContact.profilePicUrl == ""{
            cell.ProfilePic.image = UIImage(named: "avatar")
            cell.ProfilePic.layer.masksToBounds = true
            cell.ProfilePic.layer.cornerRadius = 0
            cell.ProfilePic.clipsToBounds = false
            
        }
        else{
            ImageAPi.fetchImageForUrl(urlString: employeeContact.profilePicUrl, oneSuccess: { (image) in
                if cell.ProfilePic != nil {
                    
                    cell.ProfilePic.layer.masksToBounds = false
                    cell.ProfilePic.layer.cornerRadius = cell.ProfilePic.frame.height/2
                    cell.ProfilePic.clipsToBounds = true
                    cell.ProfilePic!.image = image
                    
                }
            }) { (error) in
                
            }
        }
        cell.callButton.tag = indexPath.row
        cell.callButton.addTarget(self, action: #selector(ContactList.callIconTapped(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    @objc func callIconTapped(_ sender:UIButton){
        
        for k in 0 ..< contactArray.count {
            if k == sender.tag{
                mobileNumber = contactArray[k].mobileNo
                
            }
        }
        showAlert()
    }
    
   fileprivate func showAlert(){
        if let url = URL(string: "tel://\(self.mobileNumber)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
}
