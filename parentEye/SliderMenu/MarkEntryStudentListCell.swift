//
//  MarkEntryStudentListCell.swift
//  parentEye
//
//  Created by Vivek on 17/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class MarkEntryStudentListCell:UITableViewCell {
   
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var markTextField: UITextField!
    
    @IBOutlet weak var savedExamLabel: UILabel!
    
    @IBOutlet weak var selectAbsentOrNonApplicableButton: UIButton!
    
}
