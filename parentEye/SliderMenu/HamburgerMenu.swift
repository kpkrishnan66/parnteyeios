//
//  HamburgerMenu.swift
//  parentEye
//
//  Created by Martin Jacob on 11/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

protocol hamburgerProtocol:class {
    func showChangePasswordScreen()
    
}

class HamburgerMenu: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var user:userModel?
    let cellIdentifier = "hamburgerReuse"
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var optionsListingTable: UITableView!
    @IBOutlet weak var designationLabel:UILabel!
    weak var delegate:hamburgerProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        optionsListingTable.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    open func setUpUI(){
        user = DBController().getUserFromDB()
        userNameLabel.text = user?.username
        designationLabel.text = user?.userDesignation
        ImageAPi.makeImageViewRound(imageView: profileImage)
        
        if user?.currentTypeOfUser == .guardian{
            //userNameLabel.text = user?.getStudentCurrentlySelectedStudent()?.name
            //designationLabel.text = ""
            weak var weakProfileImage = profileImage
            weakProfileImage!.image = UIImage(named:"avatar.png")
            ImageAPi.fetchImageForUrl(urlString: (user?.getSelectedEmployeeOrStudentProfileImageUrl())!, oneSuccess: { (image) in
                if weakProfileImage != nil {
                    weakProfileImage!.image = image }
                //else{ weakProfileImage!.image = UIImage(named:"avatar.png")}
            }) { (error) in
                //weakProfileImage!.image = UIImage(named:"avatar.png")
            }
        }
            /*if user?.currentTypeOfUser == .CorporateManager {
             userNameLabel.text = user?.username
             designationLabel.text = user?.getStudentCurrentlySelectedSchool()?.schoolName
             weak var weakProfileImage = profileImage
             ImageAPi.fetchImageForUrl((user?.profilePic)!, oneSuccess: { (image) in
             if weakProfileImage != nil { weakProfileImage!.image = image }
             }) { (error) in
             }
             
             }*/
        else{
            //userNameLabel.text = user?.username
            //designationLabel.text = user?.userDesignation
            weak var weakProfileImage = profileImage
            weakProfileImage!.image = UIImage(named:"avatar.png")
            ImageAPi.fetchImageForUrl(urlString: (user?.profilePic)!, oneSuccess: { (image) in
                if weakProfileImage != nil { weakProfileImage!.image = image }
                //else{weakProfileImage!.image = UIImage(named:"avatar.png")}
            }) { (error) in
               // weakProfileImage!.image = UIImage(named:"avatar.png")
            }
            
            
        }
    }
    
    
    // MARK: Table view delegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var isUpdateAvailable = false
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject
        let existingVersionNo = nsObject as! String
        if let webVersionNo = UserDefaults.standard.object(forKey: "appVersionFromWeb") as? String {
           if existingVersionNo != webVersionNo{
             isUpdateAvailable = true
           }
         }
         var returnValue = 0;
         if isUpdateAvailable{
            returnValue = 6
         }else {
            returnValue = 5
         }
        
        if user?.currentTypeOfUser == .guardian{
            returnValue = returnValue-2
        }
        else if !webServiceRolesGroupForDifferentActivities.markEntryEligibleGroup.contains((user?.employeeType)!){
            returnValue = returnValue - 1
        }
        return returnValue
    }
    
    //TODO :- only parents  is supported in this function .. need to make it support heads also
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        ImageAPi.makeImageViewRound(imageView: cell.imageView!)
        cell.imageView?.frame = CGRect(x: 10, y: 10, width: 30, height: 30)
        
        cell.imageView?.image = getImageForRow(indexPath.row)
        cell.textLabel?.text = getNameForCellInRow(indexPath.row)
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.slideMenuController()?.closeLeft()
        
        
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            
            if !webServiceRolesGroupForDifferentActivities.markEntryEligibleGroup.contains((user?.employeeType)!){
                
                
                switch indexPath.row {
                    
                case 0: let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "ContactListVC") as! ContactList
                FireBaseKeys.FirebaseKeysRuleThree(value: "hamburger_contactList")
                self.present(ChngePwdVC, animated: true, completion: nil)
                    
                    
                case 1: delegate?.showChangePasswordScreen()
                    
                case 2: sync.syncNow()
                    
                case 3: let alter = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.wantToLogout, titleOfAlert: ConstantsInUI.wantToLogout, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
                    
                }, OkButtonBlock: { (action) in
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    DBController().clearDataBse()
                    let app = UIApplication.shared.delegate as! AppDelegate
                    #if SREEMAHARSHIDEVELOPMENT
                        app.setThisViewControllerWithIdentifierAsRoot(identifier: "sreeMaharshiLoginViewController", InStoryBoard: "Main")
                    #else
                        app.setThisViewControllerWithIdentifierAsRoot(identifier: "LoginViewController", InStoryBoard: "Main")
                    #endif
                })
                
                self.present(alter, animated: true, completion: nil )
                    
                    
                case 4: let url  = URL(string: "itms-apps://itunes.apple.com/app/parenteye/id998502097")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.openURL(url!)
                    }
                default:debugPrint("")
                    
                    
                }
            }
            else{
                
                switch indexPath.row {
                    
                case 0: let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                let mkVC = storyboard.instantiateViewController(withIdentifier: "EmloyeeMarkEntryVC") as! EmloyeeMarkEntryVC
                self.present(mkVC, animated: true, completion: nil)
                    
                    
                case 1: let storyboard     = UIStoryboard(name: "ViewControllerContainingStoryBoard", bundle: nil);
                let ChngePwdVC = storyboard.instantiateViewController(withIdentifier: "ContactListVC") as! ContactList
                self.present(ChngePwdVC, animated: true, completion: nil)
                    
                case 2:
                    delegate?.showChangePasswordScreen()
                    
                case 3: sync.syncNow()
                    
                case 4: let alter = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.wantToLogout, titleOfAlert: ConstantsInUI.wantToLogout, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
                    
                }, OkButtonBlock: { (action) in
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    DBController().clearDataBse()
                    let app = UIApplication.shared.delegate as! AppDelegate
                    #if SREEMAHARSHIDEVELOPMENT
                        app.setThisViewControllerWithIdentifierAsRoot(identifier: "sreeMaharshiLoginViewController", InStoryBoard: "Main")
                    #else
                        app.setThisViewControllerWithIdentifierAsRoot(identifier: "LoginViewController", InStoryBoard: "Main")
                    #endif
                })
                
                self.present(alter, animated: true, completion: nil )
                    
                case 5:  let url  = URL(string: "itms-apps://itunes.apple.com/app/parenteye/id998502097")
                if UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.openURL(url!)
                    }
                default:  debugPrint("")
                    
                    
                }
            }
        }
        else if user?.currentTypeOfUser == .guardian{
            
            switch indexPath.row {
                
                
            case 0: delegate?.showChangePasswordScreen()
                
            case 1: sync.syncNow()
                
            case 2: let alter = AlertController.getTwoOptionAlertControllerWithMessage(mesasage: ConstantsInUI.wantToLogout, titleOfAlert: ConstantsInUI.wantToLogout, oktitle: ConstantsInUI.ok, cancelTitle: ConstantsInUI.cancel, cancelButtonBlock: { (action) in
                
            }, OkButtonBlock: { (action) in
                UIApplication.shared.applicationIconBadgeNumber = 0
                DBController().clearDataBse()
                let app = UIApplication.shared.delegate as! AppDelegate
                app.setThisViewControllerWithIdentifierAsRoot(identifier: "LoginViewController", InStoryBoard: "Main")
            })
            
            self.present(alter, animated: true, completion: nil )
                
                
            case 3: let url  = URL(string: "itms-apps://itunes.apple.com/app/parenteye/id998502097")
            if UIApplication.shared.canOpenURL(url!) {
                UIApplication.shared.openURL(url!)
                }
            default:debugPrint("")
                
                
            }
        }
    }
    
    fileprivate func getImageForRow(_ row:Int) ->UIImage{
        var img = UIImage()
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            if !webServiceRolesGroupForDifferentActivities.markEntryEligibleGroup.contains((user?.employeeType)!){
                
                if row == 0 {
                    img = UIImage(named: "contactListIcon")!
                }
                if row == 1 {
                    img = UIImage(named: "passwordIcon")!
                }
                if row == 2 {
                    img = UIImage(named: "syncnowIcon")!
                }
                if row == 3 {
                    img = UIImage(named: "signOut")!
                }
                if row == 4 {
                    img = UIImage(named: "signOut")!
                }
                return img
            }
            else{
                if row == 0 {
                    img = UIImage(named: "maekEntryIcon")!
                }
                if row == 1 {
                    img = UIImage(named: "contactListIcon")!
                }
                if row == 2 {
                    img = UIImage(named: "passwordIcon")!
                }
                if row == 3 {
                    img = UIImage(named: "syncnowIcon")!
                }
                if row == 4 {
                    img = UIImage(named: "signOut")!
                }
                if row == 5 {
                    img = UIImage(named: "signOut")!
                }
                
                return img
            }
            
        }
        else{
            if row == 0 {
                img = UIImage(named: "passwordIcon")!
            }
            if row == 1 {
                img = UIImage(named: "syncnowIcon")!
            }
            if row == 2 {
                img = UIImage(named: "signOut")!
            }
            if row == 3 {
                img = UIImage(named: "signOut")!
            }
            return img
            
        }
    }
    
    fileprivate func getNameForCellInRow(_ row:Int) ->String {
        var returnString = ""
        if user?.currentTypeOfUser == .employee || user?.currentTypeOfUser == .CorporateManager{
            if !webServiceRolesGroupForDifferentActivities.markEntryEligibleGroup.contains((user?.employeeType)!){
                
                if row == 0 {
                    returnString = ConstantsInUI.ContactList
                }
                if row == 1 {
                    returnString = ConstantsInUI.changePassword
                }
                if row == 2 {
                    returnString = ConstantsInUI.Syncnow
                }
                if row == 3 {
                    returnString = ConstantsInUI.signOut
                }
                if row == 4 {
                    returnString = ConstantsInUI.update+" "+ConstantsInUI.appAlertName
                }
            }
            else{
                if row == 0 {
                    returnString = ConstantsInUI.markEntry
                }
                if row == 1 {
                    returnString = ConstantsInUI.ContactList
                }
                if row == 2 {
                    returnString = ConstantsInUI.changePassword
                }
                if row == 3 {
                    returnString = ConstantsInUI.Syncnow
                }
                if row == 4 {
                    returnString = ConstantsInUI.signOut
                }
                if row == 5 {
                    returnString = ConstantsInUI.update+" "+ConstantsInUI.appAlertName
                }
            }
        }
        else {
            
            if row == 0 {
                returnString = ConstantsInUI.changePassword
            }
            if row == 1 {
                returnString = ConstantsInUI.Syncnow
            }
            if row == 2 {
                returnString = ConstantsInUI.signOut
            }
            if row == 3 {
                returnString = ConstantsInUI.update+" "+ConstantsInUI.appAlertName
            }
            
        }
        return returnString
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
