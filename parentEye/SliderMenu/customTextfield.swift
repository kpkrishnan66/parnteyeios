//
//  customTextfield.swift
//  parentEye
//
//  Created by scientia on 04/03/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class customTextfield: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
//        if action == #selector(UIResponderStandardEditActions.paste(_:)) || action == #selector(UIResponderStandardEditActions.cut(_:)) || action == #selector(UIResponderStandardEditActions.copy(_:)) || action == Selector(("Replace:")) || action == Selector("Look Up:") || action == Selector(("share:")) || action == #selector(UIResponderStandardEditActions.select(_:)) || action == #selector(UIResponderStandardEditActions.selectAll(_:)){
//            return false
//        }
        if action == #selector(paste(_:)) || action == #selector(cut(_:)) || action == #selector(copy(_:)) || action == #selector(select(_:)) || action == #selector(selectAll(_:)){
            return false
        }
        

        return super.canPerformAction(action, withSender: sender)
    }
}
