//
//  ContactListTableVCCell.swift
//  parentEye
//
//  Created by scientia on 27/12/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ContactListTableVCCell:UITableViewCell{
    
    @IBOutlet weak var ProfilePic: UIImageView!
    
    @IBOutlet weak var employeeName: UILabel!
    
    @IBOutlet weak var employeeDesignation: UILabel!
    
    @IBOutlet weak var callButton: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.ProfilePic?.image = UIImage(named: "avatar")
        self.employeeName?.text = ""
        self.employeeDesignation?.text = ""
    }
    
}
