//
//  StudentProfileListingVC.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 28/06/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase


class studentProfileListingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CommonHeaderProtocol,UIPopoverPresentationControllerDelegate,popOverDelegate {
    var user:userModel?
    var selected:Bool = false
    var containerViewBottom:UIView?
    var numberOfRow = Int()
    var constantHeightOfTableView = CGFloat()
    var busArrivalStatus:String?
    var profilePic = ""
    var gotoDetailedStudentProfile = false
    var moveToGraph = false
    
    @IBOutlet var header: CommonHeader!
    @IBOutlet weak var subSelectionSgmntCtrl: UISegmentedControl!
    @IBOutlet var studentProfileListTable: UITableView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tblViewHeightLayout: NSLayoutConstraint!
    @IBOutlet weak var btnTransport: UIButton!
    @IBOutlet var btnTransportImage: UIImageView!
    @IBOutlet var btnTransportView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Added to adjust the TABLE HEADER
        setupUI()
        setupTable()
        getUserDataFromDB()
        setUpHeader ()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*override func viewDidDisappear(animated: Bool) {
     
     let defaults = NSUserDefaults.standardUserDefaults()
     
     moveToGraph = defaults.boolForKey("navigationToGraph")
     
     if gotoDetailedStudentProfile == false || !moveToGraph {
     self.navigationController?.popToRootViewControllerAnimated(false)
     }
     
     }*/
    
    
    
    fileprivate func setupUI() {
        
        btnTransportView.layer.cornerRadius = 10;
        btnTransportView.layer.masksToBounds = true;
        btnTransportView.layer.borderWidth = 1
        btnTransportView.layer.borderColor = UIColor(red:0/255.0, green:0/255.0, blue:0/255.0, alpha: 1.0).cgColor
        
        let transportImagetap = UITapGestureRecognizer(target: self, action: #selector(self.transportButtontapped))
        let transportViewtap = UITapGestureRecognizer(target: self, action:#selector(self.transportButtontapped))
        btnTransportView.addGestureRecognizer(transportViewtap)
        btnTransportImage.addGestureRecognizer(transportImagetap)
        
    }
    fileprivate func setupTable(){
        studentProfileListTable.register(UINib(nibName: "studentProfileCell", bundle: nil), forCellReuseIdentifier: "studentProfileCell")
        studentProfileListTable.register(UINib(nibName: "studentProfileDetailCell", bundle: nil), forCellReuseIdentifier: "studentProfileDetailCell")
        studentProfileListTable.dataSource = self
        studentProfileListTable.delegate = self
        studentProfileListTable.backgroundView?.backgroundColor = UIColor.white
        //studentProfileListTable.estimatedRowHeight = 180.0
        //studentProfileListTable.rowHeight = UITableViewAutomaticDimension
        numberOfRow = 1
    }
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
        studentProfileListTable.reloadData()
        if user?.Students![0].transportType == "SchoolBus"{
            gettingBusStatus()
        }
    }
    fileprivate func setUpHeader (){
        header.delegate = self
        if user?.currentTypeOfUser == .guardian{
            
            profilePic =  (user?.Students![(user?.currentOptionSelected)!].profilePic)!
        }
        else{
            profilePic = ""
        }
        
        header.setUpHeaderForNormalScreenWithNameOfScreen(name: ConstantsInUI.studentProfileScreenHeading, withProfileImageUrl: profilePic)
        
    }
    
    
    //MARK: -Common header deleagte
    
    func showHamburgerMenuOrPerformOther() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showSwitchSibling(fromButton: UIButton) {
        PopOverView.addPopOverToViewController(self, fromButton: fromButton)
    }
    
    
    
    //MARK: Table view delegate and data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        /*if section == 0{
         return 1
         }
         else{
         return 1
         }*/
        return numberOfRow
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellToDisplay = UITableViewCell()
        if indexPath.row == 0{
            let viewmorecell =  tableView.dequeueReusableCell(withIdentifier: "studentProfileCell") as! studentProfileCell
            if user?.currentTypeOfUser == .guardian{
                if let stdnts = user?.Students{
                    let student = stdnts[(user?.currentOptionSelected)!]
                    viewmorecell.alterCellWithProfileImageUrl(student.profilePic)
                }}
            
            
            viewmorecell.backgroundColor = UIColor.clear
            if numberOfRow == 2 {
                viewmorecell.viewMore.isHidden = true
            }else{
                viewmorecell.viewMore.isHidden = false
            }
            
            viewmorecell.viewMore.addTarget(self, action: #selector(self.visibilityShow(_:)), for: .touchUpInside)
            if let stdnts = user?.Students{
                let student = stdnts[(user?.currentOptionSelected)!]
                let classTeacherName = student.classTeacher
                viewmorecell.studentName.text = student.name
                viewmorecell.studentGrade.text = student.className
                viewmorecell.studentTeacherName.text = classTeacherName
                
                viewmorecell.studentAddress.text = student.adress
                viewmorecell.studentAdmnno.text = student.admissionNo
                viewmorecell.studentDob.text = student.dob
                viewmorecell.studentMobno.text = student.mobNo
                viewmorecell.studentGuardianName.text = student.parentName
                
                /*let tap = UITapGestureRecognizer(target: self, action: #selector(studentProfileListingVC.tapFunction))
                 viewmorecell.studentTeacherName.userInteractionEnabled = true
                 viewmorecell.studentTeacherName.tag = indexPath.row
                 viewmorecell.studentTeacherName.addGestureRecognizer(tap)*/
            }
            cellToDisplay = viewmorecell
            
            //return viewmorecell
        }
        if indexPath.row == 1{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "studentProfileDetailCell") as! studentProfileDetailCell
            cell.backgroundColor = UIColor.clear
            cell.hide.addTarget(self, action: #selector(self.visibilityHide(_:)), for: .touchUpInside)
            cell.hide.isHidden = true
            if let stdnts = user?.Students{
                
                let student = stdnts[(user?.currentOptionSelected)!]
                cell.studentAddress.text = student.adress
                cell.studentAdmnno.text = student.admissionNo
                cell.studentDob.text = student.dob
                cell.studentMobno.text = student.mobNo
                cell.studentGuardianName.text = student.parentName
            }
            cellToDisplay = cell
            
            //return cell
        }
        /*if indexPath.section == 1 && selected == true {
         let cell =  tableView.dequeueReusableCellWithIdentifier("studentProfileDetailCell") as! studentProfileDetailCell
         cell.backgroundColor = UIColor.clearColor()
         cell.hide.addTarget(self, action: #selector(self.visibilityHide(_:)), forControlEvents: .TouchUpInside)
         return cell
         
         }
         return viewmorecell*/
        return cellToDisplay
    }
    /*func tapFunction(sender:UITapGestureRecognizer) {
     let indexPath = NSIndexPath(forRow: sender.tag, inSection: 0)
     let cell = tableView.cellForRowAtIndexPath(indexPath) as! MarkEntryStudentListCell!
     UIView.animateWithDuration(5.0, delay: 0, options: ([.TransitionNone, .AllowAnimatedContent]), animations: {
     cell.studentTeacherName.bounds =  CGRect(x: 0,y: 0,width: 0,height: 50)
     }, completion:  { _ in })
     }*/
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 233 //UITableViewAutomaticDimension
    }
    @objc func visibilityShow(_ sender:UIButton)
    {
        selected = true
        numberOfRow = 2
        sender.isHidden = true
        //constantHeightOfTableView = tblViewHeightLayout.constant
        //tblViewHeightLayout.constant = 300
        studentProfileListTable.reloadData()
        
    }
    
    @objc func visibilityHide(_ sender:UIButton)
    {
        selected = false
        numberOfRow = 1
        //tblViewHeightLayout.constant = constantHeightOfTableView
        studentProfileListTable.reloadData()
    }
    
    
    @IBAction func transportButtontapped(_ sender: AnyObject) {
        if user?.currentTypeOfUser == .guardian{
            Analytics.logEvent("P_studentprofile_transport", parameters: nil)
        }
        gotoDetailedStudentProfile = true
        self.performSegue(withIdentifier: "transportPageSegue", sender: nil)
    }
    
    //MARK:- POP over delegates
    
    // Returns none to make ipad like pop over in iphone
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    
    //MARK:- popOverTableVC delegates
    
    //TODO:- We will have to change the logic here once user is put in coredata.
    func popOverDidSelectRow(rowSelected: Int) {
        user?.currentOptionSelected = rowSelected
        DBController().addUserToDB(user!)
        let app = UIApplication.shared.delegate as! AppDelegate
        app.maketabBarRoot()
    }
    
}
extension studentProfileListingVC{
    
    
    @IBAction func subSelectionTapped(_ sender: AnyObject) {
        let viewControllerIdentifiers = ["Leave","Timetable"]
        switch subSelectionSgmntCtrl.selectedSegmentIndex {
        case 0:
            debugPrint("Leave view will be here")
            let newController = (storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifiers[sender.selectedSegmentIndex]))! as UIViewController
            let oldController = childViewControllers.last! as UIViewController
            
            oldController.willMove(toParentViewController: nil)
            addChildViewController(newController)
            newController.view.frame = oldController.view.frame
            
            transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations:{ () -> Void in
                // nothing needed here
            }, completion: { (finished) -> Void in
                oldController.removeFromParentViewController()
                newController.didMove(toParentViewController: self)
            })
            
        case 1:debugPrint("Time table view will be here")
        let newController = (storyboard?.instantiateViewController(withIdentifier: viewControllerIdentifiers[sender.selectedSegmentIndex]))! as UIViewController
        let oldController = childViewControllers.last! as UIViewController
        
        oldController.willMove(toParentViewController: nil)
        addChildViewController(newController)
        newController.view.frame = oldController.view.frame
        
        transition(from: oldController, to: newController, duration: 0.25, options: .transitionCrossDissolve, animations:{ () -> Void in
            // nothing needed here
        }, completion: { (finished) -> Void in
            oldController.removeFromParentViewController()
            newController.didMove(toParentViewController: self)
        })
            
        default:
            debugPrint("defult")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "transportPageSegue"{
            gotoDetailedStudentProfile = true
            let svc = segue.destination as? TransportVC
            svc?.busArrivalStatus = busArrivalStatus
            
        }
        
        
    }
    
}
extension studentProfileListingVC{
    
    func gettingBusStatus(){
        hudControllerClass.showNormalHudToViewController(viewController: self)
        weak var weakSelf = self
        WebServiceApi.getBusTime(correspondingTobusID: String((user?.Students![0].busID)!), onSuccess: {(result) in
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            if let busDelay = result["busDelay"]{
                weakSelf!.busArrivalStatus = busDelay["delayReport"] as? String
                //weakSelf?.btnTransport.titleLabel?.text = weakSelf!.busArrivalStatus
            }
            debugPrint("bus",result)
            
        }, onFailure: {(error) in
            
            hudControllerClass.hideHudInViewController(viewController: weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
            
        }, duringProgress: {(progress) in
            
        })
    }
    
}
