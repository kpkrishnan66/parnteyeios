//
//  ExpansiveView.swift
//  parentEye
//
//  Created by Martin Jacob on 27/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

protocol attachmentDeleteButtonPressedProtocol:class  {
    
    func SelectedAttachmentTag(selectedAttachment : Int)
}


class ExpansiveView: UIView {

    var itemSizing:CGFloat = 30.0 // used as size holder for determining the size.
    var attachmentDeleteButton = UIButton()
    var tagPosition = 0
    var itemSizingforcompose:CGFloat = 40.0
    var itemSizingforDiaryDetailed:CGFloat = 50.0
    
    weak var delegate:attachmentDeleteButtonPressedProtocol?
    
    struct spacings {
        static let topSpacing:CGFloat    = 10.0
        static let bottomSpacing:CGFloat = 10.0
        static let leftSpacing:CGFloat   = 10.0
        static let rightSpacing:CGFloat  = 10.0
        
    }
    
    func removeAllSubviews(){
            self.subviews.forEach { $0.removeFromSuperview() }
    }
    
    func addImagesToMeFromArrayForDiaryDetailed(imageArray:[UIImageView]) -> CGFloat {
        
        if UIScreen.main.bounds.size.width - 48 <= itemSizingforDiaryDetailed+spacings.leftSpacing+spacings.rightSpacing{
            //condition added here to avoid adding views if the width is less than the minimum size of the view
        }else {
            var originX = spacings.topSpacing
            var originY = spacings.leftSpacing
            _ = CGSize(width: itemSizingforDiaryDetailed, height: itemSizingforDiaryDetailed)
            
            if imageArray.count>0 {
                for (views) in imageArray {
                   if UIScreen.main.bounds.size.width - 48 > originX+itemSizingforDiaryDetailed{
                        views.frame = CGRect(x: originX, y: originY,width: itemSizingforDiaryDetailed,height: itemSizingforDiaryDetailed)
                        originX = originX+itemSizingforDiaryDetailed+spacings.rightSpacing
                        
                        // checking done here to avoid the case of over lapping.
                        if UIScreen.main.bounds.size.width - 48 < originX+itemSizingforDiaryDetailed + spacings.rightSpacing{
                            originY = itemSizingforDiaryDetailed+spacings.bottomSpacing+spacings.topSpacing+originY
                            originX = spacings.leftSpacing
                        }
                        
                    }else{
                        originX = spacings.leftSpacing
                        originY = itemSizingforDiaryDetailed+spacings.bottomSpacing+spacings.topSpacing
                        views.frame = CGRect(x: originX, y: originY,width: itemSizingforDiaryDetailed ,height: itemSizingforDiaryDetailed )
                    }
                    self.addSubview(views)
                }
                return originY+itemSizingforDiaryDetailed
            }
        }
        return 0.0
    }
    
    
    func makeOnlineImageArrayFromImageArray(imageArray:[UIImageView]) -> CGFloat {
        if self.bounds.width <= itemSizing+spacings.leftSpacing+spacings.rightSpacing{
            //condition added here to avoid adding views if the width is less than the minimum size of the view
        }else {
            var originX = spacings.topSpacing
            let originY = spacings.leftSpacing
            
            if imageArray.count > 0{
                for imgView in imageArray {
                    imgView.frame = CGRect(x: 0, y: 0, width: itemSizing, height: itemSizing)
                    //imgView.backgroundColor = UIColor.blackColor()
                    if self.frame.width > originX+itemSizing+spacings.rightSpacing+spacings.leftSpacing{
                        imgView.frame = CGRect(x: originX, y: originY,width: itemSizing,height: itemSizing)
                        self.addSubview(imgView)
                        originX = originX+itemSizing+spacings.rightSpacing+spacings.leftSpacing
                        
                        if canNextViewWithSizeBeAdded(presentXPositionOfView: originX){
                        }else {
                            break
                        }
                    }
                    
                }
            }else {return 0.0 }
            return itemSizing + spacings.topSpacing + spacings.bottomSpacing
        }
        
        return 0.0
    }
    
    
    func makeOnelineImageArrayfromUrls(imageUrlArray:[String]) -> CGFloat {
        
        if self.bounds.width <= itemSizing+spacings.leftSpacing+spacings.rightSpacing{
            //condition added here to avoid adding views if the width is less than the minimum size of the view
        }else {
            var originX = spacings.topSpacing
            let originY = spacings.leftSpacing
            
            if imageUrlArray.count > 0{
                for imgurl in imageUrlArray {
                 let imgView =  UIImageView(frame: CGRect(x: 0, y: 0, width: itemSizing, height: itemSizing))
                    //imgView.backgroundColor = UIColor.blackColor()
                        if self.frame.width > originX+itemSizing+spacings.rightSpacing+spacings.leftSpacing{
                            imgView.frame = CGRect(x: originX, y: originY,width: itemSizing,height: itemSizing)
                            self.addSubview(imgView)
                             originX = originX+itemSizing+spacings.rightSpacing+spacings.leftSpacing
                            
                            weak var weakImage = imgView
                            if canNextViewWithSizeBeAdded(presentXPositionOfView: originX){
                                ImageAPi.fetchImageForUrl(urlString: imgurl, oneSuccess: { (image) in
                                    if weakImage != nil { weakImage!.image = image }
                                    }, onFailure: { (error) in
                                        
                                })

                            }else {
                                ImageAPi.fetchImageForUrl(urlString: imgurl, oneSuccess: { (image) in
                                     if weakImage != nil { weakImage!.image = image }
                                    }, onFailure: { (error) in
                                        
                                })
                                break
                            }
                        }
                   
                }
            }
            return itemSizing + spacings.topSpacing + spacings.bottomSpacing
        }
        
        return 0.0
    }
    
    func canNextViewWithSizeBeAdded(presentXPositionOfView:CGFloat) -> Bool {
        
        if self.frame.width > presentXPositionOfView + itemSizing + spacings.rightSpacing + spacings.leftSpacing {
            return true
        }
        return false
    }
    
    func addImagesToMeFromArray(imageArray:[UIImageView]) -> CGFloat {
     
        if self.bounds.width <= itemSizing+spacings.leftSpacing+spacings.rightSpacing{
            //condition added here to avoid adding views if the width is less than the minimum size of the view
        }else {
            var originX = spacings.topSpacing
            var originY = spacings.leftSpacing
            _ = CGSize(width: itemSizing, height: itemSizing)
            
            if imageArray.count>0 {
                for (views) in imageArray {
                    if self.frame.width > originX+itemSizing{
                        views.frame = CGRect(x: originX, y: originY,width: itemSizing,height: itemSizing)
                        originX = originX+itemSizing+spacings.rightSpacing
                        
                        // checking done here to avoid the case of over lapping. 
                        if self.frame.width < originX+itemSizing + spacings.rightSpacing{
                            originY = itemSizing+spacings.bottomSpacing+spacings.topSpacing+originY
                            originX = spacings.leftSpacing
                        }
                        
                    }else{
                        originX = spacings.leftSpacing
                        originY = itemSizing+spacings.bottomSpacing+spacings.topSpacing
                        views.frame = CGRect(x: originX, y: originY,width: itemSizing ,height: itemSizing )
                    }
                    self.addSubview(views)
                }
                return originY+itemSizing
            }
        }
        return 0.0
    }
    
    func addImagesToMeFromArrayForAttachments(imageArray:[UIImageView]) -> CGFloat {
        
          var subXSizing:CGFloat = 0.0
          var subYSizing:CGFloat = 0.0
        
        if self.bounds.width <= itemSizingforcompose+spacings.leftSpacing+spacings.rightSpacing{
            //condition added here to avoid adding views if the width is less than the minimum size of the view
        }else {
            var originX = spacings.topSpacing
            var originY = spacings.leftSpacing
             _ = CGSize(width: itemSizingforcompose, height: itemSizingforcompose)
            
            if imageArray.count>0 {
                for (views) in imageArray {
                    if self.frame.width > originX+itemSizingforcompose{
                        views.frame = CGRect(x: originX, y: originY,width: itemSizingforcompose,height: itemSizingforcompose)
                        subXSizing = originX
                        subYSizing = originY
                        originX = originX+itemSizingforcompose+spacings.rightSpacing
                        
                        // checking done here to avoid the case of over lapping.
                        if self.frame.width < originX+itemSizingforcompose + spacings.rightSpacing{
                            originY = itemSizingforcompose+spacings.bottomSpacing+spacings.topSpacing+originY
                            originX = spacings.leftSpacing
                        }
                        
                    }else{
                        originX = spacings.leftSpacing
                        originY = itemSizingforcompose+spacings.bottomSpacing+spacings.topSpacing
                        views.frame = CGRect(x: originX, y: originY,width: itemSizingforcompose ,height: itemSizingforcompose )
                        subXSizing = originX
                        subYSizing = originY
                        
                    }
                    if views.bounds.height <= views.bounds.width{
                    attachmentDeleteButton = UIButton(frame: CGRect(x: subXSizing, y: subYSizing - 10, width: 15, height: 15))
                    }
                    else{
                    attachmentDeleteButton = UIButton(frame: CGRect(x: subXSizing, y: subYSizing - 12, width: 15, height: 15))
                    }
                    let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
                    
                    attachmentDeleteButton.backgroundColor = colur
                    attachmentDeleteButton.setTitle("X", for: .normal)
                    attachmentDeleteButton.titleLabel!.font =  UIFont(name:"Roboto", size: 8)

                    attachmentDeleteButton.layer.cornerRadius = 0.5 * attachmentDeleteButton.bounds.size.width
                    attachmentDeleteButton.tag = views.tag
                    attachmentDeleteButton.addTarget(self, action: #selector(attachmentDeleteTap), for: .touchUpInside)
                    
                    self.addSubview(attachmentDeleteButton);
                    self.addSubview(views)
                }
                return originY+itemSizingforcompose
            }
        }
        return 0.0
    }
    
    
   
    public func addColumnOfView(arrayOfViews:[UIView]){
        
        var previousView:UIView?
        for view in arrayOfViews{
            previousView = view
            self.addSubview(view)
            //self.translatesAutoresizingMaskIntoConstraints = false //Don't forget this line
            
            
            let leftSideConstraint = NSLayoutConstraint(item:view , attribute: .left, relatedBy: .equal, toItem:self , attribute: .left, multiplier: 1.0, constant: 0.0)
            
            let rightConstraint = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem:self , attribute: .right, multiplier: 1.0, constant: 0.0)
            
            
            var topConstraint:NSLayoutConstraint?
            
            if view == arrayOfViews.first {
                topConstraint = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem:self , attribute: .top, multiplier: 1.0, constant: 0.0)
            }else {
                topConstraint =  NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem:previousView , attribute: .bottom, multiplier: 1.0, constant: 0.0)
            }
            
            if view == arrayOfViews.last {
                let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem:self , attribute: .bottom, multiplier: 1.0, constant: 0.0)
                self.addConstraint(bottomConstraint)
            }else {
            
            }
            self.addConstraints([leftSideConstraint, topConstraint!, rightConstraint])
        }
    }
    
    func createDynamicButtons(permission:[String])
    {
        for _ in 0..<permission.count {
        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 100, height: 50))
        button.backgroundColor = .green
        button.setTitle("Test Button", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
       
    }
    }
    
        
        
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
    }
    
     @objc private func attachmentDeleteTap(sender: UIButton!) {
        
        
        
     self.delegate?.SelectedAttachmentTag(selectedAttachment: sender.tag)
            
        
        
    }
    
}
