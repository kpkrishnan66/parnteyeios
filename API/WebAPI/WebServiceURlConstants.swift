//
//  WebServiceURlConstants.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

//TODO: need to check here with vivek as the web service could lead to decrepancies
/**
 *  Struct to store the web service url constants.
 */
struct webServiceUrls {
    
    
    //static let baseURl                  = "http://188.166.223.141/parenteye_test/index.php/parentEyeApi/"
    //static let baseURl                  = "http://192.168.100.8/parenteye-web/index.php/parentEyeApi/"
    
    
    //static var baseURl                  = "http://www.peeveesparenteye.com/index.php/parentEyeApi/"
    
    
    static var baseURl            = "http://www.sreemaharshi.parenteye.org/index.php/parentEyeApi/"
    static var url                = ""
    //static let baseURl          = "http://188.166.223.141/parenteye_v42/index.php/parentEyeApi/"
    //static var baseURl            = "http://188.166.223.141/parenteye_test/index.php/parentEyeApi/"
    //static var baseURl            = "http://192.168.0.29/parenteye-web/index.php/parentEyeApi/"
    //static var baseURl               = "http://parenteye.org/index.php/parentEyeApi/"
    
    static let loginUrl                 = webServiceUrls.baseURl+"login"
    static let studentBaseUrl           = webServiceUrls.baseURl+"student/"
    static let studentDailyCareReport   = webServiceUrls.studentBaseUrl+"forDailyCareReport/1/dcrDate/"
    static let studentMessageListUrl    = webServiceUrls.baseURl+"messageInbox/"
    static let upComingEventsUrl        = webServiceUrls.baseURl+"upcomingEvents"
    static let studentImageUploadURl    = webServiceUrls.baseURl+"studentImage"
    static let userImageUploadURl       = webServiceUrls.baseURl+"userImage"
    static let studentDiaryUrl          = webServiceUrls.baseURl+"studentDiary/"
    static let studentComposeMessageUrl = webServiceUrls.baseURl+"userRole"
    static let messageDetailedUrl       = webServiceUrls.baseURl+"messageDetails"
    static let messageReplyUrl          = webServiceUrls.baseURl+"message"
    static let messagepermissionRoles   = webServiceUrls.baseURl+"recipientRoles/id/"
    static let messageRecipients        = webServiceUrls.baseURl+"recipients/id/"
    static let recipientsForMessage     = webServiceUrls.baseURl+"recipientsForMessage/messageId/"
    static let messageSetRead           = webServiceUrls.baseURl+"updateMessageToRead"
    static let changePassword           = webServiceUrls.baseURl+"changePassword"
    static let notificationsUrl         = webServiceUrls.baseURl+"notice"
    static let notificationImgAttachmentsUrl = webServiceUrls.baseURl+"noticeAttachment"
    static let ureadCountUrl            = webServiceUrls.baseURl+"unreadCount"
    static let oneTimePassword          = webServiceUrls.baseURl+"oneTimePassword/mobileNo/"
    static let switchRole               = webServiceUrls.baseURl+"switchRole/"
    static let noticeAttachmentuploadUrl = webServiceUrls.baseURl+"noticeAttachment/"
    static let employeeDiaryUrl          = webServiceUrls.baseURl+"diary/"
    static let studentListUrl           = webServiceUrls.baseURl+"student/"
    static let studentPromotionListUrl  = webServiceUrls.baseURl+"studentPromotion/"
    static let diaryAttachmentuploadUrl = webServiceUrls.baseURl+"diaryAttachment/"
    static let messageAttachmentuploadUrl = webServiceUrls.baseURl+"messageAttachment/"
    static let studentDetailedDiaryurl  = webServiceUrls.baseURl+"studentDiary/forDeliveryReport/1/"
    static let busArrivalStatus         = webServiceUrls.baseURl+"busDelay/"
    static let latestLocation           = webServiceUrls.baseURl+"latestLocation/"
    static let progressReport           = webServiceUrls.baseURl+"progressCardV2/"
    static let progressCard           = webServiceUrls.baseURl+"studentProgressCard/"
    static let progressGraphReport      = webServiceUrls.baseURl+"progressCard/"
    static let nutShellReport           = webServiceUrls.baseURl+"nutshell/"
    static let leaveStatus              = webServiceUrls.baseURl+"studentLeaveRecord/"
    static let detailedView             = webServiceUrls.baseURl+"student/detailedView/1/"
    static let nextStudentView          = webServiceUrls.baseURl+"student/schoolClassId/"
    static let getEmployeeList          = webServiceUrls.baseURl+"employee/"
    static let getAttendanceReportList  = webServiceUrls.baseURl+"getAttendanceReport/"
    static let gethomeWorkList          = webServiceUrls.baseURl+"homework/"
    static let gethomeworkStudentsDetails = webServiceUrls.baseURl+"homeworkStudentsDetails/"
    static let subjects                   = webServiceUrls.baseURl+"subjects/"
    static let classListFromSubjectId     = webServiceUrls.baseURl+"classListFromSubjectId/"
    static let HomeworkAttachmentuploadUrl = webServiceUrls.baseURl+"homeworkAttachment/"
    static let gettimeTableList            = webServiceUrls.baseURl+"timeTable/"
    static let getClassSubTeachertimeTableList = webServiceUrls.baseURl+"employee"
    static let postSubstitutionTimetableUrl    = webServiceUrls.baseURl+"employeeSubstitution"
    static let cancelSubstitutionTimetableurl    = webServiceUrls.baseURl+"cancelSubstitution"
    static let schoolClass                 = webServiceUrls.baseURl+"schoolClass"
    static let checkForUpdate           = webServiceUrls.baseURl+"version/parenteye/1/forIOS/1"
    static let group                    = webServiceUrls.baseURl+"group"
    static let classListofCorporateManger = webServiceUrls.baseURl+"classList/"
    static let examListOfMarkEntry        = webServiceUrls.baseURl+"exam/"
    static let studentExamListOfMarkEntry = webServiceUrls.baseURl+"studentExamList/"
    static let studentAcademicEvaluation  = webServiceUrls.baseURl+"studentAcademicEvaluation/"
    static let getAttendanceReportOfClass  = webServiceUrls.baseURl+"studentLeaveRecord/"
    static let postPromotionStatus        = webServiceUrls.baseURl+"studentPromotion"
    static let studentAdmission          = webServiceUrls.baseURl+"classLevel/"
    static let submitStudentAdmission    = webServiceUrls.baseURl+"studentAdmission/"
    static let utilityFee                = webServiceUrls.baseURl+"utilityFee/"
    static let utilityFeeInvoiceDownload = webServiceUrls.baseURl+"utilityFeeInvoiceDownload/"
    //static let getInvoiceList            = webServiceUrls.baseURl+"dummyInvoice"
    static let getInvoiceList            = webServiceUrls.baseURl+"invoice"
    static let checkInvoice            = webServiceUrls.baseURl+"checkInvoice"
    static let feePaymentDetails       = webServiceUrls.baseURl+"feePaymentDetails"
    static let downloadInvoice         = webServiceUrls.baseURl+"feeInvoiceDownload"
    static let convertAudioPlay        = webServiceUrls.baseURl+"convertAudioPlay/"
    static let classTestListing        = webServiceUrls.baseURl+"classTestListing/"
    static let checkForSuccessPayment  = webServiceUrls.baseURl+"checkForSuccessPayment/"
    static let nutshellRsult           = webServiceUrls.baseURl+"nutshellV1/"
    static let graphRsult              = webServiceUrls.baseURl+"graphResult/"
    static let evaluationActivity      = webServiceUrls.baseURl+"evaluationActivity/"
    static let classSubject            = webServiceUrls.baseURl+"classSubject/"
    static let evaluationActivitySubject = webServiceUrls.baseURl+"evaluationActivitySubject/"
    static let studentEvaluationSubjectList = webServiceUrls.baseURl+"studentEvaluationSubjectList/"
    static let classTestMarkList = webServiceUrls.baseURl+"classTestMarkList/"
    static let studentAcademicEvaluationV1 = webServiceUrls.baseURl+"studentAcademicEvaluationV1/"
    static let deviceDetails               = webServiceUrls.baseURl+"deviceDetails/"
    static let notificationDetailsV2       = webServiceUrls.baseURl+"notificationDetailsV2/"
    
}
/// Closures for success and failure on web service
public typealias  successClosure  = (_ result: Dictionary<String, AnyObject>) -> Void
public typealias  failureClosure  = (_ error:NSError)->Void
public typealias  progressClosure = (_ progress:Float)->Void


public typealias   imageDownloadScuccess = (_ image:UIImage)->Void
public typealias  imageFailureClosure  = (_ error:NSError?)->Void

struct webServiceParameterConstants {
    static let userName                  = "User[userName]"
    static let userEasyLogin             = "User[easyLogin]"
    static let useroneTimePassword       = "User[oneTimePassword]"
    
    static let password                  = "User[password]"
    static let recipientId               = "recipientId"
    static let paginationFlag            = "paginationFlag"
    static let page                      = "offset"
    static let studentID                 = "studentId"
    static let groupID                   = "groupId"
    static let student                   = "student"
    static let fileParameter             = "file"
    static let dateGrouped               = "dateGrouped"
    static let corporateId               = "corporateId"
    static let userRoleId                = "userRoleId"
    static let type                      = "type"
    static let userId                    = "userId"
    static let role                      = "role"
    static let schoolClassId             = "schoolClassId"
    static let schoolClassIdList         = "schoolClassIdList"
    static let diaryId                   = "diaryId"
    static let withClassListschoolClassId = "withClassList/1/schoolClassId/"
    static let date                       = "date"
    static let leaveStatus                = "leaveStatusNotEquals/Submitted"
    static let absentDate                 = "absentDate"
    static let choosingAbsentees          = "/choosingAbsentees/1"
    static let studentProfileNextStudent  = "/detailedView/1/navigateTo/next/currentStudentName/"
    static let studentProfilePreviousStudent = "/detailedView/1/navigateTo/previous/currentStudentName/"
    static let subjectId                  = "subjectId"
    static let contactList                  = "contactList/1/"
    static let parenteye                  = "parenteye"
    static let examId                     = "examId"
    
    
    
    
    
    
    
    
    
    
    static let roleTypeKey               = "roleType"
    static let roleTypeEmployee          = "Employee"
    static let permissionNameKey         = "permissionName"
    static let permissionNameTeacher     = "Teacher"
    
    static let messageRecipientIDKey     = "recipientId"
    static let messageThreadID           = "threadId"
    
    static let messageContentKey         = "Message[content]"
    static let messageTypeKey            = "Message[messageType]"
    static let messageTitleKey           = "Message[subject]"
    static let messageSchoolIdKey        = "Message[schoolId]"
    static let messageThreadIdKey        = "Message[threadId]"
    static let messageRecipientIdListKey = "Message[recipientIdList]"
    static let messageRecipientKeyIdList = "Message[recipientKeys]"
    static let messageGroupIdLit         = "Message[groupIdList]"
    static let messageRootMessageId      = "Message[rootMessageId]"
    static let messageaAddedByIdKey      = "Message[addedById]"
    static let messagerecipientSchoolIdList = "Message[recipientSchoolIdList]"
    static let messageacorporateId       = "Message[corporateId]"
    
    
    
    static let messageStudentId          = "Message[studentId]"
    static let messageAddedByKey         = "addedById"
    static let messageTypeReply          = "Reply"
    static let nameLikeKey               = "nameLike"
    static let messageAttachment         = "Message[attachmentIdList]"
    
    static let messageReciepientThreadID = "MessageRecipient[threadId]"
    static let messageReceipientID       = "MessageRecipient[recipientId]"
    static let messageAddedByID          = "MessageRecipient[addedById]"
    
    static let changePasswordUsername           = "ChangePasswordModel[userName]"
    static let changePasswordOldPassword        = "ChangePasswordModel[oldPassword]"
    static let changePasswordNewPassword        = "ChangePasswordModel[newPassword]"
    static let changePasswordConfirmNewPassword = "ChangePasswordModel[confirmationPassword]"
    static let changePasswordWithOtp            = "ChangePasswordModel[otp]"
    
    static let viewByPhotoKey           = "viewByPhoto"
    static let forDailyCareReport       = "forDailyCareReport"
    
    static let noticeId                = "noticeId"
    static let noticeReadUpdate        = "NoticeRecipient[status]"
    static let noticeIsread            = "Read"
    static let id                      = "id"
    static let studentDiaryReadyUpdate = "StudentDiary[status]"
    static let diaryIsread             = "Read"
    
    static let composeNoticeContent             = "Notice[content]"
    static let composeNoticeTitle               = "Notice[title]"
    static let composeNoticeType                = "Notice[type]"
    static let composeNoticeAddedById           = "Notice[addedById]"
    static let composeNoticeEventDate           = "Notice[eventDate]"
    static let composeNoticeAttachmentList      = "Notice[attachmentIdList]"
    static let composeNoticerecipientSchoolIdList = "Notice[schoolIdList]"
    static let composeNoticeCorporateId          = "Notice[corporateId]"
    
    
    static let composeDiarySubject              = "Diary[subject]"
    static let composeDiaryRecipientStudentList = "Diary[recipientStudentIdList]"
    static let composeDiarycontent              = "Diary[content]"
    static let composeDiaryRecipientClassList   = "Diary[recipientClassIdList]"
    static let composeDiaryAddedbyid            = "Diary[addedById]"
    static let composeDiarysubmissionDate       = "Diary[submissionDate]"
    static let composeDiaryAttachmentidList     = "Diary[diaryAttachmentIdList]"
    static let composeDiaryrecipientSchoolIdList = "Diary[recipientSchoolIdList]"
    static let composeDiaryCorporateId          = "Diary[corporateId]"
    static let composeDiarySChoolId            = "Diary[schoolId]"
    
    
    static let composeHomeworkTitle                  = "Homework[title]"
    static let composeHomeworkDescription            = "Homework[description]"
    static let composeHomeworkrecipientStudentlist   = "Homework[recipientStudentIdList]"
    static let composeHomeworkrecipientClasslist     = "Homework[recipientClassIdList]"
    static let composeHomeworkAddedById              = "Homework[addedById]"
    static let composeHomeworkSubmissionDate         = "Homework[submissionDate]"
    static let composeHomeworkReference              = "Homework[reference]"
    static let composeHomeworkSubjectId              = "Homework[subjectId]"
    static let composeHomeworkAttachmentIdList       = "Homework[homeworkAttachmentIdList]"
    
    static let StudentLeaveRecordDateList       = "StudentLeaveRecord[dateList]"
    static let StudentLeaveRecordAddedById      = "StudentLeaveRecord[addedById]"
    static let StudentLeaveRecordStudentIdList  = "StudentLeaveRecord[studentIdList]"
    static let StudentLeaveRecordSchoolClassId  = "StudentLeaveRecord[schoolClassId]"
    
    static let busID = "busId"
    static let graphID = "forGraph"
    static let withNickName = "withNickName"
    static let termWiseGrouped = "termWiseGrouped"
    static let forIos = "forIos"
    static let dateList        = "StudentLeaveRecord[dateList]"
    static let addedById       = "StudentLeaveRecord[addedById]"
    static let reason          = "StudentLeaveRecord[reason]"
    static let studentIdList   = "StudentLeaveRecord[studentIdList]"
    static let description     = "StudentLeaveRecord[description]"
    
    
    static let schoolId        = "schoolId"
    static let schoolIdList    = "schoolIdList"
    static let employeeId      = "employeeId"
    static let homeworkId      = "homeworkId"
    static let timeTableId     = "timeTableId"
    static let forTimeTable    = "forTimeTable"
    
    
    
    static let employeeForceSubstitution =  "EmployeeSubstitution[forceSubstitution]"
    static let employeesubstitutedById   =  "EmployeeSubstitution[substitutedById]"
    static let subemployeeId =  "EmployeeSubstitution[employeeId]"
    static let subtimetableId =  "EmployeeSubstitution[timeTableId]"
    static let schoolClass     = "schoolClass"
    static let timeTableDetails  = "timeTableDetails"
    
    static let groupName         = "Group[name]"
    static let groupaddedbyId    = "Group[addedById]"
    static let groupmemberIdList = "Group[memberIdList]"
    static let groupId           = "Group[id]"
    
    static let markEntryExamSubjectId = "StudentAcademicEvaluation[0][examSubjectId]"
    static let markEntryExamId = "StudentAcademicEvaluation[0][examId]"
    static let markEntrymaxMark = "StudentAcademicEvaluation[0][maxMark]"
    static let markEntrysubjectId = "StudentAcademicEvaluation[0][subjectId]"
    static let markEntrystudentId = "StudentAcademicEvaluation[0][studentId]"
    static let markEntrymark = "StudentAcademicEvaluation[0][mark]"
    static let markEntryschoolClassId = "StudentAcademicEvaluation[0][schoolClassId]"
    
    static let studentTransferType = "StudentPromotion[promotedById]"
    
    static let studentAdmissionStudentName = "StudentAdmission[studentName]"
    static let studentAdmissionclassName = "StudentAdmission[className]"
    static let studentAdmissionstrDob    = "StudentAdmission[strDob]"
    static let studentAdmissionfatherName = "StudentAdmission[fatherName]"
    static let studentAdmissionmotherName = "StudentAdmission[motherName]"
    static let studentAdmissionmobileNo = "StudentAdmission[mobileNo]"
    static let studentAdmissionemailId = "StudentAdmission[emailId]"
    static let studentAdmissionaddress = "StudentAdmission[address]"
    static let studentAdmissionschoolName = "StudentAdmission[schoolName]"
    static let studentAdmissionnativeResident = "StudentAdmission[nativeResident]"
    static let studentAdmissionschoolCode = "StudentAdmission[schoolCode]"
    
    static let admissionFeeSchoolCode   =  "UtilityFee[schoolCode]"
    static let admissionFeeFeeType      = "UtilityFee[feeType]"
    static let admissionFeeStudentRefId = "UtilityFee[studentRefernceId]"
    static let utilityFeePaymentStatus  = "UtilityFee[paymentStatus]"
    static let utilityFeePaymentId      = "UtilityFee[paymentId]"
    static let feePaymentDetailsStatus = "FeePaymentDetails[status]"
    static let feePaymentDetailspaymentId = "FeePaymentDetails[paymentId]"
    
    
    
}

class webServiceRolesGroupForDifferentActivities {
    static let timeTableshiftingOptionGroup = ["Principal","SchoolAdministrator","Manager","CorporateManager"]
    static let timeTableActivitiesOptionGroup = ["Teacher","Coordinator"]
    static let timeTableClassTimetableOptionGroup = ["NonTeachingStaff"]
    static let homeWorkViewOnlyGroup = ["NonTeachingStaff"]
    static let addNewNoticeForStaffOrForAllOptionGroup = ["Principal","School Administrator","Manager"]
    static let markEntryEligibleGroup = ["Principal","SchoolAdministrator","Coordinator","Teacher","Manager"]
    static let attendanceReportGroup = ["Principal","SchoolAdministrator","Manager","NonTeachingStaff","CorporateManager"]
}

class colorsForGraph {
    static let colorArray = [[UIColor(red: 193/255.0, green: 37/255.0, blue: 82/255.0, alpha: 1.0)],
                             [UIColor(red: 255/255.0, green: 102/255.0, blue: 0/255.0, alpha: 1.0)],
                             [UIColor(red: 49/255.0, green: 185/255.0, blue: 158/255.0, alpha: 1.0)],
                             [UIColor(red: 175/255.0, green: 181/255.0, blue: 48/255.0, alpha: 1.0)],
                             [UIColor(red: 51/255.0, green: 47/255.0, blue: 177/255.0, alpha: 1.0)],
                             [UIColor(red: 224/255.0, green: 0/255.0, blue: 115/255.0, alpha: 1.0)],
                             [UIColor(red: 247/255.0, green: 152/255.0, blue: 9/255.0, alpha: 1.0)],
                             [UIColor(red: 67/255.0, green: 194/255.0, blue: 221/255.0, alpha: 1.0)],
                             [UIColor(red: 191/255.0, green: 134/255.0, blue: 134/255.0, alpha: 1.0)],
                             [UIColor(red: 179/255.0, green: 48/255.0, blue: 80/255.0, alpha: 1.0)],
                             [UIColor(red: 192/255.0, green: 255/255.0, blue: 140/255.0, alpha: 1.0)],
                             [UIColor(red: 255/255.0, green: 247/255.0, blue: 140/255.0, alpha: 1.0)],
                             [UIColor(red: 255/255.0, green: 208/255.0, blue: 140/255.0, alpha: 1.0)],
                             [UIColor(red: 140/255.0, green: 234/255.0, blue: 255/255.0, alpha: 1.0)],
                             [UIColor(red: 255/255.0, green: 140/255.0, blue: 157/255.0, alpha: 1.0)]]
    
    
}

