//
//  webServiceController.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
public class WebServiceApi {
   
    /**
     Controller function to login the user
     
     - parameter userName: username
     - parameter password: password
     - parameter success:  Block to be exectuted on success
     - parameter failure:  Block to be exectuted on failure
     - parameter progress: Block to be exectuted on on progress
     */
    
    public class func logMeInServerWithUserName(userName:String,password:String,loggedin:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        var parameterDictionary:[String:String] = ["":""]
        if(!loggedin){
        let easyloginString = "true"
        parameterDictionary = [webServiceParameterConstants.userName:userName,webServiceParameterConstants.userEasyLogin:easyloginString];
        }
        else{
        parameterDictionary = [webServiceParameterConstants.userName:userName,webServiceParameterConstants.password:password];
        }
        
        let loginUrl = NSURL(string: webServiceUrls.loginUrl)!
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameterDictionary, withUrl: loginUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    
    /**
     function to get message
     
     - parameter page:     pagination page needed
     - parameter success:  Block to be exectuted on success
     - parameter failure:  Block to be exectuted on failure
     - parameter progress: Block to be exectuted on progress
     */
    

    public class func getMessagesForPage(page:Int, forUserWithId userID:String, monthLimit messageMonthLimit:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
//        let parameterDictionary = [webServiceParameterConstants.recipientId:userID,
//                                       webServiceParameterConstants.page:String(page)
//                                       ]
        let messageUrl = webServiceUrls.studentMessageListUrl+webServiceParameterConstants.userRoleId+"/"+userID+"/"+webServiceParameterConstants.page+"/"+String(page)+"/monthLimit/"+messageMonthLimit
        print("message url======="+messageUrl)
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: messageUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getUpComingEventsFromWebForUseID(useriD:String, correspondingToStudentID studentID:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let parameterDictionary = [webServiceParameterConstants.recipientId:useriD,
                                   webServiceParameterConstants.studentID:studentID
                                   ]
        let upcomingEventsUrl = NSURL(string: webServiceUrls.upComingEventsUrl)!
        
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: upcomingEventsUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    public class func getBusTime(correspondingTobusID busID:String,onSuccess success:@escaping successClosure,onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let parameterDictionary = [webServiceParameterConstants.busID:busID]
        let busTimeUrl = NSURL(string: webServiceUrls.busArrivalStatus)!
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: busTimeUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getBusLocation(correspondingTostudentID studentID:String,onSuccess success:@escaping successClosure,onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let parameterDictionary = [webServiceParameterConstants.studentID:studentID]
        let latestLocationUrl = NSURL(string: webServiceUrls.latestLocation)!
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: latestLocationUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func checkForUpdates(onSuccess success:@escaping successClosure,onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let parameterDictionary = [webServiceParameterConstants.parenteye:"1",webServiceParameterConstants.forIos:"1"]
        let versionUrl = NSURL(string: webServiceUrls.checkForUpdate)!
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: versionUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func upLoadStudentProfileImage(image:UIImage, forStudentWithId stdId:String,userType:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var paramDicto = ["":""]
        var uploadUrl = ""
       
        if userType == "Guardian"{
         paramDicto = [webServiceParameterConstants.studentID:stdId];
         uploadUrl  =  webServiceUrls.studentImageUploadURl+"/"+webServiceParameterConstants.studentID+"/"+stdId
        }
        if userType == "Employee"{
            paramDicto = [webServiceParameterConstants.userId:stdId];
            uploadUrl  =  webServiceUrls.userImageUploadURl+"/"+webServiceParameterConstants.userId+"/"+stdId
        }
        
       let  imageData  = UIImageJPEGRepresentation(image, 0.0)!

        WebServiceModel.uploadImage((imageData as NSData) as Data, imageUploadUrl: uploadUrl, withParametes: paramDicto, tagForImage: webServiceParameterConstants.fileParameter,
            onSuccess: success,
            onFailure: failure,
            duringProgress: progress)
    }
    
    public class func upLoadNoticeAttachmentImage(image:UIImage,cameraSelected:Bool,
                                                  onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure,duringProgress progress:progressClosure?){
       var uploadimageData =  NSData()
        let paramDicto = ["":""]
        let uploadUrl  =  webServiceUrls.noticeAttachmentuploadUrl
        
        if cameraSelected == true{
            if let  imageData  = (UIImageJPEGRepresentation(image, 0.0)){
                uploadimageData = imageData as NSData
                
            }
        }
        else{
            if let  imageData  = (UIImageJPEGRepresentation(image, 1)){
                uploadimageData = imageData as NSData
            }
        }
        
        WebServiceModel.uploadImage((uploadimageData) as Data, imageUploadUrl: uploadUrl, withParametes: paramDicto, tagForImage: webServiceParameterConstants.fileParameter,
                                    onSuccess: success,
                                    onFailure: failure,
                                    duringProgress: progress)
    }
    
    public class func upLoadDiaryAttachmentImage(image:UIImage,cameraSelected:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure,duringProgress progress:progressClosure?){
        var uploadimageData =  NSData()
        let paramDicto = ["":""]
        let uploadUrl  =  webServiceUrls.diaryAttachmentuploadUrl
        
        if cameraSelected == true{
            if let  imageData  = (UIImageJPEGRepresentation(image, 0.0)){
                uploadimageData = imageData as NSData
                
         }
        }
        else{
        if let  imageData  = (UIImageJPEGRepresentation(image, 1)){
            uploadimageData = imageData as NSData
         }
        }
        
        WebServiceModel.uploadImage((uploadimageData) as Data, imageUploadUrl: uploadUrl, withParametes: paramDicto, tagForImage: webServiceParameterConstants.fileParameter,
                                    onSuccess: success,
                                    onFailure: failure,
                                    duringProgress: progress)
    }
    
    public class func upLoadMessageAttachmentImage(image:UIImage,cameraSelected:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure,duringProgress progress:progressClosure?){
        var uploadimageData =  NSData()
        let paramDicto = ["":""]
        let uploadUrl  =  webServiceUrls.messageAttachmentuploadUrl
        
        if cameraSelected == true{
            if let  imageData  = (UIImageJPEGRepresentation(image, 0.0)){
                uploadimageData = imageData as NSData
                
            }
        }
        else{
            if let  imageData  = (UIImageJPEGRepresentation(image, 1)){
                uploadimageData = imageData as NSData
            }
        }
        
        WebServiceModel.uploadImage((uploadimageData) as Data, imageUploadUrl: uploadUrl, withParametes: paramDicto, tagForImage: webServiceParameterConstants.fileParameter,
                                    onSuccess: success,
                                    onFailure: failure,
                                    duringProgress: progress)
    }
    
    public class func upLoadAttachmentDocument(uploadDocumentData:NSData,docName:String,selectedTab:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure,duringProgress progress:progressClosure?){

        let paramDicto = ["":""]
        var uploadUrl:String = ""
        
        if selectedTab == "Message" {
         uploadUrl  =  webServiceUrls.messageAttachmentuploadUrl
        }
        
        if selectedTab == "Homework" {
         uploadUrl  =  webServiceUrls.HomeworkAttachmentuploadUrl
        }
        
        if selectedTab == "Notification" {
         uploadUrl = webServiceUrls.notificationImgAttachmentsUrl
        }
        
        if selectedTab == "Diary"  {
          uploadUrl = webServiceUrls.diaryAttachmentuploadUrl
        }
        
        let escapeAddress = uploadUrl.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        WebServiceModel.uploadDocument((uploadDocumentData) as Data, documentUploadUrl: escapeAddress!, withParametes: paramDicto, tagForDocument: webServiceParameterConstants.fileParameter,docName: docName,onSuccess: success,onFailure: failure,duringProgress: progress)
    }
    
    public class func upLoadHomeworkAttachmentImage(image:UIImage,cameraSelected:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure,duringProgress progress:progressClosure?){
        var uploadimageData =  NSData()
        let paramDicto = ["":""]
        let uploadUrl  =  webServiceUrls.HomeworkAttachmentuploadUrl
        
        if cameraSelected == true{
            if let  imageData  = (UIImageJPEGRepresentation(image, 0.0)){
                uploadimageData = imageData as NSData
                
            }
        }
        else{
            if let  imageData  = (UIImageJPEGRepresentation(image, 1)){
                uploadimageData = imageData as NSData
            }
        }
        
        WebServiceModel.uploadImage((uploadimageData) as Data, imageUploadUrl: uploadUrl, withParametes: paramDicto, tagForImage: webServiceParameterConstants.fileParameter,
                                    onSuccess: success,
                                    onFailure: failure,
                                    duringProgress: progress)
    }
    
    
    
    public class func getDiaryEntry(studentId:String,classId:Int,selectedUser:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?,forPage page:Int){
        var diaryEntryUrl:String = ""
        
        if selectedUser == "Guardian"{
        diaryEntryUrl = webServiceUrls.studentDiaryUrl+webServiceParameterConstants.studentID+"/"+studentId+"/"+webServiceParameterConstants.page+"/"+String(page)+"/"+webServiceParameterConstants.dateGrouped+"/"+"1"
        }
        else{
        diaryEntryUrl = webServiceUrls.employeeDiaryUrl+webServiceParameterConstants.schoolClassId+"/"+String(classId)+"/"+webServiceParameterConstants.page+"/"+String(page)+"/"+webServiceParameterConstants.dateGrouped+"/"+"1"
            
        }
        
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: diaryEntryUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    
    }
    
    public class func getDiaryDetailedEntryList(diaryId:String,classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        var diaryEntryUrl:String = ""
        
        diaryEntryUrl = webServiceUrls.studentDetailedDiaryurl+webServiceParameterConstants.diaryId+"/"+diaryId+"/"+webServiceParameterConstants.withClassListschoolClassId+String(classId)
        
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: diaryEntryUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getDiaryDetailedStudentEntryList(diaryId:String,classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        var diaryEntryUrl:String = ""
        
        diaryEntryUrl = webServiceUrls.studentDetailedDiaryurl+webServiceParameterConstants.diaryId+"/"+diaryId+"/"+webServiceParameterConstants.schoolClassId+"/"+String(classId)
        
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: diaryEntryUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAttendanceStatus(date:String,classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var attendanceStatusUrl:String = ""
        let dates = Foundation.Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyy"
        let result = formatter.string(from: dates)
        
        if date > result{
             attendanceStatusUrl = webServiceUrls.leaveStatus+webServiceParameterConstants.date+"/"+date+"/"+webServiceParameterConstants.schoolClassId+"/"+String(classId)
        }else{
             attendanceStatusUrl = webServiceUrls.leaveStatus+webServiceParameterConstants.date+"/"+date+"/"+webServiceParameterConstants.schoolClassId+"/"+String(classId)+"/"+webServiceParameterConstants.leaveStatus
        }
            
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: attendanceStatusUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAttendanceReportStatus(date:String,schoolId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let attendanceStatusUrl = webServiceUrls.getAttendanceReportList+webServiceParameterConstants.date+"/"+date+"/"+webServiceParameterConstants.schoolId+"/"+String(schoolId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: attendanceStatusUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAttendanceReportOfClass(date:String,schoolClassId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let attendanceReportUrl = webServiceUrls.getAttendanceReportOfClass+webServiceParameterConstants.date+"/"+date+"/"+webServiceParameterConstants.schoolClassId+"/"+String(schoolClassId)+"/leaveStatusNotEquals/Submitted"
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: attendanceReportUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    public class func getEmployeeSelectedClassStudentList(classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getStudentListUrlUrl = webServiceUrls.studentBaseUrl+webServiceParameterConstants.schoolClassId+"/"+String(classId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getStudentListUrlUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
   
    public class func getEmployeeSelectedClassDetailedStudentProfileList(studentId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getStudentListUrlUrl = webServiceUrls.detailedView+webServiceParameterConstants.id+"/"+String(studentId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getStudentListUrlUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getEmployeeSelectedClassDetailedNextStudentProfileList(classId:Int,studentName:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let getNextStudentListUrl = webServiceUrls.nextStudentView+String(classId)+webServiceParameterConstants.studentProfileNextStudent+String(studentName)
        let escapeAddress = getNextStudentListUrl.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: escapeAddress!)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getEmployeeSelectedClassDetailedPreviousStudentProfileList(classId:Int,studentName:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getPreviousStudentListUrl = webServiceUrls.nextStudentView+String(classId)+webServiceParameterConstants.studentProfilePreviousStudent+String(studentName)
        let escapeAddress = getPreviousStudentListUrl.addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: escapeAddress!)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getHomeWorkEntry(studentId:String,classId:Int,selectedUser:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?,forPage page:Int){
        var diaryEntryUrl:String = ""
        
        if selectedUser == "Guardian"{
            diaryEntryUrl = webServiceUrls.gethomeWorkList+webServiceParameterConstants.student+"/"+studentId+"/"+webServiceParameterConstants.dateGrouped+"/"+"1"
        }
        else{
            diaryEntryUrl = webServiceUrls.gethomeWorkList+webServiceParameterConstants.schoolClassId+"/"+String(classId)+"/"+webServiceParameterConstants.dateGrouped+"/"+"1"
            
        }
        
      WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: diaryEntryUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getHomeWorkDetailsEntry(homeWorkId:String,classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        var homeWorkDetailsUrl:String = ""
        
        homeWorkDetailsUrl = webServiceUrls.gethomeworkStudentsDetails+webServiceParameterConstants.homeworkId+"/"+homeWorkId+"/"+webServiceParameterConstants.schoolClassId+"/"+String(classId)
            
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: homeWorkDetailsUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    public class func getHomeworkEmployeeStudentList(userRoleId:String,classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var homeWorkEmployeeSubjectListUrl:String = ""
        
        homeWorkEmployeeSubjectListUrl = webServiceUrls.subjects+webServiceParameterConstants.userRoleId+"/"+String(userRoleId)+"/"+webServiceParameterConstants.schoolClassId+"/"+String(classId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: homeWorkEmployeeSubjectListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getEligibleClassListToComposeHomework(userRoleId:String,subjectId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var homeWorkEmployeeClassListUrl:String = ""
        
        homeWorkEmployeeClassListUrl = webServiceUrls.classListFromSubjectId+webServiceParameterConstants.userRoleId+"/"+String(userRoleId)+"/"+webServiceParameterConstants.subjectId+"/"+String(subjectId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: homeWorkEmployeeClassListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    
    
    
//    public class func setImgWithUrlString(imageUrl:String, forImageView imageView:UIImageView){
//        WebServiceModel.setImgWithUrlString(imageUrl, forImageView: imageView)
//    }
//    
//    public class func setButtonImgWithUrlString(imageUrl:String, forButton button:UIButton){
//      WebServiceModel.setButtonImgWithUrlString(imageUrl, forButton: button)
//    }
    
    //TODO:- role type here should be made generic
    public class func getEligibleRecipientsForSendingMessageForRole(recipientRole:String,userRoleId:String,userType:String,groupId:String,corporateId:String,schoolListString:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?, forStudentID studenId:String){
        
        var eligibleRecipientsUrl:String = ""
        if userType == "Guardian" {
         eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.studentID+"/"+studenId+"/"+webServiceParameterConstants.type+"/"+recipientRole
        }
        else if userType == "Employee" {
            if groupId == "0" {
                eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.type+"/"+recipientRole
  
            }
            else{
                eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.groupID+"/"+groupId

            }
        }
        else{
            if groupId == "0" {
                eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.type+"/"+recipientRole+"/"+webServiceParameterConstants.corporateId+"/"+corporateId+"/"+webServiceParameterConstants.schoolIdList+"/"+schoolListString
                
            }
            else{
                eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.groupID+"/"+groupId+"/"+webServiceParameterConstants.corporateId+"/"+corporateId+"/"+webServiceParameterConstants.schoolIdList+"/"+schoolListString
                
            }
        }
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: eligibleRecipientsUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    
    }
    
    public class func getEligibleRecipientsForSendingMessage(recipientRole:String,userRoleId:String,userType:String,corporateId:String,schoolIdList:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?, forStudentID studenId:String){
        
        var eligibleRecipientsUrl = ""
        if userType == "Employee" {
         eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.type+"/"+recipientRole
        }
        else{
            eligibleRecipientsUrl = webServiceUrls.messageRecipients+userRoleId+"/"+webServiceParameterConstants.type+"/"+recipientRole+"/"+webServiceParameterConstants.corporateId+"/"+corporateId+"/"+webServiceParameterConstants.schoolIdList+"/"+schoolIdList
  
        }
        
       //let escapeAddress = eligibleRecipientsUrl.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        WebServiceModel.makeGetApiCallWithParameters([String:String](), withUrl: NSURL(string: eligibleRecipientsUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    
    public class func getEligibleRolesForSendingMessage(userRoleId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let eligibleRecipientsUrl = webServiceUrls.messagepermissionRoles + userRoleId+"/withGroups/1"
        let url = NSURL(string: eligibleRecipientsUrl)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: url as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getEligibleRolesForSendingMessageWithoutGroups(userRoleId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let eligibleRecipientsUrl = webServiceUrls.messagepermissionRoles + userRoleId
        let url = NSURL(string: eligibleRecipientsUrl)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: url as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getRecipientsForMessage(rootmessageId rootmessageid:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let eligibleRecipientsUrl = webServiceUrls.recipientsForMessage + rootmessageid
        let url = NSURL(string: eligibleRecipientsUrl)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: url as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    
    public class func getMessagesInThreadWithId(idOfThread:String,userRoleId:String,otherguyId: String,readViaPush:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let baseMessageThreadURl = webServiceUrls.messageDetailedUrl
        var parameterDicto:[String:String]

        if(readViaPush){
            if(otherguyId.isEmpty){
                parameterDicto = [webServiceParameterConstants.userRoleId:userRoleId,
                                  webServiceParameterConstants.messageThreadID:idOfThread,
                                  "readViaPush":"1"]
            }else{
                parameterDicto = [webServiceParameterConstants.userRoleId:userRoleId,
                                              webServiceParameterConstants.messageThreadID:idOfThread,
                                              webServiceParameterConstants.recipientId:otherguyId,
                                              "readViaPush":"1"]
            }
        }else{
            if(otherguyId.isEmpty){
                parameterDicto = [webServiceParameterConstants.userRoleId:userRoleId,
                                  webServiceParameterConstants.messageThreadID:idOfThread]
            }else{
                parameterDicto = [webServiceParameterConstants.userRoleId:userRoleId,
                                  webServiceParameterConstants.messageThreadID:idOfThread,
                                  webServiceParameterConstants.recipientId:otherguyId]
            }
            
        }
        let finalString = baseMessageThreadURl+makeStringForUrlFromDictionary(dicto: parameterDicto)
        let finalUrl =  NSURL(string: finalString)!
        print("final String...."+finalString)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: finalUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postReplyToMessageWithID(threadId:String, titleOfMessage title:String, idOfSchool schoolID:String,corporateId:String, listOfreciepient receipientList:String, addedUserId userRoleId:String,contentOfMessage content:String , rootmessageId rootmsgId:String ,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?) {
        
        
        
        let baseMessageThreadURl = webServiceUrls.messageReplyUrl
        
        var parameterDicto:[String:String]
        
        if receipientList.isEmpty{
            
            if corporateId == "0"{
                
                parameterDicto = [webServiceParameterConstants.messageContentKey: content,
                                  
                                  webServiceParameterConstants.messageTypeKey:webServiceParameterConstants.messageTypeReply,
                                  
                                  webServiceParameterConstants.messageTitleKey:title,
                                  
                                  webServiceParameterConstants.messageSchoolIdKey:schoolID,
                                  
                                  webServiceParameterConstants.messageThreadIdKey:threadId,
                                  
                                  webServiceParameterConstants.messageaAddedByIdKey:userRoleId,
                                  
                                  webServiceParameterConstants.messageRootMessageId:rootmsgId]
                
            }else{
                
                parameterDicto = [webServiceParameterConstants.messageContentKey: content,
                                  
                                  webServiceParameterConstants.messageTypeKey:webServiceParameterConstants.messageTypeReply,
                                  
                                  webServiceParameterConstants.messageTitleKey:title,
                                  
                                  webServiceParameterConstants.messageSchoolIdKey:schoolID,
                                  
                                  webServiceParameterConstants.messageacorporateId:corporateId,
                                  
                                  webServiceParameterConstants.messageThreadIdKey:threadId,
                                  
                                  webServiceParameterConstants.messageaAddedByIdKey:userRoleId,
                                  
                                  webServiceParameterConstants.messageRootMessageId:rootmsgId]
                
            }
            
        }else{
            
            if corporateId == "0"{
                
                parameterDicto = [webServiceParameterConstants.messageContentKey: content,
                                  
                                  webServiceParameterConstants.messageTypeKey:webServiceParameterConstants.messageTypeReply,
                                  
                                  webServiceParameterConstants.messageTitleKey:title,
                                  
                                  webServiceParameterConstants.messageSchoolIdKey:schoolID,
                                  
                                  webServiceParameterConstants.messageThreadIdKey:threadId,
                                  
                                  webServiceParameterConstants.messageaAddedByIdKey:userRoleId,
                                  
                                  webServiceParameterConstants.messageRecipientIdListKey:receipientList]
                
            }else{
                
                parameterDicto = [webServiceParameterConstants.messageContentKey: content,
                                  
                                  webServiceParameterConstants.messageTypeKey:webServiceParameterConstants.messageTypeReply,
                                  
                                  webServiceParameterConstants.messageTitleKey:title,
                                  
                                  webServiceParameterConstants.messageSchoolIdKey:schoolID,
                                  
                                  webServiceParameterConstants.messageacorporateId:corporateId,
                                  
                                  webServiceParameterConstants.messageThreadIdKey:threadId,
                                  
                                  webServiceParameterConstants.messageaAddedByIdKey:userRoleId,
                                  
                                  webServiceParameterConstants.messageRecipientIdListKey:receipientList]
                
            }
            
        }
        
        
        let url = NSURL(string: baseMessageThreadURl)
        
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameterDicto, withUrl: url! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
        
        
    }
    
    public class func searchForTokenName(name:String, userRoleId role:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let baseMessageThreadUrl = webServiceUrls.studentComposeMessageUrl
        let parameterDicto      = [webServiceParameterConstants.studentID:role,
                                   webServiceParameterConstants.nameLikeKey:name
                                   ]
        let urlString           = baseMessageThreadUrl+makeStringForUrlFromDictionary(dicto: parameterDicto)
        if  let url = NSURL(string:urlString) {
            WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: url as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        }
    }
    
    public class func postMessageWithTitle(title:String, contentOfMessage message:String, idOfSchool schoolId:String, recipientIdList reciepients:String,recipientKeysIdList recipientKeys:String,groupIdList groupList:String,idOfMesssageAddingPerson userId:String,studentId:String,Role:String,attachmentIdList attachments:String,corporateId:String,recipientSchoolIdList:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters = ["":""]
        let messageSendingUrl =  webServiceUrls.messageReplyUrl
        if Role == "Guardian" {
            if attachments.isEmpty{
                
               parameters = [webServiceParameterConstants.messageContentKey:message,
                        webServiceParameterConstants.messageTitleKey:title,
                          webServiceParameterConstants.messageSchoolIdKey:schoolId,
                          webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                          webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                          webServiceParameterConstants.messageGroupIdLit:groupList,
                          webServiceParameterConstants.messageaAddedByIdKey:userId,
                          webServiceParameterConstants.messageStudentId:studentId
                          ]
            }
            else{
                
                parameters = [webServiceParameterConstants.messageContentKey:message,
                              webServiceParameterConstants.messageTitleKey:title,
                              webServiceParameterConstants.messageSchoolIdKey:schoolId,
                              webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                              webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                              webServiceParameterConstants.messageGroupIdLit:groupList,
                              webServiceParameterConstants.messageaAddedByIdKey:userId,
                              webServiceParameterConstants.messageStudentId:studentId,
                              webServiceParameterConstants.messageAttachment:attachments
                ]

            }
        }
        if Role == "Employee" {
            if attachments.isEmpty{
             if corporateId == "0" {
                
                parameters = [webServiceParameterConstants.messageContentKey:message,
                          webServiceParameterConstants.messageTitleKey:title,
                          webServiceParameterConstants.messageSchoolIdKey:schoolId,
                          webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                          webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                          webServiceParameterConstants.messageGroupIdLit:groupList,
                          webServiceParameterConstants.messageaAddedByIdKey:userId
                      ]
              }
             else{
                parameters = [webServiceParameterConstants.messageContentKey:message,
                              webServiceParameterConstants.messageTitleKey:title,
                              webServiceParameterConstants.messageSchoolIdKey:schoolId,
                              webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                              webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                              webServiceParameterConstants.messageGroupIdLit:groupList,
                              webServiceParameterConstants.messageaAddedByIdKey:userId,
                              webServiceParameterConstants.messagerecipientSchoolIdList:recipientSchoolIdList,
                              webServiceParameterConstants.messageacorporateId:corporateId]
                }
            }
            else{
                if corporateId == "0" {
                    
                parameters = [webServiceParameterConstants.messageContentKey:message,
                              webServiceParameterConstants.messageTitleKey:title,
                              webServiceParameterConstants.messageSchoolIdKey:schoolId,
                              webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                              webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                              webServiceParameterConstants.messageGroupIdLit:groupList,
                              webServiceParameterConstants.messageaAddedByIdKey:userId,
                              webServiceParameterConstants.messageAttachment:attachments
                ]
                }
                else{
                    parameters = [webServiceParameterConstants.messageContentKey:message,
                                  webServiceParameterConstants.messageTitleKey:title,
                                  webServiceParameterConstants.messageSchoolIdKey:schoolId,
                                  webServiceParameterConstants.messageRecipientIdListKey:reciepients,
                                  webServiceParameterConstants.messageRecipientKeyIdList:recipientKeys,
                                  webServiceParameterConstants.messageGroupIdLit:groupList,
                                  webServiceParameterConstants.messageaAddedByIdKey:userId,
                                  webServiceParameterConstants.messageAttachment:attachments,webServiceParameterConstants.messagerecipientSchoolIdList:recipientSchoolIdList,
                                  webServiceParameterConstants.messageacorporateId:corporateId]
                }
            }
            
        }
        WebServiceModel.makePostApiCallWithParameters(parameter: parameters, withUrl: NSURL(string:messageSendingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func saveOrEditNewGroup(groupName:String,userRoleId:String,memberIdList:String,groupId:String,schoolIdListString:String,corporateId:String,userType:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let saveGroupListUrl =  webServiceUrls.group
        var parameters = ["":""]
        
        if groupId == "0" {
            if userType == "Employee" {
                parameters = [webServiceParameterConstants.groupName:groupName,
                          webServiceParameterConstants.groupaddedbyId:userRoleId,
                          webServiceParameterConstants.groupmemberIdList:memberIdList]
            }
            else{
                parameters = [webServiceParameterConstants.groupName:groupName,
                              webServiceParameterConstants.groupaddedbyId:userRoleId,
                              webServiceParameterConstants.groupmemberIdList:memberIdList,
                              webServiceParameterConstants.corporateId :corporateId,
                              webServiceParameterConstants.schoolIdList:schoolIdListString]
            }
        }
        else{
            if userType == "Employee" {
                
            parameters = [webServiceParameterConstants.groupName:groupName,
                          webServiceParameterConstants.groupaddedbyId:userRoleId,
                          webServiceParameterConstants.groupmemberIdList:memberIdList,
                          webServiceParameterConstants.groupId:groupId]
            }
            else{
                parameters = [webServiceParameterConstants.groupName:groupName,
                              webServiceParameterConstants.groupaddedbyId:userRoleId,
                              webServiceParameterConstants.groupmemberIdList:memberIdList,
                              webServiceParameterConstants.groupId:groupId,
                              webServiceParameterConstants.corporateId :corporateId,
                              webServiceParameterConstants.schoolIdList:schoolIdListString]

            }
 
        }
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:saveGroupListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func sendLeave(dateList:String, addedById:String, reason:String, studentIdList:String,description:Bool,descriptionText:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String]
        parameters = ["":""]
        let leaveSendingUrl =  webServiceUrls.leaveStatus
        if description == true{
         parameters = [webServiceParameterConstants.dateList:dateList,
                          webServiceParameterConstants.addedById:addedById,
                          webServiceParameterConstants.reason:reason,
                          webServiceParameterConstants.studentIdList:studentIdList,
                          webServiceParameterConstants.description:descriptionText
        ]
        }
        else{
            parameters = [webServiceParameterConstants.dateList:dateList,
                          webServiceParameterConstants.addedById:addedById,
                          webServiceParameterConstants.reason:reason,
                          webServiceParameterConstants.studentIdList:studentIdList
            ]
            
        }
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:leaveSendingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postNoticeWithTitle(title:String, contentOfNotice noticecontent:String, dateofnotice date:String, typeofnotice noticeType:String, idOfNoticeAddingPerson userRoleId:String,attachmentIdList attachments:String,corporateId:String,recipientSchoolIdList:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        
        let messageSendingUrl =  webServiceUrls.notificationsUrl
        
        if attachments.isEmpty{
            if corporateId == "0" {
                
            parameters = [webServiceParameterConstants.composeNoticeTitle:title,
                          webServiceParameterConstants.composeNoticeContent:noticecontent,
                          webServiceParameterConstants.composeNoticeEventDate:date,
                          webServiceParameterConstants.composeNoticeType:noticeType,
                          webServiceParameterConstants.composeNoticeAddedById:userRoleId]
            }
            else{
            parameters = [webServiceParameterConstants.composeNoticeTitle:title,
                          webServiceParameterConstants.composeNoticeContent:noticecontent,
                          webServiceParameterConstants.composeNoticeEventDate:date,
                          webServiceParameterConstants.composeNoticeType:noticeType,
                          webServiceParameterConstants.composeNoticeAddedById:userRoleId,webServiceParameterConstants.composeNoticerecipientSchoolIdList:recipientSchoolIdList,
                          webServiceParameterConstants.composeNoticeCorporateId:corporateId]
                
            }
        }
        else{
            if corporateId == "0" {
                
            parameters = [webServiceParameterConstants.composeNoticeTitle:title,
                          webServiceParameterConstants.composeNoticeContent:noticecontent,
                          webServiceParameterConstants.composeNoticeEventDate:date,
                          webServiceParameterConstants.composeNoticeType:noticeType,
                          webServiceParameterConstants.composeNoticeAddedById:userRoleId,
                          webServiceParameterConstants.composeNoticeAttachmentList:attachments]
            }
            else{
             parameters = [webServiceParameterConstants.composeNoticeTitle:title,
                              webServiceParameterConstants.composeNoticeContent:noticecontent,
                              webServiceParameterConstants.composeNoticeEventDate:date,
                              webServiceParameterConstants.composeNoticeType:noticeType,
                              webServiceParameterConstants.composeNoticeAddedById:userRoleId,
                              webServiceParameterConstants.composeNoticeAttachmentList:attachments,
                              webServiceParameterConstants.composeNoticerecipientSchoolIdList:recipientSchoolIdList,
                              webServiceParameterConstants.composeNoticeCorporateId:corporateId]
                
            }
        }
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:messageSendingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postDiaryWithTitle(classList:String, diarystudentList studentList:String, dateofdiary date:String, diarysubject subject:String, diarycontent content:String,idOfDiaryAddingPerson userRoleId:String,attachmentIdList attachments:String,corporateId:String,recipientSchoolIdList:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        
        let messageSendingUrl =  webServiceUrls.employeeDiaryUrl
        
        if attachments.isEmpty{
            if corporateId == "0" {
                
            parameters = [webServiceParameterConstants.composeDiaryRecipientClassList:classList,
                          webServiceParameterConstants.composeDiaryRecipientStudentList:studentList,
                          webServiceParameterConstants.composeDiarysubmissionDate:date,
                          webServiceParameterConstants.composeDiarySubject:subject,
                          webServiceParameterConstants.composeDiarycontent:content,
                          webServiceParameterConstants.composeDiaryAddedbyid:userRoleId,webServiceParameterConstants.composeDiarySChoolId:recipientSchoolIdList]
            }
            else{
             parameters = [webServiceParameterConstants.composeDiaryRecipientClassList:classList,
                              webServiceParameterConstants.composeDiaryRecipientStudentList:studentList,
                              webServiceParameterConstants.composeDiarysubmissionDate:date,
                              webServiceParameterConstants.composeDiarySubject:subject,
                              webServiceParameterConstants.composeDiarycontent:content,
                              webServiceParameterConstants.composeDiaryAddedbyid:userRoleId,webServiceParameterConstants.composeDiaryrecipientSchoolIdList:recipientSchoolIdList,
                              webServiceParameterConstants.composeDiaryCorporateId:corporateId]
            }
        }
        else{
            if corporateId == "0" {
              parameters = [webServiceParameterConstants.composeDiaryRecipientClassList:classList,
                          webServiceParameterConstants.composeDiaryRecipientStudentList:studentList,
                          webServiceParameterConstants.composeDiarysubmissionDate:date,
                          webServiceParameterConstants.composeDiarySubject:subject,
                          webServiceParameterConstants.composeDiarycontent:content,
                          webServiceParameterConstants.composeDiaryAttachmentidList:attachments,
                          webServiceParameterConstants.composeDiaryAddedbyid:userRoleId,webServiceParameterConstants.composeDiarySChoolId:recipientSchoolIdList]
            }
            else{
             parameters = [webServiceParameterConstants.composeDiaryRecipientClassList:classList,
                           webServiceParameterConstants.composeDiaryRecipientStudentList:studentList,
                           webServiceParameterConstants.composeDiarysubmissionDate:date,
                           webServiceParameterConstants.composeDiarySubject:subject,
                           webServiceParameterConstants.composeDiarycontent:content,
                           webServiceParameterConstants.composeDiaryAttachmentidList:attachments,
                           webServiceParameterConstants.composeDiaryAddedbyid:userRoleId,
                           webServiceParameterConstants.composeDiaryrecipientSchoolIdList:recipientSchoolIdList,
                           webServiceParameterConstants.composeDiaryCorporateId:corporateId]
                
            }
        }
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:messageSendingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postHomeworkWithTitle(homeworkTitle title:String,classList:String, homeworkstudentList studentList:String,idOfDiaryAddingPerson userRoleId:String,homeworkReference reference:String,homeworkSubjectid subjectId:Int,homeworkSubmissionDate subDate:String,  homeworkDescription description:String,attachmentIdList attachments:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        
        let messageSendingUrl =  webServiceUrls.gethomeWorkList
        
        if attachments.isEmpty{
            parameters = [webServiceParameterConstants.composeHomeworkTitle:title,
                          webServiceParameterConstants.composeHomeworkrecipientClasslist:classList,
                          webServiceParameterConstants.composeHomeworkrecipientStudentlist:studentList,
                          webServiceParameterConstants.composeHomeworkAddedById:userRoleId,
                          webServiceParameterConstants.composeHomeworkReference:reference,
                          webServiceParameterConstants.composeHomeworkSubjectId:String(subjectId),
                          webServiceParameterConstants.composeHomeworkDescription:description,
                          webServiceParameterConstants.composeHomeworkSubmissionDate:subDate]
        }
        else{
            parameters = [webServiceParameterConstants.composeHomeworkTitle:title,
                          webServiceParameterConstants.composeHomeworkrecipientClasslist:classList,
                          webServiceParameterConstants.composeHomeworkrecipientStudentlist:studentList,
                          webServiceParameterConstants.composeHomeworkAddedById:userRoleId,
                          webServiceParameterConstants.composeHomeworkReference:reference,
                          webServiceParameterConstants.composeHomeworkSubjectId:String(subjectId),
                          webServiceParameterConstants.composeHomeworkDescription:description,
                          webServiceParameterConstants.composeHomeworkSubmissionDate:subDate,
                          webServiceParameterConstants.composeHomeworkAttachmentIdList:attachments]
        }
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:messageSendingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    
    
    public class func changePasswordForUserName(userName:String,havingOldPassword oldPassword:String, toNewPassword newPassword:String, confirmedNewPassword newPwdConfirmed:String,isPassword ispassword:Bool,isForgotPassword:Bool,otp:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]

        let changePwdUrl = webServiceUrls.changePassword
        if(ispassword){
            parameters =  [webServiceParameterConstants.changePasswordOldPassword:userName,
                           webServiceParameterConstants.changePasswordNewPassword:newPassword,
                           webServiceParameterConstants.changePasswordUsername:userName
            ]
         }
        else if(isForgotPassword){
            parameters =  [webServiceParameterConstants.changePasswordWithOtp:otp,
                           webServiceParameterConstants.changePasswordNewPassword:newPassword,
                           webServiceParameterConstants.changePasswordUsername:userName
            ]
        }
            
            
        else{
            parameters = [webServiceParameterConstants.changePasswordOldPassword:oldPassword,
                          webServiceParameterConstants.changePasswordNewPassword:newPassword,
                          webServiceParameterConstants.changePasswordConfirmNewPassword:newPwdConfirmed,
                          webServiceParameterConstants.changePasswordUsername:userName
            ]
            
            
        }
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: changePwdUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func changePasswordWhenSkipped(mobNo:String,userName:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let changePwdUrl = webServiceUrls.changePassword
        let parameters =  [webServiceParameterConstants.changePasswordUsername:mobNo,         webServiceParameterConstants.changePasswordOldPassword:mobNo,
            webServiceParameterConstants.changePasswordNewPassword:mobNo]
            
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: changePwdUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
     
    public class func postAbsentees(date:String,userRoleId:String,studentIdList:String,classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let changePwdUrl = webServiceUrls.leaveStatus
        let parameters =  [webServiceParameterConstants.StudentLeaveRecordDateList:date,         webServiceParameterConstants.StudentLeaveRecordAddedById:userRoleId,
            webServiceParameterConstants.StudentLeaveRecordStudentIdList:studentIdList,webServiceParameterConstants.StudentLeaveRecordSchoolClassId:classId]
        
        
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: changePwdUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    public class func getNoticesForUser(employeeORstudentId:String,SelectedUser:String,forPage page:String, shouldBeViewedByPhoto isToBeviewedByPhoto:Bool,corporateId:String,schoolId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let notificationUrl = webServiceUrls.notificationsUrl
        let groupedByDate = "1"
        var parameters:[String:String] = ["":""]

        var viewByPhoto = "0"
        if isToBeviewedByPhoto == true{
            viewByPhoto = "1"
        }
        if SelectedUser == "Guardian"{
         parameters = [webServiceParameterConstants.studentID:employeeORstudentId,
                          webServiceParameterConstants.page:page,
                          webServiceParameterConstants.viewByPhotoKey:viewByPhoto,
                          webServiceParameterConstants.dateGrouped:groupedByDate
                          ]
        }else if SelectedUser == "Employee"{
        parameters = [webServiceParameterConstants.userRoleId:employeeORstudentId,
                          webServiceParameterConstants.page:page,
                          webServiceParameterConstants.viewByPhotoKey:viewByPhoto,
                          webServiceParameterConstants.dateGrouped:groupedByDate
            ]
            
        }
        else{
            parameters = [webServiceParameterConstants.userRoleId:employeeORstudentId,
                          webServiceParameterConstants.page:page,
                          webServiceParameterConstants.viewByPhotoKey:viewByPhoto,
                          webServiceParameterConstants.dateGrouped:groupedByDate,
                          webServiceParameterConstants.corporateId:corporateId,
                          webServiceParameterConstants.schoolId:schoolId
            ]
            
        }
        
        
        let notiString = notificationUrl + makeStringForUrlFromDictionary(dicto: parameters)
        let notiUrl = NSURL(string: notiString)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: notiUrl as URL, onSuccess: success , onFailure: failure, duringProgress: progress)
    }
    
    
    
    public class func getMoreAttachmentForNotificationWithID(notificationId:String , onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
    
        let url = webServiceUrls.notificationImgAttachmentsUrl
        
        let parameters = [webServiceParameterConstants.page:"4",
                          webServiceParameterConstants.noticeId:notificationId
                          
                          ]
        let finalString = url + makeStringForUrlFromDictionary(dicto: parameters)
        let finalUrl = NSURL(string:finalString)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: finalUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    
    public class func makeDiaryEntryRead(diaryId:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let urlString = webServiceUrls.studentDiaryUrl  + diaryId
        let parameters = [webServiceParameterConstants.studentDiaryReadyUpdate:webServiceParameterConstants.diaryIsread]
        let finalUrl = NSURL(string:urlString)!
        
        WebServiceModel.makePutApiCallWithParameters( parameters, withUrl: finalUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func downLoadFileFromUrl(urlString:String,toBeSavedTopath filePath:NSURL , onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
    
        WebServiceModel.downLoadFileFromUrl(urlString, toBeSavedTopath: filePath as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getUnreadCountForStudentId(studentId:String, forUserRoleId roleId:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let urlString = webServiceUrls.ureadCountUrl
        
        let parameters = [webServiceParameterConstants.studentID: studentId,
                          webServiceParameterConstants.userRoleId : roleId
                          ]
        let finalString = urlString + makeStringForUrlFromDictionary(dicto: parameters)
        let finaUrl =  NSURL(string: finalString)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: finaUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getUnreadCountForEmployeeId(forUserRoleId roleId:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        let urlString = webServiceUrls.ureadCountUrl
        
        let parameters = [webServiceParameterConstants.userRoleId : roleId]
        let finalString = urlString + makeStringForUrlFromDictionary(dicto: parameters)
        let finaUrl =  NSURL(string: finalString)!
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: finaUrl as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    
    private class func makeStringForUrlFromDictionary(dicto:[String:String]) ->String{
        var urlString = ""
        for (key,value) in dicto {
            urlString = urlString+"/\(key)/\(value)"
        }
        return urlString
    }
    
    
    public class func getUserOTP(mobNo:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let getOTPUrl = webServiceUrls.oneTimePassword+mobNo
        
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getOTPUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func postUserOTP(mobNo:String,OTP:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let postOTPUrl = webServiceUrls.loginUrl
        let parameters =  [webServiceParameterConstants.userName:mobNo,
                           webServiceParameterConstants.useroneTimePassword:OTP]
        
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: postOTPUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    public class func getMultiRoleSwitchForEmployee(userId:String,switchAs:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        
        let parameterDictionary = [webServiceParameterConstants.userId : userId,
                                   webServiceParameterConstants.role : switchAs]
        let switchRoleUrl = webServiceUrls.switchRole+webServiceParameterConstants.userId+"/"+userId+"/"+webServiceParameterConstants.role+"/"+switchAs
        
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: NSURL(string: switchRoleUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    public class func getEligibleStudentListToComposeDiary(schoolClassIdList:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        
        let parameterDictionary = [webServiceParameterConstants.schoolClassIdList : schoolClassIdList]
        let getStudentListUrl = webServiceUrls.studentListUrl+webServiceParameterConstants.schoolClassIdList+"/"+schoolClassIdList
        
        WebServiceModel.makeGetApiCallWithParameters( parameterDictionary, withUrl: NSURL(string: getStudentListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAllStudentListToChooseAbsentees(schoolClassId:String,date:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let getStudentListUrl = webServiceUrls.studentListUrl+webServiceParameterConstants.schoolClassId+"/"+schoolClassId + "/" + webServiceParameterConstants.absentDate+"/"+date+webServiceParameterConstants.choosingAbsentees
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getStudentListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAllStudentListForPromotion(schoolClassId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let getStudentListUrl = webServiceUrls.studentPromotionListUrl+webServiceParameterConstants.schoolClassId+"/"+schoolClassId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getStudentListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    
    
    
    public class func getProgressReport(studentId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let progressUrl = webServiceUrls.progressCard
        let parameters = [webServiceParameterConstants.studentID :studentId,
                          webServiceParameterConstants.forIos : "true"
                          ]
        
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: progressUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getNutShellReport(studentId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let progressUrl = webServiceUrls.nutShellReport
        let parameters = [webServiceParameterConstants.studentID :studentId]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: progressUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    public class func getProgressGraph(studentId:String,graphID:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let progressUrl = webServiceUrls.graphRsult
        let parameters = [webServiceParameterConstants.studentID :studentId,
//                          webServiceParameterConstants.graphID : graphID,
                          webServiceParameterConstants.withNickName : graphID]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: progressUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    public class func getLeaveStatus(studentId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let progressUrl = webServiceUrls.leaveStatus
        let parameters = [webServiceParameterConstants.studentID :studentId]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: progressUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    public class func getEmployeeList(schoolId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let progressUrl = webServiceUrls.getEmployeeList
        let parameters = [webServiceParameterConstants.schoolId :schoolId,
                          webServiceParameterConstants.forTimeTable : String(1)]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: progressUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    public class func getTimeTableByEmployeeId(employeeId:String,fromTeacher:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var parameters:[String:String] = ["":""]
        
        let timeTableUrl = webServiceUrls.gettimeTableList
        if fromTeacher == false{
        parameters = [webServiceParameterConstants.employeeId :employeeId]
        }
        else{
        parameters = [webServiceParameterConstants.userRoleId :employeeId]
        }
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: timeTableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getTimeTableByClassId(classId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var timeTableUrl:String = ""
        
        timeTableUrl = webServiceUrls.baseURl+webServiceParameterConstants.schoolClass+"/"+String(classId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: timeTableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getTeachersListForSameClassTimetableSubstitution(classId:Int,timeTableId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let timeTableUrl = webServiceUrls.getClassSubTeachertimeTableList
        let parameters = [webServiceParameterConstants.schoolClassId : String(classId),
                          webServiceParameterConstants.timeTableId :String(timeTableId)]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: timeTableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getTeachersListForOtherClassTimetableSubstitution(timeTableId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        let timeTableUrl = webServiceUrls.getClassSubTeachertimeTableList
        let parameters = [webServiceParameterConstants.timeTableId :String(timeTableId)]
        WebServiceModel.makeGetApiCallWithParameters( parameters, withUrl: NSURL(string: timeTableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postSubstitution(userRoleId:String, employeeId:Int,  timeTableId:Int,substituted:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        var postSubstitutionTimetableUrl = ""
        
         if substituted == false{
            
            postSubstitutionTimetableUrl =  webServiceUrls.postSubstitutionTimetableUrl
            
            parameters = [webServiceParameterConstants.employeeForceSubstitution:String(1),
                          webServiceParameterConstants.employeesubstitutedById:userRoleId,
                          webServiceParameterConstants.subemployeeId:String(employeeId),
                          webServiceParameterConstants.subtimetableId:String(timeTableId)
                         
        ]
        }
        else{
            postSubstitutionTimetableUrl =  webServiceUrls.cancelSubstitutionTimetableurl
            
            parameters = [webServiceParameterConstants.employeesubstitutedById:userRoleId,
            webServiceParameterConstants.subtimetableId:String(timeTableId)]
            
        }
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:postSubstitutionTimetableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getTeachersActivityList(timeTableId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var timeTableUrl:String = ""
        
        timeTableUrl = webServiceUrls.baseURl+webServiceParameterConstants.timeTableDetails+"/"+String(timeTableId)
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: timeTableUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getContactListOfTeachers(schoolId:String,userRoleId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var contactListUrl:String = ""
        
        contactListUrl = webServiceUrls.getEmployeeList+webServiceParameterConstants.contactList+webServiceParameterConstants.schoolId+"/"+schoolId+"/"+webServiceParameterConstants.userRoleId+"/"+userRoleId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: contactListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getClassListOfCorporateManager(schoolId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var classListUrl:String = ""
        
        classListUrl = webServiceUrls.classListofCorporateManger+webServiceParameterConstants.schoolId+"/"+schoolId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getClassListForCompose(schoolId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var classListUrl:String = ""
        
        classListUrl = webServiceUrls.classListofCorporateManger+webServiceParameterConstants.schoolIdList+"/"+schoolId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getExamListForMarkEntry(classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        
        let examListListUrl = webServiceUrls.examListOfMarkEntry+webServiceParameterConstants.schoolClassId+"/"+classId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: examListListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getSubjectListForMarkEntry(examId:String,userRoleId:String,subjectId:Int,examSubjectId:Int,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
         var subjectListUrl = ""
         subjectListUrl = webServiceUrls.studentExamListOfMarkEntry+webServiceParameterConstants.examId+"/"+examId+"/"+webServiceParameterConstants.userRoleId+"/"+userRoleId
        if examSubjectId > 0 {
            subjectListUrl.append("/selectedExamSubjectId/"+String(examSubjectId))
        }
        else if subjectId > 0 {
           subjectListUrl.append("/selectedSubjectId/"+String(subjectId))
        }
        else{
            
        }
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: subjectListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    

    public class func getStudentSuggessionForSearch(schoolId:Int,nameLike:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getStudentListUrlUrl = webServiceUrls.studentBaseUrl+"/"+webServiceParameterConstants.schoolId+"/"+String(schoolId)+"/"+webServiceParameterConstants.nameLikeKey+"/"
        
        let escapeAddress = nameLike.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let encodedUrl = getStudentListUrlUrl + escapeAddress!
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: encodedUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    class func postStudentPromotionStatus(studentListArray:[composeDiaryStudentPopupList],userRoleId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        var tempParameters :[String:String] = ["":""]
        var postPromotionStatusUrl = ""
        
        
        postPromotionStatusUrl =  webServiceUrls.postPromotionStatus
        
        
        tempParameters = [webServiceParameterConstants.studentTransferType:userRoleId]
        
        for k in 0..<studentListArray.count {
         if studentListArray[k].isEdit == true {
           parameters = ["StudentPromotion[\(k)][studentId]":String(studentListArray[k].studentId),"StudentPromotion[\(k)][promotionType]":studentListArray[k].result]
           tempParameters.addEntriesFromDictionary(otherDictionary: parameters)
         }
            
        }
       
            
        WebServiceModel.makePostApiCallWithParameters( parameter: tempParameters, withUrl: NSURL(string:postPromotionStatusUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getClassListForStudentAdmission(appName:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getStudentListUrlUrl = webServiceUrls.studentAdmission+appName+"/1"
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getStudentListUrlUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
  
    
    
    public class func submitStudentAdmission(studentName:String, className:String, studentDob:String, fatherName:String, motherName:String,mobileNumber:String,emailId:String,addressLineone:String,addressLineTwo:String,livingCity:String,livingPincode:String,studentResidentVal:String,appName:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var parameters:[String:String] = ["":""]
        
        let studentAdmissionUrl =  webServiceUrls.submitStudentAdmission
        
        let address = addressLineone+","+addressLineTwo+","+livingCity+","+livingPincode
        
        var schoolCode = ""
        
        #if EXCELDEVELOPMENT
          schoolCode = "EXCMAH3917928659"
        #elseif SREEMAHARSHIDEVELOPMENT
           schoolCode = "SREPAL2888727589"
        #endif
        
        parameters = [webServiceParameterConstants.studentAdmissionStudentName:studentName,
                      webServiceParameterConstants.studentAdmissionclassName:className,
                      webServiceParameterConstants.studentAdmissionstrDob:studentDob,
                      webServiceParameterConstants.studentAdmissionfatherName:fatherName,
                      webServiceParameterConstants.studentAdmissionmotherName:motherName,
                      webServiceParameterConstants.studentAdmissionmobileNo:mobileNumber,
                      webServiceParameterConstants.studentAdmissionemailId:emailId,
                      webServiceParameterConstants.studentAdmissionaddress:address,
                      webServiceParameterConstants.studentAdmissionschoolCode:schoolCode]

        
        #if EXCELDEVELOPMENT
            parameters.addEntriesFromDictionary(otherDictionary: [webServiceParameterConstants.studentAdmissionnativeResident:studentResidentVal])
            
        #endif
                   
 
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string:studentAdmissionUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postPaymentOrder(schoolCode:String,FeeType:String,studentRefId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        
        let paymentOrderUrl = webServiceUrls.utilityFee
        let parameters =  [webServiceParameterConstants.admissionFeeSchoolCode:schoolCode,
                           webServiceParameterConstants.admissionFeeFeeType:FeeType,
                           webServiceParameterConstants.admissionFeeStudentRefId:studentRefId]
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: paymentOrderUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getAtomDownloadReciptOfStudentAdmission(paymentId:String, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let getFeeInvoiceUrl = webServiceUrls.utilityFeeInvoiceDownload+"paymentId/"+paymentId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: getFeeInvoiceUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func postAdmissionPaymentStatus(response:String,id:String,paymentType:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        var status:String = ""
        var paymentOrderUrl:String = ""
        var parameters = ["":""]
        
        if response == ConstantsInUI.atomSuccessResponse || response == ConstantsInUI.atomSuccessResponse{
            status = "Success"
        }
        else if response == ConstantsInUI.atomFailedResponse || response == ConstantsInUI.atomFailedResponse {
            status = "Failed"
        }
        else{
            status = "Failed Unknown"
        }
        
        
        if paymentType == "FeePaymentDetails" {
           paymentOrderUrl = webServiceUrls.feePaymentDetails
        }
        else{
           paymentOrderUrl = webServiceUrls.utilityFee
        }
        
        if paymentType == "FeePaymentDetails" {
         parameters =  [webServiceParameterConstants.feePaymentDetailsStatus:status,
                        webServiceParameterConstants.feePaymentDetailspaymentId:id]
        }
        else{
         parameters =  [webServiceParameterConstants.utilityFeePaymentStatus:status,
                           webServiceParameterConstants.utilityFeePaymentId:id]
        }
        
         print("response=== "+response+"\n"+"id==="+id+"payment type==="+paymentType)
         print(parameters)
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: paymentOrderUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    internal class func getinvoiceList(Students:[StudentModel]?,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var invoiceListUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        
        for k in 0 ..< Students!.count {
            tempParameter = ["studentId[\(k)]":String(Students![k].id)]
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        
        invoiceListUrl = webServiceUrls.getInvoiceList
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: invoiceListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    internal class func checkInvoiceList(studentIdListArray:[String],dueDateListArray:[String],feeNameListArray:[String],onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var invoiceListUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        
        for k in 0 ..< studentIdListArray.count {
            tempParameter = ["studentId[\(k)]":studentIdListArray[k]]
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        tempParameter.removeAll()
        for j in 0 ..< dueDateListArray.count {
            tempParameter = ["dueDateList[\(j)]":dueDateListArray[j]]
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        for i in 0 ..< feeNameListArray.count {
            tempParameter = ["feeNameList[\(i)]":feeNameListArray[i]]
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        
        invoiceListUrl = webServiceUrls.checkInvoice
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: invoiceListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    internal class func postFeePaymentDetails(userRoleId:String,invoiceDetailsVCArray:[invoiceList],totalAmountValue:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var invoiceListUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        
        for k in 0 ..< invoiceDetailsVCArray.count {
            tempParameter = ["FeePaymentDetails[\(k)][addedById]":userRoleId,
                             "FeePaymentDetails[\(k)][amount]":invoiceDetailsVCArray[k].amount,
                             "FeePaymentDetails[\(k)][invoiceId]":invoiceDetailsVCArray[k].id,
                             "FeePaymentDetails[\(k)][feeName]":invoiceDetailsVCArray[k].name,
                             "FeePaymentDetails[\(k)][studentId]":String(invoiceDetailsVCArray[k].studentId)]
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        parameters.addEntriesFromDictionary(otherDictionary: ["FeePaymentDetails[totalAmount]":totalAmountValue])
        
        invoiceListUrl = webServiceUrls.feePaymentDetails
        
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: invoiceListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    internal class func downloadReciept(invoiceDetailsVCArray:[invoiceList],user:userModel?,paymentFeeDetailsArray:[feeDetails],sucessResponse:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var invoiceListUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        var feeParameter = ["":""]
        
        if sucessResponse == "1" {
            if paymentFeeDetailsArray.count > 0 {
                tempParameter = ["FeeInvoice[paymentId]":paymentFeeDetailsArray[0].paymentId]
            }
        }
        else{
            if invoiceDetailsVCArray.count > 0 {
               tempParameter = ["FeeInvoice[paymentId]":invoiceDetailsVCArray[0].paymentId]
            }
        }
          parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        for k in 0 ..< invoiceDetailsVCArray.count {
            tempParameter = ["FeeInvoice[student][\(k)][termName]":invoiceDetailsVCArray[0].name,
                "FeeInvoice[student][\(k)][studentName]":invoiceDetailsVCArray[0].studentName,
                "FeeInvoice[student][\(k)][studentId]":String(invoiceDetailsVCArray[0].studentId)]
             for j in 0 ..< invoiceDetailsVCArray[k].feeListArray.count {
                
                feeParameter = ["FeeInvoice[student][\(k)][feeList][\(j)][feeName]":invoiceDetailsVCArray[k].feeListArray[j].name ,
                    "FeeInvoice[student][\(k)][feeList][\(j)][feeAmount]":invoiceDetailsVCArray[k].feeListArray[j].amount]
                tempParameter.addEntriesFromDictionary(otherDictionary: feeParameter)
                }
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
            
        }
        
    
      
        invoiceListUrl = webServiceUrls.downloadInvoice
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: invoiceListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    internal class func getAudioPlayUrl(name:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let AudioPlayUrl = webServiceUrls.convertAudioPlay+"uniqueName/"+name

        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: AudioPlayUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)

    }

    internal class func getClassTestList(studentOrClassId:String,user:userModel,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var classTestUrl:String = ""
        
        if user.currentTypeOfUser == .guardian {
         classTestUrl = webServiceUrls.classTestListing+"studentId/"+studentOrClassId
        }
        else{
          classTestUrl = webServiceUrls.classTestListing+"schoolClassId/"+studentOrClassId
        }
        print("URL:="+classTestUrl)
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classTestUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    internal class func checkForSuccessPayment(referenceId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let checkForSuccessPaymentUrl = webServiceUrls.checkForSuccessPayment+"paymentId/"+referenceId
        
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: checkForSuccessPaymentUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getNutshellResult(studentId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let nutshellResultUrl = webServiceUrls.nutshellRsult+"studentId/"+studentId+"/forIos/1"
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: nutshellResultUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getEvaluationActivityListForMarkEntry(classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let evaluationActivityUrl = webServiceUrls.evaluationActivity+"schoolClassId/"+classId+"/forMarkEntry/1"
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: evaluationActivityUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getclassSubjectForMarkEntry(classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let classSubjectUrl = webServiceUrls.classSubject+"schoolClassId/"+classId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classSubjectUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func getclassTesttListingForMarkEntry(classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let classTestListingUrl = webServiceUrls.classTestListing+"schoolClassId/"+classId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classTestListingUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    public class func postNewEvaluationSubjectForMarkEntry(classId:String,userRoleId:String,maxmark:String,classTestName:String,evaluationActivityId:String,classSubjectId:String,isClassTestCreation:Bool,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var evaluationActivitySubjectListUrl:String = ""
        var parameters = ["":""]
                
        parameters = ["EvaluationActivitySubject[maxMark]":maxmark,
                          "EvaluationActivitySubject[schoolClassId]":classId,
                          "EvaluationActivitySubject[addedById]":userRoleId,
                          "EvaluationActivitySubject[forMarkEntry]":"1"]
        
        if isClassTestCreation {
            print("inside class test creation parametr...")
           parameters.addEntriesFromDictionary(otherDictionary: ["EvaluationActivitySubject[name]":classTestName,
                                                "EvaluationActivitySubject[forClassTest]":"1"])
        }
        else{
           parameters.addEntriesFromDictionary(otherDictionary: ["EvaluationActivitySubject[evaluationActivityId]":evaluationActivityId,
                "EvaluationActivitySubject[subjectId]":classSubjectId])
        }
        
        
        evaluationActivitySubjectListUrl = webServiceUrls.evaluationActivitySubject
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: evaluationActivitySubjectListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func postUpdateEvaluationSubjectForMarkEntry(maxmark:String,classSubjectId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var evaluationUpdateActivitySubjectListUrl:String = ""
        var parameters = ["":""]
        
        parameters = ["EvaluationActivitySubject[maxMark]":maxmark,
                      "EvaluationActivitySubject[evaluationActivitySubjectIdList]":classSubjectId]
        
       
        evaluationUpdateActivitySubjectListUrl = webServiceUrls.evaluationActivitySubject
        
        WebServiceModel.makePutApiCallWithParameters( parameters, withUrl: NSURL(string: evaluationUpdateActivitySubjectListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getstudentEvaluationSubjectListForMarkEntry(classId:String,activityId:String,subjectId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let studentEvaluationSubjectListUrl = webServiceUrls.studentEvaluationSubjectList+"schoolClassId/"+classId+"/evaluationActivityId/"+activityId+"/subjectId/"+subjectId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: studentEvaluationSubjectListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
   
   
    public class func getStudentWithMarkforClassTestForMarkEntry(classTestId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let classTestMarkListUrl = webServiceUrls.classTestMarkList+"id/"+classTestId
        
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classTestMarkListUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    internal class func saveMarkEntry(classId:String,evaluationActivitySubjectId:String,studentExamListArray:[studentExamList],onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var saveEvaluationStudentMarkistUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        
        for i in 0 ..< studentExamListArray.count {
            
             tempParameter = ["StudentAcademicEvaluationV1[\(i)][evaluationActivitySubjectId]":evaluationActivitySubjectId,
                      "StudentAcademicEvaluationV1[\(i)][studentId]":String(studentExamListArray[i].studentId),
              "StudentAcademicEvaluationV1[\(i)][mark]":studentExamListArray[i].mark,
                "StudentAcademicEvaluationV1[\(i)][schoolClassId]":classId]
              parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        }
        
        saveEvaluationStudentMarkistUrl = webServiceUrls.studentAcademicEvaluationV1
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: saveEvaluationStudentMarkistUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getSubjectListForClassTestCreation(userRoleId:String,classId:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let classTestSUbjectUrl = webServiceUrls.subjects+"userRoleId/"+userRoleId+"/schoolClassId/"+classId
        
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: classTestSUbjectUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
    internal class func postNewClassTest(subjectId:String,classTestName:String,currentUserRoleId:String,date:String,classId:String,Topics:String,maxMark:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var saveClassTestUrl:String = ""
        var parameters = ["":""]
        var tempParameter = ["":""]
        
        
        if subjectId == "" {
            tempParameter = ["EvaluationActivitySubject[name]":classTestName]
        }
        else{
           tempParameter = ["EvaluationActivitySubject[subjectId]":subjectId]
        }
          parameters = ["EvaluationActivitySubject[addedById]":currentUserRoleId,
            "EvaluationActivitySubject[date]":date,
            "EvaluationActivitySubject[schoolClassId]":classId,
            "EvaluationActivitySubject[forClassTest]":"1",
            "EvaluationActivitySubject[topic]":Topics,
            "EvaluationActivitySubject[maxMark]":maxMark]
        
            parameters.addEntriesFromDictionary(otherDictionary: tempParameter)
        
        
        saveClassTestUrl = webServiceUrls.evaluationActivitySubject
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: saveClassTestUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    internal class func upLoadDeviceDetailsForPushNotification(userId:String,deviceToken:String,deviceId:String,deviceType:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        var uploadDeviceDetailsUrl:String = ""
        var parameters = ["":""]
        
        parameters = ["DeviceDetails[userId]":userId,
                      "DeviceDetails[deviceToken]":deviceToken,
                      "DeviceDetails[deviceId]":deviceId,
                      "DeviceDetails[deviceType]":deviceType]
        
        uploadDeviceDetailsUrl = webServiceUrls.deviceDetails
        
        WebServiceModel.makePostApiCallWithParameters( parameter: parameters, withUrl: NSURL(string: uploadDeviceDetailsUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
    }
    
    public class func getNotificationDetails(noti_content_type:String,noti_content_reference_id:String,user_Id:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress: progressClosure?){
        
        let notificationDetailsUrl = webServiceUrls.notificationDetailsV2+"notificationContentDataId/"+noti_content_reference_id+"/notificationContentType/"+noti_content_type+"/userId/"+user_Id
        print("notification details url==="+notificationDetailsUrl)
        WebServiceModel.makeGetApiCallWithParameters( [String:String](), withUrl: NSURL(string: notificationDetailsUrl)! as URL, onSuccess: success, onFailure: failure, duringProgress: progress)
        
    }
    
}
