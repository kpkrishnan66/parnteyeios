//
//  WebServiceModel.swift
//  parentEye
//
//  Created by Martin Jacob on 12/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
import Haneke

public class WebServiceModel {
    
//    public class func logMeInServerWithUserName(userName:String, andPassword password:String, onSuccess success:successClosure, onFailure failure:failureClosure, duringProgress progress:progressClosure?){
//        
//        
//    }
    
    
    /**
     Generic function to make post api calls (project uses both get and post)
     
     - parameter parameter: parameters to be send
     - parameter url:       url for the service
     - parameter success:   succes closure to be executed
     - parameter failure:   failure closure to be executed
     - parameter progress:  progress closure to be executed
     */
    
    open class func makePostApiCallWithParameters(parameter:[String:String], withUrl url:URL, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        Alamofire.request(url,method: .post, parameters: parameter)
            .responseJSON { response in
                
                if let err = response.result.error{
                    failure(err as NSError)
                } else {
                    if let JSON = response.result.value as! Dictionary<String, AnyObject>! {
                        success(JSON)
                    }else {
                        failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"]))
                    }
                }
                
        }
    }
    
    
    
    /**
     Generic function to make post api calls (project uses both get and post)
     
     - parameter parameter: parameters to be send
     - parameter url:       url for the service
     - parameter success:   succes closure to be executed
     - parameter failure:   failure closure to be executed
     - parameter progress:  progress closure to be executed
     */
    
    open class func makeGetApiCallWithParameters(_ parameter:[String:String], withUrl url:URL, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        Alamofire.request(url, method:.get, parameters: parameter)
            .responseJSON { response in
                
                if let err = response.result.error{
                    failure(err as NSError)
                } else {
                    if let JSON = response.result.value as! Dictionary<String, AnyObject>! {
                        success(JSON)
                    }else {
                        failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"]))
                    }
                }
                
        }
    }

    
    
    
   
    /**
     Generic function to make post api calls (project uses both get and post)
     
     - parameter parameter: parameters to be send
     - parameter url:       url for the service
     - parameter success:   succes closure to be executed
     - parameter failure:   failure closure to be executed
     - parameter progress:  progress closure to be executed
     */
    open class func makePutApiCallWithParameters(_ parameter:[String:String], withUrl url:URL, onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        Alamofire.request(url, method:.put, parameters: parameter)
            .responseJSON { response in
                
                if let err = response.result.error{
                    failure(err as NSError)
                } else {
                    if let JSON = response.result.value as! Dictionary<String, AnyObject>! {
                        success(JSON)
                    }else {
                        failure(NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"]))
                    }
                }
                
        }
    }
    
    open class func uploadImage(_ imageData: Data,imageUploadUrl urlString:String, withParametes paramDict:[String:String], tagForImage imageTag:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?) -> Void {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in

            multipartFormData.append(imageData,
                                             withName: imageTag,
                                             fileName: "file.jpg",
                                             mimeType: "image/jpg")
            for (key,value) in paramDict{
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        },to:urlString,encodingCompletion: { (encodingResult) in
                            switch encodingResult{
                                
                            case .success(let upload,_,_): upload.responseJSON{ response in
                                if let err = response.result.error {
                                    
                                    failure(err as NSError)
                                }
                                else {
                                    if let upRes = response.result.value as? [String:AnyObject]{
                                        success(upRes)
                                    }else {
                                        let err = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"])
                                        failure(err)
                                    }
                                    
                                }
                                }
                                
                            case .failure( _):
                                let err = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:"Image Parsing Failed"])
                                failure(err)
                            }
        })
    }
    
    open class func uploadDocument(_ DocumentData: Data,documentUploadUrl urlString:String, withParametes paramDict:[String:String], tagForDocument documentTag:String,docName:String,onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?) -> Void {
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(DocumentData,
                                             withName: documentTag,
                                             fileName: docName,
                                             mimeType: "application/pdf,application/doc,application/docx")
            for (key,value) in paramDict{
                 multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        },to:urlString,
                         encodingCompletion: { encodingResult in
                            switch encodingResult{
                                
                            case .success(let upload,_,_): upload.responseJSON{ response in
                                if let err = response.result.error {
                                    
                                    failure(err as NSError)
                                }
                                else {
                                    if let upRes = response.result.value as? [String:AnyObject]{
                                        success(upRes)
                                    }else {
                                        let err = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"])
                                        failure(err)
                                    }
                                    
                                }
                                }
                                
                            case .failure( _):
                                let err = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:"Image Parsing Failed"])
                                failure(err)
                            }
        })
    }
    
    open class func downLoadFileFromUrl(_ urlString:String,toBeSavedTopath filePath:URL , onSuccess success:@escaping successClosure, onFailure failure:@escaping failureClosure, duringProgress progress:progressClosure?){
        
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory, in: .userDomainMask)
       
        Alamofire.download(
            urlString,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).responseJSON { response in
                if response.result.error == nil {
                    failure(response.result.error! as NSError)
                } else {
                   success(["Result":"Success" as AnyObject])
                }
        }
    }
    
    
    public class func setImgWithUrlString(imageUrl:String, forImageView imageView:UIImageView){
        if let URL = NSURL(string: imageUrl){
            
            imageView.hnk_setImageFromURL(URL as URL)
        }
    }
    
    
    public class func setButtonImgWithUrlString(imageUrl:String, forButton button:UIButton){
        if let URL = NSURL(string: imageUrl){
            button.hnk_setBackgroundImageFromURL(URL as URL)
           
        }
    }
}
