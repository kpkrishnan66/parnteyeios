//
//  DBController.swift
//  parentEye
//
//  Created by Martin Jacob on 20/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class DBController {
    
    func addUserToDB(_ userDetails:userModel) -> Void {
        let userData = NSKeyedArchiver.archivedData(withRootObject: userDetails)
        
        UserDefaults.standard.set(userData, forKey: userDerfaultConstants.userData)
        UserDefaults.standard.synchronize()
        let date = Foundation.Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
        let dateString = dateFormatter.string(from: date)
        sync.lastSyncTime = dateString
    }
    
    func getUserFromDB() -> userModel? {
        if let userData = UserDefaults.standard.data(forKey: userDerfaultConstants.userData) {
            if let user = NSKeyedUnarchiver.unarchiveObject(with: userData) as? userModel {
                return user
            }
        }
        return nil
    }
    
    
    /**
     Function clear the db. Now things are cleared from userdefaults.
     */
    func clearDataBse() -> Void {
        
        // if user takes admission then does not need to remove all keys.
        if let admissionData = UserDefaults.standard.data(forKey: "newAdmissionArray") {
            
            if let feeResponse = UserDefaults.standard.string(forKey: "feeResponse") {
                if let paymentId = UserDefaults.standard.string(forKey: "paymentId")  {
                    
                    let appDomain = Bundle.main.bundleIdentifier!
                    UserDefaults.standard.removePersistentDomain(forName: appDomain)
                    UserDefaults.standard.set(feeResponse, forKey: "feeResponse")
                    UserDefaults.standard.set(paymentId, forKey:"paymentId")
                    UserDefaults.standard.set(admissionData, forKey: "newAdmissionArray")
                }
            }
            else{
                let appDomain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: appDomain)
                UserDefaults.standard.set(admissionData, forKey: "newAdmissionArray")
            }
        }
        else{
            let appDomain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
    }
    /**
     Function to change the preffered choice
     
     - parameter prefferdChoice: number of the preffered
     
     - returns: The result of the change (an option added looking forward to Coredata implemention)
     */
    func changePrefferedOptionForUserTo(_ prefferdChoice:Int) -> Bool {
        return false
    }
    
    fileprivate func getNumberOfUnread(_ type:String) ->Int{
        var number = 0
        if  let dn = UserDefaults.standard.object(forKey: type) as? String{
            if dn.isEmpty{
                number = 0
            }
            else
            {
                number = (Int(dn))!
            }
        }
        return number
    }
    
    func getDiaryUnreadNumber() -> Int {
        return getNumberOfUnread(userDerfaultConstants.DiaryUnreadNumber)
    }
    func getMessageUnreadNumber() -> Int {
        return getNumberOfUnread(userDerfaultConstants.messageUnreadNumber)
    }
    
    
    
    func updateDiaryUnread(_ number:String) -> Bool {
        UserDefaults.standard.set(number, forKey: userDerfaultConstants.DiaryUnreadNumber)
        UserDefaults.standard.synchronize()
        return true
    }
    func updateMessageUnread(_ number:String) -> Bool {
        UserDefaults.standard.set(number, forKey: userDerfaultConstants.messageUnreadNumber)
        UserDefaults.standard.synchronize()
        return true
    }
    
    
}

