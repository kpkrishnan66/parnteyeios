//
//  employeeSubjectList.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 24/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class employeeSubjectList {


var subjectId:Int   = -1
var subjectName = ""

    init() {
        
    }
    
   init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
    
    if let subID = dictionary[resultsJsonConstants.homeWorkConstants.subjectId] as? Int{
        self.subjectId = subID
    }else {
        self.subjectId = -1
    }
    
    
       if let subNme = dictionary[resultsJsonConstants.homeWorkConstants.subjectName] as? String{
            self.subjectName = subNme
        }else {
            self.subjectName = ""
        }
    }
}


class  parsesubjectsArray {
    class func parseThisDictionaryForAttachments(dictionary:[String:AnyObject])->([employeeSubjectList]){
        
        var subjectsArray = [employeeSubjectList]()
        
        if let subDicto = dictionary[resultsJsonConstants.homeWorkConstants.subjects] as? [AnyObject]{
            for dicto in subDicto{
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
            if let subObj = employeeSubjectList(withDictionaryFromWebService: record){
                subjectsArray.append(subObj)
               }
              }
            }
        }
        
        return(subjectsArray)
    }
    
}
