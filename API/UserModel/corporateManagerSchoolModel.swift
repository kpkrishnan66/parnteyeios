//
//  corporateManagerSchoolModel.swift
//  parentEye
//
//  Created by Vivek on 06/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class corporateManagerSchoolModel:NSObject,NSCoding {
    
    let schoolId : Int!
    let schoolName : String!
    let schoolNickName : String!
    var selectedInUI = false
    
    
    /**
     Creating a defensive and optional initializer here
     
     - parameter student: A student guardian object
     
     - returns: if required parameters are not met the initialization will fail
     */
    init?(fromSchoolList schoolList:corporateSchoolList){
        
        var result = true
        
        
        if let corporateSchoolId = schoolList.schoolId as Int?{
            schoolId  = corporateSchoolId
        }else {
            schoolId = -1
            result = false
        }
        
        
        if let corporateSchoolName = schoolList.schoolName as String?{
            schoolName  = corporateSchoolName
        }else {
            schoolName = ""
            result = false
        }
        
        if let corporateSchoolNickName = schoolList.schoolNickName as String?{
            schoolNickName  = corporateSchoolNickName
        }else {
            schoolNickName = ""
        }
        
        
        
        if result == false {
            return nil
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.schoolId = aDecoder.decodeObject(forKey: "schoolId") as? Int ?? aDecoder.decodeInteger(forKey: "schoolId")
        
        if let mschoolName = aDecoder.decodeObject(forKey: "schoolName") as? String {
            self.schoolName = mschoolName
        }else {
            self.schoolName = ""
        }
        
        if let mschoolNickName = aDecoder.decodeObject(forKey: "schoolNickName") as? String {
            self.schoolNickName = mschoolNickName
        }else {
            self.schoolNickName = ""
            
        }
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let clsid = self.schoolId as Int?  {
            aCoder.encode(clsid, forKey: "schoolId")
        }
        
        
        if let schNme = self.schoolName as String?  {
            aCoder.encode(schNme, forKey: "schoolName")
        }
        
        if let schName = self.schoolNickName as String?  {
            aCoder.encode(schName, forKey: "schoolNickName")
        }
    }
    
    
    
}
