//
//  StudentModel.swift
//  parentEye
//
//  Created by Martin Jacob on 20/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class StudentModel:NSObject,NSCoding {
        
    let admissionNo : String!
    let adress : String!
    let classId : Int!
    let className : String!
    let classTeacher : String!
    let delayReport : String?
    let dob : String!
    let id : String!
    let mobNo : String?
    let name : String!
    let parentName : String!
    let passportNo : String?
    var profilePic : String!
    let schoolId : Int!
    let schoolName : String!
    let transportType : String!
    let busID:Int!
    let busNo:String!
    let busContact :String!
    var busPlate:String!
    var leaveReasonList = [String]()
    var leaveDescription :String!
    var feePayment :String!
    var showOnlyProgressCard :Int!
    

    /**
     Creating a defensive and optional initializer here
     
     - parameter student: A student guardian object
     
     - returns: if required parameters are not met the initialization will fail
     */
    init?(fromStudentGuardian student:StudentGuardian){
    
        var result = true
        
        if let admnno = student.admissionNo {
            admissionNo  = admnno
        }else {
            admissionNo = ""
            result      = false
        }
        
        if let uadress = student.adress {
            adress  = uadress
        }else {
            adress = ""
            result = false
        }
        
        if let StdntClassId = student.classId as Int?{
            classId  = StdntClassId
        }else {
            classId = -1
            result = false
        }
        
        
        if let StdntClassName = student.className as String?{
            className  = StdntClassName
        }else {
            className = ""
            result = false
        }
        
        if let StdntClassTeacher = student.classTeacher as String?{
            classTeacher  = StdntClassTeacher
        }else {
            classTeacher = ""
            result = false
        }
        
        if let StdntDelayReport = student.delayReport as String?{
            delayReport  = StdntDelayReport
        }else {
            delayReport = ""
            result = false
        }
        
        if let sdob = student.dob as String?{
            dob  = sdob
        }else {
            dob = ""
            result = false
        }
        
        if let idStdnt = student.id {
            id  = String(idStdnt)
        }else {
            id = ""
            result      = false
        }
        
        
        if let smobNo = student.mobNo as String?{
            mobNo  = smobNo
        }else {
            mobNo = ""
            result = false
        }
        
        if let StdnName = student.name as String?{
            name  = StdnName
        }else {
            name = ""
        }
        
        if let sparentName = student.parentName as String?{
            parentName  = sparentName
        }else {
            parentName = ""
        }
        
        if let spassportNo = student.passportNo as String?{
            passportNo  = spassportNo
        }else {
            passportNo = ""
        }
        
        if let stdntPrfPic = student.profilePic as String?{
            profilePic = stdntPrfPic
        }else {
            profilePic = ""
        }
        
        if let stdntSchoolId = student.schoolId as Int?{
            schoolId = stdntSchoolId
        }else {
            schoolId = -1
            result   = false
        }
        
        if let mschoolName = student.schoolName as String?{
            schoolName = mschoolName
        }else {
            schoolName = ""
        }
        
        if let mtransportType = student.transportType as String?{
            transportType = mtransportType
        }else {
            transportType = ""
        }
        if let mbusID = student.busid as Int?{
            busID = mbusID
        }else{
            busID = -1
        }
        if let mbusNo = student.busNO as String?{
            busNo = mbusNo
        }else{
            busNo = ""
        }
        if let mbusContact = student.busContact as String?{
            busContact = mbusContact
        }else{
            busContact = ""
        }
        if let mbusPlate = student.busPlate as String?{
            busPlate = mbusPlate
        }else{
            busPlate = ""
        }
        
        if let mleave = student.leaveReasonList as [String]?{
            for dico in mleave{
                leaveReasonList.append(dico)
            }
           
        }
        
        if let mbusPlate = student.leaveDescription as String?{
            leaveDescription = mbusPlate
        }else{
            leaveDescription = ""
        }
        
        if let mfeePayment = student.feePayment as String?{
            feePayment = mfeePayment
        }else{
            feePayment = ""
        }
        if let mshowOnlyProgressCard = student.showOnlyProgressCard as Int?{
            showOnlyProgressCard = mshowOnlyProgressCard
        }else{
            showOnlyProgressCard = 0
        }
        
        
        if result == false {
            return nil
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
    
        if let madmnno = aDecoder.decodeObject(forKey: "AdmnNo") as? String {
            self.admissionNo = madmnno
        }else {
            self.admissionNo = ""
        }
        
        if let madress = aDecoder.decodeObject(forKey: "address") as? String {
            self.adress = madress
        }else {
            self.adress = ""
        }
        
        self.classId = aDecoder.decodeObject(forKey: "classId") as? Int ?? aDecoder.decodeInteger(forKey: "classId")
        
        if let mclassName = aDecoder.decodeObject(forKey: "className") as? String {
            self.className = mclassName
        }else {
            self.className = ""
        }
        
        if let mclassTeacher = aDecoder.decodeObject(forKey: "classTeacher") as? String {
            self.classTeacher = mclassTeacher
        }else {
            self.classTeacher = ""
        }
        
        if let mdelayReport = aDecoder.decodeObject(forKey: "delayReport") as? String {
            self.delayReport = mdelayReport
        }else {
            self.delayReport = ""
        }
       
        if let mdob = aDecoder.decodeObject(forKey: "dob") as? String {
            self.dob = mdob
        }else {
            self.dob = ""
        }
        
        if let idStdnt = aDecoder.decodeObject(forKey: "id") as? String {
            self.id = idStdnt
        }else {
            self.id = ""
        }
        
        if let mmobNo = aDecoder.decodeObject(forKey: "mobileNo") as? String {
            self.mobNo = mmobNo
        }else {
            self.mobNo = ""
        }
        
        if let stdntNme = aDecoder.decodeObject(forKey: "name") as? String {
            self.name = stdntNme
        }else {
            self.name = ""
        }
        
        if let mparentName = aDecoder.decodeObject(forKey: "parentName") as? String {
            self.parentName = mparentName
        }else {
            self.parentName = ""
        }
        
        if let mpassportNo = aDecoder.decodeObject(forKey: "passportNo") as? String  {
            self.passportNo = mpassportNo
        }else {
            self.passportNo = ""
        }
        
        if let prfPic = aDecoder.decodeObject(forKey: "profilePic") as? String {
            self.profilePic = prfPic
        }else {
           self.profilePic = ""
        }
        
         self.schoolId  = aDecoder.decodeObject(forKey: "schoolId") as? Int ?? aDecoder.decodeInteger(forKey: "schoolId")
        
        if let nameOfSchool = aDecoder.decodeObject(forKey: "schoolName") as? String{
            self.schoolName = nameOfSchool
        }else {
            self.schoolName = ""
        }
        
        if let trans = aDecoder.decodeObject(forKey: "transportType") as? String {
            self.transportType = trans
        }else {
            self.transportType = ""
        }
        
        self.busID  = aDecoder.decodeObject(forKey: "busID") as? Int ?? aDecoder.decodeInteger(forKey: "busID")
        
        if let mbusno = aDecoder.decodeObject(forKey: "busNo") as? String{
            self.busNo = mbusno
        }else{
            self.busNo = ""
        }
        if let mbuscon = aDecoder.decodeObject(forKey: "contactNo") as? String{
            self.busContact = mbuscon
        }else{
            self.busContact = ""
        }
        if let mbusplate = aDecoder.decodeObject(forKey: "regNo") as? String{
            self.busPlate = mbusplate
        }else{
            self.busPlate = ""
        }
        
        if let mleave = aDecoder.decodeObject(forKey: "leaveReasonList") as? [String]{
            for dico in mleave{
                leaveReasonList.append(dico)
            }
            
        }
        
        if let mbusplate = aDecoder.decodeObject(forKey: "leaveDescription") as? String{
            self.leaveDescription = mbusplate
        }else{
            self.leaveDescription = ""
        }
        
        if let mfeePayment = aDecoder.decodeObject(forKey: "feePayment") as? String{
            self.feePayment = mfeePayment
        }else{
            self.feePayment = ""
        }
        
        if aDecoder.containsValue(forKey: "showOnlyProgressCard"){
           self.showOnlyProgressCard  = aDecoder.decodeObject(forKey: "showOnlyProgressCard") as? Int ?? aDecoder.decodeInteger(forKey: "showOnlyProgressCard")
        }
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let admn = self.admissionNo as String? {
            aCoder.encode(admn, forKey: "AdmnNo")
        }
        
        if let adre = self.adress as String? {
            aCoder.encode(adre, forKey: "address")
        }
        
        if let clsid = self.classId as Int?  {
            aCoder.encode(clsid, forKey: "classId")
        }
        
        
        if let clsNme = self.className as String?  {
            aCoder.encode(clsNme, forKey: "className")
        }
        
        if let clsteach = self.classTeacher as String?  {
            aCoder.encode(clsteach, forKey: "classTeacher")
        }
        
        if let delay = self.delayReport as String?  {
            aCoder.encode(delay, forKey: "delayReport")
        }
        
        if let mdob = self.dob as String?  {
            aCoder.encode(mdob, forKey: "dob")
        }
        
        if let idStdnt = self.id as String? {
            aCoder.encode(idStdnt, forKey: "id")
        }
        
        if let mmobNo = self.mobNo as String? {
            aCoder.encode(mmobNo, forKey: "mobileNo")
        }
        
        if let stdntNme = self.name as String?{
            aCoder.encode(stdntNme, forKey: "name")
        }
        
        if let mparentName = self.parentName as String? {
            aCoder.encode(mparentName, forKey: "parentName")
        }
        
        if let mpassportNo = self.passportNo as String? {
            aCoder.encode(mpassportNo, forKey: "passportNo")
        }
        
        if let prfPic = self.profilePic as String? {
            aCoder.encode(prfPic, forKey: "profilePic")
        }
        
        if let idOfSchool = self.schoolId as Int? {
            aCoder.encode(idOfSchool, forKey: "schoolId")
        }
        if let mschoolName = self.schoolName as String? {
            aCoder.encode(mschoolName, forKey: "schoolName")
        }
        
        if let mtransportType = self.transportType as String? {
            aCoder.encode(mtransportType, forKey: "transportType")
        }
        if let mbusid = self.busID as Int?{
            aCoder.encode(mbusid, forKey: "busId")
        }
        if let mbusno = self.busNo as String?{
            aCoder.encode(mbusno, forKey: "busNo")
        }
        if let mbuscontact = self.busContact as String?{
            aCoder.encode(mbuscontact, forKey: "contactNo")
        }
        if let mbusplate = self.busPlate as String?{
            aCoder.encode(mbusplate, forKey: "regNo")
        }
        if let mbusplate = self.leaveReasonList as [String]?{
            aCoder.encode(mbusplate, forKey: "leaveReasonList")
        }
        if let mbusplate = self.leaveDescription as String?{
            aCoder.encode(mbusplate, forKey: "leaveDescription")
        }
        if let mfeePayment = self.feePayment as String?{
            aCoder.encode(mfeePayment, forKey: "feePayment")
        }
        if let mshowOnlyProgressCard = self.showOnlyProgressCard as Int?{
            aCoder.encode(mshowOnlyProgressCard, forKey: "showOnlyProgressCard")
        }
        
    }
    
    
}
