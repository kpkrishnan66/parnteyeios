//
//  UserModel.swift
//  parentEye
//
//  Created by Martin Jacob on 20/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

/// Convinient user model created for usage . We ger data from service from the root object created by jsonExport (in the folder User full data - parent login). This class be created when we ger data from the DB .. I short this class will be used by our view classes.

import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}



enum currentUserType:String {
    case guardian
    case employee
    case CorporateManager
}

class userModel:NSObject,NSCoding {
    
    var username:String                   = ""//username--- could be email or any thing
    var userId:String                     = ""// user id
    var userDesignation:String            = ""// Designation - could be aya or teacher or some thing like that
    var currentUserRoleId:String          = ""// the current user role's id
    var currentTypeOfUser:currentUserType = .employee//
    var profilePic:String                 = ""
    var Students:[StudentModel]?// given as optinal as we could change the user from parent to teacher and teacher will not have students... they will have schools
    var Classes:[EmployeeClassModel]?
    var currentOptionSelected             = 0// variable added to identify which student (parent)/ school(was selected)
    var currentSchoolSelected             = 0
    var selectedInUI                      = false
    var adress:String                     = ""
    var canSwitch:Int                     = 0
    var mobileNo:String                   = ""
    var passwordChanged:Int               = 0
    var switchAs:String                   = ""
    var schoolId : Int                    = 0
    var employeeType : String             = ""
    var smsExceeded : Int                 = 0
    var smsCharacterLimit : String        = ""
    var schoolName : String               = ""
    var recipientRole:String              = ""
    var corporateId:Int                   = 0
    var corporateName:String              = ""
    var schools:[corporateManagerSchoolModel]?
    var employeeId:Int                    = 0
    var studentPromotionStatus:String     = ""
    var canCreateGroup:Int                = 0
    var audioAttachmentEnabled:Bool       = false
    var canComposeNotice:Bool             = false
    var canFeeDueListEnable:Bool          = false
    var isEnableContinuousEvaluation:Bool = false
    
    
    //    init(withUserName:String, userId id:Int, usersDesignation designation:String, currentUserRoleId userRoleId:Int, currentTypeOfUser typeOfUser:String, profileImageUrl imgUrl:String, withStudents students:[StudentModel]?){
    //    }
    
    //TODO: need to change the class structure to reflect the profile picture
    
    /**
     Initilizer be used when the usermodel has to be created from the web  service(JSON).
     
     - parameter root: root class
     
     - returns: if required parameters are not met the initialization will fail
     */
    init?(makeUserModelFromRootObject root:RootClass){
        super.init()
        var result = true
        
        profilePic = ""// added here because the current root structure does not have the ability to store profile image
        if let userData = root.userdata{
            
            if let uName = userData.name{
                username = uName
            }else{
                result = false
                username = ""
            }
            
            if let uDesination = userData.userDesignation{
                userDesignation = uDesination
            }else {
                userDesignation = ""
            }
            
            if let uaudioAttachmentEnabled = userData.audioAttachmentEnabled{
                audioAttachmentEnabled = uaudioAttachmentEnabled
            }else {
                audioAttachmentEnabled = false
            }
            
            if let ucanComposeNotice = userData.canComposeNotice{
                canComposeNotice = ucanComposeNotice
            }else {
                canComposeNotice = false
            }
            
            if let ucanFeeDueListEnable = userData.canFeeDueListEnable{
                canFeeDueListEnable = ucanFeeDueListEnable
            }else {
                canFeeDueListEnable = false
            }
            
            if let uisEnableContinuousEvaluation = userData.isEnableContinuousEvaluation{
                isEnableContinuousEvaluation = uisEnableContinuousEvaluation
            }else {
                isEnableContinuousEvaluation = false
            }
            
            if let curRoleId = userData.id{
                userId = String(curRoleId)
            }else {
                userId = ""
            }
            
            
            if let currentUserType = userData.userRole{
                if currentUserType == "Guardian"{
                    currentTypeOfUser = .guardian
                }else if currentUserType == "CorporateManager" {
                    currentTypeOfUser = .CorporateManager
                }
                else{
                    currentTypeOfUser = .employee
                }
            }else {
                currentTypeOfUser = .employee
                result = false
            }
            
            if let profilePicture = userData.profilePic {
                profilePic = profilePicture
            }else {
                profilePic = ""
            }
            
            if let ucanSwitch = userData.canSwitch {
                canSwitch = ucanSwitch
            }else {
                canSwitch = -1
            }
            
            if let ucanCreateGroup = userData.canCreateGroup{
                canCreateGroup = ucanCreateGroup
            }else {
                canCreateGroup = 0
            }
            
            if let uMobileNo = userData.mobileNo {
                mobileNo = uMobileNo
            }else {
                mobileNo = ""
            }
            
            if let upasswordChanged = userData.passwordChanged {
                passwordChanged = upasswordChanged
            }else {
                passwordChanged = -1
            }
            
            if let  userroleid = userData.userRoleId {
                currentUserRoleId = String(userroleid)
            }else {
                currentUserRoleId = ""
            }
            
            if let uswitchAs = userData.switchAs {
                switchAs = uswitchAs
            }else {
                switchAs = ""
            }
            
            
            if let currentUserType = userData.userRole{
                if currentUserType == "Employee"{
                    
                    if let uschoolId = userData.schoolId {
                        schoolId = uschoolId
                    }else {
                        schoolId = 0
                    }
                    if let uschoolName = userData.schoolName {
                        schoolName = uschoolName
                    }else {
                        schoolName = ""
                    }
                    if let uemployeeType = userData.employeeType {
                        employeeType = uemployeeType
                    }else {
                        employeeType = ""
                    }
                    if let usmsExceeded = userData.smsExceeded {
                        smsExceeded = usmsExceeded
                    }else {
                        smsExceeded = 0
                    }
                    
                    if let usmsCharacterLimit = userData.smsCharacterLimit {
                        smsCharacterLimit = usmsCharacterLimit
                    }else {
                        smsCharacterLimit = ""
                    }
                    
                    if let empId = userData.employeeId {
                        employeeId = empId
                    }else {
                        employeeId = 0
                    }
                    
                    if let empstudentPromotionStatus = userData.studentPromotionStatus {
                        studentPromotionStatus = empstudentPromotionStatus
                    }else {
                        studentPromotionStatus = ""
                    }
                    
                    
                    
                }
                if(currentUserType == "CorporateManager")
                {
                    if let ucorporateId = userData.corporateId {
                        corporateId = ucorporateId
                    }else {
                        corporateId = 0
                    }
                    
                    if let ucorporateName = userData.corporateName {
                        corporateName = ucorporateName
                    }else {
                        corporateName = ""
                    }
                }
                
                
            }
            
            
        }else {
            result = false
        }
        
        
        if currentTypeOfUser == .guardian{
            
            if let userStudents = root.studentList as [StudentGuardian]?{
                
                for student in userStudents{
                    if let stdnt =  StudentModel.init(fromStudentGuardian: student) {
                        if Students == nil{
                            Students = [StudentModel]()
                        }
                        Students?.append(stdnt)
                    }
                }
            }else {
                result = false
            }
        }
        
        
        if currentTypeOfUser == .employee || currentTypeOfUser == .CorporateManager {
            
            if let employeeclassList = root.classList as [classSubjectList]?{
                
                for classListOfEmployee in employeeclassList{
                    if let cls =  EmployeeClassModel.init(fromClassList: classListOfEmployee) {
                        if Classes == nil{
                            Classes = [EmployeeClassModel]()
                        }
                        Classes?.append(cls)
                    }
                }
            }else {
                result = false
            }
        }
        
        if currentTypeOfUser == .CorporateManager{
            
            if let corporateManagerSchoolList = root.schoolList as [corporateSchoolList]?{
                
                for schoolListOfManager in corporateManagerSchoolList{
                    if let sch =  corporateManagerSchoolModel.init(fromSchoolList: schoolListOfManager) {
                        if schools == nil{
                            schools = [corporateManagerSchoolModel]()
                        }
                        schools?.append(sch)
                    }
                }
            }else {
                result = false
            }
        }
        
        if result == false {
            return nil
        }
    }
    
    
    /**
     Initialization for initiating for messages recipient selection UI
     
     - parameter withName:        name of the receipinet
     - parameter userDesignation: designation of user
     - parameter userId:          User ID
     - parameter profilePIc:      Profile PIC user
     
     - returns:
     */
    init(forMessageComposeUI withName:String, designation userDesignation:String, withUserId userId:String, profilePicOfUser profilePIc:String){
        
        username             = withName
        self.userId          = userId
        self.userDesignation = userDesignation
        profilePic             = profilePIc
        
        selectedInUI         = false
    }
    
    
    
    //MARK :- nscode decoding and encoding . This is made archivable , as we are presently using userdefault instead of coredata.
    
    required init(coder aDecoder: NSCoder) {
        
        if let adress = aDecoder.decodeObject(forKey: "address") as? String{
            self.adress = adress
        }

        self.canSwitch = aDecoder.decodeObject(forKey: "canSwitch") as? Int ?? aDecoder.decodeInteger(forKey: "canSwitch")
        
        
        if let userDesignation = aDecoder.decodeObject(forKey: "designation") as? String{
            self.userDesignation = userDesignation
        }
        if let switchAs  = aDecoder.decodeObject(forKey: "switchAs") as? String{
            self.switchAs  = switchAs
        }
        
        if let userId = aDecoder.decodeObject(forKey: "id") as? String{
           self.userId = userId
        }
        if let mobileNo = aDecoder.decodeObject(forKey: "mobileNo") as? String {
            self.mobileNo = mobileNo
        }
        if let username  = aDecoder.decodeObject(forKey: "name") as? String{
            self.username = username
        }

        self.passwordChanged = aDecoder.decodeObject(forKey: "passwordChanged") as? Int ?? aDecoder.decodeInteger(forKey: "passwordChanged")
        
        if let profilePic   = aDecoder.decodeObject(forKey: "profilePic") as? String{
            self.profilePic  = profilePic
        }
        self.currentTypeOfUser = currentUserType(rawValue: (aDecoder.decodeObject(forKey: "role") as? String)!)!
        if let currentUserRoleId  = aDecoder.decodeObject(forKey: "userRoleId") as? String{
            self.currentUserRoleId = currentUserRoleId
        }

        self.currentOptionSelected = aDecoder.decodeObject(forKey: "currentOptionSelected") as? Int ?? aDecoder.decodeInteger(forKey: "currentOptionSelected")
        
        self.currentSchoolSelected = aDecoder.decodeObject(forKey: "currentSchoolSelected") as? Int ?? aDecoder.decodeInteger(forKey: "currentSchoolSelected")
        
        if let selectedInUI = aDecoder.decodeObject(forKey: "selectedInUI") as? Bool{
            self.selectedInUI = selectedInUI
        }
        
        self.canCreateGroup = aDecoder.decodeObject(forKey: "canCreateGroup") as? Int ?? aDecoder.decodeInteger(forKey: "canCreateGroup")
        
        if let empaudioAttachmentEnabled = aDecoder.decodeObject(forKey: "audioAttachmentEnabled") as? Bool{
            self.audioAttachmentEnabled = empaudioAttachmentEnabled
        }
        if let empcanComposeNotice = aDecoder.decodeObject(forKey: "canComposeNotice") as? Bool{
            self.canComposeNotice = empcanComposeNotice
        }
        if let empcanFeeDueListEnable = aDecoder.decodeObject(forKey: "canFeeDueListEnable") as? Bool{
            self.canFeeDueListEnable = empcanFeeDueListEnable
        }
        if let empisEnableContinuousEvaluation = aDecoder.decodeObject(forKey: "isEnableContinuousEvaluation") as? Bool{
            self.isEnableContinuousEvaluation = empisEnableContinuousEvaluation
        }
        
        
        
        if currentTypeOfUser == .employee{

            self.schoolId = aDecoder.decodeObject(forKey: "schoolId") as? Int ?? aDecoder.decodeInteger(forKey: "schoolId")

            if let schoolName = aDecoder.decodeObject(forKey: "schoolName") as? String{
                self.schoolName = schoolName
            }
            if let stdn = aDecoder.decodeObject(forKey: "smsCharacterLimit") as? String{
                self.smsCharacterLimit = stdn
            }
            
            if let empType = aDecoder.decodeObject(forKey: "employeeType") as? String{
                self.employeeType = empType
            }
            
            if let smsExceeded = aDecoder.decodeObject(forKey: "smsExceeded") as? Int{
                self.smsExceeded =  smsExceeded
            }
            
            self.smsExceeded = aDecoder.decodeObject(forKey: "smsExceeded") as? Int ?? aDecoder.decodeInteger(forKey: "smsExceeded")
            
            if let stdn = aDecoder.decodeObject(forKey: "smsCharacterLimit") as? String{
                self.smsCharacterLimit = stdn
            }

            self.employeeId = aDecoder.decodeObject(forKey: "employeeId") as? Int ?? aDecoder.decodeInteger(forKey: "employeeId")
            
            if let empstudentPromotionStatus = aDecoder.decodeObject(forKey: "studentPromotionStatus") as? String{
                self.studentPromotionStatus = empstudentPromotionStatus
            }
            
        }
        if currentTypeOfUser == .CorporateManager
        {

            self.corporateId = aDecoder.decodeObject(forKey: "corporateId") as? Int ?? aDecoder.decodeInteger(forKey: "corporateId")
            
            if let ucorporateName = aDecoder.decodeObject(forKey: "corporateName") as? String{
                self.corporateName = ucorporateName
            }
        }
        
        if let stdn = aDecoder.decodeObject(forKey: "students") as? [StudentModel]{
            self.Students = stdn
        }
        if let clas = aDecoder.decodeObject(forKey: "classes") as? [EmployeeClassModel]{
            self.Classes = clas
        }
        if let sch = aDecoder.decodeObject(forKey: "schools") as? [corporateManagerSchoolModel]{
            self.schools = sch
        }
        
        
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(adress, forKey: "address")
        aCoder.encode(canSwitch, forKey: "canSwitch")
        aCoder.encode(userDesignation, forKey: "designation")
        aCoder.encode(userId, forKey: "id")
        aCoder.encode(mobileNo, forKey: "mobileNo")
        aCoder.encode(username, forKey: "name")
        aCoder.encode(passwordChanged, forKey: "passwordChanged")
        aCoder.encode(profilePic, forKey: "profilePic")
        aCoder.encode(currentTypeOfUser.rawValue, forKey: "role")
        aCoder.encode(switchAs, forKey: "switchAs")
        aCoder.encode(currentUserRoleId, forKey: "userRoleId")
        aCoder.encode(currentOptionSelected, forKey: "currentOptionSelected")
        aCoder.encode(currentSchoolSelected, forKey: "currentSchoolSelected")
        aCoder.encode(canCreateGroup, forKey: "canCreateGroup")
        aCoder.encode(audioAttachmentEnabled, forKey: "audioAttachmentEnabled")
        aCoder.encode(canComposeNotice, forKey: "canComposeNotice")
        aCoder.encode(canFeeDueListEnable, forKey: "canFeeDueListEnable")
        aCoder.encode(isEnableContinuousEvaluation, forKey: "isEnableContinuousEvaluation")
        
        
        
        
        aCoder.encode(selectedInUI, forKey: "selectedInUI")
        if currentTypeOfUser == .employee{
            aCoder.encode(schoolId, forKey: "schoolId")
            aCoder.encode(schoolName, forKey: "schoolName")
            aCoder.encode(employeeType, forKey: "employeeType")
            aCoder.encode(smsExceeded, forKey: "smsExceeded")
            aCoder.encode(smsCharacterLimit, forKey: "smsCharacterLimit")
            aCoder.encode(employeeId, forKey: "employeeId")
            aCoder.encode(studentPromotionStatus, forKey: "studentPromotionStatus")
        }
        if currentTypeOfUser == .CorporateManager{
            aCoder.encode(corporateId, forKey: "corporateId")
            aCoder.encode(corporateName, forKey: "corporateName")
            
        }
        
        if let students = self.Students {
            aCoder.encode(students, forKey: "students")
        }
        if let clas = self.Classes{
            aCoder.encode(clas, forKey: "classes")
        }
        if let sch = self.schools{
            aCoder.encode(sch, forKey: "schools")
        }
        
    }
    
    class func makeArrayOfUserFromRecipientDictionary(dicto:[String:AnyObject]) -> ([userModel],NSError?){
        var result = true
        
        var error:NSError?
        var usersArray  = [userModel]()
        
        if let userArray = dicto[resultsJsonConstants.messageResultContants.messageRecipients] as? [[String:AnyObject]]{
            
            for user in userArray {
                var idOrUser          = ""
                var desigantionOfUser = ""
                var profilePicOfUser  = ""
                var nameUser          = ""
                
                if let id = user[resultsJsonConstants.messageResultContants.messagerecipientId] as? Int{
                    idOrUser = String(id)
                }else {
                    result = false
                }
                
                if let designation = user[resultsJsonConstants.messageResultContants.messageSenderDesignation] as? String{
                    let temp = designation
                    desigantionOfUser = temp.replacingOccurrences(of: ",", with: "\n")
                }else {
                    result = false
                }
                
                if let profilePic = user[resultsJsonConstants.messageResultContants.messageSenderProfilePIc] as? String{
                    profilePicOfUser = profilePic
                }
                
                if let nme = user[resultsJsonConstants.messageResultContants.messageRecipientName] as? String{
                    nameUser = nme
                }else {
                    result = false
                }
                
                usersArray.append(userModel.init(forMessageComposeUI: nameUser, designation: desigantionOfUser, withUserId: idOrUser, profilePicOfUser: profilePicOfUser))
            }
            
        }else {
            error = NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:"JSON NOT VALID"])
        }
        
        return (usersArray,error)
        
    }
    
    internal func getCurrentlySelectedSchoolClass()->Int?{
        if self.Classes != nil {
            if (self.Classes?.count)! > 0 {
                return self.Classes![0].classId
            }
        }
        return 0
    }
    
    
    internal func getStudentCurrentlySelectedStudent()->StudentModel?{
        if self.Students != nil {
            if (self.Students?.count)! > 0 {
                return self.Students![currentOptionSelected]
            }
        }
        return nil
    }
    internal func getStudentCurrentlySelectedSchool()->corporateManagerSchoolModel?{
        if self.schools != nil {
            if (self.schools?.count)! > 0 {
                return self.schools![currentSchoolSelected]
            }
        }
        return nil
    }
    
    internal func getSelectedEmployeeOrStudentProfileImageUrl() ->String{
        
        var imgUrlString = ""
        if currentTypeOfUser == .guardian{
            if self.Students != nil {
                if (self.Students?.count)! > 0 {
                    imgUrlString = self.Students![currentOptionSelected].profilePic
                }
                
            }
        }
        else{
            imgUrlString = profilePic
        }
        return imgUrlString
    }
    
    internal func getSelectedEmployeeOrStudentId() ->String? {
        
        let std = getStudentCurrentlySelectedStudent()
        if std != nil {
            return std?.id
        }
        return nil
    }
    internal func getSelectedEmployeeOrStudentschoolId() ->String? {
        
        let std = getStudentCurrentlySelectedStudent()
        if std != nil {
            return String(std!.schoolId)
        }
        return nil
    }
    internal func getSelectedEmployeeOrStudentGrade() ->String? {
        
        let std = getStudentCurrentlySelectedStudent()
        if std != nil {
            return String(std!.className)
        }
        return nil
    }
    internal func getSelectedEmployeeOrStudentClassTeacher() ->String? {
        
        let std = getStudentCurrentlySelectedStudent()
        if std != nil {
            return String(std!.classTeacher)
        }
        return nil
    }
    
}
