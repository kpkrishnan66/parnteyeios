//
//  EmployeeModel.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 04/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class EmployeeClassModel:NSObject,NSCoding {
    
    let classId : Int!
    let className : String!
    let subjectId : String!
    let subjectName : String!
    var selectedInUI = false
    var schoolName:String!
    var classInChargeEmployeeId:Int!
    var classInCharge:String!
    
    /**
     Creating a defensive and optional initializer here
     
     - parameter student: A student guardian object
     
     - returns: if required parameters are not met the initialization will fail
     */
    init?(fromClassList classList:classSubjectList){
        
        var result = true
        
        
        if let StdntClassId = classList.classId as Int?{
            classId  = StdntClassId
        }else {
            classId = -1
            result = false
        }
        
        
        if let StdntClassName = classList.className as String?{
            className  = StdntClassName
        }else {
            className = ""
            result = false
        }
        
        if let EmployeesubjectId = classList.subjectId as String?{
            subjectId  = EmployeesubjectId
        }else {
            subjectId = ""
            result = false
        }
       
        if let EmployeesubjectName = classList.subjectName as String?{
            subjectName  = EmployeesubjectName
        }else {
            subjectName = ""
            result = false
        }
        
        if let EmployeeclassInChargeEmployeeId = classList.classInChargeEmployeeId as Int?{
            classInChargeEmployeeId  = EmployeeclassInChargeEmployeeId
        }else {
            classInChargeEmployeeId = -1
        }
        
        if let EmployeeclassInCharge = classList.classInCharge as String?{
            classInCharge  = EmployeeclassInCharge
        }else {
            classInCharge = ""
        }
        
        
        if result == false {
            return nil
        }
        
    }
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atcID = dictionary[resultsJsonConstants.diaryStudentPopup.classId] as? Int{
            self.classId = atcID
        }else {
            self.classId = -1
        }
        
        
        if let clsname = dictionary[resultsJsonConstants.diaryStudentPopup.className] as? String{
            self.className = clsname
        }else {
            self.className = ""
        }
        
        
        if let schName = dictionary[resultsJsonConstants.diaryStudentPopup.schoolName] as? String{
            self.schoolName = schName
        }else {
            self.schoolName = ""
        }
        
        if let EmployeesubjectId = "subjectId" as String?{
            subjectId  = EmployeesubjectId
        }else {
            subjectId = ""
            
        }
        if let EmployeesubjectName = "subjectName" as String?{
            subjectName  = EmployeesubjectName
        }else {
            subjectName = ""
            
        }
    }
    
    
    required init(coder aDecoder: NSCoder) {
        
        self.classId = aDecoder.decodeObject(forKey: "classId") as? Int ?? aDecoder.decodeInteger(forKey: "classId")
        
        if let mclassName = aDecoder.decodeObject(forKey: "className") as? String {
            self.className = mclassName
        }else {
            self.className = ""
            
        }
        
        if let msubjectId = aDecoder.decodeObject(forKey: "subjectId") as? String {
            self.subjectId = msubjectId
        }else {
            self.subjectId = ""
            
        }
        
        if let msubjectName = aDecoder.decodeObject(forKey: "subjectName") as? String {
            self.subjectName = msubjectName
        }else {
            self.subjectName = ""
            
        }
        
        self.classInChargeEmployeeId = aDecoder.decodeObject(forKey: "classInChargeEmployeeId") as? Int ?? aDecoder.decodeInteger(forKey: "classInChargeEmployeeId")
        if let classInCharge = aDecoder.decodeObject(forKey: "classInCharge") as? String {
            self.classInCharge = classInCharge
        }else {
            self.classInCharge = ""
            
        }
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        if let clsid = self.classId as Int?  {
            aCoder.encode(clsid, forKey: "classId")
        }
        
        
        if let clsNme = self.className as String?  {
            aCoder.encode(clsNme, forKey: "className")
        }
        
        if let subjId = self.subjectId as String?  {
            aCoder.encode(subjId, forKey: "subjectId")
        }
        
        
        if let subjNme = self.subjectName as String?  {
            aCoder.encode(subjNme, forKey: "subjectName")
        }
        
        if let empclassInChargeEmployeeId = self.classInChargeEmployeeId as Int?  {
            aCoder.encode(empclassInChargeEmployeeId, forKey: "classInChargeEmployeeId")
        }
        if let empclassInCharge = self.classInCharge as String?  {
            aCoder.encode(empclassInCharge, forKey: "classInCharge")
        }
        
    }
}

    class  parseSchoolsList {
        class func parseThisDictionaryForSchoolListPopup(dictionary:[String:AnyObject])->([EmployeeClassModel]){
            
            var schoolListArray = [EmployeeClassModel]()
            
            if let schoolList = dictionary[resultsJsonConstants.diaryStudentPopup.classSubjectList] as? [AnyObject]{
                for dicto in schoolList{// parse the array in the root
                    if let msg = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = EmployeeClassModel(withDictionaryFromWebService: msg){
                            schoolListArray.append(atcObj)
                        }
                    }
                }
            }
            
            return(schoolListArray)
        }
        
    }
    

