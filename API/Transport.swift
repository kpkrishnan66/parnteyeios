//
//  Transport.swift
//  parentEye
//
//  Created by scientia on 17/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class Transport  {
    var id : Int = 0
    var busName : String =  ""
    var latitude : String = ""
    var longitude : String = ""
    var tripType : String = ""
    var lastUpdatedTime : String = ""
    var accuracy : String = ""
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atId = dictionary[resultsJsonConstants.locationTransport.id] as? Int{
            self.id = atId
        }else {
            self.id = 0
        }
        
        if let atBusName = dictionary[resultsJsonConstants.locationTransport.busName] as? String{
            self.busName = atBusName
        }else {
            self.busName = ""
        }
        
        if let atLatitude = dictionary[resultsJsonConstants.locationTransport.latitude] as? String{
            self.latitude = atLatitude
        }else {
            self.latitude = ""
        }
        
        if let atLongitude = dictionary[resultsJsonConstants.locationTransport.longitude] as? String{
            self.longitude = atLongitude
        }else {
            self.longitude = ""
        }
        if let atTripType = dictionary[resultsJsonConstants.locationTransport.tripType] as? String{
            self.tripType = atTripType
        }else {
            self.tripType = ""
        }
        if let atLastUpdated = dictionary[resultsJsonConstants.locationTransport.lastUpdatedOn] as? String{
            self.lastUpdatedTime = atLastUpdated
        }else {
            self.lastUpdatedTime = ""
        }
        if let atAccuracy = dictionary[resultsJsonConstants.locationTransport.accuracy] as? String{
            self.accuracy = atAccuracy
        }else {
            self.accuracy = ""
        }
    }
}

class  parseTransportListArray {
    class func parseThisDictionaryForLocationRecords(dictionary:[String:AnyObject])->([Transport]){
        
        var transportRecordArray = [Transport]()
        
        if let locationRecord = dictionary[resultsJsonConstants.locationTransport.location] as? [AnyObject]{
            for dicto in locationRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = Transport(withDictionaryFromWebService: record){
                        transportRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(transportRecordArray)
    }
    
}
