//
//  feeList.swift
//  parentEye
//
//  Created by scientia on 25/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class feeList {
    
    var name:String = ""
    var amount:String = ""
    
    init(fromDictionary dictionary:[String:AnyObject]){
        
        if let feeName  =  dictionary[resultsJsonConstants.invoiceList.name] as? String{
            name = feeName
        }
        if let feeAmount =  dictionary[resultsJsonConstants.invoiceList.amount] as? String {
            amount = feeAmount
        }
    }
}
