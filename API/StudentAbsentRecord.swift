//
//  StudentAbsentRecord.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 30/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class StudentAbsentRecord {
    
    var  id = 0
    var  studentName = ""
    var  profilePic = ""
    var  mobileNo = ""
    var  reason = ""
    var  date = ""
    var  leaveStatus = ""
    var  status = ""
    var description = ""
    var schoolClassId = 0
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let absrecordID = dictionary[resultsJsonConstants.StudentAbsentRecord.id] as? Int{
            self.id = absrecordID
        }else {
            self.id = -1
        }
        
        if let absrecordname = dictionary[resultsJsonConstants.StudentAbsentRecord.studentName] as? String{
            self.studentName = absrecordname
        }else {
            self.studentName = ""
        }
        
        if let absrecordprofilepic = dictionary[resultsJsonConstants.StudentAbsentRecord.profilePic] as? String{
            self.profilePic = absrecordprofilepic
        }else {
            self.profilePic = ""
        }
        
        if let absrecordmobno = dictionary[resultsJsonConstants.StudentAbsentRecord.mobileNo] as? String{
            self.mobileNo = absrecordmobno
        }else {
            self.mobileNo = ""
        }
        
        if let absrecordreason = dictionary[resultsJsonConstants.StudentAbsentRecord.reason] as? String{
            self.reason = absrecordreason
        }else {
            self.reason = ""
        }
        
        if let absrecorddate = dictionary[resultsJsonConstants.StudentAbsentRecord.date] as? String{
            self.date = absrecorddate
        }else {
            self.date = ""
        }
        
        if let absrecordleavestatus = dictionary[resultsJsonConstants.StudentAbsentRecord.leaveStatus] as? String{
            self.leaveStatus = absrecordleavestatus
        }else {
            self.leaveStatus = ""
        }
        if let absrecordstatus = dictionary[resultsJsonConstants.StudentAbsentRecord.status] as? String{
            self.status = absrecordstatus
        }else {
            self.status = ""
        }
        if let absrecorddescription = dictionary[resultsJsonConstants.StudentAbsentRecord.description] as? String{
            self.description = absrecorddescription
        }else {
            self.description = ""
        }
        
        if let absrecordschoolClassId = dictionary[resultsJsonConstants.StudentAbsentRecord.schoolClassId] as? Int{
            self.schoolClassId = absrecordschoolClassId
        }else {
            self.schoolClassId = 0
        }
        
    }

}


class  parseAbsentRecordArray {
    class func parseThisDictionaryForAbsentRecords(dictionary:[String:AnyObject])->([StudentAbsentRecord]){
        
        var absentRecordArray = [StudentAbsentRecord]()
        
        if let studentAbsentRecord = dictionary[resultsJsonConstants.StudentAbsentRecord.studentAbsentRecord] as? [AnyObject]{
            for dicto in studentAbsentRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = StudentAbsentRecord(withDictionaryFromWebService: record){
                    absentRecordArray.append(atcObj)
                    }
                }
            }
        }
        
           return(absentRecordArray)
    }
    
}

