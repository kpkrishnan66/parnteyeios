//
//  feeDetails.swift
//  parentEye
//
//  Created by scientia on 18/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class feeDetails {
    
    
    var needToUpdateMessage    = ""
    var paymentId              = ""
    var displayName            = ""
    var discriminator          = ""
    var txnidForNetBanking     = ""
    var surcharge              = ""
    var password               = ""
    var clientcodeForCard      = ""
    var custacc                = ""
    var channelid              = ""
    var txncurr                = ""
    var amount                 = ""
    var merchantId             = ""
    var txnidForCard           = ""
    var prodid                 = ""
    var ru                     = ""
    var clientcodeForNetBanking = ""
    var loginid                = ""
    var paidOn                 = ""
    var currentFeeVersion      = ""
    var publicKey              = ""
    var urlios                 = ""
    var signatureRequest       = ""
    var signatureResponse      = ""
    
    
    init?(withDictionaryFromWebServiceFeepaymentDetails feeDictionary:[String:AnyObject]){
        
        if let feneedToUpdateMessage = feeDictionary[resultsJsonConstants.feeDetails.needToUpdateMessage] as? String{
            self.needToUpdateMessage = feneedToUpdateMessage
        }else {
            self.needToUpdateMessage = ""
        }
        
        if let fepaymentId = feeDictionary[resultsJsonConstants.feeDetails.paymentId] as? String{
            self.paymentId = fepaymentId
        }else {
            self.paymentId = ""
        }
        
        if let fedisplayName = feeDictionary[resultsJsonConstants.feeDetails.displayName] as? String{
            self.displayName = fedisplayName
        }else {
            self.displayName = ""
        }
        
        if let fediscriminator = feeDictionary[resultsJsonConstants.feeDetails.discriminator] as? String{
            self.discriminator = fediscriminator
        }else {
            self.discriminator = ""
        }
        
        if let fetxnidForNetBanking = feeDictionary[resultsJsonConstants.feeDetails.txnidForNetBanking] as? String{
            self.txnidForNetBanking = fetxnidForNetBanking
        }else {
            self.txnidForNetBanking = ""
        }
        
        if let fesurcharge = feeDictionary[resultsJsonConstants.feeDetails.surcharge] as? String{
            self.surcharge = fesurcharge
        }else {
            self.surcharge = ""
        }
        
        if let fepassword = feeDictionary[resultsJsonConstants.feeDetails.password] as? String{
            self.password = fepassword
        }else {
            self.password = ""
        }
        
        if let feclientcodeForCard = feeDictionary[resultsJsonConstants.feeDetails.clientcodeForCard] as? String{
            self.clientcodeForCard = feclientcodeForCard
        }else {
            self.clientcodeForCard = ""
        }
        
        if let fecustacc = feeDictionary[resultsJsonConstants.feeDetails.custacc] as? String{
            self.custacc = fecustacc
        }else {
            self.custacc = ""
        }
        
        if let fechannelid = feeDictionary[resultsJsonConstants.feeDetails.channelid] as? String{
            self.channelid = fechannelid
        }else {
            self.channelid = ""
        }
        
        if let fetxncurr = feeDictionary[resultsJsonConstants.feeDetails.txncurr] as? String{
            self.txncurr = fetxncurr
        }else {
            self.txncurr = ""
        }
        
        if let feamount = feeDictionary[resultsJsonConstants.feeDetails.amount] as? String{
            self.amount = feamount
        }else {
            self.amount = ""
        }
        
        if let femerchantId = feeDictionary[resultsJsonConstants.feeDetails.merchantId] as? String{
            self.merchantId = femerchantId
        }else {
            self.merchantId = ""
        }
        
        if let fetxnidForCard = feeDictionary[resultsJsonConstants.feeDetails.txnidForCard] as? String{
            self.txnidForCard = fetxnidForCard
        }else {
            self.txnidForCard = ""
        }
        
        if let feprodid = feeDictionary[resultsJsonConstants.feeDetails.prodid] as? String{
            self.prodid = feprodid
        }else {
            self.prodid = ""
        }
        
        if let feru = feeDictionary[resultsJsonConstants.feeDetails.ru] as? String{
            self.ru = feru
        }else {
            self.ru = ""
        }
        
        if let feclientcodeForNetBanking = feeDictionary[resultsJsonConstants.feeDetails.clientcodeForNetBanking] as? String{
            self.clientcodeForNetBanking = feclientcodeForNetBanking
        }else {
            self.clientcodeForNetBanking = ""
        }
        
        if let feloginid = feeDictionary[resultsJsonConstants.feeDetails.loginid] as? String{
            self.loginid = feloginid
        }else {
            self.loginid = ""
        }
        
        if let fepaidOn = feeDictionary[resultsJsonConstants.feeDetails.paidOn] as? String{
            self.paidOn = fepaidOn
        }else {
            self.paidOn = ""
        }
        
        if let fecurrentFeeVersion = feeDictionary[resultsJsonConstants.feeDetails.currentFeeVersion] as? String{
            self.currentFeeVersion = fecurrentFeeVersion
        }else {
            self.currentFeeVersion = ""
        }

        
        if let fepublicKey = feeDictionary[resultsJsonConstants.feeDetails.publicKey] as? String{
            self.publicKey = fepublicKey
        }else {
            self.publicKey = ""
        }
        
        if let feurlios = feeDictionary[resultsJsonConstants.feeDetails.urlios] as? String{
            self.urlios = feurlios
        }else {
            self.urlios = ""
        }
        
        if let fesignatureRequest = feeDictionary[resultsJsonConstants.feeDetails.signatureRequest] as? String{
            self.signatureRequest = fesignatureRequest
        }else {
            self.signatureRequest = ""
        }
        
        if let fesignatureResponse = feeDictionary[resultsJsonConstants.feeDetails.signatureResponse] as? String{
            self.signatureResponse = fesignatureResponse
        }else {
            self.signatureResponse = ""
        }
        
    }
}

class  parseFeeDetailsArray {
    class func parseThisDictionaryForFeeDetailsRecords(dictionary:[String:AnyObject])->([feeDetails]){
        
        var feeDetailsRecordArray = [feeDetails]()
        
        
        if let feeDetailsRecord = dictionary[resultsJsonConstants.feeDetails.feeDetails] as? [String:AnyObject]{
            
                    if let atcObj = feeDetails(withDictionaryFromWebServiceFeepaymentDetails: feeDetailsRecord){
                        feeDetailsRecordArray.append(atcObj)
                    }
        }
        
        return(feeDetailsRecordArray)
    }
}
