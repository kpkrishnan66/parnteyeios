//
//  examList.swift
//  parentEye
//
//  Created by Vivek on 16/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class examList {
    
    var id:Int = 0
    var name:String = ""
    
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let exId = dictionary[resultsJsonConstants.contactListReport.id] as? Int{
            self.id = exId
        }else {
            self.id = 0
        }
        
        if let exName = dictionary[resultsJsonConstants.contactListReport.name] as? String{
            self.name = exName
        }else {
            self.name = ""
        }
        
    }
}

class  parseExamListArray {
    class func parseThisDictionaryForExamListRecords(dictionary:[String:AnyObject])->([examList]){
        
        var examListRecordArray = [examList]()
        
        if let examListRecord = dictionary[resultsJsonConstants.examList.examList] as? [AnyObject]{
            for dicto in examListRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = examList(withDictionaryFromWebService: record){
                        examListRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(examListRecordArray)
    }
    
}
