//
//  classTest.swift
//  parentEye
//
//  Created by scientia on 17/07/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation


class classTest {
    var id:Int = 0
    var name:String = ""
    var maximumMark:Int = 0
    var subject:String = ""
    var subjectId:Int = 0
    var date:String = ""
    var time:String = ""
    var topic:String = ""
    var mark:String = ""
    var addedBy:Int = 0
    var addedByName:String = ""
    var upcoming:String = ""
    var evaluvationListArray = [evaluationActivity]()
    var dayString = ""
    var monthString = ""
    var yearString = ""
    var topicNewlineCout:Int = 0
    var evaluationActivityName:String = ""
    
    init() {
        
    }
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let ctId = dictionary[resultsJsonConstants.classTest.id] as? Int{
            self.id = ctId
        }else {
            self.id = 0
        }
        
        if let ctname = dictionary[resultsJsonConstants.classTest.name] as? String{
            self.name = ctname
        }else {
            self.name = ""
        }
        
        if let ctmaximumMark = dictionary[resultsJsonConstants.classTest.maxMark] as? Int{
            self.maximumMark = ctmaximumMark
        }else {
            self.maximumMark = 0
        }
        
        if let ctsubject = dictionary[resultsJsonConstants.classTest.subject] as? String{
            self.subject = ctsubject
        }else {
            self.subject = ""
        }
        
        if let ctsubjectId = dictionary[resultsJsonConstants.classTest.subjectId] as? Int{
            self.subjectId = ctsubjectId
        }else {
            self.subjectId = 0
        }
        
        if let ctdate = dictionary[resultsJsonConstants.classTest.date] as? String{
            self.date = ctdate
            let dateStringArray = ctdate.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
            
            if  dateStringArray.count >= 3{
                dayString = dateStringArray[2]
                
                // This checking is used to check if the month format is a number (eg:8) or string (eg:august)
                let letters = NSCharacterSet.letters
                let range = dateStringArray[1].rangeOfCharacter(from: letters)
                
                if let test = range {
                  monthString = dateStringArray[1]
                }
                else {
                    let dateFormatter: DateFormatter = DateFormatter()
                    let months = dateFormatter.shortMonthSymbols
                    let monthSymbol = months?[Int(dateStringArray[1])!-1]
                    monthString = monthSymbol!
                }
                
                yearString = dateStringArray[0]
            }
        }else {
            self.date = ""
        }
        
        if let cttime = dictionary[resultsJsonConstants.classTest.time] as? String{
            self.time = cttime
        }else {
            self.time = ""
        }
        
        if let cttopic = dictionary[resultsJsonConstants.classTest.topic] as? String{
            self.topic = cttopic
            var newLineCount = 0
            for i in self.topic.characters {
                if i == "\n" {
                   newLineCount += 1
                }
            }
            self.topicNewlineCout = newLineCount
        }else {
            self.topic = ""
        }
        
        if let ctmark = dictionary[resultsJsonConstants.classTest.mark] as? String{
            self.mark = ctmark
        }else {
            self.mark = ""
        }
        
        if let ctaddedBy = dictionary[resultsJsonConstants.classTest.addedBy] as? Int{
            self.addedBy = ctaddedBy
        }else {
            self.addedBy = 0
        }
        
        if let ctaddedByName = dictionary[resultsJsonConstants.classTest.addedByName] as? String{
            self.addedByName = ctaddedByName
        }else {
            self.addedByName = ""
        }
        
        if let ctupcoming = dictionary[resultsJsonConstants.classTest.upcoming] as? String{
            self.upcoming = ctupcoming
        }else {
            self.upcoming = ""
        }
        
        if let atevaluvationList = dictionary[resultsJsonConstants.classTest.evaluationActivity] as? NSArray {
            for evaluvation in atevaluvationList {
                let feeValue = evaluationActivity(withDictionaryFromWebService: evaluvation as! [String : AnyObject])
                evaluvationListArray.append(feeValue)
            }
            
        }
        
        if let ctevaluationActivityName = dictionary[resultsJsonConstants.classTest.evaluationActivityName] as? String{
            self.evaluationActivityName = ctevaluationActivityName
        }else {
            self.evaluationActivityName = ""
        }
        
    
    }

  }

class  parseclassTestArray {
    class func parseThisDictionaryForclassTest(dictionary:[String:AnyObject])->([classTest]){
        
        var classTestRecordArray = [classTest]()
        
        if let classTestRecord = dictionary[resultsJsonConstants.classTest.classTestListing] as? NSArray{
            
                for dicto in classTestRecord{// parse the array in the root
                    
                        if let atcObj = classTest(withDictionaryFromWebService: dicto as! [String : AnyObject]){
                            classTestRecordArray.append(atcObj)
                        }
                    
                }
        }
        
        return(classTestRecordArray)
    }
    
}
