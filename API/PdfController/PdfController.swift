//
//  PdfController.swift
//  parentEye
//
//  Created by Martin Jacob on 26/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class PdfController:NSObject, UIDocumentInteractionControllerDelegate {
    
    var viewControllerToShowPdf:UIViewController?
    
     func showPdfInViewController(vc:UIViewController , pdfHavingUrl url:String, withName name:String) {
        
        viewControllerToShowPdf = vc
        if let path = isPdfWithNameAlreadyDownLoaded(name: name) {
            insertPdfToViewController(viewController: vc, urlOfPdf: path)
        }else {
            getPdfWithUrl(url: url, tobeSavedTopath: getDirectoryForFile(name: name))
        }
        
    }
    
    private  func getDirectoryForFile(name:String) ->NSURL {
        let paths = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let getImagePath = paths.appendingPathComponent(name)
        return getImagePath! as NSURL
    }
    
    private  func isPdfWithNameAlreadyDownLoaded(name:String) ->NSURL?{
        
        let getImagePath = getDirectoryForFile(name: name)
        let checkValidation = FileManager.default
        
        debugPrint(getImagePath.path!)
        
        if (checkValidation.fileExists(atPath: getImagePath.path!)){
                return getImagePath
        }
        else{
                return nil
        }
    }
    
      internal func addPdfToVC(vc:UIViewController, urlToBeLoaded url:String){
        let com = commonWebView()
        com.webView.scalesPageToFit = true
        //com.webView.scalesPageToFit = true
        com.frame = vc.view.bounds
        vc.view.addSubview(com)
        com.loadPage(url: NSURL(string: url)!)
        
        com.translatesAutoresizingMaskIntoConstraints = false //Don't forget this line
        let leftSideConstraint = NSLayoutConstraint(item: com, attribute: .left, relatedBy: .equal, toItem: vc.view, attribute: .left, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: com, attribute: .bottom, relatedBy: .equal, toItem: vc.view, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let widthConstraint = NSLayoutConstraint(item: com, attribute: .width, relatedBy: .equal, toItem: vc.view, attribute: .width, multiplier: 1.0, constant: 0.0)
        let heightConstraint = NSLayoutConstraint(item: com, attribute: .height, relatedBy: .equal, toItem: vc.view, attribute: .height, multiplier: 1.0, constant: 0.0)
        vc.view.addConstraints([leftSideConstraint, bottomConstraint, heightConstraint, widthConstraint])
    }
    
    internal func insertPdfToViewController (viewController:UIViewController, urlOfPdf url:NSURL){
    
        let docVC = UIDocumentInteractionController(url: url as URL)
        docVC.delegate = self
        docVC.presentPreview(animated: false)
        //docVC.presentOpenInMenuFromRect(viewController.view.frame, inView: viewController.view, animated: false)
    }
    
    private  func getPdfWithUrl(url:String, tobeSavedTopath path:NSURL){
        
        weak  var  weakSelf = self
        hudControllerClass.showNormalHudToViewController(viewController: viewControllerToShowPdf!)
        WebServiceApi.downLoadFileFromUrl(urlString: url, toBeSavedTopath: path, onSuccess: { (result) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!.viewControllerToShowPdf!)
                weakSelf!.insertPdfToViewController(viewController: weakSelf!.viewControllerToShowPdf!, urlOfPdf: path)
            }, onFailure: { (error) in
                hudControllerClass.hideHudInViewController(viewController: weakSelf!.viewControllerToShowPdf!)
            }) { (progress) in
                
        }
    }
   
    //MARK:- document interaction cintroller delegate 
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return viewControllerToShowPdf!
    }
    
}
