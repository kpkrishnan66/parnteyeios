//
//  corporateSchoolList.swift
//  parentEye
//
//  Created by Vivek on 06/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class corporateSchoolList : NSObject, NSCoding{
    
    var schoolId : Int!
    var schoolName : String!
    var schoolNickName : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        
        schoolId = dictionary["schoolId"] as? Int
        schoolName = dictionary["schoolName"] as? String
        schoolNickName = dictionary["schoolNickName"] as? String
        
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        
        
        if schoolId != nil{
            dictionary["schoolId"] = schoolId
        }
        
        if schoolName != nil{
            dictionary["schoolName"] = schoolName
        }
        if schoolNickName != nil{
            dictionary["schoolNickName"] = schoolNickName
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        
        schoolId = aDecoder.decodeObject(forKey: "schoolId") as? Int
        schoolName = aDecoder.decodeObject(forKey: "schoolName") as? String
        schoolNickName = aDecoder.decodeObject(forKey: "schoolNickName") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if schoolId != nil{
            aCoder.encode(schoolId, forKey: "schoolId")
        }
        
        if schoolName != nil{
            aCoder.encode(schoolName, forKey: "schoolName")
        }
        if schoolNickName != nil{
            aCoder.encode(schoolNickName, forKey: "schoolNickName")
        }
        
        
    }
    
}
