//
//	UserData.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserData : NSObject, NSCoding{
    
    var adress : String!
    var canSwitch : Int!
    var userDesignation : String!
    var id : Int!
    var employeeId:Int!
    var mobileNo : String!
    var name : String!
    var passwordChanged : Int!
    var profilePic : String!
    var userRole : String!
    var switchAs : String!
    var userRoleId : Int!
    var schoolId : Int!
    var employeeType : String!
    var smsExceeded : Int!
    var smsCharacterLimit : String!
    var schoolName : String!
    var corporateId:Int!
    var corporateName:String!
    var studentPromotionStatus:String!
    var canCreateGroup:Int!
    var audioAttachmentEnabled:Bool!
    var canComposeNotice:Bool!
    var canFeeDueListEnable:Bool!
    var isEnableContinuousEvaluation:Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        
        adress = dictionary["address"] as? String
        userDesignation = dictionary["designation"] as? String!
        id = dictionary["id"] as? Int
        mobileNo = dictionary["mobileNo"] as? String
        name = dictionary["name"] as? String
        passwordChanged = dictionary["passwordChanged"] as? Int
        profilePic = dictionary["profilePic"] as? String
        userRole = dictionary["role"] as? String
        switchAs = dictionary["switchAs"] as? String
        userRoleId = dictionary["userRoleId"] as? Int
        audioAttachmentEnabled = dictionary["audioAttachmentEnabled"] as? Bool
        canComposeNotice = dictionary["canComposeNotice"] as? Bool
        canFeeDueListEnable = dictionary["canFeeDueListEnable"] as? Bool
        isEnableContinuousEvaluation = dictionary["isEnableContinuousEvaluation"] as? Bool
        
        
        if(userRole == "Employee")
        {
            schoolId = dictionary["schoolId"] as? Int
            schoolName = dictionary["schoolName"] as? String
            employeeType = dictionary["employeeType"] as? String
            smsExceeded = dictionary["smsExceeded"] as? Int
            smsCharacterLimit = dictionary["smsCharacterLimit"] as? String
            employeeId = dictionary["employeeId"] as? Int
            studentPromotionStatus = dictionary["studentPromotionStatus"] as? String
        }
        
        if(userRole == "CorporateManager")
        {
            corporateId = dictionary["corporateId"] as? Int
            corporateName = dictionary["corporateName"] as? String
        }
        
        if let ucanSwitch = dictionary["canSwitch"] as? Bool{
            if ucanSwitch{
                canSwitch = 1
            }else{
                canSwitch = 0
            }
        }
        
        if let ucanCreateGroup = dictionary["canCreateGroup"] as? Bool{
            if ucanCreateGroup{
                canCreateGroup = 1
            }else{
                canCreateGroup = 0
            }
        }
        
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        
        if adress != nil{
            dictionary["address"] = adress
        }
        
        if canSwitch != nil{
            dictionary["canSwitch"] = canSwitch
        }
        
        if userDesignation != nil{
            dictionary["designation"] = userDesignation
        }
        
        
        if id != nil{
            dictionary["id"] = id
        }
        
        
        if mobileNo != nil{
            dictionary["mobileNo"] = mobileNo
        }
        
        if name != nil{
            dictionary["name"] = name
        }
        
        if passwordChanged != nil{
            dictionary["passwordChanged"] = passwordChanged
        }
        
        if profilePic != nil{
            dictionary["profilePic"] = profilePic
        }
        
        if userRole != nil{
            dictionary["role"] = userRole
        }
        
        if switchAs != nil{
            dictionary["switchAs"] = switchAs
        }
        
        if userRoleId != nil{
            dictionary["userRoleId"] = userRoleId
        }
        if canCreateGroup != nil{
            dictionary["canCreateGroup"] = canCreateGroup
        }
        if audioAttachmentEnabled != nil{
            dictionary["audioAttachmentEnabled"] = audioAttachmentEnabled
        }
        if canComposeNotice != nil{
            dictionary["canComposeNotice"] = canComposeNotice
        }
        if canFeeDueListEnable != nil{
            dictionary["canFeeDueListEnable"] = canFeeDueListEnable
        }
        if isEnableContinuousEvaluation != nil{
            dictionary["isEnableContinuousEvaluation"] = isEnableContinuousEvaluation
        }
        
        
        
        if(userRole == "Employee")
        {
            if schoolId != nil{
                dictionary["schoolId"] = schoolId
            }
            if schoolName != nil{
                dictionary["schoolName"] = schoolName
            }
            if employeeType != nil{
                dictionary["employeeType"] = employeeType
            }
            if smsExceeded != nil{
                dictionary["smsExceeded"] = smsExceeded
            }
            
            if smsCharacterLimit != nil{
                dictionary["smsCharacterLimit"] = smsCharacterLimit
            }
            
            if employeeId != nil{
                dictionary["employeeId"] = employeeId
            }
            
            if studentPromotionStatus != nil{
                dictionary["studentPromotionStatus"] = studentPromotionStatus
            }
            
            
            
        }
        
        if(userRole == "CorporateManager")
        {
            if corporateId != nil{
                dictionary["corporateId"] = corporateId
            }
            if corporateName != nil{
                dictionary["corporateName"] = corporateName
            }
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        adress = aDecoder.decodeObject(forKey: "address") as? String
        canSwitch = aDecoder.decodeObject(forKey: "canSwitch") as? Int
        userDesignation = aDecoder.decodeObject(forKey: "designation") as? String!
        id = aDecoder.decodeObject(forKey: "id") as? Int
        mobileNo = aDecoder.decodeObject(forKey: "mobileNo") as? String
        name = aDecoder.decodeObject(forKey: "name") as? String
        passwordChanged = aDecoder.decodeObject(forKey: "passwordChanged") as? Int
        profilePic = aDecoder.decodeObject(forKey: "profilePic") as? String
        userRole = aDecoder.decodeObject(forKey: "role") as? String
        switchAs = aDecoder.decodeObject(forKey: "switchAs") as? String
        userRoleId = aDecoder.decodeObject(forKey: "userRoleId") as? Int
        if aDecoder.containsValue(forKey: "canCreateGroup"){
           canCreateGroup = aDecoder.decodeInteger(forKey: "canCreateGroup")
        }
        if aDecoder.containsValue(forKey: "audioAttachmentEnabled"){
           audioAttachmentEnabled = aDecoder.decodeBool(forKey: "audioAttachmentEnabled")
        }
        if aDecoder.containsValue(forKey: "canComposeNotice"){
            canComposeNotice = aDecoder.decodeBool(forKey: "canComposeNotice")
        }
        if aDecoder.containsValue(forKey: "canFeeDueListEnable"){
            canFeeDueListEnable  = aDecoder.decodeBool(forKey: "canFeeDueListEnable")
        }
        if aDecoder.containsValue(forKey: "isEnableContinuousEvaluation"){
            isEnableContinuousEvaluation = aDecoder.decodeBool(forKey: "isEnableContinuousEvaluation")
        }
        
        if(userRole == "Employee")
        {
            schoolId = aDecoder.decodeObject(forKey: "schoolId") as? Int
            schoolName = aDecoder.decodeObject(forKey: "schoolName") as? String
            employeeType = aDecoder.decodeObject(forKey: "employeeType") as? String
            smsExceeded = aDecoder.decodeObject(forKey: "smsExceeded") as? Int
            smsCharacterLimit = aDecoder.decodeObject(forKey: "smsCharacterLimit") as? String
            employeeId = aDecoder.decodeObject(forKey: "employeeId") as? Int
            studentPromotionStatus = aDecoder.decodeObject(forKey: "studentPromotionStatus") as? String
            
        }
        
        if(userRole == "CorporateManager")
        {
            
            corporateId = aDecoder.decodeObject(forKey: "corporateId") as? Int
            corporateName = aDecoder.decodeObject(forKey: "corporateName") as? String
        }
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if adress != nil{
            aCoder.encode(adress, forKey: "address")
        }
        
        if canSwitch != nil{
        
            aCoder.encode(canSwitch, forKey: "canSwitch")
        }
        
        if userDesignation != nil{
            aCoder.encode(userDesignation, forKey: "designation")
        }
        
        
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        
        
        if mobileNo != nil{
            aCoder.encode(mobileNo, forKey: "mobileNo")
        }
        
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        
        if passwordChanged != nil{
            aCoder.encode(passwordChanged, forKey: "passwordChanged")
        }
        
        if profilePic != nil{
            aCoder.encode(profilePic, forKey: "profilePic")
        }
        
        if userRole != nil{
            aCoder.encode(userRole, forKey: "role")
        }
        
        if switchAs != nil{
            aCoder.encode(switchAs, forKey: "switchAs")
        }
        
        if userRoleId != nil{
            aCoder.encode(userRoleId, forKey: "userRoleId")
        }
        if canCreateGroup != nil{
            aCoder.encode(canCreateGroup, forKey: "canCreateGroup")
        }
        
        if audioAttachmentEnabled != nil{
            aCoder.encode(audioAttachmentEnabled, forKey: "audioAttachmentEnabled")
        }
        
        if canComposeNotice != nil{
            aCoder.encode(canComposeNotice, forKey: "canComposeNotice")
        }
        
        if canFeeDueListEnable != nil{
            aCoder.encode(canFeeDueListEnable, forKey:"canFeeDueListEnable")
        }
        
        if isEnableContinuousEvaluation != nil{
            aCoder.encode(isEnableContinuousEvaluation, forKey : "isEnableContinuousEvaluation")
        }
        
        
        if(userRole == "Employee")
        {
            if schoolId != nil{
                aCoder.encode(schoolId, forKey: "schoolId")
            }
            if schoolName != nil{
                aCoder.encode(schoolName, forKey: "schoolName")
            }
            if employeeType != nil{
                aCoder.encode(employeeType, forKey: "employeeType")
            }
            if smsExceeded != nil{
                aCoder.encode(smsExceeded, forKey: "smsExceeded")
            }
            
            if smsCharacterLimit != nil{
                aCoder.encode(smsCharacterLimit, forKey: "smsCharacterLimit")
            }
            
            if employeeId != nil{
                aCoder.encode(employeeId, forKey: "employeeId")
            }
            
            if studentPromotionStatus != nil{
                aCoder.encode(studentPromotionStatus, forKey: "studentPromotionStatus")
            }
            
            
        }
        
        if(userRole == "CorporateManager")
        {
            if corporateId != nil{
                aCoder.encode(corporateId, forKey: "corporateId")
            }
            if corporateName != nil{
                aCoder.encode(corporateName, forKey: "corporateName")
            }
        }
        
    }
    
}
