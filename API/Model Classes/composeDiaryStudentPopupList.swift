//
//  composeDiaryStudentPopupList.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 22/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class composeDiaryStudentPopupList:NSObject{
    
    
    var studentId:Int   = -1
    var studentName:String     = ""
    var compressedProfilePic:String  = ""
    var selectedInUI = false
    var profilePic = ""
    var mobileNo = ""
    var isAbsent:Bool = false
    var offset = 0
    var classNameToDisplay:String = ""
    var schoolName:String = ""
    var rollNo = ""
    var result = ""
    var isEdit:Bool = false
    var addedByAdmin:Bool = false
    var academicYear:String = ""
    var className:String = ""
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atcID = dictionary[resultsJsonConstants.diaryStudentPopup.id] as? Int{
            self.studentId = atcID
        }else {
            self.studentId = -1
        }
        
        
      if let stdname = dictionary[resultsJsonConstants.diaryStudentPopup.name] as? String{
        self.studentName = stdname
      }else {
        self.studentName = ""
       }
        
      if let stdclassName = dictionary[resultsJsonConstants.diaryStudentPopup.className] as? String{
            self.className = stdclassName
        }else {
            self.className = ""
      }
        
    if let compopic = dictionary[resultsJsonConstants.diaryStudentPopup.compressedProfilePic] as? String{
                self.compressedProfilePic = compopic
            }else {
                self.compressedProfilePic = ""
            }
            
    
    
       if let comppic = dictionary[resultsJsonConstants.diaryStudentPopup.profilePic] as? String{
        self.profilePic = comppic
       }else {
        self.profilePic = ""
       }
        
        if let comppic = dictionary[resultsJsonConstants.diaryStudentPopup.mobileNo] as? String{
            self.mobileNo = comppic
        }else {
            self.mobileNo = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.diaryStudentPopup.isAbsent] as? Bool{
            self.isAbsent = comppic
        }else {
            self.isAbsent = false
        }
        
       if let comppic = dictionary[resultsJsonConstants.diaryStudentPopup.offset] as? Int{
            self.offset = comppic
        }else {
            self.offset = 0
        }
        
        if let cclassNameToDisplay = dictionary[resultsJsonConstants.diaryStudentPopup.classNameToDisplay] as? String{
            self.classNameToDisplay = cclassNameToDisplay
        }else {
            self.classNameToDisplay = ""
        }
        if let cschoolName = dictionary[resultsJsonConstants.diaryStudentPopup.schoolName] as? String{
            self.schoolName = cschoolName
        }else {
            self.schoolName = ""
        }
	    if let compRollNo = dictionary[resultsJsonConstants.diaryStudentPopup.rollNo] as? String{
            self.rollNo = compRollNo
        }else {
            self.rollNo = ""
        }
        if let cresult = dictionary[resultsJsonConstants.diaryStudentPopup.result] as? String{
            self.result = cresult
        }else {
            self.result = ""
        }
        if let caddedByAdmin = dictionary[resultsJsonConstants.diaryStudentPopup.addedByAdmin] as? Bool{
            self.addedByAdmin = caddedByAdmin
        }else {
            self.addedByAdmin = false
        }
        if let cacademicYear = dictionary[resultsJsonConstants.diaryStudentPopup.academicYear] as? String{
            self.academicYear = cacademicYear
        }else {
            self.academicYear = ""
        }
        
    }
    
    

}

class  parseStudentList {
    class func parseThisDictionaryForStudentPopup(dictionary:[String:AnyObject])->([composeDiaryStudentPopupList]){
        
        var studentListArray = [composeDiaryStudentPopupList]()
        
        if let studentList = dictionary[resultsJsonConstants.diaryStudentPopup.studentList] as? [AnyObject]{
            for dicto in studentList{// parse the array in the root
                if let msg = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                {
                    
               if let atcObj = composeDiaryStudentPopupList(withDictionaryFromWebService: msg){
                 studentListArray.append(atcObj)
                  }
                }
            }
        }
        
        return(studentListArray)
    }
    
    
}




