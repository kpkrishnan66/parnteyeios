//
//  AttachmentModel.swift
//  parentEye
//
//  Created by Martin Jacob on 24/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

class AttachmentModel {
    var name = ""
    var url = ""
    var type = ""
    var status = true
    var uniqueName = ""
}
