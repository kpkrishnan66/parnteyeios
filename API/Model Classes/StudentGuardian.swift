//
//	StudentGuardian.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class StudentGuardian : NSObject, NSCoding{

	var admissionNo : String!
    var adress : String!
    var classId : Int!
    var className : String!
    var classTeacher : String!
    var delayReport : String?
    var dob : String!
    var id : Int!
    var mobNo : String?
    var name : String!
    var parentName : String!
    var passportNo : String?
    var profilePic : String!
    var schoolId : Int?
    var schoolName : String!
	var transportType : String!
    var busid:Int!
    var busNO:String!
    var busContact:String!
    var busPlate:String!
    var leaveReasonList = [String]()
    var leaveDescription :String!
    var feePayment : String!
    var showOnlyProgressCard:Int!
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		admissionNo = dictionary["AdmnNo"] as? String
        adress = dictionary["address"] as? String
        classId = dictionary["classId"] as? Int
        className = dictionary["className"] as? String
        classTeacher = dictionary["classTeacher"] as? String
        delayReport = dictionary["delayReport"] as? String
        dob = dictionary["dob"] as? String
        id = dictionary["id"] as? Int
        mobNo = dictionary["mobileNo"] as? String
        name = dictionary["name"] as? String
        parentName = dictionary["parentName"] as? String
        passportNo = dictionary["passportNo"] as? String
        profilePic = dictionary["profilePic"] as? String
        schoolId = dictionary["schoolId"] as? Int
        schoolName = dictionary["schoolName"] as? String
		transportType = dictionary["transportType"] as? String
        busid = dictionary["busId"] as? Int
        busNO = dictionary["busNo"] as? String
        busContact = dictionary["contactNo"] as? String
        busPlate = dictionary["regNo"] as? String
        
        if let configArray = dictionary["configItems"] as? [String:AnyObject]{
            if let leaveReasonListArray = configArray["leaveReasonList"] as? [AnyObject]{
                for dico in leaveReasonListArray{
                    leaveReasonList.append(dico as! String)
                }
            }
            if let leaveDes = configArray["leaveDescription"] as? String{
                leaveDescription = leaveDes
            }
            if let fee = configArray["feePayment"] as? String{
                feePayment = fee
            }
            if let showpc = configArray["showOnlyProgressCard"] as? Int{
                showOnlyProgressCard = showpc
            }
            
        }

	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
        
		if admissionNo != nil{
			dictionary["AdmnNo"] = admissionNo
		}
        
        if adress != nil{
            dictionary["address"] = adress
        }
        
        if classId != nil{
            dictionary["classId"] = classId
        }
        
        if className != nil{
            dictionary["className"] = transportType
        }
        
        if classTeacher != nil{
            dictionary["classTeacher"] = classTeacher
        }
        
        if delayReport != nil{
            dictionary["delayReport"] = delayReport
        }
        
		if dob != nil{
			dictionary["dob"] = dob
		}
        
		if id != nil{
			dictionary["id"] = id
		}
        
        if mobNo != nil{
            dictionary["mobileNo"] = mobNo
        }
        
		if name != nil{
			dictionary["name"] = name
		}
        
        if parentName != nil{
            dictionary["parentName"] = parentName
        }
        
        if passportNo != nil{
            dictionary["passportNo"] = passportNo
        }
        
        
		if profilePic != nil{
			dictionary["profilePic"] = profilePic
		}
        
        if schoolId != nil{
            dictionary["schoolId"] = schoolId
        }
        
        if schoolName != nil{
            dictionary["schoolName"] = schoolName
        }
        
		if transportType != nil{
			dictionary["transportType"] = transportType
		}
        if busid != nil{
            dictionary["busId"] = busid
        }
        if busNO != nil{
            dictionary["busNo"] = busNO
            
        }
        if busContact != nil{
            dictionary["contactNo"] = busContact
            
        }
        if busPlate != nil{
            dictionary["regNo"] = busPlate
            
        }
        if leaveDescription != nil{
            dictionary["leaveDescription"] = leaveDescription
            
        }
        
        if feePayment != nil{
            dictionary["feePayment"] = feePayment
            
        }
        if showOnlyProgressCard != nil{
            dictionary["showOnlyProgressCard"] = showOnlyProgressCard
            
        }
        
        if !leaveReasonList .isEmpty{
            dictionary["leaveReasonList"] = leaveReasonList
            
        }
        
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        
        
         admissionNo = aDecoder.decodeObject(forKey:"AdmnNo") as? String
         adress = aDecoder.decodeObject(forKey:"address") as? String
         classId = aDecoder.decodeObject(forKey:"classId") as? Int
         className = aDecoder.decodeObject(forKey:"className") as? String
         classTeacher = aDecoder.decodeObject(forKey:"classTeacher") as? String
         delayReport = aDecoder.decodeObject(forKey:"delayReport") as? String
         dob = aDecoder.decodeObject(forKey:"dob") as? String
         id = aDecoder.decodeObject(forKey:"id") as? Int
         mobNo = aDecoder.decodeObject(forKey:"mobileNo") as? String
         name = aDecoder.decodeObject(forKey:"name") as? String
         parentName = aDecoder.decodeObject(forKey:"parentName") as? String
         passportNo = aDecoder.decodeObject(forKey:"passportNo") as? String
         profilePic = aDecoder.decodeObject(forKey:"profilePic") as? String
         schoolId = aDecoder.decodeObject(forKey:"schoolId") as? Int
         schoolName = aDecoder.decodeObject(forKey:"schoolName") as? String
         transportType = aDecoder.decodeObject(forKey:"transportType") as? String
         if aDecoder.containsValue(forKey: "busId"){
           busid = aDecoder.decodeInteger(forKey:"busId")
         }
         busNO = aDecoder.decodeObject(forKey:"busNo") as? String
         busContact = aDecoder.decodeObject(forKey:"contactNo") as? String
         busPlate = aDecoder.decodeObject(forKey:"regNo") as? String
        
        leaveReasonList = (aDecoder.decodeObject(forKey:"leaveReasonList") as? [String]!)!
        leaveDescription = aDecoder.decodeObject(forKey:"leaveDescription") as? String
        if let fee = aDecoder.decodeObject(forKey:"feePayment") as? String {
            self.feePayment = fee
        }
        if aDecoder.containsValue(forKey: "showOnlyProgressCard"){
           self.showOnlyProgressCard = aDecoder.decodeObject(forKey:"showOnlyProgressCard") as? Int
        }
        
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if admissionNo != nil{
			aCoder.encode(admissionNo, forKey: "AdmnNo")
		}
        
        if adress != nil{
            aCoder.encode(adress, forKey: "address")
        }
        
        if classId != nil{
            aCoder.encode(classId, forKey: "classId")
        }
        
        if className != nil{
            aCoder.encode(className, forKey: "className")
        }

        if classTeacher != nil{
            aCoder.encode(classTeacher, forKey: "classTeacher")
        }

        
        if delayReport != nil{
            aCoder.encode(delayReport, forKey: "delayReport")
        }
        
        if dob != nil{
            aCoder.encode(dob, forKey: "dob")
        }
        
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        
        if mobNo != nil{
            aCoder.encode(mobNo, forKey: "mobileNo")
        }
        
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
        
        if parentName != nil{
            aCoder.encode(parentName, forKey: "parentName")
        }
        
        if passportNo != nil{
            aCoder.encode(passportNo, forKey: "passportNo")
        }
        
        if profilePic != nil{
			aCoder.encode(profilePic, forKey: "profilePic")
		}
        
        if schoolId != nil{
            aCoder.encode(schoolId, forKey: "schoolId")
        }
        
        if schoolName != nil{
            aCoder.encode(schoolName, forKey: "schoolName")
        }
        
        if transportType != nil{
			aCoder.encode(transportType, forKey: "transportType")
		}
        if busid != nil{
            aCoder.encode(busid, forKey: "busId")
        }
        if busNO != nil{
            aCoder.encode(busNO, forKey: "busNo")
        }
        if busContact != nil{
            aCoder.encode(busContact, forKey: "contactNo")
        }
        if busPlate != nil{
            aCoder.encode(busPlate, forKey: "regNo")
        }
        if !leaveReasonList.isEmpty{
            aCoder.encode(leaveReasonList, forKey: "leaveReasonList")
        }
        if leaveDescription != nil{
            aCoder.encode(leaveDescription, forKey: "leaveDescription")
        }
        if feePayment != nil{
            aCoder.encode(feePayment, forKey: "feePayment")
        }
        if showOnlyProgressCard != nil{
            aCoder.encode(showOnlyProgressCard, forKey: "showOnlyProgressCard")
        }
        
	}

}
