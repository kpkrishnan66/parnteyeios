//
//  DiaryModelClass.swift
//  parentEye
//
//  Created by Martin Jacob on 26/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

class DiaryModelClass {
    
    var dayString            = ""//String to store the day
    var monthString          = ""//String to store the  Month
    var yearString           = ""//String to store the year
    var dateString           = ""//String to store the date
    var subject              = ""//String to store the title
    var from                 = ""//String to store whoom added diary
    var hasAttachement       = false// shows whether the diary entry has attachment
    var hasPdfAttachment     = false // Shows whether we have pdf attchemtn
    var contentString        = ""//String to store the content
    var isRead               = false// shows where the diary read
    var isExpanded           = false// is expanded shows where the diary entry is expanded in tablewview
    var hasImage             = false// shows whether the entry has images
    var imageArray           = [String]()
    var pdfArray             = [PdfModel]()
    var AudioArray           = [AudioModel]()
    var diaryId              = "-1"
    var studentID            = "-1"// need to create a method by which we can get student data from db
    var attachmentUrl        = ""
    var diaryReadUnreadId    = ""
    var recipientDetails     = ""
    var readVia              = ""
    
    var attachmentArray      = [uploadAttachmentModel]()
    
    var deliveryReportId               = 0
    var deliveryReportstudentName      = ""
    var deliveryReportmobileNo         = ""
    var deliveryReportclassName        = ""
    var deliveryReportprofilePic       = ""
    var deliveryReportstudentId        = 0
    var deliveryReportaddedTime        = ""
    var deliveryReportaddedDate        = ""
    var deliveryReportupdatedTime      = ""
    var deliveryReportupdatedDate      = ""
    var deliveryReportstatus           = ""
    
    var diaryClassId        = 0
    var diaryclassName      = ""
    /**
     Initializtion from dictinary we get from web service
     
     - parameter dictionary: ditionary
     
     - returns: nil if certain key keys are not there . Othewise returns self
     */
    init?(fromDictionaryFromWebService dictionary:[String:AnyObject], assignDateValue parsedate:String){
        
        var result  = true
        
        
        if let sts = dictionary[resultsJsonConstants.diaryResultsConstants.diaryEntryReadStatusKey] as? String{
            if sts == resultsJsonConstants.diaryResultsConstants.diaryEntryReadStatus{
                isRead = false
            }else {
                isRead = true
            }
        }
        
        // Getting diary details
        
        
        if let diaryId = dictionary[resultsJsonConstants.diaryResultsConstants.diaryId] as? Int{
            self.diaryId = String(diaryId)
        }else {
            result = false
        }
        
        if let title = dictionary[resultsJsonConstants.diaryResultsConstants.diarySubject] as? String{
            self.subject = title
        }
        if let content = dictionary[resultsJsonConstants.diaryResultsConstants.diaryContent] as? String{
            self.contentString = content
        }
        
        if let recipientDetail = dictionary[resultsJsonConstants.diaryResultsConstants.recipientDetails] as? String{
            self.recipientDetails = recipientDetail
        }
        
        
        if let personName = dictionary[resultsJsonConstants.diaryResultsConstants.whomThisDiaryIsFrom] as? String{
            self.from = personName
        }
        
        if parsedate != ""{
            let dateStringArray = parsedate.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
        
           self.dateString = parsedate
           if  dateStringArray.count >= 3{
            dayString = dateStringArray[0]
            monthString = dateStringArray[1]
            yearString = dateStringArray[2]
           }
        }
        
        
        
        
        if let attChments = dictionary[resultsJsonConstants.diaryResultsConstants.attachmentArray] as? [[String:AnyObject]]{
            
            for dict in attChments {
                if let attchmnt =  dict[resultsJsonConstants.diaryResultsConstants.attachment] as? [String:AnyObject]{
                    
                    let attachment = uploadAttachmentModel(withDictionaryFromWebServiceForMessage: dict)
                    self.attachmentArray.append(attachment!)
                    
                    if let imgurl = attchmnt[resultsJsonConstants.diaryResultsConstants.attachmentImageUrl] as? String{
                        
                        if let type = attchmnt[resultsJsonConstants.diaryResultsConstants.attachmentTypeKey] as? String {
                            
                            if type == resultsJsonConstants.diaryResultsConstants.attachmentTypeImage {
                                self.imageArray.append(imgurl)
                            }
                            else if type == resultsJsonConstants.diaryResultsConstants.audioType {
                                let audio = AudioModel(fromDictionary: attchmnt)
                                AudioArray.append(audio)
                            }
                            else {
                                self.hasPdfAttachment = true
                                let pdf = PdfModel(fromDictionary: attchmnt)
                                pdfArray.append(pdf)
                            }
                            
                        }
                    }
                }
                self.hasAttachement = true
                
            }
        }
        
        
        
        if result == false{
            return nil
        }
    }
    
    
    init?(fromDictionaryFromWebServiceForDetailedView dictionary:[String:AnyObject]){
        
        var result  = true
        
        if let delreportId = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportId] as? Int{
            self.deliveryReportId = delreportId
        }
        else{
            result = false
        }
        
        if let delreportName = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportstudentName] as? String{
            self.deliveryReportstudentName = delreportName
        }
        
        if let delreportmobno = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportmobileNo] as? String{
            self.deliveryReportmobileNo = delreportmobno
        }
        if let delreportclsname = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportclassName] as? String{
            self.deliveryReportclassName = delreportclsname
        }
        
        if let delreportprf = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportprofilePic] as? String{
            self.deliveryReportprofilePic = delreportprf
        }
        
        if let delreportstdId = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportstudentId] as? Int{
            self.deliveryReportstudentId = delreportstdId
        }
        
        if let delreportaddedTime = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportaddedTime] as? String{
            self.deliveryReportaddedTime = delreportaddedTime
        }
        
        if let delreportaddedDate = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportaddedDate] as? String{
            self.deliveryReportaddedDate = delreportaddedDate
        }
        
        if let delreportuptime = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportupdatedTime] as? String{
            self.deliveryReportupdatedTime = delreportuptime
        }
        
        if let delreportupdate = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportupdatedDate] as? String{
            self.deliveryReportupdatedDate = delreportupdate
        }
        
        if let delreportsts = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportstatus] as? String{
            self.deliveryReportstatus = delreportsts
        }
        
        if let diaryDetailsReadVia = dictionary[resultsJsonConstants.diaryResultsConstants.deliveryReportreadvia] as? String{
            self.readVia = diaryDetailsReadVia
        }
        
        if result == false{
            return nil
        }
        
    }
    
    init?(fromDictionaryFromWebServiceForDetailedClassListView dictionary:[String:AnyObject]){
        
        var result  = true
        
        if let diaclassid = dictionary[resultsJsonConstants.diaryResultsConstants.diaryClassId] as? Int{
            self.diaryClassId = diaclassid
        }
        else{
            result = false
        }
        
        if let diaclassname = dictionary[resultsJsonConstants.diaryResultsConstants.diaryClassName] as? String{
            self.diaryclassName = diaclassname
        }
        if result == false{
            return nil
        }
        
    }
    
}

class parseDiary {
    
    class func parseFromDictionary(dictionary:[String:AnyObject]) ->([String:[DiaryModelClass]],NSError?) {
        
        var dateSortedDictionary  = [String:[DiaryModelClass]]()
        var error:NSError?
        
        if let studentDiaries = dictionary[resultsJsonConstants.diaryResultsConstants.studentDiary] as? [String:AnyObject]{
            for (key,dateSeparatedDiary) in studentDiaries{
                if let diaryArray = dateSeparatedDiary as? [[String:AnyObject]]{
                    var dateSortedArray = [DiaryModelClass]()
                    
                    for studentAndDiary in diaryArray {
                        if let diary = DiaryModelClass(fromDictionaryFromWebService: studentAndDiary,assignDateValue: key){
                            dateSortedArray.append(diary)
                        }
                    }
                    dateSortedDictionary[key] = dateSortedArray
                }
            }
        }else {
            error =  NSError(domain: "s", code: 1, userInfo:[NSLocalizedDescriptionKey:ConstantsInUI.noResults])
        }
        
        return (dateSortedDictionary,error)
    }
    
    
    
    class func parseFromDictionaryForDetailedDeliveryReport(dictionary:[String:AnyObject])->([DiaryModelClass]){
        
        var diaryDetailedArray = [DiaryModelClass]()
        
        
        if let diaryList = dictionary[resultsJsonConstants.diaryResultsConstants.studentDiary] as? [String:AnyObject]{// get the root key
            
            if let diaryDetailedList = diaryList[resultsJsonConstants.diaryResultsConstants.deliveryReport] as? [AnyObject]{// get the root key
                
                for dicto in diaryDetailedList{// parse the array in the root
                    if let diary = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                    {
                        if let diaryObj = DiaryModelClass(fromDictionaryFromWebServiceForDetailedView: diary){
                            diaryDetailedArray.append(diaryObj)
                        }
                    }
                }
            }
        }
        
        
        return(diaryDetailedArray)
    }
    
    class func parseFromDictionaryForDetailedClassList(dictionary:[String:AnyObject])->([DiaryModelClass]){
        
        var diaryDetailedArray = [DiaryModelClass]()
        
        if let diaryList = dictionary[resultsJsonConstants.diaryResultsConstants.studentDiary] as? [String:AnyObject]{// get the root key
            
            if let diaryDetailedList = diaryList[resultsJsonConstants.diaryResultsConstants.classList] as? [AnyObject]{// get the root key
                
                for dicto in diaryDetailedList{// parse the array in the root
                    if let diary = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                    {
                        if let diaryObj = DiaryModelClass(fromDictionaryFromWebServiceForDetailedClassListView: diary){
                            diaryDetailedArray.append(diaryObj)
                        }
                    }
                }
            }
        }
        
        
        return(diaryDetailedArray)
    }
    
    class func parseFromDictionaryForDetailedSelectedClass(dictionary:[String:AnyObject])->([DiaryModelClass]){
        
        var diaryDetailedArray = [DiaryModelClass]()
        
        
        if let diaryList = dictionary[resultsJsonConstants.diaryResultsConstants.studentDiary] as? [AnyObject]{// get the root key
            
            
            for dicto in diaryList{// parse the array in the root
                if let diary = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                {
                    if let diaryObj = DiaryModelClass(fromDictionaryFromWebServiceForDetailedView: diary){
                        diaryDetailedArray.append(diaryObj)
                    }
                }
                
            }
        }
        return(diaryDetailedArray)
    }
}
