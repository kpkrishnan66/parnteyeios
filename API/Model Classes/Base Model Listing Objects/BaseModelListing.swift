//
//  BaseModelListing.swift
//  parentEye
//
//  Created by Martin Jacob on 06/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//


//TODO: - need to make all the listing come under this class. But that means alterting some code done as of 6-may-2016. Now adding the three classes of notice ,message and diary as properties.
import Foundation

 enum typeOfList {
    case diary
    case notice
    case homeWork
    case classTest
    case message
    case StudentLeaveRecord
    case StudentLeaveSubmit
    case externalNotification
    case none
}

class BaseModelListing {
    // Designates which type of list is this. Will be used in Upcoming event listing
    var entryType:typeOfList?
    var diary:DiaryModelClass?
    var homeworks:HomeWorkModel?
    var notices:NotificationModel?
    var diaries:DiaryModelClass?
    var classTests:classTest?
    var title = ""
    var day   = ""
    var month = ""
    var year  = ""
    
    
    //TODO:-  Make this a generic class so that we could add entries other than the three type specified.
    init?(fromDictionary dictionary:[String:AnyObject], withType type:String){
        var result = true
        
        switch type {
        case resultsJsonConstants.upComingEventsConstants.eventTypeDiary:
                entryType = typeOfList.diary
          
        case resultsJsonConstants.upComingEventsConstants.eventTypeHomework:
                entryType = typeOfList.homeWork
           
        case resultsJsonConstants.upComingEventsConstants.eventTypeNotice:
            entryType = typeOfList.notice
        default:
            entryType = typeOfList.none
            result = false
        }
       
        if let titleOfEvent = dictionary[resultsJsonConstants.upComingEventsConstants.titleOfEvent] as? String{
            title = titleOfEvent
        }else {
            result = false
        }
        var dateTempString = "---"
        if let dateStr = dictionary[resultsJsonConstants.upComingEventsConstants.DateOfEvent] as? String{
            dateTempString = dateStr
        }
        let dateStringArray = dateTempString.components(separatedBy: "-")
        if  dateStringArray.count >= 3{
            day = dateStringArray[0]
            month = dateStringArray[1]
            year = dateStringArray[2]
        }
        
        if !result {
            return nil
        }
        
    }
    
    init?(fromEventDicto event:[String:AnyObject]){
        var result = true
        
        if let typeOfEntry = event[resultsJsonConstants.upComingEventsConstants.typeOfListEntry] as? String{
            switch typeOfEntry {
            case resultsJsonConstants.upComingEventsConstants.eventTypeDiary:
                entryType = typeOfList.diary
            
            case resultsJsonConstants.upComingEventsConstants.eventTypeHomework:
                entryType = typeOfList.homeWork
                
                if let homeworkObject = event[resultsJsonConstants.upComingEventsConstants.eventObject] as? [String:AnyObject]{
                    
                    
                    var homeworkId = ""
                    
                    if let dt = event[resultsJsonConstants.homeWorkConstants.id] as? Int{
                        homeworkId = String(dt)
                    }
                    let homework = HomeWorkModel(fromEventHomeworks: homeworkObject)
                    homework!.id = homeworkId
                    homeworks = homework
                    
                }
                
            case resultsJsonConstants.upComingEventsConstants.eventTypeNotice:
                entryType = typeOfList.notice
                if let noticeObject = event[resultsJsonConstants.upComingEventsConstants.eventObject] as? [String:AnyObject]{

                    
                    var noticeId = ""
                    
                    if let dt = event[resultsJsonConstants.notificationConstants.noticeID] as? Int{
                        noticeId = String(dt)
                    }
                    let notice = NotificationModel(fromEventNotices: noticeObject)
                    notice.id = noticeId
                    notices = notice
                    debugPrint(notice)
                }
                
            case resultsJsonConstants.upComingEventsConstants.eventTypeClassTest:
                entryType = typeOfList.classTest
                if let classTestObject = event[resultsJsonConstants.upComingEventsConstants.eventObject] as? [String:AnyObject]{
                    var classTestId:Int = 0
                    if let dt = event[resultsJsonConstants.classTest.id] as? Int{
                        classTestId = dt
                    }
                    let classTestVal = classTest(withDictionaryFromWebService: classTestObject)
                    classTestVal!.id = classTestId
                    classTests = classTestVal
                    debugPrint(classTest())
                }

            default:
                entryType = typeOfList.none
                result    = false
            }
        }else {
            result = false
        }
        
        if let titleOfEvent = event[resultsJsonConstants.upComingEventsConstants.titleOfEvent] as? String{
            title = titleOfEvent
        }else {
            result = false
        }
        var dateTempString = "---"
        if let dateStr = event[resultsJsonConstants.upComingEventsConstants.DateOfEvent] as? String{
            dateTempString = dateStr
        }
        let dateStringArray = dateTempString.components(separatedBy: "-")
        if  dateStringArray.count >= 3{
            day = dateStringArray[0]
            month = dateStringArray[1]
            year = dateStringArray[2]
        }
        
        if !result {
            return nil
        }
    }
    
    
    class func makeBaseClassForListingFromDictionary(dictionary:[String:AnyObject])-> [BaseModelListing]{
        var baseModelArray = [BaseModelListing]()
        
        if let eventList = dictionary[resultsJsonConstants.upComingEventsConstants.upComingEventsList] as? [[String:AnyObject]]{
            
            for eventEntry  in eventList {
                if let obj =  BaseModelListing.init(fromEventDicto: eventEntry){
                  baseModelArray.append(obj)
                }
            }
        }
        
       return baseModelArray
    }
}
