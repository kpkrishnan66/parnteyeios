//
//  PdfModel.swift
//  parentEye
//
//  Created by Martin Jacob on 24/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class PdfModel:AttachmentModel {
    
    init(fromDictionary dictionary:[String:AnyObject]){
        super.init()
        if let pdfName  =  dictionary[resultsJsonConstants.diaryResultsConstants.uniqueNameKey] as? String{
            name = pdfName
        }
        if let pdfUrl =  dictionary[resultsJsonConstants.diaryResultsConstants.attachmentImageUrl] as? String {
            url = pdfUrl
        }
        if let pdfType =  dictionary[resultsJsonConstants.diaryResultsConstants.attachmentTypeKey] as? String {
            type = pdfType
        }
        
        
    }
    
}
