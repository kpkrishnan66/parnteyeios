//
//  MessageModelForListing.swift
//  parentEye
//
//  Created by Martin Jacob on 18/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class messageForListing {
    
    //TODO: Make a user class so that the name , profile pic can be tranferred to that class
    
    let title:String
    let content:String
    let messageHasReply:Bool
    let designation:String
    let name:String
    var isRead:Bool
    let date:String
    let ID:String
    let threadId:String
    let senderProfilePic:String
    var theOtherGuysId = ""
    var masterMessageSenderId:String
    let toFromLabel:String
    var messageType:String
    var userm = [userModel]()
    //let hasBeenProperlyInitilized:Bool
    var imageArray           = [String]()
    var pdfArray             = [PdfModel]()
    var AudioArray           = [AudioModel]()
    var hasAttachement       = false
    var hasPdfAttachment     = false
    var attachmentArray      = [uploadAttachmentModel]()
    let messageCanReply:Bool
    
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject],readViaPush:Bool){
        
        
        isRead = true
        messageType = ""
        let newUer = DBController().getUserFromDB()
        masterMessageSenderId = (newUer?.currentUserRoleId)!
        
        
        if let msgID = dictionary[resultsJsonConstants.messageResultContants.messageId] as? Int{
            self.ID = String(msgID)
        }else {
            self.ID = "-1"
        }
        
        if let data = dictionary[resultsJsonConstants.messageResultContants.messageContent] as? String {
            self.content = data
        }else {
            self.content = ""
        }
        
        if let heading = dictionary[resultsJsonConstants.messageResultContants.messageTitle] as? String {
            self.title = heading
        }else {
            self.title = ""
        }
        
        if let msgSendDate = dictionary[resultsJsonConstants.messageResultContants.messageDate] as? String{
            self.date = msgSendDate
        }else {
            self.date = ""
        }
        
        if let tId = dictionary[resultsJsonConstants.messageResultContants.messageThreadID] as? String{
            self.threadId = String(tId)
        }else {
            self.threadId = "-1"
        }
        
        if let msgHasReply = dictionary[resultsJsonConstants.messageResultContants.messageType] as? String {
            if msgHasReply.isEmpty{
                self.messageHasReply = false
                self.messageType = ""
            }else {
                self.messageHasReply = true
                self.messageType = msgHasReply
            }
            
            
        }else {
            self.messageHasReply = false
        }
        
        
        
        
        if let msgSenderName = dictionary[resultsJsonConstants.messageResultContants.messageSenderName] as? String {
            self.name = msgSenderName
        }else {
            self.name = ""
        }
        
        if let msgSenderDesigntion = dictionary[resultsJsonConstants.messageResultContants.messageSenderDesignation] as? String {
            self.designation = msgSenderDesigntion
        }else {
            self.designation = ""
        }
        
        if let msgSenderID = dictionary[resultsJsonConstants.messageResultContants.messageTheOtherGuysIDKey] as? Int {
            self.theOtherGuysId = String(msgSenderID)
        }else if let msgSenderID = dictionary[resultsJsonConstants.messageResultContants.messageTheOtherGuysIDKey] as? String {
            self.theOtherGuysId = msgSenderID
        }else if (readViaPush){
            if let msgSenderID = dictionary[resultsJsonConstants.messageResultContants.messageAddedById] as? Int {
                self.theOtherGuysId = String(msgSenderID)
            }
            if let msgSenderByID = dictionary[resultsJsonConstants.messageResultContants.messageAddedById] as? String {
                self.theOtherGuysId = msgSenderByID
            }
        }else{
             self.theOtherGuysId = ""
        }
        
        if let msgSenderProfilePIc = dictionary[resultsJsonConstants.messageResultContants.messageSenderProfilePIc] as? String {
            self.senderProfilePic = msgSenderProfilePIc
        }else {
            self.senderProfilePic = ""
        }
        
        if let readornot = dictionary[resultsJsonConstants.messageResultContants.messageIsReadOrnot] as? String {
            if readornot == "Read"{
                self.isRead = true
            }
            else{
                self.isRead = false
            }
        }
        
        if let msgToFromLabel = dictionary[resultsJsonConstants.messageResultContants.messageToFromLabel] as? String {
            self.toFromLabel = msgToFromLabel
        }else {
            self.toFromLabel = ""
        }
        
        if let attChments = dictionary[resultsJsonConstants.messageResultContants.attachmentArray] as? [[String:AnyObject]]{
            
            for dict in attChments {
                if let attchmnt =  dict[resultsJsonConstants.messageResultContants.attachment] as? [String:AnyObject]{
                    
                    let attachment = uploadAttachmentModel(withDictionaryFromWebServiceForMessage: dict)
                    self.attachmentArray.append(attachment!)
                    
                    if let imgurl = attchmnt[resultsJsonConstants.messageResultContants.attachmentImageUrl] as? String{
                        
                        if let type = attchmnt[resultsJsonConstants.messageResultContants.attachmentTypeKey] as? String {
                            
                            if type == resultsJsonConstants.messageResultContants.attachmentTypeImage {
                                self.imageArray.append(imgurl)
                            }
                            else if type == resultsJsonConstants.diaryResultsConstants.audioType {
                                let audio = AudioModel(fromDictionary: attchmnt)
                                AudioArray.append(audio)
                                
                            } else {
                                self.hasPdfAttachment = true
                                let pdf = PdfModel(fromDictionary: attchmnt)
                                pdfArray.append(pdf)
                            }
                            
                        }
                    }
                }
                self.hasAttachement = true
                
            }
        }
        
        if let msgCanReply = dictionary[resultsJsonConstants.messageResultContants.messageCanReply] as? Bool {
            self.messageCanReply = msgCanReply
        }else {
            self.messageCanReply = true
        }
        
        
    }
    
    //
    //    init(withTitle title:String?, withContent content:String?, typeOfMessage typemsg:String?, sendersDesignation designation:String?, nameOfSender name:String?, isThisMessageAlreadyRead isread:Bool?,dateOfMessageSending date:String?, idOfMessageIs id:String?){
    //
    //        if let heading = title {
    //            self.title = heading
    //        }else {
    //            self.title = ""
    //        }
    //
    //        if let data = content {
    //            self.content = data
    //        }else {
    //            self.content = ""
    //        }
    //
    //        if let msgType = typemsg {
    //            self.messageType = msgType
    //        }else {
    //            self.messageType = ""
    //        }
    //
    //        if let entitlement = designation {
    //            self.designation = entitlement
    //        }else {
    //            self.designation = ""
    //        }
    //
    //        if let fromWho = name {
    //            self.name = fromWho
    //        }else {
    //            self.name = ""
    //        }
    //
    //        if let alreadyRead = isread {
    //            self.isRead = alreadyRead
    //        }else {
    //            self.isRead = false
    //        }
    //
    //        if let msgSendDate = date{
    //            self.date = msgSendDate
    //        }else {
    //            self.date = ""
    //        }
    //
    //        if let msgID = id{
    //            self.ID = msgID
    //        }else {
    //            self.ID = "-1"
    //        }
    //    }
}

/*if let msgDictionary = msg[resultsJsonConstants.messageResultContants.messageF] as? [String:AnyObject]{
 
 if let msgObj = messageForListing(withDictinary: msgDictionary){
 messageArray.append(msgObj)
 }
 
 }*/

class  parseMessageListing {
    class func parseThisDictionaryForMessages(dictionary:[String:AnyObject])->([messageForListing]){
        
        var messageArray = [messageForListing]()
        
        
        if let msgRcpList = dictionary[resultsJsonConstants.messageResultContants.messageRecipientList] as? [AnyObject]{// get the root key
            
            for dicto in msgRcpList{// parse the array in the root
                if let msg = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                {
                    if let msgObj = messageForListing(withDictionaryFromWebService: msg,readViaPush: false){
                        messageArray.append(msgObj)
                    }
                }
            }
        }
        
        
        return(messageArray)
    }
}

