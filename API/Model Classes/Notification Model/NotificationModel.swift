//
//  NotificationModel.swift
//  parentEye
//
//  Created by Martin Jacob on 12/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

class NotificationModel {
    
    var dayString         = ""
    var monthString       = ""
    var yearString        = ""
    var lastUpdatedDate   = ""
    var title             = ""
    var addedbyName       = ""
    var content           = ""
    var hasReminder       = false
    var isUpComing        = false
    var hasAttachment     = false
    var id                = ""
    var imageArray        = [ModelImage]()
    var pdfArray          = [PdfModel]()
    var isread            = true
    
    
    init?(fromDictionaryFromWebService dictionary:[String:AnyObject], withDate dateString:String){
        var result  = true
        
            if let id = dictionary[resultsJsonConstants.notificationConstants.noticeID] as? Int{
                self.id = String(id)
            }else {
                result = false
            }
            
            if let ntitle = dictionary[resultsJsonConstants.notificationConstants.noticeTitle] as? String{
                title = ntitle
            }
            if let nContent = dictionary[resultsJsonConstants.notificationConstants.noticeContent] as? String{
                content = nContent
            }
            
            if let nUpcoming = dictionary[resultsJsonConstants.notificationConstants.noticeUpComing] as? Bool{
               isUpComing = nUpcoming
            }
            
            if let nLastupdatedOn = dictionary[resultsJsonConstants.notificationConstants.noticelastUpdatedOn] as? String{
                lastUpdatedDate = nLastupdatedOn
            }
        
           if dateString != ""{
            let dateStringArray = dateString.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
            if  dateStringArray.count >= 3{
                dayString   = dateStringArray[0]
                monthString = dateStringArray[1]
               yearString  = dateStringArray[2]
            }
           }else{
            if let neventDate = dictionary[resultsJsonConstants.notificationConstants.noticeEventDate] as? String{
                let eventDate = neventDate
                let dateStringArray = eventDate.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
                if  dateStringArray.count >= 3{
                    dayString   = dateStringArray[0]
                    monthString = dateStringArray[1]
                    yearString  = dateStringArray[2]
                }
            }
        }
        
        
            if let addedBY = dictionary[resultsJsonConstants.notificationConstants.notificationAddedBy] as? String{
            
                    addedbyName = addedBY
                }
        
            
               if let attachments = dictionary[resultsJsonConstants.notificationConstants.noticeAttachments] as? [[String:AnyObject]]{
                        let attac = NotificationModel.getAttachmentsFromDictionary(attachmentArrayDictioanry: attachments)
                        imageArray.append(contentsOf: attac.imageArray)
                        pdfArray.append(contentsOf: attac.pdfArray)
                        hasAttachment = false
                           if(!pdfArray.isEmpty || !imageArray.isEmpty){
                           hasAttachment = true
                         }
                           else{
                          hasAttachment=false
                        }
                
                
        }
            if result == false{
            return nil
        }
        
    }
    
    init(fromEventNotices notice:[String:AnyObject]){
        var result = true
        if let id = notice[resultsJsonConstants.notificationConstants.noticeID] as? Int{
            self.id = String(id)
        }else {
            result = false
        }
        if let ntitle = notice[resultsJsonConstants.notificationConstants.noticeTitle] as? String{
            title = ntitle
        }
        if let nContent = notice[resultsJsonConstants.notificationConstants.noticeContent] as? String{
            content = nContent
        }
        if let addedBY = notice[resultsJsonConstants.notificationConstants.notificationAddedBy] as? String{
            
                addedbyName = addedBY
        }
        if let nLastupdatedOn = notice[resultsJsonConstants.notificationConstants.noticelastUpdatedOn] as? String{
            lastUpdatedDate = nLastupdatedOn
        }
        
        
        
                if let attachments = notice[resultsJsonConstants.notificationConstants.noticeAttachments] as? [[String:AnyObject]]{
                    let attac = NotificationModel.getAttachmentsFromDictionary(attachmentArrayDictioanry: attachments)
                    imageArray.append(contentsOf: attac.imageArray)
                    pdfArray.append(contentsOf: attac.pdfArray)
                    if(!pdfArray.isEmpty || !imageArray.isEmpty){
                        hasAttachment = true
                    }
        
        }
        
        
        if let dateString = notice[resultsJsonConstants.notificationConstants.noticeDate] as? String{
            let dateStringArray = dateString.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
            if  dateStringArray.count >= 3{
                dayString   = dateStringArray[0]
                monthString = dateStringArray[1]
                yearString  = dateStringArray[2]
            }
        }
        
        
        
        
        if let nUpcoming = notice[resultsJsonConstants.notificationConstants.noticeUpComing] as? Bool{
            isUpComing = nUpcoming
        }
    }
    
    
    class func makeDictionaryOfNotificationsFromDictionary(dictionary:[String:AnyObject])-> ([String:[NotificationModel]],NSError?){
        
        var dateSortedDictionary = [String:[NotificationModel]]()
        var error:NSError?
        
        if let studentnotifications = dictionary[resultsJsonConstants.notificationConstants.noticeRecipient] as? [String:AnyObject]{
            for (key,dateSeparatednotification) in studentnotifications{
                if let notificationArray = dateSeparatednotification as? [[String:AnyObject]]{
                    var dateSortedArray = [NotificationModel]()
                    for studentAndnotification in notificationArray {
                        if let notification = NotificationModel.init(fromDictionaryFromWebService: studentAndnotification,withDate: key){
                            dateSortedArray.append(notification)
                        }
                    }
                    dateSortedDictionary[key] = dateSortedArray
                }
            }
        }else {
            
            error =  NSError(domain: "s", code: 1, userInfo:[NSLocalizedDescriptionKey:ConstantsInUI.serverReturnedNoData])
        }
        
        return (dateSortedDictionary,error)
    }
    
    class func  getImageArrayFromDictionary(attachmentDictioanry:[String:AnyObject])->[ModelImage]{
        var imageUrlStringArray = [ModelImage]()
        
        if let attachments = attachmentDictioanry[resultsJsonConstants.notificationConstants.noticeAttachmentForImage] as? [[String:AnyObject]]{
            
            for value in attachments {
                if let attachment = value[resultsJsonConstants.notificationConstants.noticeAttachment] as? [String:AnyObject]{
                    let image = ModelImage(fromDictionary: attachment)
                    imageUrlStringArray.append(image)
                }
            }
        }
        return imageUrlStringArray
    }
    
    class func getAttachmentsFromDictionary(attachmentArrayDictioanry:[[String:AnyObject]])->(imageArray:[ModelImage],pdfArray:[PdfModel]) {
        var arrayImage = [ModelImage]()
        var pdfArray  = [PdfModel]()
    
        
        
        for attachmentDict in attachmentArrayDictioanry {
            if let attachmentDict =  attachmentDict[resultsJsonConstants.notificationConstants.noticeAttachment] as? [String:AnyObject]{
               
                if let type = attachmentDict[resultsJsonConstants.notificationConstants.noticeTypeKey] as? String {
                    if type == resultsJsonConstants.notificationConstants.noticeTypeImage{
                        
                        let image = ModelImage(fromDictionary: attachmentDict)
                        arrayImage.append(image)
                    }else {
                        let pdf = PdfModel(fromDictionary: attachmentDict)
                        pdfArray.append(pdf)
                    }
                }
               
                
            }
            
        }
        
        return (arrayImage,pdfArray)
    }
    
}
