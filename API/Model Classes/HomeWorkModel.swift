//
//  HomeWorkModel.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 19/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//


import Foundation

class HomeWorkModel {
    
    var id               = "-1"
    var title            = ""
    var description      = ""
    var reference        = ""
    var from             = ""
    var subject          = ""
    var createdOn        = ""
    var submissionDate   = ""
    var attachments      = [AttachmentModel]()
    var recipientDetails = ""
    var status           = ""
    var imageArray       = [String]()
    var pdfArray         = [PdfModel]()
    var AudioArray       = [AudioModel]()
    var hasPdfAttachment = false // Shows whether we have pdf attchemtn
    var hasAttachement   = false// shows whether the diary entry has attachment
    var dayString            = ""//String to store the day
    var monthString          = ""//String to store the  Month
    var yearString           = ""//String to store the year
    
    
    
    init?(fromDictionaryFromWebService dictionary:[String:AnyObject], assignhomeWorkValue parseHomeWorkList:String){
        
        
        var result  = true
        
        
        if let homeWorkId = dictionary[resultsJsonConstants.homeWorkConstants.id] as? Int{
            self.id = String(homeWorkId)
        }else {
            result = false
        }
        
        if let homeWorkTitle = dictionary[resultsJsonConstants.homeWorkConstants.title] as? String{
            self.title = homeWorkTitle
        }else {
            title = ""
        }
        
        if let homeWorkdes = dictionary[resultsJsonConstants.homeWorkConstants.description] as? String{
            self.description = homeWorkdes
        }else {
            description = ""
        }
        
        if let homeWorkref = dictionary[resultsJsonConstants.homeWorkConstants.reference] as? String{
            self.reference = homeWorkref
        }else {
            reference = ""
        }
        
        if let homeWorkfrom = dictionary[resultsJsonConstants.homeWorkConstants.from] as? String{
            self.from = homeWorkfrom
        }else {
            from = ""
        }
        
        if let homeWorksub = dictionary[resultsJsonConstants.homeWorkConstants.subject] as? String{
            self.subject = homeWorksub
        }else {
            subject = ""
        }
        
        if let homeWorkcr = dictionary[resultsJsonConstants.homeWorkConstants.createdOn] as? String{
            self.createdOn = homeWorkcr
            if parseHomeWorkList != ""{
            let dateStringArray = parseHomeWorkList.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
            
            if  dateStringArray.count >= 3{
                dayString = dateStringArray[0]
                monthString = dateStringArray[1]
                yearString = dateStringArray[2]
            }
            }else{
                let dateStringArray = self.createdOn.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
                
                if  dateStringArray.count >= 3{
                    dayString = dateStringArray[0]
                    monthString = dateStringArray[1]
                    yearString = dateStringArray[2]
                }
            }
            
        }else {
            createdOn = ""
        }
        
        if let homeWorksub = dictionary[resultsJsonConstants.homeWorkConstants.submissionDate] as? String{
            self.submissionDate = homeWorksub
        }else {
            submissionDate = ""
        }
        
        if let homeWorkrecp = dictionary[resultsJsonConstants.homeWorkConstants.recipientDetails] as? String{
            self.recipientDetails = homeWorkrecp
        }else {
            recipientDetails = ""
        }
        
        if let homeWorkstatus = dictionary[resultsJsonConstants.homeWorkConstants.status] as? String{
            self.status = homeWorkstatus
        }else {
            status = ""
        }
        
        
        if let attChments = dictionary[resultsJsonConstants.homeWorkConstants.attachments] as? [[String:AnyObject]]{
            
            for dict in attChments {
                if let attchmnt =  dict[resultsJsonConstants.homeWorkConstants.attachment] as? [String:AnyObject]{
                    
                    if let imgurl = attchmnt[resultsJsonConstants.homeWorkConstants.url] as? String{
                        
                        if let type = attchmnt[resultsJsonConstants.homeWorkConstants.type] as? String {
                            
                            if type == resultsJsonConstants.homeWorkConstants.attachmentTypeImage {
                                self.imageArray.append(imgurl)
                            }
                            else if type == resultsJsonConstants.diaryResultsConstants.audioType {
                                let audio = AudioModel(fromDictionary: attchmnt)
                                AudioArray.append(audio)
                                
                            } else {
                                self.hasPdfAttachment = true
                                let pdf = PdfModel(fromDictionary: attchmnt)
                                pdfArray.append(pdf)
                            }
                            
                        }
                    }
                }
                self.hasAttachement = true
                
            }
        }
        if result == false{
            return nil
        }
        
        
        
    }
    
    init?(fromEventHomeworks dictionary:[String:AnyObject]){
        
        var result  = true
        
        
        if let homeWorkId = dictionary[resultsJsonConstants.homeWorkConstants.id] as? Int{
            self.id = String(homeWorkId)
        }else {
            result = false
        }
        
        if let homeWorkTitle = dictionary[resultsJsonConstants.homeWorkConstants.title] as? String{
            self.title = homeWorkTitle
        }else {
            title = ""
        }
        
        if let homeWorkdes = dictionary[resultsJsonConstants.homeWorkConstants.description] as? String{
            self.description = homeWorkdes
        }else {
            description = ""
        }
        
        if let homeWorkref = dictionary[resultsJsonConstants.homeWorkConstants.reference] as? String{
            self.reference = homeWorkref
        }else {
            reference = ""
        }
        
        if let homeWorkfrom = dictionary[resultsJsonConstants.homeWorkConstants.from] as? String{
            self.from = homeWorkfrom
        }else {
            from = ""
        }
        
        if let homeWorksub = dictionary[resultsJsonConstants.homeWorkConstants.subject] as? String{
            self.subject = homeWorksub
        }else {
            subject = ""
        }
        
        if let homeWorkcr = dictionary[resultsJsonConstants.homeWorkConstants.createdOn] as? String{
            self.createdOn = homeWorkcr
            let dateStringArray = homeWorkcr.components(separatedBy: resultsJsonConstants.diaryResultsConstants.diaryDateSeparator)
            
            if  dateStringArray.count >= 3{
                dayString = dateStringArray[0]
                monthString = dateStringArray[1]
                yearString = dateStringArray[2]
            }
            
        }else {
            createdOn = ""
        }
        
        if let homeWorksub = dictionary[resultsJsonConstants.homeWorkConstants.submissionDate] as? String{
            self.submissionDate = homeWorksub
        }else {
            submissionDate = ""
        }
        
        
        
        if let attChments = dictionary[resultsJsonConstants.homeWorkConstants.attachments] as? [[String:AnyObject]]{
            
            for dict in attChments {
                if let attchmnt =  dict[resultsJsonConstants.homeWorkConstants.attachment] as? [String:AnyObject]{
                    
                    if let imgurl = attchmnt[resultsJsonConstants.homeWorkConstants.url] as? String{
                        
                        if let type = attchmnt[resultsJsonConstants.homeWorkConstants.type] as? String {
                            
                            if type == resultsJsonConstants.homeWorkConstants.attachmentTypeImage {
                                self.imageArray.append(imgurl)
                            }else {
                                self.hasPdfAttachment = true
                                let pdf = PdfModel(fromDictionary: attchmnt)
                                pdfArray.append(pdf)
                            }
                            
                        }
                    }
                }
                self.hasAttachement = true
                
            }
        }
        if result == false{
            return nil
        }
        
        
        
    
    }
    
}

    class ParseHomeWork {
        
        class func parseFromDictionary(dictionary:[String:AnyObject]) ->([String:[HomeWorkModel]],NSError?) {
            
            var dateSortedDictionary  = [String:[HomeWorkModel]]()
            var error:NSError?
            
            if let studentHomeWorks = dictionary[resultsJsonConstants.homeWorkConstants.homework] as? [String:AnyObject]{
                for (key,dateSeparatedHomework) in studentHomeWorks{
                    if let diaryArray = dateSeparatedHomework as? [[String:AnyObject]]{
                        var dateSortedArray = [HomeWorkModel]()
                        
                        for studentAndHomeWork in diaryArray {
                            if let homeWork = HomeWorkModel(fromDictionaryFromWebService: studentAndHomeWork,assignhomeWorkValue: key){
                                dateSortedArray.append(homeWork)
                            }
                        }
                        dateSortedDictionary[key] = dateSortedArray
                    }
                }
            }else {
                error =  NSError(domain: "s", code: 1, userInfo:[NSLocalizedDescriptionKey:ConstantsInUI.noResults])
            }
            
            return (dateSortedDictionary,error)
        }
    }
    
