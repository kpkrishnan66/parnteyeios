//
//  ModelImage.swift
//  parentEye
//
//  Created by Martin Jacob on 24/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class ModelImage:AttachmentModel {

    init(fromDictionary dictionary:[String:AnyObject]){
        super.init()
        if let imgUrl = dictionary[resultsJsonConstants.notificationConstants.noticeImageUrl] as?String{
           url = imgUrl
        }
    }
}
