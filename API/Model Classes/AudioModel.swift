//
//  AudioModel.swift
//  parentEye
//
//  Created by scientia on 24/06/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class AudioModel:AttachmentModel {
    
    init(fromDictionary dictionary:[String:AnyObject]){
        super.init()
        if let audioName  =  dictionary[resultsJsonConstants.diaryResultsConstants.uniqueNameKey] as? String{
            name = audioName
        }
        if let audioUrl =  dictionary[resultsJsonConstants.diaryResultsConstants.attachmentImageUrl] as? String {
            url = audioUrl
        }
        if let audioType =  dictionary[resultsJsonConstants.diaryResultsConstants.attachmentTypeKey] as? String {
            type = audioType
        }
        
        
    }
    
}
