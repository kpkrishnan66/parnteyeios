//
//  classSubjectList.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 04/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class classSubjectList : NSObject, NSCoding{
    
    var classId : Int!
    var className : String!
    var subjectId : String!
    var subjectName : String!
    var classInChargeEmployeeId : Int!
    var classInCharge : String!

    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        
        classId = dictionary["classId"] as? Int
        className = dictionary["className"] as? String
        subjectId = dictionary["subjectId"] as? String
        subjectName = dictionary["subjectName"] as? String
        classInChargeEmployeeId = dictionary["classInChargeEmployeeId"] as? Int
        classInCharge = dictionary["classInCharge"] as? String
        
    }
    
    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        
        
        if classId != nil{
            dictionary["classId"] = classId
        }
        
        if className != nil{
            dictionary["className"] = className
        }
        if subjectId != nil{
            dictionary["subjectId"] = subjectId
        }
        
        if subjectName != nil{
            dictionary["subjectName"] = subjectName
        }
        
        if classInChargeEmployeeId != nil{
            dictionary["classInChargeEmployeeId"] = classInChargeEmployeeId
        }
        if classInCharge != nil{
            dictionary["classInCharge"] = classInCharge
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        
        classId = aDecoder.decodeObject(forKey:"classId") as? Int
        className = aDecoder.decodeObject(forKey:"className") as? String
        subjectId = aDecoder.decodeObject(forKey:"subjectId") as? String
        subjectName = aDecoder.decodeObject(forKey:"subjectName") as? String
        classInChargeEmployeeId = aDecoder.decodeObject(forKey:"classInChargeEmployeeId") as? Int
        classInCharge = aDecoder.decodeObject(forKey:"classInCharge") as? String
        
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if classId != nil{
            aCoder.encode(classId, forKey: "classId")
        }
        
        if className != nil{
            aCoder.encode(className, forKey: "className")
        }
        if subjectId != nil{
            aCoder.encode(subjectId, forKey: "subjectId")
        }
        
        if subjectName != nil{
            aCoder.encode(subjectName, forKey: "subjectName")
        }
        if classInChargeEmployeeId != nil{
            aCoder.encode(classInChargeEmployeeId, forKey: "classInChargeEmployeeId")
        }
        
        if classInCharge != nil{
            aCoder.encode(classInCharge, forKey: "classInCharge")
        }
        
        
        
    }
    
}
