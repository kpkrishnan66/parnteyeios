//
//	RootClass.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class RootClass : NSObject, NSCoding{

	var serverLatency : Int!
	var userdata : UserData!
    var failureReason: String?
    var studentList : [StudentGuardian]!
    var classList : [classSubjectList]!
    var schoolList: [corporateSchoolList]!
    
    /**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
    init?(fromDictionary dictionary: NSDictionary){
        if let userloginData = dictionary["user"] as? NSDictionary{
            userdata = UserData(fromDictionary: userloginData)
        
          
        
        if let studentGuardianArray = userloginData["studentList"] as? [[String:AnyObject]]{
            studentList = [StudentGuardian]()
            for dic in studentGuardianArray{
                let value = StudentGuardian(fromDictionary: dic as NSDictionary)
                studentList.append(value)
            }
            
        }
        else {
            
            if let classSubjectListArray = userloginData["classSubjectList"] as? [[String:AnyObject]]{
                classList = [classSubjectList]()
                for dic in classSubjectListArray{
                    let value = classSubjectList(fromDictionary: dic as NSDictionary)
                    classList.append(value)
                }
            }
           if let schoolListArray = userloginData["schoolList"] as? [[String:AnyObject]]{
            schoolList = [corporateSchoolList]()
             for dic in schoolListArray{
                let value = corporateSchoolList(fromDictionary: dic as NSDictionary)
                schoolList.append(value)
             }
           }
            
        }
      }
       
       
        else
        {
           if let errorDict = dictionary["error"] as? NSDictionary{
                if let erMsg =  errorDict["errorMessage"] as? NSString{
                    self.failureReason = erMsg as String
                }
           }
        }
        
    }
    

    
	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if serverLatency != nil{
			dictionary["serverLatency"] = serverLatency
		}
		if userdata != nil{
			dictionary["user"] = userdata.toDictionary()
		}
        
        if studentList != nil{
            var dictionaryElements = [NSDictionary]()
            for studentGuardianElement in studentList {
                dictionaryElements.append(studentGuardianElement.toDictionary())
            }
            dictionary["studentGuardian"] = dictionaryElements
        }
        
        if classList != nil{
            var dictionaryElements = [NSDictionary]()
            for employeeClassElement in classList {
                dictionaryElements.append(employeeClassElement.toDictionary())
            }
            dictionary["classSubjectList"] = dictionaryElements
        }
        
        if schoolList != nil{
            var dictionaryElements = [NSDictionary]()
            for corporateSchoolElement in schoolList {
                dictionaryElements.append(corporateSchoolElement.toDictionary())
            }
            dictionary["corporateSchoolList"] = dictionaryElements
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         serverLatency = aDecoder.decodeObject(forKey:"serverLatency") as? Int
         userdata = aDecoder.decodeObject(forKey:"user") as? UserData
         studentList = aDecoder.decodeObject(forKey:"studentGuardian") as? [StudentGuardian]
         classList = aDecoder.decodeObject(forKey:"classSubjectList") as? [classSubjectList]
         schoolList = aDecoder.decodeObject(forKey:"corporateSchoolList") as? [corporateSchoolList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if serverLatency != nil{
			aCoder.encode(serverLatency, forKey: "serverLatency")
		}
		if userdata != nil{
			aCoder.encode(userdata, forKey: "user")
		}
        if studentList != nil{
            aCoder.encode(studentList, forKey: "studentGuardian")
        }
        if classList != nil{
            aCoder.encode(classList, forKey: "classSubjectList")
        }
        if schoolList != nil{
            aCoder.encode(schoolList, forKey: "corporateSchoolList")
        }

	}

}
