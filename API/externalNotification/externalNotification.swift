//
//  externalNotification.swift
//  parentEye
//
//  Created by scientia on 04/07/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
class externalNotificationModel{
    
    var id:Int = 0
    var externalSystemId:Int = 0
    var studentId:Int = 0
    var content:String = ""

    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let enID = dictionary[resultsJsonConstants.externalNotification.id] as? Int{
            self.id = enID
        }else {
            self.id = 0
        }
        
        if let enexternalSystemId = dictionary[resultsJsonConstants.externalNotification.externalSystemId] as? Int{
            self.externalSystemId = enexternalSystemId
        }else {
            self.externalSystemId = 0
        }
        
        if let enstudentId = dictionary[resultsJsonConstants.externalNotification.studentId] as? Int{
            self.studentId = enstudentId
        }else {
            self.studentId = 0
        }
        
        if let encontent = dictionary[resultsJsonConstants.externalNotification.content] as? String{
            self.content = encontent
        }else {
            self.content = ""
        }
    }
    
}
