//
//  Timetable.swift
//  parentEye
//
//  Created by scientia on 30/01/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
class TimetableModelClass{
   
    var id = 0
    var className = ""
    var subjectName = ""
    var subjectNickName = ""
    var classId = ""
    var substitutionDetails = ""
    
    
    init?(fromDictionaryFromWebService dictionary:[String:AnyObject], assignDateValue parsedate:String){
        
        let result  = true
        
        if let timeTableid = dictionary[resultsJsonConstants.timeTableConstants.id] as? Int{
            self.id = timeTableid
        }else{
            self.id = 0
        }
        
        if let timeTableClassName = dictionary[resultsJsonConstants.timeTableConstants.className] as? String{
            self.className = String(timeTableClassName)
        }
        
        if let timeTableSubjectName = dictionary[resultsJsonConstants.timeTableConstants.subjectName] as? String{
            self.subjectName = String(timeTableSubjectName)
        }
        
        if let timeTableSubjectNickName = dictionary[resultsJsonConstants.timeTableConstants.subjectNickName] as? String{
            self.subjectNickName = String(timeTableSubjectNickName)
        }
        
        if let timeTableClassId = dictionary[resultsJsonConstants.timeTableConstants.classId] as? Int{
            self.classId = String(timeTableClassId)
        }else{
            self.classId = "0"
        }
        
        if let timeTableSubstitutionDetails = dictionary[resultsJsonConstants.timeTableConstants.substitutionDetails] as? String{
            self.substitutionDetails = String(timeTableSubstitutionDetails)
        }
        
        if result == false{
            return nil
        }
    }
}





class parseTimetable {
    
    class func parseFromDictionary(dictionary:[String:AnyObject]) ->([String:[TimetableModelClass]],NSError?) {
        
        var timeTableSortedDictionary  = [String:[TimetableModelClass]]()
        var error:NSError?
        if let schoolClassDict = dictionary[resultsJsonConstants.timeTableConstants.schoolClass] as? [String:AnyObject]{
        if let timeTables = schoolClassDict[resultsJsonConstants.timeTableConstants.timeTable] as? [String:AnyObject]{
            for (key,dateSeparatedTimetable) in timeTables{
                if let timeTableArray = dateSeparatedTimetable as? [[String:AnyObject]]{
                    var dateSortedArray = [TimetableModelClass]()
                    
                    for studentAndTimetable in timeTableArray {
                        if let timeTable = TimetableModelClass(fromDictionaryFromWebService: studentAndTimetable,assignDateValue: key){
                            dateSortedArray.append(timeTable)
                        }
                    }
                    timeTableSortedDictionary[key] = dateSortedArray
                }
            }
         }
        }else {
            error =  NSError(domain: "s", code: 1, userInfo:[NSLocalizedDescriptionKey:ConstantsInUI.noResults])
        }
         return (timeTableSortedDictionary,error)
    }
}

class parseTimetableForEmployee {
    
    class func parseFromDictionary(dictionary:[String:AnyObject]) ->([String:[TimetableModelClass]],NSError?) {
        
        var timeTableSortedDictionary  = [String:[TimetableModelClass]]()
        var error:NSError?
            if let timeTables = dictionary[resultsJsonConstants.timeTableConstants.timeTable] as? [String:AnyObject]{
                for (key,dateSeparatedTimetable) in timeTables{
                    if let timeTableArray = dateSeparatedTimetable as? [[String:AnyObject]]{
                        var dateSortedArray = [TimetableModelClass]()
                        
                        for studentAndTimetable in timeTableArray {
                            if let timeTable = TimetableModelClass(fromDictionaryFromWebService: studentAndTimetable,assignDateValue: key){
                                dateSortedArray.append(timeTable)
                            }
                        }
                        timeTableSortedDictionary[key] = dateSortedArray
                    }
                }
            }
        else {
            error =  NSError(domain: "s", code: 1, userInfo:[NSLocalizedDescriptionKey:ConstantsInUI.noResults])
        }
        return (timeTableSortedDictionary,error)
    }
}

