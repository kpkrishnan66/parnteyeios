//
//  MediaPickerContoller.swift
//  parentEye
//
//  Created by Martin Jacob on 22/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

enum sourceTypesForMediaPIcker {
    case camera
    case library
    case album
}

class MediaPickerContoller: UIImagePickerController {

    class func checkIfSourceTypeIsAvailable(type:sourceTypesForMediaPIcker) ->Bool{
    
        switch type {
        case .camera:
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                return true
            }else {
                return false
            }
        case .library:
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                return true
            }else {
                return false
            }
        case .album:
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                return true
            }else {
                return false
            }
        
        }
        
    return false
    }
 
    
    
}
