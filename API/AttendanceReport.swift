//
//  AttendanceReport.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 09/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class AttendanceReport {
    
    var className                 = ""
    var noOfAbsentees             = ""
    var totalStudents             = ""
    var teacherName               = ""
    var schoolClassId             = 0
    var message                   = ""
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atClsName = dictionary[resultsJsonConstants.attendanceReport.className] as? String{
            self.className = atClsName
        }else {
            self.className = ""
        }
        
        if let atabs = dictionary[resultsJsonConstants.attendanceReport.noOfAbsentees] as? String{
            self.noOfAbsentees = atabs
        }else {
            self.noOfAbsentees = "0"
        }
        
        if let attot = dictionary[resultsJsonConstants.attendanceReport.totalStudents] as? String{
            self.totalStudents = attot
        }else {
            self.totalStudents = ""
        }
        
        if let attcrName = dictionary[resultsJsonConstants.attendanceReport.teacherName] as? String{
            self.teacherName = attcrName
        }else {
            self.teacherName = ""
        }
        
        if let attSchlClasId = dictionary[resultsJsonConstants.attendanceReport.schoolClassId] as? Int{
            self.schoolClassId = attSchlClasId
        }else {
            self.schoolClassId = 0
        }
        
        if let attSchlClasId = dictionary[resultsJsonConstants.attendanceReport.schoolClassId] as? Int{
            self.schoolClassId = attSchlClasId
        }else {
            self.schoolClassId = 0
        }
        
        if let msg = dictionary[resultsJsonConstants.attendanceReport.message] as? String{
            self.message = msg
        }else {
            self.message = ""
        }
    }
}

class  parseAttendanceReportArray {
    class func parseThisDictionaryForAttendanceRecords(dictionary:[String:AnyObject])->([AttendanceReport]){
        
        var attendanceRecordArray = [AttendanceReport]()
        
        if let attendanceRecord = dictionary[resultsJsonConstants.attendanceReport.attendanceReport] as? [AnyObject]{
            for dicto in attendanceRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = AttendanceReport(withDictionaryFromWebService: record){
                        attendanceRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(attendanceRecordArray)
    }
    
}
