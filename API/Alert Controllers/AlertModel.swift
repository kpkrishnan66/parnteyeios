//
//  AlertModel.swift
//  parentEye
//
//  Created by Martin Jacob on 19/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import SwiftyDrop

class AlertModel {
    class func showToastWithMessage(_ message:String, stateOfMessage msgState:stateOfMessage) {
        var msgStateForSwiftyDrop = DropState.default
        switch msgState {
        case .success:
            msgStateForSwiftyDrop = .success
        case .info:
            msgStateForSwiftyDrop = .info
        case .failure:
            msgStateForSwiftyDrop = .error
        case.defaults:
            msgStateForSwiftyDrop = .default
        }
        
        Drop.down(message, state: msgStateForSwiftyDrop)
    }

    
}
