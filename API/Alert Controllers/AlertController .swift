//
//  AlertController .swift
//  parentEye
//
//  Created by Martin Jacob on 19/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit


enum stateOfMessage {
    case success
    case failure
    case defaults
    case info
    
}

class AlertController  {
  
    class func showToastAlertWithMessage(message:String, stateOfMessage msgState:stateOfMessage){
        AlertModel.showToastWithMessage(message, stateOfMessage: msgState)
    }
    
   class func getTwoOptionAlertControllerWithMessage (mesasage:String, titleOfAlert title:String,  oktitle okbuttontitle:String,cancelTitle cancelButtontitle:String,cancelButtonBlock cancelHandler:((UIAlertAction) -> Void)?,OkButtonBlock OKhandler: ((UIAlertAction) -> Void)?) ->UIAlertController{
        
        let alert = UIAlertController(title: title, message: mesasage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: cancelButtontitle, style: UIAlertActionStyle.cancel, handler: cancelHandler))
        alert.addAction(UIAlertAction(title: okbuttontitle, style: UIAlertActionStyle.default, handler: OKhandler))
        return alert
    }
    
    class func getSingleOptionAlertControllerWithMessage (mesasage:String, titleOfAlert title:String,  oktitle okbuttontitle:String,OkButtonBlock OKhandler: ((UIAlertAction) -> Void)?) ->UIAlertController{
        
        let alert = UIAlertController(title: title, message: mesasage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: okbuttontitle, style: UIAlertActionStyle.default, handler: OKhandler))
        return alert
    }
    
}

