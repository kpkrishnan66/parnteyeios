//
//  classSubject.swift
//  parentEye
//
//  Created by scientia on 17/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class classSubject {
    
    var id:Int = 0
    var name:String = ""
    var description:String = ""
    var order:Int = 0
    var nickName:String = ""
    var maximumMark:Int = 0
    
    init() {
        
    }
    
    init(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let classSubjectId  =  dictionary[resultsJsonConstants.classSubject.id] as? Int{
            id = classSubjectId
        }
        if let classSubjectname =  dictionary[resultsJsonConstants.classSubject.name] as? String {
            name = classSubjectname
        }
        if let classSubjectdescription =  dictionary[resultsJsonConstants.classSubject.description] as? String {
            description = classSubjectdescription
        }
        if let classSubjectorder =  dictionary[resultsJsonConstants.classSubject.order] as? Int {
            order = classSubjectorder
        }
        if let classSubjectnickName =  dictionary[resultsJsonConstants.classSubject.nickName] as? String {
            nickName = classSubjectnickName
        }
    }
}
class  parseClassSubjectListArray {
    class func parseThisDictionaryForClassSubjectRecords(dictionary:[String:AnyObject])->([classSubject]){
        
        var classSubjectListRecordArray = [classSubject]()
        
        if let classSubjectListArray = dictionary[resultsJsonConstants.classSubject.subjects] as? NSArray {
            for dicto in classSubjectListArray{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    let atcObj = classSubject(withDictionaryFromWebService: record)
                    classSubjectListRecordArray.append(atcObj)
                    
                }
            }
            
        }
        
        return(classSubjectListRecordArray)
    }
}
