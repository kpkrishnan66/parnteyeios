//
//  EmployeeSubjectSelectedClassList.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 24/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class EmployeeSubjectSelectedClassList :NSObject{
    

 var classId:Int   = -1
 var className = ""
 var selectedInUI = false

init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
    
    if let clsID = dictionary[resultsJsonConstants.homeWorkConstants.classId] as? Int{
        self.classId = clsID
    }else {
        self.classId = -1
    }
    
    
    if let clsNme = dictionary[resultsJsonConstants.homeWorkConstants.className] as? String{
        self.className = clsNme
    }else {
        self.className = ""
    }
}
}


class  parseSubjectSelectesdClassArray {
    class func parseThisDictionaryForComposeHomework(dictionary:[String:AnyObject])->([EmployeeSubjectSelectedClassList]){
        
        var classesArray = [EmployeeSubjectSelectedClassList]()
        
        if let clsDicto = dictionary[resultsJsonConstants.homeWorkConstants.classList] as? [AnyObject]{
            for dicto in clsDicto{
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let subObj = EmployeeSubjectSelectedClassList(withDictionaryFromWebService: record){
                        classesArray.append(subObj)
                    }
                }
            }
        }
        
        return(classesArray)
    }
}
    