//
//  EmployeeDetailedStudentProfile.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 04/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
class  EmployeeDetailedStudentProfile:NSObject,NSCoding{
    
    var leaveListArray = [EmployeeDetailedStudentProfile]()
    
    var studentId:Int   = -1
    var studentName:String     = ""
    var compressedProfilePic:String  = ""
    var profilePic = ""
    var mobileNo = ""
    var offset = 0
    var className = ""
    var classId = -1
    var schoolId = -1
    var classTeacher = ""
    var AdmnNo = ""
    var dob = ""
    var transportType = ""
    var delayReport = ""
    var address = ""
    var parentName = ""
    var regNo = ""
    var contactNo = ""
    var busId = -1
    var busNo = ""
    var date = ""
    var reason = ""
    var leaveStatus = ""
    var leaveReasonList = [String]()
    var leaveDescription:String = ""
    var feePayment:String = ""
    var showOnlyProgressCard:Bool = false
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atcID = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.id] as? Int{
            self.studentId = atcID
        }else {
            self.studentId = -1
        }
        
        
        if let stdname = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.name] as? String{
            self.studentName = stdname
        }else {
            self.studentName = ""
        }
        
        
        if let compopic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.compressedProfilePic] as? String{
            self.compressedProfilePic = compopic
        }else {
            self.compressedProfilePic = ""
        }
        
        
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.profilePic] as? String{
            self.profilePic = comppic
        }else {
            self.profilePic = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.mobileNo] as? String{
            self.mobileNo = comppic
        }else {
            self.mobileNo = ""
        }
        
        
        if let compclaname = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.className] as? String{
            self.className = compclaname
        }else {
            self.className = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.classId] as? Int{
            self.classId = comppic
        }else {
            self.classId = -1
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.schoolId] as? Int{
            self.schoolId = comppic
        }else {
            self.schoolId = -1
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.classTeacher] as? String{
            self.classTeacher = comppic
        }else {
            self.classTeacher = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.AdmnNo] as? String{
            self.AdmnNo = comppic
        }else {
            self.AdmnNo = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.dob] as? String{
            self.dob = comppic
        }else {
            self.dob = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.transportType] as? String{
            self.transportType = comppic
        }else {
            self.transportType = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.delayReport] as? String{
            self.delayReport = comppic
        }else {
            self.delayReport = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.address] as? String{
            self.address = comppic
        }else {
            self.address = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.parentName] as? String{
            self.parentName = comppic
        }else {
            self.parentName = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.regNo] as? String{
            self.regNo = comppic
        }else {
            self.regNo = ""
        }
        
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.contactNo] as? String{
            self.contactNo = comppic
        }else {
            self.contactNo = ""
        }
       
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.busId] as? Int{
            self.busId = comppic
        }else {
            self.busId = -1
        }
       
        if let comppic = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.busNo] as? String{
            self.busNo = comppic
        }else {
            self.busNo = ""
        }
        
        
        if let configItemsObj = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.configItems] as? [String:AnyObject]{
            if let leaveReasonListObj = configItemsObj[resultsJsonConstants.employeeDetailedStudentProfile.leaveReasonList] as? [String]{
                for index in leaveReasonListObj {
                    leaveReasonList.append(index)
                }
            }
            
            if let compleaveDescription = configItemsObj[resultsJsonConstants.employeeDetailedStudentProfile.leaveDescription] as? String{
                self.leaveDescription = compleaveDescription
            }else {
                self.leaveDescription = ""
            }
            
            if let compfeePayment = configItemsObj[resultsJsonConstants.employeeDetailedStudentProfile.feePayment] as? String{
                self.feePayment = compfeePayment
            }else {
                self.feePayment = ""
            }
            
            if let compshowOnlyProgressCard = configItemsObj[resultsJsonConstants.employeeDetailedStudentProfile.showOnlyProgressCard] as? Bool{
                self.showOnlyProgressCard = compshowOnlyProgressCard
            }else {
                self.showOnlyProgressCard = false
            }
        }
        
        
    }
    
    init?(withDictionaryFromWebServiceForLeave dictionary:[String:AnyObject]){
        
        if let lvDate = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.date] as? String{
            self.date = lvDate
        }else {
            self.date = ""
        }
        
        if let lvReason = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.reason] as? String{
            self.reason = lvReason
        }else {
            self.reason = ""
        }
        
        if let lvleaveStatus = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.leaveStatus] as? String{
            self.leaveStatus = lvleaveStatus
        }else {
            self.leaveStatus = ""
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        self.date                = aDecoder.decodeObject(forKey: "date") as! String
        self.reason             = aDecoder.decodeObject(forKey: "reason") as! String
    }
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(date, forKey: "date")
        aCoder.encode(reason, forKey: "reason")
    }
    
}

class  parseEmployeeDetailedStudentProfileList {
    class func parseThisDictionaryForDetailedStudentProfileList(dictionary:[String:AnyObject])->([EmployeeDetailedStudentProfile]){
        
        var studentListArray = [EmployeeDetailedStudentProfile]()
        
        if let studentList = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.studentList] as? [AnyObject]{
            for dicto in studentList{// parse the array in the root
                if let std = dicto[resultsJsonConstants.employeeDetailedStudentProfile.student] as? [String:AnyObject]{
                    if let msg = std as? [String:AnyObject]{ // check if the thing is dictionary of the format
                      if let atcObj = EmployeeDetailedStudentProfile(withDictionaryFromWebService: msg){
                        studentListArray.append(atcObj)
                      }
                  }
                }
            }
        }
        
        return(studentListArray)
    }
}

class  parseEmployeeDetailedStudentProfileLeaveList {
    
    
        class func parseThisDictionaryForDetailedStudentProfileLeaveList(dictionary:[String:AnyObject])->([EmployeeDetailedStudentProfile]){
            
            var leaveListArray = [EmployeeDetailedStudentProfile]()
            
            if let lvList = dictionary[resultsJsonConstants.employeeDetailedStudentProfile.studentList] as? [AnyObject]{
                for dicto in lvList{// parse the array in the root
                    if let std = dicto[resultsJsonConstants.employeeDetailedStudentProfile.leaves] as? [AnyObject]{
                        for par in std{// parse the array in the root
                            
                        if let leave = par as? [String : AnyObject] // check if the thing is dictionary of the format
                        {
                            
                            if let atcObj = EmployeeDetailedStudentProfile(withDictionaryFromWebServiceForLeave: leave){
                                leaveListArray.append(atcObj)
                            }
                        }
                            
                        }
                    }
                }
            }
            
            return(leaveListArray)
        }
    
    
   
}

