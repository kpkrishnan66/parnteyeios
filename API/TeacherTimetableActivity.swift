//
//  TeacherTimetableActivity.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 01/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import Foundation
import UIKit
class TeacherTimetableActivity :NSObject {
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
    }

}


class  parseTeacherListForActivityList {
    class func parseThisDictionaryForEmployeePopup(dictionary:[String:AnyObject])->([TeacherTimetableActivity]){
        
        var activityListArray = [TeacherTimetableActivity]()
        
        if let activityList = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.employeeList] as? [AnyObject]{
            for dicto in activityList{// parse the array in the root
                if let msg = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = TeacherTimetableActivity(withDictionaryFromWebService: msg){
                        activityListArray.append(atcObj)
                    }
                }
            }
        }
        
        return(activityListArray)
    }
    
}