//
//  DatePickerController.swift
//  parentEye
//
//  Created by Martin Jacob on 15/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

protocol dateSelectedButtonPressedProtocol:class  {
    
    func ButtonPressed()
}


class DatePickerController: UIViewController {
    
    var valueChangedCompletion : ((_ date: Foundation.Date)->Void)!
    var viewLoadedCompletion : ((_ datePicker: UIDatePicker)->Void)!
    weak var delegate:dateSelectedButtonPressedProtocol?
    
    @IBOutlet weak var datePicker: UIDatePicker!
    var canDismiss = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        canDismiss = false
        if (self.viewLoadedCompletion != nil) {
            //self.viewLoadedCompletion((datePicker: self.datePicker) as! UIDatePicker)
        }
        
    }
    
    fileprivate func setMaxAndMinDate(){
        
        self.datePicker.maximumDate = Foundation.Date()
    }
    
    @IBAction func DatePicked(_ sender: AnyObject) {
        if (self.valueChangedCompletion != nil) {
            self.valueChangedCompletion(datePicker.date)
            canDismiss = true
            self.delegate?.ButtonPressed()
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func dismiss(animated flag: Bool,
                          completion: (() -> Void)?)
    {
        if canDismiss == true{
            canDismiss = false
            super.dismiss(animated: flag, completion:completion)
        }
        
        // Your custom code here...
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        canDismiss = true
        self.dismiss(animated: true, completion: nil)
    }
}

