//
//  DateController.swift
//  parentEye
//
//  Created by Martin Jacob on 13/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

enum dateFormatNeeded {
    case MMddyyyyWithHypen
    case MMddyyyyWithSlash
    case ddMMyyyyWithHypen
    case yyyyMMddWithHypen
    case ddMMyyyyWithMonthNameWithHyphen
}

class DateApi: NSDate {
    
    class func getDateOfDayWithRespectToDay(_ day:Foundation.Date, shouldGetPreviousDate isPreviousDateNeeded:Bool, numberOfDaysToShift days:Int) ->(Foundation.Date){
        
        var d = days
        let calendar = Calendar.current
        if isPreviousDateNeeded {
            d = -d
        }
        
        let daysAgo = (calendar as NSCalendar).date(byAdding: .day, value: d, to: day, options: [])
        return daysAgo!
    }
    
    class func convertThisDateToString(_ date:Foundation.Date, formatNeeded format:dateFormatNeeded) ->String {
        let dateFormatter = DateFormatter()
        switch format {
        case .MMddyyyyWithHypen: dateFormatter.dateFormat = "MM-dd-yyyy"
        case .MMddyyyyWithSlash: dateFormatter.dateFormat = "MM/dd/yyyy"
        case .ddMMyyyyWithHypen: dateFormatter.dateFormat = "dd-MM-yyyy"
        case .yyyyMMddWithHypen: dateFormatter.dateFormat = "yyyy-MM-dd"
        case .ddMMyyyyWithMonthNameWithHyphen : dateFormatter.dateFormat = "dd-MMM-yyyy"
            
            
        }
        return dateFormatter.string(from: date)
    }
    
    class func convertThisStringToDate(dateString:String, formatNeeded format:dateFormatNeeded) ->NSDate{
        let dateFormatter = DateFormatter()
        switch format {
        case .MMddyyyyWithHypen: dateFormatter.dateFormat = "MM-dd-yyyy"
        case .MMddyyyyWithSlash: dateFormatter.dateFormat = "MM/dd/yyyy"
        case .ddMMyyyyWithHypen: dateFormatter.dateFormat = "dd-MM-yyyy"
        case .yyyyMMddWithHypen: dateFormatter.dateFormat = "yyyy-MM-dd"
        case .ddMMyyyyWithMonthNameWithHyphen : dateFormatter.dateFormat = "dd-MMM-yyyy"
        }
       return dateFormatter.date(from: dateString)! as NSDate
    }
}
