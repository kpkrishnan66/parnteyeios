//
//  EmployeeTimetableAlternateSameClass.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 30/08/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
class EmployeeTimetableAlternateSameClass :NSObject {
    
    
        
        var employeeId = -1
        var employeeName = ""
        var profilePic = ""
        var mobileNo = ""
        var classInCharge = ""
        var designation = ""
        var substituted = false
    
        init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
            
            if let atcID = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.id] as? Int{
                self.employeeId = atcID
            }else {
                self.employeeId = -1
            }
            
            
            if let stdname = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.name] as? String{
                self.employeeName = stdname
            }else {
                self.employeeName = ""
            }
            
            
            if let comppic = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.profilePicUrl] as? String{
                self.profilePic = comppic
            }else {
                self.profilePic = ""
            }
            
            if let comppic = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.mobileNo] as? String{
                self.mobileNo = comppic
            }else {
                self.mobileNo = ""
            }
            
            if let comppic = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.classInCharge] as? String{
                self.classInCharge = comppic
            }else {
                self.classInCharge = ""
            }
            
            if let comppic = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.designation] as? String{
                self.designation = comppic
            }else {
                self.designation = ""
            }
            
            if let comppic = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.substituted] as? Bool{
                self.substituted = comppic
            }else {
                self.substituted = false
            }
        }
        
    }
    
    class  parseEmployeeListForSubstitution {
        class func parseThisDictionaryForEmployeePopup(dictionary:[String:AnyObject])->([EmployeeTimetableAlternateSameClass]){
            
            var employeeListArray = [EmployeeTimetableAlternateSameClass]()
            
            if let employeeList = dictionary[resultsJsonConstants.substituteEmployeesOfSameClass.employeeList] as? [AnyObject]{
                for dicto in employeeList{// parse the array in the root
                    if let msg = dicto as? [String : AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = EmployeeTimetableAlternateSameClass(withDictionaryFromWebService: msg){
                            employeeListArray.append(atcObj)
                        }
                    }
                }
            }
            
            return(employeeListArray)
        }
        
}





    

