//
//  ImageApi.swift
//  parentEye
//
//  Created by Martin Jacob on 19/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Haneke



class ImageAPi {
    
    /**
     Function to make image view round

     
     - parameter imageView: image view to be made round
     */
    class func makeImageViewRound(imageView:UIImageView){
        imageView.clipsToBounds = true;
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = imageView.frame.size.height / 2.3;
        
    }
    class func makeButtonRound(button:UIButton){
        button.layer.masksToBounds = true
        button.layer.cornerRadius = button.frame.size.height / 2.9;
        button.clipsToBounds = true;
    }
    
    class func makeViewRealRound(view:UIView) {
        view.clipsToBounds = true;
        view.layer.masksToBounds = true
        view.layer.cornerRadius = view.frame.size.height / 4;
        
    }
   
    
    class  func resizeImage (image:UIImage) ->UIImage {
        
        var actualHeight = image.size.height
        var actualWidth = image.size.width
        let maxHeight:CGFloat = 300.0;
        let maxWidth:CGFloat = 400.0;
        var imgRatio = actualWidth/actualHeight;
        let maxRatio = maxWidth/maxHeight;
        let compressionQuality:CGFloat = 0.5;//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight);
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!, compressionQuality)
        UIGraphicsEndImageContext();
        
        return UIImage(data: imageData!)!
    }
    
    
    class func getCircleImageFromImage(image:UIImage) ->UIImage?{
        
        let square = CGSize(width: min(image.size.width, image.size.height), height: min(image.size.width, image.size.height))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = .scaleAspectFill
        imageView.image = image
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, image.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result
    }
    
    class  func  removeImageForURl (urlString: String) {
        let imageCache = Shared.imageCache
        imageCache.remove(key: urlString)
    }
    
    class  func fetchImageForUrl(urlString:String, oneSuccess success:@escaping imageDownloadScuccess , onFailure failure:imageFailureClosure) {
        
        if let url = NSURL(string:urlString){
            let imageCache = Shared.imageCache
            let fetch = imageCache.fetch(URL: url as URL)
            
            fetch.onSuccess(success)
            //fetch.onFailure(failure)
        }
    }
    
    class  func setImage(image:UIImage, forUrlString urlString:String , oneSuccess success:@escaping imageDownloadScuccess) {
        if let _ = NSURL(string:urlString){
            let imageCache = Shared.imageCache
            _ =  imageCache.set(value: image, key: urlString, formatName: HanekeGlobals.Cache.OriginalFormatName, success: { (image) in
               success(image)
            })
        }
    }
  
}
