//
//  imageVCForUploadProcess.swift
//  reusableImageV
//
//  Created by Martin Jacob on 04/03/16.
//  Copyright © 2016 Martin Jacob. All rights reserved.
//

import UIKit


protocol imageUpLoaderProtocol:class{
    /**
     function to be called on the delegate if the upload failed
     */
    func reuploadImage()
    func initiatePhotoChange()
}

open class imageVCForUploadProcess: UIImageView {
    
    let progressBar  = UIProgressView()
    let reloadButton = UIButton()
    weak var delegate:imageUpLoaderProtocol?
    
    init(){
        super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.isUserInteractionEnabled = true
        // self.reloadButton.hidden    = true
    }
    override open func didMoveToSuperview() {
        addProgressBartoView()
        addReloadButton()
        addTapGesture()
        setVisibiltyOfProgressBarhidden(true, andUploadButtonHidden: true)
    }
    
    fileprivate func addTapGesture(){
        
        
        let tap = UITapGestureRecognizer(target: self, action:#selector(imageVCForUploadProcess.initiatePhotoChange))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
        
    }
    
    fileprivate func addReloadButton(){
        
        
        reloadButton.frame = CGRect(x: self.bounds.origin.x, y: self.bounds.origin.y+20, width: 100, height: 100)
        reloadButton.center = CGPoint(x: self.frame.size.width/2,y: self.frame.size.height/2);
        
        reloadButton.setTitle("Re UpLoad", for: UIControlState())
        reloadButton.backgroundColor = UIColor.red
        self.addSubview(reloadButton)
        self.bringSubview(toFront: reloadButton)
        reloadButton.addTarget(self, action: "reUpload", for:UIControlEvents.touchUpInside)
        
        
    }
    
    
    //MARK :- set visibility of uploadButton and progress bar
    
    fileprivate func setVisibiltyOfProgressBarhidden(_ progresHidden:Bool, andUploadButtonHidden reloadBtnVisibility:Bool){
        progressBar.isHidden = progresHidden
        reloadButton.isHidden = reloadBtnVisibility
        
    }
    //MARK:- UPLOAD FAILED METHODS
    
    open func UploadStatus(_ status:Bool){
        privateUploadStatus(status)
    }
    
    fileprivate func privateUploadStatus(_ status:Bool){
        if status == true{
            reloadButton.isHidden = true
        }else {
            reloadButton.isHidden = false
        }
        
    }
    //MARK: PROGRESSBAR METHODS
    /**
     Sets the progress bar
     
     - parameter progress: value to be set on the progress bar.
     */
    open func setProgress(_ progress:Float){
        setProgressPrivateFunction(progress)
    }
    /**
     Sets the progress bar- private function
     
     - parameter progress: value to be set on the progress bar.
     */
    fileprivate func setProgressPrivateFunction(_ progress:Float){
        self.progressBar.setProgress(progress, animated: true)
    }
    
    /**
     Adds the progress bar on top of the imageView.
     */
    fileprivate func addProgressBartoView(){
        progressBar.backgroundColor = UIColor.black
        progressBar.progress        = 0.5
        progressBar.frame           = CGRect(x: 0, y: 0, width: self.frame.width, height: 100)
        self.addSubview(progressBar)
    }
    /**
     initializes the upload
     
     - returns: <#return value description#>
     */
    @objc fileprivate func initiatePhotoChange(){
        self.delegate?.initiatePhotoChange()
    }
    
    //MARK:PROTOCOL METHODS
    
    
    fileprivate func reUpload(){
        self.delegate?.reuploadImage()
    }
    
}

