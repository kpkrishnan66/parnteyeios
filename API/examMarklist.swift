//
//  examMarklist.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 19/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class ExamMarklist :NSObject {
    
    var examNames = [String] ()
    var examSubjectsList = [examSubject] ()
    
    override init() {
        
    }
    
    func setexamNames(names:[String]){
        self.examNames = names
    }
    func getexamNames() -> [String]{
        return examNames
    }
    func setExamSubjectList(subjectList:[examSubject]){
        self.examSubjectsList = subjectList
    }
    func getExamSubjectList() -> [examSubject]{
        return examSubjectsList
    }
    
}