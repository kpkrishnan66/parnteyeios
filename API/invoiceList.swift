//
//  invoiceList.swift
//  parentEye
//
//  Created by scientia on 25/05/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class invoiceList {
    
    var id:String = ""
    var name:String = ""
    var amount:String = ""
    var date:String = ""
    var dueDate:String = ""
    var studentName:String = ""
    var studentId:Int = 0
    var status:String = ""
    var paymentId:String = ""
    var paidOn:String = ""
    var schoolId:Int = 0
    var schoolName:String = ""
    var schoolAddress:String = ""
    var payButton:String = ""
    var feeListArray = [feeList]()
    var selected:Bool = false
    var canDownloadReceipt:Bool = false
    var paymentGateway:String = ""
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atId = dictionary[resultsJsonConstants.invoiceList.id] as? String{
            self.id = atId
        }else {
            self.id = ""
        }
        
        if let atName = dictionary[resultsJsonConstants.invoiceList.name] as? String{
            self.name = atName
        }else {
            self.name = ""
        }
        
        if let atamount = dictionary[resultsJsonConstants.invoiceList.amount] as? String{
            self.amount = atamount
        }
        else if let atamount = dictionary[resultsJsonConstants.invoiceList.amount] as? Int{
            self.amount = String(atamount)
        }
        else {
            self.amount = ""
        }
        
        if let atdate = dictionary[resultsJsonConstants.invoiceList.date] as? String{
            self.date = atdate
        }else {
            self.date = ""
        }
        
        if let atdueDate = dictionary[resultsJsonConstants.invoiceList.dueDate] as? String{
            self.dueDate = atdueDate
        }else {
            self.dueDate = ""
        }
        
        if let atstudentName = dictionary[resultsJsonConstants.invoiceList.studentName] as? String{
            self.studentName = atstudentName
        }else {
            self.studentName = ""
        }
        
        if let atstudentId = dictionary[resultsJsonConstants.invoiceList.studentId] as? Int{
            self.studentId = atstudentId
        }else {
            self.studentId = 0
        }
        
        
        if let atstatus = dictionary[resultsJsonConstants.invoiceList.status] as? String{
            self.status = atstatus
        }else {
            self.status = ""
        }
        
        if let atpaymentId = dictionary[resultsJsonConstants.invoiceList.paymentId] as? String{
            self.paymentId = atpaymentId
        }else {
            self.paymentId = ""
        }
        
        if let atpaidOn = dictionary[resultsJsonConstants.invoiceList.paidOn] as? String{
            self.paidOn = atpaidOn
        }else {
            self.paidOn = ""
        }
        
        if let atpaymentGateway = dictionary[resultsJsonConstants.invoiceList.paymentGateway] as? String{
            self.paymentGateway = atpaymentGateway
        }else {
            self.paymentGateway = ""
        }
        
        if let atschoolId = dictionary[resultsJsonConstants.invoiceList.schoolId] as? Int{
            self.schoolId = atschoolId
        }else {
            self.schoolId = 0
        }
        
        if let atschoolName = dictionary[resultsJsonConstants.invoiceList.schoolName] as? String{
            self.schoolName = atschoolName
        }else {
            self.schoolName = ""
        }
        
        if let atschoolAddress = dictionary[resultsJsonConstants.invoiceList.schoolAddress] as? String{
            self.schoolAddress = atschoolAddress
        }else {
            self.schoolAddress = ""
        }
        
        if let atpayButton = dictionary[resultsJsonConstants.invoiceList.payButton] as? String{
            self.payButton = atpayButton
        }else {
            self.payButton = ""
        }
        
        if let atcanDownloadReceipt = dictionary[resultsJsonConstants.invoiceList.canDownloadReceipt] as? Bool{
            self.canDownloadReceipt = atcanDownloadReceipt
        }else {
            self.canDownloadReceipt = false
        }
        
        if let atFeeList = dictionary[resultsJsonConstants.invoiceList.feeList] as? NSArray {
            for fee in atFeeList {
                let feeValue = feeList(fromDictionary: fee as! [String : AnyObject])
                feeListArray.append(feeValue)
            }
            
        }
        
    }
}

class  parseInvoiceListArray {
    class func parseThisDictionaryForInvoiceRecords(dictionary:[String:AnyObject])->([invoiceList]){
        
        var invoiceListRecordArray = [invoiceList]()
        
        if let invoiceRecord = dictionary[resultsJsonConstants.invoiceList.feePaymentDetails] as? [String:AnyObject]{
            if let invoiceListArray = invoiceRecord[resultsJsonConstants.invoiceList.invoiceList] as? NSArray {
                for dicto in invoiceListArray{// parse the array in the root
                    if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = invoiceList(withDictionaryFromWebService: record){
                            invoiceListRecordArray.append(atcObj)
                        }
                    }
                }
            }
        }
        
        return(invoiceListRecordArray)
    }
    
}
