//
//  PhotoShowingView.swift
//  parentEye
//
//  Created by Martin Jacob on 28/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class PhotoShowingView: UIView,UIScrollViewDelegate {

    @IBOutlet weak var imageView:UIImageView!
    
   
    /**
     To remove self from super view
     */
    @IBAction func removeMe(){
        self.removeFromSuperview()
    }
}
