//
//  evaluationActivity.swift
//  parentEye
//
//  Created by scientia on 17/07/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class evaluationActivity {
    
    var id:Int = 0
    var name:String = ""
    
    init() {
        
    }
    
    init(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let evaluationId  =  dictionary[resultsJsonConstants.evaluationActivity.id] as? Int{
            id = evaluationId
        }
        if let evaluationname =  dictionary[resultsJsonConstants.evaluationActivity.name] as? String {
            name = evaluationname
        }
    }
}

class  parseEvaluationActivityListArray {
    class func parseThisDictionaryForEvaluationActivityRecords(dictionary:[String:AnyObject])->([evaluationActivity]){
        
        var evaluationActivityListRecordArray = [evaluationActivity]()
        
           if let evaluationActivityListArray = dictionary[resultsJsonConstants.evaluationActivity.evaluationActivity] as? NSArray {
                for dicto in evaluationActivityListArray{// parse the array in the root
                    if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                            let atcObj = evaluationActivity(withDictionaryFromWebService: record)
                            evaluationActivityListRecordArray.append(atcObj)
                        
                    }
                }
            
        }
        
        return(evaluationActivityListRecordArray)
   }
}

