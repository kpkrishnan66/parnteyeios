//
//  AttendanceClassReport.swift
//  parentEye
//
//  Created by scientia on 27/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class AttendanceClassReport {
    
    var studentName               = ""
    var studentRollNo             = ""
    var studentProfilePic         = ""
    var mobileNo                  = ""
    var leaveReason               = ""
    var leaveDescription          = ""
    
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atStdName = dictionary[resultsJsonConstants.attendanceClassReport.student] as? String{
            self.studentName = atStdName
        }else {
            self.studentName = ""
        }
        if let atStdRollNo = dictionary[resultsJsonConstants.attendanceClassReport.rollNo] as? String{
            self.studentRollNo = atStdRollNo
        }else {
            self.studentRollNo = ""
        }
        if let atStdPic = dictionary[resultsJsonConstants.attendanceClassReport.profilePic] as? String{
            self.studentProfilePic = atStdPic
        }else {
            self.studentProfilePic = ""
        }
        if let atMobile = dictionary[resultsJsonConstants.attendanceClassReport.mobileNo] as? String{
            self.mobileNo = atMobile
        }else {
            self.mobileNo = ""
        }
        if let atLvResaon = dictionary[resultsJsonConstants.attendanceClassReport.reason] as? String{
            self.leaveReason = atLvResaon
        }else {
            self.leaveReason = ""
        }
        
        if let atLvDesc = dictionary[resultsJsonConstants.attendanceClassReport.description] as? String{
            self.leaveDescription = atLvDesc
        }else {
            self.leaveDescription = ""
        }
        
        
    }
}

class  parseAttendanceClassReportArray {
    class func parseThisDictionaryForAttendanceClassRecords(dictionary:[String:AnyObject])->([AttendanceClassReport]){
        
        var attendanceRecordArray = [AttendanceClassReport]()
        
        if let attendanceRecord = dictionary[resultsJsonConstants.attendanceClassReport.studentAbsentRecord] as? [AnyObject]{
            for dicto in attendanceRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = AttendanceClassReport(withDictionaryFromWebService: record){
                        attendanceRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(attendanceRecordArray)
    }
    
}
