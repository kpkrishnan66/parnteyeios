//
//  FontApi.swift
//  parentEye
//
//  Created by Martin Jacob on 14/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit


class FontApi {
    
    enum device {
        case iphone
    }
    struct fontSizeHeading {
        static let iphoneSize:CGFloat = 14.0
        static let ipadSize:CGFloat   = 20.0
    }
    /**
     Function to get the font for each device.This frunction should be altered to used trait collection now we will just be checking if the device is ipad.
     
     - parameter traitCollection: trait Colection
     */
    class func getFontSizeCellHeadingForTraitCollection(traitCollection:UITraitCollection) ->CGFloat{
        
        var fontSize:CGFloat = 0.0
        if weNeedLargeFont(traitCollection: traitCollection){
            fontSize = fontSizeHeading.ipadSize
        }else {
           fontSize = fontSizeHeading.iphoneSize
        }
        return fontSize
    }
    
    /**
     
     
     - parameter traitCollection: traitcollection
     
     - returns: large size needed or not
     */
    private class func weNeedLargeFont(traitCollection:UITraitCollection) ->Bool{
        
        switch UIDevice.current.userInterfaceIdiom  {
        case .pad: return true
        case .phone: return false
        default:return false
        }
    }

}
