//
//  evaluationActivitySubject.swift
//  parentEye
//
//  Created by scientia on 19/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
class evaluationActivitySubject {
    
    var id:Int = 0
    var name:String = ""
    var maxMark:Int = 0
    var subject:String = ""
    var subjectId:Int = 0
    var date:String = ""
    var time:String = ""
    var topic:String = ""
    var addedBy:Int = 0
    var addedByName:String = ""
    var upcoming:String = ""
    var status:String = ""
    var schoolClassName:String = ""
    var schoolClassId:Int = 0
    var evaluationActivityName:String = ""
    var evaluvationListArray = [evaluationActivity]()
    
    init() {
        
    }
    
    init(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let evaluationActivitySubjectId  =  dictionary[resultsJsonConstants.evaluationActivitySubject.id] as? Int{
            id = evaluationActivitySubjectId
        }
        if let evaluationActivitySubjectname =  dictionary[resultsJsonConstants.evaluationActivitySubject.name] as? String {
            name = evaluationActivitySubjectname
        }
        if let evaluationActivitySubjectmaxMark =  dictionary[resultsJsonConstants.evaluationActivitySubject.maxMark] as? Int {
            maxMark = evaluationActivitySubjectmaxMark
        }
        if let evaluationActivitySubjectsubject =  dictionary[resultsJsonConstants.evaluationActivitySubject.subject] as? String {
            subject = evaluationActivitySubjectsubject
        }
        if let evaluationActivitySubjectsubjectId =  dictionary[resultsJsonConstants.evaluationActivitySubject.subjectId] as? Int {
            subjectId = evaluationActivitySubjectsubjectId
        }
        
        if let evaluationActivitySubjectdate  =  dictionary[resultsJsonConstants.evaluationActivitySubject.date] as? String{
            date = evaluationActivitySubjectdate
        }
        if let evaluationActivitySubjecttime =  dictionary[resultsJsonConstants.evaluationActivitySubject.time] as? String {
            time = evaluationActivitySubjecttime
        }
        if let evaluationActivitySubjecttopic =  dictionary[resultsJsonConstants.evaluationActivitySubject.topic] as? String {
            topic = evaluationActivitySubjecttopic
        }
        if let evaluationActivitySubjectaddedBy =  dictionary[resultsJsonConstants.evaluationActivitySubject.addedBy] as? Int {
            addedBy = evaluationActivitySubjectaddedBy
        }
        if let evaluationActivitySubjectaddedByName =  dictionary[resultsJsonConstants.evaluationActivitySubject.addedByName] as? String {
            addedByName = evaluationActivitySubjectaddedByName
        }
        if let evaluationActivitySubjectupcoming =  dictionary[resultsJsonConstants.evaluationActivitySubject.upcoming] as? String {
            upcoming = evaluationActivitySubjectupcoming
        }
        if let evaluationActivitySubjectstatus =  dictionary[resultsJsonConstants.evaluationActivitySubject.status] as? String {
            status = evaluationActivitySubjectstatus
        }
        if let evaluationActivitySubjectschoolClassName =  dictionary[resultsJsonConstants.evaluationActivitySubject.schoolClassName] as? String {
            schoolClassName = evaluationActivitySubjectschoolClassName
        }
        if let evaluationActivitySubjectschoolClassId =  dictionary[resultsJsonConstants.evaluationActivitySubject.schoolClassId] as? Int {
            schoolClassId = evaluationActivitySubjectschoolClassId
        }
        if let evaluationActivitySubjectevaluationActivityName =  dictionary[resultsJsonConstants.evaluationActivitySubject.evaluationActivityName] as? String {
            evaluationActivityName = evaluationActivitySubjectevaluationActivityName
        }
        if let atevaluvationList = dictionary[resultsJsonConstants.classTest.evaluationActivity] as? NSArray {
            for evaluvation in atevaluvationList {
                let feeValue = evaluationActivity(withDictionaryFromWebService: evaluvation as! [String : AnyObject])
                evaluvationListArray.append(feeValue)
            }
            
        }
        
    }
}
class  evaluationActivitySubjectArray {
    class func parseThisDictionaryForevaluationActivitySubjectRecords(dictionary:[String:AnyObject])->([evaluationActivitySubject]){
        
        var evaluationActivitySubjectRecordArray = [evaluationActivitySubject]()
        
        if let studentEvaluationSubjectList = dictionary[resultsJsonConstants.evaluationActivitySubject.studentEvaluationSubjectList] as? [String:AnyObject] {
            if let classSubjectListArray = studentEvaluationSubjectList[resultsJsonConstants.evaluationActivitySubject.examDetails] as? [String:AnyObject] {
            
                    let atcObj = evaluationActivitySubject(withDictionaryFromWebService: classSubjectListArray)
                    evaluationActivitySubjectRecordArray.append(atcObj)
                    
            }
        }
        
        return(evaluationActivitySubjectRecordArray)
    }
    
    class func parseThisDictionaryForevaluationActivitySubjectClassTestRecords(dictionary:[String:AnyObject])->([evaluationActivitySubject]){
        
        var evaluationActivitySubjectclasstestRecordArray = [evaluationActivitySubject]()
        
        if let studentEvaluationSubjectList = dictionary[resultsJsonConstants.evaluationActivitySubject.evaluationActivitySubject] as? [String:AnyObject] {
            
                let atcObj = evaluationActivitySubject(withDictionaryFromWebService: studentEvaluationSubjectList)
                evaluationActivitySubjectclasstestRecordArray.append(atcObj)
                
            
        }
        
        return(evaluationActivitySubjectclasstestRecordArray)
    }
    
}
