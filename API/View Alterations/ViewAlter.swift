//
//  ViewAlter.swift
//  parentEye
//
//  Created by Martin Jacob on 14/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
class ViewAlter {
    
    class func addBorderToView(view:UIView){
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red:244.0/255.0, green:244.0/255.0, blue:244.0/255.0, alpha: 1.0).cgColor
    }
    
}
