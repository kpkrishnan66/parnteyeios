//
//  Contacts.swift
//  parentEye
//
//  Created by scientia on 27/12/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

class Contacts {
    
    var name:String = ""
    var employeeId:Int = 0
    var mobileNo:String = ""
    var profilePicUrl:String = ""
    var designation:String = ""
    
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atEmpId = dictionary[resultsJsonConstants.contactListReport.id] as? Int{
            self.employeeId = atEmpId
        }else {
            self.employeeId = 0
        }
        
        if let atEmpName = dictionary[resultsJsonConstants.contactListReport.name] as? String{
            self.name = atEmpName
        }else {
            self.name = ""
        }
        
        if let atEmpMob = dictionary[resultsJsonConstants.contactListReport.mobileNo] as? String{
            self.mobileNo = atEmpMob
        }else {
            self.mobileNo = ""
        }
        
        if let atProfilePicUrl = dictionary[resultsJsonConstants.contactListReport.profilePicUrl] as? String{
            self.profilePicUrl = atProfilePicUrl
        }else {
            self.profilePicUrl = ""
        }
        if let atEmpDesg = dictionary[resultsJsonConstants.contactListReport.designation] as? String{
            self.designation = atEmpDesg
        }else {
            self.designation = ""
        }
    }
}

class  parseContactListArray {
    class func parseThisDictionaryForContactListRecords(dictionary:[String:AnyObject])->([Contacts]){
        
        var contactListRecordArray = [Contacts]()
        
        if let contactListRecord = dictionary[resultsJsonConstants.contactListReport.employeeList] as? [AnyObject]{
            for dicto in contactListRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = Contacts(withDictionaryFromWebService: record){
                        contactListRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(contactListRecordArray)
    }
    
}
