//
//  recipientRoles.swift
//  parentEye
//
//  Created by scientia on 11/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class recipientRoles {
    
    var id:Int = 0
    var name:String = ""
    var value:String = ""
    var selected:Bool = false
    var canEdit:Bool = false
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atId = dictionary[resultsJsonConstants.recipientRoles.id] as? Int{
            self.id = atId
        }else {
            self.id = 0
        }
        
        if let atName = dictionary[resultsJsonConstants.recipientRoles.name] as? String{
            self.name = atName
        }else {
            self.name = ""
        }
        
        if let atValue = dictionary[resultsJsonConstants.recipientRoles.value] as? String{
            self.value = atValue
        }else {
            self.value = ""
        }
        
        if let atEditVal = dictionary[resultsJsonConstants.recipientRoles.canEdit] as? Bool{
            self.canEdit = atEditVal
        }
        
       
    }
    init?(afterAddOrEditNewGroup id:Int,name:String,selection:Bool) {
        self.id = id
        self.name = name
        self.selected = selection
    }
}

class  recipientRolesListArray {
    class func parseThisDictionaryForContactListRecords(dictionary:[String:AnyObject])->([recipientRoles]){
        
        var recipientRolesListRecordArray = [recipientRoles]()
        
        if let recipientRolesListRecord = dictionary[resultsJsonConstants.recipientRoles.recipientRoles] as? [AnyObject]{
            for dicto in recipientRolesListRecord{// parse the array in the root
                if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                {
                    
                    if let atcObj = recipientRoles(withDictionaryFromWebService: record){
                        recipientRolesListRecordArray.append(atcObj)
                    }
                }
            }
        }
        
        return(recipientRolesListRecordArray)
    }
    
}
