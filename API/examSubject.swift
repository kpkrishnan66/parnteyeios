//
//  examSubject.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 19/09/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit

class examSubject :NSObject {

    var examSubjectName = ""
    var isBranched:Bool = false
    var examSubjectsList = [examSubject]()
    var examSubjectMarkList = [String] ()
    var displayed = false
    
    override init(){
        
    }
    func setExamSubjctName(SubjectName:String) {
        self.examSubjectName = SubjectName
    }
    
    func getExamSubjectName() -> String {
        return examSubjectName
    }
    
    func setBranched(branchedornot:Bool){
         self.isBranched = branchedornot
    }
    func getIsBranched() -> Bool{
        return isBranched
    }
    
    func setExamSubjectList(subjectList:[examSubject]){
       self.examSubjectsList = subjectList
    }
    func getExamSubjectList() -> [examSubject]{
        return examSubjectsList
    }
    func setexamSubjectMarkList(subjectList:[String]){
        self.examSubjectMarkList = subjectList
    }
    func getExamSubjectMarkList() -> [String]{
        return examSubjectMarkList
    }
    
    
   
}