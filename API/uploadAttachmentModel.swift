//
//  uploadAttachmentModel.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 15/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

class uploadAttachmentModel {
    
    
    var attachmentId:Int   = -1
    var attachmentMimeType = ""
    var attachmentUrl      = ""
    var attachmentType     = ""
    var attachmentName     = ""
    
    
    init?(withDictionaryFromWebService dictionary:[String:AnyObject]){
        
        if let atcID = dictionary[resultsJsonConstants.noticeUpload.attachmentId] as? Int{
            self.attachmentId = atcID
        }else {
            self.attachmentId = -1
        }
        
        if let resp = dictionary[resultsJsonConstants.noticeUpload.attachment] as? [String:AnyObject]{
            
            if let atcMimeType = resp[resultsJsonConstants.noticeUpload.mimeType] as? String{
                self.attachmentMimeType = atcMimeType
            }else {
                self.attachmentMimeType = ""
            }
            
        }
        
        if let resp = dictionary[resultsJsonConstants.noticeUpload.attachment] as? [String:AnyObject]{
            
            if let atcurl = resp[resultsJsonConstants.noticeUpload.imageUrl] as? String{
                self.attachmentUrl = atcurl
            }else {
                self.attachmentUrl = ""
            }
            
        }
        
        if let resp = dictionary[resultsJsonConstants.noticeUpload.attachment] as? [String:AnyObject]{
            
            if let atType = resp[resultsJsonConstants.noticeUpload.type] as? String{
                self.attachmentType = atType
            }else {
                self.attachmentType = ""
            }
            
        }
        
        if let resp = dictionary[resultsJsonConstants.noticeUpload.attachment] as? [String:AnyObject]{
            
            if let atName = resp[resultsJsonConstants.noticeUpload.realName] as? String{
                self.attachmentName = atName
            }else {
                self.attachmentName = ""
            }
            
        }

    
    }
    
    init?(withDictionaryFromWebServiceForMessage dictionary:[String:AnyObject]){
        
        let dictionary1 = dictionary[resultsJsonConstants.messageResultContants.attachment] as? [String:AnyObject]
        
        if let atcID = dictionary[resultsJsonConstants.noticeUpload.attachmentId] as? Int{
            self.attachmentId = atcID
        }else {
            self.attachmentId = -1
        }
        
        
        if let atcMimeType = dictionary1![resultsJsonConstants.noticeUpload.mimeType] as? String{
            self.attachmentMimeType = atcMimeType
        }else {
            self.attachmentMimeType = ""
        }

        
    
        if let atcurl = dictionary1![resultsJsonConstants.noticeUpload.imageUrl] as? String{
            self.attachmentUrl = atcurl
        }else {
            self.attachmentUrl = ""
        }
        
        if let atctype = dictionary1![resultsJsonConstants.noticeUpload.type] as? String{
            self.attachmentType = atctype
        }else {
            self.attachmentType = ""
        }

        if let atcname = dictionary1![resultsJsonConstants.noticeUpload.realName] as? String{
            self.attachmentName = atcname
        }else {
            self.attachmentName = ""
        }

  
    }
    
}

 
class  parseAttachmentArray {
    class func parseThisDictionaryForAttachments(dictionary:[String:AnyObject])->([uploadAttachmentModel]){
        
        var attachmentArray = [uploadAttachmentModel]()
        
            if let imgDicto = dictionary[resultsJsonConstants.noticeUpload.noticeAttachment] as? [String:AnyObject]{
                if let atcObj = uploadAttachmentModel(withDictionaryFromWebService: imgDicto){
                    attachmentArray.append(atcObj)
                }
            }
        
        return(attachmentArray)
    }
    
}

class  parseAttachmentArrayForDiary {
    class func parseThisDictionaryForAttachments(dictionary:[String:AnyObject])->([uploadAttachmentModel]){
        
        var attachmentArray = [uploadAttachmentModel]()
        
        if let imgDicto = dictionary[resultsJsonConstants.noticeUpload.diaryAttachment] as? [String:AnyObject]{
            if let atcObj = uploadAttachmentModel(withDictionaryFromWebService: imgDicto){
                attachmentArray.append(atcObj)
            }
        }
        
        return(attachmentArray)
    }
}

class  parseAttachmentArrayForMessage {
    class func parseThisDictionaryForAttachments(dictionary:[String:AnyObject])->([uploadAttachmentModel]){
        
        var attachmentArray = [uploadAttachmentModel]()
        
        if let imgDicto = dictionary[resultsJsonConstants.noticeUpload.messageAttachment] as? [String:AnyObject]{
            if let atcObj = uploadAttachmentModel(withDictionaryFromWebService: imgDicto){
                attachmentArray.append(atcObj)
            }
        }
        
        return(attachmentArray)
    }
}


 class  parseAttachmentArrayForHomework {
        class func parseThisDictionaryForAttachments(dictionary:[String:AnyObject])->([uploadAttachmentModel]){
            
            var attachmentArray = [uploadAttachmentModel]()
            
            if let imgDicto = dictionary[resultsJsonConstants.homeworkUpload.homeworkAttachment] as? [String:AnyObject]{
                if let atcObj = uploadAttachmentModel(withDictionaryFromWebService: imgDicto){
                    attachmentArray.append(atcObj)
                }
            }
            
            return(attachmentArray)
        }

}




