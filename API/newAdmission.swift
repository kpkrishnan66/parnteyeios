//
//  newAdmission.swift
//  parentEye
//
//  Created by scientia on 26/04/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class newAdmission : NSObject,NSCoding {
    
    
    var messageToDisplay    = ""
    var amount              = ""
    var feeType             = ""
    var id                  = 0
    var dateOfBirth         = ""
    var motherName          = ""
    var fatherName          = ""
    var address             = ""
    var emailId             = ""
    var studentName         = ""
    var className           = ""
    var feePaymentStatus    = ""
    var mobileNo           = ""
    var appliedOn          = ""
    var nativeResident     = ""
    
    init?(withDictionaryFromWebServiceFeepaymentDetails feeDictionary:[String:AnyObject],withDictionaryFromWebServiceAdmissionDetails admissionDictionary:[String:AnyObject]){
        
        if let atmessageToDisplay = feeDictionary[resultsJsonConstants.newAdmissionPay.messageToDisplay] as? String{
            self.messageToDisplay = atmessageToDisplay
        }else {
            self.messageToDisplay = ""
        }
        if let atamount = feeDictionary[resultsJsonConstants.newAdmissionPay.amount] as? String{
            self.amount = atamount
        }else {
            self.amount = ""
        }
        if let atfeeType = feeDictionary[resultsJsonConstants.newAdmissionPay.feeType] as? String{
            self.feeType = atfeeType
        }else {
            self.feeType = ""
        }
        
        if let atid = admissionDictionary[resultsJsonConstants.newAdmissionPay.id] as? Int{
            self.id = atid
        }else {
            self.id = 0
        }
        if let atdateOfBirth = admissionDictionary[resultsJsonConstants.newAdmissionPay.dateOfBirth] as? String{
            self.dateOfBirth = atdateOfBirth
        }else {
            self.dateOfBirth = ""
        }
        
        if let atmotherName = admissionDictionary[resultsJsonConstants.newAdmissionPay.motherName] as? String{
            self.motherName = atmotherName
        }else {
            self.motherName = ""
        }
        
        if let atfatherName = admissionDictionary[resultsJsonConstants.newAdmissionPay.fatherName] as? String{
            self.fatherName = atfatherName
        }else {
            self.fatherName = ""
        }
        
        if let ataddress = admissionDictionary[resultsJsonConstants.newAdmissionPay.address] as? String{
            self.address = ataddress
        }else {
            self.address = ""
        }
        
        if let atemailId = admissionDictionary[resultsJsonConstants.newAdmissionPay.emailId] as? String{
            self.emailId = atemailId
        }else {
            self.emailId = ""
        }
        
        if let atstudentName = admissionDictionary[resultsJsonConstants.newAdmissionPay.studentName] as? String{
            self.studentName = atstudentName
        }else {
            self.studentName = ""
        }
        
        if let atclassName = admissionDictionary[resultsJsonConstants.newAdmissionPay.className] as? String{
            self.className = atclassName
        }else {
            self.className = ""
        }
        
        if let atfeePaymentStatus = admissionDictionary[resultsJsonConstants.newAdmissionPay.feePaymentStatus] as? String{
            self.feePaymentStatus = atfeePaymentStatus
        }else {
            self.feePaymentStatus = ""
        }
        
        if let atmobileNo = admissionDictionary[resultsJsonConstants.newAdmissionPay.mobileNo] as? String{
            self.mobileNo = atmobileNo
        }else {
            self.mobileNo = ""
        }
        
        if let atappliedOn = admissionDictionary[resultsJsonConstants.newAdmissionPay.appliedOn] as? String{
            self.appliedOn = atappliedOn
        }else {
            self.mobileNo = ""
        }
        
        if let atnativeResident = admissionDictionary[resultsJsonConstants.newAdmissionPay.nativeResident] as? String{
            self.nativeResident = atnativeResident
        }else {
            self.nativeResident = ""
        }
        
        
    }
    
    required init(coder aDecoder: NSCoder) {
        
         self.messageToDisplay = aDecoder.decodeObject(forKey:"messageToDisplay") as! String
         self.amount = aDecoder.decodeObject(forKey:"amount") as! String
         self.feeType = aDecoder.decodeObject(forKey:"feeType") as! String
         self.id = aDecoder.decodeObject(forKey:"id") as! Int
         self.dateOfBirth = aDecoder.decodeObject(forKey:"dateOfBirth") as! String
         self.motherName = aDecoder.decodeObject(forKey:"motherName") as! String
         self.fatherName = aDecoder.decodeObject(forKey:"fatherName") as! String
         self.address = aDecoder.decodeObject(forKey:"address") as! String
         self.emailId = aDecoder.decodeObject(forKey:"emailId") as! String
         self.studentName = aDecoder.decodeObject(forKey:"studentName") as! String
         self.className = aDecoder.decodeObject(forKey:"className") as! String
         self.feePaymentStatus = aDecoder.decodeObject(forKey:"feePaymentStatus") as! String
         self.mobileNo = aDecoder.decodeObject(forKey:"mobileNo") as! String
         self.appliedOn = aDecoder.decodeObject(forKey:"appliedOn") as! String
         self.nativeResident = aDecoder.decodeObject(forKey:"nativeResident") as! String
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(messageToDisplay, forKey: "messageToDisplay")
        aCoder.encode(amount, forKey: "amount")
        aCoder.encode(feeType, forKey: "feeType")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(dateOfBirth, forKey: "dateOfBirth")
        aCoder.encode(motherName, forKey: "motherName")
        aCoder.encode(fatherName, forKey: "fatherName")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(emailId, forKey: "emailId")
        aCoder.encode(studentName, forKey: "studentName")
        aCoder.encode(className, forKey: "className")
        aCoder.encode(feePaymentStatus, forKey: "feePaymentStatus")
        aCoder.encode(mobileNo, forKey: "mobileNo")
        aCoder.encode(appliedOn, forKey: "appliedOn")
        aCoder.encode(nativeResident, forKey: "nativeResident")

    }
}

class  parseNewAdmissionArray {
    class func parseThisDictionaryForNewAdmissionRecords(dictionary:[String:AnyObject])->([newAdmission]){
        
        var newAdmissionRecordArray = [newAdmission]()
        var feeDetailsDictionary = [String:AnyObject]()
        var admissionDetailsDictionary = [String:AnyObject]()
        
        if let newAdmissionRecord = dictionary[resultsJsonConstants.newAdmissionPay.StudentAdmission] as? NSDictionary {
            if let feepaymentDetails = newAdmissionRecord[resultsJsonConstants.newAdmissionPay.feepaymentDetails] as? [String:AnyObject] {
                
              feeDetailsDictionary = feepaymentDetails
                    
            }
            
            if let admissionDetails = newAdmissionRecord[resultsJsonConstants.newAdmissionPay.admissionDetails] as? [String:AnyObject] {
                
                admissionDetailsDictionary = admissionDetails
            }
        }
        if let atcObj = newAdmission(withDictionaryFromWebServiceFeepaymentDetails: feeDetailsDictionary,withDictionaryFromWebServiceAdmissionDetails:admissionDetailsDictionary){
            newAdmissionRecordArray.append(atcObj)
        }
        return(newAdmissionRecordArray)
    }
    
}
