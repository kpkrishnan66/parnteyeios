//
//  studentExamList.swift
//  parentEye
//
//  Created by Vivek on 17/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation

class studentExamList {
    
    var subjectId:Int      = 0
    var subjectName:String = ""
    var examSubjectId:Int  = 0
    var maxMark:Int        = 0
    var studentId:Int      = 0
    var studentName:String = ""
    var mark:String        = ""
    var studentGender:String = ""
    var rollNo:String        = ""
    var gendRollNo:Int       = 0
    var edit:Bool            = false
    var isValidation         = true
    var savedExamLabelText:String = ""
    var gender:String        = ""
    
    init() {
        
    }
    
    init?(withDictionaryFromWebServiceForsubjectList dictionary:[String:AnyObject]){
        
        if let subId = dictionary[resultsJsonConstants.examList.studentId] as? Int{
            self.studentId = subId
        }else {
            self.studentId = 0
        }
        
        if let sName = dictionary[resultsJsonConstants.examList.studentName] as? String{
            self.studentName = sName
        }else {
            self.studentName = ""
        }
        
        if let mrk = dictionary[resultsJsonConstants.examList.mark] as? String{
            self.mark = mrk
        }else {
            self.mark = ""
        }
        
        if let gnd = dictionary[resultsJsonConstants.examList.studentGender] as? String{
            self.studentGender = gnd
        }else {
            self.studentGender = ""
        }
        
        if let roll = dictionary[resultsJsonConstants.examList.rollNo] as? String{
            self.rollNo = roll
        }else {
            self.rollNo = ""
        }
        self.edit = false
        
    }
    
    init?(withDictionaryFromWebServiceForstudentList dictionary:[String:AnyObject]){
        
        if let subId = dictionary[resultsJsonConstants.examList.subjectId] as? Int{
            self.subjectId = subId
        }else {
            self.subjectId = 0
        }
        
        if let exName = dictionary[resultsJsonConstants.examList.subjectName] as? String{
            self.subjectName = exName
        }else {
            self.subjectName = ""
        }
        
        if let exsubId = dictionary[resultsJsonConstants.examList.examSubjectId] as? Int{
            self.examSubjectId = exsubId
        }else {
            self.examSubjectId = 0
        }
        
        if let exMaxMark = dictionary[resultsJsonConstants.examList.maxMark] as? Int{
            self.maxMark = exMaxMark
        }else {
            self.maxMark = 0
        }
        
    }
    
    init?(withDictionaryFromWebServiceForstudentEvaluationList dictionary:[String:AnyObject]){
        if let exstudentId = dictionary[resultsJsonConstants.examList.id] as? Int{
            self.studentId = exstudentId
        }else {
            self.studentId = 0
        }
        
        if let exmark = dictionary[resultsJsonConstants.examList.mark] as? String{
            self.mark = exmark
        }else {
            self.mark = ""
        }
        
        if let exstudentName = dictionary[resultsJsonConstants.examList.studentName] as? String{
            self.studentName = exstudentName
        }else {
            self.studentName = ""
        }
        
        if let exrollNo = dictionary[resultsJsonConstants.examList.rollNo] as? String{
            self.rollNo = exrollNo
        }else {
            self.rollNo = ""
        }
        
        if let exgender = dictionary[resultsJsonConstants.examList.gender] as? String{
            self.gender = exgender
        }else {
            self.gender = ""
        }
        
        
        
    }
    
    init?(withDictionaryFromWebServiceForclassTestMarkListList dictionary:[String:AnyObject]){
        
        if let exstudentId = dictionary[resultsJsonConstants.examList.id] as? Int{
            self.studentId = exstudentId
        }else {
            self.studentId = 0
        }
        
        if let exstudentName = dictionary[resultsJsonConstants.examList.name] as? String{
            self.studentName = exstudentName
        }else {
            self.studentName = ""
        }
        
        if let exgender = dictionary[resultsJsonConstants.examList.sex] as? String{
            self.gender = exgender
        }else {
            self.gender = ""
        }
        
        if let exmark = dictionary[resultsJsonConstants.examList.mark] as? String{
            self.mark = exmark
        }else {
            self.mark = ""
        }

        
    }
}

class  parseStudentExamListArray {
    
    
    class func parseThisDictionaryForSubjectListRecords(dictionary:[String:AnyObject])->([studentExamList]){
        
        var subjectListRecordArray = [studentExamList]()
        
        if let examListRecord = dictionary[resultsJsonConstants.examList.studentExamList] as? [String:AnyObject]{
            if let subjectList = examListRecord[resultsJsonConstants.examList.subjectList] as? NSArray {
                for dicto in subjectList{// parse the array in the root
                    if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = studentExamList(withDictionaryFromWebServiceForstudentList: record){
                            subjectListRecordArray.append(atcObj)
                        }
                    }
                }
            }
        }
        
        return(subjectListRecordArray)
    }
    
    class func parseThisDictionaryForExamListRecords(dictionary:[String:AnyObject])->([studentExamList]){
        
        var studentExamListRecordArray = [studentExamList]()
        
        if let examListRecord = dictionary[resultsJsonConstants.examList.studentExamList] as? [String:AnyObject]{
            if let subjectList = examListRecord[resultsJsonConstants.examList.studentList] as? NSArray {
                for dicto in subjectList{// parse the array in the root
                    if let record = dicto as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = studentExamList(withDictionaryFromWebServiceForsubjectList: record){
                            studentExamListRecordArray.append(atcObj)
                        }
                    }
                }
            }
        }
        
        return(studentExamListRecordArray)
    }
    
    class func parseThisDictionaryForstudentEvaluationSubjectStudentListRecords (dictionary:[String:AnyObject])->([studentExamList]) {
        var studentExamListRecordArray = [studentExamList]()
        
        if let studentEvaluationSubjectListRecord = dictionary[resultsJsonConstants.evaluationActivitySubject.studentEvaluationSubjectList] as? [String:AnyObject]{
            
                
            if let studentList = studentEvaluationSubjectListRecord[resultsJsonConstants.examList.studentList] as? [String:AnyObject] {
                for (_,studentIdObject) in studentList{// parse the array in the root
                    if let studentObject = studentIdObject as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = studentExamList(withDictionaryFromWebServiceForstudentEvaluationList: studentObject){
                            studentExamListRecordArray.append(atcObj)
                        }
                       
                    }
                 }
               
            }
        }
        
        return(studentExamListRecordArray)
    }
    
    class func parseThisDictionaryForsclassTestMarkListRecords (dictionary:[String:AnyObject])->([studentExamList]) {
        var classTestMarkListRecordArray = [studentExamList]()
        
            if let studentList = dictionary[resultsJsonConstants.examList.classTestMarkList] as? [String:AnyObject] {
                for (_,studentIdObject) in studentList{// parse the array in the root
                    if let studentObject = studentIdObject as? [String:AnyObject] // check if the thing is dictionary of the format
                    {
                        
                        if let atcObj = studentExamList(withDictionaryFromWebServiceForclassTestMarkListList: studentObject){
                            classTestMarkListRecordArray.append(atcObj)
                        }
                        
                    }
                }
        }
        
        return(classTestMarkListRecordArray)
    }
}
