//
//  HudControllerClass.swift
//  parentEye
//
//  Created by Martin Jacob on 16/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import PKHUD
class hudControllerClass{

   class func showNormalHudToViewController(viewController:UIViewController) -> Void {
      HUD.show(.systemActivity)
    }
    
   class func hideHudInViewController(viewController:UIViewController) -> Void {
        HUD.hide()
    }
    class func hideHudInViewControllerForUIView(viewController:UIView) -> Void {
        HUD.hide()
    }
    class func showNormalHudToViewControllerForUIView(viewController:UIView) -> Void {
        HUD.show(.systemActivity)
    }
    

}
