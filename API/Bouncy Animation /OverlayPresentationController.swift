//
// Copyright 2014 Scott Logic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import UIKit

class OverlayPresentationController: UIPresentationController {
   let dimmingView = UIView()
   var valueForDy:CGFloat = 30
    
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        
        NotificationCenter.default.addObserver(self, selector: #selector(methodOFReceivedNotication), name:NSNotification.Name(rawValue: "NotificationIdentifierForPopUp"), object: nil)
        
        // code added by martin J
        let tap = UITapGestureRecognizer(target: self, action:#selector(OverlayPresentationController.removeSelf))
        dimmingView.addGestureRecognizer(tap)
        
        // code added by martin J end
        dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        // dimmingView.addBlurView()
        
    }
    
    override func presentationTransitionWillBegin() {
        
        dimmingView.frame = containerView!.bounds
        dimmingView.alpha = 0.0
        containerView!.insertSubview(dimmingView, at: 0)
        
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: {
            context in
            self.dimmingView.alpha = 1.0
        }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator?.animate(alongsideTransition: {
            context in
            self.dimmingView.alpha = 0.0
        }, completion: {
            context in
            self.dimmingView.removeFromSuperview()
            
        })
    }
    
    override var frameOfPresentedViewInContainerView : CGRect {
        return containerView!.bounds.insetBy(dx: 30, dy: valueForDy)
        
        //var rect :CGRect = CGRectMake(containerView.bounds.origin.x, containerView.bounds.origin.y+20, containerView.bounds.width, containerView.bounds.height-20)
        // return rect
    }
    
    override func containerViewWillLayoutSubviews() {
        
        dimmingView.frame = containerView!.bounds
        presentedView!.frame = frameOfPresentedViewInContainerView
    }
    
    @objc func removeSelf(){
        self.presentedViewController.dismiss(animated: true, completion: nil)
        
    }
    @objc open func methodOFReceivedNotication(_ notification: Notification) {
        valueForDy = 150
    }
}
