//
//  HamburgerMenu.swift
//  parentEye
//
//  Created by Martin Jacob on 11/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import SlideMenuControllerSwift

class hamburger {
    class func addHambugerWithLeftController(leftController:UIViewController, mainController mainVC:UIViewController)->UIViewController {
        
        return dekatotoro.addHambugerWithLeftController(leftController: leftController, mainController: mainVC)
    }
}


class dekatotoro: SlideMenuController {
    
    class func addHambugerWithLeftController(leftController:UIViewController, mainController mainVC:UIViewController)->SlideMenuController {
        
       return SlideMenuController(mainViewController: mainVC, leftMenuViewController: leftController)
    }
}
