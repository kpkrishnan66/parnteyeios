//
//  notificationDetails.swift
//  parentEye
//
//  Created by scientia on 20/04/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
class notificationDetails {
    
    var entryType:typeOfList?
    var diaryArray:DiaryModelClass?
    var noticeArray:NotificationModel?
    var homeworkArray:HomeWorkModel?
    var messageArray:messageForListing?
    var studentLeaveRecordArray:StudentAbsentRecord?
    var StudentLeaveSubmitRecordArray:StudentAbsentRecord?
    var externalNotificationArray:externalNotificationModel?
    var title    = ""
    var from     = ""
    var type     = ""
    var userId   = ""
    var userRoleId = ""
    var studentId = ""
    
    
    init?(withDictionaryFromWebServicenotificationDetails notificationDictionary:[String:AnyObject]){
        
        
        if let noti_title = notificationDictionary[resultsJsonConstants.notificationDetails.title] as? String{
            self.title = noti_title
        }else {
            self.title = ""
        }
        
        if let noti_from = notificationDictionary[resultsJsonConstants.notificationDetails.from] as? String{
            self.from = noti_from
        }else {
            self.from = ""
        }
        
        if let noti_type = notificationDictionary[resultsJsonConstants.notificationDetails.type] as? String{
            self.type = noti_type
        }else {
            self.type = ""
        }
        
        if let noti_userId = notificationDictionary[resultsJsonConstants.notificationDetails.userId] as? Int{
            self.userId = String(noti_userId)
        }else {
            self.userId = ""
        }
        if let noti_userRoleId = notificationDictionary[resultsJsonConstants.notificationDetails.userRoleId] as? Int{
            self.userRoleId = String(noti_userRoleId)
        }else {
            self.userRoleId = ""
        }
        
        if let noti_studentId = notificationDictionary[resultsJsonConstants.notificationDetails.studentId] as? Int{
            self.studentId = String(noti_studentId)
        }else {
            self.studentId = ""
        }
        
     if let typeOfNotification = notificationDictionary[resultsJsonConstants.notificationDetails.type] as? String{
      switch typeOfNotification {
        
       case resultsJsonConstants.notificationDetails.notificationTypeDiary:
        entryType = typeOfList.diary
        if let diaryObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let diary = DiaryModelClass(fromDictionaryFromWebService: diaryObject, assignDateValue: "")
            self.diaryArray = diary
        }
      
        
      case resultsJsonConstants.notificationDetails.notificationTypNotice:
        entryType = typeOfList.notice
        if let noticeObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let notice = NotificationModel(fromDictionaryFromWebService: noticeObject, withDate: "")
            self.noticeArray = notice
        }
        
        
      case resultsJsonConstants.notificationDetails.notificationTypHomework:
        entryType = typeOfList.homeWork
        if let homeworkObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let homework = HomeWorkModel(fromDictionaryFromWebService: homeworkObject, assignhomeWorkValue: "")
            self.homeworkArray = homework
        }
        
      case resultsJsonConstants.notificationDetails.notificationTypMessage:
        entryType = typeOfList.message
        if let messageObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let message = messageForListing(withDictionaryFromWebService: messageObject,readViaPush: true)
            self.messageArray = message
        }
        
      case resultsJsonConstants.notificationDetails.notificationTypStudentLeaveRecord:
        entryType = typeOfList.StudentLeaveRecord
        if let leaveRecordObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let leave = StudentAbsentRecord(withDictionaryFromWebService: leaveRecordObject)
            self.studentLeaveRecordArray = leave
        }
        
      case resultsJsonConstants.notificationDetails.notificationTypStudentLeaveSubmit:
        entryType = typeOfList.StudentLeaveSubmit
        if let leaveSubmitRecordObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let leave = StudentAbsentRecord(withDictionaryFromWebService: leaveSubmitRecordObject)
            self.StudentLeaveSubmitRecordArray = leave
        }
        
      case resultsJsonConstants.notificationDetails.notificationTypExternalNotification:
        entryType = typeOfList.externalNotification
        if let externalNotificationRecordObject = notificationDictionary[resultsJsonConstants.notificationDetails.object] as? [String:AnyObject]{
            let externalNotification = externalNotificationModel(withDictionaryFromWebService: externalNotificationRecordObject)
            self.externalNotificationArray = externalNotification
        }
        default:
            entryType = typeOfList.none
        }
     }
    }
}

class  parseNotificationDetailsArray {
    class func parseThisDictionaryForotificationDetailsRecords(dictionary:[String:AnyObject])->([notificationDetails]){
        
        var notificationDetailsRecordArray = [notificationDetails]()
        
        
        if let notificationDetailsRecord = dictionary[resultsJsonConstants.notificationDetails.notificationDetails] as? [String:AnyObject]{
            
            if let atcObj = notificationDetails(withDictionaryFromWebServicenotificationDetails: notificationDetailsRecord){
                notificationDetailsRecordArray.append(atcObj)
            }
        }
        
        return(notificationDetailsRecordArray)
    }
}

