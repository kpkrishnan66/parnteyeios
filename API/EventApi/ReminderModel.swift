    //
//  ReminderModel.swift
//  parentEye
//
//  Created by Martin Jacob on 27/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import EventKit

enum eventAccessStatus {
    case denied
    case succes
    case notDetermined
}

class EventModel {

    static let sharedInstance = EventModel()
    let store = EKEventStore()
    var calendar:EKCalendar?
    private  init() {
        
    }
    
    public func checkIfWeAreAuthorizedToAccessEvents ()->(areWeAuthorized:eventAccessStatus, whyAreWeNotAuthorized:String){
        
        var areWeAuthorized = eventAccessStatus.denied
        var reasonForFailure = ""
        
        let authorizationStatus = EKEventStore.authorizationStatus(for: .reminder)
        
        switch authorizationStatus {
            case .denied: areWeAuthorized = .denied
            reasonForFailure = ConstantsInUI.applicationDoesNotHavePermission
            case .authorized : areWeAuthorized = .succes
            case .restricted : areWeAuthorized = .denied
            reasonForFailure = ConstantsInUI.applicationDoesNotHavePermission
            case .notDetermined : areWeAuthorized = .notDetermined
        }
        
        return (areWeAuthorized,reasonForFailure)
    }
    
    public func addReminderWith(title:String, onDate date:NSDate,onSuccess succes:eventSuccess, onFailure failure:eventFailure){
        
        let reminder =  EKReminder(eventStore: store)
        reminder.title = title
        
        if getCalender().result != false{
            
            reminder.calendar = calendar!
            
            do {
                try store.save(reminder, commit: true)
                
            } catch{
                
                failure(NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:ConstantsInUI.reminderAddFailed]))
            }
        }else {
            failure(NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:ConstantsInUI.reminderAddFailed]))
        }
      
    }
    
    private func getCalender() ->(result:Bool,calender:EKCalendar?){
        var result = true
        
        if calendar == nil{
            let calendarArray = store.calendars(for: .reminder)
            let calendarTitle = "Parent-Eye"
            let predicate = NSPredicate(format: "title matches %@", calendarTitle)
            let filteredArray = calendarArray.filter{predicate.evaluate(with: $0)}
            
            if filteredArray.count > 0 {
                 calendar =  filteredArray[0]
                calendar?.source =  store.defaultCalendarForNewReminders()?.source
            }else {
                calendar =  EKCalendar(for: .reminder, eventStore: store)
                calendar?.title = "Parent-Eye"
                calendar?.source =  store.defaultCalendarForNewReminders()?.source
                do {
                    try store.saveCalendar(calendar!, commit: true)
                } catch {
                    result = false
                }
            }
        }else {
            //calendar?.source = store.defaultCalendarForNewReminders().source
        }
        return (result,calendar)
    }
    
    public func requestPermission (onGivingPermission succes:@escaping eventSuccess, onNotGivingPermission failure:@escaping eventFailure){
        store.requestAccess(to: .reminder) { (result, error) in
            if result == true {
                succes("")
            }else {
                failure(NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:ConstantsInUI.userDeniedPermission]))
            }
        }
    }
}
