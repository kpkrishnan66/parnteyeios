//
//  EventController.swift
//  parentEye
//
//  Created by Martin Jacob on 27/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

public typealias  eventSuccess  = (_ result: String) -> Void
public typealias  eventFailure  = (_ error:NSError)->Void



class EventController {
    
    var title = ""
    var date = NSDate()
    var selfSucces:eventSuccess?
    var selfFailure:eventFailure?
    
    public func addReminderWithTitle(title:String, Ondate date:NSDate, onSuccess succes:@escaping eventSuccess, onFailure failure:@escaping eventFailure){
        
        
        
        let eventStatus = EventModel.sharedInstance.checkIfWeAreAuthorizedToAccessEvents()
        
        switch eventStatus.areWeAuthorized {
            case .denied:
                failure(NSError(domain: "", code: 1, userInfo: [NSLocalizedDescriptionKey:ConstantsInUI.applicationDoesNotHavePermission]))
            case .notDetermined:
                weak var  weakSelf = self
                self.title = title
                self.date = date
                selfSucces = succes
                selfFailure = failure
                EventModel.sharedInstance.requestPermission(onGivingPermission: { (result) in
                    if weakSelf != nil {
                        weakSelf?.addReminderWithSubject(title: (weakSelf?.title)!, Ondate: (weakSelf?.date)!, onSuccess: weakSelf!.selfSucces!, onFailure: weakSelf!.selfFailure!)
                    }
                    
            }, onNotGivingPermission: { (error) in
                failure(error)
        })
        
        case .succes: addReminderWithSubject(title: title, Ondate: date, onSuccess: succes, onFailure: failure)
        }
        
    }
    
    private func addReminderWithSubject(title:String, Ondate date:NSDate, onSuccess succes:eventSuccess, onFailure failure:eventFailure){
        
        EventModel.sharedInstance.addReminderWith(title: title, onDate: date, onSuccess: succes, onFailure: failure)
        
    }

}
