//
//  UserDefaultConstants.swift
//  parentEye
//
//  Created by Martin Jacob on 19/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
struct userDerfaultConstants {
    
    static let isLoggedIn          = "isLoggedIn"
    static let userData            = "userData"
    static let deviceToken         = "deviceToken"
    
    static let DiaryUnreadNumber   = "unreadDiary"
    static let noticeUnreadNumber  = "noticeUnread"
    static let messageUnreadNumber = "messageUnread"
    static let dailyCareUnread     = "dailyCareUnread"
    static let leave               = "leave"
    static let deviceTokenRegistered = "deviceTokenRegistered"
    
    
}

