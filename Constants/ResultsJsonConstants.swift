//
//  ResultsJsonConstants.swift
//  parentEye
//
//  Created by Martin Jacob on 18/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
struct resultsJsonConstants {
    
    
    struct loginConstants  {
        static let userlogin = "user"
        static let message   = "message"
    }
    
    
    struct messageResultContants {
        static let messageRecipientList     = "messageList"
        static let messageF                 = "message"
        static let messageAddedBy           = "addedBy"
        static let messageTitle             = "subject"
        static let messageContent           = "content"
        static let messageType              = "type"
        static let messageDate              = "date"
        static let messageSenderDesignation = "designation"
        static let messageSenderProfilePIc  = "profilePic"
        static let messageSenderName        = "fromName"
        static let messageIsReadOrnot       = "status"
        static let messageId                = "id"
        static let messageThreadID          = "threadId"
        static let messageUserRole          = "userRole"
        static let messageRecipients        = "recipients"
        static let messageToFromLabel       = "toFromLabel"
        static let attachmentArray         = "attachments"
        static let attachment              = "attachment"
        static let attachmentImageUrl      = "url"
        static let attachmentTypeKey       = "type"
        static let attachmentTypeImage     = "Image"
        static let uniqueNameKey           = "uniqueName"
        static let messageCanReply         = "canReply"
        
        static let messageRecipientName     = "name"
        static let messagerecipientId       = "id"
        static let masterMessageSenderID    = "id"
        static let masterMesageOtherGuyID   = "replyRecipient"
        static let messageTheOtherGuysIDKey = "otherUserRoleId"
        static let messageRecipientRoles    = "recipientRoles"
        static let messageAddedById         = "addedById"
        
    }
    
    struct diaryResultsConstants {
        static let studentDiary            = "diary"
        static let deliveryReport          = "deliveryReport"
        static let classList               = "classList"
        static let student                 = "student"
        static let studentIdDiary          = "id"
        static let diary                   = "diary"
        static let diaryId                 = "id"
        static let diarySubject            = "subject"
        static let diaryContent            = "content"
        static let hasAttachment           = "attachments"
        static let recipientDetails        = "recipientDetails"
        static let attachmentArray         = "attachments"
        static let attachment              = "attachment"
        static let attachmentImageUrl      = "url"
        static let diaryAddedDate          = "addedDate"
        static let diaryDateSeparator      = "-"
        static let diaryEntryReadStatusKey = "status"
        static let diaryEntryReadStatus    = "Unread"
        static let whomThisDiaryIsFrom     = "from"
        static let attachmentTypeKey       = "type"
        static let attachmentTypeImage     = "Image"
        static let uniqueNameKey           = "uniqueName"
        static let audioType               = "Audio"
        
        static let deliveryReportId           = "id"
        static let deliveryReportstudentName  = "studentName"
        static let deliveryReportmobileNo     = "mobileNo"
        static let deliveryReportclassName    = "className"
        static let deliveryReportprofilePic   = "profilePic"
        static let deliveryReportstudentId    = "studentId"
        static let deliveryReportaddedTime    = "addedTime"
        static let deliveryReportaddedDate    = "addedDate"
        static let deliveryReportupdatedTime  = "updatedTime"
        static let deliveryReportupdatedDate  = "updatedDate"
        static let deliveryReportstatus       = "status"
        static let deliveryReportreadvia       = "readVia"
        
        static let diaryClassId       = "id"
        static let diaryClassName     = "name"
        
        
    }
    
    struct timeTableConstants{
        static let schoolClass = "schoolClass"
        static let timeTable = "timeTable"
        static let id = "id"
        static let className = "className"
        static let subjectName = "subjectName"
        static let subjectNickName = "subjectNickName"
        static let classId = "classId"
        static let substitutionDetails = "substitutionDetails"
    }
    
    struct upComingEventsConstants {
        static let upComingEventsList = "upcomingEventList"
        static let typeOfListEntry    = "type"
        static let eventTypeDiary     = "Diary"
        static let eventTypeNotice    = "Notice"
        static let eventTypeHomework  = "Homework"
        static let eventTypeClassTest = "EvaluationActivitySubject"
        static let eventObject        = "object"
        static let titleOfEvent       = "content"
        static let DateOfEvent        = "submissionDate"
    }
    
    struct profileImageChange {
        static let response   = "response"
        static let studentImg = "studentImage"
        static let userImage = "userImage"
        static let imageUrl   = "imageUrl"
    }
    
    struct noticeUpload {
        static let response         = "response"
        static let attachment       = "attachment"
        static let noticeAttachment = "noticeAttachment"
        static let imageUrl         = "url"
        static let mimeType         = "mimeType"
        static let attachmentId     = "id"
        static let diaryAttachment  = "diaryAttachment"
        static let messageAttachment = "messageAttachment"
        static let type              = "type"
        static let uniqueName        = "uniqueName"
        static let realName          = "realName"
    }
    
    
    struct StudentAbsentRecord {
        
        static let studentAbsentRecord = "studentAbsentRecord"
        static let response            = "response"
        static let id                  = "id"
        static let studentName         = "student"
        static let profilePic          = "profilePic"
        static let mobileNo            = "mobileNo"
        static let reason              = "reason"
        static let date                = "date"
        static let leaveStatus         = "leaveStatus"
        static let status              = "status"
        static let attendanceResult    = "attendanceResult"
        static let description         = "description"
        static let schoolClassId       = "schoolClassId"
    }
    
    struct attendanceReport {
        
        static let attendanceReport   = "attendanceReport"
        static let className          = "class"
        static let noOfAbsentees      = "noOfAbsentees"
        static let totalStudents      = "totalStudents"
        static let teacherName        = "teacherName"
        static let schoolClassId      = "schoolClassId"
        static let message            = "message"
        
    }
    
    struct attendanceClassReport {
        
        static let student              = "student"
        static let rollNo               = "rollNo"
        static let profilePic           = "profilePic"
        static let mobileNo             = "mobileNo"
        static let reason               = "reason"
        static let description          = "description"
        static let studentAbsentRecord  = "studentAbsentRecord"
        
    }
    
    struct contactListReport {
        
        static let employeeList   = "employeeList"
        static let id             = "id"
        static let name           = "name"
        static let mobileNo       = "mobileNo"
        static let profilePicUrl  = "profilePicUrl"
        static let designation    = "designation"
        
    }
    
    struct examList {
        static let id             =  "id"
        static let name           =  "name"
        static let examList       =  "examList"
        static let studentExamList =  "studentExamList"
        static let subjectList    =  "subjectList"
        static let studentList    =  "studentList"
        static let subjectId      =  "subjectId"
        static let subjectName    =  "subjectName"
        static let examSubjectId  =  "examSubjectId"
        static let maxMark        =  "maxMark"
        static let studentId    =  "studentId"
        static let studentName  =  "studentName"
        static let mark        =  "mark"
        static let studentGender = "studentGender"
        static let rollNo        = "rollNo"
        static let gender        = "gender"
        static let classTestMarkList = "classTestMarkList"
        static let sex      = "sex"
        
    }
    
    struct locationTransport {
        
        static let location       = "location"
        static let id             = "id"
        static let busName        = "busName"
        static let latitude       = "latitude"
        static let longitude      = "longitude"
        static let tripType       = "tripType"
        static let lastUpdatedOn  = "lastUpdatedOn"
        static let accuracy       = "accuracy"
        
    }
    
    
    
    struct notificationConstants {
        static let noticeRecipient               = "noticeList"
        static let noticeKey                     = "notice"
        static let noticeID                      = "id"
        static let noticeTitle                   = "title"
        static let noticeContent                 = "content"
        static let noticeDate                    = "eventDate"
        static let noticeHasAttachment           = "hasAttachment"
        static let noticeAttachments             = "attachments"
        static let noticeAttachment              = "attachment"
        static let noticeImageUrl                = "url"
        static let noticeStatus                  = "status"
        static let noticeUpComing                = "upcoming"
        static let noticelastUpdatedOn           = "lastUpdatedDate"
        static let noticeReminder                = "noticeReminder"
        static let notificationAddedBy           = "addedBy"
        static let notificationSenderDesignation = "designation"
        static let noticeAttachmentForImage      = "noticeAttachment"
        static let noticeRead                    = "Read"
        static let noticeTypeKey                 = "type"
        static let noticeTypeImage               = "Image"
        static let noticeEventDate               = "eventDate"
    }
    struct dailyReportConstants {
        static let student = "student"
        static let dailyreport = "dailyReport"
        static let waterConsumed = "waterConsumed"
        static let juiceOrMilkConsumed = "juiceOrMilkConsumed"
        static let noOfDiapersOrToiletUsage = "noOfDiapersOrToiletUsage"
        static let checkInTime = "checkInTime"
        static let checkOutTime = "checkOutTime"
        static let request = "request"
        
        static let moodKey = "mood"
        static let moodOftheDay = "name"
        static let moodImageUrl = "imageUrl"
        static let medicactionKey = "medication"
        static let medicineName = "medicine"
        static let medicineTiming = "timing"
        
        static let foodArrayKey = "foodConsumed"
        static let foodMenuKey = "menuRecipe"
        static let foodType = "mealType"
        static let foodreciepie = "recipeName"
        static let foodQuantity = "quantity"
        
        static let napTimingKey  = "napTiming"
        static let napFromTimingKey  = "fromTime"
        static let napToTimingKey  = "toTime"
        
        static let accidentReportKey = "accidentReport"
        static let accidentdescriptionKey = "description"
        static let accidentactionTakenKey = "actionTaken"
        
    }
    struct readUnreadConstants {
        static let unreadCountKey          = "unreadCount"
        static let studentDairyCountKey    = "studentDairy"
        static let noticeCountKey          = "notice"
        static let messageCountKey         = "message"
        static let dailyCareReportCountKey = "dailyCareReport"
    }
    struct diaryStudentPopup {
        static let response              = "response"
        static let studentList           = "studentList"
        static let id                    = "id"
        static let name                  = "name"
        static let compressedProfilePic  = "compressedProfilePic"
        static let profilePic            = "profilePic"
        static let mobileNo              = "mobileNo"
        static let isAbsent              = "isAbsent"
        static let offset                = "offset"
        static let classId               = "classId"
        static let className             = "className"
        static let schoolName            = "schoolName"
        static let classSubjectList      = "classSubjectList"
        static let classNameToDisplay    = "classNameToDisplay"
        static let rollNo                = "rollNo"
        static let result                = "result"
        static let addedByAdmin          = "addedByAdmin"
        static let academicYear          = "academicYear"
        
    }
    struct employeeDetailedStudentProfile {
        static let response              = "response"
        static let studentList           = "studentList"
        static let leaves                = "leaves"
        static let student               = "student"
        static let id                    = "id"
        static let name                  = "name"
        static let date                  = "date"
        static let reason                = "reason"
        static let compressedProfilePic  = "compressedProfilePic"
        static let profilePic            = "profilePic"
        static let mobileNo              = "mobileNo"
        static let className             = "className"
        static let classId               = "classId"
        static let schoolId              = "schoolId"
        static let classTeacher          = "classTeacher"
        static let AdmnNo                = "AdmnNo"
        static let dob                   = "dob"
        static let transportType         = "transportType"
        static let delayReport           = "delayReport"
        static let address               = "address"
        static let parentName            = "parentName"
        static let regNo                 = "regNo"
        static let contactNo             = "contactNo"
        static let busId                 = "busId"
        static let busNo                 = "busNo"
        static let configItems           = "configItems"
        static let leaveReasonList       = "leaveReasonList"
        static let leaveDescription      = "leaveDescription"
        static let feePayment            = "feePayment"
        static let showOnlyProgressCard  = "showOnlyProgressCard"
        static let leaveStatus           = "leaveStatus"
        
    }
    
    struct homeWorkConstants {
        
        
        static let homework           = "homework"
        static let response           = "response"
        static let id                 = "id"
        static let title              = "title"
        static let description        = "description"
        static let reference          = "reference"
        static let from               = "from"
        static let subject            = "subject"
        static let createdOn          = "createdOn"
        static let submissionDate     = "submissionDate"
        static let attachments        = "attachments"
        static let recipientDetails   = "recipientDetails"
        static let status             = "status"
        static let url                = "url"
        static let mimeType           = "mimeType"
        static let type               = "type"
        static let uniqueName         = "uniqueName"
        static let attachment         = "attachment"
        static let attachmentTypeImage = "Image"
        static let homeworkStudentDetails = "homeworkStudentDetails"
        static let subjects            = "subjects"
        static let subjectId           = "id"
        static let subjectName         = "subject"
        static let classList           = "classList"
        static let classId             = "id"
        static let className           = "className"
        
    }
    
    struct homeworkUpload {
        static let response         = "response"
        static let attachment       = "attachment"
        static let homeworkAttachment = "homeworkAttachment"
        static let imageUrl         = "url"
        static let mimeType         = "mimeType"
        static let attachmentId     = "id"
        static let messageAttachment = "messageAttachment"
        
    }
    struct substituteEmployeesOfSameClass {
        static let id              = "id"
        static let name            = "name"
        static let mobileNo        = "mobileNo"
        static let profilePicUrl   = "profilePicUrl"
        static let classInCharge   = "classInCharge"
        static let designation     = "designation"
        static let employeeList    = "employeeList"
        static let substituted     = "substituted"
        
    }
    struct recipientRoles {
        static let id              = "id"
        static let name            = "name"
        static let value           = "value"
        static let recipientRoles  = "recipientRoles"
        static let canEdit         = "canEdit"
        
    }
    struct newAdmissionPay {
        static let StudentAdmission = "StudentAdmission"
        static let feepaymentDetails = "feepaymentDetails"
        static let messageToDisplay = "messageToDisplay"
        static let amount = "amount"
        static let feeType = "feeType"
        static let admissionDetails = "admissionDetails"
        static let id = "id"
        static let dateOfBirth = "dateOfBirth"
        static let motherName = "motherName"
        static let fatherName = "fatherName"
        static let address = "address"
        static let emailId = "emailId"
        static let studentName = "studentName"
        static let className = "className"
        static let feePaymentStatus = "feePaymentStatus"
        static let mobileNo = "mobileNo"
        static let appliedOn = "appliedOn"
        static let nativeResident = "nativeResident"
        
    }
    
    struct feeDetails {
        
        static let feeDetails = "feeDetails"
        static let needToUpdateMessage = "needToUpdateMessage"
        static let paymentId = "paymentId"
        static let displayName = "displayName"
        static let discriminator = "discriminator"
        static let txnidForNetBanking = "txnidForNetBanking"
        static let surcharge = "surcharge"
        static let password = "password"
        static let clientcodeForCard = "clientcodeForCard"
        static let custacc = "custacc"
        static let channelid = "channelid"
        static let txncurr = "txncurr"
        static let amount = "amount"
        static let merchantId = "merchantId"
        static let txnidForCard = "txnidForCard"
        static let prodid = "prodid"
        static let ru = "ru"
        static let clientcodeForNetBanking = "clientcodeForNetBanking"
        static let loginid = "loginid"
        static let paidOn = "paidOn"
        static let currentFeeVersion = "currentFeeVersionForIOS"
        static let publicKey = "publicKey"
        static let urlios    = "urlios"
        static let signatureRequest = "signature_request"
        static let signatureResponse = "signature_response"

    }
    
    struct invoiceList {
        static let id            = "id"
        static let name          = "name"
        static let amount        = "amount"
        static let date          = "date"
        static let dueDate       = "dueDate"
        static let studentName   = "studentName"
        static let studentId     = "studentId"
        static let status        = "status"
        static let paymentId     = "paymentId"
        static let paidOn        = "paidOn"
        static let feeList       = "feeList"
        static let schoolId      = "schoolId"
        static let schoolName    = "schoolName"
        static let schoolAddress = "schoolAddress"
        static let payButton     = "payButton"
        static let feePaymentDetails = "feePaymentDetails"
        static let canDownloadReceipt = "canDownloadReceipt"
        static let invoiceList   = "invoiceList"
        static let paymentGateway = "paymentGateway"
    }
    
    struct classTest {
        static let id            = "id"
        static let name          = "name"
        static let maxMark       = "maxMark"
        static let subject       = "subject"
        static let subjectId     = "subjectId"
        static let date          = "date"
        static let time          = "time"
        static let topic         = "topic"
        static let mark          = "mark"
        static let addedBy       = "addedBy"
        static let addedByName   = "addedByName"
        static let upcoming      = "upcoming"
        static let evaluvationId = "evaluvationId"
        static let evaluvationname = "evaluvationname"
        static let classTestListing = "classTestListing"
        static let evaluationActivity = "evaluationActivity"
        static let evaluationActivityName = "evaluationActivityName"
        
        
        
    }
    
    struct evaluationActivity {
        static let id            = "id"
        static let name          = "name"
        static let evaluationActivity = "evaluationActivity"
    }
    
    struct classSubject {
        static let id           = "id"
        static let name         = "name"
        static let description  = "description"
        static let order        = "order"
        static let nickName     = "nickName"
        static let classSubject = "classSubject"
        static let subjects     = "subjects"
    }
    
    struct evaluationActivitySubject {
        static let id           = "id"
        static let name         = "name"
        static let studentEvaluationSubjectList = "studentEvaluationSubjectList"
        static let examDetails  = "examDetails"
        static let maxMark      = "maxMark"
        static let subject      = "subject"
        static let subjectId    = "subjectId"
        static let date         = "date"
        static let time         = "time"
        static let topic        = "topic"
        static let addedBy      = "addedBy"
        static let addedByName  = "addedByName"
        static let upcoming     = "upcoming"
        static let status       = "status"
        static let schoolClassName = "schoolClassName"
        static let schoolClassId = "schoolClassId"
        static let evaluationActivityName = "evaluationActivityName"
        static let studentList = "studentList"
        static let evaluationActivitySubject = "evaluationActivitySubject"
    }
    
    struct notificationDetails{
        static let notificationDetails = "notificationDetails"
        static let title = "title"
        static let from = "from"
        static let object = "object"
        static let type = "type"
        static let userId = "userId"
        static let userRoleId = "userRoleId"
        static let studentId = "studentId"
        static let notificationTypeDiary = "Diary"
        static let notificationTypNotice = "Notice"
        static let notificationTypHomework = "Homework"
        static let notificationTypMessage = "Message"
        static let notificationTypStudentLeaveRecord = "StudentLeaveRecord"
        static let notificationTypStudentLeaveSubmit = "StudentLeaveSubmit"
        static let notificationTypExternalNotification = "ExternalNotification"
    }
    
    struct externalNotification{
        static let id = "id"
        static let externalSystemId = "externalSystemId"
        static let studentId = "studentId"
        static let content = "content"
    }
    
}
