//
//  segueConstants.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

struct segueConstants {
    
    static let segueForShowingComposeMessage = "segueForShowingComposeMessage"
    static let segueForShowingDetailedMessage = "showMessageDetailedScreen"
    static let segueForLoadingNotificationDetailed = "showDetailedNotificationWithDesc"
    static let segueForLoadingExtraDetails = "segueForNotificationExtraDtailed"
    static let segueForLoadingStudentProfile = "studentProfileSegue"
    static let segueForLoadingComposeNotice = "AddNewNoticeSegue"
    static let segueForLoadingComposeDiary  = "AddNewDiarySegue"
    static let segueForLoadingDiaryDetails  = "DiaryDetailsSegue"
    static let segueForLoadingEmployeeStudentProfileDetails  = "EmployeeDetailedStudentProfileSegue"
    static let segueForLoadingHomeworkDetailedScene  = "segueToHomeworkDetailedScene"
    static let segueForLoadingComposeHomework  = "AddNewHomeworkSegue"
    static let segueToAddNewGroup  = "segueToAddNewGroup"
    static let segueForForwardingMessage = "segueForForwardingMessage"
    static let segueToSearchStudent  = "studentSearchProfileSegue"
    static let segueForDiaryForward  =  "SegueForwardDiary"
    static let segueToLogin          = "segueToLogin"
    static let EmployeeApplyLeaveSegue = "EmployeeApplyLeaveSegue"
    static let showEmployeeStudentProfileSegue = "showEmployeeStudentProfileSegue"
    static let attendanceReportSegue = "attendanceReportSegue"
    static let segueToTimeTable = "segueToTimeTable"
    static let segueToSudentPromotion = "segueToSudentPromotion"
    static let segueToStudentShuffle = "segueToStudentShuffle"
    static let segueToFeePaymentDetailsPage = "segueToFeePaymentDetailsPage"
    static let segueToInvoiceDetailsPage = "segueToInvoiceDetailsPage"
    static let segueToClassTestListingVC = "segueToClassTestListingVC"
    static let segueToProgresscard       = "segueToProgresscard"
    static let segueToNutshell           = "segueToNutshell"
    static let segueToGraph              = "segueToGraph"
    static let segueToMarkentryDetailvc  = "segueToMarkentryDetailvc"
    static let segueToProgresscardFromStudentProfile = "segueToProgresscardFromStudentProfile"
    static let segueToEmployeeStudentProfileVC = "segueToEmployeeStudentProfileVC"
    
}

struct parentEyeConstants {
    
    struct userDefaultsConstants {
        static let userIsloggedIn = "userLoggedIn"
    }
    
}
