//
//  DictionaryExtension.swift
//  parentEye
//
//  Created by Martin Jacob on 22/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
extension Dictionary {

    mutating func addEntriesFromDictionary(otherDictionary:Dictionary){
        for (key,value) in otherDictionary {
            self.updateValue(value, forKey:key)
        }
    }

}