//
//  customString.swift
//  parentEye
//
//  Created by Martin Jacob on 18/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit


extension String{

    func createAttributedStringFromArray(stringArray:[StringClassAttri]) -> NSAttributedString {
       let attriStrings = NSMutableAttributedString()
        
        for attriString in stringArray{
           
            let attrStr = NSMutableAttributedString(string: attriString.stringTobeAtributed, attributes: attriString.AtributeDictioanry)
            attriStrings.append(attrStr)
        }
        
        return attriStrings
    }
}

extension String {
    func getFirstString() -> String {
     return  String(self[self.startIndex])
    }
    func contains(_ string: String, options: CompareOptions) -> Bool {
        return range(of: string, options: options) != nil
    }
}

    

