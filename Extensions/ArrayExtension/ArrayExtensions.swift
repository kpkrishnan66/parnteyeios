//
//  ArrayExtensions.swift
//  parentEye
//
//  Created by Martin Jacob on 21/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation

extension Array {
    mutating func sortDateStringArrayBasedOnDateWithDateFormat(_ format:dateFormatNeeded) {
        self = self.sorted(by: {
            let firstString = $0 as! String
            let secondString = $1 as! String
            
            let firstDate = DateApi.convertThisStringToDate(dateString: firstString, formatNeeded: format)
            let secondDate = DateApi.convertThisStringToDate(dateString: secondString, formatNeeded: format)
            
            if   firstDate.compare(secondDate as Foundation.Date) == .orderedAscending{
                return false
            }
            return true
        })
    }
    
}
