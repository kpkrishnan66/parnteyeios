//
//  CustomButton.swift
//  justrent
//
//  Created by emvigo on 29/06/16.
//  Copyright © 2016 Emvigo. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    //ibinspectable to set the custom bordercolor for button
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    //ibinspectable to set the custom borderwidth for button
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    //ibinspectable to set the corner radius for button
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
 }
