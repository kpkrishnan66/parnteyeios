//
//  ButtonForDownLoadImg.swift
//  parentEye
//
//  Created by Martin Jacob on 19/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class ButtonForDownLoadImg: UIButton {
    var imageForDownloading = UIImage(named: "ApplicationLogo")
    var downloadUrlString = ""
    var name = ""
}
