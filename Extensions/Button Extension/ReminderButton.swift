//
//  ReminderButton.swift
//  parentEye
//
//  Created by Martin Jacob on 27/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class ReminderButton: UIButton {
    /*Variable used to  identify generic id attached to it */
    var id = ""
    var genericThing:AnyObject?
}
