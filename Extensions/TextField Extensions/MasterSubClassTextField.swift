//
//  MasterSubClassTextField.swift
//  facebookHelper
//
//  Created by Martin Jacob on 10/03/16.
//  Copyright © 2016 Martin Jacob. All rights reserved.
//

import UIKit
import AudioToolbox

@IBDesignable open class MasterSubClassTextField: UITextField {
    var isValid            = false
    @IBInspectable var incorrectTextColor = UIColor.red
    @IBInspectable var correctTextColor   = UIColor.black
    
    // variable to store regular expression
    var regEX:String?
    
    //MARK:- initialization functions
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK:- set regular expression
    
    open func setRegularExpression(_ regExp:String){
        self.regEX = regExp
    }
    
    
    //MARK:- internal functions
    //Function to change the color
    internal func changeTextFieldUIToShowError(_ showError:Bool){
        
        if showError == false {
            self.textColor = correctTextColor
        }else {
            self.textColor = incorrectTextColor
        }
        
    }
    
    // function to shake the textfield and vibrate the phone
    internal func vibrate(){
        AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
        
        self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: UIViewAnimationOptions.allowUserInteraction,
                       animations: {
                        self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    
}
