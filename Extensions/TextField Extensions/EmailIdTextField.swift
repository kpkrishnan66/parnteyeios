//
//  emailIdTextField.swift
//  facebookHelper
//
//  Created by Martin Jacob on 09/03/16.
//  Copyright © 2016 Martin Jacob. All rights reserved.
//

import UIKit
import AudioToolbox
@IBDesignable open class EmailIdTextField: MasterSubClassTextField,UITextFieldDelegate {
    
    
    
    //Regular expression for email validation
    var emailRegEx          = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    //Allowed characters in email. Used for
    let allowedCharacters   = "@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!#$%&'*+-/=?^_`{|}~."
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setRegularExpression(emailRegEx)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setRegularExpression(emailRegEx)
    }
    
    
    override open func didMoveToSuperview(){
        initialSetUP()
    }
    
    
    //MARK:- textfield delegate functions
    
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    open func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        changeTextFieldUIToShowError(false)
    }
    open func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldChange = true;
        if( string.characters.count > 0){
            shouldChange = preventInvalidCharacterSetEntry(string)
        }
        return shouldChange;
        
    }
    
    
    open func textFieldDidEndEditing(_ textField: UITextField) {
        //calls the validateEmail function to check whether the given string is valid email
        if  validateEmail(textField.text) == false {
            isValid = false
            changeTextFieldUIToShowError(true)
            vibrate()
        }else {
            isValid = true
            changeTextFieldUIToShowError(false)
        }
    }
    
    /*----------------------------------------------------------------------------------------------------------*/
    //MARK:- Private function
    
    /**
     initial setup for the textfield
     
     - returns: nothing
     */
    
    fileprivate func initialSetUP(){
        self.delegate = self
        self.keyboardType = UIKeyboardType.emailAddress
    }
    
    open func isValidEmail () ->Bool{
        return validate(self.text!)
    }
    
    /**
     Checks if the given string is valid email.
     
     - parameter email: email string that is to be tested
     
     - returns: returns whether the string is valid email in bool.
     */
    
    fileprivate func validateEmail(_ email:String?) ->Bool{
        
        var result:Bool = false
        
        if let emailString = email {
            let emailTest = NSPredicate(format:"SELF MATCHES %@", regEX!)
            return emailTest.evaluate(with: emailString)
        }else {
            result = false
        }
        
        return result
    }
    
    /**
     Checks the email for validity
     
     - parameter string: the string to be tested
     
     - returns: returns true or false
     */
    
    
    fileprivate func validate(_ value: String) -> Bool {
        
        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
    fileprivate  func preventInvalidCharacterSetEntry(_ string:String) -> Bool {
        let validCharSet       = CharacterSet(charactersIn: allowedCharacters).inverted;
        let trimmedReplacement = string.components(separatedBy: validCharSet).joined(separator: "")
        let shouldChange       = trimmedReplacement.trimmingCharacters(in: validCharSet).characters.count > 0
        return shouldChange
    }
    
}
