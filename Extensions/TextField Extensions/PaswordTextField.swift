//
//  PaswordTextField.swift
//  parentEye
//
//  Created by Martin Jacob on 19/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
public class PasswordTextField: MasterSubClassTextField,UITextFieldDelegate {
  
    
    override public func didMoveToSuperview(){
        initialSetUP()
    }
    
    private func initialSetUP(){
        self.delegate = self
        self.keyboardType = UIKeyboardType.default
        self.isSecureTextEntry = true
    }
    
    func validatePasswordFiels() -> Bool {
        var isValid = false
        
        
        if (self.text?.characters.count)! > 0 {
            isValid = true
        }
       return isValid
    }
    
    //MARK:- textfield delegate functions
    
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    open func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    open func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        changeTextFieldUIToShowError(false)
    }
    open func textFieldDidEndEditing(_ textField: UITextField) {
        if  validatePasswordFiels() == false {
            vibrate()
        }else {
            isValid = true
        }
    }
}
