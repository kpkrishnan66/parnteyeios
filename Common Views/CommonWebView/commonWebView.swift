//
//  commonWebView.swift
//  dealPlatzSwift
//
//  Created by martin on 7/28/15.
//  Copyright (c) 2015 martin. All rights reserved.
//

import UIKit

class commonWebView: UIView,UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    @IBAction func dismissWebView(_ sender: UIButton) {
        self.removeFromSuperview()
    }
    
    func loadPage(url:NSURL){
    
    webView.loadRequest(NSURLRequest(url: url as URL) as URLRequest)
    
    }
    
        
    
    @IBOutlet var view:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = UIScreen.main.bounds
        Bundle.main.loadNibNamed("commonWebView", owner: self, options: nil)
        self.view.frame = UIScreen.main.bounds
        self.addSubview(self.view)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
