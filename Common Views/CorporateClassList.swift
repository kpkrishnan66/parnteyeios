//
//  CorporateClassList.swift
//  parentEye
//
//  Created by Vivek on 08/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

public class CorporateClassList {
    
    
   public class func getClassListForCorporateManager() {
    var schoolId:Int = 0
    var classList : [classSubjectList]!
    var Classes:[EmployeeClassModel]?
    var user:userModel? // Used tovarore the userdata.
    

    user = DBController().getUserFromDB()
   // weak var weakSelf = CorporateClassList()
    //hudControllerClass.showNormalHudToViewController()
    schoolId = user!.schools![(user?.currentSchoolSelected)!].schoolId
    WebServiceApi.getClassListOfCorporateManager(schoolId: String(schoolId),onSuccess: { (result) in
        
        //hudControllerClass.hideHudInViewController(weakSelf!)
        if let classSubjectListArray = result["classSubjectList"] as? [[String:AnyObject]]{
            classList = [classSubjectList]()
            for dic in classSubjectListArray{
                let value = classSubjectList(fromDictionary: dic as NSDictionary)
                classList.append(value)
            }
        }
        if let employeeclassList = classList as [classSubjectList]?{
            Classes?.removeAll()
            for classListOfEmployee in employeeclassList{
                if let cls =  EmployeeClassModel.init(fromClassList: classListOfEmployee) {
                    if Classes == nil{
                        Classes = [EmployeeClassModel]()
                    }
                    Classes?.append(cls)
                }
            }
        }
        user?.Classes?.removeAll()
        user?.Classes = Classes
        DBController().addUserToDB(user!)
        }, onFailure: { (error) in
            //hudControllerClass.hideHudInViewController(weakSelf!)
            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
    }) { (progress) in
        
    }
    }
}
    
