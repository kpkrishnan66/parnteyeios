//
//  PopOverView.swift
//  parentEye
//
//  Created by Martin Jacob on 25/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit
import Firebase

class PopOverView: UIView,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var userListingTable: UITableView!
    
    var user:userModel?
    let cellIdentifier = "popOverCell"
    
    weak var delegate:popOverDelegate?
    weak var selectedSchoolDelegate:SelectedSchoolpopOverDelegate?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        user = DBController().getUserFromDB()

    }

    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        user = DBController().getUserFromDB()
    }

    override func didMoveToSuperview() {
        setUpTable()
    }

    private func setUpTable(){

        userListingTable.register(UINib(nibName: "popOverCell", bundle: nil), forCellReuseIdentifier: "popOverCell")
        userListingTable.estimatedRowHeight = 75.0
        userListingTable.rowHeight = UITableViewAutomaticDimension
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(self.tableGotTappedWithGesture))

        userListingTable.addGestureRecognizer(tapGes)

    }

    @objc private func tableGotTappedWithGesture(gesture:UITapGestureRecognizer){
        let tapPoint = gesture.location(in: userListingTable)

         if userListingTable.indexPathForRow(at: tapPoint) != nil {
            gesture.cancelsTouchesInView = false
        }else {
            self.removeFromSuperview()
        }
    }


    // MARK: Table view delegate methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let usr =  user{
            if  usr.currentTypeOfUser == .guardian{
                if let student = usr.Students{
                    if user?.switchAs == "Employee"{
                        var studentCount = student.count
                        studentCount += 1
                        return  studentCount
                    }
                    else{
                        return student.count
                    }
                }
            }

            if  usr.currentTypeOfUser == .CorporateManager{
                if let schools = usr.schools{
                    if user?.switchAs == "Employee"{
                        return schools.count + 1
                    }
                    else{
                        return schools.count
                    }
                }
            }
        }

        return 0
    }

    //TODO :- only parents  is supported in this function .. need to make it support heads also
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! popOverCell

        if user?.currentTypeOfUser == .guardian {
            cell.profileImageViewWidth.constant = 40
        if user?.switchAs == "Employee"{
            if indexPath.row == (user?.Students?.count){
                cell.profileImageView.isHidden = true
                cell.nameLabel.isHidden = true
                cell.switchasemployeeLabel.isHidden = false
                return cell
            }
        }

        cell.switchasemployeeLabel.isHidden = true
        weak var imgWeak = cell.profileImageView
        ImageAPi.fetchImageForUrl(urlString: (user?.Students![indexPath.row].profilePic)!, oneSuccess: { (image) in
            if imgWeak != nil{
                imgWeak!.image = image
                ImageAPi.makeImageViewRound(imageView: imgWeak!)
            }
            }, onFailure: { (error) in
                debugPrint("Download failed")
        })
        cell.nameLabel.text = user?.Students![indexPath.row].name
        }


        if user?.currentTypeOfUser == .CorporateManager {
            if user?.switchAs == "Employee"{
                if indexPath.row == (user?.schools?.count){
                    cell.profileImageView.isHidden = true
                    cell.nameLabel.isHidden = true
                    cell.switchasemployeeLabel.isHidden = false
                    return cell
                }
            }
            cell.profileImageViewWidth.constant = 0
            cell.profileImageView.isHidden = true
            cell.switchasemployeeLabel.isHidden = true
            cell.nameLabel.text = user?.schools![indexPath.row].schoolName

        }


        return cell
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if user?.currentTypeOfUser == .guardian {
        if indexPath.row == (user?.Students?.count){
            Analytics.logEvent("P_switchToEmployee", parameters: nil)
            switchToBackEmployee()
        }

        else{
            user?.currentOptionSelected = indexPath.row
            DBController().addUserToDB(user!)

            self.delegate?.popOverDidSelectRow(rowSelected: indexPath.row)
        }
        self.removeFromSuperview()
        }

        if user?.currentTypeOfUser == .CorporateManager {
            if indexPath.row == (user?.schools?.count){
                switchToBackEmployee()
            }

            else{
                user?.currentSchoolSelected = indexPath.row
                DBController().addUserToDB(user!)

                self.selectedSchoolDelegate?.selectedSchoolRow(rowSelected: indexPath.row)
            }
            self.removeFromSuperview()
        }
    }


    //MARK:- Touch

    func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.removeFromSuperview()
    }
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */

    class func addPopOverToViewController<vc:UIViewController> (_ inparam:vc, fromButton:UIButton) where vc:popOverDelegate{
        let popArray = Bundle.main.loadNibNamed("PopOverView", owner: self, options: nil)
        let pop = popArray![0] as! PopOverView
        pop.delegate = inparam
        pop.addMeAsSubViewToView(view: inparam.view, fromView: fromButton, popView: pop)
    }

    class func addPopOverofSchoolsToViewController<vc:UIViewController> (_ inparam:vc, fromButton:UIButton) where vc:SelectedSchoolpopOverDelegate{
        let popArray = Bundle.main.loadNibNamed("PopOverView", owner: self, options: nil)
        let pop = popArray![0] as! PopOverView
        pop.selectedSchoolDelegate = inparam
        pop.addMeAsSubViewToView(view: inparam.view, fromView: fromButton, popView: pop)
    }
    private func isPopAlreadyInView (mainView:UIView)-> Bool{

        var result = false
        for subView in mainView.subviews {
            if subView is PopOverView {
                result = true
            }
        }
        return result
    }

    public func addMeAsSubViewToView(view:UIView, fromView sourceView:UIView, popView:UIView){

        if !isPopAlreadyInView(mainView: view){
            view.addSubview(popView)
            self.translatesAutoresizingMaskIntoConstraints = false //Don't forget this line
            let leftSideConstraint = NSLayoutConstraint(item: self, attribute: .left, relatedBy: .equal, toItem:view , attribute: .left, multiplier: 1.0, constant: 0.0)

            let bottomConstraint = NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem:view , attribute: .bottom, multiplier: 1.0, constant: 0.0)

            let topConstraint = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem:view , attribute: .top, multiplier: 1.0, constant: sourceView.frame.origin.y+sourceView.frame.size.height+20)

            let rightConstraint = NSLayoutConstraint(item: self, attribute: .right, relatedBy: .equal, toItem:view , attribute: .right, multiplier: 1.0, constant: 0.0)

            view.addConstraints([leftSideConstraint, bottomConstraint, topConstraint, rightConstraint])
        }

        //        view.addSubview(self)
    }


    func switchToBackEmployee(){

        hudControllerClass.showNormalHudToViewControllerForUIView(viewController: self)

        WebServiceApi.getMultiRoleSwitchForEmployee(userId: (user?.userId)!,switchAs: (user?.switchAs)!,

                                                    onSuccess: { (result) in

                                                        hudControllerClass.hideHudInViewControllerForUIView(viewController: self)
                                                        let rt = RootClass.init(fromDictionary: result as NSDictionary)
                                                        if rt?.failureReason == nil {
                                                            // setting the user defaults to show that we are logged in
                                                            //DBController().clearDataBse()
                                                            if let userModel = userModel(makeUserModelFromRootObject: rt!){

                                                                if userModel.currentTypeOfUser == .employee {
                                                                    DBController().addUserToDB(userModel)
                                                                    UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)

                                                                    let app = UIApplication.shared.delegate as! AppDelegate
                                                                    app.maketabBarRoot()
                                                                }
                                                            }




                                                        }
                                                        else
                                                        {       AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .success)
                                                        }


            },
                                                    onFailure: { (error) in
                                                        AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)

            },
                                                    duringProgress: { (progress) in

        })
    }


}
