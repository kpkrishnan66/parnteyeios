//
//  FireBaseKeys.swift
//  parentEye
//
//  Created by Vivek on 24/02/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import Firebase

public class FireBaseKeys {
    
    
    
    public class func FirebaseKeysRuleOne(value:String) {
        var user:userModel? // Used the userdata.

        user = DBController().getUserFromDB()
        if user?.currentTypeOfUser == .guardian {
            Analytics.logEvent("P_"+value, parameters: nil)
        }
        else{
        if user?.currentTypeOfUser == .employee {
            if user?.employeeType != "SupportingStaff" {
                if user?.employeeType == "Principal" || user?.employeeType == "School Administrator" || user?.employeeType == "Coordinator" || user?.employeeType == "Manager"{
                    Analytics.logEvent("TP_"+value, parameters: nil)
                }
                else{
                    Analytics.logEvent("T_"+value, parameters: nil)
                }
            }
        }
        if user?.currentTypeOfUser == .CorporateManager {
            Analytics.logEvent("CM_"+value, parameters: nil)
        }
        }

    }
    
    public class func FirebaseKeysRuleTwo(value:String) {
        var user:userModel? // Used the userdata.
        
        user = DBController().getUserFromDB()
        
        if user?.currentTypeOfUser == .guardian{
            Analytics.logEvent("P_"+value, parameters: nil)
            
        }
        else{
            if user?.currentTypeOfUser == .employee {
                if user?.employeeType != "SupportingStaff" {
                    if user?.employeeType == "Principal" || user?.employeeType == "School Administrator" || user?.employeeType == "Coordinator" {
                        Analytics.logEvent("TP_"+value, parameters: nil)
                    }
                    else if user?.employeeType == "Manager" {
                        Analytics.logEvent("M_"+value, parameters: nil)
                    }
                    else{
                        Analytics.logEvent("T_"+value, parameters: nil)
                    }
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                Analytics.logEvent("CM_"+value, parameters: nil)
            }
            
        }

    }
    
    public class func FirebaseKeysRuleThree(value:String) {
        var user:userModel? // Used the userdata.
        
        user = DBController().getUserFromDB()
        
        if user?.currentTypeOfUser == .employee {
                if user?.employeeType != "SupportingStaff" {
                    if user?.employeeType == "Principal" || user?.employeeType == "School Administrator" || user?.employeeType == "Coordinator" {
                        Analytics.logEvent("TP_"+value, parameters: nil)
                    }
                    else if user?.employeeType == "Manager" {
                        Analytics.logEvent("M_"+value, parameters: nil)
                    }
                    else{
                        Analytics.logEvent("T_"+value, parameters: nil)
                    }
                }
            }
            if user?.currentTypeOfUser == .CorporateManager {
                Analytics.logEvent("CM_"+value, parameters: nil)
            }
        
    }
    
    public class func FirebaseKeysRuleFour(value:String) {
        var user:userModel? // Used the userdata.
        
        user = DBController().getUserFromDB()
        
        if user?.currentTypeOfUser == .employee {
            if user?.employeeType != "SupportingStaff" {
                if user?.employeeType == "Principal" || user?.employeeType == "School Administrator" || user?.employeeType == "Coordinator" || user?.employeeType == "Manager"{
                    Analytics.logEvent("TP_"+value, parameters: nil)
                }
                else{
                    Analytics.logEvent("T_"+value, parameters: nil)
                }
            }
        }
        if user?.currentTypeOfUser == .CorporateManager {
            Analytics.logEvent("CM_"+value, parameters: nil)
        }
        
    }

}
  
