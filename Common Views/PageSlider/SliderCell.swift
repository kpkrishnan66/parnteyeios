//
//  SliderCell.swift
//  parentEye
//
//  Created by Martin Jacob on 16/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class SliderCell: UICollectionViewCell {

    @IBOutlet weak var shareButton:UIButton!
    @IBOutlet weak var downLoadButton:ButtonForDownLoadImg!
    @IBOutlet weak var slideImage:UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}
