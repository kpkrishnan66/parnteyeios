//
//  SliderBaseView.swift
//  parentEye
//
//  Created by Martin Jacob on 16/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

@IBDesignable class SliderBaseView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collection:UICollectionView!
    @IBOutlet weak var goForwardButton:UIButton!
    @IBOutlet weak var goBackButton:UIButton!
    
    @IBOutlet var goForwardButtonHeight: NSLayoutConstraint!
    @IBOutlet var goForwardButtonWidth: NSLayoutConstraint!
    @IBOutlet var goBackButtonHeight: NSLayoutConstraint!
    @IBOutlet var goBackButtonWidth: NSLayoutConstraint!
    
    var imageArray = [AttachmentModel]()
    var IndexSelected = -1
    var shouldShowDownloadAndShare = false
    var indexClickedClosure : ((_ indexClicked: Int)->Void)!
    var shareButtonClicked : ((_ indexClicked: Int) ->Void)!
    var downloadButtonClicked : ((_ button: UIButton) ->Void)!
    var logoImages: [UIImage] = []
    var fromSreeMaharshiLogin:Bool = false
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    var view:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        
    }
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        
        
    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SliderBaseView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        //view.frame = bounds
        //view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        let leftSideConstraint = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0.0)
        let heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0.0)
        self.addConstraints([leftSideConstraint, bottomConstraint, heightConstraint, widthConstraint])
        collection.register(UINib(nibName: "SliderCell", bundle: nil), forCellWithReuseIdentifier: "SliderCell")
    }
    
    override func didMoveToSuperview() {
        self.setNeedsLayout()
        self.layoutIfNeeded()
       
    }

    public func invalidateLayoutAndReload(){
        self.fromSreeMaharshiLogin = false
        collection.reloadData()
    }
    
    public func reloadSlidingView (){
        
        collection.reloadData()
    }
    
    internal func setForNoIMages(){
        self.collection.isHidden = true
        self.goBackButton.isHidden = true
        self.goForwardButton.isHidden = true
        self.backgroundColor = UIColor.green
        
    }
    
    internal func setForsreeMaharshiLoginImages() {
        self.goBackButton.isHidden = true
        self.goForwardButton.isHidden = true
        self.fromSreeMaharshiLogin = true
    }
    private func setUpCollectionView (){
        
    }
    //MARK: - collectiview delagate and data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         if fromSreeMaharshiLogin == true {
            return logoImages.count
        }
        return imageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderCell", for: indexPath as IndexPath) as! SliderCell
      IndexSelected = indexPath.row
      if fromSreeMaharshiLogin == true {
        cell.slideImage.image = logoImages[indexPath.row]
        cell.shareButton.isHidden = true
        cell.downLoadButton.isHidden = true
    }
      else {
        let attachment = imageArray[indexPath.row]
    
        if attachment is ModelImage {
            weak var weakCell = cell
            ImageAPi.fetchImageForUrl(urlString: attachment.url, oneSuccess: { (image) in
                if weakCell != nil { weakCell!.slideImage.image = image }
            }) { (error) in
                if weakCell != nil{
                    weakCell!.slideImage.image = nil
                }
                
            }
        } else {
            if attachment.type == "PDF" {
             cell.slideImage.image = UIImage(named: "pdfIcon")
            }
            if attachment.type == "DOC" || attachment.type == "DOCX" {
             cell.slideImage.image = UIImage(named: "wordIcon")
            }
        }
        
        //WebServiceApi.setImgWithUrlString(imageArray[indexPath.row], forImageView: cell.slideImage)
        
        
        cell.shareButton.isHidden = !shouldShowDownloadAndShare
        cell.downLoadButton.isHidden = !shouldShowDownloadAndShare
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(self.showShare), for: .touchUpInside)
        cell.downLoadButton.addTarget(self, action: #selector(self.downLoadImage), for: .touchUpInside)
        cell.downLoadButton.tag = indexPath.row
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if fromSreeMaharshiLogin == true {
           let cellWidth = UIScreen.main.bounds.size.width
           return CGSize(width: cellWidth, height: cellWidth)
        }
        return self.frame.size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.indexClickedClosure != nil {
            self.indexClickedClosure(indexPath.row)
        }
    }
    
    
    func  scrollToPosition(position:Int)  {
        
        IndexSelected = position
        collection.scrollToItem(at: NSIndexPath(row: IndexSelected, section: 0) as IndexPath, at: .centeredHorizontally, animated: true)
    }
    
    @objc func scrollToNextCell(){
        
        //get Collection View Instance
        //let collectionView:UICollectionView;
        
        //get cell size
        let cellSize = CGSize(width: self.view.frame.width, height: self.view.frame.height);
        
        //get current content Offset of the Collection view
        let contentOffset = collection.contentOffset;
        
        //scroll to next cell
        //collection.scrollRectToVisible(CGRectMake(contentOffset.x + cellSize.width, contentOffset.y, cellSize.width, cellSize.height), animated: true);
        if collection.contentSize.width <= collection.contentOffset.x + cellSize.width
        {
            collection.scrollRectToVisible(CGRect(x: 0, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
            
        } else {
            collection.scrollRectToVisible(CGRect(x: contentOffset.x + cellSize.width, y: contentOffset.y, width: cellSize.width, height: cellSize.height), animated: true);
            
        }

        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        
        _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(SliderBaseView.scrollToNextCell), userInfo: nil, repeats: true);
        
        
    }
    
    
    @IBAction func scrollToNext(_ sender: UIButton) {
    
    if IndexSelected != -1 {
           
            if (sender.tag == 1)&&(imageArray.count-1 >= IndexSelected+1) {
               collection.scrollToItem(at: NSIndexPath(row: IndexSelected+1, section: 0) as IndexPath, at: .centeredHorizontally, animated: true)
            }else if (sender.tag == 0)&&(IndexSelected != 0) {
                collection.scrollToItem(at: NSIndexPath(row: IndexSelected-1, section: 0) as IndexPath, at: .centeredHorizontally, animated: true)
            }
        }
    }
    
    @IBAction func downLoadImage(_ sender:UIButton){
        if downloadButtonClicked != nil{
            downloadButtonClicked(sender)
        }
    }
    
    @objc private func showShare(_ sender:UIButton){
        if shareButtonClicked != nil {
            self.shareButtonClicked(sender.tag)
        }
    }
}
