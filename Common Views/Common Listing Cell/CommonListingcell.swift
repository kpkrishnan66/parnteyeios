//
//  CommonListingcell.swift
//  parentEye
//
//  Created by Martin Jacob on 25/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

class CommonListingcell: UITableViewCell {
    
    
    @IBOutlet weak var dateView:UIView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var designatedUpdatedUponLabel: UILabel!
    @IBOutlet weak var attachmentShowingImage: UIImageView!

    @IBOutlet weak var downLoadAttachmentButton: ButtonForDownLoadImg!


    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var reminderButton: ReminderButton!
    @IBOutlet weak var imageContainerView: ExpansiveView!
 
    @IBOutlet weak var separatorView:UIView!
    @IBOutlet weak var topLineView:UIView!
    @IBOutlet weak var bottomLineView:UIView!
    
    @IBOutlet weak var imageContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var reminderButtonWidth:NSLayoutConstraint!
    @IBOutlet weak var downLoadButtonHeight:NSLayoutConstraint!
    @IBOutlet weak var attachmentShowingImageWidthConstraint:NSLayoutConstraint!
    
    @IBOutlet weak var dayLabel:UILabel!
    @IBOutlet weak var monthAndYearLabel:UILabel!
    @IBOutlet weak var separatorForDate:UIView!
    @IBOutlet weak var upComingLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //Function to set title label accrdingly if entry is read.
    func setEntryRead(isread:Bool)  {
        if isread {
            titleLabel.textColor = UIColor.black
        }else {
            let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
            titleLabel.textColor = colur
        }
    }
    
    func setcolorforupcoming(isUpcoming:Bool)
    {
        if isUpcoming{
            let colur = UIColor(red: 99.0/255.0, green: 198.0/255.0, blue: 116.0/255.0, alpha: 1.0)
            upComingLabel.textColor = colur
            }
        else{
            let colur = UIColor(red: 229.0/255.0, green: 91.0/255.0, blue: 67.0/255.0, alpha: 1.0)
            upComingLabel.textColor = colur
            }
        
    }
    
    func setContentLabelTextWithString(content:String?) {
        if let expl = content {
            contentLabel.text = expl
        }else {
            contentLabel.text = nil
        }
    }
    
    func removeAllImagesFromImageContainerView() -> Void {
        imageContainerView.subviews.forEach { $0.removeFromSuperview() }
    }
    
    //MARK:- Diary Methods
     func setCellForDiary(){
        reminderButtonWidth.constant = 0
        separatorForDate.isHidden = true
        upComingLabel.isHidden = true
    }
    func setDiaryCellForNoAttachments() {
        downLoadButtonHeight.constant = 0
        attachmentShowingImageWidthConstraint.constant = 0
        separatorView.isHidden = true
    }
    func setDiaryCellForAttachment()  {
        downLoadButtonHeight.constant = 22.0
        attachmentShowingImageWidthConstraint.constant = 10.0
        separatorView.isHidden = false
    }
  
    
    
}
