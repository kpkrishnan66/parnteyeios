//
//  CommonDateView.swift
//  parentEye
//
//  Created by Martin Jacob on 25/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

@IBDesignable public class CommonDateView: UIView {

    var view:UIView!
    @IBOutlet weak var dayLabel:UILabel!
    @IBOutlet weak var monthAndYearLabel:UILabel!
    @IBOutlet weak var separatorView:UIView!
    @IBOutlet weak var upComingLabel:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       // addMonthLabel()
        xibSetup()
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // addMonthLabel()
        xibSetup()
        
        
    }
    
    
   
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CommonDateView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
    


    
    public override func didMoveToSuperview() {
        self.setNeedsLayout()
        self.layoutIfNeeded()

    }
}
