//
//  sync.swift
//  parentEye
//
//  Created by scientia on 06/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

public class sync {
    
   static var lastSyncTime = ""
    
    public class func syncNow() {
        print("Sync")
        
         var user:userModel? // Used tovarore the userdata.
         var userRole:String = ""
        
            user = DBController().getUserFromDB()
              if user?.currentTypeOfUser == .guardian {
                userRole = "Guardian"
              }
              else if user?.currentTypeOfUser == .employee {
                userRole = "Employee"
              }
              else {
                userRole = "CorporateManager"
              }
        //First get the nsObject by defining as an optional anyObject
        let nsObject: AnyObject? = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as AnyObject?
        
        //Then just cast the object as a String, but be careful, you may want to double check for nil
        let version = nsObject as! String
        UserDefaults.standard.set(version, forKey:"versionNumber")

            /*let date = NSDate()
            let calender = NSCalendar.currentCalendar()
            let components = calender.components([.Hour, .Minute], fromDate: date1)*/
        /*let date1 = NSUserDefaults.standardUserDefaults().objectForKey("updateUserTimes") as! NSDate
        print(date1)
        print(NSDate())
        let compareResult = date1.compare(NSDate())
        let interval = date1.timeIntervalSinceDate(NSDate())
        print(interval)*/
        
            WebServiceApi.getMultiRoleSwitchForEmployee(userId: user!.userId,switchAs:userRole,onSuccess: { (result) in
                
                
                if let PasswordResponse = result["error"] as? NSDictionary{
                    if let errorMessage = PasswordResponse["errorMessage"] as? String{
                        
                        DBController().clearDataBse()
                        let app = UIApplication.shared.delegate as! AppDelegate
                        app.setThisViewControllerWithIdentifierAsRoot(identifier: "LoginViewController", InStoryBoard: "Main")
                        AlertController.showToastAlertWithMessage(message: errorMessage, stateOfMessage: .success)
                    }
                }
                if (result["user"] as? NSDictionary) != nil{
                   let rt = RootClass.init(fromDictionary: result as NSDictionary)
                      if rt?.failureReason == nil {
                         // setting the user defaults to show that we are logged in
                    
                         if let userModel = userModel(makeUserModelFromRootObject: rt!){
                        
                           DBController().addUserToDB(userModel)
                            UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)
                            let app = UIApplication.shared.delegate as! AppDelegate
                            app.maketabBarRoot()
                         }
                     }
                }
                
                
                }, onFailure: { (error) in
                    
                    AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                    
                    
                }, duringProgress: nil)
        }
    public class func checkForSync(){
        if lastSyncTime == ""{
            syncNow()
        }else{
            let now = Foundation.Date()
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd hh:mm"
            let lastSyncNSDate = dateFormatter.date(from: lastSyncTime)
            let newdateString = dateFormatter.string(from: now)
            let newDate = lastSyncNSDate!.addingTimeInterval(60*60)
            let newNSDate = dateFormatter.date(from: newdateString)
            if ((newDate.compare(newNSDate!)) == .orderedAscending){
                syncNow()
            }

        }
    }
    
}


   
