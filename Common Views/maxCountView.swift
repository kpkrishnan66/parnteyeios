//
//  maxCountView.swift
//  parentEye
//
//  Created by scientia on 03/01/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

protocol dismissKeyBoardDelegateProtocol {
    func didPressButton()
}
protocol infoButtonDelegateProtocol {
    func didPressedButton()
}

 class maxCountView: UIView {
    
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var characterCount: UILabel!
    @IBOutlet weak var messageExceedLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var infoIcon: UIButton!
    
    
    var view:UIView!
    var countdelegate:dismissKeyBoardDelegateProtocol?
    var infoButtonDelegate:infoButtonDelegateProtocol?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        // addMonthLabel()
        xibSetup()
        //done.addTarget(self, action: #selector(maxCountView.buttonPress(_:)), forControlEvents: .TouchUpInside)
    }
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // addMonthLabel()
        xibSetup()
        
        
    }
    
    @IBAction func buttonPress(_ sender: Any) {
        self.countdelegate?.didPressButton()
    }
    
    @IBAction func infoButtonAction(_ sender: Any) {
     self.infoButtonDelegate?.didPressedButton()
    }
    
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "maxCountView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        //view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        //view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
    }
    
    
    
    public override func didMoveToSuperview() {
        /*self.setNeedsLayout()
        self.layoutIfNeeded()*/
        
    }

}
