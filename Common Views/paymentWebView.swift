//
//  paymentWebView.swift
//  parentEye
//
//  Created by scientia on 24/07/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import UIKit

 protocol paymentWebviewProtocol:class{
    func paymentWebviewResponse(referenceId:String)
 }

class paymentWebView:UIViewController,UIWebViewDelegate {
    
    @IBOutlet var webView: UIWebView!
    @IBOutlet var closeButton: UIButton!
    
    weak var delegate:paymentWebviewProtocol?
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    let activityLabel = UILabel(frame: CGRect(x:0,y:0,width:200,height:200))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = UIScreen.main.bounds
        self.webView.delegate = self
        //indicator.center =  view.center
        indicator.center = CGPoint(x:view.frame.width/2, y:view.frame.height/2)
        webView.addSubview(indicator)
        
        
        activityLabel.center = CGPoint (x: view.frame.width/2 , y: view.frame.height/1.8)
        activityLabel.text = "Loading.."
        activityLabel.textAlignment = .center
        webView.addSubview(activityLabel)
        
        
        
    }
    
   
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("start")
        indicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       print("stop")
       activityLabel.text = ""
       indicator.stopAnimating()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        switch navigationType {
        case .other:
            if (request.url!.scheme! == "alert") {
                print("inside alert")
                print(webView.stringByEvaluatingJavaScript(from: "response")!)
                //print(webView.stringByEvaluatingJavaScriptFromString("success. transaction ref is ")!)
                let response = request.url!.query
                self.delegate?.paymentWebviewResponse(referenceId: response!)
                self.dismiss(animated: false, completion: nil)
                return false
            }
            if (request.url!.scheme! == "close") {
                print("close")
                self.delegate?.paymentWebviewResponse(referenceId: "")
                self.dismiss(animated: false, completion: nil)
            }
        default:
            return true
        }
        return true
    }
    

    
    
    
    @IBAction func closeWebView(_ sender: UIButton) {
    
        if self.webView.canGoBack {
            self.webView.goBack()
            
        }
        else{
           self.dismiss(animated: true, completion: nil)
            
        }
    }
    //payment is in progress.Do you want to cancel the transaction
    //9447338360
}
