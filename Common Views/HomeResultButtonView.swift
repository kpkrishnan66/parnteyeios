//
//  HomeResultButtonView.swift
//  parentEye
//
//  Created by scientia on 03/08/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit

class HomeResultButtonView:UIView {
    
    
    @IBOutlet var mainView: UIView!
    
    @IBOutlet var progresscardImageView: UIImageView!
    
    @IBOutlet var progresscardLabel: UILabel!
    
    @IBOutlet var classTestImageView: UIImageView!
    
    @IBOutlet var classTestLabel: UILabel!
    
    @IBOutlet var graphImageView: UIImageView!
    
    @IBOutlet var graphLabel: UILabel!
    
    @IBOutlet var nutshellImageView: UIImageView!
    
    @IBOutlet var nutshellLabel: UILabel!
    
    @IBOutlet var horizontalView: UIView!
    
    @IBOutlet var verticalView: UIView!
    
    
}
