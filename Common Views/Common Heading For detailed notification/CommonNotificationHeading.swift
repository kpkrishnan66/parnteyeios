//
//  CommonNotificationHeading.swift
//  parentEye
//
//  Created by Martin Jacob on 16/05/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit

@IBDesignable class CommonNotificationHeading: UIView {
    
    var view:UIView!
    
    @IBOutlet weak var updatedUponLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var yearMonthLabel: UILabel!
    @IBOutlet weak var pastOrUpComingLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var designationLabel: UILabel!
    @IBOutlet weak var updatedUponDescriptionLabel: UILabel!
    @IBOutlet weak var reminderButton: UIButton!
    @IBOutlet weak var reminderButtonHeight: NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
        // addMonthLabel()
        xibSetup() 
    }
    required internal init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // addMonthLabel()
        xibSetup()
        
        
    }

    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CommonNotificationHeading", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    private func xibSetup() {
        view = loadViewFromNib()
        //view.frame = bounds
        //view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(view)
        
        let leftSideConstraint = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1.0, constant: 0.0)
        let bottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        let widthConstraint = NSLayoutConstraint(item: view, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0.0)
        let heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0.0)
        self.addConstraints([leftSideConstraint, bottomConstraint, heightConstraint, widthConstraint])
    }
    
    
    
    
    public override func didMoveToSuperview() {
        //self.setNeedsLayout()
        //self.layoutIfNeeded()
        
    }

}
