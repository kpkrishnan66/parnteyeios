//
//  CommonHeaderForSwitch.swift
//  parentEye
//
//  Created by Shaiju Cheriya Kummeri on 07/07/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
//
//  CommonHeader.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit




@IBDesignable public class CommonHeaderForSwitch: UIView {
    
    weak var delegate:CommonHeaderProtocol?
    
    @IBOutlet var hambergerButton: UIButton!
    @IBOutlet var switchMultiRole: UIButton!
    @IBOutlet var screenShowLabel: UILabel!
    var view:UIView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        
        
    }
    private func loadViewFromNib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CommonHeaderForSwitch", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        
    }
    
    /**
     <#Description#>
     - parameter sender: <#sender description#>
     */
    
    
    
    
    
    @IBAction func hambergerButtonPressed(_ sender: Any) {
    self.delegate?.showHamburgerMenuOrPerformOther()
    }
    
    
    @IBAction func multiSwitchAction(_ sender: UIButton) {
    self.delegate?.showSwitchSibling(fromButton: sender)
    }
    
}
