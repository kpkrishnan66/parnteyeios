//
//  CommonHeader.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import UIKit




@IBDesignable public class CommonHeader: UIView {

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var profileChangeButton:UIButton!
    @IBOutlet weak var screenShowingLabel:UILabel!
    @IBOutlet var rightLabel: UIButton!
    @IBOutlet weak var downArrowButton: UIButton!
    @IBOutlet weak var profileImageRightSeparator: UIView!
    @IBOutlet var downArrowButtonWidth: NSLayoutConstraint!
    @IBOutlet var downArrowWidth: UIButton!
    @IBOutlet var multiSwitchButton: UIButton!
    
    
    var user:userModel? // Used tovarore the userdata.
    var view:UIView!
    var isPassword:Bool = false
    weak var delegate:CommonHeaderProtocol?
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
        
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        
        
    }
    func getUserDataFromDB() -> Void {
        user = DBController().getUserFromDB()
    }
    
    
    private func loadViewFromNib() -> UIView {
        
        getUserDataFromDB()
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CommonHeader", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    private func xibSetup() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        ImageAPi.makeButtonRound(button: profileChangeButton)
        
    }
    
    
    
    public func setUpheaderForHomeScreen(){
        multiSwitchButton.isHidden = true
        self.backgroundColor = UIColor.clear
        rightLabel.setTitle(ConstantsInUI.switchToMe, for: .normal)
    }
    
    public func setUPHeaderForMultiRole(name:String){
        screenShowingLabel.text = name
        profileChangeButton.isHidden = true
        rightLabel.isHidden = true
        downArrowButton.isHidden = true
        profileImageRightSeparator.isHidden = true
        multiSwitchButton.isHidden = false
        
    }
    
    public func setUPHeaderForEmployee(name:String){
        profileChangeButton.isHidden = true
        rightLabel.isHidden = true
        downArrowButton.isHidden = true
        profileImageRightSeparator.isHidden = true
        multiSwitchButton.isHidden = true
        screenShowingLabel.text = name
        
    }
    
    public func setupHeaderForCorporateManager(name:String) {
       leftButton.isHidden = false
       screenShowingLabel.isHidden = true
       profileImageRightSeparator.isHidden = true
       rightLabel.isHidden = true
       screenShowingLabel.isHidden = true
       profileChangeButton.isHidden = true
       downArrowButton.isHidden = true
       multiSwitchButton.isHidden = false
       multiSwitchButton.setTitle(name, for: .normal)
       
    }
    
    
    public func setUpHeaderForRootScreensWithNameOfScreen(name:String, withProfileImageUrl prfIMgUrl:String){
        if let stdnts = user?.Students{
            if stdnts.count == 1 && user?.switchAs == ""{
                rightLabel.isHidden = true
                profileImageRightSeparator.isHidden = true
                
            }
        }
        multiSwitchButton.isHidden = true
        screenShowingLabel.text = name
        weak var weakProfileButton = profileChangeButton
        ImageAPi.fetchImageForUrl(urlString: prfIMgUrl, oneSuccess: { (image) in
            
            if weakProfileButton != nil {
                weakProfileButton!.setBackgroundImage(image, for: .normal) }
            
        }) { (error) in
            
            
        }
    }
    
    public func setUpHeaderForNormalScreenWithNameOfScreen(name:String, withProfileImageUrl prfIMgUrl:String ){
        if user?.currentTypeOfUser == .guardian{
            if user?.switchAs == "" || user?.switchAs == "Employee"{
        rightLabel.isHidden = true
        profileImageRightSeparator.isHidden = true
         multiSwitchButton.isHidden = true
            weak var weakProfileButton = profileChangeButton
            ImageAPi.fetchImageForUrl(urlString: prfIMgUrl, oneSuccess: { (image) in
                
                if weakProfileButton != nil {
                    weakProfileButton!.setBackgroundImage(image, for: .normal) }
                
            }) { (error) in
                
            }
            }
        }
        else{
            rightLabel.isHidden = true
            profileImageRightSeparator.isHidden = true
            downArrowButton.isHidden = true
            profileChangeButton.isHidden = true
            if user?.currentTypeOfUser == .employee {
            multiSwitchButton.isHidden = true
            }
            if user?.currentTypeOfUser == .CorporateManager {
             multiSwitchButton.isHidden = false
             multiSwitchButton.setTitle(user?.schools![(user?.currentSchoolSelected)!].schoolName, for: .normal)
            }
            
        }
        
        leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        screenShowingLabel.text = name
        
        
    }
    
    public func SetupForChangePassword (){
        leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        if(isPassword){
            rightLabel.setTitle("SKIP", for: .normal)
            rightLabel.titleLabel?.textAlignment = .right
            rightLabel.setTitleColor(UIColor.white, for: .normal)
            multiSwitchButton.isHidden = true
        }
        else{
            profileChangeButton.isHidden = true
            rightLabel.isHidden = true
        }
        profileChangeButton.isHidden = true
        profileImageRightSeparator.isHidden = true
        downArrowButton.isHidden = true
        multiSwitchButton.isHidden = true
        screenShowingLabel.text = ConstantsInUI.changePassword
    }
    public func SetupForForgotPassword (){
        leftButton.setImage(UIImage(named: "whiteBackArrow"), for: UIControlState.normal)
        profileChangeButton.isHidden = true
        rightLabel.isHidden = true
        profileChangeButton.isHidden = true
        profileImageRightSeparator.isHidden = true
        downArrowButton.isHidden = true
        multiSwitchButton.isHidden = true
        screenShowingLabel.text = ConstantsInUI.forgotPassword
    }
    //MARK:- IBActions
    
    @IBAction func leftButtonPressed(_ sender: Any) {
      self.delegate?.showHamburgerMenuOrPerformOther()
    }
    

    
    @IBAction func profileImageButtonPressed(_ sender: UIButton) {
       self.delegate?.showSwitchSibling(fromButton: sender)
    }
    
    @IBAction func downArrowButtonPressed(_ sender: UIButton) {
         self.delegate?.showSwitchSibling(fromButton: sender)
    }
    
    @IBAction func rightLabelButtonTapped(_ sender: UIButton) {
        self.delegate?.showSwitchSibling(fromButton: sender)
    }
    
    @IBAction func multiSwitchAction(_ sender: Any) {
        switchToGuardian(sender: sender)
    }
    
    func switchToGuardian(sender: Any) {
        if user?.currentTypeOfUser == .employee {
            
            hudControllerClass.showNormalHudToViewControllerForUIView(viewController: self)
            WebServiceApi.getMultiRoleSwitchForEmployee(userId: (user?.userId)!,switchAs: (user?.switchAs)!,
                                                        
                                                        onSuccess: { (result) in
                                                            
                                                            hudControllerClass.hideHudInViewControllerForUIView(viewController: self)
                                                            let rt = RootClass.init(fromDictionary: result as NSDictionary)
                                                            if rt?.failureReason == nil {
                                                                // setting the user defaults to show that we are logged in
                                                                //DBController().clearDataBse()
                                                                if let userModel = userModel(makeUserModelFromRootObject: rt!){
                                                                    
                                                                    if userModel.currentTypeOfUser == .guardian {
                                                                        DBController().addUserToDB(userModel)
                                                                        UserDefaults.standard.set(userDerfaultConstants.isLoggedIn, forKey: userDerfaultConstants.isLoggedIn)
                                                                        
                                                                        let app = UIApplication.shared.delegate as! AppDelegate
                                                                        app.maketabBarRoot()
                                                                    }
                                                                }
                                                                
                                                                
                                                                
                                                                
                                                            }
                                                            else
                                                            {       AlertController.showToastAlertWithMessage(message: (rt?.failureReason)!, stateOfMessage: .success)
                                                            }
                                                            
                                                            
            },
                                                        onFailure: { (error) in
                                                            AlertController.showToastAlertWithMessage(message: error.localizedDescription, stateOfMessage: .failure)
                                                            
            },
                                                        duringProgress: { (progress) in
                                                            
            })
            
            
        }
        
        if user?.currentTypeOfUser == .CorporateManager {
            self.delegate?.showSwitchSibling(fromButton: sender as! UIButton)
        }
    }
}
