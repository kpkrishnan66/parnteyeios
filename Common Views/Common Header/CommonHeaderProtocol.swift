//
//  CommonHeaderProtocol.swift
//  parentEye
//
//  Created by Martin Jacob on 13/04/16.
//  Copyright © 2016 Scientia. All rights reserved.
//

import Foundation
import UIKit
protocol CommonHeaderProtocol:class {
    func showHamburgerMenuOrPerformOther()->Void
    func showSwitchSibling(fromButton:UIButton) ->Void
    
}
