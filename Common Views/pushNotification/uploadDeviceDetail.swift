//
//  uploadDeviceDetail.swift
//  parentEye
//
//  Created by scientia on 12/04/18.
//  Copyright © 2018 Scientia. All rights reserved.
//

import Foundation
import UIKit

public class uploadDeviceDetail{
    public class func deviceDetails(_ deviceToken:String){
        print("uploadDeviceDetails")
        UserDefaults.standard.set("true",forKey: userDerfaultConstants.deviceTokenRegistered)
        var user:userModel?
        user = DBController().getUserFromDB()
        let userId = user?.userId
        if userId != nil{
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let deviceType = "FCM"

        WebServiceApi.upLoadDeviceDetailsForPushNotification(userId: userId!,deviceToken:deviceToken,deviceId: deviceId,deviceType: deviceType,
                                                             onSuccess: { (result) in
                                                                print("uploadDeviceDetails sucess")
                                                                
                                                                
        }, onFailure: { (error) in
            print("uploadDeviceDetails error")
            UserDefaults.standard.set("false",forKey: userDerfaultConstants.deviceTokenRegistered)
        }) { (progress) in
            
        }
        }
        
    }
    
}

