//
//  audioPlay.swift
//  parentEye
//
//  Created by scientia on 17/06/17.
//  Copyright © 2017 Scientia. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

protocol audioPlayBackProtocol : class {
    func exitSubView()
}

 class audioPlay:UIView,AVAudioPlayerDelegate {
    
    
    @IBOutlet var playAudioButton: UIButton!
    @IBOutlet var soundSlider: UISlider!
    @IBOutlet var playTimeText: UILabel!
    @IBOutlet var audioView: UIView!
    
   
    
    var updater : CADisplayLink! = nil
    var player : AVAudioPlayer! = nil
    var toPass = NSURL()
    var downloadUrl:String = ""
    var delegate : audioPlayBackProtocol?
    var destinationUrl = NSURL()
    var isFileExistAtLocal:Bool = false
    var playerPauseTime = TimeInterval()
    var playPauseEnabled:Bool = false
    var initialLoad:Bool = false
    var sliderChanged:Bool = false
    var isstopCADisplay:Bool = false
    
   
    @IBAction func playAudioButtonTapped(_ sender: UIButton) {
        self.play(url: toPass)
        print(toPass)
    }
    
    func play(url:NSURL) {
        
        playAudioButton.isSelected = !(playAudioButton.isSelected)
        if soundSlider.value == 0.0 || initialLoad == true || sliderChanged == true {
           playAudioButton.isSelected = true
        }
        
        
        if playAudioButton.isSelected || soundSlider.value == 0.0 {
            
            
            if soundSlider.value == 0.5 || soundSlider.value == 0.0 || initialLoad == true 
             {
                if isstopCADisplay == true {
                    updater.invalidate()
                    updater = nil
                }
            updater = CADisplayLink(target: self, selector: #selector(audioPlay.soundSliderTapped))
            updater.frameInterval = 1
            updater.add(to: RunLoop.current, forMode: RunLoopMode.commonModes)
            
        
        print("playing \(url)")
        do
        {
            self.player = try AVAudioPlayer(contentsOf: url as URL)
            player.prepareToPlay()
            player.numberOfLoops = 0
            player.volume = 2.0
            player.delegate = self
            if soundSlider.value == 0.5 || soundSlider.value == 0.0 || initialLoad == true {
             
             player.play()
             isstopCADisplay = true
             updateTime()
            }
            else{
                
            }
            print(player.currentTime)
            //playTimeText.text = String(player.currentTime)
            if  soundSlider.value == 0.5 || soundSlider.value == 0.0 || initialLoad == true {
            initialLoad = false
            soundSlider.minimumValue = 0
            soundSlider.maximumValue = 100
            }
        } catch let error as NSError {
            //self.player = nil
            print(error.localizedDescription)
            AlertController.showToastAlertWithMessage(message: ConstantsInUI.audioDownloadFailed, stateOfMessage: .failure)
        } catch {
            print("AVAudioPlayer init failed")
        }
        
       }
            else{
               playPauseEnabled = false
               sliderChanged = false
               
               player.numberOfLoops = 0
               player.play()
               updateTime()
            }
        }
        else {
            playPauseEnabled = true
            
            self.player.pause()
            updateTime()
            playerPauseTime = player.currentTime
            
        }
        
    }
    
    @objc func soundSliderTapped() {
        if playPauseEnabled == false {
            let normalizedTime = Float(player.currentTime * 100.0 / player.duration)
            soundSlider.value = normalizedTime
            print(player.currentTime)
            print(player.duration)
            if player.currentTime == 0.0 {
              playAudioButton.setImage(UIImage(named:"playIcon"), for: .normal)
            }
            else{
                if sliderChanged == true || player.currentTime == 0.0{
                    playAudioButton.setImage(UIImage(named:"playIcon"), for: .normal)
                }
                else{
                    playAudioButton.setImage(UIImage(named:"pauseIcon"), for: .normal)
                }
            }
        }
        else{
           playAudioButton.setImage(UIImage(named:"playIcon"), for: .normal) 
        }
        updateTime()
        let elapsed = CACurrentMediaTime()
        print("elapsed time = \(elapsed)")
        
    }
    
    @IBAction func trackAudio(_ sender: Any) {
        if initialLoad == false {
        sliderChanged = true
        print("player time")
        print(player.currentTime)
        print("sliderValue=\(soundSlider.value)")
        player.currentTime = TimeInterval((soundSlider.value/100)*(Float)(player.duration))
        print("after assign = \(player.currentTime)")
        updateTime()
        }
    }
    
   
    
    func updateTime() {
        let currentTime = Int(player.currentTime)
        let duration = Int(player.duration)
        let total = currentTime - duration
        _ = String(total)
        
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
        
        playTimeText.text = NSString(format: "%02d:%02d", minutes,seconds) as String
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        if player != nil {
        player.stop()
        updateTime()
        isstopCADisplay = false
        updater.invalidate()
        updater = nil
        }
        audioView.removeFromSuperview()
        delegate?.exitSubView()
        

    }
    
       // handle audio interruptions
   //https://stackoverflow.com/questions/38800204/how-to-resume-audio-after-interruption-in-swift
    //https://developer.apple.com/library/content/documentation/Audio/Conceptual/AudioSessionProgrammingGuide/HandlingAudioInterruptions/HandlingAudioInterruptions.html
 }


